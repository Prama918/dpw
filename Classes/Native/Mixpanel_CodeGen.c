﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 <item>j__TPar <>f__AnonymousType0`2::get_item()
// 0x00000002 <inx>j__TPar <>f__AnonymousType0`2::get_inx()
// 0x00000003 System.Void <>f__AnonymousType0`2::.ctor(<item>j__TPar,<inx>j__TPar)
// 0x00000004 System.Boolean <>f__AnonymousType0`2::Equals(System.Object)
// 0x00000005 System.Int32 <>f__AnonymousType0`2::GetHashCode()
// 0x00000006 System.String <>f__AnonymousType0`2::ToString()
// 0x00000007 System.Void mixpanel.Config::.cctor()
extern void Config__cctor_m76C873B0B3F866AD5AB6163AFA338B78EC2678EC ();
// 0x00000008 System.Void mixpanel.Controller::InitializeBeforeSceneLoad()
extern void Controller_InitializeBeforeSceneLoad_m9E41E740A2B4A5CAF737EE2D774F5BC607794921 ();
// 0x00000009 System.Void mixpanel.Controller::InitializeAfterSceneLoad()
extern void Controller_InitializeAfterSceneLoad_m331099178D617BFC83D2DF7EB27555E51C5CF6F4 ();
// 0x0000000A mixpanel.Controller mixpanel.Controller::GetInstance()
extern void Controller_GetInstance_mCD620C7F78BE8D8E681C58CE2E5876E6FF48065E ();
// 0x0000000B System.Void mixpanel.Controller::OnDestroy()
extern void Controller_OnDestroy_m60483C6471483CCEDB39E6F11094C686834999B1 ();
// 0x0000000C System.Void mixpanel.Controller::OnApplicationPause(System.Boolean)
extern void Controller_OnApplicationPause_m5EF81D2166D169145FCEFA6DE0A4ED2FA0D1CF03 ();
// 0x0000000D System.Collections.IEnumerator mixpanel.Controller::Start()
extern void Controller_Start_m85877E9FBA4B17C9D82AFB9F5A528F46A966F0A9 ();
// 0x0000000E System.Void mixpanel.Controller::TrackIntegrationEvent()
extern void Controller_TrackIntegrationEvent_mF43CF282CD7BBC35381EC723D29CD02EB921C2F3 ();
// 0x0000000F System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.UnityWebRequest> mixpanel.Controller::WaitForIntegrationRequest(UnityEngine.Networking.UnityWebRequest)
extern void Controller_WaitForIntegrationRequest_mCD27154B6B5F76B42615CD834804EAC5B9CBF6DE ();
// 0x00000010 System.Collections.IEnumerator mixpanel.Controller::PopulatePools()
extern void Controller_PopulatePools_mC4895BE19C981E90C810A26996AA4C35282F6D83 ();
// 0x00000011 System.Void mixpanel.Controller::MigrateFrom1To2()
extern void Controller_MigrateFrom1To2_m6C44D065949722B86359991417850529757B225A ();
// 0x00000012 mixpanel.Value mixpanel.Controller::GetEngageDefaultProperties()
extern void Controller_GetEngageDefaultProperties_m6DC0064234C2B567CBDE6D9A08A23F06AD4CC391 ();
// 0x00000013 mixpanel.Value mixpanel.Controller::GetEventsDefaultProperties()
extern void Controller_GetEventsDefaultProperties_mB811B913107D1901E221D70FED07353FF5BA3906 ();
// 0x00000014 System.Void mixpanel.Controller::DoTrack(System.String,mixpanel.Value)
extern void Controller_DoTrack_m53C4A8F3606EB1F87C986F93F1C11ED436AA9105 ();
// 0x00000015 System.Void mixpanel.Controller::DoEngage(mixpanel.Value)
extern void Controller_DoEngage_m068D7F6406673657BB0C89B5CE134AAF29F29BCE ();
// 0x00000016 System.Void mixpanel.Controller::DoFlush()
extern void Controller_DoFlush_m252908AB55C0016F77234B352402F7BA11F4B626 ();
// 0x00000017 System.Void mixpanel.Controller::DoClear()
extern void Controller_DoClear_m70187181AB6FEDE7B9CB9F61C2B166C060280A00 ();
// 0x00000018 System.Void mixpanel.Controller::.ctor()
extern void Controller__ctor_m98F7D8656EFE994E6E5FD632F8DC940840C39BF9 ();
// 0x00000019 System.Collections.Generic.IEnumerable`1<System.Collections.Generic.IEnumerable`1<T>> mixpanel.Extensions::Batch(System.Collections.Generic.IEnumerable`1<T>,System.Int32)
// 0x0000001A System.Void mixpanel.Mixpanel::Log(System.String)
extern void Mixpanel_Log_m299A441F3C29E0740A12F5BD98D832E94F195FE7 ();
// 0x0000001B System.Void mixpanel.Mixpanel::LogError(System.String)
extern void Mixpanel_LogError_mB342A805B3E9E282F31B0DA467F8A7DAC2229E6A ();
// 0x0000001C System.Void mixpanel.Mixpanel::Alias(System.String)
extern void Mixpanel_Alias_mD851A043999D6A73BF1B12BD12FD38D72D6D4C65 ();
// 0x0000001D System.Void mixpanel.Mixpanel::ClearTimedEvents()
extern void Mixpanel_ClearTimedEvents_mDA20AB2EF921657CC71ABFB08C6780BD146D9CFA ();
// 0x0000001E System.Void mixpanel.Mixpanel::ClearTimedEvent(System.String)
extern void Mixpanel_ClearTimedEvent_m4A4EDAD0C1DC7732010DD272B4ED80FDB47FDBF2 ();
// 0x0000001F System.Void mixpanel.Mixpanel::Identify(System.String)
extern void Mixpanel_Identify_mE1F9403FB085EC360AED39FF0AF033E28B3DD333 ();
// 0x00000020 System.String mixpanel.Mixpanel::get_DistinctID()
extern void Mixpanel_get_DistinctID_mB00F357B5B3C9C1E10578A8A288098D3E8E00368 ();
// 0x00000021 System.String mixpanel.Mixpanel::get_DistinctId()
extern void Mixpanel_get_DistinctId_m33637E68B66C7D3CDCC9C35FE588CFDB408CC42F ();
// 0x00000022 System.Void mixpanel.Mixpanel::OptOutTracking()
extern void Mixpanel_OptOutTracking_m732D3139A9ACAF9EE7A4577BF7D8BBB7ED4BA21A ();
// 0x00000023 System.Void mixpanel.Mixpanel::OptInTracking()
extern void Mixpanel_OptInTracking_m762A2A27B5808A4D35F86E4119FE3933B6D6CB53 ();
// 0x00000024 System.Void mixpanel.Mixpanel::OptInTracking(System.String)
extern void Mixpanel_OptInTracking_m7F6277AEC48DDE7E7562FB5BA29C66728EFB3BDC ();
// 0x00000025 System.Void mixpanel.Mixpanel::Register(System.String,mixpanel.Value)
extern void Mixpanel_Register_m6E8F1FA5277795BCDE4FAC658AAB85E1A2B564FE ();
// 0x00000026 System.Void mixpanel.Mixpanel::RegisterOnce(System.String,mixpanel.Value)
extern void Mixpanel_RegisterOnce_m464052763F1123D5D3F2501F4E963252E59B2C0E ();
// 0x00000027 System.Void mixpanel.Mixpanel::Reset()
extern void Mixpanel_Reset_mD7C9EA0B32C77C07CD37F6AA34C9A9A305795E54 ();
// 0x00000028 System.Void mixpanel.Mixpanel::Clear()
extern void Mixpanel_Clear_m052A22A5008561F8D6BCAC96ED270D85444C617F ();
// 0x00000029 System.Void mixpanel.Mixpanel::StartTimedEvent(System.String)
extern void Mixpanel_StartTimedEvent_mA716485D9E5254A5ABA97F6394736A39160AC005 ();
// 0x0000002A System.Void mixpanel.Mixpanel::StartTimedEventOnce(System.String)
extern void Mixpanel_StartTimedEventOnce_m49A7CDEBA05DA916DA093FAE6C8BABC05665B9C2 ();
// 0x0000002B System.Void mixpanel.Mixpanel::Track(System.String)
extern void Mixpanel_Track_mCBFD38C713C0369905C54450E867444318AE803E ();
// 0x0000002C System.Void mixpanel.Mixpanel::Track(System.String,System.String,mixpanel.Value)
extern void Mixpanel_Track_m87834893FE7643F9AD58A23BD02EC49773FA0D77 ();
// 0x0000002D System.Void mixpanel.Mixpanel::Track(System.String,mixpanel.Value)
extern void Mixpanel_Track_mD28F4D081033D27062BAED0585AD9BB72E237D6B ();
// 0x0000002E System.Void mixpanel.Mixpanel::Unregister(System.String)
extern void Mixpanel_Unregister_m03768AD614FACEDD46548554AB0D4C1E97301FAC ();
// 0x0000002F System.Void mixpanel.Mixpanel::Flush()
extern void Mixpanel_Flush_m341FF29FD6E97E3A95190DB6E991E2FD62478B87 ();
// 0x00000030 System.Void mixpanel.Mixpanel::SetToken(System.String)
extern void Mixpanel_SetToken_m83AC5B1CEC50253F130D4DF29FA7B7B02FAAE6E2 ();
// 0x00000031 System.Void mixpanel.Mixpanel::Put(mixpanel.Value)
extern void Mixpanel_Put_mDFD85570E526E4F60F09BC53F14C201C6AEC1C79 ();
// 0x00000032 System.Void mixpanel.Mixpanel::.cctor()
extern void Mixpanel__cctor_mA662E0675FD800C76A1E6DC128788DB1D840334F ();
// 0x00000033 System.String mixpanel.MixpanelSettings::get_Token()
extern void MixpanelSettings_get_Token_mCCC83428615584D6E1800DA9CBA6B33BDD4430C7 ();
// 0x00000034 System.Void mixpanel.MixpanelSettings::LoadSettings()
extern void MixpanelSettings_LoadSettings_mCBF0E978B1A479025DD13D0A3C110F00652248EB ();
// 0x00000035 mixpanel.MixpanelSettings mixpanel.MixpanelSettings::get_Instance()
extern void MixpanelSettings_get_Instance_m7B7BC51EC7413F5BC5BD46C2FA7CC0826120A87A ();
// 0x00000036 mixpanel.MixpanelSettings mixpanel.MixpanelSettings::FindOrCreateInstance()
extern void MixpanelSettings_FindOrCreateInstance_mEAEA7748D36F5AEBCBC5A8EBA931D7FACC259A9B ();
// 0x00000037 T mixpanel.MixpanelSettings::CreateAndSave()
// 0x00000038 System.Void mixpanel.MixpanelSettings::.ctor()
extern void MixpanelSettings__ctor_mC0F27A023F7B37D006AA835056AF341576CE1C8B ();
// 0x00000039 System.Void mixpanel.IMixpanelPoolable::OnRecycle()
// 0x0000003A System.Void mixpanel.Pool`1::.ctor(System.Func`1<T>)
// 0x0000003B T mixpanel.Pool`1::Get()
// 0x0000003C System.Void mixpanel.Pool`1::Put(T)
// 0x0000003D System.Int32 mixpanel.Pool`1::get_Count()
// 0x0000003E System.Boolean mixpanel.MixpanelStorage::get_HasMigratedFrom1To2()
extern void MixpanelStorage_get_HasMigratedFrom1To2_m072A035552E825DA7C30C08C7786390F5CCAC9D2 ();
// 0x0000003F System.Void mixpanel.MixpanelStorage::set_HasMigratedFrom1To2(System.Boolean)
extern void MixpanelStorage_set_HasMigratedFrom1To2_m4C88CCA7C9E256C8A624AF6BF1C48CED011B3AF3 ();
// 0x00000040 System.Boolean mixpanel.MixpanelStorage::get_HasIntegratedLibrary()
extern void MixpanelStorage_get_HasIntegratedLibrary_m12A4A0A9C134DFD87FB1980E1222404BA988BAA9 ();
// 0x00000041 System.Void mixpanel.MixpanelStorage::set_HasIntegratedLibrary(System.Boolean)
extern void MixpanelStorage_set_HasIntegratedLibrary_m2621E27E7C01FABB8D826993FF35BC052D56D541 ();
// 0x00000042 System.String mixpanel.MixpanelStorage::get_DistinctId()
extern void MixpanelStorage_get_DistinctId_m7A3290B16574FC3452F5FCCB5CBDD35B1DB4E94A ();
// 0x00000043 System.Void mixpanel.MixpanelStorage::set_DistinctId(System.String)
extern void MixpanelStorage_set_DistinctId_m0B4644DF0DC2206EA6B8DBF4A413A1FB7ACC907F ();
// 0x00000044 System.Boolean mixpanel.MixpanelStorage::get_IsTracking()
extern void MixpanelStorage_get_IsTracking_mC91931B7E94D3887725590CC08A8170795BEABA6 ();
// 0x00000045 System.Void mixpanel.MixpanelStorage::set_IsTracking(System.Boolean)
extern void MixpanelStorage_set_IsTracking_m0FEE41668052655B7508D6CAD24EEC172223F0B8 ();
// 0x00000046 System.String mixpanel.MixpanelStorage::get_PushDeviceTokenString()
extern void MixpanelStorage_get_PushDeviceTokenString_m5C88CC3E2570BF835826C63454C5839412D73B1A ();
// 0x00000047 System.Void mixpanel.MixpanelStorage::set_PushDeviceTokenString(System.String)
extern void MixpanelStorage_set_PushDeviceTokenString_m1D553D5802D81774A3CEA3F3561516EAFED71D6C ();
// 0x00000048 System.Void mixpanel.MixpanelStorage::SavePushDeviceToken(System.String)
extern void MixpanelStorage_SavePushDeviceToken_m1BFF3FAF64DEEAC1D0B3EB973548F0013AE27394 ();
// 0x00000049 mixpanel.Value mixpanel.MixpanelStorage::get_OnceProperties()
extern void MixpanelStorage_get_OnceProperties_m5AB3C3ADF919B38E50A39719914A5CFB029D9FCD ();
// 0x0000004A System.Void mixpanel.MixpanelStorage::set_OnceProperties(mixpanel.Value)
extern void MixpanelStorage_set_OnceProperties_mE81DB961488A0124E9F945A8F82EEC8C7562EB04 ();
// 0x0000004B System.Void mixpanel.MixpanelStorage::ResetOnceProperties()
extern void MixpanelStorage_ResetOnceProperties_m6E73A2E4D4793EE2E5804989E24C029880391B79 ();
// 0x0000004C mixpanel.Value mixpanel.MixpanelStorage::get_SuperProperties()
extern void MixpanelStorage_get_SuperProperties_mAA440F4F304F9B7ADE26549013ED741B20078294 ();
// 0x0000004D System.Void mixpanel.MixpanelStorage::set_SuperProperties(mixpanel.Value)
extern void MixpanelStorage_set_SuperProperties_m95482C793A955B5163F3CDD9147C772A81515F6A ();
// 0x0000004E System.Void mixpanel.MixpanelStorage::ResetSuperProperties()
extern void MixpanelStorage_ResetSuperProperties_m825ED696264D16E98C6D2B8C6736375B050DB649 ();
// 0x0000004F mixpanel.Value mixpanel.MixpanelStorage::get_TimedEvents()
extern void MixpanelStorage_get_TimedEvents_m53546B09A5A947FDB35B8240933D37F20D642551 ();
// 0x00000050 System.Void mixpanel.MixpanelStorage::set_TimedEvents(mixpanel.Value)
extern void MixpanelStorage_set_TimedEvents_m4D439BE6FCE375DF7386461166DF407463DB4988 ();
// 0x00000051 System.Void mixpanel.MixpanelStorage::ResetTimedEvents()
extern void MixpanelStorage_ResetTimedEvents_m94771FC59C303619D62944CD26D260A476AAB52B ();
// 0x00000052 System.Void mixpanel.MixpanelStorage::.cctor()
extern void MixpanelStorage__cctor_m258716BE6636442936F8A537CEA2BFD72076E2FC ();
// 0x00000053 System.Boolean mixpanel.Value::get_IsNull()
extern void Value_get_IsNull_m7AFA60AD0836E4908811E93744CA0B94507946E9 ();
// 0x00000054 System.Boolean mixpanel.Value::get_IsArray()
extern void Value_get_IsArray_mC7777181EE6381C5C21E5A6F64E95559BB95C980 ();
// 0x00000055 System.Boolean mixpanel.Value::get_IsObject()
extern void Value_get_IsObject_m5BF07983249AE1785CAD765F4C4DB9C47FAE7DE5 ();
// 0x00000056 System.Void mixpanel.Value::OnRecycle()
extern void Value_OnRecycle_m53AE36316A611ABE351F03DE03D8FABF303F9B33 ();
// 0x00000057 mixpanel.Value mixpanel.Value::get_Item(System.Int32)
extern void Value_get_Item_mC16F70BF027016C13F45A298747A6C606207C2F2 ();
// 0x00000058 System.Void mixpanel.Value::set_Item(System.Int32,mixpanel.Value)
extern void Value_set_Item_mDC042E5BDC085E1326BD4808B3FED55517DA4E91 ();
// 0x00000059 mixpanel.Value mixpanel.Value::get_Item(System.String)
extern void Value_get_Item_mD6194F5800B63BED1CBE8A8DDE3FCB40BB82C34A ();
// 0x0000005A System.Void mixpanel.Value::set_Item(System.String,mixpanel.Value)
extern void Value_set_Item_m0AB3D98B3053F57091EAECD303BC3CB7FED82060 ();
// 0x0000005B System.String mixpanel.Value::ToString()
extern void Value_ToString_m5156C794630B6DDEDD908966E519719810CC3FD8 ();
// 0x0000005C System.Collections.IEnumerator mixpanel.Value::GetEnumerator()
extern void Value_GetEnumerator_m157913440E37BDFA7793E60C1AD9B8BDE6043D33 ();
// 0x0000005D System.Int32 mixpanel.Value::get_Count()
extern void Value_get_Count_m04B7EACB5F8EE36FB9C28D08A7812C123DCDC292 ();
// 0x0000005E System.Boolean mixpanel.Value::Contains(System.Int32)
extern void Value_Contains_m708B2CFE804F117EAB20138F87523C1FAA3A46ED ();
// 0x0000005F System.Boolean mixpanel.Value::ContainsKey(System.String)
extern void Value_ContainsKey_mEF229323A432A7BF4BAB759FEC089429E40B7A23 ();
// 0x00000060 System.Void mixpanel.Value::Add(mixpanel.Value)
extern void Value_Add_m4F71B067D552DF74EDEA92F68AEE6A183672B32E ();
// 0x00000061 System.Void mixpanel.Value::Add(System.String,mixpanel.Value)
extern void Value_Add_m69666C8196ABFF7B2C1C5ADF6546C20237258230 ();
// 0x00000062 System.Void mixpanel.Value::Remove(System.Int32)
extern void Value_Remove_m2DA1502A2ABC3B2960E43EBD0ED43EC713CDE250 ();
// 0x00000063 System.Void mixpanel.Value::Remove(System.String)
extern void Value_Remove_m07751B0F284D4D039ED18268A9CB56051760D5B0 ();
// 0x00000064 System.Collections.Generic.IEnumerable`1<mixpanel.Value> mixpanel.Value::get_Values()
extern void Value_get_Values_mD689BDC01C23F8884D009714406CDCAE73B9DB11 ();
// 0x00000065 System.Boolean mixpanel.Value::TryGetValue(System.String,mixpanel.Value&)
extern void Value_TryGetValue_mD61F77BB3208F1107E2A9AC283B117969F302AD0 ();
// 0x00000066 System.Void mixpanel.Value::Merge(mixpanel.Value)
extern void Value_Merge_m56764985179CFAA092966673F20EFDCF23B3A734 ();
// 0x00000067 System.String mixpanel.Value::get_String()
extern void Value_get_String_mB05CD0D474E75BC6C342F98FFF1D352951CE98F7 ();
// 0x00000068 System.Void mixpanel.Value::set_String(System.String)
extern void Value_set_String_mBB8E20A200A90E8C7DB46E30F4481C718236C984 ();
// 0x00000069 System.Boolean mixpanel.Value::get_Bool()
extern void Value_get_Bool_m381241CF04621A315F418BE27F608F7CB5886C29 ();
// 0x0000006A System.Void mixpanel.Value::set_Bool(System.Boolean)
extern void Value_set_Bool_m42E953C596CDFE29041EFC16E05E30E72F382D41 ();
// 0x0000006B System.Double mixpanel.Value::get_Number()
extern void Value_get_Number_m72782E5029E6533712EA62FEB55BF8BEBC5566C1 ();
// 0x0000006C System.Void mixpanel.Value::set_Number(System.Double)
extern void Value_set_Number_mA8E0D0FD2DBB40B2E46A97D492D1825AC91CB416 ();
// 0x0000006D System.Uri mixpanel.Value::get_Uri()
extern void Value_get_Uri_m5B3BA01D72EDB5D7510226E42A3025C388E43EAE ();
// 0x0000006E System.Void mixpanel.Value::set_Uri(System.Uri)
extern void Value_set_Uri_m9017F2D19F880F4B3BDC2DF9310838C5414EA8F3 ();
// 0x0000006F System.Guid mixpanel.Value::get_Guid()
extern void Value_get_Guid_m7CF58B3FF4B0176A5FD73FF34985B9E2C4C03C16 ();
// 0x00000070 System.Void mixpanel.Value::set_Guid(System.Guid)
extern void Value_set_Guid_mA773D4E9522F60C4AE1742821B505C7DD995AAF6 ();
// 0x00000071 System.DateTime mixpanel.Value::get_DateTime()
extern void Value_get_DateTime_m01CFB0861035AA0518E17820A679CC0FAF5D9F88 ();
// 0x00000072 System.Void mixpanel.Value::set_DateTime(System.DateTime)
extern void Value_set_DateTime_m9DDD70B5B0651C3B6705DA9A9DBFCF1AAB5A6090 ();
// 0x00000073 System.DateTimeOffset mixpanel.Value::get_DateTimeOffset()
extern void Value_get_DateTimeOffset_m87A6904DCE5220E8A596B89DDD87AF94830AC782 ();
// 0x00000074 System.Void mixpanel.Value::set_DateTimeOffset(System.DateTimeOffset)
extern void Value_set_DateTimeOffset_m46296BCB3C986B8562452AD9BBBE49C6348FF9D3 ();
// 0x00000075 System.TimeSpan mixpanel.Value::get_TimeSpan()
extern void Value_get_TimeSpan_m3DB536F2D66304104205101787F597C5F13A01C5 ();
// 0x00000076 System.Void mixpanel.Value::set_TimeSpan(System.TimeSpan)
extern void Value_set_TimeSpan_m5E5754CD18319240DFF81AAD54761FA59E2B9ACD ();
// 0x00000077 UnityEngine.Color mixpanel.Value::get_Color()
extern void Value_get_Color_m2B0CCD95EB30B830299F545C1BAB5DB9D589B121 ();
// 0x00000078 System.Void mixpanel.Value::set_Color(UnityEngine.Color)
extern void Value_set_Color_m615E24548DBE4F27CE1727AC69F687762EE25268 ();
// 0x00000079 UnityEngine.Color32 mixpanel.Value::get_Color32()
extern void Value_get_Color32_m63E5692FAB8E6D185BA78AB0C8FD720F5F14532C ();
// 0x0000007A System.Void mixpanel.Value::set_Color32(UnityEngine.Color32)
extern void Value_set_Color32_mA93609A2CAB4950B037A3F675F2FB63F301A0AE7 ();
// 0x0000007B UnityEngine.Vector2 mixpanel.Value::get_Vector2()
extern void Value_get_Vector2_mEAB035225FD92ECEB087DDCDCAFC6DFF2F39739B ();
// 0x0000007C System.Void mixpanel.Value::set_Vector2(UnityEngine.Vector2)
extern void Value_set_Vector2_mFF25A3F2B4B725D1CD29039861FEDFF96ED1AF91 ();
// 0x0000007D UnityEngine.Vector3 mixpanel.Value::get_Vector3()
extern void Value_get_Vector3_mE3172895E7231FBA354F3DA5118FEFD54AD1C8E7 ();
// 0x0000007E System.Void mixpanel.Value::set_Vector3(UnityEngine.Vector3)
extern void Value_set_Vector3_m55BCB7FA842C62FC250FFB69F0972FE2AC3798E2 ();
// 0x0000007F UnityEngine.Vector4 mixpanel.Value::get_Vector4()
extern void Value_get_Vector4_mFD212B272DA8C311CBFED955896E03CF72B6C82E ();
// 0x00000080 System.Void mixpanel.Value::set_Vector4(UnityEngine.Vector4)
extern void Value_set_Vector4_m255FA42336D80DEE5E1B5D439A48B84F310D5200 ();
// 0x00000081 UnityEngine.Quaternion mixpanel.Value::get_Quaternion()
extern void Value_get_Quaternion_mEC9712ADB3DA12CC033F1DF902CC481776E3538E ();
// 0x00000082 System.Void mixpanel.Value::set_Quaternion(UnityEngine.Quaternion)
extern void Value_set_Quaternion_m282EF205EA48B24D066211F9366F33A749382407 ();
// 0x00000083 UnityEngine.Bounds mixpanel.Value::get_Bounds()
extern void Value_get_Bounds_m52D2D83222613AB79F117FC560B7997A1777AA03 ();
// 0x00000084 System.Void mixpanel.Value::set_Bounds(UnityEngine.Bounds)
extern void Value_set_Bounds_m9D26F712BBCD8CA775284F038962ECA13A25DF55 ();
// 0x00000085 UnityEngine.Rect mixpanel.Value::get_Rect()
extern void Value_get_Rect_m13BA00EA5649F6C418827EF995F15EAC37692E93 ();
// 0x00000086 System.Void mixpanel.Value::set_Rect(UnityEngine.Rect)
extern void Value_set_Rect_mDC158D1B2F3E525AD163910B642DE5272F6AB79C ();
// 0x00000087 System.Void mixpanel.Value::.ctor()
extern void Value__ctor_mB395E4756C596B1F575A9AA2F51B60289DC04110 ();
// 0x00000088 System.Void mixpanel.Value::.ctor(mixpanel.Value_ValueTypes,mixpanel.Value_DataTypes)
extern void Value__ctor_mD087A88134F6D5F68807892CF4F686915FFCE70B ();
// 0x00000089 System.Void mixpanel.Value::.ctor(System.String)
extern void Value__ctor_mFD09BDD374EF187AD4F52894C01062DBB881EAEC ();
// 0x0000008A System.Void mixpanel.Value::.ctor(System.Boolean)
extern void Value__ctor_m76B08832F2863379E5EEA375F4005A6D2168C9A4 ();
// 0x0000008B System.Void mixpanel.Value::.ctor(System.Double)
extern void Value__ctor_m19F1CD8073DCE13DF20A4CE83572600D57A0F971 ();
// 0x0000008C System.Void mixpanel.Value::.ctor(System.Uri)
extern void Value__ctor_mA63237DB926368CA0D53F04DDE8587B1203FB118 ();
// 0x0000008D System.Void mixpanel.Value::.ctor(System.Guid)
extern void Value__ctor_m286C1ED9B22E0AD2F0F871DF4E0A3D3782FB9ADF ();
// 0x0000008E System.Void mixpanel.Value::.ctor(System.DateTime)
extern void Value__ctor_m8C56C49BC099ED3326622CAD854BAEEC499D842F ();
// 0x0000008F System.Void mixpanel.Value::.ctor(System.DateTimeOffset)
extern void Value__ctor_m1776DD5A97AAD025A2E995EBA63E7735C9A162DE ();
// 0x00000090 System.Void mixpanel.Value::.ctor(System.TimeSpan)
extern void Value__ctor_m7A6DC8F60B9850C0756FC362814CF65825737780 ();
// 0x00000091 System.Void mixpanel.Value::.ctor(UnityEngine.Color)
extern void Value__ctor_m92FDC172360742D8B39251F37AD09A7423F6565C ();
// 0x00000092 System.Void mixpanel.Value::.ctor(UnityEngine.Color32)
extern void Value__ctor_mB2D25772373B1C0EC5D62BCD245D661D18BCE2DC ();
// 0x00000093 System.Void mixpanel.Value::.ctor(UnityEngine.Vector2)
extern void Value__ctor_m8035B92B43D8CDDFD029943BBC4649DEA16CD966 ();
// 0x00000094 System.Void mixpanel.Value::.ctor(UnityEngine.Vector3)
extern void Value__ctor_m524420357C43F17D4734F06A1648CA30A718FD48 ();
// 0x00000095 System.Void mixpanel.Value::.ctor(UnityEngine.Vector4)
extern void Value__ctor_mB04DF6C3C8FBABFFB00519D180369945DD58966F ();
// 0x00000096 System.Void mixpanel.Value::.ctor(UnityEngine.Quaternion)
extern void Value__ctor_m710C25F3168A60ECFD21F9229359037709926C5C ();
// 0x00000097 System.Void mixpanel.Value::.ctor(UnityEngine.Bounds)
extern void Value__ctor_mE2E0FA0917F24E4E63C3F5A7F171B7834AC06E9D ();
// 0x00000098 System.Void mixpanel.Value::.ctor(UnityEngine.Rect)
extern void Value__ctor_m24B904799D25E391BC2DE2EAD36191A50B872850 ();
// 0x00000099 System.Void mixpanel.Value::.ctor(System.Collections.Generic.IEnumerable`1<mixpanel.Value>)
extern void Value__ctor_m84BE4F51AAF47B4C51FB5ECB3F7162CAE4EB1935 ();
// 0x0000009A System.Void mixpanel.Value::.ctor(System.Collections.Generic.IDictionary`2<System.String,mixpanel.Value>)
extern void Value__ctor_m67F3A732283F748A2CFB0A986A2AEC079E647461 ();
// 0x0000009B mixpanel.Value mixpanel.Value::get_Null()
extern void Value_get_Null_mF46ED107BE2AA49B1A0743F064B9B19207CE61E2 ();
// 0x0000009C mixpanel.Value mixpanel.Value::get_Array()
extern void Value_get_Array_m89FFB9ADE87A5869770FFEDE25C93C080A2C6942 ();
// 0x0000009D mixpanel.Value mixpanel.Value::get_Object()
extern void Value_get_Object_m45418D4F1FC5E8155A4C90A322214E3FFC8787D8 ();
// 0x0000009E mixpanel.Value mixpanel.Value::op_Implicit(System.String)
extern void Value_op_Implicit_mF31D114087FFEA9984B3E4F81B41EF7DAF306052 ();
// 0x0000009F mixpanel.Value mixpanel.Value::op_Implicit(System.String[])
extern void Value_op_Implicit_mC36615BBFB5705E9FD1FABEBD7BC901FFED6388D ();
// 0x000000A0 mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<System.String>)
extern void Value_op_Implicit_mC3338A0A28FB37536373E99B799AF444C5E630C8 ();
// 0x000000A1 mixpanel.Value mixpanel.Value::op_Implicit(System.Boolean)
extern void Value_op_Implicit_m5AD3076AB84753271E6AB481D71217D5E24B144B ();
// 0x000000A2 mixpanel.Value mixpanel.Value::op_Implicit(System.Boolean[])
extern void Value_op_Implicit_mE1D2263E72179E6CAD1B97498B9E684518C6BECD ();
// 0x000000A3 mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<System.Boolean>)
extern void Value_op_Implicit_mBB990DB9F838B8E661E4444A2F22597E36F30FAD ();
// 0x000000A4 mixpanel.Value mixpanel.Value::op_Implicit(System.Single)
extern void Value_op_Implicit_m2C1DFE34B581578142514BD4BF76F73485E99CDC ();
// 0x000000A5 mixpanel.Value mixpanel.Value::op_Implicit(System.Single[])
extern void Value_op_Implicit_m15A6B5151B5B5400254C3BA24270724E77234433 ();
// 0x000000A6 mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<System.Single>)
extern void Value_op_Implicit_m60203BCF514733968391DE97E02EF96F263BFEC5 ();
// 0x000000A7 mixpanel.Value mixpanel.Value::op_Implicit(System.Double)
extern void Value_op_Implicit_m1F41165645BCAF025DC205F976FEE5B8C29FCD1C ();
// 0x000000A8 mixpanel.Value mixpanel.Value::op_Implicit(System.Double[])
extern void Value_op_Implicit_m500596540F61667764870FD1C1A45DB66412AD56 ();
// 0x000000A9 mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<System.Double>)
extern void Value_op_Implicit_mEDCC0D15532FA9EB9F27D86E316FDE90F73A4CBB ();
// 0x000000AA mixpanel.Value mixpanel.Value::op_Implicit(System.Decimal)
extern void Value_op_Implicit_mAB2F3A24E169F44F396FADA907493500E24CA50F ();
// 0x000000AB mixpanel.Value mixpanel.Value::op_Implicit(System.Decimal[])
extern void Value_op_Implicit_mC5AF8C19803551EC2E4F285595BFE353A3FBEFD2 ();
// 0x000000AC mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<System.Decimal>)
extern void Value_op_Implicit_mB75C0D2B20C740AF5CC4253DC81C20FF2B9E51D1 ();
// 0x000000AD mixpanel.Value mixpanel.Value::op_Implicit(System.Int16)
extern void Value_op_Implicit_m4B97073B243AE71FD6DF83BF27036258928AE6D7 ();
// 0x000000AE mixpanel.Value mixpanel.Value::op_Implicit(System.Int16[])
extern void Value_op_Implicit_m1CB3F9085376E51A77EDB6E6AFC659EE5CCB3CF2 ();
// 0x000000AF mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<System.Int16>)
extern void Value_op_Implicit_m57419BB271E8FE81508726A3737D82C23841DD63 ();
// 0x000000B0 mixpanel.Value mixpanel.Value::op_Implicit(System.Int32)
extern void Value_op_Implicit_m3802980A56C0BD2ED6D726EB29F5921F06986391 ();
// 0x000000B1 mixpanel.Value mixpanel.Value::op_Implicit(System.Int32[])
extern void Value_op_Implicit_mCE894BE10E134B530B4DE34213897FDCBF9B0CEE ();
// 0x000000B2 mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<System.Int32>)
extern void Value_op_Implicit_mA5D8B9420F76EC1D5E1E978E8D9F5A5C0A8CE94E ();
// 0x000000B3 mixpanel.Value mixpanel.Value::op_Implicit(System.Int64)
extern void Value_op_Implicit_mB4E0B2C5A47784FCF0DF5DDFE424960836E9C82A ();
// 0x000000B4 mixpanel.Value mixpanel.Value::op_Implicit(System.Int64[])
extern void Value_op_Implicit_m9F3CE77C07BDA75A13ADD824206BEC78E6F7ABD1 ();
// 0x000000B5 mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<System.Int64>)
extern void Value_op_Implicit_m07ABA94F7789F6813263C4B955D16B7A5556FF78 ();
// 0x000000B6 mixpanel.Value mixpanel.Value::op_Implicit(System.UInt16)
extern void Value_op_Implicit_m164095B3EE2E31F20EE3B16C20344714218A1276 ();
// 0x000000B7 mixpanel.Value mixpanel.Value::op_Implicit(System.UInt16[])
extern void Value_op_Implicit_m9F121BFBD5325B07AEE385611494F7B8D4343BDC ();
// 0x000000B8 mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<System.UInt16>)
extern void Value_op_Implicit_m7D8A6AB2CA7051DD6DC90A2650787A16C00766CB ();
// 0x000000B9 mixpanel.Value mixpanel.Value::op_Implicit(System.UInt32)
extern void Value_op_Implicit_m7E8E0E1BCA62A93717F9D992A142E1AB80E58BD0 ();
// 0x000000BA mixpanel.Value mixpanel.Value::op_Implicit(System.UInt32[])
extern void Value_op_Implicit_m37359F037D1E2B13ED70E7CD9CB28CD508AA3151 ();
// 0x000000BB mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<System.UInt32>)
extern void Value_op_Implicit_m0C56B32902BCFED9BA7D9BA6701378BC081275F9 ();
// 0x000000BC mixpanel.Value mixpanel.Value::op_Implicit(System.UInt64)
extern void Value_op_Implicit_mAD45F3E966FD66DF09516BF59AB0754ADCB0ADBB ();
// 0x000000BD mixpanel.Value mixpanel.Value::op_Implicit(System.UInt64[])
extern void Value_op_Implicit_m5DC43F4B91FC10B6EBD03D68AABE04183609679A ();
// 0x000000BE mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<System.UInt64>)
extern void Value_op_Implicit_m09144AC55FD98C234CCFE0E4D6E101CB2869A9EC ();
// 0x000000BF mixpanel.Value mixpanel.Value::op_Implicit(System.SByte)
extern void Value_op_Implicit_m0A3622830C0EC5AA258A1C4565613C565AA586E3 ();
// 0x000000C0 mixpanel.Value mixpanel.Value::op_Implicit(System.SByte[])
extern void Value_op_Implicit_m645B338E6F9BC710C7A49866E2A9087FC276950F ();
// 0x000000C1 mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<System.SByte>)
extern void Value_op_Implicit_mCC170AED72159B9579CA51D9DF165466743BCA08 ();
// 0x000000C2 mixpanel.Value mixpanel.Value::op_Implicit(System.Byte)
extern void Value_op_Implicit_m999589CDFAB7B4A6541EEE79EECD7138A0072029 ();
// 0x000000C3 mixpanel.Value mixpanel.Value::op_Implicit(System.Byte[])
extern void Value_op_Implicit_m1C36D133814068999C2D01D3790EE59A9C14E538 ();
// 0x000000C4 mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<System.Byte>)
extern void Value_op_Implicit_mDD500F10DB50B708D35214CACA0BC7E1692074CB ();
// 0x000000C5 mixpanel.Value mixpanel.Value::op_Implicit(System.Uri)
extern void Value_op_Implicit_mB607D9FBA46EE1B3069EC288EB5721AEE4C067DD ();
// 0x000000C6 mixpanel.Value mixpanel.Value::op_Implicit(System.Uri[])
extern void Value_op_Implicit_mA6F91BA9BE2A60EC86CEFBEF3EB63C95994B07AA ();
// 0x000000C7 mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<System.Uri>)
extern void Value_op_Implicit_m55594EE9A7BE753B7DC73672B819649044FB6F4F ();
// 0x000000C8 mixpanel.Value mixpanel.Value::op_Implicit(System.Guid)
extern void Value_op_Implicit_m825DE729C7F7F8DA9B93F447733A1D2D48CFE7BF ();
// 0x000000C9 mixpanel.Value mixpanel.Value::op_Implicit(System.Guid[])
extern void Value_op_Implicit_mE1BAEE2C39C980AA8507CF1A89197B4E2CD974FB ();
// 0x000000CA mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<System.Guid>)
extern void Value_op_Implicit_m27235A399FA3164CB3A563B72AC20836ECCF7007 ();
// 0x000000CB mixpanel.Value mixpanel.Value::op_Implicit(System.DateTime)
extern void Value_op_Implicit_mF5B95080211B77032C82EEB2BD460412F5A03576 ();
// 0x000000CC mixpanel.Value mixpanel.Value::op_Implicit(System.DateTime[])
extern void Value_op_Implicit_mF971C2DA44A482AA27F0AC76FA70F7314321760D ();
// 0x000000CD mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<System.DateTime>)
extern void Value_op_Implicit_m2057146B7F67134A49A9F9254CA6DBC4B3D0D77D ();
// 0x000000CE mixpanel.Value mixpanel.Value::op_Implicit(System.DateTimeOffset)
extern void Value_op_Implicit_mF5394F113C7405814E487C0E2F8CDB29033C6DD3 ();
// 0x000000CF mixpanel.Value mixpanel.Value::op_Implicit(System.DateTimeOffset[])
extern void Value_op_Implicit_mA1C08EA68CC68D18DED454F98FCE64C13A5E663C ();
// 0x000000D0 mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<System.DateTimeOffset>)
extern void Value_op_Implicit_m77D256AD495C80A2C6780CF359A4DF56A95DAB89 ();
// 0x000000D1 mixpanel.Value mixpanel.Value::op_Implicit(System.TimeSpan)
extern void Value_op_Implicit_m755C36308234B9C3EF21999579F6ACF8BD43896C ();
// 0x000000D2 mixpanel.Value mixpanel.Value::op_Implicit(System.TimeSpan[])
extern void Value_op_Implicit_m0B8BB00FB2A1C7239EB1A7D09B8D5372768F196A ();
// 0x000000D3 mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<System.TimeSpan>)
extern void Value_op_Implicit_mF019C0BFEAA158BD745EB22BCE5AF8AA3CDF1877 ();
// 0x000000D4 mixpanel.Value mixpanel.Value::op_Implicit(UnityEngine.Color)
extern void Value_op_Implicit_m96E458DEE6E7CD40905FB1DB0A250BCF97535891 ();
// 0x000000D5 mixpanel.Value mixpanel.Value::op_Implicit(UnityEngine.Color[])
extern void Value_op_Implicit_m4CD33F1A9CC6F8D08E4911E4AD35941A83FFA418 ();
// 0x000000D6 mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<UnityEngine.Color>)
extern void Value_op_Implicit_m2F7FA017CCF00209BBA2724929901C06A4AC9C17 ();
// 0x000000D7 mixpanel.Value mixpanel.Value::op_Implicit(UnityEngine.Color32)
extern void Value_op_Implicit_m1401AC9FAC6BC4E236A7D4FB4FE98E16011B28D8 ();
// 0x000000D8 mixpanel.Value mixpanel.Value::op_Implicit(UnityEngine.Color32[])
extern void Value_op_Implicit_m4EAF78AC3038759BE205219282EEDE782E6D59E5 ();
// 0x000000D9 mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<UnityEngine.Color32>)
extern void Value_op_Implicit_mA98FA73B3C272EF3ED461EFF494D01270696FF1C ();
// 0x000000DA mixpanel.Value mixpanel.Value::op_Implicit(UnityEngine.Vector2)
extern void Value_op_Implicit_mFAE4578B48CB234AEB86F1959F8A53C4AF79FB4D ();
// 0x000000DB mixpanel.Value mixpanel.Value::op_Implicit(UnityEngine.Vector2[])
extern void Value_op_Implicit_mE9272C3FE5DA9A28BC5942D547441A81F5666F39 ();
// 0x000000DC mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern void Value_op_Implicit_m1824D49333B9E1AE2D5610D54360462921A7AA8F ();
// 0x000000DD mixpanel.Value mixpanel.Value::op_Implicit(UnityEngine.Vector3)
extern void Value_op_Implicit_mD85D2C91A07F485C89C4A19DA1B8F72FB87D21F7 ();
// 0x000000DE mixpanel.Value mixpanel.Value::op_Implicit(UnityEngine.Vector3[])
extern void Value_op_Implicit_m66B675FCD738AD25A830C5F756463BEC67FA4425 ();
// 0x000000DF mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void Value_op_Implicit_m20B8CD55AF53826137A7ECC11B4373AFBE551504 ();
// 0x000000E0 mixpanel.Value mixpanel.Value::op_Implicit(UnityEngine.Vector4)
extern void Value_op_Implicit_mF7A693861D556426171CD43686678DF6272ECAA2 ();
// 0x000000E1 mixpanel.Value mixpanel.Value::op_Implicit(UnityEngine.Vector4[])
extern void Value_op_Implicit_m91CD8770B677563B143D9158491D88DBD80A6C1A ();
// 0x000000E2 mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Value_op_Implicit_mA06418B3AA6C5BCD211B3AD2810F1D0D44E1675A ();
// 0x000000E3 mixpanel.Value mixpanel.Value::op_Implicit(UnityEngine.Quaternion)
extern void Value_op_Implicit_mC6A8C7F1DF45680D6F4E8C7EF05F10B44D771461 ();
// 0x000000E4 mixpanel.Value mixpanel.Value::op_Implicit(UnityEngine.Quaternion[])
extern void Value_op_Implicit_mB3EFAB828E4F255DC8459B91F75EFA20202935A5 ();
// 0x000000E5 mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<UnityEngine.Quaternion>)
extern void Value_op_Implicit_m6E22DCB9CC34FCCE25FF2CB8E1F9A6B3329173C6 ();
// 0x000000E6 mixpanel.Value mixpanel.Value::op_Implicit(UnityEngine.Bounds)
extern void Value_op_Implicit_mF6FBC19476821AE380A523B680CCA6732442FBF9 ();
// 0x000000E7 mixpanel.Value mixpanel.Value::op_Implicit(UnityEngine.Bounds[])
extern void Value_op_Implicit_m092165AF8E2CE854FF6E4A12E9714068C0F6C37F ();
// 0x000000E8 mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<UnityEngine.Bounds>)
extern void Value_op_Implicit_mFC8DB3733BA89C63C1B677B2F75E6C98D3DC4329 ();
// 0x000000E9 mixpanel.Value mixpanel.Value::op_Implicit(UnityEngine.Rect)
extern void Value_op_Implicit_m9BF54D74D9925EB7E79B5F2B05EE378A44E2C23A ();
// 0x000000EA mixpanel.Value mixpanel.Value::op_Implicit(UnityEngine.Rect[])
extern void Value_op_Implicit_m8F05842558E73AC5BA689F7045DE5C90CA5911F9 ();
// 0x000000EB mixpanel.Value mixpanel.Value::op_Implicit(System.Collections.Generic.List`1<UnityEngine.Rect>)
extern void Value_op_Implicit_mC47F2134D2EA16291330B3ED20CFA03F18E3859B ();
// 0x000000EC System.String mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m8F013BB1F42A611CE3511420D7292FC64AD2B85E ();
// 0x000000ED System.String[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mAD16FCE5EDC02BCDACDFB462EFB27BE3691DA1E3 ();
// 0x000000EE System.Collections.Generic.List`1<System.String> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m8CB0C3BA53E4D0C81342649FEFF1A32C49D0DF64 ();
// 0x000000EF System.Boolean mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m5970A975647BCEF6C77519B340C70633A190C3AE ();
// 0x000000F0 System.Boolean[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m36560DDF64ACE9CDA43D6838BE097A79AEF73D3C ();
// 0x000000F1 System.Collections.Generic.List`1<System.Boolean> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m19732E2EE16FCE7847A721CE80134B219CD301FF ();
// 0x000000F2 System.Single mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m3080B7739584D390A4901EE23C849A6ADD89FEE6 ();
// 0x000000F3 System.Single[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mD0E11F258D2CBFE9DD3E6F0FFBBF7DC6D6CAEC2C ();
// 0x000000F4 System.Collections.Generic.List`1<System.Single> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mCEF67C742E8EF23995301E1EC7448C8F4CD75024 ();
// 0x000000F5 System.Double mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m402ED57BEBA4F2CFED3FF24A0558EE178357DF7B ();
// 0x000000F6 System.Double[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m0F1BC675AF23A423342B627089B83C7BAAACBB19 ();
// 0x000000F7 System.Collections.Generic.List`1<System.Double> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m8E5C690DBABFA9B1B58264CD19CF0573488322F3 ();
// 0x000000F8 System.Decimal mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m63BDFC16EEC06DE8957EF1B90D69F6099D4BD8A2 ();
// 0x000000F9 System.Decimal[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m8FB0800225FA14FB0693DD6BE4E841651F8D33AB ();
// 0x000000FA System.Collections.Generic.List`1<System.Decimal> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mBF86FE88A7C3B4F88C4A3A7E5C55217B052581C1 ();
// 0x000000FB System.Int16 mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m4DF02859E66F2ABB41F5D8ABE37120D5DA4B7F02 ();
// 0x000000FC System.Int16[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m91AA3E6A4C67649E145D320B9AC6FFC52993CD2F ();
// 0x000000FD System.Collections.Generic.List`1<System.Int16> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mBB5D6A2F31EDF2A9C81E93312EA7CA037DD66357 ();
// 0x000000FE System.Int32 mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m9CAE4DB0AFAD3F44543399FE4B4A7638C24B84B7 ();
// 0x000000FF System.Int32[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mB3A668F49EF514028C442271AB4A5E68BE07BA22 ();
// 0x00000100 System.Collections.Generic.List`1<System.Int32> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m5EEB6A80088E3609870A1C57F6B54784FC6A0865 ();
// 0x00000101 System.Int64 mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mC2766FEF14781C339DB6B3CEF6684263BEDDBBFB ();
// 0x00000102 System.Int64[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mB6F5912AFB1D6BC46DEDC8962CEC6A8338CFCCD5 ();
// 0x00000103 System.Collections.Generic.List`1<System.Int64> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mE6277D75A4720AC5222F8E335AA62EC974F0222B ();
// 0x00000104 System.UInt16 mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m9014BB7C7A00F90911ADBEDE339982EF26A3B2C3 ();
// 0x00000105 System.UInt16[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mE07976CD966D6E3FA6292A058BA7BB2EB6C1281D ();
// 0x00000106 System.Collections.Generic.List`1<System.UInt16> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m7F19F5EFFB82F04576C0AE8C606B730B62AA6840 ();
// 0x00000107 System.UInt32 mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mF425A79429129C5399C4E77DD757200DF9121583 ();
// 0x00000108 System.UInt32[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m3378646FDF49824B7562587DD38A90E13A8A9944 ();
// 0x00000109 System.Collections.Generic.List`1<System.UInt32> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mAA3CB03AFA9B5C858A90DCC9F06F9926C8AE40E6 ();
// 0x0000010A System.UInt64 mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m353D1997B1A755D001115C4B1C8E4E72B6C44F27 ();
// 0x0000010B System.UInt64[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mF3612DCD6520ECB3AD6A94CFA60569A2084D40F7 ();
// 0x0000010C System.Collections.Generic.List`1<System.UInt64> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m0D507544FE170C6862BAE0CC2785393D905F5135 ();
// 0x0000010D System.SByte mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mAD3C0172A27D3488CFD60657B0BA2E9FAA8F6671 ();
// 0x0000010E System.SByte[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m10D3F43A30A53DAF253AA74B399262EC1522E96B ();
// 0x0000010F System.Collections.Generic.List`1<System.SByte> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m80BC116C307D7D72AFEC5D5D9BC9FB9F78CAFD4C ();
// 0x00000110 System.Byte mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m2890A8B7916D7B5766B93F612802EC4F7119081F ();
// 0x00000111 System.Byte[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mD87D6C638F10DB34EE139CC682FCBE14FC4611A6 ();
// 0x00000112 System.Collections.Generic.List`1<System.Byte> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m62B4A222C6F2E3478C0F9F69C248F3CCBACEE831 ();
// 0x00000113 System.Uri mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mDABBF5AE2793D6CC2E55ED98A2A7A640146CD998 ();
// 0x00000114 System.Uri[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m4208C9C8782672660B770332E5E5DCE4FD096DB1 ();
// 0x00000115 System.Collections.Generic.List`1<System.Uri> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m146E27F894240B3E19A1CEB7B3A5CEB8B6EC0EBC ();
// 0x00000116 System.Guid mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mE4D993BB1A5388AB2F6EC8F0785D3CC0D68F7E90 ();
// 0x00000117 System.Guid[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m897A015CC2952802D30F60ACA2C052A71D66FEB5 ();
// 0x00000118 System.Collections.Generic.List`1<System.Guid> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mA2AF915DBBCACA6641572D92DC1EAC68EEF8A106 ();
// 0x00000119 System.DateTime mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mC167DF65BAE52BF1B8E8BE86F6C422F2CAD28456 ();
// 0x0000011A System.DateTime[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mC2A2B6973801511728D594151EC720995DFE16CB ();
// 0x0000011B System.Collections.Generic.List`1<System.DateTime> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m2AE974254209174388660D932EE20628C444A0DF ();
// 0x0000011C System.DateTimeOffset mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m47EBBBBC35C53386175532B5794798677C669831 ();
// 0x0000011D System.DateTimeOffset[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m734B77D7A66FF6DC6C3A0E379E7F3F46A54962EA ();
// 0x0000011E System.Collections.Generic.List`1<System.DateTimeOffset> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m3216E13274E7669CEF373F8CAA79AE426BCA2912 ();
// 0x0000011F System.TimeSpan mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m7F965A4D5EE5E031274EACC145292F9B5C76ABF6 ();
// 0x00000120 System.TimeSpan[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m15EAC904AD9C74D73B1982119466C22A64DB6389 ();
// 0x00000121 System.Collections.Generic.List`1<System.TimeSpan> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mAF67E326BAE9128B401706B0527D4C350A21972E ();
// 0x00000122 UnityEngine.Color mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m83DF5FE88B0F4A5FC883184A59EF40708C739BF1 ();
// 0x00000123 UnityEngine.Color[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m71C8A1CD020141FACD06DE105FC733E7C59014A1 ();
// 0x00000124 System.Collections.Generic.List`1<UnityEngine.Color> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mEB841B56E501620CF674CA356A394B80CC442D63 ();
// 0x00000125 UnityEngine.Color32 mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m2F2C5A73F63EDB81E47F08C2E88705460D1E56F7 ();
// 0x00000126 UnityEngine.Color32[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m2297690D3D408FC5BEB3D9CADCA9113CE766D30F ();
// 0x00000127 System.Collections.Generic.List`1<UnityEngine.Color32> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m0303E56CE57BEB39A4A094553483C1081DBC5F3E ();
// 0x00000128 UnityEngine.Vector2 mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mEE989FB011F7D4B5915E510CF9CCB98A42786BDA ();
// 0x00000129 UnityEngine.Vector2[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mA936FDECA9FCD0CFF83B2319D48DABE036AA130A ();
// 0x0000012A System.Collections.Generic.List`1<UnityEngine.Vector2> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m2EB66EE36792654E8A52D21FC4DC03A9F82D97AC ();
// 0x0000012B UnityEngine.Vector3 mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m039FDDC6CE46E2E3655ACFB0364C8E6489DBD1DD ();
// 0x0000012C UnityEngine.Vector3[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m607F0148734DA7C936A78648D8E2673C0018021E ();
// 0x0000012D System.Collections.Generic.List`1<UnityEngine.Vector3> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mE534A948BF1B4CF9EAA7004B43F5F2AA19F345A8 ();
// 0x0000012E UnityEngine.Vector4 mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m6DE8B85544FB43557B1AD2EF388A9412E93000A4 ();
// 0x0000012F UnityEngine.Vector4[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m8B340DF650BF6FC76908ECDB3BA7D2A1A317D68B ();
// 0x00000130 System.Collections.Generic.List`1<UnityEngine.Vector4> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m5DA88A6250E7FE3745C801DA6760FABFFC6A8A5E ();
// 0x00000131 UnityEngine.Quaternion mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m6DA1504B642C562A60B7947122154C235BE458E3 ();
// 0x00000132 UnityEngine.Quaternion[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mB3DD4665F4608A79469492F5A0EEA8CB44A853C8 ();
// 0x00000133 System.Collections.Generic.List`1<UnityEngine.Quaternion> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mF45671FAA436DEDD16DA6A3B9D973C61E475D675 ();
// 0x00000134 UnityEngine.Bounds mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m22D766607BABAB99BB9E4AFD63E5DB779F92358C ();
// 0x00000135 UnityEngine.Bounds[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mCAA09D5FA67067CA4E130F5B0CD78CC83773788B ();
// 0x00000136 System.Collections.Generic.List`1<UnityEngine.Bounds> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mE0A3F40229CA2319DB79C25DF62FF18DC9F67739 ();
// 0x00000137 UnityEngine.Rect mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mACD08E678B9053C4F5DF133C78BC4944F57F31EF ();
// 0x00000138 UnityEngine.Rect[] mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_m566D7A652287277874B5AAC044408F9366DD944C ();
// 0x00000139 System.Collections.Generic.List`1<UnityEngine.Rect> mixpanel.Value::op_Implicit(mixpanel.Value)
extern void Value_op_Implicit_mD049B1A7923EAE81F19210C3BF164BFAA385A003 ();
// 0x0000013A System.Void mixpanel.Value::Write(System.IO.StringWriter,System.Boolean)
extern void Value_Write_mBCC75A65036ABDA646D5CB156978E7DDFA69C528 ();
// 0x0000013B System.String mixpanel.Value::SanitizeStringForJson(System.String)
extern void Value_SanitizeStringForJson_mCBA1EA599CB3C1859E22106490FCCE94C8200907 ();
// 0x0000013C System.Void mixpanel.Value::OnBeforeSerialize()
extern void Value_OnBeforeSerialize_m3F7C3C64EBFD1E47E33BD831A65B3405729666C6 ();
// 0x0000013D System.Void mixpanel.Value::SerializeList()
extern void Value_SerializeList_m265A062C7CD876E0912BBCE100D79B2E66FCCB43 ();
// 0x0000013E System.Void mixpanel.Value::SerializeDictionary()
extern void Value_SerializeDictionary_mBE394B113B0108A526688750765782FF40829145 ();
// 0x0000013F System.Void mixpanel.Value::OnAfterDeserialize()
extern void Value_OnAfterDeserialize_mE7AFB9A5DE38A8AEE474943FB71C1C866F0AFA63 ();
// 0x00000140 System.Void mixpanel.Value::DeserializeList()
extern void Value_DeserializeList_m57594A41CFF7AD58E40B27DE9509756EB0BF041B ();
// 0x00000141 System.Void mixpanel.Value::DeserializeDictionary()
extern void Value_DeserializeDictionary_m102D0E96A9E43AB4DE1B8A121D411FCA021506B1 ();
// 0x00000142 mixpanel.Value mixpanel.Value::Deserialize(System.String)
extern void Value_Deserialize_mCE8FD1E2B40D6BF3032B964CB59F5BEC527BD6A8 ();
// 0x00000143 mixpanel.Value mixpanel.Value::ParseValue(System.IO.StringReader)
extern void Value_ParseValue_m8C695E110BB4D26D6FC0B1FBBB6D666934084ABD ();
// 0x00000144 mixpanel.Value mixpanel.Value::ParseByToken(System.IO.StringReader,mixpanel.Value_Token)
extern void Value_ParseByToken_m5D515F2F9CCB525153CD1869EB9FDB76D6186874 ();
// 0x00000145 System.String mixpanel.Value::ParseString(System.IO.StringReader)
extern void Value_ParseString_m8D2F74CDA6BED414D2564C8DD92B08F70CD76AC7 ();
// 0x00000146 System.Double mixpanel.Value::ParseNumber(System.IO.StringReader)
extern void Value_ParseNumber_m174415820524FC143F7B72AE72FA297BAA55349C ();
// 0x00000147 mixpanel.Value mixpanel.Value::ParseArray(System.IO.StringReader)
extern void Value_ParseArray_mECD8BCF38353CE21CA2749B475E22DF1A5DF6E8A ();
// 0x00000148 mixpanel.Value mixpanel.Value::ParseObject(System.IO.StringReader)
extern void Value_ParseObject_m8D14F6B3BAB7D415594950D5503C2B1607AA2A2D ();
// 0x00000149 System.Char mixpanel.Value::PeekChar(System.IO.StringReader)
extern void Value_PeekChar_mB289307320C54B04983483A1657F9FBB79944EB6 ();
// 0x0000014A System.Char mixpanel.Value::NextChar(System.IO.StringReader)
extern void Value_NextChar_m25C313EE3B7A1049BC5076D325B931A1410E041A ();
// 0x0000014B System.String mixpanel.Value::NextWord(System.IO.StringReader)
extern void Value_NextWord_m01F9CA184C22B9F6A51A5FE97E943A171FEAE97A ();
// 0x0000014C System.Void mixpanel.Value::EatWhitespace(System.IO.StringReader)
extern void Value_EatWhitespace_m0B765254AA7E3D34885838EB4A0E9C091FF8C440 ();
// 0x0000014D mixpanel.Value_Token mixpanel.Value::NextToken(System.IO.StringReader)
extern void Value_NextToken_mB0D2352BCC7301A6612E75861FB34F4A19634484 ();
// 0x0000014E mixpanel.Value mixpanel.Value::FromSerialization(mixpanel.Value,mixpanel.Value,mixpanel.Value)
extern void Value_FromSerialization_mE45BEF7F6768DD876B595FC5F1A3DD2C6408FFD2 ();
// 0x0000014F System.Void mixpanel.Worker::StartWorkerThread()
extern void Worker_StartWorkerThread_m052315380D9457E4CB6A072D5A81527E41EF9193 ();
// 0x00000150 System.Void mixpanel.Worker::StopWorkerThread()
extern void Worker_StopWorkerThread_mC9359090030160BA51ECD293EF900DE129DC9EE5 ();
// 0x00000151 System.Void mixpanel.Worker::ForceStop()
extern void Worker_ForceStop_m15394C3EA8EC0FE4BE5AE5604E21FB6F5EFC8E53 ();
// 0x00000152 System.Void mixpanel.Worker::EnqueueEventOp(mixpanel.Value)
extern void Worker_EnqueueEventOp_m8ADAA34953A637B85E546C5E4F9E4C57126F9A66 ();
// 0x00000153 System.Void mixpanel.Worker::EnqueuePeopleOp(mixpanel.Value)
extern void Worker_EnqueuePeopleOp_mBB9D9D68BDCB35AF03450FBE3FC6599066601A00 ();
// 0x00000154 System.Void mixpanel.Worker::FlushOp()
extern void Worker_FlushOp_m6DC1B6E9392F26CFEF035E800078E3BB5B80F67B ();
// 0x00000155 System.Void mixpanel.Worker::ForceFlushOp()
extern void Worker_ForceFlushOp_m988A7B6CB594EC12013D245A9BC8BA02B5981616 ();
// 0x00000156 System.Void mixpanel.Worker::ClearOp()
extern void Worker_ClearOp_m72B0FC05614D0F94911C22A0FF49D8E8B2B8619C ();
// 0x00000157 System.Void mixpanel.Worker::RunBackgroundThread()
extern void Worker_RunBackgroundThread_m243E2997EE75170521BDBE9EC8E6CC8A206020E1 ();
// 0x00000158 System.Void mixpanel.Worker::DispatchOperations()
extern void Worker_DispatchOperations_m5B4430C10686EFB7939BC1B69F30C2E2008C3138 ();
// 0x00000159 System.Collections.IEnumerator mixpanel.Worker::SendData(mixpanel.queue.PersistentQueue,System.String)
extern void Worker_SendData_mECF28CC21B3BD79A4CFAF01BA8318796D5FB6070 ();
// 0x0000015A System.Void mixpanel.Worker::EnqueueMixpanelQueue(mixpanel.queue.PersistentQueue,mixpanel.Value)
extern void Worker_EnqueueMixpanelQueue_m09AC5B423B62F3F47054F3E451F59390A7575A87 ();
// 0x0000015B System.Void mixpanel.Worker::.cctor()
extern void Worker__cctor_m73EA7CA1DB905DD2E10B662FA4CEA68CA63A0774 ();
// 0x0000015C System.Double mixpanel.Util::CurrentTime()
extern void Util_CurrentTime_mE9CEBB1787F3D59C92E20B07E72829B63DC4CB22 ();
// 0x0000015D System.String mixpanel.Util::CurrentDateTime()
extern void Util_CurrentDateTime_m7C9A38080569FB6D0599A0D1E4C921140D64CA6D ();
// 0x0000015E System.String mixpanel.Util::GetRadio()
extern void Util_GetRadio_m2DBAA8F0ACFCCC47E25AA1E6DB3D0779B7F7B12C ();
// 0x0000015F System.String mixpanel.platforms.Android::GetBrand()
extern void Android_GetBrand_m7B271AC1EE09BEBF2DF0B148CE43A3FF9964A0B5 ();
// 0x00000160 System.String mixpanel.platforms.Android::GetManufacturer()
extern void Android_GetManufacturer_m20B9F3E02D6625121B8E096993E1E67655AAA1FD ();
// 0x00000161 System.Int32 mixpanel.platforms.Android::GetVersionCode()
extern void Android_GetVersionCode_mEE745C7A5246002A93E0E70C52D4D66EB73CE3B3 ();
// 0x00000162 System.Void mixpanel.platforms.Android::.ctor()
extern void Android__ctor_mC4AC8FBB13694F2CDF0536CAA7D1351ED50A7EBF ();
// 0x00000163 T mixpanel.queue.Extensions::GetValueOrDefault(System.Collections.Generic.IDictionary`2<TK,T>,TK)
// 0x00000164 System.Void mixpanel.queue.PersistentQueue::.ctor(System.String,System.Int32)
extern void PersistentQueue__ctor_m60C9CDF686347FBD1FD18E5252DE9292F336730E ();
// 0x00000165 System.Void mixpanel.queue.PersistentQueue::Finalize()
extern void PersistentQueue_Finalize_m895F8636F6D1BA9706B6C8996B01A945C254001D ();
// 0x00000166 System.Void mixpanel.queue.PersistentQueue::UnlockQueue()
extern void PersistentQueue_UnlockQueue_m6F6CDE9BEB4ABD2B2F2BC2700E15611A3179C415 ();
// 0x00000167 System.Void mixpanel.queue.PersistentQueue::LockQueue()
extern void PersistentQueue_LockQueue_mCF647CA9CF247616B784A2FEAE7D72D640C78361 ();
// 0x00000168 System.Int32 mixpanel.queue.PersistentQueue::get_CurrentCountOfItemsInQueue()
extern void PersistentQueue_get_CurrentCountOfItemsInQueue_mDA69159F9B8BE2480FBD2DE08BD953A1625532EB ();
// 0x00000169 System.Int64 mixpanel.queue.PersistentQueue::get_CurrentFilePosition()
extern void PersistentQueue_get_CurrentFilePosition_m7FE5FB4E210F28BFC530D75FA9BE5387F25DBFE5 ();
// 0x0000016A System.Void mixpanel.queue.PersistentQueue::set_CurrentFilePosition(System.Int64)
extern void PersistentQueue_set_CurrentFilePosition_mA29BF2DF75C176148670AECBA751DA43617660E1 ();
// 0x0000016B System.String mixpanel.queue.PersistentQueue::get_TransactionLog()
extern void PersistentQueue_get_TransactionLog_m05D66210201E0EF284900AD05F7DD6C9A1433BE9 ();
// 0x0000016C System.String mixpanel.queue.PersistentQueue::get_Meta()
extern void PersistentQueue_get_Meta_mE0C0F40FEA6AB88118B84A56891708D317EE293E ();
// 0x0000016D System.Int32 mixpanel.queue.PersistentQueue::get_CurrentFileNumber()
extern void PersistentQueue_get_CurrentFileNumber_mEA54919870FD34A7E69FCD761993FB1C592B3986 ();
// 0x0000016E System.Void mixpanel.queue.PersistentQueue::set_CurrentFileNumber(System.Int32)
extern void PersistentQueue_set_CurrentFileNumber_m0CB27894872E71361781DFC76A55B2D64F645304 ();
// 0x0000016F System.Void mixpanel.queue.PersistentQueue::Dispose()
extern void PersistentQueue_Dispose_m9867F74DE847563FB6A3500FAA873266B391B29B ();
// 0x00000170 System.Void mixpanel.queue.PersistentQueue::AcquireWriter(System.IO.Stream,System.Func`2<System.IO.Stream,System.Int64>,System.Action`1<System.IO.Stream>)
extern void PersistentQueue_AcquireWriter_mC946B341A323EFB01E23822AF9FC82CA1DFE7BFA ();
// 0x00000171 System.Void mixpanel.queue.PersistentQueue::CommitTransaction(System.Collections.Generic.ICollection`1<mixpanel.queue.PersistentQueueOperation>)
extern void PersistentQueue_CommitTransaction_m6F9DB4D286D328A89EFC0687F9EC6BEFF4105F4E ();
// 0x00000172 System.IO.FileStream mixpanel.queue.PersistentQueue::WaitForTransactionLog(System.Collections.Generic.IReadOnlyCollection`1<System.Byte>)
extern void PersistentQueue_WaitForTransactionLog_m6AE140BB38431F27E5731F9C2EDF2C2E35164D0E ();
// 0x00000173 mixpanel.queue.PersistentQueueEntry mixpanel.queue.PersistentQueue::Dequeue()
extern void PersistentQueue_Dequeue_m71899BBB07161B055C5337E38FB9084AF015413A ();
// 0x00000174 System.Void mixpanel.queue.PersistentQueue::ReadAhead()
extern void PersistentQueue_ReadAhead_m03A8D597DEE101AC6ED5A475A1AA2DE5DCFDD80D ();
// 0x00000175 System.Byte[] mixpanel.queue.PersistentQueue::ReadEntriesFromFile(mixpanel.queue.PersistentQueueEntry,System.Int64)
extern void PersistentQueue_ReadEntriesFromFile_m8D710E3132A58F2DD0F98EAEF6D6BAF9F59781AE ();
// 0x00000176 mixpanel.queue.PersistentQueueSession mixpanel.queue.PersistentQueue::OpenSession()
extern void PersistentQueue_OpenSession_mFC3056E0F0A577554DD1C616737DF178509122F5 ();
// 0x00000177 System.Void mixpanel.queue.PersistentQueue::Reinstate(System.Collections.Generic.IEnumerable`1<mixpanel.queue.PersistentQueueOperation>)
extern void PersistentQueue_Reinstate_mD630C130E071829D27A91175CC9A36C74CC5DEC7 ();
// 0x00000178 System.Void mixpanel.queue.PersistentQueue::ReadTransactionLog()
extern void PersistentQueue_ReadTransactionLog_m8128DB718F94FB049C9AC28B42B38AF27F128D3D ();
// 0x00000179 System.Void mixpanel.queue.PersistentQueue::FlushTrimmedTransactionLog()
extern void PersistentQueue_FlushTrimmedTransactionLog_mC3FC11484117C9CA3D99D8FDE4DCF7566FD0E840 ();
// 0x0000017A System.Void mixpanel.queue.PersistentQueue::WriteEntryToTransactionLog(System.IO.Stream,mixpanel.queue.PersistentQueueEntry,mixpanel.queue.PersistentQueueOperationTypes)
extern void PersistentQueue_WriteEntryToTransactionLog_m9024865AF44B89963661575093909D8B9CA819BC ();
// 0x0000017B System.Void mixpanel.queue.PersistentQueue::AssertOperationSeparator(System.IO.BinaryReader)
extern void PersistentQueue_AssertOperationSeparator_m346D83803A2A4B1E31B79BFF49B23B9D4298131E ();
// 0x0000017C System.Collections.Generic.IEnumerable`1<System.Int32> mixpanel.queue.PersistentQueue::ApplyTransactionOperationsInMemory(System.Collections.Generic.IEnumerable`1<mixpanel.queue.PersistentQueueOperation>)
extern void PersistentQueue_ApplyTransactionOperationsInMemory_m3D95236137A4307D949AB0A667229F987E76F29D ();
// 0x0000017D System.Void mixpanel.queue.PersistentQueue::AssertTransactionSeparator(System.IO.BinaryReader,System.Int32,mixpanel.queue.PersistentQueueMarkers,System.Action)
extern void PersistentQueue_AssertTransactionSeparator_m78708D03928BD590ED08A0EBF751B051403A5473 ();
// 0x0000017E System.Void mixpanel.queue.PersistentQueue::ReadMetaState()
extern void PersistentQueue_ReadMetaState_m0CA569D17FF9A33F1F789F44B28AE2A0803FBF56 ();
// 0x0000017F System.Void mixpanel.queue.PersistentQueue::TrimTransactionLogIfNeeded(System.Int64)
extern void PersistentQueue_TrimTransactionLogIfNeeded_mF69737D35DF580A1A115BD780A87A4491F1AAFF3 ();
// 0x00000180 System.Void mixpanel.queue.PersistentQueue::ApplyTransactionOperations(System.Collections.Generic.IEnumerable`1<mixpanel.queue.PersistentQueueOperation>)
extern void PersistentQueue_ApplyTransactionOperations_m0A6E07FEBC317628A77FF113DECEDD300F899BA0 ();
// 0x00000181 System.Byte[] mixpanel.queue.PersistentQueue::GenerateTransactionBuffer(System.Collections.Generic.ICollection`1<mixpanel.queue.PersistentQueueOperation>)
extern void PersistentQueue_GenerateTransactionBuffer_m7D1FFF1713F789C222438614E1DE2027B1C35802 ();
// 0x00000182 System.IO.FileStream mixpanel.queue.PersistentQueue::CreateWriter()
extern void PersistentQueue_CreateWriter_mBFC5EC9CC39F0A9104B9CC666C499268D42160B1 ();
// 0x00000183 System.String mixpanel.queue.PersistentQueue::GetDataPath(System.Int32)
extern void PersistentQueue_GetDataPath_m8D96E6DDD94319C5B571F4619F044F8B6FF7A85E ();
// 0x00000184 System.Int64 mixpanel.queue.PersistentQueue::GetOptimalTransactionLogSize()
extern void PersistentQueue_GetOptimalTransactionLogSize_mBE59C364573BC0739B5C610A8B6528E5BA30854F ();
// 0x00000185 System.Void mixpanel.queue.PersistentQueue::Clear()
extern void PersistentQueue_Clear_mDF9B255D59221CCBF6E4CD5C8BF80D1D580B8CCD ();
// 0x00000186 System.Void mixpanel.queue.PersistentQueue::.cctor()
extern void PersistentQueue__cctor_mB2A8F64702526EA18557133A04C188E4E4119A4D ();
// 0x00000187 System.Void mixpanel.queue.PersistentQueue::<CommitTransaction>b__31_0(System.IO.Stream)
extern void PersistentQueue_U3CCommitTransactionU3Eb__31_0_m4129F0280E92041DC43B33D640F1660FD9955108 ();
// 0x00000188 System.Void mixpanel.queue.PersistentQueue::<ReadMetaState>b__44_0(System.IO.Stream)
extern void PersistentQueue_U3CReadMetaStateU3Eb__44_0_m1C5AEE3065D893F16008595351519AF36BF8BFF3 ();
// 0x00000189 System.Void mixpanel.queue.PersistentQueueEntry::.ctor(System.Int32,System.Int32,System.Int32)
extern void PersistentQueueEntry__ctor_mEB206A35F980CD0A8FC898AF39D9CAD87D75EFE8 ();
// 0x0000018A System.Void mixpanel.queue.PersistentQueueEntry::.ctor(mixpanel.queue.PersistentQueueOperation)
extern void PersistentQueueEntry__ctor_mCBAC51D0FBC1A9BC6D1213B2BF8121A7764D7744 ();
// 0x0000018B System.Boolean mixpanel.queue.PersistentQueueEntry::Equals(mixpanel.queue.PersistentQueueEntry)
extern void PersistentQueueEntry_Equals_m458126F0D5A8BBFECD646B5E2CED80113BFAAF41 ();
// 0x0000018C System.Boolean mixpanel.queue.PersistentQueueEntry::Equals(System.Object)
extern void PersistentQueueEntry_Equals_m2C2C8BEC897BFCFFED93C12A43FFB4813276A185 ();
// 0x0000018D System.Int32 mixpanel.queue.PersistentQueueEntry::GetHashCode()
extern void PersistentQueueEntry_GetHashCode_mD369196BDDF534A2A1260723EC9C9948C2D96C08 ();
// 0x0000018E System.Boolean mixpanel.queue.PersistentQueueEntry::op_Equality(mixpanel.queue.PersistentQueueEntry,mixpanel.queue.PersistentQueueEntry)
extern void PersistentQueueEntry_op_Equality_mEA0F62EB5F3B72A8C241D0B6A3D5A2B223D0DAB4 ();
// 0x0000018F System.Boolean mixpanel.queue.PersistentQueueEntry::op_Inequality(mixpanel.queue.PersistentQueueEntry,mixpanel.queue.PersistentQueueEntry)
extern void PersistentQueueEntry_op_Inequality_m5F47B256D218A73B1B90E3494503453D67300D02 ();
// 0x00000190 System.Void mixpanel.queue.PersistentQueueOperation::.ctor(mixpanel.queue.PersistentQueueOperationTypes,System.Int32,System.Int32,System.Int32)
extern void PersistentQueueOperation__ctor_mA52A128EF311BED74241CE8F393634EC0B61B9A6 ();
// 0x00000191 System.Void mixpanel.queue.PersistentQueueSession::.ctor(mixpanel.queue.PersistentQueue,System.IO.Stream)
extern void PersistentQueueSession__ctor_m65837A3111BE6219458716645231F6292DCD4955 ();
// 0x00000192 System.Void mixpanel.queue.PersistentQueueSession::Finalize()
extern void PersistentQueueSession_Finalize_m0FA33708F84D834BD0825ECAFCDDB06FCB9FF032 ();
// 0x00000193 System.Void mixpanel.queue.PersistentQueueSession::Enqueue(System.Byte[])
extern void PersistentQueueSession_Enqueue_m274733E3E48A42C254312808CC94DA03D4D95C50 ();
// 0x00000194 System.Void mixpanel.queue.PersistentQueueSession::AsyncFlushBuffer()
extern void PersistentQueueSession_AsyncFlushBuffer_m8FD2C50C3F6884F965490030895187710E0601A7 ();
// 0x00000195 System.Void mixpanel.queue.PersistentQueueSession::SyncFlushBuffer()
extern void PersistentQueueSession_SyncFlushBuffer_m2F6D9D269AC0D30E9976A4AC3A09B9A576D196D8 ();
// 0x00000196 System.Int64 mixpanel.queue.PersistentQueueSession::AsyncWriteToStream(System.IO.Stream)
extern void PersistentQueueSession_AsyncWriteToStream_m6C9D372CBAD07BC6E4E3A503C3FD6B8733522C33 ();
// 0x00000197 System.Byte[] mixpanel.queue.PersistentQueueSession::ConcatenateBufferAndAddIndividualOperations(System.IO.Stream)
extern void PersistentQueueSession_ConcatenateBufferAndAddIndividualOperations_m1F4735CE325637612B78A6DC6D8F7C871FA21894 ();
// 0x00000198 System.Void mixpanel.queue.PersistentQueueSession::OnReplaceStream(System.IO.Stream)
extern void PersistentQueueSession_OnReplaceStream_m36896D1B59C214C86E1CD371212E227AC32E8B32 ();
// 0x00000199 System.Byte[] mixpanel.queue.PersistentQueueSession::Dequeue()
extern void PersistentQueueSession_Dequeue_m4B5E669B9208A5E3AC4550CD6CC66EF6962A1370 ();
// 0x0000019A System.Void mixpanel.queue.PersistentQueueSession::Flush()
extern void PersistentQueueSession_Flush_m07F15FE5C4F66C24A7AF4FCB760185E566245CB2 ();
// 0x0000019B System.Void mixpanel.queue.PersistentQueueSession::WaitForPendingWrites()
extern void PersistentQueueSession_WaitForPendingWrites_m105E3D4E7EA55B5E947BF87E12A6873E9299BFBE ();
// 0x0000019C System.Void mixpanel.queue.PersistentQueueSession::AssertNoPendingWritesFailures()
extern void PersistentQueueSession_AssertNoPendingWritesFailures_mEBC84FCA8E6D166C6AB29880C9B76C80F1C426AF ();
// 0x0000019D System.Void mixpanel.queue.PersistentQueueSession::Dispose()
extern void PersistentQueueSession_Dispose_mCCAC09F6A1BADB8E297CD0F42ED28178152D0A37 ();
// 0x0000019E System.Void mixpanel.queue.PersistentQueueSession::.cctor()
extern void PersistentQueueSession__cctor_mD5EB5C1B0B79B18AC77D6EB9897D143BDEC95831 ();
// 0x0000019F System.Int64 mixpanel.queue.PersistentQueueSession::<SyncFlushBuffer>b__14_0(System.IO.Stream)
extern void PersistentQueueSession_U3CSyncFlushBufferU3Eb__14_0_mBCCA91C92F2723E2773D72FA0BB4778EA83D0FDC ();
// 0x000001A0 System.Void mixpanel.queue.PendingWriteException::.ctor(System.Exception[])
extern void PendingWriteException__ctor_m21342C30EFE1762297E1091AEDE3A3EBA2E693C2 ();
// 0x000001A1 System.String mixpanel.queue.PendingWriteException::get_Message()
extern void PendingWriteException_get_Message_m043870E809E0F4F6681CA676EDCF555B95ADB8CF ();
// 0x000001A2 System.String mixpanel.queue.PendingWriteException::ToString()
extern void PendingWriteException_ToString_m1CFC670078A1600F567017767577479C8BAA86AD ();
// 0x000001A3 System.Void mixpanel.queue.PendingWriteException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void PendingWriteException_GetObjectData_mE397DC37C1116528E2B8278A789B07FC408E4582 ();
// 0x000001A4 System.IO.FileStream mixpanel.queue.PersistentQueueUtils::CreateFileStream(System.String,System.IO.FileMode,System.IO.FileAccess,System.IO.FileOptions)
extern void PersistentQueueUtils_CreateFileStream_m2499F0F23E8AD0EC95EE99007F7B399B2F86C26F ();
// 0x000001A5 System.IO.FileStream mixpanel.queue.PersistentQueueUtils::CreateReadStream(System.String)
extern void PersistentQueueUtils_CreateReadStream_mB2A55B03F4A2A9A35139A87EEDF0520B5FE56981 ();
// 0x000001A6 System.IO.FileStream mixpanel.queue.PersistentQueueUtils::CreateWriteStream(System.String)
extern void PersistentQueueUtils_CreateWriteStream_mFB0597CC35C0FC6AA0D37C9D58D059B9C79B5A26 ();
// 0x000001A7 System.Void mixpanel.queue.PersistentQueueUtils::Read(System.String,System.Action`1<System.IO.Stream>)
extern void PersistentQueueUtils_Read_mED8A8E34A21E61A990018FDBC041DFC0F059AFC4 ();
// 0x000001A8 System.Void mixpanel.queue.PersistentQueueUtils::Write(System.String,System.Action`1<System.IO.Stream>)
extern void PersistentQueueUtils_Write_mD0CD814DFFBA6D65FC4EE712F97F5EFF56B51D18 ();
// 0x000001A9 System.Boolean mixpanel.queue.PersistentQueueUtils::WaitDelete(System.String)
extern void PersistentQueueUtils_WaitDelete_m11EE50B1C6DEE458F410146BAFF11CE1A8AA7195 ();
// 0x000001AA System.Void mixpanel.queue.PersistentQueueUtils::.cctor()
extern void PersistentQueueUtils__cctor_m4AA6BE8D00B1C62A5E75A61DEB9828A27D4C63EE ();
// 0x000001AB System.Void mixpanel.Controller_Metadata::InitSession()
extern void Metadata_InitSession_m8D739B6A2407320AA4A31C39398E52CED3B2EA1F ();
// 0x000001AC mixpanel.Value mixpanel.Controller_Metadata::GetEventMetadata()
extern void Metadata_GetEventMetadata_mF455A4C21B7097C3C8B6E8F413B7E7BE18D302E8 ();
// 0x000001AD mixpanel.Value mixpanel.Controller_Metadata::GetPeopleMetadata()
extern void Metadata_GetPeopleMetadata_m714E66E2B7E8001848DBB53CCF99BFEB510645DF ();
// 0x000001AE System.Void mixpanel.Controller_Metadata::.cctor()
extern void Metadata__cctor_m298657D0CDF78F39952F4E58D18EE824996323F6 ();
// 0x000001AF System.Void mixpanel.Controller_<Start>d__8::.ctor(System.Int32)
extern void U3CStartU3Ed__8__ctor_m1E5F43D06B79EB58DCA389A10A3F47E26F742B29 ();
// 0x000001B0 System.Void mixpanel.Controller_<Start>d__8::System.IDisposable.Dispose()
extern void U3CStartU3Ed__8_System_IDisposable_Dispose_mBE9EFC28242ABDDA2681FF5F770CC6351C6A1CF6 ();
// 0x000001B1 System.Boolean mixpanel.Controller_<Start>d__8::MoveNext()
extern void U3CStartU3Ed__8_MoveNext_mEFA949E8314919B469946596C9581DAF36B0271D ();
// 0x000001B2 System.Object mixpanel.Controller_<Start>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF983395B30CA3D247879E8E37611DCF9DBB63569 ();
// 0x000001B3 System.Void mixpanel.Controller_<Start>d__8::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__8_System_Collections_IEnumerator_Reset_mFF6526037D0C10E5487CBCFEB4C81893DDFF292D ();
// 0x000001B4 System.Object mixpanel.Controller_<Start>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__8_System_Collections_IEnumerator_get_Current_mA03EB3CE99BB01261ED4522FE11ACE95FF938B31 ();
// 0x000001B5 System.Void mixpanel.Controller_<WaitForIntegrationRequest>d__10::.ctor(System.Int32)
extern void U3CWaitForIntegrationRequestU3Ed__10__ctor_m189EDB7A9C48C66747ED5B9803BDFF2972084340 ();
// 0x000001B6 System.Void mixpanel.Controller_<WaitForIntegrationRequest>d__10::System.IDisposable.Dispose()
extern void U3CWaitForIntegrationRequestU3Ed__10_System_IDisposable_Dispose_m1C0105A91134DDBF01CD44A5F29774CBCC5C31E8 ();
// 0x000001B7 System.Boolean mixpanel.Controller_<WaitForIntegrationRequest>d__10::MoveNext()
extern void U3CWaitForIntegrationRequestU3Ed__10_MoveNext_m36AAAAF0BC42FA75B17790A795F5373C3CBC4F02 ();
// 0x000001B8 UnityEngine.Networking.UnityWebRequest mixpanel.Controller_<WaitForIntegrationRequest>d__10::System.Collections.Generic.IEnumerator<UnityEngine.Networking.UnityWebRequest>.get_Current()
extern void U3CWaitForIntegrationRequestU3Ed__10_System_Collections_Generic_IEnumeratorU3CUnityEngine_Networking_UnityWebRequestU3E_get_Current_m9C550F01B51B3AF87A491D33C156ECDE9F7E9439 ();
// 0x000001B9 System.Void mixpanel.Controller_<WaitForIntegrationRequest>d__10::System.Collections.IEnumerator.Reset()
extern void U3CWaitForIntegrationRequestU3Ed__10_System_Collections_IEnumerator_Reset_m8F384AFDBC723B64E62A800265D0D27D6D2A3324 ();
// 0x000001BA System.Object mixpanel.Controller_<WaitForIntegrationRequest>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForIntegrationRequestU3Ed__10_System_Collections_IEnumerator_get_Current_m2D5CC25728632EE8E377F917363C32058C50FFE9 ();
// 0x000001BB System.Void mixpanel.Controller_<PopulatePools>d__11::.ctor(System.Int32)
extern void U3CPopulatePoolsU3Ed__11__ctor_m4ED3FF273F08BBEC2E4263A24B23E7EA1E8C39DB ();
// 0x000001BC System.Void mixpanel.Controller_<PopulatePools>d__11::System.IDisposable.Dispose()
extern void U3CPopulatePoolsU3Ed__11_System_IDisposable_Dispose_m403AB53466FDCECF3D4134980C1D3AC050B1EEFD ();
// 0x000001BD System.Boolean mixpanel.Controller_<PopulatePools>d__11::MoveNext()
extern void U3CPopulatePoolsU3Ed__11_MoveNext_mCA08CA7850DC596D7C93E7C1AA06F351D53155AC ();
// 0x000001BE System.Object mixpanel.Controller_<PopulatePools>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPopulatePoolsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3EDEAE544386A2C548C709291069CB4C5B102B01 ();
// 0x000001BF System.Void mixpanel.Controller_<PopulatePools>d__11::System.Collections.IEnumerator.Reset()
extern void U3CPopulatePoolsU3Ed__11_System_Collections_IEnumerator_Reset_m641D565D3CD1EADED236F44B4C8D33975B572B2A ();
// 0x000001C0 System.Object mixpanel.Controller_<PopulatePools>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CPopulatePoolsU3Ed__11_System_Collections_IEnumerator_get_Current_m420AC1E1D95FC863391780415F4DF08076D5B5AC ();
// 0x000001C1 System.Void mixpanel.Extensions_<>c__DisplayClass0_0`1::.ctor()
// 0x000001C2 System.Int32 mixpanel.Extensions_<>c__DisplayClass0_0`1::<Batch>b__1(<>f__AnonymousType0`2<T,System.Int32>)
// 0x000001C3 System.Void mixpanel.Extensions_<>c__0`1::.cctor()
// 0x000001C4 System.Void mixpanel.Extensions_<>c__0`1::.ctor()
// 0x000001C5 <>f__AnonymousType0`2<T,System.Int32> mixpanel.Extensions_<>c__0`1::<Batch>b__0_0(T,System.Int32)
// 0x000001C6 System.Collections.Generic.IEnumerable`1<T> mixpanel.Extensions_<>c__0`1::<Batch>b__0_2(System.Linq.IGrouping`2<System.Int32,<>f__AnonymousType0`2<T,System.Int32>>)
// 0x000001C7 T mixpanel.Extensions_<>c__0`1::<Batch>b__0_3(<>f__AnonymousType0`2<T,System.Int32>)
// 0x000001C8 System.Void mixpanel.Mixpanel_People::Append(mixpanel.Value)
extern void People_Append_m1AF128583ECF3A531B45EC8329AFD84850C19640 ();
// 0x000001C9 System.Void mixpanel.Mixpanel_People::Append(System.String,mixpanel.Value)
extern void People_Append_m0835FC7506A8BC16EBA57BF4A56134B01C8ABC39 ();
// 0x000001CA System.Void mixpanel.Mixpanel_People::ClearCharges()
extern void People_ClearCharges_mBC2B60DAB8448D007F22D3B2982B8E4E42752029 ();
// 0x000001CB System.Void mixpanel.Mixpanel_People::DeleteUser()
extern void People_DeleteUser_mACE5E17A8236CD2BDF5B3EB1A00F18EE71A8758C ();
// 0x000001CC System.Void mixpanel.Mixpanel_People::Increment(mixpanel.Value)
extern void People_Increment_m29354447C00251CE8272EE445DE7C0EEC47F1B4F ();
// 0x000001CD System.Void mixpanel.Mixpanel_People::Increment(System.String,mixpanel.Value)
extern void People_Increment_m8D0457939C1F64DC5A90A1EA5CA8EF91A5ED82A8 ();
// 0x000001CE System.Void mixpanel.Mixpanel_People::Set(mixpanel.Value)
extern void People_Set_m6316E825DD8874E363543D1CFB570CF8DB79E2DA ();
// 0x000001CF System.Void mixpanel.Mixpanel_People::Set(System.String,mixpanel.Value)
extern void People_Set_m7FE99127F03EE093726B01313099441E461D1014 ();
// 0x000001D0 System.Void mixpanel.Mixpanel_People::SetOnce(mixpanel.Value)
extern void People_SetOnce_mD0A6311734AD35E15FCC9CA050A9112FDEB51283 ();
// 0x000001D1 System.Void mixpanel.Mixpanel_People::SetOnce(System.String,mixpanel.Value)
extern void People_SetOnce_m412322D9E02308EB7594B79CF21A434799326941 ();
// 0x000001D2 System.Void mixpanel.Mixpanel_People::TrackCharge(System.Double)
extern void People_TrackCharge_m9B3794A4730BFA67878655D6FEE05A0EB1702309 ();
// 0x000001D3 System.Void mixpanel.Mixpanel_People::TrackCharge(mixpanel.Value)
extern void People_TrackCharge_mFD8F836798F4712AC5516C35363D888602DA8CDA ();
// 0x000001D4 System.Void mixpanel.Mixpanel_People::Union(mixpanel.Value)
extern void People_Union_m80032B0C1A73518A0E19DCCBA62588C25B71DE5B ();
// 0x000001D5 System.Void mixpanel.Mixpanel_People::Union(System.String,mixpanel.Value)
extern void People_Union_mCE8C1C56A269F772BAE4063851E236E4B5F6209D ();
// 0x000001D6 System.Void mixpanel.Mixpanel_People::Unset(System.String)
extern void People_Unset_mD3F9A7191EB68FD815F1EFAC9078158A9DAC2467 ();
// 0x000001D7 System.Void mixpanel.Mixpanel_People::set_Email(System.String)
extern void People_set_Email_m32DAEA9AA662B865F15F378B63A6E6E456874257 ();
// 0x000001D8 System.Void mixpanel.Mixpanel_People::set_FirstName(System.String)
extern void People_set_FirstName_m518DA84D3FA7A6E70A9C3978112F14CD561770AE ();
// 0x000001D9 System.Void mixpanel.Mixpanel_People::set_LastName(System.String)
extern void People_set_LastName_mF5231BC946009D5AB6B4AD73495FF853AB970445 ();
// 0x000001DA System.Void mixpanel.Mixpanel_People::set_Name(System.String)
extern void People_set_Name_mCDFD6AE18C56D74256A155681F43711232FD7592 ();
// 0x000001DB System.Void mixpanel.Mixpanel_People::set_PushDeviceToken(System.Byte[])
extern void People_set_PushDeviceToken_m475D5FD38EB5AE28E9BBEEA7081BE8F55C3C0E39 ();
// 0x000001DC System.Void mixpanel.Mixpanel_<>c::.cctor()
extern void U3CU3Ec__cctor_m5A088F65A8921BA9F995D4B99C44C831224B333F ();
// 0x000001DD System.Void mixpanel.Mixpanel_<>c::.ctor()
extern void U3CU3Ec__ctor_mD4CE4D96A0E149D4546970C4DB4AB1C4C718E2EF ();
// 0x000001DE mixpanel.Value mixpanel.Mixpanel_<>c::<.cctor>b__31_0()
extern void U3CU3Ec_U3C_cctorU3Eb__31_0_mFB84FE890A90881B8615D20FFB81CCB56680BA97 ();
// 0x000001DF mixpanel.Value mixpanel.Mixpanel_<>c::<.cctor>b__31_1()
extern void U3CU3Ec_U3C_cctorU3Eb__31_1_mF17BF1E4F43ED80D16A39D3932BFDA7D308799AD ();
// 0x000001E0 mixpanel.Value mixpanel.Mixpanel_<>c::<.cctor>b__31_2()
extern void U3CU3Ec_U3C_cctorU3Eb__31_2_m3D61F11AE4E05CAB6968DBF8EB49D8BE680BCBD6 ();
// 0x000001E1 System.Void mixpanel.Value_<>c::.cctor()
extern void U3CU3Ec__cctor_mCF109041E91F6B4D407ED6B21A3FE79D78561CD2 ();
// 0x000001E2 System.Void mixpanel.Value_<>c::.ctor()
extern void U3CU3Ec__ctor_m7D127594D6243247D88507C06510F85CEC4AA9FF ();
// 0x000001E3 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__114_0(System.String)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__114_0_mA7A1B3E56F7A884CF38EA7CA34867F8608FB3A1F ();
// 0x000001E4 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__115_0(System.String)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__115_0_m0F5E7DEBF74D9A391A3CA41253B419470389D663 ();
// 0x000001E5 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__117_0(System.Boolean)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__117_0_mF5A3FEA2E067D92B748A40DBBDABCF60895309A0 ();
// 0x000001E6 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__118_0(System.Boolean)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__118_0_m272F3D1785480530814E4D42356B9E87505AA841 ();
// 0x000001E7 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__120_0(System.Single)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__120_0_mEB2EE4361A2B293911A7CA7F08E9325D365E352F ();
// 0x000001E8 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__121_0(System.Single)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__121_0_mEAA1E6FDF01DF9ACE19AE0497A597A0B064F8D93 ();
// 0x000001E9 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__123_0(System.Double)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__123_0_m4A1BDCFD3F7F02C2D3770D0BA64AB9F6F0196D73 ();
// 0x000001EA mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__124_0(System.Double)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__124_0_m7BE7B659023FBCDB8F0A881FE4E5B2566B7C6698 ();
// 0x000001EB mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__126_0(System.Decimal)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__126_0_mA134383B9E97ED27248F8AAD475D0168032553B4 ();
// 0x000001EC mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__127_0(System.Decimal)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__127_0_m3DEDF631B4038BEC3C4678903844C60D09B6132E ();
// 0x000001ED mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__129_0(System.Int16)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__129_0_m77F2DB53B34FFEB901B0A220C4979C4F822789A9 ();
// 0x000001EE mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__130_0(System.Int16)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__130_0_m3E86001AC36299EBE8E29558DF8E53594B8B469D ();
// 0x000001EF mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__132_0(System.Int32)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__132_0_m8066E140CD1DAC8DD263B7752839277953560F49 ();
// 0x000001F0 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__133_0(System.Int32)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__133_0_mF3A7D6CD06491A88EFBE447D0F718D2A82D491D7 ();
// 0x000001F1 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__135_0(System.Int64)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__135_0_m0ADBCD472002384977572BC0FFC75B628378477F ();
// 0x000001F2 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__136_0(System.Int64)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__136_0_mFEA2D80FBAD1B3A51F5478EBEEDF93E91CC60D8F ();
// 0x000001F3 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__138_0(System.UInt16)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__138_0_m364FFE65F5B3E72265FB68EB0B3471435032E4C5 ();
// 0x000001F4 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__139_0(System.UInt16)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__139_0_m8A28378FCC2FA71213CE6FDA95967EDE37A4A2D4 ();
// 0x000001F5 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__141_0(System.UInt32)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__141_0_m87840995CB9C0609AF5B53577AAEB8E73BBD6CE9 ();
// 0x000001F6 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__142_0(System.UInt32)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__142_0_mA710ECAA9511A0F3B923D40444F9707E33A9C089 ();
// 0x000001F7 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__144_0(System.UInt64)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__144_0_mF0D028217318502B570669C9B653D8E9A5BDCB4E ();
// 0x000001F8 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__145_0(System.UInt64)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__145_0_m535617CB88D4E263A8DFDEE8864D6B2925EDB97A ();
// 0x000001F9 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__147_0(System.SByte)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__147_0_m03D24EEEA8E265456DD5BE70CC0E1EDF6067011A ();
// 0x000001FA mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__148_0(System.SByte)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__148_0_m560873732CEBC5F3C100A06EA08895302972D28C ();
// 0x000001FB mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__150_0(System.Byte)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__150_0_m3F16AEE6217C6210782E0843AAA87F1EE45B49B3 ();
// 0x000001FC mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__151_0(System.Byte)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__151_0_m905DF442A5FFF7EEA3DFD88F789FBCE50F3D60EF ();
// 0x000001FD mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__153_0(System.Uri)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__153_0_m316F324279FAAC81C1FC8D57EA9E509F9E22A347 ();
// 0x000001FE mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__154_0(System.Uri)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__154_0_m4131136D61B47224E9B4B6E3EFCC4FCD1197B2C2 ();
// 0x000001FF mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__156_0(System.Guid)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__156_0_m741BDB96B5BA72D3289DEF954ABE9441499E8348 ();
// 0x00000200 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__157_0(System.Guid)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__157_0_mE09021EE9C26EBDC0735A9B3BEE58ECCA33AEDBB ();
// 0x00000201 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__159_0(System.DateTime)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__159_0_m621A2E1F7C26C0E814497D963B2A9CBC730533E0 ();
// 0x00000202 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__160_0(System.DateTime)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__160_0_mBE54BA01BB7905C91DFE4110E09087865535336D ();
// 0x00000203 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__162_0(System.DateTimeOffset)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__162_0_mAC167B19FB4E0BB9D3DD5CBE76015167C3C00B99 ();
// 0x00000204 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__163_0(System.DateTimeOffset)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__163_0_m8ACB6600785A13579031E986593523F88ED35EE2 ();
// 0x00000205 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__165_0(System.TimeSpan)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__165_0_m0075866D37C6E6C480C56C402EFBDB77C5ABA5AC ();
// 0x00000206 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__166_0(System.TimeSpan)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__166_0_m87AEF8D8F0FB4FF220B3A31CC4C04B1EF411EC66 ();
// 0x00000207 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__168_0(UnityEngine.Color)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__168_0_mFD585FB22C67E4BA2F72D410FE0C3916F159322F ();
// 0x00000208 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__169_0(UnityEngine.Color)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__169_0_m46EDC645192F3D8263A689E3C88DD9BB53087AF4 ();
// 0x00000209 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__171_0(UnityEngine.Color32)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__171_0_mD31CA3E205FD5D222672CCFEC6E22D13AA23DE8D ();
// 0x0000020A mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__172_0(UnityEngine.Color32)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__172_0_mF0182CBE6238ECCF2AF8F7A0CB4809512603A4AA ();
// 0x0000020B mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__174_0(UnityEngine.Vector2)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__174_0_mE3FDFC7332EDC9D63AF51EB8046C31A81403B6C7 ();
// 0x0000020C mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__175_0(UnityEngine.Vector2)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__175_0_m9C573F25D8C7F8681AB96E577E70EBF170CD151E ();
// 0x0000020D mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__177_0(UnityEngine.Vector3)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__177_0_m5E289D6CB9D8BAC0F0AA2DCC5C4A3E7D0332B2F3 ();
// 0x0000020E mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__178_0(UnityEngine.Vector3)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__178_0_m649F6DD8D5C8CAD165DF4986833B5E82E7D77528 ();
// 0x0000020F mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__180_0(UnityEngine.Vector4)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__180_0_m354A3D74B69BF09B027D78F40D16FE7736A7B3C2 ();
// 0x00000210 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__181_0(UnityEngine.Vector4)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__181_0_m7C8E47854D95A76A3C996972EA6DA50C862D6E94 ();
// 0x00000211 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__183_0(UnityEngine.Quaternion)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__183_0_mC1B2F2C8D40368718AC288769827AA7A7E6D0089 ();
// 0x00000212 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__184_0(UnityEngine.Quaternion)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__184_0_m6C55C8CE48B949C91B11585C344845F70F859F17 ();
// 0x00000213 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__186_0(UnityEngine.Bounds)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__186_0_m510ECD1ADF26216BA768F2BC120BF7D8C2EB66BA ();
// 0x00000214 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__187_0(UnityEngine.Bounds)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__187_0_m7874F2A0B0ACD500C8408978F2B7F3594488BF46 ();
// 0x00000215 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__189_0(UnityEngine.Rect)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__189_0_m0DB7C843FA18A110A49C4E6D1F71CAC0F9BF12DF ();
// 0x00000216 mixpanel.Value mixpanel.Value_<>c::<op_Implicit>b__190_0(UnityEngine.Rect)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__190_0_mDDB69181E9D16FD8D53737B76FC8590CC7A51CF3 ();
// 0x00000217 System.String mixpanel.Value_<>c::<op_Implicit>b__192_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__192_0_m54A1034632E30FEDE7131DA77E6D1A2A9EF7A152 ();
// 0x00000218 System.String mixpanel.Value_<>c::<op_Implicit>b__193_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__193_0_m56BA602BEAAC2B79D5B2197DF558528DA0454998 ();
// 0x00000219 System.Boolean mixpanel.Value_<>c::<op_Implicit>b__195_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__195_0_m063C72D3E8368D1253EDAE133415620689E3A302 ();
// 0x0000021A System.Boolean mixpanel.Value_<>c::<op_Implicit>b__196_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__196_0_mD309EB60BE7711CCF21AB171BC7D60A7E54A9C23 ();
// 0x0000021B System.Single mixpanel.Value_<>c::<op_Implicit>b__198_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__198_0_mED966F4DF4AC273E320DA0A483B9E36CA1CB01DC ();
// 0x0000021C System.Single mixpanel.Value_<>c::<op_Implicit>b__199_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__199_0_mCDDDB573232075C3BF5E7D5BBA0B0751F54BCCB5 ();
// 0x0000021D System.Double mixpanel.Value_<>c::<op_Implicit>b__201_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__201_0_mEA7DD45BFF23F15AFF53AC41763274C13B5EDCF6 ();
// 0x0000021E System.Double mixpanel.Value_<>c::<op_Implicit>b__202_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__202_0_m00D8E72D01E1CDA2D298A7A4A7F69F40AD407518 ();
// 0x0000021F System.Decimal mixpanel.Value_<>c::<op_Implicit>b__204_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__204_0_m99EDD55A7BD424E3EB34ECE989381902612B840B ();
// 0x00000220 System.Decimal mixpanel.Value_<>c::<op_Implicit>b__205_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__205_0_m296A583FDEE787A79F87F54373EDEF561BF1FD60 ();
// 0x00000221 System.Int16 mixpanel.Value_<>c::<op_Implicit>b__207_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__207_0_m9A391E08D696A9E923CAA7CFB2D485B67EDDB07E ();
// 0x00000222 System.Int16 mixpanel.Value_<>c::<op_Implicit>b__208_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__208_0_m4EBE51062BAD9C1BBE9D4358C8CDD0F4E7773C2C ();
// 0x00000223 System.Int32 mixpanel.Value_<>c::<op_Implicit>b__210_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__210_0_m69B31C8007928E7683A02FC7195C8279D9C97B1B ();
// 0x00000224 System.Int32 mixpanel.Value_<>c::<op_Implicit>b__211_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__211_0_mCDBF77A9B5A626CFAF094A47BF5E5891E1092A6D ();
// 0x00000225 System.Int64 mixpanel.Value_<>c::<op_Implicit>b__213_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__213_0_mE4057A03A3B433AA1317388706FF327A0EBB9969 ();
// 0x00000226 System.Int64 mixpanel.Value_<>c::<op_Implicit>b__214_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__214_0_mF8AFCFA7E76C2BD636C0C2C301A8CF2F5D4180F4 ();
// 0x00000227 System.UInt16 mixpanel.Value_<>c::<op_Implicit>b__216_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__216_0_m6D1F371B27FB64F02C82D1D1F6F5C7333DAD975C ();
// 0x00000228 System.UInt16 mixpanel.Value_<>c::<op_Implicit>b__217_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__217_0_m0EE51255D0CFF2D389EF12F9C9CA7AFF2CE4305D ();
// 0x00000229 System.UInt32 mixpanel.Value_<>c::<op_Implicit>b__219_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__219_0_mF87F9E0A45022D86FFCEA59FEAF592EADFF14582 ();
// 0x0000022A System.UInt32 mixpanel.Value_<>c::<op_Implicit>b__220_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__220_0_m77FFB08FF119E36C3B3B06F4A46DCEE015FA3201 ();
// 0x0000022B System.UInt64 mixpanel.Value_<>c::<op_Implicit>b__222_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__222_0_m8A69D8B16BDD3DACFB953B317AF1FCB956845457 ();
// 0x0000022C System.UInt64 mixpanel.Value_<>c::<op_Implicit>b__223_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__223_0_mD6127D7AE16580E25CE2E2E5359CA8B5AB4DBA5D ();
// 0x0000022D System.SByte mixpanel.Value_<>c::<op_Implicit>b__225_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__225_0_m806F495611B4E1E3E9431E98E8E1D110940EA80F ();
// 0x0000022E System.SByte mixpanel.Value_<>c::<op_Implicit>b__226_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__226_0_mF9C977521E3AF4139ED2414205BC55B96580F979 ();
// 0x0000022F System.Byte mixpanel.Value_<>c::<op_Implicit>b__228_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__228_0_m79BA6C2A73B66B742A76AEAC327AB04D3DA05FBB ();
// 0x00000230 System.Byte mixpanel.Value_<>c::<op_Implicit>b__229_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__229_0_mC85C99D76B58CFEAAAA557744542E803FDF53D10 ();
// 0x00000231 System.Uri mixpanel.Value_<>c::<op_Implicit>b__231_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__231_0_m5F7EB21D510056D3838B22973BD3B1EE5A698ED8 ();
// 0x00000232 System.Uri mixpanel.Value_<>c::<op_Implicit>b__232_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__232_0_m368F67D37FAA42ED27D30993B0D2D8CAD910CA53 ();
// 0x00000233 System.Guid mixpanel.Value_<>c::<op_Implicit>b__234_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__234_0_mAB569B015E20268AA1578306A3B33F14E6D70981 ();
// 0x00000234 System.Guid mixpanel.Value_<>c::<op_Implicit>b__235_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__235_0_m02682C0EEC02D825D21E6FDD095D246D1F820747 ();
// 0x00000235 System.DateTime mixpanel.Value_<>c::<op_Implicit>b__237_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__237_0_mA437377BFABDA51E0F0C54F1FFA554854808D171 ();
// 0x00000236 System.DateTime mixpanel.Value_<>c::<op_Implicit>b__238_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__238_0_mA4690A2A267C10D3226300514326337A415C4108 ();
// 0x00000237 System.DateTimeOffset mixpanel.Value_<>c::<op_Implicit>b__240_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__240_0_mE2AC9CD0CCB7A9999F0C8D677CB6171DCC0800D8 ();
// 0x00000238 System.DateTimeOffset mixpanel.Value_<>c::<op_Implicit>b__241_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__241_0_mD42EA266A16D0E83AE3A12F542075A9FF4B3A525 ();
// 0x00000239 System.TimeSpan mixpanel.Value_<>c::<op_Implicit>b__243_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__243_0_mB0AA67C9521127BD8D148563BC2157504181C31D ();
// 0x0000023A System.TimeSpan mixpanel.Value_<>c::<op_Implicit>b__244_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__244_0_mEE2FC20007E8F9827BF2C0E9BA1126263636CD54 ();
// 0x0000023B UnityEngine.Color mixpanel.Value_<>c::<op_Implicit>b__246_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__246_0_m28D9CDE9E94927700BE5318124C15F556EFBA58E ();
// 0x0000023C UnityEngine.Color mixpanel.Value_<>c::<op_Implicit>b__247_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__247_0_m5AFA198C9E8CA02793E418620694D9F1092E82A3 ();
// 0x0000023D UnityEngine.Color32 mixpanel.Value_<>c::<op_Implicit>b__249_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__249_0_mD3BA5EB3F95EA1DD1410A7C293ED3AAD3BE29B1B ();
// 0x0000023E UnityEngine.Color32 mixpanel.Value_<>c::<op_Implicit>b__250_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__250_0_mF5D024023802A95D7341B7952FFF8E4874D2E077 ();
// 0x0000023F UnityEngine.Vector2 mixpanel.Value_<>c::<op_Implicit>b__252_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__252_0_m8E137551802405D34ED7140E3B0EC1DBE8B2654B ();
// 0x00000240 UnityEngine.Vector2 mixpanel.Value_<>c::<op_Implicit>b__253_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__253_0_m35B958B154DDE4D04F3D4115CBE56CE316E0EDB2 ();
// 0x00000241 UnityEngine.Vector3 mixpanel.Value_<>c::<op_Implicit>b__255_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__255_0_m82B8DDDC7CD001D7900409F7AE9485268F073DF0 ();
// 0x00000242 UnityEngine.Vector3 mixpanel.Value_<>c::<op_Implicit>b__256_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__256_0_m6B263F080B40A8CA40CFD6D957D576DF7BA558E4 ();
// 0x00000243 UnityEngine.Vector4 mixpanel.Value_<>c::<op_Implicit>b__258_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__258_0_m6BD0CB872E466A54EBD315144F70684986C93E5B ();
// 0x00000244 UnityEngine.Vector4 mixpanel.Value_<>c::<op_Implicit>b__259_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__259_0_m134E5818D7E403459D1A391A2B44CBA83E3451B4 ();
// 0x00000245 UnityEngine.Quaternion mixpanel.Value_<>c::<op_Implicit>b__261_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__261_0_m86FD6CF9C561360B6B38C0D4375E3B99C3F178A2 ();
// 0x00000246 UnityEngine.Quaternion mixpanel.Value_<>c::<op_Implicit>b__262_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__262_0_m40EDFADE4735E298ADBD1B7DDEFEDCADA48DE2AE ();
// 0x00000247 UnityEngine.Bounds mixpanel.Value_<>c::<op_Implicit>b__264_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__264_0_m03DA55BF8C773CC1597310696BACD60912E8E38D ();
// 0x00000248 UnityEngine.Bounds mixpanel.Value_<>c::<op_Implicit>b__265_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__265_0_mDD672F10179D092DE18D04D05BF84D01785AF13A ();
// 0x00000249 UnityEngine.Rect mixpanel.Value_<>c::<op_Implicit>b__267_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__267_0_m84A40AD22995360D4F459D819D69970C5DD87BAD ();
// 0x0000024A UnityEngine.Rect mixpanel.Value_<>c::<op_Implicit>b__268_0(mixpanel.Value)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__268_0_mAF6FFBE38627673413AE78F127FF2CE17659D7CA ();
// 0x0000024B System.Void mixpanel.Worker_ThreadOperation::.ctor(mixpanel.Worker_ThreadOperation_ThreadOperationAction)
extern void ThreadOperation__ctor_mB00FAF38239AECA1EF3B827F6DBFB9FAA2B8D06F ();
// 0x0000024C System.Void mixpanel.Worker_ThreadOperation::.ctor(mixpanel.Worker_ThreadOperation_ThreadOperationAction,mixpanel.Value)
extern void ThreadOperation__ctor_m9176DC01023FCFCA49C58B7D27AA0CC82975B5D0 ();
// 0x0000024D mixpanel.Worker_ThreadOperation_ThreadOperationAction mixpanel.Worker_ThreadOperation::GetAction()
extern void ThreadOperation_GetAction_m667BDC2EA9FAF2F5D62AE3C8873B5A75F330F55C ();
// 0x0000024E mixpanel.Value mixpanel.Worker_ThreadOperation::GetWhat()
extern void ThreadOperation_GetWhat_m6068B88F0A8EF4E152882573F1290A46C0A28ABA ();
// 0x0000024F System.Void mixpanel.Worker_<>c::.cctor()
extern void U3CU3Ec__cctor_m6F511E4657E22307FBA3CF52A0ACDE0F16AC7776 ();
// 0x00000250 System.Void mixpanel.Worker_<>c::.ctor()
extern void U3CU3Ec__ctor_mDF3779D82D8F8585269E784685FB991CC073EDE7 ();
// 0x00000251 System.Void mixpanel.Worker_<>c::<SendData>b__18_0(System.Object)
extern void U3CU3Ec_U3CSendDataU3Eb__18_0_mDA97D86A94AD1327ECC076DF613DB1F0F7A1FFEA ();
// 0x00000252 System.Void mixpanel.Worker_<SendData>d__18::.ctor(System.Int32)
extern void U3CSendDataU3Ed__18__ctor_mE39723A866DA28EB86ECE2F843D7F1569D45201C ();
// 0x00000253 System.Void mixpanel.Worker_<SendData>d__18::System.IDisposable.Dispose()
extern void U3CSendDataU3Ed__18_System_IDisposable_Dispose_m8E01C92B4D60AB4813E7C60F2B62F1787949B308 ();
// 0x00000254 System.Boolean mixpanel.Worker_<SendData>d__18::MoveNext()
extern void U3CSendDataU3Ed__18_MoveNext_mEDE8C09CAC875A5AE7536AAAD2528F6744703B1C ();
// 0x00000255 System.Void mixpanel.Worker_<SendData>d__18::<>m__Finally1()
extern void U3CSendDataU3Ed__18_U3CU3Em__Finally1_mCDDC26019690D848F1F105F03C3CB3A32B76B344 ();
// 0x00000256 System.Object mixpanel.Worker_<SendData>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSendDataU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBAE8425D6B1A29B53D5147F3DB28B9F573AF6DD1 ();
// 0x00000257 System.Void mixpanel.Worker_<SendData>d__18::System.Collections.IEnumerator.Reset()
extern void U3CSendDataU3Ed__18_System_Collections_IEnumerator_Reset_m5241380F229302BA30FF5EDEB199E812A7A33594 ();
// 0x00000258 System.Object mixpanel.Worker_<SendData>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CSendDataU3Ed__18_System_Collections_IEnumerator_get_Current_mA209565953C9739F4D9613D3592DFD65886481C2 ();
// 0x00000259 System.Void mixpanel.queue.PersistentQueue_<>c::.cctor()
extern void U3CU3Ec__cctor_m268F61E56FB7E186C29B6D2830EDB6EEC86F0B06 ();
// 0x0000025A System.Void mixpanel.queue.PersistentQueue_<>c::.ctor()
extern void U3CU3Ec__ctor_m00FCFC696FF235951542ED821F5BCFB62416EE20 ();
// 0x0000025B System.Boolean mixpanel.queue.PersistentQueue_<>c::<Reinstate>b__37_0(mixpanel.queue.PersistentQueueOperation)
extern void U3CU3Ec_U3CReinstateU3Eb__37_0_mCBB99DEC8C9ECD295A8DF11DE805FCC9ED9B0C5C ();
// 0x0000025C mixpanel.queue.PersistentQueueOperation mixpanel.queue.PersistentQueue_<>c::<Reinstate>b__37_1(mixpanel.queue.PersistentQueueOperation)
extern void U3CU3Ec_U3CReinstateU3Eb__37_1_mD52A96913C7A99BAC86F3125947ED3B8740E545D ();
// 0x0000025D System.Void mixpanel.queue.PersistentQueue_<>c::<ReadTransactionLog>b__38_2()
extern void U3CU3Ec_U3CReadTransactionLogU3Eb__38_2_m9D396619BE3FC0B707F2231A4A5F85092F181DE9 ();
// 0x0000025E System.Boolean mixpanel.queue.PersistentQueue_<>c::<ApplyTransactionOperationsInMemory>b__42_0(System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>)
extern void U3CU3Ec_U3CApplyTransactionOperationsInMemoryU3Eb__42_0_mA7A26B9DDD543B2482EFA045F321C7FD0A14E305 ();
// 0x0000025F System.Int32 mixpanel.queue.PersistentQueue_<>c::<ApplyTransactionOperationsInMemory>b__42_1(System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>)
extern void U3CU3Ec_U3CApplyTransactionOperationsInMemoryU3Eb__42_1_m500314484BD87AFD566F2CA81A29CACF24E883CA ();
// 0x00000260 System.Void mixpanel.queue.PersistentQueue_<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_mF3BE6B971EE414563D3A926795612B9FCC60ABAC ();
// 0x00000261 System.Void mixpanel.queue.PersistentQueue_<>c__DisplayClass38_0::<ReadTransactionLog>b__0(System.IO.Stream)
extern void U3CU3Ec__DisplayClass38_0_U3CReadTransactionLogU3Eb__0_m94D5F9520C93079C88B6383B50CFADAF79C1E571 ();
// 0x00000262 System.Void mixpanel.queue.PersistentQueue_<>c__DisplayClass38_1::.ctor()
extern void U3CU3Ec__DisplayClass38_1__ctor_m935637813A6919065A7B59751E3FD7DA972E1081 ();
// 0x00000263 System.Void mixpanel.queue.PersistentQueue_<>c__DisplayClass38_1::<ReadTransactionLog>b__1()
extern void U3CU3Ec__DisplayClass38_1_U3CReadTransactionLogU3Eb__1_m809233E2E75548C019762CF93199AF104078E077 ();
// 0x00000264 System.Void mixpanel.queue.PersistentQueue_<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_mF4CE80F1C5B960611E044C4FCD56C200C2789A99 ();
// 0x00000265 System.Void mixpanel.queue.PersistentQueue_<>c__DisplayClass39_0::<FlushTrimmedTransactionLog>b__0(System.IO.Stream)
extern void U3CU3Ec__DisplayClass39_0_U3CFlushTrimmedTransactionLogU3Eb__0_m86C12BFF7DA82E0E3E00BD8DE7746BD4E96E9E52 ();
// 0x00000266 System.Void mixpanel.queue.PersistentQueueSession_<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_mDB92AB36CC844BB6EC46B22255B0234DD58CDC20 ();
// 0x00000267 System.Void mixpanel.queue.PersistentQueueSession_<>c__DisplayClass15_0::<AsyncWriteToStream>b__0(System.IAsyncResult)
extern void U3CU3Ec__DisplayClass15_0_U3CAsyncWriteToStreamU3Eb__0_m23B9A65D254C30C79EDC2DA9077ABFFE054C1915 ();
static Il2CppMethodPointer s_methodPointers[615] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Config__cctor_m76C873B0B3F866AD5AB6163AFA338B78EC2678EC,
	Controller_InitializeBeforeSceneLoad_m9E41E740A2B4A5CAF737EE2D774F5BC607794921,
	Controller_InitializeAfterSceneLoad_m331099178D617BFC83D2DF7EB27555E51C5CF6F4,
	Controller_GetInstance_mCD620C7F78BE8D8E681C58CE2E5876E6FF48065E,
	Controller_OnDestroy_m60483C6471483CCEDB39E6F11094C686834999B1,
	Controller_OnApplicationPause_m5EF81D2166D169145FCEFA6DE0A4ED2FA0D1CF03,
	Controller_Start_m85877E9FBA4B17C9D82AFB9F5A528F46A966F0A9,
	Controller_TrackIntegrationEvent_mF43CF282CD7BBC35381EC723D29CD02EB921C2F3,
	Controller_WaitForIntegrationRequest_mCD27154B6B5F76B42615CD834804EAC5B9CBF6DE,
	Controller_PopulatePools_mC4895BE19C981E90C810A26996AA4C35282F6D83,
	Controller_MigrateFrom1To2_m6C44D065949722B86359991417850529757B225A,
	Controller_GetEngageDefaultProperties_m6DC0064234C2B567CBDE6D9A08A23F06AD4CC391,
	Controller_GetEventsDefaultProperties_mB811B913107D1901E221D70FED07353FF5BA3906,
	Controller_DoTrack_m53C4A8F3606EB1F87C986F93F1C11ED436AA9105,
	Controller_DoEngage_m068D7F6406673657BB0C89B5CE134AAF29F29BCE,
	Controller_DoFlush_m252908AB55C0016F77234B352402F7BA11F4B626,
	Controller_DoClear_m70187181AB6FEDE7B9CB9F61C2B166C060280A00,
	Controller__ctor_m98F7D8656EFE994E6E5FD632F8DC940840C39BF9,
	NULL,
	Mixpanel_Log_m299A441F3C29E0740A12F5BD98D832E94F195FE7,
	Mixpanel_LogError_mB342A805B3E9E282F31B0DA467F8A7DAC2229E6A,
	Mixpanel_Alias_mD851A043999D6A73BF1B12BD12FD38D72D6D4C65,
	Mixpanel_ClearTimedEvents_mDA20AB2EF921657CC71ABFB08C6780BD146D9CFA,
	Mixpanel_ClearTimedEvent_m4A4EDAD0C1DC7732010DD272B4ED80FDB47FDBF2,
	Mixpanel_Identify_mE1F9403FB085EC360AED39FF0AF033E28B3DD333,
	Mixpanel_get_DistinctID_mB00F357B5B3C9C1E10578A8A288098D3E8E00368,
	Mixpanel_get_DistinctId_m33637E68B66C7D3CDCC9C35FE588CFDB408CC42F,
	Mixpanel_OptOutTracking_m732D3139A9ACAF9EE7A4577BF7D8BBB7ED4BA21A,
	Mixpanel_OptInTracking_m762A2A27B5808A4D35F86E4119FE3933B6D6CB53,
	Mixpanel_OptInTracking_m7F6277AEC48DDE7E7562FB5BA29C66728EFB3BDC,
	Mixpanel_Register_m6E8F1FA5277795BCDE4FAC658AAB85E1A2B564FE,
	Mixpanel_RegisterOnce_m464052763F1123D5D3F2501F4E963252E59B2C0E,
	Mixpanel_Reset_mD7C9EA0B32C77C07CD37F6AA34C9A9A305795E54,
	Mixpanel_Clear_m052A22A5008561F8D6BCAC96ED270D85444C617F,
	Mixpanel_StartTimedEvent_mA716485D9E5254A5ABA97F6394736A39160AC005,
	Mixpanel_StartTimedEventOnce_m49A7CDEBA05DA916DA093FAE6C8BABC05665B9C2,
	Mixpanel_Track_mCBFD38C713C0369905C54450E867444318AE803E,
	Mixpanel_Track_m87834893FE7643F9AD58A23BD02EC49773FA0D77,
	Mixpanel_Track_mD28F4D081033D27062BAED0585AD9BB72E237D6B,
	Mixpanel_Unregister_m03768AD614FACEDD46548554AB0D4C1E97301FAC,
	Mixpanel_Flush_m341FF29FD6E97E3A95190DB6E991E2FD62478B87,
	Mixpanel_SetToken_m83AC5B1CEC50253F130D4DF29FA7B7B02FAAE6E2,
	Mixpanel_Put_mDFD85570E526E4F60F09BC53F14C201C6AEC1C79,
	Mixpanel__cctor_mA662E0675FD800C76A1E6DC128788DB1D840334F,
	MixpanelSettings_get_Token_mCCC83428615584D6E1800DA9CBA6B33BDD4430C7,
	MixpanelSettings_LoadSettings_mCBF0E978B1A479025DD13D0A3C110F00652248EB,
	MixpanelSettings_get_Instance_m7B7BC51EC7413F5BC5BD46C2FA7CC0826120A87A,
	MixpanelSettings_FindOrCreateInstance_mEAEA7748D36F5AEBCBC5A8EBA931D7FACC259A9B,
	NULL,
	MixpanelSettings__ctor_mC0F27A023F7B37D006AA835056AF341576CE1C8B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MixpanelStorage_get_HasMigratedFrom1To2_m072A035552E825DA7C30C08C7786390F5CCAC9D2,
	MixpanelStorage_set_HasMigratedFrom1To2_m4C88CCA7C9E256C8A624AF6BF1C48CED011B3AF3,
	MixpanelStorage_get_HasIntegratedLibrary_m12A4A0A9C134DFD87FB1980E1222404BA988BAA9,
	MixpanelStorage_set_HasIntegratedLibrary_m2621E27E7C01FABB8D826993FF35BC052D56D541,
	MixpanelStorage_get_DistinctId_m7A3290B16574FC3452F5FCCB5CBDD35B1DB4E94A,
	MixpanelStorage_set_DistinctId_m0B4644DF0DC2206EA6B8DBF4A413A1FB7ACC907F,
	MixpanelStorage_get_IsTracking_mC91931B7E94D3887725590CC08A8170795BEABA6,
	MixpanelStorage_set_IsTracking_m0FEE41668052655B7508D6CAD24EEC172223F0B8,
	MixpanelStorage_get_PushDeviceTokenString_m5C88CC3E2570BF835826C63454C5839412D73B1A,
	MixpanelStorage_set_PushDeviceTokenString_m1D553D5802D81774A3CEA3F3561516EAFED71D6C,
	MixpanelStorage_SavePushDeviceToken_m1BFF3FAF64DEEAC1D0B3EB973548F0013AE27394,
	MixpanelStorage_get_OnceProperties_m5AB3C3ADF919B38E50A39719914A5CFB029D9FCD,
	MixpanelStorage_set_OnceProperties_mE81DB961488A0124E9F945A8F82EEC8C7562EB04,
	MixpanelStorage_ResetOnceProperties_m6E73A2E4D4793EE2E5804989E24C029880391B79,
	MixpanelStorage_get_SuperProperties_mAA440F4F304F9B7ADE26549013ED741B20078294,
	MixpanelStorage_set_SuperProperties_m95482C793A955B5163F3CDD9147C772A81515F6A,
	MixpanelStorage_ResetSuperProperties_m825ED696264D16E98C6D2B8C6736375B050DB649,
	MixpanelStorage_get_TimedEvents_m53546B09A5A947FDB35B8240933D37F20D642551,
	MixpanelStorage_set_TimedEvents_m4D439BE6FCE375DF7386461166DF407463DB4988,
	MixpanelStorage_ResetTimedEvents_m94771FC59C303619D62944CD26D260A476AAB52B,
	MixpanelStorage__cctor_m258716BE6636442936F8A537CEA2BFD72076E2FC,
	Value_get_IsNull_m7AFA60AD0836E4908811E93744CA0B94507946E9,
	Value_get_IsArray_mC7777181EE6381C5C21E5A6F64E95559BB95C980,
	Value_get_IsObject_m5BF07983249AE1785CAD765F4C4DB9C47FAE7DE5,
	Value_OnRecycle_m53AE36316A611ABE351F03DE03D8FABF303F9B33,
	Value_get_Item_mC16F70BF027016C13F45A298747A6C606207C2F2,
	Value_set_Item_mDC042E5BDC085E1326BD4808B3FED55517DA4E91,
	Value_get_Item_mD6194F5800B63BED1CBE8A8DDE3FCB40BB82C34A,
	Value_set_Item_m0AB3D98B3053F57091EAECD303BC3CB7FED82060,
	Value_ToString_m5156C794630B6DDEDD908966E519719810CC3FD8,
	Value_GetEnumerator_m157913440E37BDFA7793E60C1AD9B8BDE6043D33,
	Value_get_Count_m04B7EACB5F8EE36FB9C28D08A7812C123DCDC292,
	Value_Contains_m708B2CFE804F117EAB20138F87523C1FAA3A46ED,
	Value_ContainsKey_mEF229323A432A7BF4BAB759FEC089429E40B7A23,
	Value_Add_m4F71B067D552DF74EDEA92F68AEE6A183672B32E,
	Value_Add_m69666C8196ABFF7B2C1C5ADF6546C20237258230,
	Value_Remove_m2DA1502A2ABC3B2960E43EBD0ED43EC713CDE250,
	Value_Remove_m07751B0F284D4D039ED18268A9CB56051760D5B0,
	Value_get_Values_mD689BDC01C23F8884D009714406CDCAE73B9DB11,
	Value_TryGetValue_mD61F77BB3208F1107E2A9AC283B117969F302AD0,
	Value_Merge_m56764985179CFAA092966673F20EFDCF23B3A734,
	Value_get_String_mB05CD0D474E75BC6C342F98FFF1D352951CE98F7,
	Value_set_String_mBB8E20A200A90E8C7DB46E30F4481C718236C984,
	Value_get_Bool_m381241CF04621A315F418BE27F608F7CB5886C29,
	Value_set_Bool_m42E953C596CDFE29041EFC16E05E30E72F382D41,
	Value_get_Number_m72782E5029E6533712EA62FEB55BF8BEBC5566C1,
	Value_set_Number_mA8E0D0FD2DBB40B2E46A97D492D1825AC91CB416,
	Value_get_Uri_m5B3BA01D72EDB5D7510226E42A3025C388E43EAE,
	Value_set_Uri_m9017F2D19F880F4B3BDC2DF9310838C5414EA8F3,
	Value_get_Guid_m7CF58B3FF4B0176A5FD73FF34985B9E2C4C03C16,
	Value_set_Guid_mA773D4E9522F60C4AE1742821B505C7DD995AAF6,
	Value_get_DateTime_m01CFB0861035AA0518E17820A679CC0FAF5D9F88,
	Value_set_DateTime_m9DDD70B5B0651C3B6705DA9A9DBFCF1AAB5A6090,
	Value_get_DateTimeOffset_m87A6904DCE5220E8A596B89DDD87AF94830AC782,
	Value_set_DateTimeOffset_m46296BCB3C986B8562452AD9BBBE49C6348FF9D3,
	Value_get_TimeSpan_m3DB536F2D66304104205101787F597C5F13A01C5,
	Value_set_TimeSpan_m5E5754CD18319240DFF81AAD54761FA59E2B9ACD,
	Value_get_Color_m2B0CCD95EB30B830299F545C1BAB5DB9D589B121,
	Value_set_Color_m615E24548DBE4F27CE1727AC69F687762EE25268,
	Value_get_Color32_m63E5692FAB8E6D185BA78AB0C8FD720F5F14532C,
	Value_set_Color32_mA93609A2CAB4950B037A3F675F2FB63F301A0AE7,
	Value_get_Vector2_mEAB035225FD92ECEB087DDCDCAFC6DFF2F39739B,
	Value_set_Vector2_mFF25A3F2B4B725D1CD29039861FEDFF96ED1AF91,
	Value_get_Vector3_mE3172895E7231FBA354F3DA5118FEFD54AD1C8E7,
	Value_set_Vector3_m55BCB7FA842C62FC250FFB69F0972FE2AC3798E2,
	Value_get_Vector4_mFD212B272DA8C311CBFED955896E03CF72B6C82E,
	Value_set_Vector4_m255FA42336D80DEE5E1B5D439A48B84F310D5200,
	Value_get_Quaternion_mEC9712ADB3DA12CC033F1DF902CC481776E3538E,
	Value_set_Quaternion_m282EF205EA48B24D066211F9366F33A749382407,
	Value_get_Bounds_m52D2D83222613AB79F117FC560B7997A1777AA03,
	Value_set_Bounds_m9D26F712BBCD8CA775284F038962ECA13A25DF55,
	Value_get_Rect_m13BA00EA5649F6C418827EF995F15EAC37692E93,
	Value_set_Rect_mDC158D1B2F3E525AD163910B642DE5272F6AB79C,
	Value__ctor_mB395E4756C596B1F575A9AA2F51B60289DC04110,
	Value__ctor_mD087A88134F6D5F68807892CF4F686915FFCE70B,
	Value__ctor_mFD09BDD374EF187AD4F52894C01062DBB881EAEC,
	Value__ctor_m76B08832F2863379E5EEA375F4005A6D2168C9A4,
	Value__ctor_m19F1CD8073DCE13DF20A4CE83572600D57A0F971,
	Value__ctor_mA63237DB926368CA0D53F04DDE8587B1203FB118,
	Value__ctor_m286C1ED9B22E0AD2F0F871DF4E0A3D3782FB9ADF,
	Value__ctor_m8C56C49BC099ED3326622CAD854BAEEC499D842F,
	Value__ctor_m1776DD5A97AAD025A2E995EBA63E7735C9A162DE,
	Value__ctor_m7A6DC8F60B9850C0756FC362814CF65825737780,
	Value__ctor_m92FDC172360742D8B39251F37AD09A7423F6565C,
	Value__ctor_mB2D25772373B1C0EC5D62BCD245D661D18BCE2DC,
	Value__ctor_m8035B92B43D8CDDFD029943BBC4649DEA16CD966,
	Value__ctor_m524420357C43F17D4734F06A1648CA30A718FD48,
	Value__ctor_mB04DF6C3C8FBABFFB00519D180369945DD58966F,
	Value__ctor_m710C25F3168A60ECFD21F9229359037709926C5C,
	Value__ctor_mE2E0FA0917F24E4E63C3F5A7F171B7834AC06E9D,
	Value__ctor_m24B904799D25E391BC2DE2EAD36191A50B872850,
	Value__ctor_m84BE4F51AAF47B4C51FB5ECB3F7162CAE4EB1935,
	Value__ctor_m67F3A732283F748A2CFB0A986A2AEC079E647461,
	Value_get_Null_mF46ED107BE2AA49B1A0743F064B9B19207CE61E2,
	Value_get_Array_m89FFB9ADE87A5869770FFEDE25C93C080A2C6942,
	Value_get_Object_m45418D4F1FC5E8155A4C90A322214E3FFC8787D8,
	Value_op_Implicit_mF31D114087FFEA9984B3E4F81B41EF7DAF306052,
	Value_op_Implicit_mC36615BBFB5705E9FD1FABEBD7BC901FFED6388D,
	Value_op_Implicit_mC3338A0A28FB37536373E99B799AF444C5E630C8,
	Value_op_Implicit_m5AD3076AB84753271E6AB481D71217D5E24B144B,
	Value_op_Implicit_mE1D2263E72179E6CAD1B97498B9E684518C6BECD,
	Value_op_Implicit_mBB990DB9F838B8E661E4444A2F22597E36F30FAD,
	Value_op_Implicit_m2C1DFE34B581578142514BD4BF76F73485E99CDC,
	Value_op_Implicit_m15A6B5151B5B5400254C3BA24270724E77234433,
	Value_op_Implicit_m60203BCF514733968391DE97E02EF96F263BFEC5,
	Value_op_Implicit_m1F41165645BCAF025DC205F976FEE5B8C29FCD1C,
	Value_op_Implicit_m500596540F61667764870FD1C1A45DB66412AD56,
	Value_op_Implicit_mEDCC0D15532FA9EB9F27D86E316FDE90F73A4CBB,
	Value_op_Implicit_mAB2F3A24E169F44F396FADA907493500E24CA50F,
	Value_op_Implicit_mC5AF8C19803551EC2E4F285595BFE353A3FBEFD2,
	Value_op_Implicit_mB75C0D2B20C740AF5CC4253DC81C20FF2B9E51D1,
	Value_op_Implicit_m4B97073B243AE71FD6DF83BF27036258928AE6D7,
	Value_op_Implicit_m1CB3F9085376E51A77EDB6E6AFC659EE5CCB3CF2,
	Value_op_Implicit_m57419BB271E8FE81508726A3737D82C23841DD63,
	Value_op_Implicit_m3802980A56C0BD2ED6D726EB29F5921F06986391,
	Value_op_Implicit_mCE894BE10E134B530B4DE34213897FDCBF9B0CEE,
	Value_op_Implicit_mA5D8B9420F76EC1D5E1E978E8D9F5A5C0A8CE94E,
	Value_op_Implicit_mB4E0B2C5A47784FCF0DF5DDFE424960836E9C82A,
	Value_op_Implicit_m9F3CE77C07BDA75A13ADD824206BEC78E6F7ABD1,
	Value_op_Implicit_m07ABA94F7789F6813263C4B955D16B7A5556FF78,
	Value_op_Implicit_m164095B3EE2E31F20EE3B16C20344714218A1276,
	Value_op_Implicit_m9F121BFBD5325B07AEE385611494F7B8D4343BDC,
	Value_op_Implicit_m7D8A6AB2CA7051DD6DC90A2650787A16C00766CB,
	Value_op_Implicit_m7E8E0E1BCA62A93717F9D992A142E1AB80E58BD0,
	Value_op_Implicit_m37359F037D1E2B13ED70E7CD9CB28CD508AA3151,
	Value_op_Implicit_m0C56B32902BCFED9BA7D9BA6701378BC081275F9,
	Value_op_Implicit_mAD45F3E966FD66DF09516BF59AB0754ADCB0ADBB,
	Value_op_Implicit_m5DC43F4B91FC10B6EBD03D68AABE04183609679A,
	Value_op_Implicit_m09144AC55FD98C234CCFE0E4D6E101CB2869A9EC,
	Value_op_Implicit_m0A3622830C0EC5AA258A1C4565613C565AA586E3,
	Value_op_Implicit_m645B338E6F9BC710C7A49866E2A9087FC276950F,
	Value_op_Implicit_mCC170AED72159B9579CA51D9DF165466743BCA08,
	Value_op_Implicit_m999589CDFAB7B4A6541EEE79EECD7138A0072029,
	Value_op_Implicit_m1C36D133814068999C2D01D3790EE59A9C14E538,
	Value_op_Implicit_mDD500F10DB50B708D35214CACA0BC7E1692074CB,
	Value_op_Implicit_mB607D9FBA46EE1B3069EC288EB5721AEE4C067DD,
	Value_op_Implicit_mA6F91BA9BE2A60EC86CEFBEF3EB63C95994B07AA,
	Value_op_Implicit_m55594EE9A7BE753B7DC73672B819649044FB6F4F,
	Value_op_Implicit_m825DE729C7F7F8DA9B93F447733A1D2D48CFE7BF,
	Value_op_Implicit_mE1BAEE2C39C980AA8507CF1A89197B4E2CD974FB,
	Value_op_Implicit_m27235A399FA3164CB3A563B72AC20836ECCF7007,
	Value_op_Implicit_mF5B95080211B77032C82EEB2BD460412F5A03576,
	Value_op_Implicit_mF971C2DA44A482AA27F0AC76FA70F7314321760D,
	Value_op_Implicit_m2057146B7F67134A49A9F9254CA6DBC4B3D0D77D,
	Value_op_Implicit_mF5394F113C7405814E487C0E2F8CDB29033C6DD3,
	Value_op_Implicit_mA1C08EA68CC68D18DED454F98FCE64C13A5E663C,
	Value_op_Implicit_m77D256AD495C80A2C6780CF359A4DF56A95DAB89,
	Value_op_Implicit_m755C36308234B9C3EF21999579F6ACF8BD43896C,
	Value_op_Implicit_m0B8BB00FB2A1C7239EB1A7D09B8D5372768F196A,
	Value_op_Implicit_mF019C0BFEAA158BD745EB22BCE5AF8AA3CDF1877,
	Value_op_Implicit_m96E458DEE6E7CD40905FB1DB0A250BCF97535891,
	Value_op_Implicit_m4CD33F1A9CC6F8D08E4911E4AD35941A83FFA418,
	Value_op_Implicit_m2F7FA017CCF00209BBA2724929901C06A4AC9C17,
	Value_op_Implicit_m1401AC9FAC6BC4E236A7D4FB4FE98E16011B28D8,
	Value_op_Implicit_m4EAF78AC3038759BE205219282EEDE782E6D59E5,
	Value_op_Implicit_mA98FA73B3C272EF3ED461EFF494D01270696FF1C,
	Value_op_Implicit_mFAE4578B48CB234AEB86F1959F8A53C4AF79FB4D,
	Value_op_Implicit_mE9272C3FE5DA9A28BC5942D547441A81F5666F39,
	Value_op_Implicit_m1824D49333B9E1AE2D5610D54360462921A7AA8F,
	Value_op_Implicit_mD85D2C91A07F485C89C4A19DA1B8F72FB87D21F7,
	Value_op_Implicit_m66B675FCD738AD25A830C5F756463BEC67FA4425,
	Value_op_Implicit_m20B8CD55AF53826137A7ECC11B4373AFBE551504,
	Value_op_Implicit_mF7A693861D556426171CD43686678DF6272ECAA2,
	Value_op_Implicit_m91CD8770B677563B143D9158491D88DBD80A6C1A,
	Value_op_Implicit_mA06418B3AA6C5BCD211B3AD2810F1D0D44E1675A,
	Value_op_Implicit_mC6A8C7F1DF45680D6F4E8C7EF05F10B44D771461,
	Value_op_Implicit_mB3EFAB828E4F255DC8459B91F75EFA20202935A5,
	Value_op_Implicit_m6E22DCB9CC34FCCE25FF2CB8E1F9A6B3329173C6,
	Value_op_Implicit_mF6FBC19476821AE380A523B680CCA6732442FBF9,
	Value_op_Implicit_m092165AF8E2CE854FF6E4A12E9714068C0F6C37F,
	Value_op_Implicit_mFC8DB3733BA89C63C1B677B2F75E6C98D3DC4329,
	Value_op_Implicit_m9BF54D74D9925EB7E79B5F2B05EE378A44E2C23A,
	Value_op_Implicit_m8F05842558E73AC5BA689F7045DE5C90CA5911F9,
	Value_op_Implicit_mC47F2134D2EA16291330B3ED20CFA03F18E3859B,
	Value_op_Implicit_m8F013BB1F42A611CE3511420D7292FC64AD2B85E,
	Value_op_Implicit_mAD16FCE5EDC02BCDACDFB462EFB27BE3691DA1E3,
	Value_op_Implicit_m8CB0C3BA53E4D0C81342649FEFF1A32C49D0DF64,
	Value_op_Implicit_m5970A975647BCEF6C77519B340C70633A190C3AE,
	Value_op_Implicit_m36560DDF64ACE9CDA43D6838BE097A79AEF73D3C,
	Value_op_Implicit_m19732E2EE16FCE7847A721CE80134B219CD301FF,
	Value_op_Implicit_m3080B7739584D390A4901EE23C849A6ADD89FEE6,
	Value_op_Implicit_mD0E11F258D2CBFE9DD3E6F0FFBBF7DC6D6CAEC2C,
	Value_op_Implicit_mCEF67C742E8EF23995301E1EC7448C8F4CD75024,
	Value_op_Implicit_m402ED57BEBA4F2CFED3FF24A0558EE178357DF7B,
	Value_op_Implicit_m0F1BC675AF23A423342B627089B83C7BAAACBB19,
	Value_op_Implicit_m8E5C690DBABFA9B1B58264CD19CF0573488322F3,
	Value_op_Implicit_m63BDFC16EEC06DE8957EF1B90D69F6099D4BD8A2,
	Value_op_Implicit_m8FB0800225FA14FB0693DD6BE4E841651F8D33AB,
	Value_op_Implicit_mBF86FE88A7C3B4F88C4A3A7E5C55217B052581C1,
	Value_op_Implicit_m4DF02859E66F2ABB41F5D8ABE37120D5DA4B7F02,
	Value_op_Implicit_m91AA3E6A4C67649E145D320B9AC6FFC52993CD2F,
	Value_op_Implicit_mBB5D6A2F31EDF2A9C81E93312EA7CA037DD66357,
	Value_op_Implicit_m9CAE4DB0AFAD3F44543399FE4B4A7638C24B84B7,
	Value_op_Implicit_mB3A668F49EF514028C442271AB4A5E68BE07BA22,
	Value_op_Implicit_m5EEB6A80088E3609870A1C57F6B54784FC6A0865,
	Value_op_Implicit_mC2766FEF14781C339DB6B3CEF6684263BEDDBBFB,
	Value_op_Implicit_mB6F5912AFB1D6BC46DEDC8962CEC6A8338CFCCD5,
	Value_op_Implicit_mE6277D75A4720AC5222F8E335AA62EC974F0222B,
	Value_op_Implicit_m9014BB7C7A00F90911ADBEDE339982EF26A3B2C3,
	Value_op_Implicit_mE07976CD966D6E3FA6292A058BA7BB2EB6C1281D,
	Value_op_Implicit_m7F19F5EFFB82F04576C0AE8C606B730B62AA6840,
	Value_op_Implicit_mF425A79429129C5399C4E77DD757200DF9121583,
	Value_op_Implicit_m3378646FDF49824B7562587DD38A90E13A8A9944,
	Value_op_Implicit_mAA3CB03AFA9B5C858A90DCC9F06F9926C8AE40E6,
	Value_op_Implicit_m353D1997B1A755D001115C4B1C8E4E72B6C44F27,
	Value_op_Implicit_mF3612DCD6520ECB3AD6A94CFA60569A2084D40F7,
	Value_op_Implicit_m0D507544FE170C6862BAE0CC2785393D905F5135,
	Value_op_Implicit_mAD3C0172A27D3488CFD60657B0BA2E9FAA8F6671,
	Value_op_Implicit_m10D3F43A30A53DAF253AA74B399262EC1522E96B,
	Value_op_Implicit_m80BC116C307D7D72AFEC5D5D9BC9FB9F78CAFD4C,
	Value_op_Implicit_m2890A8B7916D7B5766B93F612802EC4F7119081F,
	Value_op_Implicit_mD87D6C638F10DB34EE139CC682FCBE14FC4611A6,
	Value_op_Implicit_m62B4A222C6F2E3478C0F9F69C248F3CCBACEE831,
	Value_op_Implicit_mDABBF5AE2793D6CC2E55ED98A2A7A640146CD998,
	Value_op_Implicit_m4208C9C8782672660B770332E5E5DCE4FD096DB1,
	Value_op_Implicit_m146E27F894240B3E19A1CEB7B3A5CEB8B6EC0EBC,
	Value_op_Implicit_mE4D993BB1A5388AB2F6EC8F0785D3CC0D68F7E90,
	Value_op_Implicit_m897A015CC2952802D30F60ACA2C052A71D66FEB5,
	Value_op_Implicit_mA2AF915DBBCACA6641572D92DC1EAC68EEF8A106,
	Value_op_Implicit_mC167DF65BAE52BF1B8E8BE86F6C422F2CAD28456,
	Value_op_Implicit_mC2A2B6973801511728D594151EC720995DFE16CB,
	Value_op_Implicit_m2AE974254209174388660D932EE20628C444A0DF,
	Value_op_Implicit_m47EBBBBC35C53386175532B5794798677C669831,
	Value_op_Implicit_m734B77D7A66FF6DC6C3A0E379E7F3F46A54962EA,
	Value_op_Implicit_m3216E13274E7669CEF373F8CAA79AE426BCA2912,
	Value_op_Implicit_m7F965A4D5EE5E031274EACC145292F9B5C76ABF6,
	Value_op_Implicit_m15EAC904AD9C74D73B1982119466C22A64DB6389,
	Value_op_Implicit_mAF67E326BAE9128B401706B0527D4C350A21972E,
	Value_op_Implicit_m83DF5FE88B0F4A5FC883184A59EF40708C739BF1,
	Value_op_Implicit_m71C8A1CD020141FACD06DE105FC733E7C59014A1,
	Value_op_Implicit_mEB841B56E501620CF674CA356A394B80CC442D63,
	Value_op_Implicit_m2F2C5A73F63EDB81E47F08C2E88705460D1E56F7,
	Value_op_Implicit_m2297690D3D408FC5BEB3D9CADCA9113CE766D30F,
	Value_op_Implicit_m0303E56CE57BEB39A4A094553483C1081DBC5F3E,
	Value_op_Implicit_mEE989FB011F7D4B5915E510CF9CCB98A42786BDA,
	Value_op_Implicit_mA936FDECA9FCD0CFF83B2319D48DABE036AA130A,
	Value_op_Implicit_m2EB66EE36792654E8A52D21FC4DC03A9F82D97AC,
	Value_op_Implicit_m039FDDC6CE46E2E3655ACFB0364C8E6489DBD1DD,
	Value_op_Implicit_m607F0148734DA7C936A78648D8E2673C0018021E,
	Value_op_Implicit_mE534A948BF1B4CF9EAA7004B43F5F2AA19F345A8,
	Value_op_Implicit_m6DE8B85544FB43557B1AD2EF388A9412E93000A4,
	Value_op_Implicit_m8B340DF650BF6FC76908ECDB3BA7D2A1A317D68B,
	Value_op_Implicit_m5DA88A6250E7FE3745C801DA6760FABFFC6A8A5E,
	Value_op_Implicit_m6DA1504B642C562A60B7947122154C235BE458E3,
	Value_op_Implicit_mB3DD4665F4608A79469492F5A0EEA8CB44A853C8,
	Value_op_Implicit_mF45671FAA436DEDD16DA6A3B9D973C61E475D675,
	Value_op_Implicit_m22D766607BABAB99BB9E4AFD63E5DB779F92358C,
	Value_op_Implicit_mCAA09D5FA67067CA4E130F5B0CD78CC83773788B,
	Value_op_Implicit_mE0A3F40229CA2319DB79C25DF62FF18DC9F67739,
	Value_op_Implicit_mACD08E678B9053C4F5DF133C78BC4944F57F31EF,
	Value_op_Implicit_m566D7A652287277874B5AAC044408F9366DD944C,
	Value_op_Implicit_mD049B1A7923EAE81F19210C3BF164BFAA385A003,
	Value_Write_mBCC75A65036ABDA646D5CB156978E7DDFA69C528,
	Value_SanitizeStringForJson_mCBA1EA599CB3C1859E22106490FCCE94C8200907,
	Value_OnBeforeSerialize_m3F7C3C64EBFD1E47E33BD831A65B3405729666C6,
	Value_SerializeList_m265A062C7CD876E0912BBCE100D79B2E66FCCB43,
	Value_SerializeDictionary_mBE394B113B0108A526688750765782FF40829145,
	Value_OnAfterDeserialize_mE7AFB9A5DE38A8AEE474943FB71C1C866F0AFA63,
	Value_DeserializeList_m57594A41CFF7AD58E40B27DE9509756EB0BF041B,
	Value_DeserializeDictionary_m102D0E96A9E43AB4DE1B8A121D411FCA021506B1,
	Value_Deserialize_mCE8FD1E2B40D6BF3032B964CB59F5BEC527BD6A8,
	Value_ParseValue_m8C695E110BB4D26D6FC0B1FBBB6D666934084ABD,
	Value_ParseByToken_m5D515F2F9CCB525153CD1869EB9FDB76D6186874,
	Value_ParseString_m8D2F74CDA6BED414D2564C8DD92B08F70CD76AC7,
	Value_ParseNumber_m174415820524FC143F7B72AE72FA297BAA55349C,
	Value_ParseArray_mECD8BCF38353CE21CA2749B475E22DF1A5DF6E8A,
	Value_ParseObject_m8D14F6B3BAB7D415594950D5503C2B1607AA2A2D,
	Value_PeekChar_mB289307320C54B04983483A1657F9FBB79944EB6,
	Value_NextChar_m25C313EE3B7A1049BC5076D325B931A1410E041A,
	Value_NextWord_m01F9CA184C22B9F6A51A5FE97E943A171FEAE97A,
	Value_EatWhitespace_m0B765254AA7E3D34885838EB4A0E9C091FF8C440,
	Value_NextToken_mB0D2352BCC7301A6612E75861FB34F4A19634484,
	Value_FromSerialization_mE45BEF7F6768DD876B595FC5F1A3DD2C6408FFD2,
	Worker_StartWorkerThread_m052315380D9457E4CB6A072D5A81527E41EF9193,
	Worker_StopWorkerThread_mC9359090030160BA51ECD293EF900DE129DC9EE5,
	Worker_ForceStop_m15394C3EA8EC0FE4BE5AE5604E21FB6F5EFC8E53,
	Worker_EnqueueEventOp_m8ADAA34953A637B85E546C5E4F9E4C57126F9A66,
	Worker_EnqueuePeopleOp_mBB9D9D68BDCB35AF03450FBE3FC6599066601A00,
	Worker_FlushOp_m6DC1B6E9392F26CFEF035E800078E3BB5B80F67B,
	Worker_ForceFlushOp_m988A7B6CB594EC12013D245A9BC8BA02B5981616,
	Worker_ClearOp_m72B0FC05614D0F94911C22A0FF49D8E8B2B8619C,
	Worker_RunBackgroundThread_m243E2997EE75170521BDBE9EC8E6CC8A206020E1,
	Worker_DispatchOperations_m5B4430C10686EFB7939BC1B69F30C2E2008C3138,
	Worker_SendData_mECF28CC21B3BD79A4CFAF01BA8318796D5FB6070,
	Worker_EnqueueMixpanelQueue_m09AC5B423B62F3F47054F3E451F59390A7575A87,
	Worker__cctor_m73EA7CA1DB905DD2E10B662FA4CEA68CA63A0774,
	Util_CurrentTime_mE9CEBB1787F3D59C92E20B07E72829B63DC4CB22,
	Util_CurrentDateTime_m7C9A38080569FB6D0599A0D1E4C921140D64CA6D,
	Util_GetRadio_m2DBAA8F0ACFCCC47E25AA1E6DB3D0779B7F7B12C,
	Android_GetBrand_m7B271AC1EE09BEBF2DF0B148CE43A3FF9964A0B5,
	Android_GetManufacturer_m20B9F3E02D6625121B8E096993E1E67655AAA1FD,
	Android_GetVersionCode_mEE745C7A5246002A93E0E70C52D4D66EB73CE3B3,
	Android__ctor_mC4AC8FBB13694F2CDF0536CAA7D1351ED50A7EBF,
	NULL,
	PersistentQueue__ctor_m60C9CDF686347FBD1FD18E5252DE9292F336730E,
	PersistentQueue_Finalize_m895F8636F6D1BA9706B6C8996B01A945C254001D,
	PersistentQueue_UnlockQueue_m6F6CDE9BEB4ABD2B2F2BC2700E15611A3179C415,
	PersistentQueue_LockQueue_mCF647CA9CF247616B784A2FEAE7D72D640C78361,
	PersistentQueue_get_CurrentCountOfItemsInQueue_mDA69159F9B8BE2480FBD2DE08BD953A1625532EB,
	PersistentQueue_get_CurrentFilePosition_m7FE5FB4E210F28BFC530D75FA9BE5387F25DBFE5,
	PersistentQueue_set_CurrentFilePosition_mA29BF2DF75C176148670AECBA751DA43617660E1,
	PersistentQueue_get_TransactionLog_m05D66210201E0EF284900AD05F7DD6C9A1433BE9,
	PersistentQueue_get_Meta_mE0C0F40FEA6AB88118B84A56891708D317EE293E,
	PersistentQueue_get_CurrentFileNumber_mEA54919870FD34A7E69FCD761993FB1C592B3986,
	PersistentQueue_set_CurrentFileNumber_m0CB27894872E71361781DFC76A55B2D64F645304,
	PersistentQueue_Dispose_m9867F74DE847563FB6A3500FAA873266B391B29B,
	PersistentQueue_AcquireWriter_mC946B341A323EFB01E23822AF9FC82CA1DFE7BFA,
	PersistentQueue_CommitTransaction_m6F9DB4D286D328A89EFC0687F9EC6BEFF4105F4E,
	PersistentQueue_WaitForTransactionLog_m6AE140BB38431F27E5731F9C2EDF2C2E35164D0E,
	PersistentQueue_Dequeue_m71899BBB07161B055C5337E38FB9084AF015413A,
	PersistentQueue_ReadAhead_m03A8D597DEE101AC6ED5A475A1AA2DE5DCFDD80D,
	PersistentQueue_ReadEntriesFromFile_m8D710E3132A58F2DD0F98EAEF6D6BAF9F59781AE,
	PersistentQueue_OpenSession_mFC3056E0F0A577554DD1C616737DF178509122F5,
	PersistentQueue_Reinstate_mD630C130E071829D27A91175CC9A36C74CC5DEC7,
	PersistentQueue_ReadTransactionLog_m8128DB718F94FB049C9AC28B42B38AF27F128D3D,
	PersistentQueue_FlushTrimmedTransactionLog_mC3FC11484117C9CA3D99D8FDE4DCF7566FD0E840,
	PersistentQueue_WriteEntryToTransactionLog_m9024865AF44B89963661575093909D8B9CA819BC,
	PersistentQueue_AssertOperationSeparator_m346D83803A2A4B1E31B79BFF49B23B9D4298131E,
	PersistentQueue_ApplyTransactionOperationsInMemory_m3D95236137A4307D949AB0A667229F987E76F29D,
	PersistentQueue_AssertTransactionSeparator_m78708D03928BD590ED08A0EBF751B051403A5473,
	PersistentQueue_ReadMetaState_m0CA569D17FF9A33F1F789F44B28AE2A0803FBF56,
	PersistentQueue_TrimTransactionLogIfNeeded_mF69737D35DF580A1A115BD780A87A4491F1AAFF3,
	PersistentQueue_ApplyTransactionOperations_m0A6E07FEBC317628A77FF113DECEDD300F899BA0,
	PersistentQueue_GenerateTransactionBuffer_m7D1FFF1713F789C222438614E1DE2027B1C35802,
	PersistentQueue_CreateWriter_mBFC5EC9CC39F0A9104B9CC666C499268D42160B1,
	PersistentQueue_GetDataPath_m8D96E6DDD94319C5B571F4619F044F8B6FF7A85E,
	PersistentQueue_GetOptimalTransactionLogSize_mBE59C364573BC0739B5C610A8B6528E5BA30854F,
	PersistentQueue_Clear_mDF9B255D59221CCBF6E4CD5C8BF80D1D580B8CCD,
	PersistentQueue__cctor_mB2A8F64702526EA18557133A04C188E4E4119A4D,
	PersistentQueue_U3CCommitTransactionU3Eb__31_0_m4129F0280E92041DC43B33D640F1660FD9955108,
	PersistentQueue_U3CReadMetaStateU3Eb__44_0_m1C5AEE3065D893F16008595351519AF36BF8BFF3,
	PersistentQueueEntry__ctor_mEB206A35F980CD0A8FC898AF39D9CAD87D75EFE8,
	PersistentQueueEntry__ctor_mCBAC51D0FBC1A9BC6D1213B2BF8121A7764D7744,
	PersistentQueueEntry_Equals_m458126F0D5A8BBFECD646B5E2CED80113BFAAF41,
	PersistentQueueEntry_Equals_m2C2C8BEC897BFCFFED93C12A43FFB4813276A185,
	PersistentQueueEntry_GetHashCode_mD369196BDDF534A2A1260723EC9C9948C2D96C08,
	PersistentQueueEntry_op_Equality_mEA0F62EB5F3B72A8C241D0B6A3D5A2B223D0DAB4,
	PersistentQueueEntry_op_Inequality_m5F47B256D218A73B1B90E3494503453D67300D02,
	PersistentQueueOperation__ctor_mA52A128EF311BED74241CE8F393634EC0B61B9A6,
	PersistentQueueSession__ctor_m65837A3111BE6219458716645231F6292DCD4955,
	PersistentQueueSession_Finalize_m0FA33708F84D834BD0825ECAFCDDB06FCB9FF032,
	PersistentQueueSession_Enqueue_m274733E3E48A42C254312808CC94DA03D4D95C50,
	PersistentQueueSession_AsyncFlushBuffer_m8FD2C50C3F6884F965490030895187710E0601A7,
	PersistentQueueSession_SyncFlushBuffer_m2F6D9D269AC0D30E9976A4AC3A09B9A576D196D8,
	PersistentQueueSession_AsyncWriteToStream_m6C9D372CBAD07BC6E4E3A503C3FD6B8733522C33,
	PersistentQueueSession_ConcatenateBufferAndAddIndividualOperations_m1F4735CE325637612B78A6DC6D8F7C871FA21894,
	PersistentQueueSession_OnReplaceStream_m36896D1B59C214C86E1CD371212E227AC32E8B32,
	PersistentQueueSession_Dequeue_m4B5E669B9208A5E3AC4550CD6CC66EF6962A1370,
	PersistentQueueSession_Flush_m07F15FE5C4F66C24A7AF4FCB760185E566245CB2,
	PersistentQueueSession_WaitForPendingWrites_m105E3D4E7EA55B5E947BF87E12A6873E9299BFBE,
	PersistentQueueSession_AssertNoPendingWritesFailures_mEBC84FCA8E6D166C6AB29880C9B76C80F1C426AF,
	PersistentQueueSession_Dispose_mCCAC09F6A1BADB8E297CD0F42ED28178152D0A37,
	PersistentQueueSession__cctor_mD5EB5C1B0B79B18AC77D6EB9897D143BDEC95831,
	PersistentQueueSession_U3CSyncFlushBufferU3Eb__14_0_mBCCA91C92F2723E2773D72FA0BB4778EA83D0FDC,
	PendingWriteException__ctor_m21342C30EFE1762297E1091AEDE3A3EBA2E693C2,
	PendingWriteException_get_Message_m043870E809E0F4F6681CA676EDCF555B95ADB8CF,
	PendingWriteException_ToString_m1CFC670078A1600F567017767577479C8BAA86AD,
	PendingWriteException_GetObjectData_mE397DC37C1116528E2B8278A789B07FC408E4582,
	PersistentQueueUtils_CreateFileStream_m2499F0F23E8AD0EC95EE99007F7B399B2F86C26F,
	PersistentQueueUtils_CreateReadStream_mB2A55B03F4A2A9A35139A87EEDF0520B5FE56981,
	PersistentQueueUtils_CreateWriteStream_mFB0597CC35C0FC6AA0D37C9D58D059B9C79B5A26,
	PersistentQueueUtils_Read_mED8A8E34A21E61A990018FDBC041DFC0F059AFC4,
	PersistentQueueUtils_Write_mD0CD814DFFBA6D65FC4EE712F97F5EFF56B51D18,
	PersistentQueueUtils_WaitDelete_m11EE50B1C6DEE458F410146BAFF11CE1A8AA7195,
	PersistentQueueUtils__cctor_m4AA6BE8D00B1C62A5E75A61DEB9828A27D4C63EE,
	Metadata_InitSession_m8D739B6A2407320AA4A31C39398E52CED3B2EA1F,
	Metadata_GetEventMetadata_mF455A4C21B7097C3C8B6E8F413B7E7BE18D302E8,
	Metadata_GetPeopleMetadata_m714E66E2B7E8001848DBB53CCF99BFEB510645DF,
	Metadata__cctor_m298657D0CDF78F39952F4E58D18EE824996323F6,
	U3CStartU3Ed__8__ctor_m1E5F43D06B79EB58DCA389A10A3F47E26F742B29,
	U3CStartU3Ed__8_System_IDisposable_Dispose_mBE9EFC28242ABDDA2681FF5F770CC6351C6A1CF6,
	U3CStartU3Ed__8_MoveNext_mEFA949E8314919B469946596C9581DAF36B0271D,
	U3CStartU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF983395B30CA3D247879E8E37611DCF9DBB63569,
	U3CStartU3Ed__8_System_Collections_IEnumerator_Reset_mFF6526037D0C10E5487CBCFEB4C81893DDFF292D,
	U3CStartU3Ed__8_System_Collections_IEnumerator_get_Current_mA03EB3CE99BB01261ED4522FE11ACE95FF938B31,
	U3CWaitForIntegrationRequestU3Ed__10__ctor_m189EDB7A9C48C66747ED5B9803BDFF2972084340,
	U3CWaitForIntegrationRequestU3Ed__10_System_IDisposable_Dispose_m1C0105A91134DDBF01CD44A5F29774CBCC5C31E8,
	U3CWaitForIntegrationRequestU3Ed__10_MoveNext_m36AAAAF0BC42FA75B17790A795F5373C3CBC4F02,
	U3CWaitForIntegrationRequestU3Ed__10_System_Collections_Generic_IEnumeratorU3CUnityEngine_Networking_UnityWebRequestU3E_get_Current_m9C550F01B51B3AF87A491D33C156ECDE9F7E9439,
	U3CWaitForIntegrationRequestU3Ed__10_System_Collections_IEnumerator_Reset_m8F384AFDBC723B64E62A800265D0D27D6D2A3324,
	U3CWaitForIntegrationRequestU3Ed__10_System_Collections_IEnumerator_get_Current_m2D5CC25728632EE8E377F917363C32058C50FFE9,
	U3CPopulatePoolsU3Ed__11__ctor_m4ED3FF273F08BBEC2E4263A24B23E7EA1E8C39DB,
	U3CPopulatePoolsU3Ed__11_System_IDisposable_Dispose_m403AB53466FDCECF3D4134980C1D3AC050B1EEFD,
	U3CPopulatePoolsU3Ed__11_MoveNext_mCA08CA7850DC596D7C93E7C1AA06F351D53155AC,
	U3CPopulatePoolsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3EDEAE544386A2C548C709291069CB4C5B102B01,
	U3CPopulatePoolsU3Ed__11_System_Collections_IEnumerator_Reset_m641D565D3CD1EADED236F44B4C8D33975B572B2A,
	U3CPopulatePoolsU3Ed__11_System_Collections_IEnumerator_get_Current_m420AC1E1D95FC863391780415F4DF08076D5B5AC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	People_Append_m1AF128583ECF3A531B45EC8329AFD84850C19640,
	People_Append_m0835FC7506A8BC16EBA57BF4A56134B01C8ABC39,
	People_ClearCharges_mBC2B60DAB8448D007F22D3B2982B8E4E42752029,
	People_DeleteUser_mACE5E17A8236CD2BDF5B3EB1A00F18EE71A8758C,
	People_Increment_m29354447C00251CE8272EE445DE7C0EEC47F1B4F,
	People_Increment_m8D0457939C1F64DC5A90A1EA5CA8EF91A5ED82A8,
	People_Set_m6316E825DD8874E363543D1CFB570CF8DB79E2DA,
	People_Set_m7FE99127F03EE093726B01313099441E461D1014,
	People_SetOnce_mD0A6311734AD35E15FCC9CA050A9112FDEB51283,
	People_SetOnce_m412322D9E02308EB7594B79CF21A434799326941,
	People_TrackCharge_m9B3794A4730BFA67878655D6FEE05A0EB1702309,
	People_TrackCharge_mFD8F836798F4712AC5516C35363D888602DA8CDA,
	People_Union_m80032B0C1A73518A0E19DCCBA62588C25B71DE5B,
	People_Union_mCE8C1C56A269F772BAE4063851E236E4B5F6209D,
	People_Unset_mD3F9A7191EB68FD815F1EFAC9078158A9DAC2467,
	People_set_Email_m32DAEA9AA662B865F15F378B63A6E6E456874257,
	People_set_FirstName_m518DA84D3FA7A6E70A9C3978112F14CD561770AE,
	People_set_LastName_mF5231BC946009D5AB6B4AD73495FF853AB970445,
	People_set_Name_mCDFD6AE18C56D74256A155681F43711232FD7592,
	People_set_PushDeviceToken_m475D5FD38EB5AE28E9BBEEA7081BE8F55C3C0E39,
	U3CU3Ec__cctor_m5A088F65A8921BA9F995D4B99C44C831224B333F,
	U3CU3Ec__ctor_mD4CE4D96A0E149D4546970C4DB4AB1C4C718E2EF,
	U3CU3Ec_U3C_cctorU3Eb__31_0_mFB84FE890A90881B8615D20FFB81CCB56680BA97,
	U3CU3Ec_U3C_cctorU3Eb__31_1_mF17BF1E4F43ED80D16A39D3932BFDA7D308799AD,
	U3CU3Ec_U3C_cctorU3Eb__31_2_m3D61F11AE4E05CAB6968DBF8EB49D8BE680BCBD6,
	U3CU3Ec__cctor_mCF109041E91F6B4D407ED6B21A3FE79D78561CD2,
	U3CU3Ec__ctor_m7D127594D6243247D88507C06510F85CEC4AA9FF,
	U3CU3Ec_U3Cop_ImplicitU3Eb__114_0_mA7A1B3E56F7A884CF38EA7CA34867F8608FB3A1F,
	U3CU3Ec_U3Cop_ImplicitU3Eb__115_0_m0F5E7DEBF74D9A391A3CA41253B419470389D663,
	U3CU3Ec_U3Cop_ImplicitU3Eb__117_0_mF5A3FEA2E067D92B748A40DBBDABCF60895309A0,
	U3CU3Ec_U3Cop_ImplicitU3Eb__118_0_m272F3D1785480530814E4D42356B9E87505AA841,
	U3CU3Ec_U3Cop_ImplicitU3Eb__120_0_mEB2EE4361A2B293911A7CA7F08E9325D365E352F,
	U3CU3Ec_U3Cop_ImplicitU3Eb__121_0_mEAA1E6FDF01DF9ACE19AE0497A597A0B064F8D93,
	U3CU3Ec_U3Cop_ImplicitU3Eb__123_0_m4A1BDCFD3F7F02C2D3770D0BA64AB9F6F0196D73,
	U3CU3Ec_U3Cop_ImplicitU3Eb__124_0_m7BE7B659023FBCDB8F0A881FE4E5B2566B7C6698,
	U3CU3Ec_U3Cop_ImplicitU3Eb__126_0_mA134383B9E97ED27248F8AAD475D0168032553B4,
	U3CU3Ec_U3Cop_ImplicitU3Eb__127_0_m3DEDF631B4038BEC3C4678903844C60D09B6132E,
	U3CU3Ec_U3Cop_ImplicitU3Eb__129_0_m77F2DB53B34FFEB901B0A220C4979C4F822789A9,
	U3CU3Ec_U3Cop_ImplicitU3Eb__130_0_m3E86001AC36299EBE8E29558DF8E53594B8B469D,
	U3CU3Ec_U3Cop_ImplicitU3Eb__132_0_m8066E140CD1DAC8DD263B7752839277953560F49,
	U3CU3Ec_U3Cop_ImplicitU3Eb__133_0_mF3A7D6CD06491A88EFBE447D0F718D2A82D491D7,
	U3CU3Ec_U3Cop_ImplicitU3Eb__135_0_m0ADBCD472002384977572BC0FFC75B628378477F,
	U3CU3Ec_U3Cop_ImplicitU3Eb__136_0_mFEA2D80FBAD1B3A51F5478EBEEDF93E91CC60D8F,
	U3CU3Ec_U3Cop_ImplicitU3Eb__138_0_m364FFE65F5B3E72265FB68EB0B3471435032E4C5,
	U3CU3Ec_U3Cop_ImplicitU3Eb__139_0_m8A28378FCC2FA71213CE6FDA95967EDE37A4A2D4,
	U3CU3Ec_U3Cop_ImplicitU3Eb__141_0_m87840995CB9C0609AF5B53577AAEB8E73BBD6CE9,
	U3CU3Ec_U3Cop_ImplicitU3Eb__142_0_mA710ECAA9511A0F3B923D40444F9707E33A9C089,
	U3CU3Ec_U3Cop_ImplicitU3Eb__144_0_mF0D028217318502B570669C9B653D8E9A5BDCB4E,
	U3CU3Ec_U3Cop_ImplicitU3Eb__145_0_m535617CB88D4E263A8DFDEE8864D6B2925EDB97A,
	U3CU3Ec_U3Cop_ImplicitU3Eb__147_0_m03D24EEEA8E265456DD5BE70CC0E1EDF6067011A,
	U3CU3Ec_U3Cop_ImplicitU3Eb__148_0_m560873732CEBC5F3C100A06EA08895302972D28C,
	U3CU3Ec_U3Cop_ImplicitU3Eb__150_0_m3F16AEE6217C6210782E0843AAA87F1EE45B49B3,
	U3CU3Ec_U3Cop_ImplicitU3Eb__151_0_m905DF442A5FFF7EEA3DFD88F789FBCE50F3D60EF,
	U3CU3Ec_U3Cop_ImplicitU3Eb__153_0_m316F324279FAAC81C1FC8D57EA9E509F9E22A347,
	U3CU3Ec_U3Cop_ImplicitU3Eb__154_0_m4131136D61B47224E9B4B6E3EFCC4FCD1197B2C2,
	U3CU3Ec_U3Cop_ImplicitU3Eb__156_0_m741BDB96B5BA72D3289DEF954ABE9441499E8348,
	U3CU3Ec_U3Cop_ImplicitU3Eb__157_0_mE09021EE9C26EBDC0735A9B3BEE58ECCA33AEDBB,
	U3CU3Ec_U3Cop_ImplicitU3Eb__159_0_m621A2E1F7C26C0E814497D963B2A9CBC730533E0,
	U3CU3Ec_U3Cop_ImplicitU3Eb__160_0_mBE54BA01BB7905C91DFE4110E09087865535336D,
	U3CU3Ec_U3Cop_ImplicitU3Eb__162_0_mAC167B19FB4E0BB9D3DD5CBE76015167C3C00B99,
	U3CU3Ec_U3Cop_ImplicitU3Eb__163_0_m8ACB6600785A13579031E986593523F88ED35EE2,
	U3CU3Ec_U3Cop_ImplicitU3Eb__165_0_m0075866D37C6E6C480C56C402EFBDB77C5ABA5AC,
	U3CU3Ec_U3Cop_ImplicitU3Eb__166_0_m87AEF8D8F0FB4FF220B3A31CC4C04B1EF411EC66,
	U3CU3Ec_U3Cop_ImplicitU3Eb__168_0_mFD585FB22C67E4BA2F72D410FE0C3916F159322F,
	U3CU3Ec_U3Cop_ImplicitU3Eb__169_0_m46EDC645192F3D8263A689E3C88DD9BB53087AF4,
	U3CU3Ec_U3Cop_ImplicitU3Eb__171_0_mD31CA3E205FD5D222672CCFEC6E22D13AA23DE8D,
	U3CU3Ec_U3Cop_ImplicitU3Eb__172_0_mF0182CBE6238ECCF2AF8F7A0CB4809512603A4AA,
	U3CU3Ec_U3Cop_ImplicitU3Eb__174_0_mE3FDFC7332EDC9D63AF51EB8046C31A81403B6C7,
	U3CU3Ec_U3Cop_ImplicitU3Eb__175_0_m9C573F25D8C7F8681AB96E577E70EBF170CD151E,
	U3CU3Ec_U3Cop_ImplicitU3Eb__177_0_m5E289D6CB9D8BAC0F0AA2DCC5C4A3E7D0332B2F3,
	U3CU3Ec_U3Cop_ImplicitU3Eb__178_0_m649F6DD8D5C8CAD165DF4986833B5E82E7D77528,
	U3CU3Ec_U3Cop_ImplicitU3Eb__180_0_m354A3D74B69BF09B027D78F40D16FE7736A7B3C2,
	U3CU3Ec_U3Cop_ImplicitU3Eb__181_0_m7C8E47854D95A76A3C996972EA6DA50C862D6E94,
	U3CU3Ec_U3Cop_ImplicitU3Eb__183_0_mC1B2F2C8D40368718AC288769827AA7A7E6D0089,
	U3CU3Ec_U3Cop_ImplicitU3Eb__184_0_m6C55C8CE48B949C91B11585C344845F70F859F17,
	U3CU3Ec_U3Cop_ImplicitU3Eb__186_0_m510ECD1ADF26216BA768F2BC120BF7D8C2EB66BA,
	U3CU3Ec_U3Cop_ImplicitU3Eb__187_0_m7874F2A0B0ACD500C8408978F2B7F3594488BF46,
	U3CU3Ec_U3Cop_ImplicitU3Eb__189_0_m0DB7C843FA18A110A49C4E6D1F71CAC0F9BF12DF,
	U3CU3Ec_U3Cop_ImplicitU3Eb__190_0_mDDB69181E9D16FD8D53737B76FC8590CC7A51CF3,
	U3CU3Ec_U3Cop_ImplicitU3Eb__192_0_m54A1034632E30FEDE7131DA77E6D1A2A9EF7A152,
	U3CU3Ec_U3Cop_ImplicitU3Eb__193_0_m56BA602BEAAC2B79D5B2197DF558528DA0454998,
	U3CU3Ec_U3Cop_ImplicitU3Eb__195_0_m063C72D3E8368D1253EDAE133415620689E3A302,
	U3CU3Ec_U3Cop_ImplicitU3Eb__196_0_mD309EB60BE7711CCF21AB171BC7D60A7E54A9C23,
	U3CU3Ec_U3Cop_ImplicitU3Eb__198_0_mED966F4DF4AC273E320DA0A483B9E36CA1CB01DC,
	U3CU3Ec_U3Cop_ImplicitU3Eb__199_0_mCDDDB573232075C3BF5E7D5BBA0B0751F54BCCB5,
	U3CU3Ec_U3Cop_ImplicitU3Eb__201_0_mEA7DD45BFF23F15AFF53AC41763274C13B5EDCF6,
	U3CU3Ec_U3Cop_ImplicitU3Eb__202_0_m00D8E72D01E1CDA2D298A7A4A7F69F40AD407518,
	U3CU3Ec_U3Cop_ImplicitU3Eb__204_0_m99EDD55A7BD424E3EB34ECE989381902612B840B,
	U3CU3Ec_U3Cop_ImplicitU3Eb__205_0_m296A583FDEE787A79F87F54373EDEF561BF1FD60,
	U3CU3Ec_U3Cop_ImplicitU3Eb__207_0_m9A391E08D696A9E923CAA7CFB2D485B67EDDB07E,
	U3CU3Ec_U3Cop_ImplicitU3Eb__208_0_m4EBE51062BAD9C1BBE9D4358C8CDD0F4E7773C2C,
	U3CU3Ec_U3Cop_ImplicitU3Eb__210_0_m69B31C8007928E7683A02FC7195C8279D9C97B1B,
	U3CU3Ec_U3Cop_ImplicitU3Eb__211_0_mCDBF77A9B5A626CFAF094A47BF5E5891E1092A6D,
	U3CU3Ec_U3Cop_ImplicitU3Eb__213_0_mE4057A03A3B433AA1317388706FF327A0EBB9969,
	U3CU3Ec_U3Cop_ImplicitU3Eb__214_0_mF8AFCFA7E76C2BD636C0C2C301A8CF2F5D4180F4,
	U3CU3Ec_U3Cop_ImplicitU3Eb__216_0_m6D1F371B27FB64F02C82D1D1F6F5C7333DAD975C,
	U3CU3Ec_U3Cop_ImplicitU3Eb__217_0_m0EE51255D0CFF2D389EF12F9C9CA7AFF2CE4305D,
	U3CU3Ec_U3Cop_ImplicitU3Eb__219_0_mF87F9E0A45022D86FFCEA59FEAF592EADFF14582,
	U3CU3Ec_U3Cop_ImplicitU3Eb__220_0_m77FFB08FF119E36C3B3B06F4A46DCEE015FA3201,
	U3CU3Ec_U3Cop_ImplicitU3Eb__222_0_m8A69D8B16BDD3DACFB953B317AF1FCB956845457,
	U3CU3Ec_U3Cop_ImplicitU3Eb__223_0_mD6127D7AE16580E25CE2E2E5359CA8B5AB4DBA5D,
	U3CU3Ec_U3Cop_ImplicitU3Eb__225_0_m806F495611B4E1E3E9431E98E8E1D110940EA80F,
	U3CU3Ec_U3Cop_ImplicitU3Eb__226_0_mF9C977521E3AF4139ED2414205BC55B96580F979,
	U3CU3Ec_U3Cop_ImplicitU3Eb__228_0_m79BA6C2A73B66B742A76AEAC327AB04D3DA05FBB,
	U3CU3Ec_U3Cop_ImplicitU3Eb__229_0_mC85C99D76B58CFEAAAA557744542E803FDF53D10,
	U3CU3Ec_U3Cop_ImplicitU3Eb__231_0_m5F7EB21D510056D3838B22973BD3B1EE5A698ED8,
	U3CU3Ec_U3Cop_ImplicitU3Eb__232_0_m368F67D37FAA42ED27D30993B0D2D8CAD910CA53,
	U3CU3Ec_U3Cop_ImplicitU3Eb__234_0_mAB569B015E20268AA1578306A3B33F14E6D70981,
	U3CU3Ec_U3Cop_ImplicitU3Eb__235_0_m02682C0EEC02D825D21E6FDD095D246D1F820747,
	U3CU3Ec_U3Cop_ImplicitU3Eb__237_0_mA437377BFABDA51E0F0C54F1FFA554854808D171,
	U3CU3Ec_U3Cop_ImplicitU3Eb__238_0_mA4690A2A267C10D3226300514326337A415C4108,
	U3CU3Ec_U3Cop_ImplicitU3Eb__240_0_mE2AC9CD0CCB7A9999F0C8D677CB6171DCC0800D8,
	U3CU3Ec_U3Cop_ImplicitU3Eb__241_0_mD42EA266A16D0E83AE3A12F542075A9FF4B3A525,
	U3CU3Ec_U3Cop_ImplicitU3Eb__243_0_mB0AA67C9521127BD8D148563BC2157504181C31D,
	U3CU3Ec_U3Cop_ImplicitU3Eb__244_0_mEE2FC20007E8F9827BF2C0E9BA1126263636CD54,
	U3CU3Ec_U3Cop_ImplicitU3Eb__246_0_m28D9CDE9E94927700BE5318124C15F556EFBA58E,
	U3CU3Ec_U3Cop_ImplicitU3Eb__247_0_m5AFA198C9E8CA02793E418620694D9F1092E82A3,
	U3CU3Ec_U3Cop_ImplicitU3Eb__249_0_mD3BA5EB3F95EA1DD1410A7C293ED3AAD3BE29B1B,
	U3CU3Ec_U3Cop_ImplicitU3Eb__250_0_mF5D024023802A95D7341B7952FFF8E4874D2E077,
	U3CU3Ec_U3Cop_ImplicitU3Eb__252_0_m8E137551802405D34ED7140E3B0EC1DBE8B2654B,
	U3CU3Ec_U3Cop_ImplicitU3Eb__253_0_m35B958B154DDE4D04F3D4115CBE56CE316E0EDB2,
	U3CU3Ec_U3Cop_ImplicitU3Eb__255_0_m82B8DDDC7CD001D7900409F7AE9485268F073DF0,
	U3CU3Ec_U3Cop_ImplicitU3Eb__256_0_m6B263F080B40A8CA40CFD6D957D576DF7BA558E4,
	U3CU3Ec_U3Cop_ImplicitU3Eb__258_0_m6BD0CB872E466A54EBD315144F70684986C93E5B,
	U3CU3Ec_U3Cop_ImplicitU3Eb__259_0_m134E5818D7E403459D1A391A2B44CBA83E3451B4,
	U3CU3Ec_U3Cop_ImplicitU3Eb__261_0_m86FD6CF9C561360B6B38C0D4375E3B99C3F178A2,
	U3CU3Ec_U3Cop_ImplicitU3Eb__262_0_m40EDFADE4735E298ADBD1B7DDEFEDCADA48DE2AE,
	U3CU3Ec_U3Cop_ImplicitU3Eb__264_0_m03DA55BF8C773CC1597310696BACD60912E8E38D,
	U3CU3Ec_U3Cop_ImplicitU3Eb__265_0_mDD672F10179D092DE18D04D05BF84D01785AF13A,
	U3CU3Ec_U3Cop_ImplicitU3Eb__267_0_m84A40AD22995360D4F459D819D69970C5DD87BAD,
	U3CU3Ec_U3Cop_ImplicitU3Eb__268_0_mAF6FFBE38627673413AE78F127FF2CE17659D7CA,
	ThreadOperation__ctor_mB00FAF38239AECA1EF3B827F6DBFB9FAA2B8D06F,
	ThreadOperation__ctor_m9176DC01023FCFCA49C58B7D27AA0CC82975B5D0,
	ThreadOperation_GetAction_m667BDC2EA9FAF2F5D62AE3C8873B5A75F330F55C,
	ThreadOperation_GetWhat_m6068B88F0A8EF4E152882573F1290A46C0A28ABA,
	U3CU3Ec__cctor_m6F511E4657E22307FBA3CF52A0ACDE0F16AC7776,
	U3CU3Ec__ctor_mDF3779D82D8F8585269E784685FB991CC073EDE7,
	U3CU3Ec_U3CSendDataU3Eb__18_0_mDA97D86A94AD1327ECC076DF613DB1F0F7A1FFEA,
	U3CSendDataU3Ed__18__ctor_mE39723A866DA28EB86ECE2F843D7F1569D45201C,
	U3CSendDataU3Ed__18_System_IDisposable_Dispose_m8E01C92B4D60AB4813E7C60F2B62F1787949B308,
	U3CSendDataU3Ed__18_MoveNext_mEDE8C09CAC875A5AE7536AAAD2528F6744703B1C,
	U3CSendDataU3Ed__18_U3CU3Em__Finally1_mCDDC26019690D848F1F105F03C3CB3A32B76B344,
	U3CSendDataU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBAE8425D6B1A29B53D5147F3DB28B9F573AF6DD1,
	U3CSendDataU3Ed__18_System_Collections_IEnumerator_Reset_m5241380F229302BA30FF5EDEB199E812A7A33594,
	U3CSendDataU3Ed__18_System_Collections_IEnumerator_get_Current_mA209565953C9739F4D9613D3592DFD65886481C2,
	U3CU3Ec__cctor_m268F61E56FB7E186C29B6D2830EDB6EEC86F0B06,
	U3CU3Ec__ctor_m00FCFC696FF235951542ED821F5BCFB62416EE20,
	U3CU3Ec_U3CReinstateU3Eb__37_0_mCBB99DEC8C9ECD295A8DF11DE805FCC9ED9B0C5C,
	U3CU3Ec_U3CReinstateU3Eb__37_1_mD52A96913C7A99BAC86F3125947ED3B8740E545D,
	U3CU3Ec_U3CReadTransactionLogU3Eb__38_2_m9D396619BE3FC0B707F2231A4A5F85092F181DE9,
	U3CU3Ec_U3CApplyTransactionOperationsInMemoryU3Eb__42_0_mA7A26B9DDD543B2482EFA045F321C7FD0A14E305,
	U3CU3Ec_U3CApplyTransactionOperationsInMemoryU3Eb__42_1_m500314484BD87AFD566F2CA81A29CACF24E883CA,
	U3CU3Ec__DisplayClass38_0__ctor_mF3BE6B971EE414563D3A926795612B9FCC60ABAC,
	U3CU3Ec__DisplayClass38_0_U3CReadTransactionLogU3Eb__0_m94D5F9520C93079C88B6383B50CFADAF79C1E571,
	U3CU3Ec__DisplayClass38_1__ctor_m935637813A6919065A7B59751E3FD7DA972E1081,
	U3CU3Ec__DisplayClass38_1_U3CReadTransactionLogU3Eb__1_m809233E2E75548C019762CF93199AF104078E077,
	U3CU3Ec__DisplayClass39_0__ctor_mF4CE80F1C5B960611E044C4FCD56C200C2789A99,
	U3CU3Ec__DisplayClass39_0_U3CFlushTrimmedTransactionLogU3Eb__0_m86C12BFF7DA82E0E3E00BD8DE7746BD4E96E9E52,
	U3CU3Ec__DisplayClass15_0__ctor_mDB92AB36CC844BB6EC46B22255B0234DD58CDC20,
	U3CU3Ec__DisplayClass15_0_U3CAsyncWriteToStreamU3Eb__0_m23B9A65D254C30C79EDC2DA9077ABFFE054C1915,
};
static const int32_t s_InvokerIndices[615] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3,
	3,
	3,
	4,
	23,
	31,
	14,
	23,
	28,
	4,
	23,
	4,
	4,
	137,
	163,
	3,
	3,
	23,
	-1,
	163,
	163,
	163,
	3,
	163,
	163,
	4,
	4,
	3,
	3,
	163,
	137,
	137,
	3,
	3,
	163,
	163,
	163,
	195,
	137,
	163,
	3,
	163,
	163,
	3,
	14,
	3,
	4,
	4,
	-1,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	49,
	841,
	49,
	841,
	4,
	163,
	49,
	841,
	4,
	163,
	163,
	4,
	163,
	3,
	4,
	163,
	3,
	4,
	163,
	3,
	3,
	89,
	89,
	89,
	23,
	34,
	62,
	28,
	27,
	14,
	14,
	10,
	30,
	9,
	26,
	27,
	32,
	26,
	14,
	915,
	26,
	14,
	26,
	89,
	31,
	452,
	333,
	14,
	26,
	709,
	1018,
	110,
	948,
	1960,
	1864,
	314,
	807,
	1382,
	1383,
	1961,
	1731,
	1362,
	1363,
	1354,
	1355,
	1518,
	1962,
	1509,
	1510,
	1395,
	1396,
	1341,
	1718,
	23,
	129,
	26,
	31,
	333,
	26,
	1018,
	948,
	1864,
	807,
	1383,
	1731,
	1363,
	1355,
	1962,
	1510,
	1396,
	1718,
	26,
	26,
	4,
	4,
	4,
	0,
	0,
	0,
	808,
	0,
	0,
	97,
	0,
	0,
	98,
	0,
	0,
	341,
	0,
	0,
	216,
	0,
	0,
	43,
	0,
	0,
	217,
	0,
	0,
	216,
	0,
	0,
	43,
	0,
	0,
	217,
	0,
	0,
	808,
	0,
	0,
	808,
	0,
	0,
	0,
	0,
	0,
	1963,
	0,
	0,
	1964,
	0,
	0,
	1965,
	0,
	0,
	1966,
	0,
	0,
	1967,
	0,
	0,
	1968,
	0,
	0,
	1969,
	0,
	0,
	1970,
	0,
	0,
	1971,
	0,
	0,
	1972,
	0,
	0,
	1973,
	0,
	0,
	1974,
	0,
	0,
	0,
	0,
	0,
	114,
	0,
	0,
	480,
	0,
	0,
	279,
	0,
	0,
	342,
	0,
	0,
	242,
	0,
	0,
	94,
	0,
	0,
	160,
	0,
	0,
	242,
	0,
	0,
	94,
	0,
	0,
	160,
	0,
	0,
	114,
	0,
	0,
	114,
	0,
	0,
	0,
	0,
	0,
	413,
	0,
	0,
	95,
	0,
	0,
	327,
	0,
	0,
	1975,
	0,
	0,
	1976,
	0,
	0,
	1977,
	0,
	0,
	1526,
	0,
	0,
	1978,
	0,
	0,
	1525,
	0,
	0,
	1979,
	0,
	0,
	1980,
	0,
	0,
	1981,
	0,
	0,
	448,
	0,
	23,
	23,
	23,
	23,
	23,
	23,
	0,
	0,
	119,
	0,
	279,
	0,
	0,
	242,
	242,
	0,
	163,
	94,
	2,
	3,
	3,
	3,
	163,
	163,
	3,
	3,
	3,
	3,
	3,
	1,
	137,
	3,
	2000,
	4,
	4,
	4,
	4,
	106,
	23,
	-1,
	130,
	23,
	23,
	23,
	10,
	181,
	208,
	14,
	14,
	10,
	32,
	23,
	206,
	26,
	28,
	14,
	23,
	2001,
	14,
	26,
	23,
	23,
	109,
	26,
	28,
	1274,
	23,
	208,
	26,
	0,
	14,
	34,
	181,
	23,
	3,
	26,
	26,
	38,
	26,
	9,
	9,
	10,
	135,
	135,
	2004,
	27,
	23,
	26,
	23,
	23,
	227,
	28,
	26,
	14,
	23,
	23,
	23,
	23,
	3,
	227,
	26,
	14,
	14,
	111,
	203,
	0,
	0,
	137,
	137,
	114,
	3,
	3,
	4,
	4,
	3,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	163,
	137,
	3,
	3,
	163,
	137,
	163,
	137,
	163,
	137,
	1959,
	163,
	163,
	137,
	163,
	163,
	163,
	163,
	163,
	163,
	3,
	23,
	14,
	14,
	14,
	3,
	23,
	28,
	28,
	123,
	123,
	1878,
	1878,
	654,
	654,
	1982,
	1982,
	653,
	653,
	34,
	34,
	183,
	183,
	653,
	653,
	34,
	34,
	183,
	183,
	123,
	123,
	123,
	123,
	28,
	28,
	1066,
	1066,
	543,
	543,
	1983,
	1983,
	1984,
	1984,
	1985,
	1985,
	1986,
	1986,
	1987,
	1987,
	1921,
	1921,
	1988,
	1988,
	1989,
	1989,
	1990,
	1990,
	1991,
	1991,
	28,
	28,
	9,
	9,
	228,
	228,
	229,
	229,
	230,
	230,
	226,
	226,
	112,
	112,
	227,
	227,
	226,
	226,
	112,
	112,
	227,
	227,
	9,
	9,
	9,
	9,
	28,
	28,
	1992,
	1992,
	231,
	231,
	1993,
	1993,
	973,
	973,
	1391,
	1391,
	1994,
	1994,
	1693,
	1693,
	1995,
	1995,
	1996,
	1996,
	1997,
	1997,
	1998,
	1998,
	1999,
	1999,
	32,
	62,
	10,
	14,
	3,
	23,
	26,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	3,
	23,
	9,
	28,
	23,
	2002,
	2003,
	23,
	26,
	23,
	23,
	23,
	26,
	23,
	26,
};
static const Il2CppTokenRangePair s_rgctxIndices[7] = 
{
	{ 0x02000002, { 0, 11 } },
	{ 0x02000009, { 38, 7 } },
	{ 0x0200001C, { 26, 1 } },
	{ 0x0200001D, { 27, 10 } },
	{ 0x06000019, { 11, 15 } },
	{ 0x06000037, { 37, 1 } },
	{ 0x06000163, { 45, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[46] = 
{
	{ (Il2CppRGCTXDataType)2, 25547 },
	{ (Il2CppRGCTXDataType)3, 18263 },
	{ (Il2CppRGCTXDataType)2, 25548 },
	{ (Il2CppRGCTXDataType)3, 18264 },
	{ (Il2CppRGCTXDataType)3, 18265 },
	{ (Il2CppRGCTXDataType)2, 25549 },
	{ (Il2CppRGCTXDataType)3, 18266 },
	{ (Il2CppRGCTXDataType)3, 18267 },
	{ (Il2CppRGCTXDataType)3, 18268 },
	{ (Il2CppRGCTXDataType)2, 22389 },
	{ (Il2CppRGCTXDataType)2, 22390 },
	{ (Il2CppRGCTXDataType)2, 25550 },
	{ (Il2CppRGCTXDataType)3, 18269 },
	{ (Il2CppRGCTXDataType)2, 25551 },
	{ (Il2CppRGCTXDataType)3, 18270 },
	{ (Il2CppRGCTXDataType)2, 25552 },
	{ (Il2CppRGCTXDataType)3, 18271 },
	{ (Il2CppRGCTXDataType)3, 18272 },
	{ (Il2CppRGCTXDataType)3, 18273 },
	{ (Il2CppRGCTXDataType)2, 25554 },
	{ (Il2CppRGCTXDataType)3, 18274 },
	{ (Il2CppRGCTXDataType)3, 18275 },
	{ (Il2CppRGCTXDataType)3, 18276 },
	{ (Il2CppRGCTXDataType)2, 25555 },
	{ (Il2CppRGCTXDataType)3, 18277 },
	{ (Il2CppRGCTXDataType)3, 18278 },
	{ (Il2CppRGCTXDataType)3, 18279 },
	{ (Il2CppRGCTXDataType)2, 25557 },
	{ (Il2CppRGCTXDataType)3, 18280 },
	{ (Il2CppRGCTXDataType)2, 25557 },
	{ (Il2CppRGCTXDataType)2, 22421 },
	{ (Il2CppRGCTXDataType)3, 18281 },
	{ (Il2CppRGCTXDataType)3, 18282 },
	{ (Il2CppRGCTXDataType)2, 25558 },
	{ (Il2CppRGCTXDataType)3, 18283 },
	{ (Il2CppRGCTXDataType)3, 18284 },
	{ (Il2CppRGCTXDataType)3, 18285 },
	{ (Il2CppRGCTXDataType)3, 18286 },
	{ (Il2CppRGCTXDataType)2, 25559 },
	{ (Il2CppRGCTXDataType)3, 18287 },
	{ (Il2CppRGCTXDataType)3, 18288 },
	{ (Il2CppRGCTXDataType)3, 18289 },
	{ (Il2CppRGCTXDataType)2, 22443 },
	{ (Il2CppRGCTXDataType)3, 18290 },
	{ (Il2CppRGCTXDataType)3, 18291 },
	{ (Il2CppRGCTXDataType)2, 22548 },
};
extern const Il2CppCodeGenModule g_MixpanelCodeGenModule;
const Il2CppCodeGenModule g_MixpanelCodeGenModule = 
{
	"Mixpanel.dll",
	615,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	7,
	s_rgctxIndices,
	46,
	s_rgctxValues,
	NULL,
};
