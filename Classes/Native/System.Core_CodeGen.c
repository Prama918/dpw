﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B ();
// 0x00000002 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64 ();
// 0x00000003 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712 ();
// 0x00000004 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E ();
// 0x00000005 System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722 ();
// 0x00000006 System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088 ();
// 0x00000007 System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC ();
// 0x00000008 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6 ();
// 0x00000009 System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349 ();
// 0x0000000A System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA ();
// 0x0000000B System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8 ();
// 0x0000000C System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0 ();
// 0x0000000D System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23 ();
// 0x0000000E System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD ();
// 0x0000000F System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1 ();
// 0x00000010 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4 ();
// 0x00000011 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91 ();
// 0x00000012 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED ();
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC ();
// 0x00000014 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146 ();
// 0x00000015 System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E ();
// 0x00000016 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC ();
// 0x00000017 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586 ();
// 0x00000018 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139 ();
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672 ();
// 0x0000001A System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F ();
// 0x0000001B System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C ();
// 0x0000001C System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A ();
// 0x0000001D System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700 ();
// 0x0000001E System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA ();
// 0x0000001F System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B ();
// 0x00000020 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F ();
// 0x00000021 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F ();
// 0x00000022 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA ();
// 0x00000023 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438 ();
// 0x00000024 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22 ();
// 0x00000025 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44 ();
// 0x00000026 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA ();
// 0x00000027 System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F ();
// 0x00000028 System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD ();
// 0x00000029 System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA ();
// 0x0000002A System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24 ();
// 0x0000002B System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA ();
// 0x0000002C System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463 ();
// 0x0000002D System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325 ();
// 0x0000002E System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x0000002F System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 ();
// 0x00000030 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000031 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000032 System.Exception System.Linq.Error::NotSupported()
extern void Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D ();
// 0x00000033 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000034 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000035 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`3<TSource,System.Int32,TResult>)
// 0x00000036 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`3<TSource,System.Int32,TResult>)
// 0x00000037 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000038 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000039 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Take(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000003A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::TakeIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000003B System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003C System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003D System.Collections.Generic.IEnumerable`1<System.Linq.IGrouping`2<TKey,TSource>> System.Linq.Enumerable::GroupBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Reverse(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ReverseIterator(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000040 System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000041 System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000042 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000043 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000044 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000045 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000046 TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000047 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000048 TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000049 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x0000004A System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004B System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004C System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000004D System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000004E TAccumulate System.Linq.Enumerable::Aggregate(System.Collections.Generic.IEnumerable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
// 0x0000004F System.Single System.Linq.Enumerable::Max(System.Collections.Generic.IEnumerable`1<System.Single>)
extern void Enumerable_Max_mBE99D67A0AF1147E3C46DCF7D489129134248262 ();
// 0x00000050 System.Single System.Linq.Enumerable::Average(System.Collections.Generic.IEnumerable`1<System.Single>)
extern void Enumerable_Average_m48238E2FDB29EE702DAD6BBAD1D019D8D4550FFF ();
// 0x00000051 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000052 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000053 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000054 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000055 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000056 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000057 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000058 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000059 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000005A System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000005B System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000005C System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000005D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000005E System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000005F System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000060 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000061 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000062 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000063 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000064 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000065 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000066 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000067 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000068 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000069 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000006A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000006B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000006C System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000006D System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x0000006E System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x0000006F System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000070 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000071 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000072 System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000073 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000074 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000075 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000076 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000077 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000078 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000079 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x0000007A System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000007B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000007C System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::.ctor(System.Int32)
// 0x0000007D System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::System.IDisposable.Dispose()
// 0x0000007E System.Boolean System.Linq.Enumerable_<SelectIterator>d__5`2::MoveNext()
// 0x0000007F System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::<>m__Finally1()
// 0x00000080 TResult System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000081 System.Void System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.IEnumerator.Reset()
// 0x00000082 System.Object System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.IEnumerator.get_Current()
// 0x00000083 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000084 System.Collections.IEnumerator System.Linq.Enumerable_<SelectIterator>d__5`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000085 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000086 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000087 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x00000088 TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000089 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::.ctor(System.Int32)
// 0x0000008A System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::System.IDisposable.Dispose()
// 0x0000008B System.Boolean System.Linq.Enumerable_<TakeIterator>d__25`1::MoveNext()
// 0x0000008C System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::<>m__Finally1()
// 0x0000008D TSource System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000008E System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerator.Reset()
// 0x0000008F System.Object System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerator.get_Current()
// 0x00000090 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000091 System.Collections.IEnumerator System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000092 System.Void System.Linq.Enumerable_<ReverseIterator>d__79`1::.ctor(System.Int32)
// 0x00000093 System.Void System.Linq.Enumerable_<ReverseIterator>d__79`1::System.IDisposable.Dispose()
// 0x00000094 System.Boolean System.Linq.Enumerable_<ReverseIterator>d__79`1::MoveNext()
// 0x00000095 TSource System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000096 System.Void System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.IEnumerator.Reset()
// 0x00000097 System.Object System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.IEnumerator.get_Current()
// 0x00000098 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000099 System.Collections.IEnumerator System.Linq.Enumerable_<ReverseIterator>d__79`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000009A System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x0000009B System.Func`2<TElement,TElement> System.Linq.IdentityFunction`1::get_Instance()
// 0x0000009C System.Void System.Linq.IdentityFunction`1_<>c::.cctor()
// 0x0000009D System.Void System.Linq.IdentityFunction`1_<>c::.ctor()
// 0x0000009E TElement System.Linq.IdentityFunction`1_<>c::<get_Instance>b__1_0(TElement)
// 0x0000009F System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000A0 System.Linq.Lookup`2<TKey,TElement> System.Linq.Lookup`2::Create(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000A1 System.Void System.Linq.Lookup`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000A2 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.Lookup`2::GetEnumerator()
// 0x000000A3 System.Collections.IEnumerator System.Linq.Lookup`2::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A4 System.Int32 System.Linq.Lookup`2::InternalGetHashCode(TKey)
// 0x000000A5 System.Linq.Lookup`2_Grouping<TKey,TElement> System.Linq.Lookup`2::GetGrouping(TKey,System.Boolean)
// 0x000000A6 System.Void System.Linq.Lookup`2::Resize()
// 0x000000A7 System.Void System.Linq.Lookup`2_Grouping::Add(TElement)
// 0x000000A8 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.Lookup`2_Grouping::GetEnumerator()
// 0x000000A9 System.Collections.IEnumerator System.Linq.Lookup`2_Grouping::System.Collections.IEnumerable.GetEnumerator()
// 0x000000AA System.Int32 System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.get_Count()
// 0x000000AB System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.get_IsReadOnly()
// 0x000000AC System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Add(TElement)
// 0x000000AD System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Clear()
// 0x000000AE System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Contains(TElement)
// 0x000000AF System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.CopyTo(TElement[],System.Int32)
// 0x000000B0 System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Remove(TElement)
// 0x000000B1 System.Int32 System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.IndexOf(TElement)
// 0x000000B2 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.Insert(System.Int32,TElement)
// 0x000000B3 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.RemoveAt(System.Int32)
// 0x000000B4 TElement System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.get_Item(System.Int32)
// 0x000000B5 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.set_Item(System.Int32,TElement)
// 0x000000B6 System.Void System.Linq.Lookup`2_Grouping::.ctor()
// 0x000000B7 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::.ctor(System.Int32)
// 0x000000B8 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.IDisposable.Dispose()
// 0x000000B9 System.Boolean System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::MoveNext()
// 0x000000BA TElement System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000BB System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.IEnumerator.Reset()
// 0x000000BC System.Object System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.IEnumerator.get_Current()
// 0x000000BD System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::.ctor(System.Int32)
// 0x000000BE System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::System.IDisposable.Dispose()
// 0x000000BF System.Boolean System.Linq.Lookup`2_<GetEnumerator>d__12::MoveNext()
// 0x000000C0 System.Linq.IGrouping`2<TKey,TElement> System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.Generic.IEnumerator<System.Linq.IGrouping<TKey,TElement>>.get_Current()
// 0x000000C1 System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.IEnumerator.Reset()
// 0x000000C2 System.Object System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.IEnumerator.get_Current()
// 0x000000C3 System.Void System.Linq.GroupedEnumerable`3::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000C4 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.GroupedEnumerable`3::GetEnumerator()
// 0x000000C5 System.Collections.IEnumerator System.Linq.GroupedEnumerable`3::System.Collections.IEnumerable.GetEnumerator()
// 0x000000C6 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x000000C7 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000C8 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000C9 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000CA System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x000000CB System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x000000CC System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x000000CD System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x000000CE TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000CF System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x000000D0 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x000000D1 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000D2 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000D3 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x000000D4 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x000000D5 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x000000D6 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x000000D7 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x000000D8 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x000000D9 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x000000DA System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x000000DB System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x000000DC TElement[] System.Linq.Buffer`1::ToArray()
// 0x000000DD System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x000000DE System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000DF System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000E0 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000E1 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000E2 System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x000000E3 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x000000E4 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x000000E5 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x000000E6 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x000000E7 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x000000E8 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x000000E9 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x000000EA System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x000000EB System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000EC System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000ED System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000EE System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x000000EF System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x000000F0 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000F1 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x000000F2 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x000000F3 System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x000000F4 System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x000000F5 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x000000F6 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000F7 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000F8 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000F9 System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x000000FA System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x000000FB System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000FC System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000FD System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x000000FE System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x000000FF T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x00000100 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000101 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[257] = 
{
	SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B,
	AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64,
	AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712,
	AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E,
	AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722,
	AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088,
	AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC,
	AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6,
	AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349,
	AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA,
	AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8,
	AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0,
	AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23,
	AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD,
	AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1,
	AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4,
	AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91,
	AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED,
	AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC,
	AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146,
	AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E,
	AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC,
	AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586,
	AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139,
	AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672,
	AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F,
	AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C,
	AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A,
	AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700,
	AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA,
	AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B,
	AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F,
	AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F,
	AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA,
	AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438,
	AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22,
	AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44,
	AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA,
	AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F,
	AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD,
	AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA,
	AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24,
	AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA,
	AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463,
	AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325,
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Max_mBE99D67A0AF1147E3C46DCF7D489129134248262,
	Enumerable_Average_m48238E2FDB29EE702DAD6BBAD1D019D8D4550FFF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[257] = 
{
	0,
	23,
	10,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	105,
	14,
	105,
	31,
	23,
	23,
	23,
	23,
	23,
	105,
	105,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	10,
	32,
	10,
	32,
	14,
	14,
	31,
	912,
	27,
	37,
	206,
	206,
	3,
	0,
	0,
	4,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	480,
	480,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[63] = 
{
	{ 0x02000008, { 90, 4 } },
	{ 0x02000009, { 94, 9 } },
	{ 0x0200000A, { 105, 7 } },
	{ 0x0200000B, { 114, 10 } },
	{ 0x0200000C, { 126, 11 } },
	{ 0x0200000D, { 140, 9 } },
	{ 0x0200000E, { 152, 12 } },
	{ 0x0200000F, { 167, 9 } },
	{ 0x02000010, { 176, 1 } },
	{ 0x02000011, { 177, 2 } },
	{ 0x02000012, { 179, 8 } },
	{ 0x02000013, { 187, 6 } },
	{ 0x02000014, { 193, 2 } },
	{ 0x02000015, { 195, 4 } },
	{ 0x02000016, { 199, 3 } },
	{ 0x02000019, { 202, 17 } },
	{ 0x0200001A, { 223, 5 } },
	{ 0x0200001B, { 228, 1 } },
	{ 0x0200001D, { 229, 4 } },
	{ 0x0200001E, { 233, 3 } },
	{ 0x0200001F, { 238, 5 } },
	{ 0x02000020, { 243, 7 } },
	{ 0x02000021, { 250, 3 } },
	{ 0x02000022, { 253, 7 } },
	{ 0x02000023, { 260, 4 } },
	{ 0x02000024, { 264, 34 } },
	{ 0x02000026, { 298, 2 } },
	{ 0x06000033, { 0, 10 } },
	{ 0x06000034, { 10, 10 } },
	{ 0x06000035, { 20, 1 } },
	{ 0x06000036, { 21, 2 } },
	{ 0x06000037, { 23, 5 } },
	{ 0x06000038, { 28, 5 } },
	{ 0x06000039, { 33, 1 } },
	{ 0x0600003A, { 34, 2 } },
	{ 0x0600003B, { 36, 2 } },
	{ 0x0600003C, { 38, 1 } },
	{ 0x0600003D, { 39, 4 } },
	{ 0x0600003E, { 43, 1 } },
	{ 0x0600003F, { 44, 2 } },
	{ 0x06000040, { 46, 1 } },
	{ 0x06000041, { 47, 5 } },
	{ 0x06000042, { 52, 3 } },
	{ 0x06000043, { 55, 2 } },
	{ 0x06000044, { 57, 4 } },
	{ 0x06000045, { 61, 4 } },
	{ 0x06000046, { 65, 4 } },
	{ 0x06000047, { 69, 3 } },
	{ 0x06000048, { 72, 3 } },
	{ 0x06000049, { 75, 1 } },
	{ 0x0600004A, { 76, 1 } },
	{ 0x0600004B, { 77, 3 } },
	{ 0x0600004C, { 80, 2 } },
	{ 0x0600004D, { 82, 5 } },
	{ 0x0600004E, { 87, 3 } },
	{ 0x06000060, { 103, 2 } },
	{ 0x06000065, { 112, 2 } },
	{ 0x0600006A, { 124, 2 } },
	{ 0x06000070, { 137, 3 } },
	{ 0x06000075, { 149, 3 } },
	{ 0x0600007A, { 164, 3 } },
	{ 0x060000A0, { 219, 4 } },
	{ 0x060000C9, { 236, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[300] = 
{
	{ (Il2CppRGCTXDataType)2, 25419 },
	{ (Il2CppRGCTXDataType)3, 17947 },
	{ (Il2CppRGCTXDataType)2, 25420 },
	{ (Il2CppRGCTXDataType)2, 25421 },
	{ (Il2CppRGCTXDataType)3, 17948 },
	{ (Il2CppRGCTXDataType)2, 25422 },
	{ (Il2CppRGCTXDataType)2, 25423 },
	{ (Il2CppRGCTXDataType)3, 17949 },
	{ (Il2CppRGCTXDataType)2, 25424 },
	{ (Il2CppRGCTXDataType)3, 17950 },
	{ (Il2CppRGCTXDataType)2, 25425 },
	{ (Il2CppRGCTXDataType)3, 17951 },
	{ (Il2CppRGCTXDataType)2, 25426 },
	{ (Il2CppRGCTXDataType)2, 25427 },
	{ (Il2CppRGCTXDataType)3, 17952 },
	{ (Il2CppRGCTXDataType)2, 25428 },
	{ (Il2CppRGCTXDataType)2, 25429 },
	{ (Il2CppRGCTXDataType)3, 17953 },
	{ (Il2CppRGCTXDataType)2, 25430 },
	{ (Il2CppRGCTXDataType)3, 17954 },
	{ (Il2CppRGCTXDataType)3, 17955 },
	{ (Il2CppRGCTXDataType)2, 25431 },
	{ (Il2CppRGCTXDataType)3, 17956 },
	{ (Il2CppRGCTXDataType)2, 25432 },
	{ (Il2CppRGCTXDataType)3, 17957 },
	{ (Il2CppRGCTXDataType)3, 17958 },
	{ (Il2CppRGCTXDataType)2, 19177 },
	{ (Il2CppRGCTXDataType)3, 17959 },
	{ (Il2CppRGCTXDataType)2, 25433 },
	{ (Il2CppRGCTXDataType)3, 17960 },
	{ (Il2CppRGCTXDataType)3, 17961 },
	{ (Il2CppRGCTXDataType)2, 19184 },
	{ (Il2CppRGCTXDataType)3, 17962 },
	{ (Il2CppRGCTXDataType)3, 17963 },
	{ (Il2CppRGCTXDataType)2, 25434 },
	{ (Il2CppRGCTXDataType)3, 17964 },
	{ (Il2CppRGCTXDataType)2, 25435 },
	{ (Il2CppRGCTXDataType)3, 17965 },
	{ (Il2CppRGCTXDataType)3, 17966 },
	{ (Il2CppRGCTXDataType)3, 17967 },
	{ (Il2CppRGCTXDataType)2, 25436 },
	{ (Il2CppRGCTXDataType)2, 25437 },
	{ (Il2CppRGCTXDataType)3, 17968 },
	{ (Il2CppRGCTXDataType)3, 17969 },
	{ (Il2CppRGCTXDataType)2, 25438 },
	{ (Il2CppRGCTXDataType)3, 17970 },
	{ (Il2CppRGCTXDataType)3, 17971 },
	{ (Il2CppRGCTXDataType)3, 17972 },
	{ (Il2CppRGCTXDataType)2, 25439 },
	{ (Il2CppRGCTXDataType)2, 19210 },
	{ (Il2CppRGCTXDataType)2, 25440 },
	{ (Il2CppRGCTXDataType)2, 19212 },
	{ (Il2CppRGCTXDataType)2, 25441 },
	{ (Il2CppRGCTXDataType)3, 17973 },
	{ (Il2CppRGCTXDataType)3, 17974 },
	{ (Il2CppRGCTXDataType)2, 19218 },
	{ (Il2CppRGCTXDataType)3, 17975 },
	{ (Il2CppRGCTXDataType)2, 25442 },
	{ (Il2CppRGCTXDataType)2, 25443 },
	{ (Il2CppRGCTXDataType)2, 19219 },
	{ (Il2CppRGCTXDataType)2, 25444 },
	{ (Il2CppRGCTXDataType)2, 25445 },
	{ (Il2CppRGCTXDataType)2, 25446 },
	{ (Il2CppRGCTXDataType)2, 19221 },
	{ (Il2CppRGCTXDataType)2, 25447 },
	{ (Il2CppRGCTXDataType)2, 25448 },
	{ (Il2CppRGCTXDataType)2, 25449 },
	{ (Il2CppRGCTXDataType)2, 19223 },
	{ (Il2CppRGCTXDataType)2, 25450 },
	{ (Il2CppRGCTXDataType)2, 19225 },
	{ (Il2CppRGCTXDataType)2, 25451 },
	{ (Il2CppRGCTXDataType)3, 17976 },
	{ (Il2CppRGCTXDataType)2, 25452 },
	{ (Il2CppRGCTXDataType)2, 19228 },
	{ (Il2CppRGCTXDataType)2, 25453 },
	{ (Il2CppRGCTXDataType)2, 25454 },
	{ (Il2CppRGCTXDataType)2, 19232 },
	{ (Il2CppRGCTXDataType)2, 19234 },
	{ (Il2CppRGCTXDataType)2, 25455 },
	{ (Il2CppRGCTXDataType)3, 17977 },
	{ (Il2CppRGCTXDataType)2, 25456 },
	{ (Il2CppRGCTXDataType)3, 17978 },
	{ (Il2CppRGCTXDataType)3, 17979 },
	{ (Il2CppRGCTXDataType)2, 25457 },
	{ (Il2CppRGCTXDataType)2, 19239 },
	{ (Il2CppRGCTXDataType)2, 25458 },
	{ (Il2CppRGCTXDataType)2, 19241 },
	{ (Il2CppRGCTXDataType)2, 19242 },
	{ (Il2CppRGCTXDataType)2, 25459 },
	{ (Il2CppRGCTXDataType)3, 17980 },
	{ (Il2CppRGCTXDataType)3, 17981 },
	{ (Il2CppRGCTXDataType)3, 17982 },
	{ (Il2CppRGCTXDataType)2, 19248 },
	{ (Il2CppRGCTXDataType)3, 17983 },
	{ (Il2CppRGCTXDataType)3, 17984 },
	{ (Il2CppRGCTXDataType)2, 19260 },
	{ (Il2CppRGCTXDataType)2, 25460 },
	{ (Il2CppRGCTXDataType)3, 17985 },
	{ (Il2CppRGCTXDataType)3, 17986 },
	{ (Il2CppRGCTXDataType)2, 19262 },
	{ (Il2CppRGCTXDataType)2, 25283 },
	{ (Il2CppRGCTXDataType)3, 17987 },
	{ (Il2CppRGCTXDataType)3, 17988 },
	{ (Il2CppRGCTXDataType)2, 25461 },
	{ (Il2CppRGCTXDataType)3, 17989 },
	{ (Il2CppRGCTXDataType)3, 17990 },
	{ (Il2CppRGCTXDataType)2, 19272 },
	{ (Il2CppRGCTXDataType)2, 25462 },
	{ (Il2CppRGCTXDataType)3, 17991 },
	{ (Il2CppRGCTXDataType)3, 17992 },
	{ (Il2CppRGCTXDataType)3, 17375 },
	{ (Il2CppRGCTXDataType)3, 17993 },
	{ (Il2CppRGCTXDataType)2, 25463 },
	{ (Il2CppRGCTXDataType)3, 17994 },
	{ (Il2CppRGCTXDataType)3, 17995 },
	{ (Il2CppRGCTXDataType)2, 19284 },
	{ (Il2CppRGCTXDataType)2, 25464 },
	{ (Il2CppRGCTXDataType)3, 17996 },
	{ (Il2CppRGCTXDataType)3, 17997 },
	{ (Il2CppRGCTXDataType)3, 17998 },
	{ (Il2CppRGCTXDataType)3, 17999 },
	{ (Il2CppRGCTXDataType)3, 18000 },
	{ (Il2CppRGCTXDataType)3, 17381 },
	{ (Il2CppRGCTXDataType)3, 18001 },
	{ (Il2CppRGCTXDataType)2, 25465 },
	{ (Il2CppRGCTXDataType)3, 18002 },
	{ (Il2CppRGCTXDataType)3, 18003 },
	{ (Il2CppRGCTXDataType)2, 19297 },
	{ (Il2CppRGCTXDataType)2, 25466 },
	{ (Il2CppRGCTXDataType)3, 18004 },
	{ (Il2CppRGCTXDataType)3, 18005 },
	{ (Il2CppRGCTXDataType)2, 19299 },
	{ (Il2CppRGCTXDataType)2, 25467 },
	{ (Il2CppRGCTXDataType)3, 18006 },
	{ (Il2CppRGCTXDataType)3, 18007 },
	{ (Il2CppRGCTXDataType)2, 25468 },
	{ (Il2CppRGCTXDataType)3, 18008 },
	{ (Il2CppRGCTXDataType)3, 18009 },
	{ (Il2CppRGCTXDataType)2, 25469 },
	{ (Il2CppRGCTXDataType)3, 18010 },
	{ (Il2CppRGCTXDataType)3, 18011 },
	{ (Il2CppRGCTXDataType)2, 19314 },
	{ (Il2CppRGCTXDataType)2, 25470 },
	{ (Il2CppRGCTXDataType)3, 18012 },
	{ (Il2CppRGCTXDataType)3, 18013 },
	{ (Il2CppRGCTXDataType)3, 18014 },
	{ (Il2CppRGCTXDataType)3, 17392 },
	{ (Il2CppRGCTXDataType)2, 25471 },
	{ (Il2CppRGCTXDataType)3, 18015 },
	{ (Il2CppRGCTXDataType)3, 18016 },
	{ (Il2CppRGCTXDataType)2, 25472 },
	{ (Il2CppRGCTXDataType)3, 18017 },
	{ (Il2CppRGCTXDataType)3, 18018 },
	{ (Il2CppRGCTXDataType)2, 19330 },
	{ (Il2CppRGCTXDataType)2, 25473 },
	{ (Il2CppRGCTXDataType)3, 18019 },
	{ (Il2CppRGCTXDataType)3, 18020 },
	{ (Il2CppRGCTXDataType)3, 18021 },
	{ (Il2CppRGCTXDataType)3, 18022 },
	{ (Il2CppRGCTXDataType)3, 18023 },
	{ (Il2CppRGCTXDataType)3, 18024 },
	{ (Il2CppRGCTXDataType)3, 17398 },
	{ (Il2CppRGCTXDataType)2, 25474 },
	{ (Il2CppRGCTXDataType)3, 18025 },
	{ (Il2CppRGCTXDataType)3, 18026 },
	{ (Il2CppRGCTXDataType)2, 25475 },
	{ (Il2CppRGCTXDataType)3, 18027 },
	{ (Il2CppRGCTXDataType)3, 18028 },
	{ (Il2CppRGCTXDataType)2, 25476 },
	{ (Il2CppRGCTXDataType)2, 25477 },
	{ (Il2CppRGCTXDataType)3, 18029 },
	{ (Il2CppRGCTXDataType)3, 18030 },
	{ (Il2CppRGCTXDataType)2, 19347 },
	{ (Il2CppRGCTXDataType)2, 25478 },
	{ (Il2CppRGCTXDataType)3, 18031 },
	{ (Il2CppRGCTXDataType)3, 18032 },
	{ (Il2CppRGCTXDataType)3, 18033 },
	{ (Il2CppRGCTXDataType)3, 18034 },
	{ (Il2CppRGCTXDataType)3, 18035 },
	{ (Il2CppRGCTXDataType)3, 18036 },
	{ (Il2CppRGCTXDataType)2, 19377 },
	{ (Il2CppRGCTXDataType)2, 19372 },
	{ (Il2CppRGCTXDataType)3, 18037 },
	{ (Il2CppRGCTXDataType)2, 19371 },
	{ (Il2CppRGCTXDataType)2, 25479 },
	{ (Il2CppRGCTXDataType)3, 18038 },
	{ (Il2CppRGCTXDataType)3, 18039 },
	{ (Il2CppRGCTXDataType)2, 25480 },
	{ (Il2CppRGCTXDataType)3, 18040 },
	{ (Il2CppRGCTXDataType)2, 19381 },
	{ (Il2CppRGCTXDataType)2, 25481 },
	{ (Il2CppRGCTXDataType)3, 18041 },
	{ (Il2CppRGCTXDataType)3, 18042 },
	{ (Il2CppRGCTXDataType)2, 25482 },
	{ (Il2CppRGCTXDataType)2, 25483 },
	{ (Il2CppRGCTXDataType)2, 25484 },
	{ (Il2CppRGCTXDataType)3, 18043 },
	{ (Il2CppRGCTXDataType)2, 19395 },
	{ (Il2CppRGCTXDataType)3, 18044 },
	{ (Il2CppRGCTXDataType)2, 25485 },
	{ (Il2CppRGCTXDataType)3, 18045 },
	{ (Il2CppRGCTXDataType)2, 25485 },
	{ (Il2CppRGCTXDataType)2, 19423 },
	{ (Il2CppRGCTXDataType)3, 18046 },
	{ (Il2CppRGCTXDataType)3, 18047 },
	{ (Il2CppRGCTXDataType)3, 18048 },
	{ (Il2CppRGCTXDataType)3, 18049 },
	{ (Il2CppRGCTXDataType)2, 25486 },
	{ (Il2CppRGCTXDataType)2, 25487 },
	{ (Il2CppRGCTXDataType)2, 25488 },
	{ (Il2CppRGCTXDataType)3, 18050 },
	{ (Il2CppRGCTXDataType)3, 18051 },
	{ (Il2CppRGCTXDataType)2, 19419 },
	{ (Il2CppRGCTXDataType)2, 19422 },
	{ (Il2CppRGCTXDataType)3, 18052 },
	{ (Il2CppRGCTXDataType)3, 18053 },
	{ (Il2CppRGCTXDataType)2, 19426 },
	{ (Il2CppRGCTXDataType)3, 18054 },
	{ (Il2CppRGCTXDataType)2, 25489 },
	{ (Il2CppRGCTXDataType)2, 19416 },
	{ (Il2CppRGCTXDataType)2, 25490 },
	{ (Il2CppRGCTXDataType)3, 18055 },
	{ (Il2CppRGCTXDataType)3, 18056 },
	{ (Il2CppRGCTXDataType)3, 18057 },
	{ (Il2CppRGCTXDataType)2, 25491 },
	{ (Il2CppRGCTXDataType)3, 18058 },
	{ (Il2CppRGCTXDataType)3, 18059 },
	{ (Il2CppRGCTXDataType)3, 18060 },
	{ (Il2CppRGCTXDataType)2, 19441 },
	{ (Il2CppRGCTXDataType)3, 18061 },
	{ (Il2CppRGCTXDataType)2, 25492 },
	{ (Il2CppRGCTXDataType)3, 18062 },
	{ (Il2CppRGCTXDataType)3, 18063 },
	{ (Il2CppRGCTXDataType)2, 25493 },
	{ (Il2CppRGCTXDataType)3, 18064 },
	{ (Il2CppRGCTXDataType)3, 18065 },
	{ (Il2CppRGCTXDataType)2, 25494 },
	{ (Il2CppRGCTXDataType)3, 18066 },
	{ (Il2CppRGCTXDataType)2, 25495 },
	{ (Il2CppRGCTXDataType)3, 18067 },
	{ (Il2CppRGCTXDataType)3, 18068 },
	{ (Il2CppRGCTXDataType)3, 18069 },
	{ (Il2CppRGCTXDataType)2, 19488 },
	{ (Il2CppRGCTXDataType)3, 18070 },
	{ (Il2CppRGCTXDataType)2, 19496 },
	{ (Il2CppRGCTXDataType)3, 18071 },
	{ (Il2CppRGCTXDataType)2, 25496 },
	{ (Il2CppRGCTXDataType)2, 25497 },
	{ (Il2CppRGCTXDataType)3, 18072 },
	{ (Il2CppRGCTXDataType)3, 18073 },
	{ (Il2CppRGCTXDataType)3, 18074 },
	{ (Il2CppRGCTXDataType)3, 18075 },
	{ (Il2CppRGCTXDataType)3, 18076 },
	{ (Il2CppRGCTXDataType)3, 18077 },
	{ (Il2CppRGCTXDataType)2, 19512 },
	{ (Il2CppRGCTXDataType)2, 25498 },
	{ (Il2CppRGCTXDataType)3, 18078 },
	{ (Il2CppRGCTXDataType)3, 18079 },
	{ (Il2CppRGCTXDataType)2, 19516 },
	{ (Il2CppRGCTXDataType)3, 18080 },
	{ (Il2CppRGCTXDataType)2, 25499 },
	{ (Il2CppRGCTXDataType)2, 19526 },
	{ (Il2CppRGCTXDataType)2, 19524 },
	{ (Il2CppRGCTXDataType)2, 25500 },
	{ (Il2CppRGCTXDataType)3, 18081 },
	{ (Il2CppRGCTXDataType)2, 25501 },
	{ (Il2CppRGCTXDataType)3, 18082 },
	{ (Il2CppRGCTXDataType)3, 18083 },
	{ (Il2CppRGCTXDataType)2, 19533 },
	{ (Il2CppRGCTXDataType)3, 18084 },
	{ (Il2CppRGCTXDataType)2, 19533 },
	{ (Il2CppRGCTXDataType)3, 18085 },
	{ (Il2CppRGCTXDataType)2, 19550 },
	{ (Il2CppRGCTXDataType)3, 18086 },
	{ (Il2CppRGCTXDataType)3, 18087 },
	{ (Il2CppRGCTXDataType)3, 18088 },
	{ (Il2CppRGCTXDataType)2, 25502 },
	{ (Il2CppRGCTXDataType)3, 18089 },
	{ (Il2CppRGCTXDataType)3, 18090 },
	{ (Il2CppRGCTXDataType)3, 18091 },
	{ (Il2CppRGCTXDataType)2, 19530 },
	{ (Il2CppRGCTXDataType)3, 18092 },
	{ (Il2CppRGCTXDataType)3, 18093 },
	{ (Il2CppRGCTXDataType)2, 19535 },
	{ (Il2CppRGCTXDataType)3, 18094 },
	{ (Il2CppRGCTXDataType)1, 25503 },
	{ (Il2CppRGCTXDataType)2, 19534 },
	{ (Il2CppRGCTXDataType)3, 18095 },
	{ (Il2CppRGCTXDataType)1, 19534 },
	{ (Il2CppRGCTXDataType)1, 19530 },
	{ (Il2CppRGCTXDataType)2, 25502 },
	{ (Il2CppRGCTXDataType)2, 19534 },
	{ (Il2CppRGCTXDataType)2, 19532 },
	{ (Il2CppRGCTXDataType)2, 19536 },
	{ (Il2CppRGCTXDataType)3, 18096 },
	{ (Il2CppRGCTXDataType)3, 18097 },
	{ (Il2CppRGCTXDataType)3, 18098 },
	{ (Il2CppRGCTXDataType)2, 19531 },
	{ (Il2CppRGCTXDataType)3, 18099 },
	{ (Il2CppRGCTXDataType)2, 19546 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	257,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	63,
	s_rgctxIndices,
	300,
	s_rgctxValues,
	NULL,
};
