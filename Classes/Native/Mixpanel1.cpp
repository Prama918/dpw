﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct VirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
struct VirtFuncInvoker5
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// Microsoft.Win32.SafeHandles.SafeFileHandle
struct SafeFileHandle_tE1B31BE63CD11BBF2B9B6A205A72735F32EB1BCB;
// Microsoft.Win32.SafeHandles.SafeWaitHandle
struct SafeWaitHandle_t51DB35FF382E636FF3B868D87816733894D46CF2;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.IO.Stream>
struct Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Byte[][]
struct ByteU5BU5DU5BU5D_t1DE3927D87FD236507BFE9CA7E3EEA348C53E0E1;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t6567430A4033E968FED88FBBD298DC9D0DFA398F;
// System.Collections.Generic.HashSet`1<mixpanel.queue.PersistentQueueEntry>
struct HashSet_1_t172DF6846FC334649B0EA6BADF143BA53D0E03F3;
// System.Collections.Generic.ICollection`1<mixpanel.queue.PersistentQueueOperation>
struct ICollection_1_t3C665615DA92DBC197109FCFEBE41EAC8708CCF9;
// System.Collections.Generic.IEnumerable`1<System.Exception>
struct IEnumerable_1_t6B3D33CE5B4DC884E5267C7B8CBC12380D09B3F1;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2F75FCBEC68AFE08982DA43985F9D04056E2BE73;
// System.Collections.Generic.IEnumerable`1<System.Threading.WaitHandle>
struct IEnumerable_1_tCC6BCEC8B4813D2AE600A981DD8A18678FBCD093;
// System.Collections.Generic.IEnumerable`1<mixpanel.queue.PersistentQueueOperation>
struct IEnumerable_1_t2868DE6B2D3FEC305B19B13FF5EA606293FAC008;
// System.Collections.Generic.IList`1<System.Exception>
struct IList_1_t79A79A556E69BA20A09771D2D61B0440B6F4EFBA;
// System.Collections.Generic.IList`1<System.Threading.WaitHandle>
struct IList_1_t7D86688D8B8BA10DAD266FB4DFB20CA3855DFC74;
// System.Collections.Generic.LinkedList`1<mixpanel.queue.PersistentQueueEntry>
struct LinkedList_1_t7C6A05BAC60F7CAB159B06575F4827077E31F562;
// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531;
// System.Collections.Generic.List`1<System.Exception>
struct List_1_t864E43C85FFFD16A2A0685F30AC877138E106F2A;
// System.Collections.Generic.List`1<System.IO.Stream>
struct List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<System.Threading.WaitHandle>
struct List_1_tE87EDEDF7A49228B90A7C51CFFD8114D62D5EF30;
// System.Collections.Generic.List`1<mixpanel.queue.PersistentQueueOperation>
struct List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Exception
struct Exception_t;
// System.Exception[]
struct ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Boolean>
struct Func_2_tBD3BF5B8A6857D5E6CFA087C76B05A52E76F2FB2;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>
struct Func_2_t629E2A2E190281BBB9863A1CAB74C21975B59EE0;
// System.Func`2<System.IO.Stream,System.Int64>
struct Func_2_t4D4D87A23A55C4AC43EDC08F16D4C8A88C4748A7;
// System.Func`2<System.Object,System.Int64>
struct Func_2_t82B378444729B3A0021330940E7867B6D348F383;
// System.Func`2<mixpanel.queue.PersistentQueueOperation,System.Boolean>
struct Func_2_tFCA57E92A32141A0BF9637E0EABE4F37E2B4FB6D;
// System.Func`2<mixpanel.queue.PersistentQueueOperation,mixpanel.queue.PersistentQueueOperation>
struct Func_2_tB4C66CF15D7698C4C0618C6D1C23F362CD6ED754;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.BinaryReader
struct BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969;
// System.IO.FileStream
struct FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.IO.Stream[]
struct StreamU5BU5D_tDADB5390E6C9A2BDA8A341EECC9F7F9DE08DB362;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.String
struct String_t;
// System.Text.Decoder
struct Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26;
// System.Threading.EventWaitHandle
struct EventWaitHandle_t7603BF1D3D30FE42DD07A450C8D09E2684DC4D98;
// System.Threading.ManualResetEvent
struct ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Threading.WaitHandle
struct WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6;
// System.Threading.WaitHandle[]
struct WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// mixpanel.queue.PendingWriteException
struct PendingWriteException_tD008EED8A9DD4BB29844EF57D33275EE9A882F48;
// mixpanel.queue.PersistentQueue
struct PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26;
// mixpanel.queue.PersistentQueue/<>c
struct U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591;
// mixpanel.queue.PersistentQueue/<>c__DisplayClass38_0
struct U3CU3Ec__DisplayClass38_0_t151099A4060264DDAAECA19075B7DD121A8DB2B7;
// mixpanel.queue.PersistentQueue/<>c__DisplayClass38_1
struct U3CU3Ec__DisplayClass38_1_t51468321D6041881A73633BB17566E2FDE8E4269;
// mixpanel.queue.PersistentQueue/<>c__DisplayClass39_0
struct U3CU3Ec__DisplayClass39_0_t24AB86C76478BA26F0821541C9174F6766DE1508;
// mixpanel.queue.PersistentQueueEntry
struct PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA;
// mixpanel.queue.PersistentQueueOperation
struct PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69;
// mixpanel.queue.PersistentQueueOperation[]
struct PersistentQueueOperationU5BU5D_tC47ACD37FB31D1AD1F14C680DAF3E142D97DE085;
// mixpanel.queue.PersistentQueueSession
struct PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359;
// mixpanel.queue.PersistentQueueSession/<>c__DisplayClass15_0
struct U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C;

IL2CPP_EXTERN_C RuntimeClass* Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BitConverter_tD5DF1CB5C5A5CB087D90BD881C8E75A332E546EE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EndOfStreamException_t1B47BA867EC337F83056C2833A59293754AAC01F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t4D4D87A23A55C4AC43EDC08F16D4C8A88C4748A7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GC_tC1D7BD74E8F44ECCEF5CD2B5D84BFF9AAE02D01D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ICollection_1_t141497430FA8C150A49A8B04E2C6476230A8D741_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ICollection_1_t369F219933FB866C23070CD9749538883440F044_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t864E43C85FFFD16A2A0685F30AC877138E106F2A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tE87EDEDF7A49228B90A7C51CFFD8114D62D5EF30_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PendingWriteException_tD008EED8A9DD4BB29844EF57D33275EE9A882F48_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass38_1_t51468321D6041881A73633BB17566E2FDE8E4269_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteralAA90888F4CA0449EE8C332709193E9D1C885CC1A;
IL2CPP_EXTERN_C String_t* _stringLiteralB2CAAE2FF3E57291CEF022D8A7EAA7C6A6ABDC74;
IL2CPP_EXTERN_C String_t* _stringLiteralD50A5324DC4F48AA08597AC234FDF95918035D01;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1_Invoke_m25501B02ECA680749841E48EEA596F763A82F475_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_m948E1585F7105BAF7467A6E48FE716E71CD4BAB9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Take_TisWaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_mD6127D3008326E62523DDEBDCA22C2623A48076C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToArray_TisException_t_mC45E629FDAFE5B708F9F65A7458539250D8461A7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToArray_TisWaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_mB82DA928C5644B54EA8198BD2135F4F6D579928F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m075340F31C13EF2A612E40C80988C9E651B7F829_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m4A7857ABA06AF87BCEB0EB05F49C6DEF148DC27F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m386C5CC04A468880E8A50EBF6086B47EB2461FCB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m78855BD085E859C5228061D642FD29C4161D7284_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m2DA2B2533227571D1B3AB4AFBA0E69F5484E1F73_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mDD08B6F8CE246828D34AF51A4B4A5CB333C9A4AE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_2__ctor_m29194A2FFE45ED9FF0FB5E4DEE6861B3979C0CB0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* KeyValuePair_2_get_Key_mB735BC2D7232A3B45D667D28C17BA51AAAFFB4A1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* KeyValuePair_2_get_Value_mF7293AF44DA0B8EB7B455A6227F7C36EEE9CF508_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m9FF374C04A41E750BACA841918306E6559FADD0D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mB306C980BF53D3411577EE0AF430FF5F9B1D2605_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mCC9D38BB3CBE3F2E67EDF3390D36ABFAD293468E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m1465A2F72D3985860367250F2F3CA0FF83A09D63_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m458DDFB5AB146FD65D91BAF36907B40AAD1585B3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_mBBF7F2EBBE5FD254B09CE99D16F6BB0C340CEA50_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m1E687BD559DDDC595828B26A476F527A1FA5F7A4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_mB6287B4A0B81DE7915896C1771FD16C182091404_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m0DE87A5A6E3E1768A8A8B4BA660934BE2600D063_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m33EBEFF81F45499476516DAAA3BE06259CDDF6B5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m3CA9F30DC986E649F8E82AD69F5085D355D91AC1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m450486C7FD0DA7D1068FD8977B5A99FC51F62307_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m4701F78844832952C66B59FAE9BDD3432B391F79_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m52FC81AB50BD21B6BFA16546E3AF9C94611D9811_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PersistentQueueSession_AssertNoPendingWritesFailures_mEBC84FCA8E6D166C6AB29880C9B76C80F1C426AF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PersistentQueueSession_AsyncWriteToStream_m6C9D372CBAD07BC6E4E3A503C3FD6B8733522C33_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PersistentQueueSession_OnReplaceStream_m36896D1B59C214C86E1CD371212E227AC32E8B32_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PersistentQueueSession_U3CSyncFlushBufferU3Eb__14_0_mBCCA91C92F2723E2773D72FA0BB4778EA83D0FDC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CReadTransactionLogU3Eb__38_2_m9D396619BE3FC0B707F2231A4A5F85092F181DE9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass15_0_U3CAsyncWriteToStreamU3Eb__0_m23B9A65D254C30C79EDC2DA9077ABFFE054C1915_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass38_1_U3CReadTransactionLogU3Eb__1_m809233E2E75548C019762CF93199AF104078E077_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t PersistentQueueEntry_Equals_m2C2C8BEC897BFCFFED93C12A43FFB4813276A185_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersistentQueueSession_AssertNoPendingWritesFailures_mEBC84FCA8E6D166C6AB29880C9B76C80F1C426AF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersistentQueueSession_AsyncFlushBuffer_m8FD2C50C3F6884F965490030895187710E0601A7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersistentQueueSession_AsyncWriteToStream_m6C9D372CBAD07BC6E4E3A503C3FD6B8733522C33_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersistentQueueSession_ConcatenateBufferAndAddIndividualOperations_m1F4735CE325637612B78A6DC6D8F7C871FA21894_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersistentQueueSession_Dequeue_m4B5E669B9208A5E3AC4550CD6CC66EF6962A1370_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersistentQueueSession_Dispose_mCCAC09F6A1BADB8E297CD0F42ED28178152D0A37_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersistentQueueSession_Enqueue_m274733E3E48A42C254312808CC94DA03D4D95C50_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersistentQueueSession_Flush_m07F15FE5C4F66C24A7AF4FCB760185E566245CB2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersistentQueueSession_OnReplaceStream_m36896D1B59C214C86E1CD371212E227AC32E8B32_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersistentQueueSession_SyncFlushBuffer_m2F6D9D269AC0D30E9976A4AC3A09B9A576D196D8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersistentQueueSession_WaitForPendingWrites_m105E3D4E7EA55B5E947BF87E12A6873E9299BFBE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersistentQueueSession__cctor_mD5EB5C1B0B79B18AC77D6EB9897D143BDEC95831_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersistentQueueSession__ctor_m65837A3111BE6219458716645231F6292DCD4955_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersistentQueueUtils_CreateFileStream_m2499F0F23E8AD0EC95EE99007F7B399B2F86C26F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersistentQueueUtils_CreateReadStream_mB2A55B03F4A2A9A35139A87EEDF0520B5FE56981_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersistentQueueUtils_CreateWriteStream_mFB0597CC35C0FC6AA0D37C9D58D059B9C79B5A26_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersistentQueueUtils_Read_mED8A8E34A21E61A990018FDBC041DFC0F059AFC4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersistentQueueUtils_WaitDelete_m11EE50B1C6DEE458F410146BAFF11CE1A8AA7195_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersistentQueueUtils_Write_mD0CD814DFFBA6D65FC4EE712F97F5EFF56B51D18_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersistentQueueUtils__cctor_m4AA6BE8D00B1C62A5E75A61DEB9828A27D4C63EE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec_U3CApplyTransactionOperationsInMemoryU3Eb__42_0_mA7A26B9DDD543B2482EFA045F321C7FD0A14E305_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec_U3CApplyTransactionOperationsInMemoryU3Eb__42_1_m500314484BD87AFD566F2CA81A29CACF24E883CA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec_U3CReinstateU3Eb__37_1_mD52A96913C7A99BAC86F3125947ED3B8740E545D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass15_0_U3CAsyncWriteToStreamU3Eb__0_m23B9A65D254C30C79EDC2DA9077ABFFE054C1915_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass38_0_U3CReadTransactionLogU3Eb__0_m94D5F9520C93079C88B6383B50CFADAF79C1E571_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__cctor_m268F61E56FB7E186C29B6D2830EDB6EEC86F0B06_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
struct ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209;
struct WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.List`1<System.Byte[]>
struct  List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ByteU5BU5DU5BU5D_t1DE3927D87FD236507BFE9CA7E3EEA348C53E0E1* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531, ____items_1)); }
	inline ByteU5BU5DU5BU5D_t1DE3927D87FD236507BFE9CA7E3EEA348C53E0E1* get__items_1() const { return ____items_1; }
	inline ByteU5BU5DU5BU5D_t1DE3927D87FD236507BFE9CA7E3EEA348C53E0E1** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ByteU5BU5DU5BU5D_t1DE3927D87FD236507BFE9CA7E3EEA348C53E0E1* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ByteU5BU5DU5BU5D_t1DE3927D87FD236507BFE9CA7E3EEA348C53E0E1* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531_StaticFields, ____emptyArray_5)); }
	inline ByteU5BU5DU5BU5D_t1DE3927D87FD236507BFE9CA7E3EEA348C53E0E1* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ByteU5BU5DU5BU5D_t1DE3927D87FD236507BFE9CA7E3EEA348C53E0E1** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ByteU5BU5DU5BU5D_t1DE3927D87FD236507BFE9CA7E3EEA348C53E0E1* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Exception>
struct  List_1_t864E43C85FFFD16A2A0685F30AC877138E106F2A  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t864E43C85FFFD16A2A0685F30AC877138E106F2A, ____items_1)); }
	inline ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209* get__items_1() const { return ____items_1; }
	inline ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t864E43C85FFFD16A2A0685F30AC877138E106F2A, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t864E43C85FFFD16A2A0685F30AC877138E106F2A, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t864E43C85FFFD16A2A0685F30AC877138E106F2A, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t864E43C85FFFD16A2A0685F30AC877138E106F2A_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t864E43C85FFFD16A2A0685F30AC877138E106F2A_StaticFields, ____emptyArray_5)); }
	inline ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.IO.Stream>
struct  List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StreamU5BU5D_tDADB5390E6C9A2BDA8A341EECC9F7F9DE08DB362* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF, ____items_1)); }
	inline StreamU5BU5D_tDADB5390E6C9A2BDA8A341EECC9F7F9DE08DB362* get__items_1() const { return ____items_1; }
	inline StreamU5BU5D_tDADB5390E6C9A2BDA8A341EECC9F7F9DE08DB362** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StreamU5BU5D_tDADB5390E6C9A2BDA8A341EECC9F7F9DE08DB362* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	StreamU5BU5D_tDADB5390E6C9A2BDA8A341EECC9F7F9DE08DB362* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF_StaticFields, ____emptyArray_5)); }
	inline StreamU5BU5D_tDADB5390E6C9A2BDA8A341EECC9F7F9DE08DB362* get__emptyArray_5() const { return ____emptyArray_5; }
	inline StreamU5BU5D_tDADB5390E6C9A2BDA8A341EECC9F7F9DE08DB362** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(StreamU5BU5D_tDADB5390E6C9A2BDA8A341EECC9F7F9DE08DB362* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Threading.WaitHandle>
struct  List_1_tE87EDEDF7A49228B90A7C51CFFD8114D62D5EF30  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE87EDEDF7A49228B90A7C51CFFD8114D62D5EF30, ____items_1)); }
	inline WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* get__items_1() const { return ____items_1; }
	inline WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE87EDEDF7A49228B90A7C51CFFD8114D62D5EF30, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE87EDEDF7A49228B90A7C51CFFD8114D62D5EF30, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE87EDEDF7A49228B90A7C51CFFD8114D62D5EF30, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tE87EDEDF7A49228B90A7C51CFFD8114D62D5EF30_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE87EDEDF7A49228B90A7C51CFFD8114D62D5EF30_StaticFields, ____emptyArray_5)); }
	inline WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* get__emptyArray_5() const { return ____emptyArray_5; }
	inline WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<mixpanel.queue.PersistentQueueOperation>
struct  List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	PersistentQueueOperationU5BU5D_tC47ACD37FB31D1AD1F14C680DAF3E142D97DE085* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7, ____items_1)); }
	inline PersistentQueueOperationU5BU5D_tC47ACD37FB31D1AD1F14C680DAF3E142D97DE085* get__items_1() const { return ____items_1; }
	inline PersistentQueueOperationU5BU5D_tC47ACD37FB31D1AD1F14C680DAF3E142D97DE085** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(PersistentQueueOperationU5BU5D_tC47ACD37FB31D1AD1F14C680DAF3E142D97DE085* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	PersistentQueueOperationU5BU5D_tC47ACD37FB31D1AD1F14C680DAF3E142D97DE085* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7_StaticFields, ____emptyArray_5)); }
	inline PersistentQueueOperationU5BU5D_tC47ACD37FB31D1AD1F14C680DAF3E142D97DE085* get__emptyArray_5() const { return ____emptyArray_5; }
	inline PersistentQueueOperationU5BU5D_tC47ACD37FB31D1AD1F14C680DAF3E142D97DE085** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(PersistentQueueOperationU5BU5D_tC47ACD37FB31D1AD1F14C680DAF3E142D97DE085* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.IO.BinaryReader
struct  BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969  : public RuntimeObject
{
public:
	// System.IO.Stream System.IO.BinaryReader::m_stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___m_stream_0;
	// System.Byte[] System.IO.BinaryReader::m_buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_buffer_1;
	// System.Text.Decoder System.IO.BinaryReader::m_decoder
	Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 * ___m_decoder_2;
	// System.Byte[] System.IO.BinaryReader::m_charBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___m_charBytes_3;
	// System.Char[] System.IO.BinaryReader::m_singleChar
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_singleChar_4;
	// System.Char[] System.IO.BinaryReader::m_charBuffer
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_charBuffer_5;
	// System.Int32 System.IO.BinaryReader::m_maxCharsSize
	int32_t ___m_maxCharsSize_6;
	// System.Boolean System.IO.BinaryReader::m_2BytesPerChar
	bool ___m_2BytesPerChar_7;
	// System.Boolean System.IO.BinaryReader::m_isMemoryStream
	bool ___m_isMemoryStream_8;
	// System.Boolean System.IO.BinaryReader::m_leaveOpen
	bool ___m_leaveOpen_9;

public:
	inline static int32_t get_offset_of_m_stream_0() { return static_cast<int32_t>(offsetof(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969, ___m_stream_0)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_m_stream_0() const { return ___m_stream_0; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_m_stream_0() { return &___m_stream_0; }
	inline void set_m_stream_0(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___m_stream_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_stream_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_buffer_1() { return static_cast<int32_t>(offsetof(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969, ___m_buffer_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_buffer_1() const { return ___m_buffer_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_buffer_1() { return &___m_buffer_1; }
	inline void set_m_buffer_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_buffer_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_buffer_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_decoder_2() { return static_cast<int32_t>(offsetof(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969, ___m_decoder_2)); }
	inline Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 * get_m_decoder_2() const { return ___m_decoder_2; }
	inline Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 ** get_address_of_m_decoder_2() { return &___m_decoder_2; }
	inline void set_m_decoder_2(Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 * value)
	{
		___m_decoder_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_decoder_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_charBytes_3() { return static_cast<int32_t>(offsetof(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969, ___m_charBytes_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_m_charBytes_3() const { return ___m_charBytes_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_m_charBytes_3() { return &___m_charBytes_3; }
	inline void set_m_charBytes_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___m_charBytes_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_charBytes_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_singleChar_4() { return static_cast<int32_t>(offsetof(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969, ___m_singleChar_4)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_singleChar_4() const { return ___m_singleChar_4; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_singleChar_4() { return &___m_singleChar_4; }
	inline void set_m_singleChar_4(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_singleChar_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_singleChar_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_charBuffer_5() { return static_cast<int32_t>(offsetof(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969, ___m_charBuffer_5)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_charBuffer_5() const { return ___m_charBuffer_5; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_charBuffer_5() { return &___m_charBuffer_5; }
	inline void set_m_charBuffer_5(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_charBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_charBuffer_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_maxCharsSize_6() { return static_cast<int32_t>(offsetof(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969, ___m_maxCharsSize_6)); }
	inline int32_t get_m_maxCharsSize_6() const { return ___m_maxCharsSize_6; }
	inline int32_t* get_address_of_m_maxCharsSize_6() { return &___m_maxCharsSize_6; }
	inline void set_m_maxCharsSize_6(int32_t value)
	{
		___m_maxCharsSize_6 = value;
	}

	inline static int32_t get_offset_of_m_2BytesPerChar_7() { return static_cast<int32_t>(offsetof(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969, ___m_2BytesPerChar_7)); }
	inline bool get_m_2BytesPerChar_7() const { return ___m_2BytesPerChar_7; }
	inline bool* get_address_of_m_2BytesPerChar_7() { return &___m_2BytesPerChar_7; }
	inline void set_m_2BytesPerChar_7(bool value)
	{
		___m_2BytesPerChar_7 = value;
	}

	inline static int32_t get_offset_of_m_isMemoryStream_8() { return static_cast<int32_t>(offsetof(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969, ___m_isMemoryStream_8)); }
	inline bool get_m_isMemoryStream_8() const { return ___m_isMemoryStream_8; }
	inline bool* get_address_of_m_isMemoryStream_8() { return &___m_isMemoryStream_8; }
	inline void set_m_isMemoryStream_8(bool value)
	{
		___m_isMemoryStream_8 = value;
	}

	inline static int32_t get_offset_of_m_leaveOpen_9() { return static_cast<int32_t>(offsetof(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969, ___m_leaveOpen_9)); }
	inline bool get_m_leaveOpen_9() const { return ___m_leaveOpen_9; }
	inline bool* get_address_of_m_leaveOpen_9() { return &___m_leaveOpen_9; }
	inline void set_m_leaveOpen_9(bool value)
	{
		___m_leaveOpen_9 = value;
	}
};


// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____identity_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// mixpanel.queue.PersistentQueue_<>c
struct  U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591_StaticFields
{
public:
	// mixpanel.queue.PersistentQueue_<>c mixpanel.queue.PersistentQueue_<>c::<>9
	U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591 * ___U3CU3E9_0;
	// System.Func`2<mixpanel.queue.PersistentQueueOperation,System.Boolean> mixpanel.queue.PersistentQueue_<>c::<>9__37_0
	Func_2_tFCA57E92A32141A0BF9637E0EABE4F37E2B4FB6D * ___U3CU3E9__37_0_1;
	// System.Func`2<mixpanel.queue.PersistentQueueOperation,mixpanel.queue.PersistentQueueOperation> mixpanel.queue.PersistentQueue_<>c::<>9__37_1
	Func_2_tB4C66CF15D7698C4C0618C6D1C23F362CD6ED754 * ___U3CU3E9__37_1_2;
	// System.Action mixpanel.queue.PersistentQueue_<>c::<>9__38_2
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__38_2_3;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Boolean> mixpanel.queue.PersistentQueue_<>c::<>9__42_0
	Func_2_tBD3BF5B8A6857D5E6CFA087C76B05A52E76F2FB2 * ___U3CU3E9__42_0_4;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32> mixpanel.queue.PersistentQueue_<>c::<>9__42_1
	Func_2_t629E2A2E190281BBB9863A1CAB74C21975B59EE0 * ___U3CU3E9__42_1_5;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__37_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591_StaticFields, ___U3CU3E9__37_0_1)); }
	inline Func_2_tFCA57E92A32141A0BF9637E0EABE4F37E2B4FB6D * get_U3CU3E9__37_0_1() const { return ___U3CU3E9__37_0_1; }
	inline Func_2_tFCA57E92A32141A0BF9637E0EABE4F37E2B4FB6D ** get_address_of_U3CU3E9__37_0_1() { return &___U3CU3E9__37_0_1; }
	inline void set_U3CU3E9__37_0_1(Func_2_tFCA57E92A32141A0BF9637E0EABE4F37E2B4FB6D * value)
	{
		___U3CU3E9__37_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__37_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__37_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591_StaticFields, ___U3CU3E9__37_1_2)); }
	inline Func_2_tB4C66CF15D7698C4C0618C6D1C23F362CD6ED754 * get_U3CU3E9__37_1_2() const { return ___U3CU3E9__37_1_2; }
	inline Func_2_tB4C66CF15D7698C4C0618C6D1C23F362CD6ED754 ** get_address_of_U3CU3E9__37_1_2() { return &___U3CU3E9__37_1_2; }
	inline void set_U3CU3E9__37_1_2(Func_2_tB4C66CF15D7698C4C0618C6D1C23F362CD6ED754 * value)
	{
		___U3CU3E9__37_1_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__37_1_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__38_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591_StaticFields, ___U3CU3E9__38_2_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__38_2_3() const { return ___U3CU3E9__38_2_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__38_2_3() { return &___U3CU3E9__38_2_3; }
	inline void set_U3CU3E9__38_2_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__38_2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__38_2_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__42_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591_StaticFields, ___U3CU3E9__42_0_4)); }
	inline Func_2_tBD3BF5B8A6857D5E6CFA087C76B05A52E76F2FB2 * get_U3CU3E9__42_0_4() const { return ___U3CU3E9__42_0_4; }
	inline Func_2_tBD3BF5B8A6857D5E6CFA087C76B05A52E76F2FB2 ** get_address_of_U3CU3E9__42_0_4() { return &___U3CU3E9__42_0_4; }
	inline void set_U3CU3E9__42_0_4(Func_2_tBD3BF5B8A6857D5E6CFA087C76B05A52E76F2FB2 * value)
	{
		___U3CU3E9__42_0_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__42_0_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__42_1_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591_StaticFields, ___U3CU3E9__42_1_5)); }
	inline Func_2_t629E2A2E190281BBB9863A1CAB74C21975B59EE0 * get_U3CU3E9__42_1_5() const { return ___U3CU3E9__42_1_5; }
	inline Func_2_t629E2A2E190281BBB9863A1CAB74C21975B59EE0 ** get_address_of_U3CU3E9__42_1_5() { return &___U3CU3E9__42_1_5; }
	inline void set_U3CU3E9__42_1_5(Func_2_t629E2A2E190281BBB9863A1CAB74C21975B59EE0 * value)
	{
		___U3CU3E9__42_1_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__42_1_5), (void*)value);
	}
};


// mixpanel.queue.PersistentQueue_<>c__DisplayClass38_0
struct  U3CU3Ec__DisplayClass38_0_t151099A4060264DDAAECA19075B7DD121A8DB2B7  : public RuntimeObject
{
public:
	// mixpanel.queue.PersistentQueue mixpanel.queue.PersistentQueue_<>c__DisplayClass38_0::<>4__this
	PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * ___U3CU3E4__this_0;
	// System.Boolean mixpanel.queue.PersistentQueue_<>c__DisplayClass38_0::requireTxLogTrimming
	bool ___requireTxLogTrimming_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t151099A4060264DDAAECA19075B7DD121A8DB2B7, ___U3CU3E4__this_0)); }
	inline PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_requireTxLogTrimming_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t151099A4060264DDAAECA19075B7DD121A8DB2B7, ___requireTxLogTrimming_1)); }
	inline bool get_requireTxLogTrimming_1() const { return ___requireTxLogTrimming_1; }
	inline bool* get_address_of_requireTxLogTrimming_1() { return &___requireTxLogTrimming_1; }
	inline void set_requireTxLogTrimming_1(bool value)
	{
		___requireTxLogTrimming_1 = value;
	}
};


// mixpanel.queue.PersistentQueue_<>c__DisplayClass38_1
struct  U3CU3Ec__DisplayClass38_1_t51468321D6041881A73633BB17566E2FDE8E4269  : public RuntimeObject
{
public:
	// System.Boolean mixpanel.queue.PersistentQueue_<>c__DisplayClass38_1::readingTransaction
	bool ___readingTransaction_0;
	// System.Action mixpanel.queue.PersistentQueue_<>c__DisplayClass38_1::<>9__1
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__1_1;

public:
	inline static int32_t get_offset_of_readingTransaction_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_1_t51468321D6041881A73633BB17566E2FDE8E4269, ___readingTransaction_0)); }
	inline bool get_readingTransaction_0() const { return ___readingTransaction_0; }
	inline bool* get_address_of_readingTransaction_0() { return &___readingTransaction_0; }
	inline void set_readingTransaction_0(bool value)
	{
		___readingTransaction_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E9__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_1_t51468321D6041881A73633BB17566E2FDE8E4269, ___U3CU3E9__1_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__1_1() const { return ___U3CU3E9__1_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__1_1() { return &___U3CU3E9__1_1; }
	inline void set_U3CU3E9__1_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__1_1), (void*)value);
	}
};


// mixpanel.queue.PersistentQueue_<>c__DisplayClass39_0
struct  U3CU3Ec__DisplayClass39_0_t24AB86C76478BA26F0821541C9174F6766DE1508  : public RuntimeObject
{
public:
	// System.Byte[] mixpanel.queue.PersistentQueue_<>c__DisplayClass39_0::transactionBuffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___transactionBuffer_0;

public:
	inline static int32_t get_offset_of_transactionBuffer_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_t24AB86C76478BA26F0821541C9174F6766DE1508, ___transactionBuffer_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_transactionBuffer_0() const { return ___transactionBuffer_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_transactionBuffer_0() { return &___transactionBuffer_0; }
	inline void set_transactionBuffer_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___transactionBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___transactionBuffer_0), (void*)value);
	}
};


// mixpanel.queue.PersistentQueueEntry
struct  PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA  : public RuntimeObject
{
public:
	// System.Byte[] mixpanel.queue.PersistentQueueEntry::Data
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Data_0;
	// System.Int32 mixpanel.queue.PersistentQueueEntry::FileNumber
	int32_t ___FileNumber_1;
	// System.Int32 mixpanel.queue.PersistentQueueEntry::Start
	int32_t ___Start_2;
	// System.Int32 mixpanel.queue.PersistentQueueEntry::Length
	int32_t ___Length_3;

public:
	inline static int32_t get_offset_of_Data_0() { return static_cast<int32_t>(offsetof(PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA, ___Data_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Data_0() const { return ___Data_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Data_0() { return &___Data_0; }
	inline void set_Data_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Data_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Data_0), (void*)value);
	}

	inline static int32_t get_offset_of_FileNumber_1() { return static_cast<int32_t>(offsetof(PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA, ___FileNumber_1)); }
	inline int32_t get_FileNumber_1() const { return ___FileNumber_1; }
	inline int32_t* get_address_of_FileNumber_1() { return &___FileNumber_1; }
	inline void set_FileNumber_1(int32_t value)
	{
		___FileNumber_1 = value;
	}

	inline static int32_t get_offset_of_Start_2() { return static_cast<int32_t>(offsetof(PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA, ___Start_2)); }
	inline int32_t get_Start_2() const { return ___Start_2; }
	inline int32_t* get_address_of_Start_2() { return &___Start_2; }
	inline void set_Start_2(int32_t value)
	{
		___Start_2 = value;
	}

	inline static int32_t get_offset_of_Length_3() { return static_cast<int32_t>(offsetof(PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA, ___Length_3)); }
	inline int32_t get_Length_3() const { return ___Length_3; }
	inline int32_t* get_address_of_Length_3() { return &___Length_3; }
	inline void set_Length_3(int32_t value)
	{
		___Length_3 = value;
	}
};


// mixpanel.queue.PersistentQueueSession_<>c__DisplayClass15_0
struct  U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C  : public RuntimeObject
{
public:
	// System.IO.Stream mixpanel.queue.PersistentQueueSession_<>c__DisplayClass15_0::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_0;
	// mixpanel.queue.PersistentQueueSession mixpanel.queue.PersistentQueueSession_<>c__DisplayClass15_0::<>4__this
	PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * ___U3CU3E4__this_1;
	// System.Threading.ManualResetEvent mixpanel.queue.PersistentQueueSession_<>c__DisplayClass15_0::resetEvent
	ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * ___resetEvent_2;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C, ___stream_0)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_0() const { return ___stream_0; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stream_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C, ___U3CU3E4__this_1)); }
	inline PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}

	inline static int32_t get_offset_of_resetEvent_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C, ___resetEvent_2)); }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * get_resetEvent_2() const { return ___resetEvent_2; }
	inline ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 ** get_address_of_resetEvent_2() { return &___resetEvent_2; }
	inline void set_resetEvent_2(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * value)
	{
		___resetEvent_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resetEvent_2), (void*)value);
	}
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct  Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
struct  KeyValuePair_2_tA9AFBC865B07606ED8F020A8E3AF8E27491AF809 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_tA9AFBC865B07606ED8F020A8E3AF8E27491AF809, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_tA9AFBC865B07606ED8F020A8E3AF8E27491AF809, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};


// System.Collections.Generic.List`1_Enumerator<System.Byte[]>
struct  Enumerator_t4A0753267EA56CCB5A11BD47A5A39F7DC4154DB1 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t4A0753267EA56CCB5A11BD47A5A39F7DC4154DB1, ___list_0)); }
	inline List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 * get_list_0() const { return ___list_0; }
	inline List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t4A0753267EA56CCB5A11BD47A5A39F7DC4154DB1, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t4A0753267EA56CCB5A11BD47A5A39F7DC4154DB1, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t4A0753267EA56CCB5A11BD47A5A39F7DC4154DB1, ___current_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_current_3() const { return ___current_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<System.IO.Stream>
struct  Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55, ___list_0)); }
	inline List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF * get_list_0() const { return ___list_0; }
	inline List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55, ___current_3)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_current_3() const { return ___current_3; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<System.Object>
struct  Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___list_0)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_list_0() const { return ___list_0; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rngAccess_12), (void*)value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rng_13), (void*)value);
	}
};


// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_3;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_2() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_2)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_2() const { return ____activeReadWriteTask_2; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_2() { return &____activeReadWriteTask_2; }
	inline void set__activeReadWriteTask_2(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____activeReadWriteTask_2), (void*)value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_3)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_3() const { return ____asyncActiveSemaphore_3; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_3() { return &____asyncActiveSemaphore_3; }
	inline void set__asyncActiveSemaphore_3(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____asyncActiveSemaphore_3), (void*)value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Null_1), (void*)value);
	}
};


// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Int64
struct  Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.IO.FileAccess
struct  FileAccess_t31950F3A853EAE886AC8F13EA7FC03A3EB46E3F6 
{
public:
	// System.Int32 System.IO.FileAccess::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileAccess_t31950F3A853EAE886AC8F13EA7FC03A3EB46E3F6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.IO.FileMode
struct  FileMode_tD19D05B1E6CAF201F88401B04FDB25227664C419 
{
public:
	// System.Int32 System.IO.FileMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileMode_tD19D05B1E6CAF201F88401B04FDB25227664C419, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.IO.FileOptions
struct  FileOptions_t12395DCB579B97DF4788AB79553F8815F9625FA7 
{
public:
	// System.Int32 System.IO.FileOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileOptions_t12395DCB579B97DF4788AB79553F8815F9625FA7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.IO.FileShare
struct  FileShare_t9AA8473BBE5DD8532CEAF3F48F26DA5A25A93684 
{
public:
	// System.Int32 System.IO.FileShare::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileShare_t9AA8473BBE5DD8532CEAF3F48F26DA5A25A93684, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Threading.WaitHandle
struct  WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IntPtr System.Threading.WaitHandle::waitHandle
	intptr_t ___waitHandle_3;
	// Microsoft.Win32.SafeHandles.SafeWaitHandle modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.WaitHandle::safeWaitHandle
	SafeWaitHandle_t51DB35FF382E636FF3B868D87816733894D46CF2 * ___safeWaitHandle_4;
	// System.Boolean System.Threading.WaitHandle::hasThreadAffinity
	bool ___hasThreadAffinity_5;

public:
	inline static int32_t get_offset_of_waitHandle_3() { return static_cast<int32_t>(offsetof(WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6, ___waitHandle_3)); }
	inline intptr_t get_waitHandle_3() const { return ___waitHandle_3; }
	inline intptr_t* get_address_of_waitHandle_3() { return &___waitHandle_3; }
	inline void set_waitHandle_3(intptr_t value)
	{
		___waitHandle_3 = value;
	}

	inline static int32_t get_offset_of_safeWaitHandle_4() { return static_cast<int32_t>(offsetof(WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6, ___safeWaitHandle_4)); }
	inline SafeWaitHandle_t51DB35FF382E636FF3B868D87816733894D46CF2 * get_safeWaitHandle_4() const { return ___safeWaitHandle_4; }
	inline SafeWaitHandle_t51DB35FF382E636FF3B868D87816733894D46CF2 ** get_address_of_safeWaitHandle_4() { return &___safeWaitHandle_4; }
	inline void set_safeWaitHandle_4(SafeWaitHandle_t51DB35FF382E636FF3B868D87816733894D46CF2 * value)
	{
		___safeWaitHandle_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___safeWaitHandle_4), (void*)value);
	}

	inline static int32_t get_offset_of_hasThreadAffinity_5() { return static_cast<int32_t>(offsetof(WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6, ___hasThreadAffinity_5)); }
	inline bool get_hasThreadAffinity_5() const { return ___hasThreadAffinity_5; }
	inline bool* get_address_of_hasThreadAffinity_5() { return &___hasThreadAffinity_5; }
	inline void set_hasThreadAffinity_5(bool value)
	{
		___hasThreadAffinity_5 = value;
	}
};

struct WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_StaticFields
{
public:
	// System.IntPtr System.Threading.WaitHandle::InvalidHandle
	intptr_t ___InvalidHandle_10;

public:
	inline static int32_t get_offset_of_InvalidHandle_10() { return static_cast<int32_t>(offsetof(WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_StaticFields, ___InvalidHandle_10)); }
	inline intptr_t get_InvalidHandle_10() const { return ___InvalidHandle_10; }
	inline intptr_t* get_address_of_InvalidHandle_10() { return &___InvalidHandle_10; }
	inline void set_InvalidHandle_10(intptr_t value)
	{
		___InvalidHandle_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Threading.WaitHandle
struct WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_marshaled_pinvoke : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	intptr_t ___waitHandle_3;
	void* ___safeWaitHandle_4;
	int32_t ___hasThreadAffinity_5;
};
// Native definition for COM marshalling of System.Threading.WaitHandle
struct WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_marshaled_com : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	intptr_t ___waitHandle_3;
	void* ___safeWaitHandle_4;
	int32_t ___hasThreadAffinity_5;
};

// mixpanel.queue.PersistentQueue
struct  PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26  : public RuntimeObject
{
public:
	// System.Collections.Generic.HashSet`1<mixpanel.queue.PersistentQueueEntry> mixpanel.queue.PersistentQueue::_checkedOutEntries
	HashSet_1_t172DF6846FC334649B0EA6BADF143BA53D0E03F3 * ____checkedOutEntries_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> mixpanel.queue.PersistentQueue::_countOfItemsPerFile
	Dictionary_2_t6567430A4033E968FED88FBBD298DC9D0DFA398F * ____countOfItemsPerFile_2;
	// System.Collections.Generic.LinkedList`1<mixpanel.queue.PersistentQueueEntry> mixpanel.queue.PersistentQueue::_entries
	LinkedList_1_t7C6A05BAC60F7CAB159B06575F4827077E31F562 * ____entries_3;
	// System.String mixpanel.queue.PersistentQueue::_path
	String_t* ____path_4;
	// System.Object mixpanel.queue.PersistentQueue::_transactionLogLock
	RuntimeObject * ____transactionLogLock_5;
	// System.Object mixpanel.queue.PersistentQueue::_writerLock
	RuntimeObject * ____writerLock_6;
	// System.Boolean mixpanel.queue.PersistentQueue::_trimTransactionLogOnDispose
	bool ____trimTransactionLogOnDispose_7;
	// System.Int32 mixpanel.queue.PersistentQueue::_maxFileSize
	int32_t ____maxFileSize_8;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) mixpanel.queue.PersistentQueue::_disposed
	bool ____disposed_9;
	// System.IO.FileStream mixpanel.queue.PersistentQueue::_fileLock
	FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * ____fileLock_10;
	// System.Int64 mixpanel.queue.PersistentQueue::<CurrentFilePosition>k__BackingField
	int64_t ___U3CCurrentFilePositionU3Ek__BackingField_11;
	// System.Int32 mixpanel.queue.PersistentQueue::<CurrentFileNumber>k__BackingField
	int32_t ___U3CCurrentFileNumberU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of__checkedOutEntries_1() { return static_cast<int32_t>(offsetof(PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26, ____checkedOutEntries_1)); }
	inline HashSet_1_t172DF6846FC334649B0EA6BADF143BA53D0E03F3 * get__checkedOutEntries_1() const { return ____checkedOutEntries_1; }
	inline HashSet_1_t172DF6846FC334649B0EA6BADF143BA53D0E03F3 ** get_address_of__checkedOutEntries_1() { return &____checkedOutEntries_1; }
	inline void set__checkedOutEntries_1(HashSet_1_t172DF6846FC334649B0EA6BADF143BA53D0E03F3 * value)
	{
		____checkedOutEntries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____checkedOutEntries_1), (void*)value);
	}

	inline static int32_t get_offset_of__countOfItemsPerFile_2() { return static_cast<int32_t>(offsetof(PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26, ____countOfItemsPerFile_2)); }
	inline Dictionary_2_t6567430A4033E968FED88FBBD298DC9D0DFA398F * get__countOfItemsPerFile_2() const { return ____countOfItemsPerFile_2; }
	inline Dictionary_2_t6567430A4033E968FED88FBBD298DC9D0DFA398F ** get_address_of__countOfItemsPerFile_2() { return &____countOfItemsPerFile_2; }
	inline void set__countOfItemsPerFile_2(Dictionary_2_t6567430A4033E968FED88FBBD298DC9D0DFA398F * value)
	{
		____countOfItemsPerFile_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____countOfItemsPerFile_2), (void*)value);
	}

	inline static int32_t get_offset_of__entries_3() { return static_cast<int32_t>(offsetof(PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26, ____entries_3)); }
	inline LinkedList_1_t7C6A05BAC60F7CAB159B06575F4827077E31F562 * get__entries_3() const { return ____entries_3; }
	inline LinkedList_1_t7C6A05BAC60F7CAB159B06575F4827077E31F562 ** get_address_of__entries_3() { return &____entries_3; }
	inline void set__entries_3(LinkedList_1_t7C6A05BAC60F7CAB159B06575F4827077E31F562 * value)
	{
		____entries_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____entries_3), (void*)value);
	}

	inline static int32_t get_offset_of__path_4() { return static_cast<int32_t>(offsetof(PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26, ____path_4)); }
	inline String_t* get__path_4() const { return ____path_4; }
	inline String_t** get_address_of__path_4() { return &____path_4; }
	inline void set__path_4(String_t* value)
	{
		____path_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____path_4), (void*)value);
	}

	inline static int32_t get_offset_of__transactionLogLock_5() { return static_cast<int32_t>(offsetof(PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26, ____transactionLogLock_5)); }
	inline RuntimeObject * get__transactionLogLock_5() const { return ____transactionLogLock_5; }
	inline RuntimeObject ** get_address_of__transactionLogLock_5() { return &____transactionLogLock_5; }
	inline void set__transactionLogLock_5(RuntimeObject * value)
	{
		____transactionLogLock_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____transactionLogLock_5), (void*)value);
	}

	inline static int32_t get_offset_of__writerLock_6() { return static_cast<int32_t>(offsetof(PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26, ____writerLock_6)); }
	inline RuntimeObject * get__writerLock_6() const { return ____writerLock_6; }
	inline RuntimeObject ** get_address_of__writerLock_6() { return &____writerLock_6; }
	inline void set__writerLock_6(RuntimeObject * value)
	{
		____writerLock_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____writerLock_6), (void*)value);
	}

	inline static int32_t get_offset_of__trimTransactionLogOnDispose_7() { return static_cast<int32_t>(offsetof(PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26, ____trimTransactionLogOnDispose_7)); }
	inline bool get__trimTransactionLogOnDispose_7() const { return ____trimTransactionLogOnDispose_7; }
	inline bool* get_address_of__trimTransactionLogOnDispose_7() { return &____trimTransactionLogOnDispose_7; }
	inline void set__trimTransactionLogOnDispose_7(bool value)
	{
		____trimTransactionLogOnDispose_7 = value;
	}

	inline static int32_t get_offset_of__maxFileSize_8() { return static_cast<int32_t>(offsetof(PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26, ____maxFileSize_8)); }
	inline int32_t get__maxFileSize_8() const { return ____maxFileSize_8; }
	inline int32_t* get_address_of__maxFileSize_8() { return &____maxFileSize_8; }
	inline void set__maxFileSize_8(int32_t value)
	{
		____maxFileSize_8 = value;
	}

	inline static int32_t get_offset_of__disposed_9() { return static_cast<int32_t>(offsetof(PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26, ____disposed_9)); }
	inline bool get__disposed_9() const { return ____disposed_9; }
	inline bool* get_address_of__disposed_9() { return &____disposed_9; }
	inline void set__disposed_9(bool value)
	{
		____disposed_9 = value;
	}

	inline static int32_t get_offset_of__fileLock_10() { return static_cast<int32_t>(offsetof(PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26, ____fileLock_10)); }
	inline FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * get__fileLock_10() const { return ____fileLock_10; }
	inline FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 ** get_address_of__fileLock_10() { return &____fileLock_10; }
	inline void set__fileLock_10(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * value)
	{
		____fileLock_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____fileLock_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCurrentFilePositionU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26, ___U3CCurrentFilePositionU3Ek__BackingField_11)); }
	inline int64_t get_U3CCurrentFilePositionU3Ek__BackingField_11() const { return ___U3CCurrentFilePositionU3Ek__BackingField_11; }
	inline int64_t* get_address_of_U3CCurrentFilePositionU3Ek__BackingField_11() { return &___U3CCurrentFilePositionU3Ek__BackingField_11; }
	inline void set_U3CCurrentFilePositionU3Ek__BackingField_11(int64_t value)
	{
		___U3CCurrentFilePositionU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentFileNumberU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26, ___U3CCurrentFileNumberU3Ek__BackingField_12)); }
	inline int32_t get_U3CCurrentFileNumberU3Ek__BackingField_12() const { return ___U3CCurrentFileNumberU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CCurrentFileNumberU3Ek__BackingField_12() { return &___U3CCurrentFileNumberU3Ek__BackingField_12; }
	inline void set_U3CCurrentFileNumberU3Ek__BackingField_12(int32_t value)
	{
		___U3CCurrentFileNumberU3Ek__BackingField_12 = value;
	}
};

struct PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26_StaticFields
{
public:
	// System.Object mixpanel.queue.PersistentQueue::ConfigLock
	RuntimeObject * ___ConfigLock_0;

public:
	inline static int32_t get_offset_of_ConfigLock_0() { return static_cast<int32_t>(offsetof(PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26_StaticFields, ___ConfigLock_0)); }
	inline RuntimeObject * get_ConfigLock_0() const { return ___ConfigLock_0; }
	inline RuntimeObject ** get_address_of_ConfigLock_0() { return &___ConfigLock_0; }
	inline void set_ConfigLock_0(RuntimeObject * value)
	{
		___ConfigLock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ConfigLock_0), (void*)value);
	}
};


// mixpanel.queue.PersistentQueueMarkers
struct  PersistentQueueMarkers_t6EE2C40AD4A7510DAF6EBB426A141F271CC83D02 
{
public:
	// System.Int32 mixpanel.queue.PersistentQueueMarkers::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PersistentQueueMarkers_t6EE2C40AD4A7510DAF6EBB426A141F271CC83D02, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// mixpanel.queue.PersistentQueueOperationTypes
struct  PersistentQueueOperationTypes_tDC2B54374E31382ADB8061A1E7074D209966EE48 
{
public:
	// System.Byte mixpanel.queue.PersistentQueueOperationTypes::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PersistentQueueOperationTypes_tDC2B54374E31382ADB8061A1E7074D209966EE48, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};


// mixpanel.queue.PersistentQueueSession
struct  PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<mixpanel.queue.PersistentQueueOperation> mixpanel.queue.PersistentQueueSession::_operations
	List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 * ____operations_1;
	// System.Collections.Generic.IList`1<System.Exception> mixpanel.queue.PersistentQueueSession::_pendingWritesFailures
	RuntimeObject* ____pendingWritesFailures_2;
	// System.Collections.Generic.IList`1<System.Threading.WaitHandle> mixpanel.queue.PersistentQueueSession::_pendingWritesHandles
	RuntimeObject* ____pendingWritesHandles_3;
	// mixpanel.queue.PersistentQueue mixpanel.queue.PersistentQueueSession::_queue
	PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * ____queue_4;
	// System.Collections.Generic.List`1<System.IO.Stream> mixpanel.queue.PersistentQueueSession::_streamsToDisposeOnFlush
	List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF * ____streamsToDisposeOnFlush_5;
	// System.Collections.Generic.List`1<System.Byte[]> mixpanel.queue.PersistentQueueSession::_buffer
	List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 * ____buffer_6;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) mixpanel.queue.PersistentQueueSession::_disposed
	bool ____disposed_7;
	// System.IO.Stream mixpanel.queue.PersistentQueueSession::_currentStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ____currentStream_8;
	// System.Int32 mixpanel.queue.PersistentQueueSession::_bufferSize
	int32_t ____bufferSize_9;

public:
	inline static int32_t get_offset_of__operations_1() { return static_cast<int32_t>(offsetof(PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359, ____operations_1)); }
	inline List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 * get__operations_1() const { return ____operations_1; }
	inline List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 ** get_address_of__operations_1() { return &____operations_1; }
	inline void set__operations_1(List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 * value)
	{
		____operations_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____operations_1), (void*)value);
	}

	inline static int32_t get_offset_of__pendingWritesFailures_2() { return static_cast<int32_t>(offsetof(PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359, ____pendingWritesFailures_2)); }
	inline RuntimeObject* get__pendingWritesFailures_2() const { return ____pendingWritesFailures_2; }
	inline RuntimeObject** get_address_of__pendingWritesFailures_2() { return &____pendingWritesFailures_2; }
	inline void set__pendingWritesFailures_2(RuntimeObject* value)
	{
		____pendingWritesFailures_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____pendingWritesFailures_2), (void*)value);
	}

	inline static int32_t get_offset_of__pendingWritesHandles_3() { return static_cast<int32_t>(offsetof(PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359, ____pendingWritesHandles_3)); }
	inline RuntimeObject* get__pendingWritesHandles_3() const { return ____pendingWritesHandles_3; }
	inline RuntimeObject** get_address_of__pendingWritesHandles_3() { return &____pendingWritesHandles_3; }
	inline void set__pendingWritesHandles_3(RuntimeObject* value)
	{
		____pendingWritesHandles_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____pendingWritesHandles_3), (void*)value);
	}

	inline static int32_t get_offset_of__queue_4() { return static_cast<int32_t>(offsetof(PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359, ____queue_4)); }
	inline PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * get__queue_4() const { return ____queue_4; }
	inline PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 ** get_address_of__queue_4() { return &____queue_4; }
	inline void set__queue_4(PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * value)
	{
		____queue_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____queue_4), (void*)value);
	}

	inline static int32_t get_offset_of__streamsToDisposeOnFlush_5() { return static_cast<int32_t>(offsetof(PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359, ____streamsToDisposeOnFlush_5)); }
	inline List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF * get__streamsToDisposeOnFlush_5() const { return ____streamsToDisposeOnFlush_5; }
	inline List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF ** get_address_of__streamsToDisposeOnFlush_5() { return &____streamsToDisposeOnFlush_5; }
	inline void set__streamsToDisposeOnFlush_5(List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF * value)
	{
		____streamsToDisposeOnFlush_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____streamsToDisposeOnFlush_5), (void*)value);
	}

	inline static int32_t get_offset_of__buffer_6() { return static_cast<int32_t>(offsetof(PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359, ____buffer_6)); }
	inline List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 * get__buffer_6() const { return ____buffer_6; }
	inline List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 ** get_address_of__buffer_6() { return &____buffer_6; }
	inline void set__buffer_6(List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 * value)
	{
		____buffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____buffer_6), (void*)value);
	}

	inline static int32_t get_offset_of__disposed_7() { return static_cast<int32_t>(offsetof(PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359, ____disposed_7)); }
	inline bool get__disposed_7() const { return ____disposed_7; }
	inline bool* get_address_of__disposed_7() { return &____disposed_7; }
	inline void set__disposed_7(bool value)
	{
		____disposed_7 = value;
	}

	inline static int32_t get_offset_of__currentStream_8() { return static_cast<int32_t>(offsetof(PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359, ____currentStream_8)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get__currentStream_8() const { return ____currentStream_8; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of__currentStream_8() { return &____currentStream_8; }
	inline void set__currentStream_8(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		____currentStream_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____currentStream_8), (void*)value);
	}

	inline static int32_t get_offset_of__bufferSize_9() { return static_cast<int32_t>(offsetof(PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359, ____bufferSize_9)); }
	inline int32_t get__bufferSize_9() const { return ____bufferSize_9; }
	inline int32_t* get_address_of__bufferSize_9() { return &____bufferSize_9; }
	inline void set__bufferSize_9(int32_t value)
	{
		____bufferSize_9 = value;
	}
};

struct PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359_StaticFields
{
public:
	// System.Object mixpanel.queue.PersistentQueueSession::CtorLock
	RuntimeObject * ___CtorLock_0;

public:
	inline static int32_t get_offset_of_CtorLock_0() { return static_cast<int32_t>(offsetof(PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359_StaticFields, ___CtorLock_0)); }
	inline RuntimeObject * get_CtorLock_0() const { return ___CtorLock_0; }
	inline RuntimeObject ** get_address_of_CtorLock_0() { return &___CtorLock_0; }
	inline void set_CtorLock_0(RuntimeObject * value)
	{
		___CtorLock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CtorLock_0), (void*)value);
	}
};


// mixpanel.queue.PersistentQueueUtils
struct  PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE  : public RuntimeObject
{
public:

public:
};

struct PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_StaticFields
{
public:
	// System.Byte[] mixpanel.queue.PersistentQueueUtils::OperationSeparatorBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___OperationSeparatorBytes_1;
	// System.Guid mixpanel.queue.PersistentQueueUtils::StartTransactionSeparatorGuid
	Guid_t  ___StartTransactionSeparatorGuid_2;
	// System.Byte[] mixpanel.queue.PersistentQueueUtils::StartTransactionSeparator
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___StartTransactionSeparator_3;
	// System.Guid mixpanel.queue.PersistentQueueUtils::EndTransactionSeparatorGuid
	Guid_t  ___EndTransactionSeparatorGuid_4;
	// System.Byte[] mixpanel.queue.PersistentQueueUtils::EndTransactionSeparator
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___EndTransactionSeparator_5;
	// System.Object mixpanel.queue.PersistentQueueUtils::Lock
	RuntimeObject * ___Lock_11;

public:
	inline static int32_t get_offset_of_OperationSeparatorBytes_1() { return static_cast<int32_t>(offsetof(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_StaticFields, ___OperationSeparatorBytes_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_OperationSeparatorBytes_1() const { return ___OperationSeparatorBytes_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_OperationSeparatorBytes_1() { return &___OperationSeparatorBytes_1; }
	inline void set_OperationSeparatorBytes_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___OperationSeparatorBytes_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OperationSeparatorBytes_1), (void*)value);
	}

	inline static int32_t get_offset_of_StartTransactionSeparatorGuid_2() { return static_cast<int32_t>(offsetof(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_StaticFields, ___StartTransactionSeparatorGuid_2)); }
	inline Guid_t  get_StartTransactionSeparatorGuid_2() const { return ___StartTransactionSeparatorGuid_2; }
	inline Guid_t * get_address_of_StartTransactionSeparatorGuid_2() { return &___StartTransactionSeparatorGuid_2; }
	inline void set_StartTransactionSeparatorGuid_2(Guid_t  value)
	{
		___StartTransactionSeparatorGuid_2 = value;
	}

	inline static int32_t get_offset_of_StartTransactionSeparator_3() { return static_cast<int32_t>(offsetof(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_StaticFields, ___StartTransactionSeparator_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_StartTransactionSeparator_3() const { return ___StartTransactionSeparator_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_StartTransactionSeparator_3() { return &___StartTransactionSeparator_3; }
	inline void set_StartTransactionSeparator_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___StartTransactionSeparator_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StartTransactionSeparator_3), (void*)value);
	}

	inline static int32_t get_offset_of_EndTransactionSeparatorGuid_4() { return static_cast<int32_t>(offsetof(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_StaticFields, ___EndTransactionSeparatorGuid_4)); }
	inline Guid_t  get_EndTransactionSeparatorGuid_4() const { return ___EndTransactionSeparatorGuid_4; }
	inline Guid_t * get_address_of_EndTransactionSeparatorGuid_4() { return &___EndTransactionSeparatorGuid_4; }
	inline void set_EndTransactionSeparatorGuid_4(Guid_t  value)
	{
		___EndTransactionSeparatorGuid_4 = value;
	}

	inline static int32_t get_offset_of_EndTransactionSeparator_5() { return static_cast<int32_t>(offsetof(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_StaticFields, ___EndTransactionSeparator_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_EndTransactionSeparator_5() const { return ___EndTransactionSeparator_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_EndTransactionSeparator_5() { return &___EndTransactionSeparator_5; }
	inline void set_EndTransactionSeparator_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___EndTransactionSeparator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EndTransactionSeparator_5), (void*)value);
	}

	inline static int32_t get_offset_of_Lock_11() { return static_cast<int32_t>(offsetof(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_StaticFields, ___Lock_11)); }
	inline RuntimeObject * get_Lock_11() const { return ___Lock_11; }
	inline RuntimeObject ** get_address_of_Lock_11() { return &___Lock_11; }
	inline void set_Lock_11(RuntimeObject * value)
	{
		___Lock_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Lock_11), (void*)value);
	}
};


// System.IO.FileStream
struct  FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Byte[] System.IO.FileStream::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_6;
	// System.String System.IO.FileStream::name
	String_t* ___name_7;
	// Microsoft.Win32.SafeHandles.SafeFileHandle System.IO.FileStream::safeHandle
	SafeFileHandle_tE1B31BE63CD11BBF2B9B6A205A72735F32EB1BCB * ___safeHandle_8;
	// System.Boolean System.IO.FileStream::isExposed
	bool ___isExposed_9;
	// System.Int64 System.IO.FileStream::append_startpos
	int64_t ___append_startpos_10;
	// System.IO.FileAccess System.IO.FileStream::access
	int32_t ___access_11;
	// System.Boolean System.IO.FileStream::owner
	bool ___owner_12;
	// System.Boolean System.IO.FileStream::async
	bool ___async_13;
	// System.Boolean System.IO.FileStream::canseek
	bool ___canseek_14;
	// System.Boolean System.IO.FileStream::anonymous
	bool ___anonymous_15;
	// System.Boolean System.IO.FileStream::buf_dirty
	bool ___buf_dirty_16;
	// System.Int32 System.IO.FileStream::buf_size
	int32_t ___buf_size_17;
	// System.Int32 System.IO.FileStream::buf_length
	int32_t ___buf_length_18;
	// System.Int32 System.IO.FileStream::buf_offset
	int32_t ___buf_offset_19;
	// System.Int64 System.IO.FileStream::buf_start
	int64_t ___buf_start_20;

public:
	inline static int32_t get_offset_of_buf_6() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_6() const { return ___buf_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_6() { return &___buf_6; }
	inline void set_buf_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buf_6), (void*)value);
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_7), (void*)value);
	}

	inline static int32_t get_offset_of_safeHandle_8() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___safeHandle_8)); }
	inline SafeFileHandle_tE1B31BE63CD11BBF2B9B6A205A72735F32EB1BCB * get_safeHandle_8() const { return ___safeHandle_8; }
	inline SafeFileHandle_tE1B31BE63CD11BBF2B9B6A205A72735F32EB1BCB ** get_address_of_safeHandle_8() { return &___safeHandle_8; }
	inline void set_safeHandle_8(SafeFileHandle_tE1B31BE63CD11BBF2B9B6A205A72735F32EB1BCB * value)
	{
		___safeHandle_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___safeHandle_8), (void*)value);
	}

	inline static int32_t get_offset_of_isExposed_9() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___isExposed_9)); }
	inline bool get_isExposed_9() const { return ___isExposed_9; }
	inline bool* get_address_of_isExposed_9() { return &___isExposed_9; }
	inline void set_isExposed_9(bool value)
	{
		___isExposed_9 = value;
	}

	inline static int32_t get_offset_of_append_startpos_10() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___append_startpos_10)); }
	inline int64_t get_append_startpos_10() const { return ___append_startpos_10; }
	inline int64_t* get_address_of_append_startpos_10() { return &___append_startpos_10; }
	inline void set_append_startpos_10(int64_t value)
	{
		___append_startpos_10 = value;
	}

	inline static int32_t get_offset_of_access_11() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___access_11)); }
	inline int32_t get_access_11() const { return ___access_11; }
	inline int32_t* get_address_of_access_11() { return &___access_11; }
	inline void set_access_11(int32_t value)
	{
		___access_11 = value;
	}

	inline static int32_t get_offset_of_owner_12() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___owner_12)); }
	inline bool get_owner_12() const { return ___owner_12; }
	inline bool* get_address_of_owner_12() { return &___owner_12; }
	inline void set_owner_12(bool value)
	{
		___owner_12 = value;
	}

	inline static int32_t get_offset_of_async_13() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___async_13)); }
	inline bool get_async_13() const { return ___async_13; }
	inline bool* get_address_of_async_13() { return &___async_13; }
	inline void set_async_13(bool value)
	{
		___async_13 = value;
	}

	inline static int32_t get_offset_of_canseek_14() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___canseek_14)); }
	inline bool get_canseek_14() const { return ___canseek_14; }
	inline bool* get_address_of_canseek_14() { return &___canseek_14; }
	inline void set_canseek_14(bool value)
	{
		___canseek_14 = value;
	}

	inline static int32_t get_offset_of_anonymous_15() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___anonymous_15)); }
	inline bool get_anonymous_15() const { return ___anonymous_15; }
	inline bool* get_address_of_anonymous_15() { return &___anonymous_15; }
	inline void set_anonymous_15(bool value)
	{
		___anonymous_15 = value;
	}

	inline static int32_t get_offset_of_buf_dirty_16() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_dirty_16)); }
	inline bool get_buf_dirty_16() const { return ___buf_dirty_16; }
	inline bool* get_address_of_buf_dirty_16() { return &___buf_dirty_16; }
	inline void set_buf_dirty_16(bool value)
	{
		___buf_dirty_16 = value;
	}

	inline static int32_t get_offset_of_buf_size_17() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_size_17)); }
	inline int32_t get_buf_size_17() const { return ___buf_size_17; }
	inline int32_t* get_address_of_buf_size_17() { return &___buf_size_17; }
	inline void set_buf_size_17(int32_t value)
	{
		___buf_size_17 = value;
	}

	inline static int32_t get_offset_of_buf_length_18() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_length_18)); }
	inline int32_t get_buf_length_18() const { return ___buf_length_18; }
	inline int32_t* get_address_of_buf_length_18() { return &___buf_length_18; }
	inline void set_buf_length_18(int32_t value)
	{
		___buf_length_18 = value;
	}

	inline static int32_t get_offset_of_buf_offset_19() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_offset_19)); }
	inline int32_t get_buf_offset_19() const { return ___buf_offset_19; }
	inline int32_t* get_address_of_buf_offset_19() { return &___buf_offset_19; }
	inline void set_buf_offset_19(int32_t value)
	{
		___buf_offset_19 = value;
	}

	inline static int32_t get_offset_of_buf_start_20() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418, ___buf_start_20)); }
	inline int64_t get_buf_start_20() const { return ___buf_start_20; }
	inline int64_t* get_address_of_buf_start_20() { return &___buf_start_20; }
	inline void set_buf_start_20(int64_t value)
	{
		___buf_start_20 = value;
	}
};

struct FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418_StaticFields
{
public:
	// System.Byte[] System.IO.FileStream::buf_recycle
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_recycle_4;
	// System.Object System.IO.FileStream::buf_recycle_lock
	RuntimeObject * ___buf_recycle_lock_5;

public:
	inline static int32_t get_offset_of_buf_recycle_4() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418_StaticFields, ___buf_recycle_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_recycle_4() const { return ___buf_recycle_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_recycle_4() { return &___buf_recycle_4; }
	inline void set_buf_recycle_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_recycle_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buf_recycle_4), (void*)value);
	}

	inline static int32_t get_offset_of_buf_recycle_lock_5() { return static_cast<int32_t>(offsetof(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418_StaticFields, ___buf_recycle_lock_5)); }
	inline RuntimeObject * get_buf_recycle_lock_5() const { return ___buf_recycle_lock_5; }
	inline RuntimeObject ** get_address_of_buf_recycle_lock_5() { return &___buf_recycle_lock_5; }
	inline void set_buf_recycle_lock_5(RuntimeObject * value)
	{
		___buf_recycle_lock_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buf_recycle_lock_5), (void*)value);
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};


// System.Threading.EventWaitHandle
struct  EventWaitHandle_t7603BF1D3D30FE42DD07A450C8D09E2684DC4D98  : public WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6
{
public:

public:
};


// mixpanel.queue.PendingWriteException
struct  PendingWriteException_tD008EED8A9DD4BB29844EF57D33275EE9A882F48  : public Exception_t
{
public:
	// System.Exception[] mixpanel.queue.PendingWriteException::_pendingWritesExceptions
	ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209* ____pendingWritesExceptions_17;

public:
	inline static int32_t get_offset_of__pendingWritesExceptions_17() { return static_cast<int32_t>(offsetof(PendingWriteException_tD008EED8A9DD4BB29844EF57D33275EE9A882F48, ____pendingWritesExceptions_17)); }
	inline ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209* get__pendingWritesExceptions_17() const { return ____pendingWritesExceptions_17; }
	inline ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209** get_address_of__pendingWritesExceptions_17() { return &____pendingWritesExceptions_17; }
	inline void set__pendingWritesExceptions_17(ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209* value)
	{
		____pendingWritesExceptions_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____pendingWritesExceptions_17), (void*)value);
	}
};


// mixpanel.queue.PersistentQueueOperation
struct  PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69  : public RuntimeObject
{
public:
	// mixpanel.queue.PersistentQueueOperationTypes mixpanel.queue.PersistentQueueOperation::Type
	uint8_t ___Type_0;
	// System.Int32 mixpanel.queue.PersistentQueueOperation::FileNumber
	int32_t ___FileNumber_1;
	// System.Int32 mixpanel.queue.PersistentQueueOperation::Start
	int32_t ___Start_2;
	// System.Int32 mixpanel.queue.PersistentQueueOperation::Length
	int32_t ___Length_3;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69, ___Type_0)); }
	inline uint8_t get_Type_0() const { return ___Type_0; }
	inline uint8_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(uint8_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_FileNumber_1() { return static_cast<int32_t>(offsetof(PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69, ___FileNumber_1)); }
	inline int32_t get_FileNumber_1() const { return ___FileNumber_1; }
	inline int32_t* get_address_of_FileNumber_1() { return &___FileNumber_1; }
	inline void set_FileNumber_1(int32_t value)
	{
		___FileNumber_1 = value;
	}

	inline static int32_t get_offset_of_Start_2() { return static_cast<int32_t>(offsetof(PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69, ___Start_2)); }
	inline int32_t get_Start_2() const { return ___Start_2; }
	inline int32_t* get_address_of_Start_2() { return &___Start_2; }
	inline void set_Start_2(int32_t value)
	{
		___Start_2 = value;
	}

	inline static int32_t get_offset_of_Length_3() { return static_cast<int32_t>(offsetof(PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69, ___Length_3)); }
	inline int32_t get_Length_3() const { return ___Length_3; }
	inline int32_t* get_address_of_Length_3() { return &___Length_3; }
	inline void set_Length_3(int32_t value)
	{
		___Length_3 = value;
	}
};


// System.Action
struct  Action_t591D2A86165F896B4B800BB5C25CE18672A55579  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<System.IO.Stream>
struct  Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621  : public MulticastDelegate_t
{
public:

public:
};


// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.IO.Stream,System.Int64>
struct  Func_2_t4D4D87A23A55C4AC43EDC08F16D4C8A88C4748A7  : public MulticastDelegate_t
{
public:

public:
};


// System.IO.IOException
struct  IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.IO.IOException::_maybeFullPath
	String_t* ____maybeFullPath_17;

public:
	inline static int32_t get_offset_of__maybeFullPath_17() { return static_cast<int32_t>(offsetof(IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA, ____maybeFullPath_17)); }
	inline String_t* get__maybeFullPath_17() const { return ____maybeFullPath_17; }
	inline String_t** get_address_of__maybeFullPath_17() { return &____maybeFullPath_17; }
	inline void set__maybeFullPath_17(String_t* value)
	{
		____maybeFullPath_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____maybeFullPath_17), (void*)value);
	}
};


// System.Threading.ManualResetEvent
struct  ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408  : public EventWaitHandle_t7603BF1D3D30FE42DD07A450C8D09E2684DC4D98
{
public:

public:
};


// System.IO.EndOfStreamException
struct  EndOfStreamException_t1B47BA867EC337F83056C2833A59293754AAC01F  : public IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Threading.WaitHandle[]
struct WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6 * m_Items[1];

public:
	inline WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Exception[]
struct ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Exception_t * m_Items[1];

public:
	inline Exception_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Exception_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Exception_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Exception_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Exception_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Exception_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !1 System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Value()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t KeyValuePair_2_get_Value_mF7293AF44DA0B8EB7B455A6227F7C36EEE9CF508_gshared_inline (KeyValuePair_2_tA9AFBC865B07606ED8F020A8E3AF8E27491AF809 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Key()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t KeyValuePair_2_get_Key_mB735BC2D7232A3B45D667D28C17BA51AAAFFB4A1_gshared_inline (KeyValuePair_2_tA9AFBC865B07606ED8F020A8E3AF8E27491AF809 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mEE468B81D8E7C140F567D10FF7F5894A50EEEA57_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___capacity0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Func`2<System.Object,System.Int64>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_m5209BD6F70BB1BC7C5782B4E74474ECF1A28EF11_gshared (Func_2_t82B378444729B3A0021330940E7867B6D348F383 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD  List_1_GetEnumerator_m52CC760E475D226A2B75048D70C4E22692F9F68D_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_m94D0DAE031619503CDA6E53C5C3CC78AF3139472_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Take_TisRuntimeObject_m6BC91C34C8D9D0A0A7976885C7C7A2EB9F4D0D69_gshared (RuntimeObject* ___source0, int32_t ___count1, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* Enumerable_ToArray_TisRuntimeObject_m2C80338C3467FFC454C3E8E844DB4CEDEEE63F52_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::Invoke(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1_Invoke_mB86FC1B303E77C41ED0E94FC3592A9CF8DA571D5_gshared (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method);

// System.Void mixpanel.queue.PersistentQueue/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m00FCFC696FF235951542ED821F5BCFB62416EE20 (U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void mixpanel.queue.PersistentQueueOperation::.ctor(mixpanel.queue.PersistentQueueOperationTypes,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueOperation__ctor_mA52A128EF311BED74241CE8F393634EC0B61B9A6 (PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * __this, uint8_t ___type0, int32_t ___fileNumber1, int32_t ___start2, int32_t ___length3, const RuntimeMethod* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Value()
inline int32_t KeyValuePair_2_get_Value_mF7293AF44DA0B8EB7B455A6227F7C36EEE9CF508_inline (KeyValuePair_2_tA9AFBC865B07606ED8F020A8E3AF8E27491AF809 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (KeyValuePair_2_tA9AFBC865B07606ED8F020A8E3AF8E27491AF809 *, const RuntimeMethod*))KeyValuePair_2_get_Value_mF7293AF44DA0B8EB7B455A6227F7C36EEE9CF508_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Key()
inline int32_t KeyValuePair_2_get_Key_mB735BC2D7232A3B45D667D28C17BA51AAAFFB4A1_inline (KeyValuePair_2_tA9AFBC865B07606ED8F020A8E3AF8E27491AF809 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (KeyValuePair_2_tA9AFBC865B07606ED8F020A8E3AF8E27491AF809 *, const RuntimeMethod*))KeyValuePair_2_get_Key_mB735BC2D7232A3B45D667D28C17BA51AAAFFB4A1_gshared_inline)(__this, method);
}
// System.Void System.IO.BinaryReader::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BinaryReader__ctor_mD134893EA93809AFA29B025FF7439B82C35FE55F (BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___input0, const RuntimeMethod* method);
// System.Void mixpanel.queue.PersistentQueue/<>c__DisplayClass38_1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass38_1__ctor_m935637813A6919065A7B59751E3FD7DA972E1081 (U3CU3Ec__DisplayClass38_1_t51468321D6041881A73633BB17566E2FDE8E4269 * __this, const RuntimeMethod* method);
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760 (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void mixpanel.queue.PersistentQueue::AssertTransactionSeparator(System.IO.BinaryReader,System.Int32,mixpanel.queue.PersistentQueueMarkers,System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueue_AssertTransactionSeparator_m78708D03928BD590ED08A0EBF751B051403A5473 (PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * __this, BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * ___binaryReader0, int32_t ___txCount1, int32_t ___whichSeparator2, Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___hasData3, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<mixpanel.queue.PersistentQueueOperation>::.ctor(System.Int32)
inline void List_1__ctor_m4701F78844832952C66B59FAE9BDD3432B391F79 (List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 * __this, int32_t ___capacity0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 *, int32_t, const RuntimeMethod*))List_1__ctor_mEE468B81D8E7C140F567D10FF7F5894A50EEEA57_gshared)(__this, ___capacity0, method);
}
// System.Void mixpanel.queue.PersistentQueue::AssertOperationSeparator(System.IO.BinaryReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueue_AssertOperationSeparator_m346D83803A2A4B1E31B79BFF49B23B9D4298131E (PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * __this, BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * ___reader0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<mixpanel.queue.PersistentQueueOperation>::Add(!0)
inline void List_1_Add_m9FF374C04A41E750BACA841918306E6559FADD0D (List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 * __this, PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 *, PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, ___item0, method);
}
// System.Void mixpanel.queue.PersistentQueue::ApplyTransactionOperations(System.Collections.Generic.IEnumerable`1<mixpanel.queue.PersistentQueueOperation>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueue_ApplyTransactionOperations_m0A6E07FEBC317628A77FF113DECEDD300F899BA0 (PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * __this, RuntimeObject* ___operations0, const RuntimeMethod* method);
// System.Void mixpanel.queue.PersistentQueueEntry::.ctor(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueEntry__ctor_mEB206A35F980CD0A8FC898AF39D9CAD87D75EFE8 (PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * __this, int32_t ___fileNumber0, int32_t ___start1, int32_t ___length2, const RuntimeMethod* method);
// System.Boolean mixpanel.queue.PersistentQueueEntry::Equals(mixpanel.queue.PersistentQueueEntry)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PersistentQueueEntry_Equals_m458126F0D5A8BBFECD646B5E2CED80113BFAAF41 (PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * __this, PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * ___obj0, const RuntimeMethod* method);
// System.Boolean System.Object::Equals(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_Equals_mD98CD6D19C28ADC48B8468F78F94D38E203D0521 (RuntimeObject * ___objA0, RuntimeObject * ___objB1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<mixpanel.queue.PersistentQueueOperation>::.ctor()
inline void List_1__ctor_m0DE87A5A6E3E1768A8A8B4BA660934BE2600D063 (List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Exception>::.ctor()
inline void List_1__ctor_m3CA9F30DC986E649F8E82AD69F5085D355D91AC1 (List_1_t864E43C85FFFD16A2A0685F30AC877138E106F2A * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t864E43C85FFFD16A2A0685F30AC877138E106F2A *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Threading.WaitHandle>::.ctor()
inline void List_1__ctor_m450486C7FD0DA7D1068FD8977B5A99FC51F62307 (List_1_tE87EDEDF7A49228B90A7C51CFFD8114D62D5EF30 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE87EDEDF7A49228B90A7C51CFFD8114D62D5EF30 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.IO.Stream>::.ctor()
inline void List_1__ctor_m33EBEFF81F45499476516DAAA3BE06259CDDF6B5 (List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Byte[]>::.ctor()
inline void List_1__ctor_m52FC81AB50BD21B6BFA16546E3AF9C94611D9811 (List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void System.Threading.Monitor::Enter(System.Object,System.Boolean&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5 (RuntimeObject * ___obj0, bool* ___lockTaken1, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Exit(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2 (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void mixpanel.queue.PersistentQueueSession::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueSession_Dispose_mCCAC09F6A1BADB8E297CD0F42ED28178152D0A37 (PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * __this, const RuntimeMethod* method);
// System.Void System.Object::Finalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Finalize_m4015B7D3A44DE125C5FE34D7276CD4697C06F380 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Byte[]>::Add(!0)
inline void List_1_Add_mCC9D38BB3CBE3F2E67EDF3390D36ABFAD293468E (List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 *, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, ___item0, method);
}
// System.Void mixpanel.queue.PersistentQueueSession::AsyncFlushBuffer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueSession_AsyncFlushBuffer_m8FD2C50C3F6884F965490030895187710E0601A7 (PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * __this, const RuntimeMethod* method);
// System.Void System.Func`2<System.IO.Stream,System.Int64>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m29194A2FFE45ED9FF0FB5E4DEE6861B3979C0CB0 (Func_2_t4D4D87A23A55C4AC43EDC08F16D4C8A88C4748A7 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t4D4D87A23A55C4AC43EDC08F16D4C8A88C4748A7 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_m5209BD6F70BB1BC7C5782B4E74474ECF1A28EF11_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Action`1<System.IO.Stream>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m948E1585F7105BAF7467A6E48FE716E71CD4BAB9 (Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared)(__this, ___object0, ___method1, method);
}
// System.Void mixpanel.queue.PersistentQueue::AcquireWriter(System.IO.Stream,System.Func`2<System.IO.Stream,System.Int64>,System.Action`1<System.IO.Stream>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueue_AcquireWriter_mC946B341A323EFB01E23822AF9FC82CA1DFE7BFA (PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream0, Func_2_t4D4D87A23A55C4AC43EDC08F16D4C8A88C4748A7 * ___action1, Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 * ___onReplaceStream2, const RuntimeMethod* method);
// System.Void mixpanel.queue.PersistentQueueSession/<>c__DisplayClass15_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass15_0__ctor_mDB92AB36CC844BB6EC46B22255B0234DD58CDC20 (U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C * __this, const RuntimeMethod* method);
// System.Byte[] mixpanel.queue.PersistentQueueSession::ConcatenateBufferAndAddIndividualOperations(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* PersistentQueueSession_ConcatenateBufferAndAddIndividualOperations_m1F4735CE325637612B78A6DC6D8F7C871FA21894 (PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream0, const RuntimeMethod* method);
// System.Void System.Threading.ManualResetEvent::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManualResetEvent__ctor_m8973D9E3C622B9602641C017A33870F51D0311E1 (ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * __this, bool ___initialState0, const RuntimeMethod* method);
// System.Void System.AsyncCallback::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncCallback__ctor_m36AEE927E6AFEE950656BC5F7841682D23DE2EBB (AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Byte[]>::GetEnumerator()
inline Enumerator_t4A0753267EA56CCB5A11BD47A5A39F7DC4154DB1  List_1_GetEnumerator_m1E687BD559DDDC595828B26A476F527A1FA5F7A4 (List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t4A0753267EA56CCB5A11BD47A5A39F7DC4154DB1  (*) (List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 *, const RuntimeMethod*))List_1_GetEnumerator_m52CC760E475D226A2B75048D70C4E22692F9F68D_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.Byte[]>::get_Current()
inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* Enumerator_get_Current_m2DA2B2533227571D1B3AB4AFBA0E69F5484E1F73_inline (Enumerator_t4A0753267EA56CCB5A11BD47A5A39F7DC4154DB1 * __this, const RuntimeMethod* method)
{
	return ((  ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* (*) (Enumerator_t4A0753267EA56CCB5A11BD47A5A39F7DC4154DB1 *, const RuntimeMethod*))Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline)(__this, method);
}
// System.Int32 mixpanel.queue.PersistentQueue::get_CurrentFileNumber()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t PersistentQueue_get_CurrentFileNumber_mEA54919870FD34A7E69FCD761993FB1C592B3986_inline (PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * __this, const RuntimeMethod* method);
// System.Void System.Buffer::BlockCopy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353 (RuntimeArray * ___src0, int32_t ___srcOffset1, RuntimeArray * ___dst2, int32_t ___dstOffset3, int32_t ___count4, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Byte[]>::MoveNext()
inline bool Enumerator_MoveNext_m386C5CC04A468880E8A50EBF6086B47EB2461FCB (Enumerator_t4A0753267EA56CCB5A11BD47A5A39F7DC4154DB1 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t4A0753267EA56CCB5A11BD47A5A39F7DC4154DB1 *, const RuntimeMethod*))Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte[]>::Dispose()
inline void Enumerator_Dispose_m4A7857ABA06AF87BCEB0EB05F49C6DEF148DC27F (Enumerator_t4A0753267EA56CCB5A11BD47A5A39F7DC4154DB1 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t4A0753267EA56CCB5A11BD47A5A39F7DC4154DB1 *, const RuntimeMethod*))Enumerator_Dispose_m94D0DAE031619503CDA6E53C5C3CC78AF3139472_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Byte[]>::Clear()
inline void List_1_Clear_m458DDFB5AB146FD65D91BAF36907B40AAD1585B3 (List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 *, const RuntimeMethod*))List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.IO.Stream>::Add(!0)
inline void List_1_Add_mB306C980BF53D3411577EE0AF430FF5F9B1D2605 (List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF *, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, ___item0, method);
}
// mixpanel.queue.PersistentQueueEntry mixpanel.queue.PersistentQueue::Dequeue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * PersistentQueue_Dequeue_m71899BBB07161B055C5337E38FB9084AF015413A (PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * __this, const RuntimeMethod* method);
// System.Boolean mixpanel.queue.PersistentQueueEntry::op_Equality(mixpanel.queue.PersistentQueueEntry,mixpanel.queue.PersistentQueueEntry)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PersistentQueueEntry_op_Equality_mEA0F62EB5F3B72A8C241D0B6A3D5A2B223D0DAB4 (PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * ___left0, PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * ___right1, const RuntimeMethod* method);
// System.Void mixpanel.queue.PersistentQueueSession::WaitForPendingWrites()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueSession_WaitForPendingWrites_m105E3D4E7EA55B5E947BF87E12A6873E9299BFBE (PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * __this, const RuntimeMethod* method);
// System.Void mixpanel.queue.PersistentQueueSession::SyncFlushBuffer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueSession_SyncFlushBuffer_m2F6D9D269AC0D30E9976A4AC3A09B9A576D196D8 (PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.IO.Stream>::GetEnumerator()
inline Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55  List_1_GetEnumerator_mB6287B4A0B81DE7915896C1771FD16C182091404 (List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55  (*) (List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF *, const RuntimeMethod*))List_1_GetEnumerator_m52CC760E475D226A2B75048D70C4E22692F9F68D_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.IO.Stream>::get_Current()
inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * Enumerator_get_Current_mDD08B6F8CE246828D34AF51A4B4A5CB333C9A4AE_inline (Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55 * __this, const RuntimeMethod* method)
{
	return ((  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * (*) (Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55 *, const RuntimeMethod*))Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline)(__this, method);
}
// System.Void System.IO.Stream::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Stream_Dispose_m186A8E680F2528DEDFF8F0069CC33BD813FFB1C7 (Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.IO.Stream>::MoveNext()
inline bool Enumerator_MoveNext_m78855BD085E859C5228061D642FD29C4161D7284 (Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55 *, const RuntimeMethod*))Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.IO.Stream>::Dispose()
inline void Enumerator_Dispose_m075340F31C13EF2A612E40C80988C9E651B7F829 (Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55 *, const RuntimeMethod*))Enumerator_Dispose_m94D0DAE031619503CDA6E53C5C3CC78AF3139472_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.IO.Stream>::Clear()
inline void List_1_Clear_m1465A2F72D3985860367250F2F3CA0FF83A09D63 (List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF *, const RuntimeMethod*))List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC_gshared)(__this, method);
}
// System.Void mixpanel.queue.PersistentQueue::CommitTransaction(System.Collections.Generic.ICollection`1<mixpanel.queue.PersistentQueueOperation>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueue_CommitTransaction_m6F9DB4D286D328A89EFC0687F9EC6BEFF4105F4E (PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * __this, RuntimeObject* ___operations0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<mixpanel.queue.PersistentQueueOperation>::Clear()
inline void List_1_Clear_mBBF7F2EBBE5FD254B09CE99D16F6BB0C340CEA50 (List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 *, const RuntimeMethod*))List_1_Clear_mC5CFC6C9F3007FC24FE020198265D4B5B0659FFC_gshared)(__this, method);
}
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<System.Threading.WaitHandle>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
inline RuntimeObject* Enumerable_Take_TisWaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_mD6127D3008326E62523DDEBDCA22C2623A48076C (RuntimeObject* ___source0, int32_t ___count1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, int32_t, const RuntimeMethod*))Enumerable_Take_TisRuntimeObject_m6BC91C34C8D9D0A0A7976885C7C7A2EB9F4D0D69_gshared)(___source0, ___count1, method);
}
// !!0[] System.Linq.Enumerable::ToArray<System.Threading.WaitHandle>(System.Collections.Generic.IEnumerable`1<!!0>)
inline WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* Enumerable_ToArray_TisWaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_mB82DA928C5644B54EA8198BD2135F4F6D579928F (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisRuntimeObject_m2C80338C3467FFC454C3E8E844DB4CEDEEE63F52_gshared)(___source0, method);
}
// System.Boolean System.Threading.WaitHandle::WaitAll(System.Threading.WaitHandle[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WaitHandle_WaitAll_m83A50834628130B07B1A5BC9FB2968C9F78B1A23 (WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* ___waitHandles0, const RuntimeMethod* method);
// System.Void mixpanel.queue.PersistentQueueSession::AssertNoPendingWritesFailures()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueSession_AssertNoPendingWritesFailures_mEBC84FCA8E6D166C6AB29880C9B76C80F1C426AF (PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * __this, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<System.Exception>(System.Collections.Generic.IEnumerable`1<!!0>)
inline ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209* Enumerable_ToArray_TisException_t_mC45E629FDAFE5B708F9F65A7458539250D8461A7 (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisRuntimeObject_m2C80338C3467FFC454C3E8E844DB4CEDEEE63F52_gshared)(___source0, method);
}
// System.Void mixpanel.queue.PendingWriteException::.ctor(System.Exception[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PendingWriteException__ctor_m21342C30EFE1762297E1091AEDE3A3EBA2E693C2 (PendingWriteException_tD008EED8A9DD4BB29844EF57D33275EE9A882F48 * __this, ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209* ___pendingWritesExceptions0, const RuntimeMethod* method);
// System.Void mixpanel.queue.PersistentQueue::Reinstate(System.Collections.Generic.IEnumerable`1<mixpanel.queue.PersistentQueueOperation>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueue_Reinstate_mD630C130E071829D27A91175CC9A36C74CC5DEC7 (PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * __this, RuntimeObject* ___reinstatedOperations0, const RuntimeMethod* method);
// System.Void System.GC::SuppressFinalize(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GC_SuppressFinalize_m037319A9B95A5BA437E806DE592802225EE5B425 (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void System.Threading.Thread::Sleep(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Thread_Sleep_m2CD320EAB7BE02CC1F395EAFE9970D53A5F9EAEF (int32_t ___millisecondsTimeout0, const RuntimeMethod* method);
// System.Boolean System.Threading.EventWaitHandle::Set()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool EventWaitHandle_Set_m7959A86A39735296FC949EC86FDA42A6CFAAB94C (EventWaitHandle_t7603BF1D3D30FE42DD07A450C8D09E2684DC4D98 * __this, const RuntimeMethod* method);
// System.Void System.IO.FileStream::.ctor(System.String,System.IO.FileMode,System.IO.FileAccess,System.IO.FileShare,System.Int32,System.IO.FileOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FileStream__ctor_mE7B19C44ACADF935195F4209CECBC3EF962987C9 (FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * __this, String_t* ___path0, int32_t ___mode1, int32_t ___access2, int32_t ___share3, int32_t ___bufferSize4, int32_t ___options5, const RuntimeMethod* method);
// System.IO.FileStream mixpanel.queue.PersistentQueueUtils::CreateFileStream(System.String,System.IO.FileMode,System.IO.FileAccess,System.IO.FileOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * PersistentQueueUtils_CreateFileStream_m2499F0F23E8AD0EC95EE99007F7B399B2F86C26F (String_t* ___path0, int32_t ___mode1, int32_t ___access2, int32_t ___options3, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Boolean System.IO.File::Exists(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool File_Exists_m6B9BDD8EEB33D744EB0590DD27BC0152FAFBD1FB (String_t* ___path0, const RuntimeMethod* method);
// System.Boolean mixpanel.queue.PersistentQueueUtils::WaitDelete(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PersistentQueueUtils_WaitDelete_m11EE50B1C6DEE458F410146BAFF11CE1A8AA7195 (String_t* ___s0, const RuntimeMethod* method);
// System.Void System.IO.File::Move(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void File_Move_mBBAEF2F3353F8E46E70495C88E1735C866E953B1 (String_t* ___sourceFileName0, String_t* ___destFileName1, const RuntimeMethod* method);
// System.IO.FileStream mixpanel.queue.PersistentQueueUtils::CreateReadStream(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * PersistentQueueUtils_CreateReadStream_mB2A55B03F4A2A9A35139A87EEDF0520B5FE56981 (String_t* ___path0, const RuntimeMethod* method);
// System.Void System.Action`1<System.IO.Stream>::Invoke(!0)
inline void Action_1_Invoke_m25501B02ECA680749841E48EEA596F763A82F475 (Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 *, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 *, const RuntimeMethod*))Action_1_Invoke_mB86FC1B303E77C41ED0E94FC3592A9CF8DA571D5_gshared)(__this, ___obj0, method);
}
// System.IO.FileStream mixpanel.queue.PersistentQueueUtils::CreateWriteStream(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * PersistentQueueUtils_CreateWriteStream_mFB0597CC35C0FC6AA0D37C9D58D059B9C79B5A26 (String_t* ___path0, const RuntimeMethod* method);
// System.Void System.IO.File::Delete(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void File_Delete_mBE814E569EAB07FAD140C6DCDB957F1CB8C85DE2 (String_t* ___path0, const RuntimeMethod* method);
// System.Byte[] System.BitConverter::GetBytes(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* BitConverter_GetBytes_mB5BCBAAFE3AE14F2AF1731187C7155A236DF38EA (int32_t ___value0, const RuntimeMethod* method);
// System.Void System.Guid::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Guid__ctor_mC668142577A40A77D13B78AADDEFFFC2E2705079 (Guid_t * __this, String_t* ___g0, const RuntimeMethod* method);
// System.Byte[] System.Guid::ToByteArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* Guid_ToByteArray_m5E99B09A26EA3A1943CC8FE697E247DAC5465223 (Guid_t * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void mixpanel.queue.PersistentQueue_<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m268F61E56FB7E186C29B6D2830EDB6EEC86F0B06 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_m268F61E56FB7E186C29B6D2830EDB6EEC86F0B06_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591 * L_0 = (U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591 *)il2cpp_codegen_object_new(U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m00FCFC696FF235951542ED821F5BCFB62416EE20(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void mixpanel.queue.PersistentQueue_<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m00FCFC696FF235951542ED821F5BCFB62416EE20 (U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean mixpanel.queue.PersistentQueue_<>c::<Reinstate>b__37_0(mixpanel.queue.PersistentQueueOperation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3CReinstateU3Eb__37_0_mCBB99DEC8C9ECD295A8DF11DE805FCC9ED9B0C5C (U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591 * __this, PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * ___entry0, const RuntimeMethod* method)
{
	{
		// where entry.Type == PersistentQueueOperationTypes.DEQUEUE
		PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * L_0 = ___entry0;
		NullCheck(L_0);
		uint8_t L_1 = L_0->get_Type_0();
		return (bool)((((int32_t)L_1) == ((int32_t)2))? 1 : 0);
	}
}
// mixpanel.queue.PersistentQueueOperation mixpanel.queue.PersistentQueue_<>c::<Reinstate>b__37_1(mixpanel.queue.PersistentQueueOperation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * U3CU3Ec_U3CReinstateU3Eb__37_1_mD52A96913C7A99BAC86F3125947ED3B8740E545D (U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591 * __this, PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * ___entry0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CReinstateU3Eb__37_1_mD52A96913C7A99BAC86F3125947ED3B8740E545D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// select new PersistentQueueOperation(
		//     PersistentQueueOperationTypes.REINSTATE,
		//     entry.FileNumber,
		//     entry.Start,
		//     entry.Length
		// )
		PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * L_0 = ___entry0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_FileNumber_1();
		PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * L_2 = ___entry0;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_Start_2();
		PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * L_4 = ___entry0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_Length_3();
		PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * L_6 = (PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 *)il2cpp_codegen_object_new(PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69_il2cpp_TypeInfo_var);
		PersistentQueueOperation__ctor_mA52A128EF311BED74241CE8F393634EC0B61B9A6(L_6, 3, L_1, L_3, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void mixpanel.queue.PersistentQueue_<>c::<ReadTransactionLog>b__38_2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CReadTransactionLogU3Eb__38_2_m9D396619BE3FC0B707F2231A4A5F85092F181DE9 (U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591 * __this, const RuntimeMethod* method)
{
	{
		// AssertTransactionSeparator(binaryReader, txCount, PersistentQueueMarkers.END, () => { });
		return;
	}
}
// System.Boolean mixpanel.queue.PersistentQueue_<>c::<ApplyTransactionOperationsInMemory>b__42_0(System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3CApplyTransactionOperationsInMemoryU3Eb__42_0_mA7A26B9DDD543B2482EFA045F321C7FD0A14E305 (U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591 * __this, KeyValuePair_2_tA9AFBC865B07606ED8F020A8E3AF8E27491AF809  ___pair0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CApplyTransactionOperationsInMemoryU3Eb__42_0_mA7A26B9DDD543B2482EFA045F321C7FD0A14E305_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_tA9AFBC865B07606ED8F020A8E3AF8E27491AF809  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// where pair.Value < 1
		KeyValuePair_2_tA9AFBC865B07606ED8F020A8E3AF8E27491AF809  L_0 = ___pair0;
		V_0 = L_0;
		int32_t L_1 = KeyValuePair_2_get_Value_mF7293AF44DA0B8EB7B455A6227F7C36EEE9CF508_inline((KeyValuePair_2_tA9AFBC865B07606ED8F020A8E3AF8E27491AF809 *)(&V_0), /*hidden argument*/KeyValuePair_2_get_Value_mF7293AF44DA0B8EB7B455A6227F7C36EEE9CF508_RuntimeMethod_var);
		return (bool)((((int32_t)L_1) < ((int32_t)1))? 1 : 0);
	}
}
// System.Int32 mixpanel.queue.PersistentQueue_<>c::<ApplyTransactionOperationsInMemory>b__42_1(System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t U3CU3Ec_U3CApplyTransactionOperationsInMemoryU3Eb__42_1_m500314484BD87AFD566F2CA81A29CACF24E883CA (U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591 * __this, KeyValuePair_2_tA9AFBC865B07606ED8F020A8E3AF8E27491AF809  ___pair0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CApplyTransactionOperationsInMemoryU3Eb__42_1_m500314484BD87AFD566F2CA81A29CACF24E883CA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_tA9AFBC865B07606ED8F020A8E3AF8E27491AF809  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// select pair.Key
		KeyValuePair_2_tA9AFBC865B07606ED8F020A8E3AF8E27491AF809  L_0 = ___pair0;
		V_0 = L_0;
		int32_t L_1 = KeyValuePair_2_get_Key_mB735BC2D7232A3B45D667D28C17BA51AAAFFB4A1_inline((KeyValuePair_2_tA9AFBC865B07606ED8F020A8E3AF8E27491AF809 *)(&V_0), /*hidden argument*/KeyValuePair_2_get_Key_mB735BC2D7232A3B45D667D28C17BA51AAAFFB4A1_RuntimeMethod_var);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void mixpanel.queue.PersistentQueue_<>c__DisplayClass38_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass38_0__ctor_mF3BE6B971EE414563D3A926795612B9FCC60ABAC (U3CU3Ec__DisplayClass38_0_t151099A4060264DDAAECA19075B7DD121A8DB2B7 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void mixpanel.queue.PersistentQueue_<>c__DisplayClass38_0::<ReadTransactionLog>b__0(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass38_0_U3CReadTransactionLogU3Eb__0_m94D5F9520C93079C88B6383B50CFADAF79C1E571 (U3CU3Ec__DisplayClass38_0_t151099A4060264DDAAECA19075B7DD121A8DB2B7 * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass38_0_U3CReadTransactionLogU3Eb__0_m94D5F9520C93079C88B6383B50CFADAF79C1E571_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * V_0 = NULL;
	U3CU3Ec__DisplayClass38_1_t51468321D6041881A73633BB17566E2FDE8E4269 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 * V_4 = NULL;
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * V_5 = NULL;
	int32_t V_6 = 0;
	PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * V_7 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * G_B5_0 = NULL;
	int32_t G_B5_1 = 0;
	int32_t G_B5_2 = 0;
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * G_B5_3 = NULL;
	PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * G_B5_4 = NULL;
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * G_B4_0 = NULL;
	int32_t G_B4_1 = 0;
	int32_t G_B4_2 = 0;
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * G_B4_3 = NULL;
	PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * G_B4_4 = NULL;
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * G_B12_0 = NULL;
	int32_t G_B12_1 = 0;
	int32_t G_B12_2 = 0;
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * G_B12_3 = NULL;
	PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * G_B12_4 = NULL;
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * G_B11_0 = NULL;
	int32_t G_B11_1 = 0;
	int32_t G_B11_2 = 0;
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * G_B11_3 = NULL;
	PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * G_B11_4 = NULL;
	{
		// using (var binaryReader = new BinaryReader(stream))
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_0 = ___stream0;
		BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * L_1 = (BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 *)il2cpp_codegen_object_new(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969_il2cpp_TypeInfo_var);
		BinaryReader__ctor_mD134893EA93809AFA29B025FF7439B82C35FE55F(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			U3CU3Ec__DisplayClass38_1_t51468321D6041881A73633BB17566E2FDE8E4269 * L_2 = (U3CU3Ec__DisplayClass38_1_t51468321D6041881A73633BB17566E2FDE8E4269 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass38_1_t51468321D6041881A73633BB17566E2FDE8E4269_il2cpp_TypeInfo_var);
			U3CU3Ec__DisplayClass38_1__ctor_m935637813A6919065A7B59751E3FD7DA972E1081(L_2, /*hidden argument*/NULL);
			V_1 = L_2;
			// bool readingTransaction = false;
			U3CU3Ec__DisplayClass38_1_t51468321D6041881A73633BB17566E2FDE8E4269 * L_3 = V_1;
			NullCheck(L_3);
			L_3->set_readingTransaction_0((bool)0);
		}

IL_0014:
		try
		{ // begin try (depth: 2)
			{
				// int txCount = 0;
				V_2 = 0;
			}

IL_0016:
			{
				// txCount += 1;
				int32_t L_4 = V_2;
				V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
				// AssertTransactionSeparator(binaryReader, txCount, PersistentQueueMarkers.START,
				//     () => readingTransaction = true);
				PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * L_5 = __this->get_U3CU3E4__this_0();
				BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * L_6 = V_0;
				int32_t L_7 = V_2;
				U3CU3Ec__DisplayClass38_1_t51468321D6041881A73633BB17566E2FDE8E4269 * L_8 = V_1;
				NullCheck(L_8);
				Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_9 = L_8->get_U3CU3E9__1_1();
				Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_10 = L_9;
				G_B4_0 = L_10;
				G_B4_1 = 0;
				G_B4_2 = L_7;
				G_B4_3 = L_6;
				G_B4_4 = L_5;
				if (L_10)
				{
					G_B5_0 = L_10;
					G_B5_1 = 0;
					G_B5_2 = L_7;
					G_B5_3 = L_6;
					G_B5_4 = L_5;
					goto IL_0044;
				}
			}

IL_002c:
			{
				U3CU3Ec__DisplayClass38_1_t51468321D6041881A73633BB17566E2FDE8E4269 * L_11 = V_1;
				U3CU3Ec__DisplayClass38_1_t51468321D6041881A73633BB17566E2FDE8E4269 * L_12 = V_1;
				Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_13 = (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 *)il2cpp_codegen_object_new(Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var);
				Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760(L_13, L_12, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass38_1_U3CReadTransactionLogU3Eb__1_m809233E2E75548C019762CF93199AF104078E077_RuntimeMethod_var), /*hidden argument*/NULL);
				Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_14 = L_13;
				V_5 = L_14;
				NullCheck(L_11);
				L_11->set_U3CU3E9__1_1(L_14);
				Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_15 = V_5;
				G_B5_0 = L_15;
				G_B5_1 = G_B4_1;
				G_B5_2 = G_B4_2;
				G_B5_3 = G_B4_3;
				G_B5_4 = G_B4_4;
			}

IL_0044:
			{
				NullCheck(G_B5_4);
				PersistentQueue_AssertTransactionSeparator_m78708D03928BD590ED08A0EBF751B051403A5473(G_B5_4, G_B5_3, G_B5_2, G_B5_1, G_B5_0, /*hidden argument*/NULL);
				// var opsCount = binaryReader.ReadInt32();
				BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * L_16 = V_0;
				NullCheck(L_16);
				int32_t L_17 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_16);
				V_3 = L_17;
				// var txOps = new List<PersistentQueueOperation>(opsCount);
				int32_t L_18 = V_3;
				List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 * L_19 = (List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 *)il2cpp_codegen_object_new(List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7_il2cpp_TypeInfo_var);
				List_1__ctor_m4701F78844832952C66B59FAE9BDD3432B391F79(L_19, L_18, /*hidden argument*/List_1__ctor_m4701F78844832952C66B59FAE9BDD3432B391F79_RuntimeMethod_var);
				V_4 = L_19;
				// for (var i = 0; i < opsCount; i++)
				V_6 = 0;
				goto IL_00a8;
			}

IL_005d:
			{
				// AssertOperationSeparator(binaryReader);
				PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * L_20 = __this->get_U3CU3E4__this_0();
				BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * L_21 = V_0;
				NullCheck(L_20);
				PersistentQueue_AssertOperationSeparator_m346D83803A2A4B1E31B79BFF49B23B9D4298131E(L_20, L_21, /*hidden argument*/NULL);
				// var operation = new PersistentQueueOperation(
				//     (PersistentQueueOperationTypes) binaryReader.ReadByte(),
				//     binaryReader.ReadInt32(),
				//     binaryReader.ReadInt32(),
				//     binaryReader.ReadInt32()
				// );
				BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * L_22 = V_0;
				NullCheck(L_22);
				uint8_t L_23 = VirtFuncInvoker0< uint8_t >::Invoke(10 /* System.Byte System.IO.BinaryReader::ReadByte() */, L_22);
				BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * L_24 = V_0;
				NullCheck(L_24);
				int32_t L_25 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_24);
				BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * L_26 = V_0;
				NullCheck(L_26);
				int32_t L_27 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_26);
				BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * L_28 = V_0;
				NullCheck(L_28);
				int32_t L_29 = VirtFuncInvoker0< int32_t >::Invoke(15 /* System.Int32 System.IO.BinaryReader::ReadInt32() */, L_28);
				PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * L_30 = (PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 *)il2cpp_codegen_object_new(PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69_il2cpp_TypeInfo_var);
				PersistentQueueOperation__ctor_mA52A128EF311BED74241CE8F393634EC0B61B9A6(L_30, L_23, L_25, L_27, L_29, /*hidden argument*/NULL);
				V_7 = L_30;
				// txOps.Add(operation);
				List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 * L_31 = V_4;
				PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * L_32 = V_7;
				NullCheck(L_31);
				List_1_Add_m9FF374C04A41E750BACA841918306E6559FADD0D(L_31, L_32, /*hidden argument*/List_1_Add_m9FF374C04A41E750BACA841918306E6559FADD0D_RuntimeMethod_var);
				// if (operation.Type != PersistentQueueOperationTypes.ENQUEUE)
				PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * L_33 = V_7;
				NullCheck(L_33);
				uint8_t L_34 = L_33->get_Type_0();
				if ((((int32_t)L_34) == ((int32_t)1)))
				{
					goto IL_00a2;
				}
			}

IL_009b:
			{
				// requireTxLogTrimming = true;
				__this->set_requireTxLogTrimming_1((bool)1);
			}

IL_00a2:
			{
				// for (var i = 0; i < opsCount; i++)
				int32_t L_35 = V_6;
				V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)1));
			}

IL_00a8:
			{
				// for (var i = 0; i < opsCount; i++)
				int32_t L_36 = V_6;
				int32_t L_37 = V_3;
				if ((((int32_t)L_36) < ((int32_t)L_37)))
				{
					goto IL_005d;
				}
			}

IL_00ad:
			{
				// AssertTransactionSeparator(binaryReader, txCount, PersistentQueueMarkers.END, () => { });
				PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * L_38 = __this->get_U3CU3E4__this_0();
				BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * L_39 = V_0;
				int32_t L_40 = V_2;
				IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591_il2cpp_TypeInfo_var);
				Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_41 = ((U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591_il2cpp_TypeInfo_var))->get_U3CU3E9__38_2_3();
				Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_42 = L_41;
				G_B11_0 = L_42;
				G_B11_1 = (-1);
				G_B11_2 = L_40;
				G_B11_3 = L_39;
				G_B11_4 = L_38;
				if (L_42)
				{
					G_B12_0 = L_42;
					G_B12_1 = (-1);
					G_B12_2 = L_40;
					G_B12_3 = L_39;
					G_B12_4 = L_38;
					goto IL_00d5;
				}
			}

IL_00be:
			{
				IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591_il2cpp_TypeInfo_var);
				U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591 * L_43 = ((U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
				Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_44 = (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 *)il2cpp_codegen_object_new(Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var);
				Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760(L_44, L_43, (intptr_t)((intptr_t)U3CU3Ec_U3CReadTransactionLogU3Eb__38_2_m9D396619BE3FC0B707F2231A4A5F85092F181DE9_RuntimeMethod_var), /*hidden argument*/NULL);
				Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_45 = L_44;
				((U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t53372D9D8D88A348C32820AD23CB9DEE44CA6591_il2cpp_TypeInfo_var))->set_U3CU3E9__38_2_3(L_45);
				G_B12_0 = L_45;
				G_B12_1 = G_B11_1;
				G_B12_2 = G_B11_2;
				G_B12_3 = G_B11_3;
				G_B12_4 = G_B11_4;
			}

IL_00d5:
			{
				NullCheck(G_B12_4);
				PersistentQueue_AssertTransactionSeparator_m78708D03928BD590ED08A0EBF751B051403A5473(G_B12_4, G_B12_3, G_B12_2, G_B12_1, G_B12_0, /*hidden argument*/NULL);
				// readingTransaction = false;
				U3CU3Ec__DisplayClass38_1_t51468321D6041881A73633BB17566E2FDE8E4269 * L_46 = V_1;
				NullCheck(L_46);
				L_46->set_readingTransaction_0((bool)0);
				// ApplyTransactionOperations(txOps);
				PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * L_47 = __this->get_U3CU3E4__this_0();
				List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 * L_48 = V_4;
				NullCheck(L_47);
				PersistentQueue_ApplyTransactionOperations_m0A6E07FEBC317628A77FF113DECEDD300F899BA0(L_47, L_48, /*hidden argument*/NULL);
				// while (true)
				goto IL_0016;
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (EndOfStreamException_t1B47BA867EC337F83056C2833A59293754AAC01F_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
				goto CATCH_00f3;
			throw e;
		}

CATCH_00f3:
		{ // begin catch(System.IO.EndOfStreamException)
			{
				// catch (EndOfStreamException)
				// if (readingTransaction) requireTxLogTrimming = true;
				U3CU3Ec__DisplayClass38_1_t51468321D6041881A73633BB17566E2FDE8E4269 * L_49 = V_1;
				NullCheck(L_49);
				bool L_50 = L_49->get_readingTransaction_0();
				if (!L_50)
				{
					goto IL_0103;
				}
			}

IL_00fc:
			{
				// if (readingTransaction) requireTxLogTrimming = true;
				__this->set_requireTxLogTrimming_1((bool)1);
			}

IL_0103:
			{
				// }
				IL2CPP_LEAVE(0x10F, FINALLY_0105);
			}
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0105;
	}

FINALLY_0105:
	{ // begin finally (depth: 1)
		{
			BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * L_51 = V_0;
			if (!L_51)
			{
				goto IL_010e;
			}
		}

IL_0108:
		{
			BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * L_52 = V_0;
			NullCheck(L_52);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_52);
		}

IL_010e:
		{
			IL2CPP_END_FINALLY(261)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(261)
	{
		IL2CPP_JUMP_TBL(0x10F, IL_010f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_010f:
	{
		// });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void mixpanel.queue.PersistentQueue_<>c__DisplayClass38_1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass38_1__ctor_m935637813A6919065A7B59751E3FD7DA972E1081 (U3CU3Ec__DisplayClass38_1_t51468321D6041881A73633BB17566E2FDE8E4269 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void mixpanel.queue.PersistentQueue_<>c__DisplayClass38_1::<ReadTransactionLog>b__1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass38_1_U3CReadTransactionLogU3Eb__1_m809233E2E75548C019762CF93199AF104078E077 (U3CU3Ec__DisplayClass38_1_t51468321D6041881A73633BB17566E2FDE8E4269 * __this, const RuntimeMethod* method)
{
	{
		// () => readingTransaction = true);
		__this->set_readingTransaction_0((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void mixpanel.queue.PersistentQueue_<>c__DisplayClass39_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass39_0__ctor_mF4CE80F1C5B960611E044C4FCD56C200C2789A99 (U3CU3Ec__DisplayClass39_0_t24AB86C76478BA26F0821541C9174F6766DE1508 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void mixpanel.queue.PersistentQueue_<>c__DisplayClass39_0::<FlushTrimmedTransactionLog>b__0(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass39_0_U3CFlushTrimmedTransactionLogU3Eb__0_m86C12BFF7DA82E0E3E00BD8DE7746BD4E96E9E52 (U3CU3Ec__DisplayClass39_0_t24AB86C76478BA26F0821541C9174F6766DE1508 * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream0, const RuntimeMethod* method)
{
	{
		// stream.SetLength(transactionBuffer.Length);
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_0 = ___stream0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = __this->get_transactionBuffer_0();
		NullCheck(L_1);
		NullCheck(L_0);
		VirtActionInvoker1< int64_t >::Invoke(26 /* System.Void System.IO.Stream::SetLength(System.Int64) */, L_0, (((int64_t)((int64_t)(((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length))))))));
		// stream.Write(transactionBuffer, 0, transactionBuffer.Length);
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_2 = ___stream0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = __this->get_transactionBuffer_0();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = __this->get_transactionBuffer_0();
		NullCheck(L_4);
		NullCheck(L_2);
		VirtActionInvoker3< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t >::Invoke(29 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_2, L_3, 0, (((int32_t)((int32_t)(((RuntimeArray*)L_4)->max_length)))));
		// });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void mixpanel.queue.PersistentQueueEntry::.ctor(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueEntry__ctor_mEB206A35F980CD0A8FC898AF39D9CAD87D75EFE8 (PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * __this, int32_t ___fileNumber0, int32_t ___start1, int32_t ___length2, const RuntimeMethod* method)
{
	{
		// public PersistentQueueEntry(int fileNumber, int start, int length)
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// FileNumber = fileNumber;
		int32_t L_0 = ___fileNumber0;
		__this->set_FileNumber_1(L_0);
		// Start = start;
		int32_t L_1 = ___start1;
		__this->set_Start_2(L_1);
		// Length = length;
		int32_t L_2 = ___length2;
		__this->set_Length_3(L_2);
		// }
		return;
	}
}
// System.Void mixpanel.queue.PersistentQueueEntry::.ctor(mixpanel.queue.PersistentQueueOperation)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueEntry__ctor_mCBAC51D0FBC1A9BC6D1213B2BF8121A7764D7744 (PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * __this, PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * ___operation0, const RuntimeMethod* method)
{
	{
		// public PersistentQueueEntry(PersistentQueueOperation operation) : this(operation.FileNumber, operation.Start, operation.Length) {}
		PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * L_0 = ___operation0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_FileNumber_1();
		PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * L_2 = ___operation0;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_Start_2();
		PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * L_4 = ___operation0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_Length_3();
		PersistentQueueEntry__ctor_mEB206A35F980CD0A8FC898AF39D9CAD87D75EFE8(__this, L_1, L_3, L_5, /*hidden argument*/NULL);
		// public PersistentQueueEntry(PersistentQueueOperation operation) : this(operation.FileNumber, operation.Start, operation.Length) {}
		return;
	}
}
// System.Boolean mixpanel.queue.PersistentQueueEntry::Equals(mixpanel.queue.PersistentQueueEntry)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PersistentQueueEntry_Equals_m458126F0D5A8BBFECD646B5E2CED80113BFAAF41 (PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * __this, PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * ___obj0, const RuntimeMethod* method)
{
	{
		// if (ReferenceEquals(null, obj)) return false;
		PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		// if (ReferenceEquals(null, obj)) return false;
		return (bool)0;
	}

IL_0005:
	{
		// if (ReferenceEquals(this, obj)) return true;
		PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * L_1 = ___obj0;
		if ((!(((RuntimeObject*)(PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA *)__this) == ((RuntimeObject*)(PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA *)L_1))))
		{
			goto IL_000b;
		}
	}
	{
		// if (ReferenceEquals(this, obj)) return true;
		return (bool)1;
	}

IL_000b:
	{
		// return obj.FileNumber == FileNumber && obj.Start == Start && obj.Length == Length;
		PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * L_2 = ___obj0;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_FileNumber_1();
		int32_t L_4 = __this->get_FileNumber_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
		{
			goto IL_0036;
		}
	}
	{
		PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * L_5 = ___obj0;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_Start_2();
		int32_t L_7 = __this->get_Start_2();
		if ((!(((uint32_t)L_6) == ((uint32_t)L_7))))
		{
			goto IL_0036;
		}
	}
	{
		PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * L_8 = ___obj0;
		NullCheck(L_8);
		int32_t L_9 = L_8->get_Length_3();
		int32_t L_10 = __this->get_Length_3();
		return (bool)((((int32_t)L_9) == ((int32_t)L_10))? 1 : 0);
	}

IL_0036:
	{
		return (bool)0;
	}
}
// System.Boolean mixpanel.queue.PersistentQueueEntry::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PersistentQueueEntry_Equals_m2C2C8BEC897BFCFFED93C12A43FFB4813276A185 (PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueEntry_Equals_m2C2C8BEC897BFCFFED93C12A43FFB4813276A185_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (ReferenceEquals(null, obj)) return false;
		RuntimeObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		// if (ReferenceEquals(null, obj)) return false;
		return (bool)0;
	}

IL_0005:
	{
		// if (ReferenceEquals(this, obj)) return true;
		RuntimeObject * L_1 = ___obj0;
		if ((!(((RuntimeObject*)(PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA *)__this) == ((RuntimeObject*)(RuntimeObject *)L_1))))
		{
			goto IL_000b;
		}
	}
	{
		// if (ReferenceEquals(this, obj)) return true;
		return (bool)1;
	}

IL_000b:
	{
		// return Equals(obj as PersistentQueueEntry);
		RuntimeObject * L_2 = ___obj0;
		bool L_3 = PersistentQueueEntry_Equals_m458126F0D5A8BBFECD646B5E2CED80113BFAAF41(__this, ((PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA *)IsInstClass((RuntimeObject*)L_2, PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 mixpanel.queue.PersistentQueueEntry::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PersistentQueueEntry_GetHashCode_mD369196BDDF534A2A1260723EC9C9948C2D96C08 (PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * __this, const RuntimeMethod* method)
{
	{
		// int result = FileNumber;
		int32_t L_0 = __this->get_FileNumber_1();
		// result = (result * 397) ^ Start;
		int32_t L_1 = __this->get_Start_2();
		// result = (result * 397) ^ Length;
		int32_t L_2 = __this->get_Length_3();
		// return result;
		return ((int32_t)((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_0, (int32_t)((int32_t)397)))^(int32_t)L_1)), (int32_t)((int32_t)397)))^(int32_t)L_2));
	}
}
// System.Boolean mixpanel.queue.PersistentQueueEntry::op_Equality(mixpanel.queue.PersistentQueueEntry,mixpanel.queue.PersistentQueueEntry)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PersistentQueueEntry_op_Equality_mEA0F62EB5F3B72A8C241D0B6A3D5A2B223D0DAB4 (PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * ___left0, PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * ___right1, const RuntimeMethod* method)
{
	{
		// public static bool operator ==(PersistentQueueEntry left, PersistentQueueEntry right) => Equals(left, right);
		PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * L_0 = ___left0;
		PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * L_1 = ___right1;
		bool L_2 = Object_Equals_mD98CD6D19C28ADC48B8468F78F94D38E203D0521(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean mixpanel.queue.PersistentQueueEntry::op_Inequality(mixpanel.queue.PersistentQueueEntry,mixpanel.queue.PersistentQueueEntry)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PersistentQueueEntry_op_Inequality_m5F47B256D218A73B1B90E3494503453D67300D02 (PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * ___left0, PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * ___right1, const RuntimeMethod* method)
{
	{
		// public static bool operator !=(PersistentQueueEntry left, PersistentQueueEntry right) => !Equals(left, right);
		PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * L_0 = ___left0;
		PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * L_1 = ___right1;
		bool L_2 = Object_Equals_mD98CD6D19C28ADC48B8468F78F94D38E203D0521(L_0, L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void mixpanel.queue.PersistentQueueOperation::.ctor(mixpanel.queue.PersistentQueueOperationTypes,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueOperation__ctor_mA52A128EF311BED74241CE8F393634EC0B61B9A6 (PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * __this, uint8_t ___type0, int32_t ___fileNumber1, int32_t ___start2, int32_t ___length3, const RuntimeMethod* method)
{
	{
		// public PersistentQueueOperation(PersistentQueueOperationTypes type, int fileNumber, int start, int length)
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// Type = type;
		uint8_t L_0 = ___type0;
		__this->set_Type_0(L_0);
		// FileNumber = fileNumber;
		int32_t L_1 = ___fileNumber1;
		__this->set_FileNumber_1(L_1);
		// Start = start;
		int32_t L_2 = ___start2;
		__this->set_Start_2(L_2);
		// Length = length;
		int32_t L_3 = ___length3;
		__this->set_Length_3(L_3);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void mixpanel.queue.PersistentQueueSession::.ctor(mixpanel.queue.PersistentQueue,System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueSession__ctor_m65837A3111BE6219458716645231F6292DCD4955 (PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * __this, PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * ___queue0, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___currentStream1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueSession__ctor_m65837A3111BE6219458716645231F6292DCD4955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// private readonly List<PersistentQueueOperation> _operations = new List<PersistentQueueOperation>();
		List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 * L_0 = (List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 *)il2cpp_codegen_object_new(List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7_il2cpp_TypeInfo_var);
		List_1__ctor_m0DE87A5A6E3E1768A8A8B4BA660934BE2600D063(L_0, /*hidden argument*/List_1__ctor_m0DE87A5A6E3E1768A8A8B4BA660934BE2600D063_RuntimeMethod_var);
		__this->set__operations_1(L_0);
		// private readonly IList<Exception> _pendingWritesFailures = new List<Exception>();
		List_1_t864E43C85FFFD16A2A0685F30AC877138E106F2A * L_1 = (List_1_t864E43C85FFFD16A2A0685F30AC877138E106F2A *)il2cpp_codegen_object_new(List_1_t864E43C85FFFD16A2A0685F30AC877138E106F2A_il2cpp_TypeInfo_var);
		List_1__ctor_m3CA9F30DC986E649F8E82AD69F5085D355D91AC1(L_1, /*hidden argument*/List_1__ctor_m3CA9F30DC986E649F8E82AD69F5085D355D91AC1_RuntimeMethod_var);
		__this->set__pendingWritesFailures_2(L_1);
		// private readonly IList<WaitHandle> _pendingWritesHandles = new List<WaitHandle>();
		List_1_tE87EDEDF7A49228B90A7C51CFFD8114D62D5EF30 * L_2 = (List_1_tE87EDEDF7A49228B90A7C51CFFD8114D62D5EF30 *)il2cpp_codegen_object_new(List_1_tE87EDEDF7A49228B90A7C51CFFD8114D62D5EF30_il2cpp_TypeInfo_var);
		List_1__ctor_m450486C7FD0DA7D1068FD8977B5A99FC51F62307(L_2, /*hidden argument*/List_1__ctor_m450486C7FD0DA7D1068FD8977B5A99FC51F62307_RuntimeMethod_var);
		__this->set__pendingWritesHandles_3(L_2);
		// private readonly List<Stream> _streamsToDisposeOnFlush = new List<Stream>();
		List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF * L_3 = (List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF *)il2cpp_codegen_object_new(List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF_il2cpp_TypeInfo_var);
		List_1__ctor_m33EBEFF81F45499476516DAAA3BE06259CDDF6B5(L_3, /*hidden argument*/List_1__ctor_m33EBEFF81F45499476516DAAA3BE06259CDDF6B5_RuntimeMethod_var);
		__this->set__streamsToDisposeOnFlush_5(L_3);
		// private readonly List<byte[]> _buffer = new List<byte[]>();
		List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 * L_4 = (List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 *)il2cpp_codegen_object_new(List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531_il2cpp_TypeInfo_var);
		List_1__ctor_m52FC81AB50BD21B6BFA16546E3AF9C94611D9811(L_4, /*hidden argument*/List_1__ctor_m52FC81AB50BD21B6BFA16546E3AF9C94611D9811_RuntimeMethod_var);
		__this->set__buffer_6(L_4);
		// public PersistentQueueSession(PersistentQueue queue, Stream currentStream)
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// lock (CtorLock)
		IL2CPP_RUNTIME_CLASS_INIT(PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359_il2cpp_TypeInfo_var);
		RuntimeObject * L_5 = ((PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359_StaticFields*)il2cpp_codegen_static_fields_for(PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359_il2cpp_TypeInfo_var))->get_CtorLock_0();
		V_0 = L_5;
		V_1 = (bool)0;
	}

IL_0045:
	try
	{ // begin try (depth: 1)
		RuntimeObject * L_6 = V_0;
		Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5(L_6, (bool*)(&V_1), /*hidden argument*/NULL);
		// _queue = queue;
		PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * L_7 = ___queue0;
		__this->set__queue_4(L_7);
		// _currentStream = currentStream;
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_8 = ___currentStream1;
		__this->set__currentStream_8(L_8);
		// _disposed = false;
		il2cpp_codegen_memory_barrier();
		__this->set__disposed_7(0);
		// }
		IL2CPP_LEAVE(0x70, FINALLY_0066);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0066;
	}

FINALLY_0066:
	{ // begin finally (depth: 1)
		{
			bool L_9 = V_1;
			if (!L_9)
			{
				goto IL_006f;
			}
		}

IL_0069:
		{
			RuntimeObject * L_10 = V_0;
			Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_10, /*hidden argument*/NULL);
		}

IL_006f:
		{
			IL2CPP_END_FINALLY(102)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(102)
	{
		IL2CPP_JUMP_TBL(0x70, IL_0070)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0070:
	{
		// }
		return;
	}
}
// System.Void mixpanel.queue.PersistentQueueSession::Finalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueSession_Finalize_m0FA33708F84D834BD0825ECAFCDDB06FCB9FF032 (PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			// if (_disposed) return;
			bool L_0 = __this->get__disposed_7();
			il2cpp_codegen_memory_barrier();
			if (!L_0)
			{
				goto IL_000c;
			}
		}

IL_000a:
		{
			// if (_disposed) return;
			IL2CPP_LEAVE(0x1B, FINALLY_0014);
		}

IL_000c:
		{
			// Dispose();
			PersistentQueueSession_Dispose_mCCAC09F6A1BADB8E297CD0F42ED28178152D0A37(__this, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x1B, FINALLY_0014);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0014;
	}

FINALLY_0014:
	{ // begin finally (depth: 1)
		// }
		Object_Finalize_m4015B7D3A44DE125C5FE34D7276CD4697C06F380(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(20)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(20)
	{
		IL2CPP_JUMP_TBL(0x1B, IL_001b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_001b:
	{
		// }
		return;
	}
}
// System.Void mixpanel.queue.PersistentQueueSession::Enqueue(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueSession_Enqueue_m274733E3E48A42C254312808CC94DA03D4D95C50 (PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueSession_Enqueue_m274733E3E48A42C254312808CC94DA03D4D95C50_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _buffer.Add(data);
		List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 * L_0 = __this->get__buffer_6();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = ___data0;
		NullCheck(L_0);
		List_1_Add_mCC9D38BB3CBE3F2E67EDF3390D36ABFAD293468E(L_0, L_1, /*hidden argument*/List_1_Add_mCC9D38BB3CBE3F2E67EDF3390D36ABFAD293468E_RuntimeMethod_var);
		// _bufferSize += data.Length;
		int32_t L_2 = __this->get__bufferSize_9();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = ___data0;
		NullCheck(L_3);
		__this->set__bufferSize_9(((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_3)->max_length)))))));
		// if (_bufferSize > PersistentQueueUtils.MinSizeThatMakeAsyncWritePractical)
		int32_t L_4 = __this->get__bufferSize_9();
		if ((((int32_t)L_4) <= ((int32_t)((int32_t)65536))))
		{
			goto IL_002f;
		}
	}
	{
		// AsyncFlushBuffer();
		PersistentQueueSession_AsyncFlushBuffer_m8FD2C50C3F6884F965490030895187710E0601A7(__this, /*hidden argument*/NULL);
	}

IL_002f:
	{
		// }
		return;
	}
}
// System.Void mixpanel.queue.PersistentQueueSession::AsyncFlushBuffer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueSession_AsyncFlushBuffer_m8FD2C50C3F6884F965490030895187710E0601A7 (PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueSession_AsyncFlushBuffer_m8FD2C50C3F6884F965490030895187710E0601A7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _queue.AcquireWriter(_currentStream, AsyncWriteToStream, OnReplaceStream);
		PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * L_0 = __this->get__queue_4();
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_1 = __this->get__currentStream_8();
		Func_2_t4D4D87A23A55C4AC43EDC08F16D4C8A88C4748A7 * L_2 = (Func_2_t4D4D87A23A55C4AC43EDC08F16D4C8A88C4748A7 *)il2cpp_codegen_object_new(Func_2_t4D4D87A23A55C4AC43EDC08F16D4C8A88C4748A7_il2cpp_TypeInfo_var);
		Func_2__ctor_m29194A2FFE45ED9FF0FB5E4DEE6861B3979C0CB0(L_2, __this, (intptr_t)((intptr_t)PersistentQueueSession_AsyncWriteToStream_m6C9D372CBAD07BC6E4E3A503C3FD6B8733522C33_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m29194A2FFE45ED9FF0FB5E4DEE6861B3979C0CB0_RuntimeMethod_var);
		Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 * L_3 = (Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 *)il2cpp_codegen_object_new(Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621_il2cpp_TypeInfo_var);
		Action_1__ctor_m948E1585F7105BAF7467A6E48FE716E71CD4BAB9(L_3, __this, (intptr_t)((intptr_t)PersistentQueueSession_OnReplaceStream_m36896D1B59C214C86E1CD371212E227AC32E8B32_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m948E1585F7105BAF7467A6E48FE716E71CD4BAB9_RuntimeMethod_var);
		NullCheck(L_0);
		PersistentQueue_AcquireWriter_mC946B341A323EFB01E23822AF9FC82CA1DFE7BFA(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void mixpanel.queue.PersistentQueueSession::SyncFlushBuffer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueSession_SyncFlushBuffer_m2F6D9D269AC0D30E9976A4AC3A09B9A576D196D8 (PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueSession_SyncFlushBuffer_m2F6D9D269AC0D30E9976A4AC3A09B9A576D196D8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _queue.AcquireWriter(_currentStream, stream =>
		// {
		//     byte[] data = ConcatenateBufferAndAddIndividualOperations(stream);
		//     stream.Write(data, 0, data.Length);
		//     return stream.Position;
		// }, OnReplaceStream);
		PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * L_0 = __this->get__queue_4();
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_1 = __this->get__currentStream_8();
		Func_2_t4D4D87A23A55C4AC43EDC08F16D4C8A88C4748A7 * L_2 = (Func_2_t4D4D87A23A55C4AC43EDC08F16D4C8A88C4748A7 *)il2cpp_codegen_object_new(Func_2_t4D4D87A23A55C4AC43EDC08F16D4C8A88C4748A7_il2cpp_TypeInfo_var);
		Func_2__ctor_m29194A2FFE45ED9FF0FB5E4DEE6861B3979C0CB0(L_2, __this, (intptr_t)((intptr_t)PersistentQueueSession_U3CSyncFlushBufferU3Eb__14_0_mBCCA91C92F2723E2773D72FA0BB4778EA83D0FDC_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m29194A2FFE45ED9FF0FB5E4DEE6861B3979C0CB0_RuntimeMethod_var);
		Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 * L_3 = (Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 *)il2cpp_codegen_object_new(Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621_il2cpp_TypeInfo_var);
		Action_1__ctor_m948E1585F7105BAF7467A6E48FE716E71CD4BAB9(L_3, __this, (intptr_t)((intptr_t)PersistentQueueSession_OnReplaceStream_m36896D1B59C214C86E1CD371212E227AC32E8B32_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m948E1585F7105BAF7467A6E48FE716E71CD4BAB9_RuntimeMethod_var);
		NullCheck(L_0);
		PersistentQueue_AcquireWriter_mC946B341A323EFB01E23822AF9FC82CA1DFE7BFA(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Int64 mixpanel.queue.PersistentQueueSession::AsyncWriteToStream(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t PersistentQueueSession_AsyncWriteToStream_m6C9D372CBAD07BC6E4E3A503C3FD6B8733522C33 (PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueSession_AsyncWriteToStream_m6C9D372CBAD07BC6E4E3A503C3FD6B8733522C33_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C * V_0 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_1 = NULL;
	{
		U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C * L_0 = (U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass15_0__ctor_mDB92AB36CC844BB6EC46B22255B0234DD58CDC20(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C * L_1 = V_0;
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_2 = ___stream0;
		NullCheck(L_1);
		L_1->set_stream_0(L_2);
		U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U3CU3E4__this_1(__this);
		// byte[] data = ConcatenateBufferAndAddIndividualOperations(stream);
		U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C * L_4 = V_0;
		NullCheck(L_4);
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_5 = L_4->get_stream_0();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = PersistentQueueSession_ConcatenateBufferAndAddIndividualOperations_m1F4735CE325637612B78A6DC6D8F7C871FA21894(__this, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		// ManualResetEvent resetEvent = new ManualResetEvent(false);
		U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C * L_7 = V_0;
		ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * L_8 = (ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 *)il2cpp_codegen_object_new(ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408_il2cpp_TypeInfo_var);
		ManualResetEvent__ctor_m8973D9E3C622B9602641C017A33870F51D0311E1(L_8, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_7);
		L_7->set_resetEvent_2(L_8);
		// _pendingWritesHandles.Add(resetEvent);
		RuntimeObject* L_9 = __this->get__pendingWritesHandles_3();
		U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C * L_10 = V_0;
		NullCheck(L_10);
		ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * L_11 = L_10->get_resetEvent_2();
		NullCheck(L_9);
		InterfaceActionInvoker1< WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6 * >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Threading.WaitHandle>::Add(!0) */, ICollection_1_t141497430FA8C150A49A8B04E2C6476230A8D741_il2cpp_TypeInfo_var, L_9, L_11);
		// long positionAfterWrite = stream.Position + data.Length;
		U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C * L_12 = V_0;
		NullCheck(L_12);
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_13 = L_12->get_stream_0();
		NullCheck(L_13);
		int64_t L_14 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_13);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_15 = V_1;
		NullCheck(L_15);
		// stream.BeginWrite(data, 0, data.Length, delegate(IAsyncResult ar)
		// {
		//     try
		//     {
		//         stream.EndWrite(ar);
		//     }
		//     catch (Exception e)
		//     {
		//         lock (_pendingWritesFailures)
		//         {
		//             _pendingWritesFailures.Add(e);
		//         }
		//     }
		//     finally
		//     {
		//         resetEvent.Set();
		//     }
		// }, null);
		U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C * L_16 = V_0;
		NullCheck(L_16);
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_17 = L_16->get_stream_0();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_18 = V_1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_19 = V_1;
		NullCheck(L_19);
		U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C * L_20 = V_0;
		AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * L_21 = (AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 *)il2cpp_codegen_object_new(AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4_il2cpp_TypeInfo_var);
		AsyncCallback__ctor_m36AEE927E6AFEE950656BC5F7841682D23DE2EBB(L_21, L_20, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass15_0_U3CAsyncWriteToStreamU3Eb__0_m23B9A65D254C30C79EDC2DA9077ABFFE054C1915_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_17);
		VirtFuncInvoker5< RuntimeObject*, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 *, RuntimeObject * >::Invoke(22 /* System.IAsyncResult System.IO.Stream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object) */, L_17, L_18, 0, (((int32_t)((int32_t)(((RuntimeArray*)L_19)->max_length)))), L_21, NULL);
		// return positionAfterWrite;
		return ((int64_t)il2cpp_codegen_add((int64_t)L_14, (int64_t)(((int64_t)((int64_t)(((int32_t)((int32_t)(((RuntimeArray*)L_15)->max_length)))))))));
	}
}
// System.Byte[] mixpanel.queue.PersistentQueueSession::ConcatenateBufferAndAddIndividualOperations(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* PersistentQueueSession_ConcatenateBufferAndAddIndividualOperations_m1F4735CE325637612B78A6DC6D8F7C871FA21894 (PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueSession_ConcatenateBufferAndAddIndividualOperations_m1F4735CE325637612B78A6DC6D8F7C871FA21894_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Enumerator_t4A0753267EA56CCB5A11BD47A5A39F7DC4154DB1  V_3;
	memset((&V_3), 0, sizeof(V_3));
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_4 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// byte[] data = new byte[_bufferSize];
		int32_t L_0 = __this->get__bufferSize_9();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)L_0);
		V_0 = L_1;
		// int start = (int)stream.Position;
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_2 = ___stream0;
		NullCheck(L_2);
		int64_t L_3 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_2);
		V_1 = (((int32_t)((int32_t)L_3)));
		// int index = 0;
		V_2 = 0;
		// foreach (byte[] bytes in _buffer)
		List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 * L_4 = __this->get__buffer_6();
		NullCheck(L_4);
		Enumerator_t4A0753267EA56CCB5A11BD47A5A39F7DC4154DB1  L_5 = List_1_GetEnumerator_m1E687BD559DDDC595828B26A476F527A1FA5F7A4(L_4, /*hidden argument*/List_1_GetEnumerator_m1E687BD559DDDC595828B26A476F527A1FA5F7A4_RuntimeMethod_var);
		V_3 = L_5;
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006a;
		}

IL_0024:
		{
			// foreach (byte[] bytes in _buffer)
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = Enumerator_get_Current_m2DA2B2533227571D1B3AB4AFBA0E69F5484E1F73_inline((Enumerator_t4A0753267EA56CCB5A11BD47A5A39F7DC4154DB1 *)(&V_3), /*hidden argument*/Enumerator_get_Current_m2DA2B2533227571D1B3AB4AFBA0E69F5484E1F73_RuntimeMethod_var);
			V_4 = L_6;
			// _operations.Add(new PersistentQueueOperation(
			//     PersistentQueueOperationTypes.ENQUEUE,
			//     _queue.CurrentFileNumber,
			//     start,
			//     bytes.Length
			// ));
			List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 * L_7 = __this->get__operations_1();
			PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * L_8 = __this->get__queue_4();
			NullCheck(L_8);
			int32_t L_9 = PersistentQueue_get_CurrentFileNumber_mEA54919870FD34A7E69FCD761993FB1C592B3986_inline(L_8, /*hidden argument*/NULL);
			int32_t L_10 = V_1;
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_11 = V_4;
			NullCheck(L_11);
			PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * L_12 = (PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 *)il2cpp_codegen_object_new(PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69_il2cpp_TypeInfo_var);
			PersistentQueueOperation__ctor_mA52A128EF311BED74241CE8F393634EC0B61B9A6(L_12, 1, L_9, L_10, (((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length)))), /*hidden argument*/NULL);
			NullCheck(L_7);
			List_1_Add_m9FF374C04A41E750BACA841918306E6559FADD0D(L_7, L_12, /*hidden argument*/List_1_Add_m9FF374C04A41E750BACA841918306E6559FADD0D_RuntimeMethod_var);
			// Buffer.BlockCopy(bytes, 0, data, index, bytes.Length);
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_13 = V_4;
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_14 = V_0;
			int32_t L_15 = V_2;
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_16 = V_4;
			NullCheck(L_16);
			Buffer_BlockCopy_m1F882D595976063718AF6E405664FC761924D353((RuntimeArray *)(RuntimeArray *)L_13, 0, (RuntimeArray *)(RuntimeArray *)L_14, L_15, (((int32_t)((int32_t)(((RuntimeArray*)L_16)->max_length)))), /*hidden argument*/NULL);
			// start += bytes.Length;
			int32_t L_17 = V_1;
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_18 = V_4;
			NullCheck(L_18);
			V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_18)->max_length))))));
			// index += bytes.Length;
			int32_t L_19 = V_2;
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_20 = V_4;
			NullCheck(L_20);
			V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_20)->max_length))))));
		}

IL_006a:
		{
			// foreach (byte[] bytes in _buffer)
			bool L_21 = Enumerator_MoveNext_m386C5CC04A468880E8A50EBF6086B47EB2461FCB((Enumerator_t4A0753267EA56CCB5A11BD47A5A39F7DC4154DB1 *)(&V_3), /*hidden argument*/Enumerator_MoveNext_m386C5CC04A468880E8A50EBF6086B47EB2461FCB_RuntimeMethod_var);
			if (L_21)
			{
				goto IL_0024;
			}
		}

IL_0073:
		{
			IL2CPP_LEAVE(0x83, FINALLY_0075);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0075;
	}

FINALLY_0075:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m4A7857ABA06AF87BCEB0EB05F49C6DEF148DC27F((Enumerator_t4A0753267EA56CCB5A11BD47A5A39F7DC4154DB1 *)(&V_3), /*hidden argument*/Enumerator_Dispose_m4A7857ABA06AF87BCEB0EB05F49C6DEF148DC27F_RuntimeMethod_var);
		IL2CPP_END_FINALLY(117)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(117)
	{
		IL2CPP_JUMP_TBL(0x83, IL_0083)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0083:
	{
		// _bufferSize = 0;
		__this->set__bufferSize_9(0);
		// _buffer.Clear();
		List_1_t4AB280456F4DE770AC993DE9A7C8C563A6311531 * L_22 = __this->get__buffer_6();
		NullCheck(L_22);
		List_1_Clear_m458DDFB5AB146FD65D91BAF36907B40AAD1585B3(L_22, /*hidden argument*/List_1_Clear_m458DDFB5AB146FD65D91BAF36907B40AAD1585B3_RuntimeMethod_var);
		// return data;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_23 = V_0;
		return L_23;
	}
}
// System.Void mixpanel.queue.PersistentQueueSession::OnReplaceStream(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueSession_OnReplaceStream_m36896D1B59C214C86E1CD371212E227AC32E8B32 (PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___newStream0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueSession_OnReplaceStream_m36896D1B59C214C86E1CD371212E227AC32E8B32_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _streamsToDisposeOnFlush.Add(_currentStream);
		List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF * L_0 = __this->get__streamsToDisposeOnFlush_5();
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_1 = __this->get__currentStream_8();
		NullCheck(L_0);
		List_1_Add_mB306C980BF53D3411577EE0AF430FF5F9B1D2605(L_0, L_1, /*hidden argument*/List_1_Add_mB306C980BF53D3411577EE0AF430FF5F9B1D2605_RuntimeMethod_var);
		// _currentStream = newStream;
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_2 = ___newStream0;
		__this->set__currentStream_8(L_2);
		// }
		return;
	}
}
// System.Byte[] mixpanel.queue.PersistentQueueSession::Dequeue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* PersistentQueueSession_Dequeue_m4B5E669B9208A5E3AC4550CD6CC66EF6962A1370 (PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueSession_Dequeue_m4B5E669B9208A5E3AC4550CD6CC66EF6962A1370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * V_0 = NULL;
	{
		// PersistentQueueEntry entry = _queue.Dequeue();
		PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * L_0 = __this->get__queue_4();
		NullCheck(L_0);
		PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * L_1 = PersistentQueue_Dequeue_m71899BBB07161B055C5337E38FB9084AF015413A(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// if (entry == null)
		PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * L_2 = V_0;
		bool L_3 = PersistentQueueEntry_op_Equality_mEA0F62EB5F3B72A8C241D0B6A3D5A2B223D0DAB4(L_2, (PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0017;
		}
	}
	{
		// return null;
		return (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)NULL;
	}

IL_0017:
	{
		// _operations.Add(new PersistentQueueOperation(
		//     PersistentQueueOperationTypes.DEQUEUE,
		//     entry.FileNumber,
		//     entry.Start,
		//     entry.Length
		// ));
		List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 * L_4 = __this->get__operations_1();
		PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_FileNumber_1();
		PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_Start_2();
		PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = L_9->get_Length_3();
		PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 * L_11 = (PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69 *)il2cpp_codegen_object_new(PersistentQueueOperation_t1D5FD59ED664DE4E0B394B1227B01D132A326D69_il2cpp_TypeInfo_var);
		PersistentQueueOperation__ctor_mA52A128EF311BED74241CE8F393634EC0B61B9A6(L_11, 2, L_6, L_8, L_10, /*hidden argument*/NULL);
		NullCheck(L_4);
		List_1_Add_m9FF374C04A41E750BACA841918306E6559FADD0D(L_4, L_11, /*hidden argument*/List_1_Add_m9FF374C04A41E750BACA841918306E6559FADD0D_RuntimeMethod_var);
		// return entry.Data;
		PersistentQueueEntry_t862FEF066434F5F29E6EBA79F83B7401D70559BA * L_12 = V_0;
		NullCheck(L_12);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_13 = L_12->get_Data_0();
		return L_13;
	}
}
// System.Void mixpanel.queue.PersistentQueueSession::Flush()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueSession_Flush_m07F15FE5C4F66C24A7AF4FCB760185E566245CB2 (PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueSession_Flush_m07F15FE5C4F66C24A7AF4FCB760185E566245CB2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		// WaitForPendingWrites();
		PersistentQueueSession_WaitForPendingWrites_m105E3D4E7EA55B5E947BF87E12A6873E9299BFBE(__this, /*hidden argument*/NULL);
		// SyncFlushBuffer();
		PersistentQueueSession_SyncFlushBuffer_m2F6D9D269AC0D30E9976A4AC3A09B9A576D196D8(__this, /*hidden argument*/NULL);
		// }
		IL2CPP_LEAVE(0x53, FINALLY_000e);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000e;
	}

FINALLY_000e:
	{ // begin finally (depth: 1)
		{
			// foreach (Stream stream in _streamsToDisposeOnFlush)
			List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF * L_0 = __this->get__streamsToDisposeOnFlush_5();
			NullCheck(L_0);
			Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55  L_1 = List_1_GetEnumerator_mB6287B4A0B81DE7915896C1771FD16C182091404(L_0, /*hidden argument*/List_1_GetEnumerator_mB6287B4A0B81DE7915896C1771FD16C182091404_RuntimeMethod_var);
			V_0 = L_1;
		}

IL_001a:
		try
		{ // begin try (depth: 2)
			{
				goto IL_002e;
			}

IL_001c:
			{
				// foreach (Stream stream in _streamsToDisposeOnFlush)
				Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_2 = Enumerator_get_Current_mDD08B6F8CE246828D34AF51A4B4A5CB333C9A4AE_inline((Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55 *)(&V_0), /*hidden argument*/Enumerator_get_Current_mDD08B6F8CE246828D34AF51A4B4A5CB333C9A4AE_RuntimeMethod_var);
				// stream.Flush();
				Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_3 = L_2;
				NullCheck(L_3);
				VirtActionInvoker0::Invoke(18 /* System.Void System.IO.Stream::Flush() */, L_3);
				// stream.Dispose();
				NullCheck(L_3);
				Stream_Dispose_m186A8E680F2528DEDFF8F0069CC33BD813FFB1C7(L_3, /*hidden argument*/NULL);
			}

IL_002e:
			{
				// foreach (Stream stream in _streamsToDisposeOnFlush)
				bool L_4 = Enumerator_MoveNext_m78855BD085E859C5228061D642FD29C4161D7284((Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_m78855BD085E859C5228061D642FD29C4161D7284_RuntimeMethod_var);
				if (L_4)
				{
					goto IL_001c;
				}
			}

IL_0037:
			{
				IL2CPP_LEAVE(0x47, FINALLY_0039);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0039;
		}

FINALLY_0039:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m075340F31C13EF2A612E40C80988C9E651B7F829((Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55 *)(&V_0), /*hidden argument*/Enumerator_Dispose_m075340F31C13EF2A612E40C80988C9E651B7F829_RuntimeMethod_var);
			IL2CPP_END_FINALLY(57)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(57)
		{
			IL2CPP_JUMP_TBL(0x47, IL_0047)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_0047:
		{
			// _streamsToDisposeOnFlush.Clear();
			List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF * L_5 = __this->get__streamsToDisposeOnFlush_5();
			NullCheck(L_5);
			List_1_Clear_m1465A2F72D3985860367250F2F3CA0FF83A09D63(L_5, /*hidden argument*/List_1_Clear_m1465A2F72D3985860367250F2F3CA0FF83A09D63_RuntimeMethod_var);
			// }
			IL2CPP_END_FINALLY(14)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(14)
	{
		IL2CPP_JUMP_TBL(0x53, IL_0053)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0053:
	{
		// _currentStream.Flush();
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_6 = __this->get__currentStream_8();
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(18 /* System.Void System.IO.Stream::Flush() */, L_6);
		// _queue.CommitTransaction(_operations);
		PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * L_7 = __this->get__queue_4();
		List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 * L_8 = __this->get__operations_1();
		NullCheck(L_7);
		PersistentQueue_CommitTransaction_m6F9DB4D286D328A89EFC0687F9EC6BEFF4105F4E(L_7, L_8, /*hidden argument*/NULL);
		// _operations.Clear();
		List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 * L_9 = __this->get__operations_1();
		NullCheck(L_9);
		List_1_Clear_mBBF7F2EBBE5FD254B09CE99D16F6BB0C340CEA50(L_9, /*hidden argument*/List_1_Clear_mBBF7F2EBBE5FD254B09CE99D16F6BB0C340CEA50_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void mixpanel.queue.PersistentQueueSession::WaitForPendingWrites()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueSession_WaitForPendingWrites_m105E3D4E7EA55B5E947BF87E12A6873E9299BFBE (PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueSession_WaitForPendingWrites_m105E3D4E7EA55B5E947BF87E12A6873E9299BFBE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* V_0 = NULL;
	WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* V_1 = NULL;
	int32_t V_2 = 0;
	WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6 * V_3 = NULL;
	{
		goto IL_005b;
	}

IL_0002:
	{
		// WaitHandle[] handles = _pendingWritesHandles.Take(64).ToArray();
		RuntimeObject* L_0 = __this->get__pendingWritesHandles_3();
		RuntimeObject* L_1 = Enumerable_Take_TisWaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_mD6127D3008326E62523DDEBDCA22C2623A48076C(L_0, ((int32_t)64), /*hidden argument*/Enumerable_Take_TisWaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_mD6127D3008326E62523DDEBDCA22C2623A48076C_RuntimeMethod_var);
		WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* L_2 = Enumerable_ToArray_TisWaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_mB82DA928C5644B54EA8198BD2135F4F6D579928F(L_1, /*hidden argument*/Enumerable_ToArray_TisWaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_mB82DA928C5644B54EA8198BD2135F4F6D579928F_RuntimeMethod_var);
		V_0 = L_2;
		// foreach (WaitHandle handle in handles)
		WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* L_3 = V_0;
		V_1 = L_3;
		V_2 = 0;
		goto IL_0030;
	}

IL_001b:
	{
		// foreach (WaitHandle handle in handles)
		WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* L_4 = V_1;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_3 = L_7;
		// _pendingWritesHandles.Remove(handle);
		RuntimeObject* L_8 = __this->get__pendingWritesHandles_3();
		WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6 * L_9 = V_3;
		NullCheck(L_8);
		InterfaceFuncInvoker1< bool, WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6 * >::Invoke(6 /* System.Boolean System.Collections.Generic.ICollection`1<System.Threading.WaitHandle>::Remove(!0) */, ICollection_1_t141497430FA8C150A49A8B04E2C6476230A8D741_il2cpp_TypeInfo_var, L_8, L_9);
		int32_t L_10 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0030:
	{
		// foreach (WaitHandle handle in handles)
		int32_t L_11 = V_2;
		WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* L_12 = V_1;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		// WaitHandle.WaitAll(handles);
		WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6_il2cpp_TypeInfo_var);
		WaitHandle_WaitAll_m83A50834628130B07B1A5BC9FB2968C9F78B1A23(L_13, /*hidden argument*/NULL);
		// foreach (WaitHandle handle in handles)
		WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* L_14 = V_0;
		V_1 = L_14;
		V_2 = 0;
		goto IL_004f;
	}

IL_0043:
	{
		// foreach (WaitHandle handle in handles)
		WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* L_15 = V_1;
		int32_t L_16 = V_2;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		WaitHandle_tFD46B5B45A6BB296EA3A104C91DF2A7C03C10AC6 * L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		// handle.Close();
		NullCheck(L_18);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Threading.WaitHandle::Close() */, L_18);
		int32_t L_19 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
	}

IL_004f:
	{
		// foreach (WaitHandle handle in handles)
		int32_t L_20 = V_2;
		WaitHandleU5BU5D_t79FF2E6AC099CC1FDD2B24F21A72D36BA31298CC* L_21 = V_1;
		NullCheck(L_21);
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_21)->max_length)))))))
		{
			goto IL_0043;
		}
	}
	{
		// AssertNoPendingWritesFailures();
		PersistentQueueSession_AssertNoPendingWritesFailures_mEBC84FCA8E6D166C6AB29880C9B76C80F1C426AF(__this, /*hidden argument*/NULL);
	}

IL_005b:
	{
		// while (_pendingWritesHandles.Count != 0)
		RuntimeObject* L_22 = __this->get__pendingWritesHandles_3();
		NullCheck(L_22);
		int32_t L_23 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Threading.WaitHandle>::get_Count() */, ICollection_1_t141497430FA8C150A49A8B04E2C6476230A8D741_il2cpp_TypeInfo_var, L_22);
		if (L_23)
		{
			goto IL_0002;
		}
	}
	{
		// }
		return;
	}
}
// System.Void mixpanel.queue.PersistentQueueSession::AssertNoPendingWritesFailures()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueSession_AssertNoPendingWritesFailures_mEBC84FCA8E6D166C6AB29880C9B76C80F1C426AF (PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueSession_AssertNoPendingWritesFailures_mEBC84FCA8E6D166C6AB29880C9B76C80F1C426AF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (_pendingWritesFailures)
		RuntimeObject* L_0 = __this->get__pendingWritesFailures_2();
		V_0 = L_0;
		V_1 = (bool)0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject* L_1 = V_0;
			Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5(L_1, (bool*)(&V_1), /*hidden argument*/NULL);
			// if (_pendingWritesFailures.Count == 0)
			RuntimeObject* L_2 = __this->get__pendingWritesFailures_2();
			NullCheck(L_2);
			int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Exception>::get_Count() */, ICollection_1_t369F219933FB866C23070CD9749538883440F044_il2cpp_TypeInfo_var, L_2);
			if (L_3)
			{
				goto IL_0020;
			}
		}

IL_001e:
		{
			// return;
			IL2CPP_LEAVE(0x46, FINALLY_003c);
		}

IL_0020:
		{
			// Exception[] array = _pendingWritesFailures.ToArray();
			RuntimeObject* L_4 = __this->get__pendingWritesFailures_2();
			ExceptionU5BU5D_t09C3EFFA7CF3F84DA802016E2017E1608442F209* L_5 = Enumerable_ToArray_TisException_t_mC45E629FDAFE5B708F9F65A7458539250D8461A7(L_4, /*hidden argument*/Enumerable_ToArray_TisException_t_mC45E629FDAFE5B708F9F65A7458539250D8461A7_RuntimeMethod_var);
			// _pendingWritesFailures.Clear();
			RuntimeObject* L_6 = __this->get__pendingWritesFailures_2();
			NullCheck(L_6);
			InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Exception>::Clear() */, ICollection_1_t369F219933FB866C23070CD9749538883440F044_il2cpp_TypeInfo_var, L_6);
			// throw new PendingWriteException(array);
			PendingWriteException_tD008EED8A9DD4BB29844EF57D33275EE9A882F48 * L_7 = (PendingWriteException_tD008EED8A9DD4BB29844EF57D33275EE9A882F48 *)il2cpp_codegen_object_new(PendingWriteException_tD008EED8A9DD4BB29844EF57D33275EE9A882F48_il2cpp_TypeInfo_var);
			PendingWriteException__ctor_m21342C30EFE1762297E1091AEDE3A3EBA2E693C2(L_7, L_5, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_7, PersistentQueueSession_AssertNoPendingWritesFailures_mEBC84FCA8E6D166C6AB29880C9B76C80F1C426AF_RuntimeMethod_var);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_003c;
	}

FINALLY_003c:
	{ // begin finally (depth: 1)
		{
			bool L_8 = V_1;
			if (!L_8)
			{
				goto IL_0045;
			}
		}

IL_003f:
		{
			RuntimeObject* L_9 = V_0;
			Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_9, /*hidden argument*/NULL);
		}

IL_0045:
		{
			IL2CPP_END_FINALLY(60)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(60)
	{
		IL2CPP_JUMP_TBL(0x46, IL_0046)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0046:
	{
		// }
		return;
	}
}
// System.Void mixpanel.queue.PersistentQueueSession::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueSession_Dispose_mCCAC09F6A1BADB8E297CD0F42ED28178152D0A37 (PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueSession_Dispose_mCCAC09F6A1BADB8E297CD0F42ED28178152D0A37_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 3);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (CtorLock)
		IL2CPP_RUNTIME_CLASS_INIT(PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359_il2cpp_TypeInfo_var);
		RuntimeObject * L_0 = ((PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359_StaticFields*)il2cpp_codegen_static_fields_for(PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359_il2cpp_TypeInfo_var))->get_CtorLock_0();
		V_0 = L_0;
		V_1 = (bool)0;
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_1 = V_0;
			Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5(L_1, (bool*)(&V_1), /*hidden argument*/NULL);
			// if (_disposed) return;
			bool L_2 = __this->get__disposed_7();
			il2cpp_codegen_memory_barrier();
			if (!L_2)
			{
				goto IL_001c;
			}
		}

IL_001a:
		{
			// if (_disposed) return;
			IL2CPP_LEAVE(0x97, FINALLY_0087);
		}

IL_001c:
		{
			// _disposed = true;
			il2cpp_codegen_memory_barrier();
			__this->set__disposed_7(1);
			// _queue.Reinstate(_operations);
			PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * L_3 = __this->get__queue_4();
			List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 * L_4 = __this->get__operations_1();
			NullCheck(L_3);
			PersistentQueue_Reinstate_mD630C130E071829D27A91175CC9A36C74CC5DEC7(L_3, L_4, /*hidden argument*/NULL);
			// _operations.Clear();
			List_1_tFBC48BE827B321979C12096915DBBC3166FC03F7 * L_5 = __this->get__operations_1();
			NullCheck(L_5);
			List_1_Clear_mBBF7F2EBBE5FD254B09CE99D16F6BB0C340CEA50(L_5, /*hidden argument*/List_1_Clear_mBBF7F2EBBE5FD254B09CE99D16F6BB0C340CEA50_RuntimeMethod_var);
			// foreach (Stream stream in _streamsToDisposeOnFlush)
			List_1_t41903256145E04B3CDF944F1C86211DAB3FB28EF * L_6 = __this->get__streamsToDisposeOnFlush_5();
			NullCheck(L_6);
			Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55  L_7 = List_1_GetEnumerator_mB6287B4A0B81DE7915896C1771FD16C182091404(L_6, /*hidden argument*/List_1_GetEnumerator_mB6287B4A0B81DE7915896C1771FD16C182091404_RuntimeMethod_var);
			V_2 = L_7;
		}

IL_004d:
		try
		{ // begin try (depth: 2)
			{
				goto IL_005b;
			}

IL_004f:
			{
				// foreach (Stream stream in _streamsToDisposeOnFlush)
				Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_8 = Enumerator_get_Current_mDD08B6F8CE246828D34AF51A4B4A5CB333C9A4AE_inline((Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55 *)(&V_2), /*hidden argument*/Enumerator_get_Current_mDD08B6F8CE246828D34AF51A4B4A5CB333C9A4AE_RuntimeMethod_var);
				// stream.Dispose();
				NullCheck(L_8);
				Stream_Dispose_m186A8E680F2528DEDFF8F0069CC33BD813FFB1C7(L_8, /*hidden argument*/NULL);
			}

IL_005b:
			{
				// foreach (Stream stream in _streamsToDisposeOnFlush)
				bool L_9 = Enumerator_MoveNext_m78855BD085E859C5228061D642FD29C4161D7284((Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55 *)(&V_2), /*hidden argument*/Enumerator_MoveNext_m78855BD085E859C5228061D642FD29C4161D7284_RuntimeMethod_var);
				if (L_9)
				{
					goto IL_004f;
				}
			}

IL_0064:
			{
				IL2CPP_LEAVE(0x74, FINALLY_0066);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0066;
		}

FINALLY_0066:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m075340F31C13EF2A612E40C80988C9E651B7F829((Enumerator_t46BDE8BF68CDE9B2FF3AE4F247AC85D5DCB33D55 *)(&V_2), /*hidden argument*/Enumerator_Dispose_m075340F31C13EF2A612E40C80988C9E651B7F829_RuntimeMethod_var);
			IL2CPP_END_FINALLY(102)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(102)
		{
			IL2CPP_JUMP_TBL(0x74, IL_0074)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_0074:
		{
			// _currentStream.Dispose();
			Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_10 = __this->get__currentStream_8();
			NullCheck(L_10);
			Stream_Dispose_m186A8E680F2528DEDFF8F0069CC33BD813FFB1C7(L_10, /*hidden argument*/NULL);
			// GC.SuppressFinalize(this);
			IL2CPP_RUNTIME_CLASS_INIT(GC_tC1D7BD74E8F44ECCEF5CD2B5D84BFF9AAE02D01D_il2cpp_TypeInfo_var);
			GC_SuppressFinalize_m037319A9B95A5BA437E806DE592802225EE5B425(__this, /*hidden argument*/NULL);
			// }
			IL2CPP_LEAVE(0x91, FINALLY_0087);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0087;
	}

FINALLY_0087:
	{ // begin finally (depth: 1)
		{
			bool L_11 = V_1;
			if (!L_11)
			{
				goto IL_0090;
			}
		}

IL_008a:
		{
			RuntimeObject * L_12 = V_0;
			Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_12, /*hidden argument*/NULL);
		}

IL_0090:
		{
			IL2CPP_END_FINALLY(135)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(135)
	{
		IL2CPP_JUMP_TBL(0x97, IL_0097)
		IL2CPP_JUMP_TBL(0x91, IL_0091)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0091:
	{
		// Thread.Sleep(0);
		Thread_Sleep_m2CD320EAB7BE02CC1F395EAFE9970D53A5F9EAEF(0, /*hidden argument*/NULL);
	}

IL_0097:
	{
		// }
		return;
	}
}
// System.Void mixpanel.queue.PersistentQueueSession::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueSession__cctor_mD5EB5C1B0B79B18AC77D6EB9897D143BDEC95831 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueSession__cctor_mD5EB5C1B0B79B18AC77D6EB9897D143BDEC95831_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static readonly object CtorLock = new object();
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(L_0, /*hidden argument*/NULL);
		((PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359_StaticFields*)il2cpp_codegen_static_fields_for(PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359_il2cpp_TypeInfo_var))->set_CtorLock_0(L_0);
		return;
	}
}
// System.Int64 mixpanel.queue.PersistentQueueSession::<SyncFlushBuffer>b__14_0(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t PersistentQueueSession_U3CSyncFlushBufferU3Eb__14_0_mBCCA91C92F2723E2773D72FA0BB4778EA83D0FDC (PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * __this, Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream0, const RuntimeMethod* method)
{
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	{
		// byte[] data = ConcatenateBufferAndAddIndividualOperations(stream);
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_0 = ___stream0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = PersistentQueueSession_ConcatenateBufferAndAddIndividualOperations_m1F4735CE325637612B78A6DC6D8F7C871FA21894(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// stream.Write(data, 0, data.Length);
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_2 = ___stream0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = V_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = V_0;
		NullCheck(L_4);
		NullCheck(L_2);
		VirtActionInvoker3< ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*, int32_t, int32_t >::Invoke(29 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_2, L_3, 0, (((int32_t)((int32_t)(((RuntimeArray*)L_4)->max_length)))));
		// return stream.Position;
		Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_5 = ___stream0;
		NullCheck(L_5);
		int64_t L_6 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_5);
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void mixpanel.queue.PersistentQueueSession_<>c__DisplayClass15_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass15_0__ctor_mDB92AB36CC844BB6EC46B22255B0234DD58CDC20 (U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void mixpanel.queue.PersistentQueueSession_<>c__DisplayClass15_0::<AsyncWriteToStream>b__0(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass15_0_U3CAsyncWriteToStreamU3Eb__0_m23B9A65D254C30C79EDC2DA9077ABFFE054C1915 (U3CU3Ec__DisplayClass15_0_tFB27BB80A5FEF8AB0C3D92470E7261AD39895A3C * __this, RuntimeObject* ___ar0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass15_0_U3CAsyncWriteToStreamU3Eb__0_m23B9A65D254C30C79EDC2DA9077ABFFE054C1915_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	bool V_2 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 3);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			// stream.EndWrite(ar);
			Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * L_0 = __this->get_stream_0();
			RuntimeObject* L_1 = ___ar0;
			NullCheck(L_0);
			VirtActionInvoker1< RuntimeObject* >::Invoke(23 /* System.Void System.IO.Stream::EndWrite(System.IAsyncResult) */, L_0, L_1);
			// }
			IL2CPP_LEAVE(0x51, FINALLY_0044);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__exception_local = (Exception_t *)e.ex;
			if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
				goto CATCH_000e;
			throw e;
		}

CATCH_000e:
		{ // begin catch(System.Exception)
			{
				// catch (Exception e)
				V_0 = ((Exception_t *)__exception_local);
				// lock (_pendingWritesFailures)
				PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * L_2 = __this->get_U3CU3E4__this_1();
				NullCheck(L_2);
				RuntimeObject* L_3 = L_2->get__pendingWritesFailures_2();
				V_1 = L_3;
				V_2 = (bool)0;
			}

IL_001d:
			try
			{ // begin try (depth: 3)
				RuntimeObject* L_4 = V_1;
				Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5(L_4, (bool*)(&V_2), /*hidden argument*/NULL);
				// _pendingWritesFailures.Add(e);
				PersistentQueueSession_tA6B90E7A8D108518385AAEF2F3A6438EE7A03359 * L_5 = __this->get_U3CU3E4__this_1();
				NullCheck(L_5);
				RuntimeObject* L_6 = L_5->get__pendingWritesFailures_2();
				Exception_t * L_7 = V_0;
				NullCheck(L_6);
				InterfaceActionInvoker1< Exception_t * >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.Exception>::Add(!0) */, ICollection_1_t369F219933FB866C23070CD9749538883440F044_il2cpp_TypeInfo_var, L_6, L_7);
				// }
				IL2CPP_LEAVE(0x42, FINALLY_0038);
			} // end try (depth: 3)
			catch(Il2CppExceptionWrapper& e)
			{
				__last_unhandled_exception = (Exception_t *)e.ex;
				goto FINALLY_0038;
			}

FINALLY_0038:
			{ // begin finally (depth: 3)
				{
					bool L_8 = V_2;
					if (!L_8)
					{
						goto IL_0041;
					}
				}

IL_003b:
				{
					RuntimeObject* L_9 = V_1;
					Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_9, /*hidden argument*/NULL);
				}

IL_0041:
				{
					IL2CPP_END_FINALLY(56)
				}
			} // end finally (depth: 3)
			IL2CPP_CLEANUP(56)
			{
				IL2CPP_JUMP_TBL(0x42, IL_0042)
				IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
			}

IL_0042:
			{
				// }
				IL2CPP_LEAVE(0x51, FINALLY_0044);
			}
		} // end catch (depth: 2)
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		// resetEvent.Set();
		ManualResetEvent_tDFAF117B200ECA4CCF4FD09593F949A016D55408 * L_10 = __this->get_resetEvent_2();
		NullCheck(L_10);
		EventWaitHandle_Set_m7959A86A39735296FC949EC86FDA42A6CFAAB94C(L_10, /*hidden argument*/NULL);
		// }
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x51, IL_0051)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0051:
	{
		// }, null);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.IO.FileStream mixpanel.queue.PersistentQueueUtils::CreateFileStream(System.String,System.IO.FileMode,System.IO.FileAccess,System.IO.FileOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * PersistentQueueUtils_CreateFileStream_m2499F0F23E8AD0EC95EE99007F7B399B2F86C26F (String_t* ___path0, int32_t ___mode1, int32_t ___access2, int32_t ___options3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueUtils_CreateFileStream_m2499F0F23E8AD0EC95EE99007F7B399B2F86C26F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// new FileStream(path, mode, access, FileShare.None, 0x10000, options);
		String_t* L_0 = ___path0;
		int32_t L_1 = ___mode1;
		int32_t L_2 = ___access2;
		int32_t L_3 = ___options3;
		FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * L_4 = (FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 *)il2cpp_codegen_object_new(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418_il2cpp_TypeInfo_var);
		FileStream__ctor_mE7B19C44ACADF935195F4209CECBC3EF962987C9(L_4, L_0, L_1, L_2, 0, ((int32_t)65536), L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.IO.FileStream mixpanel.queue.PersistentQueueUtils::CreateReadStream(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * PersistentQueueUtils_CreateReadStream_mB2A55B03F4A2A9A35139A87EEDF0520B5FE56981 (String_t* ___path0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueUtils_CreateReadStream_mB2A55B03F4A2A9A35139A87EEDF0520B5FE56981_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// CreateFileStream(path, FileMode.OpenOrCreate, FileAccess.Read, FileOptions.SequentialScan);
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_il2cpp_TypeInfo_var);
		FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * L_1 = PersistentQueueUtils_CreateFileStream_m2499F0F23E8AD0EC95EE99007F7B399B2F86C26F(L_0, 4, 1, ((int32_t)134217728), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.IO.FileStream mixpanel.queue.PersistentQueueUtils::CreateWriteStream(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * PersistentQueueUtils_CreateWriteStream_mFB0597CC35C0FC6AA0D37C9D58D059B9C79B5A26 (String_t* ___path0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueUtils_CreateWriteStream_mFB0597CC35C0FC6AA0D37C9D58D059B9C79B5A26_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// CreateFileStream(path, FileMode.Create, FileAccess.Write, FileOptions.WriteThrough | FileOptions.SequentialScan);
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_il2cpp_TypeInfo_var);
		FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * L_1 = PersistentQueueUtils_CreateFileStream_m2499F0F23E8AD0EC95EE99007F7B399B2F86C26F(L_0, 2, 2, ((int32_t)-2013265920), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void mixpanel.queue.PersistentQueueUtils::Read(System.String,System.Action`1<System.IO.Stream>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueUtils_Read_mED8A8E34A21E61A990018FDBC041DFC0F059AFC4 (String_t* ___path0, Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 * ___action1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueUtils_Read_mED8A8E34A21E61A990018FDBC041DFC0F059AFC4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (Lock)
		IL2CPP_RUNTIME_CLASS_INIT(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_il2cpp_TypeInfo_var);
		RuntimeObject * L_0 = ((PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_StaticFields*)il2cpp_codegen_static_fields_for(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_il2cpp_TypeInfo_var))->get_Lock_11();
		V_0 = L_0;
		V_1 = (bool)0;
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_1 = V_0;
			Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5(L_1, (bool*)(&V_1), /*hidden argument*/NULL);
			// if (File.Exists(path + ".old_copy"))
			String_t* L_2 = ___path0;
			String_t* L_3 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_2, _stringLiteralD50A5324DC4F48AA08597AC234FDF95918035D01, /*hidden argument*/NULL);
			bool L_4 = File_Exists_m6B9BDD8EEB33D744EB0590DD27BC0152FAFBD1FB(L_3, /*hidden argument*/NULL);
			if (!L_4)
			{
				goto IL_003b;
			}
		}

IL_0022:
		{
			// if (WaitDelete(path))
			String_t* L_5 = ___path0;
			IL2CPP_RUNTIME_CLASS_INIT(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_il2cpp_TypeInfo_var);
			bool L_6 = PersistentQueueUtils_WaitDelete_m11EE50B1C6DEE458F410146BAFF11CE1A8AA7195(L_5, /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_003b;
			}
		}

IL_002a:
		{
			// File.Move(path + ".old_copy", path);
			String_t* L_7 = ___path0;
			String_t* L_8 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_7, _stringLiteralD50A5324DC4F48AA08597AC234FDF95918035D01, /*hidden argument*/NULL);
			String_t* L_9 = ___path0;
			File_Move_mBBAEF2F3353F8E46E70495C88E1735C866E953B1(L_8, L_9, /*hidden argument*/NULL);
		}

IL_003b:
		{
			// using (FileStream stream = CreateReadStream(path))
			String_t* L_10 = ___path0;
			IL2CPP_RUNTIME_CLASS_INIT(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_il2cpp_TypeInfo_var);
			FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * L_11 = PersistentQueueUtils_CreateReadStream_mB2A55B03F4A2A9A35139A87EEDF0520B5FE56981(L_10, /*hidden argument*/NULL);
			V_2 = L_11;
		}

IL_0042:
		try
		{ // begin try (depth: 2)
			// action(stream);
			Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 * L_12 = ___action1;
			FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * L_13 = V_2;
			NullCheck(L_12);
			Action_1_Invoke_m25501B02ECA680749841E48EEA596F763A82F475(L_12, L_13, /*hidden argument*/Action_1_Invoke_m25501B02ECA680749841E48EEA596F763A82F475_RuntimeMethod_var);
			// }
			IL2CPP_LEAVE(0x5F, FINALLY_004b);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_004b;
		}

FINALLY_004b:
		{ // begin finally (depth: 2)
			{
				FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * L_14 = V_2;
				if (!L_14)
				{
					goto IL_0054;
				}
			}

IL_004e:
			{
				FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * L_15 = V_2;
				NullCheck(L_15);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_15);
			}

IL_0054:
			{
				IL2CPP_END_FINALLY(75)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(75)
		{
			IL2CPP_END_CLEANUP(0x5F, FINALLY_0055);
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0055;
	}

FINALLY_0055:
	{ // begin finally (depth: 1)
		{
			bool L_16 = V_1;
			if (!L_16)
			{
				goto IL_005e;
			}
		}

IL_0058:
		{
			RuntimeObject * L_17 = V_0;
			Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_17, /*hidden argument*/NULL);
		}

IL_005e:
		{
			IL2CPP_END_FINALLY(85)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(85)
	{
		IL2CPP_JUMP_TBL(0x5F, IL_005f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_005f:
	{
		// }
		return;
	}
}
// System.Void mixpanel.queue.PersistentQueueUtils::Write(System.String,System.Action`1<System.IO.Stream>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueUtils_Write_mD0CD814DFFBA6D65FC4EE712F97F5EFF56B51D18 (String_t* ___path0, Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 * ___action1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueUtils_Write_mD0CD814DFFBA6D65FC4EE712F97F5EFF56B51D18_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (Lock)
		IL2CPP_RUNTIME_CLASS_INIT(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_il2cpp_TypeInfo_var);
		RuntimeObject * L_0 = ((PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_StaticFields*)il2cpp_codegen_static_fields_for(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_il2cpp_TypeInfo_var))->get_Lock_11();
		V_0 = L_0;
		V_1 = (bool)0;
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_1 = V_0;
			Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5(L_1, (bool*)(&V_1), /*hidden argument*/NULL);
			// if (File.Exists(path + ".old_copy") == false)
			String_t* L_2 = ___path0;
			String_t* L_3 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_2, _stringLiteralD50A5324DC4F48AA08597AC234FDF95918035D01, /*hidden argument*/NULL);
			bool L_4 = File_Exists_m6B9BDD8EEB33D744EB0590DD27BC0152FAFBD1FB(L_3, /*hidden argument*/NULL);
			if (L_4)
			{
				goto IL_0033;
			}
		}

IL_0022:
		{
			// File.Move(path, path + ".old_copy");
			String_t* L_5 = ___path0;
			String_t* L_6 = ___path0;
			String_t* L_7 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_6, _stringLiteralD50A5324DC4F48AA08597AC234FDF95918035D01, /*hidden argument*/NULL);
			File_Move_mBBAEF2F3353F8E46E70495C88E1735C866E953B1(L_5, L_7, /*hidden argument*/NULL);
		}

IL_0033:
		{
			// using (FileStream stream = CreateWriteStream(path))
			String_t* L_8 = ___path0;
			IL2CPP_RUNTIME_CLASS_INIT(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_il2cpp_TypeInfo_var);
			FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * L_9 = PersistentQueueUtils_CreateWriteStream_mFB0597CC35C0FC6AA0D37C9D58D059B9C79B5A26(L_8, /*hidden argument*/NULL);
			V_2 = L_9;
		}

IL_003a:
		try
		{ // begin try (depth: 2)
			// action(stream);
			Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 * L_10 = ___action1;
			FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * L_11 = V_2;
			NullCheck(L_10);
			Action_1_Invoke_m25501B02ECA680749841E48EEA596F763A82F475(L_10, L_11, /*hidden argument*/Action_1_Invoke_m25501B02ECA680749841E48EEA596F763A82F475_RuntimeMethod_var);
			// stream.Flush();
			FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * L_12 = V_2;
			NullCheck(L_12);
			VirtActionInvoker0::Invoke(18 /* System.Void System.IO.Stream::Flush() */, L_12);
			// }
			IL2CPP_LEAVE(0x53, FINALLY_0049);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0049;
		}

FINALLY_0049:
		{ // begin finally (depth: 2)
			{
				FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * L_13 = V_2;
				if (!L_13)
				{
					goto IL_0052;
				}
			}

IL_004c:
			{
				FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * L_14 = V_2;
				NullCheck(L_14);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_14);
			}

IL_0052:
			{
				IL2CPP_END_FINALLY(73)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(73)
		{
			IL2CPP_JUMP_TBL(0x53, IL_0053)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_0053:
		{
			// WaitDelete(path + ".old_copy");
			String_t* L_15 = ___path0;
			String_t* L_16 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_15, _stringLiteralD50A5324DC4F48AA08597AC234FDF95918035D01, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_il2cpp_TypeInfo_var);
			PersistentQueueUtils_WaitDelete_m11EE50B1C6DEE458F410146BAFF11CE1A8AA7195(L_16, /*hidden argument*/NULL);
			// }
			IL2CPP_LEAVE(0x70, FINALLY_0066);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0066;
	}

FINALLY_0066:
	{ // begin finally (depth: 1)
		{
			bool L_17 = V_1;
			if (!L_17)
			{
				goto IL_006f;
			}
		}

IL_0069:
		{
			RuntimeObject * L_18 = V_0;
			Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_18, /*hidden argument*/NULL);
		}

IL_006f:
		{
			IL2CPP_END_FINALLY(102)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(102)
	{
		IL2CPP_JUMP_TBL(0x70, IL_0070)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0070:
	{
		// }
		return;
	}
}
// System.Boolean mixpanel.queue.PersistentQueueUtils::WaitDelete(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PersistentQueueUtils_WaitDelete_m11EE50B1C6DEE458F410146BAFF11CE1A8AA7195 (String_t* ___s0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueUtils_WaitDelete_m11EE50B1C6DEE458F410146BAFF11CE1A8AA7195_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// for (int i = 0; i < 5; i++)
		V_0 = 0;
		goto IL_001d;
	}

IL_0004:
	{
	}

IL_0005:
	try
	{ // begin try (depth: 1)
		// File.Delete(s);
		String_t* L_0 = ___s0;
		File_Delete_mBE814E569EAB07FAD140C6DCDB957F1CB8C85DE2(L_0, /*hidden argument*/NULL);
		// return true;
		V_1 = (bool)1;
		goto IL_0023;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_000f;
		throw e;
	}

CATCH_000f:
	{ // begin catch(System.Object)
		// catch
		// Thread.Sleep(100);
		Thread_Sleep_m2CD320EAB7BE02CC1F395EAFE9970D53A5F9EAEF(((int32_t)100), /*hidden argument*/NULL);
		// }
		goto IL_0019;
	} // end catch (depth: 1)

IL_0019:
	{
		// for (int i = 0; i < 5; i++)
		int32_t L_1 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)1));
	}

IL_001d:
	{
		// for (int i = 0; i < 5; i++)
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)5)))
		{
			goto IL_0004;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_0023:
	{
		// }
		bool L_3 = V_1;
		return L_3;
	}
}
// System.Void mixpanel.queue.PersistentQueueUtils::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersistentQueueUtils__cctor_m4AA6BE8D00B1C62A5E75A61DEB9828A27D4C63EE (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentQueueUtils__cctor_m4AA6BE8D00B1C62A5E75A61DEB9828A27D4C63EE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static readonly byte[] OperationSeparatorBytes = BitConverter.GetBytes(OperationSeparator);
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_tD5DF1CB5C5A5CB087D90BD881C8E75A332E546EE_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = BitConverter_GetBytes_mB5BCBAAFE3AE14F2AF1731187C7155A236DF38EA(((int32_t)1123990689), /*hidden argument*/NULL);
		((PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_StaticFields*)il2cpp_codegen_static_fields_for(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_il2cpp_TypeInfo_var))->set_OperationSeparatorBytes_1(L_0);
		// public static readonly Guid StartTransactionSeparatorGuid = new Guid("b75bfb12-93bb-42b6-acb1-a897239ea3a5");
		Guid_t  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Guid__ctor_mC668142577A40A77D13B78AADDEFFFC2E2705079((&L_1), _stringLiteralAA90888F4CA0449EE8C332709193E9D1C885CC1A, /*hidden argument*/NULL);
		((PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_StaticFields*)il2cpp_codegen_static_fields_for(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_il2cpp_TypeInfo_var))->set_StartTransactionSeparatorGuid_2(L_1);
		// public static readonly byte[] StartTransactionSeparator = StartTransactionSeparatorGuid.ToByteArray();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = Guid_ToByteArray_m5E99B09A26EA3A1943CC8FE697E247DAC5465223((Guid_t *)(((PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_StaticFields*)il2cpp_codegen_static_fields_for(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_il2cpp_TypeInfo_var))->get_address_of_StartTransactionSeparatorGuid_2()), /*hidden argument*/NULL);
		((PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_StaticFields*)il2cpp_codegen_static_fields_for(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_il2cpp_TypeInfo_var))->set_StartTransactionSeparator_3(L_2);
		// public static readonly Guid EndTransactionSeparatorGuid = new Guid("866c9705-4456-4e9d-b452-3146b3bfa4ce");
		Guid_t  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Guid__ctor_mC668142577A40A77D13B78AADDEFFFC2E2705079((&L_3), _stringLiteralB2CAAE2FF3E57291CEF022D8A7EAA7C6A6ABDC74, /*hidden argument*/NULL);
		((PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_StaticFields*)il2cpp_codegen_static_fields_for(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_il2cpp_TypeInfo_var))->set_EndTransactionSeparatorGuid_4(L_3);
		// public static readonly byte[] EndTransactionSeparator = EndTransactionSeparatorGuid.ToByteArray();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = Guid_ToByteArray_m5E99B09A26EA3A1943CC8FE697E247DAC5465223((Guid_t *)(((PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_StaticFields*)il2cpp_codegen_static_fields_for(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_il2cpp_TypeInfo_var))->get_address_of_EndTransactionSeparatorGuid_4()), /*hidden argument*/NULL);
		((PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_StaticFields*)il2cpp_codegen_static_fields_for(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_il2cpp_TypeInfo_var))->set_EndTransactionSeparator_5(L_4);
		// private static readonly object Lock = new object();
		RuntimeObject * L_5 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(L_5, /*hidden argument*/NULL);
		((PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_StaticFields*)il2cpp_codegen_static_fields_for(PersistentQueueUtils_t600328259E35E016F31A48EBE8E026FDEE3A5BEE_il2cpp_TypeInfo_var))->set_Lock_11(L_5);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t PersistentQueue_get_CurrentFileNumber_mEA54919870FD34A7E69FCD761993FB1C592B3986_inline (PersistentQueue_t79F57183C7BA70AAD27A43F217DD48797AD86C26 * __this, const RuntimeMethod* method)
{
	{
		// public int CurrentFileNumber { get; private set; }
		int32_t L_0 = __this->get_U3CCurrentFileNumberU3Ek__BackingField_12();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t KeyValuePair_2_get_Value_mF7293AF44DA0B8EB7B455A6227F7C36EEE9CF508_gshared_inline (KeyValuePair_2_tA9AFBC865B07606ED8F020A8E3AF8E27491AF809 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t KeyValuePair_2_get_Key_mB735BC2D7232A3B45D667D28C17BA51AAAFFB4A1_gshared_inline (KeyValuePair_2_tA9AFBC865B07606ED8F020A8E3AF8E27491AF809 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return L_0;
	}
}
