﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void ApiExample::Update()
extern void ApiExample_Update_m3B2E77CA197799B03F83F1B29D466EC4976AC1CC ();
// 0x00000002 System.Void ApiExample::FixedUpdate()
extern void ApiExample_FixedUpdate_m4F49759DE3C2BB60CD28807C5852FF3BF7558DD7 ();
// 0x00000003 System.Void ApiExample::Start()
extern void ApiExample_Start_mAF6A04D0D0B7297C5C5DD6A11D7B4A722C880F0C ();
// 0x00000004 System.String ApiExample::ReturnXL_Link(System.String)
extern void ApiExample_ReturnXL_Link_mA8A7868FFC871033F108C61F916FC1E06F5A8603 ();
// 0x00000005 UnityEngine.Texture2D ApiExample::CropTex(UnityEngine.Texture2D)
extern void ApiExample_CropTex_m89926E40021BEDD468EBE559B3B7AC937EED86ED ();
// 0x00000006 System.Void ApiExample::GetDetailProduct(System.String)
extern void ApiExample_GetDetailProduct_mD16F1D07CFFF3926B6AC955BBE701D6113E38453 ();
// 0x00000007 System.Void ApiExample::TestCategories(System.String,System.String)
extern void ApiExample_TestCategories_m87E82886B283BC4EA03008D76C45E68445D09F2F ();
// 0x00000008 System.Collections.IEnumerator ApiExample::GETTexWithTimer()
extern void ApiExample_GETTexWithTimer_m44FB46DD924C3B349AF7FE12BB1E18661690A0EB ();
// 0x00000009 System.Collections.IEnumerator ApiExample::GETWITHHEADER()
extern void ApiExample_GETWITHHEADER_mC0914C9ABECDAAD63A334FEF83ECC80B645F36DA ();
// 0x0000000A System.Collections.IEnumerator ApiExample::DLExample(UnityEngine.GameObject)
extern void ApiExample_DLExample_m0408D673C3592FBC07409053874594CED0146DE1 ();
// 0x0000000B UnityEngine.Color32[] ApiExample::Encode(System.String,System.Int32,System.Int32)
extern void ApiExample_Encode_mB19386CEFF0ED408DD6F07A4BE39C7B41DE94349 ();
// 0x0000000C UnityEngine.Texture2D ApiExample::generateQR(System.String)
extern void ApiExample_generateQR_m5E16397D3E071C8843B89C191197020A18EC8404 ();
// 0x0000000D System.Void ApiExample::DownloadLP()
extern void ApiExample_DownloadLP_mE166EC15058DE3BA2CF698458CD78FB485FD281B ();
// 0x0000000E System.Void ApiExample::DownloadContinuouslyProductDetail(System.String)
extern void ApiExample_DownloadContinuouslyProductDetail_m1CB409FC22BF1C36049C3BDE5F1DF5AE68B5C960 ();
// 0x0000000F System.Void ApiExample::DownloadProductNDetail(System.Int32)
extern void ApiExample_DownloadProductNDetail_mAD4411CD0F714EE7E0BB20E922DCE7F98AFA887F ();
// 0x00000010 System.Collections.IEnumerator ApiExample::Downloading(System.String)
extern void ApiExample_Downloading_mCE39887CF1662BF30445FB50DE802B801463D677 ();
// 0x00000011 System.Collections.IEnumerator ApiExample::GetRequest(System.String)
extern void ApiExample_GetRequest_m81264FB9877B70E2CD2BD7143A14813196BEF953 ();
// 0x00000012 System.Void ApiExample::DownloadReadImageAsync(System.String,System.String,System.String,System.String,System.String,System.String)
extern void ApiExample_DownloadReadImageAsync_m2F02B230074A1477C5031C4A95BF383717149B2B ();
// 0x00000013 System.Void ApiExample::ReadImageAsync()
extern void ApiExample_ReadImageAsync_mD92832EB192D20F20E5F7BB92247A63BFF8382B8 ();
// 0x00000014 System.Collections.IEnumerator ApiExample::ReduceTexture2(Intelistyle.LatestProduct)
extern void ApiExample_ReduceTexture2_m53B91140DC9847BE0B127F6CD6301442E8FB82F5 ();
// 0x00000015 System.Collections.IEnumerator ApiExample::ReduceTexture(Intelistyle.LatestProduct)
extern void ApiExample_ReduceTexture_m941EC2E650758C1A74F425397366B9A4EE7067F2 ();
// 0x00000016 System.Void ApiExample::PreparingDatas(Intelistyle.LatestProduct)
extern void ApiExample_PreparingDatas_m133775AEF6D54A48A0A1B0F972C8F3653FBAFD45 ();
// 0x00000017 System.Collections.IEnumerator ApiExample::GetTexture(Intelistyle.LatestProduct)
extern void ApiExample_GetTexture_mD7BDBC2232403E5FC626A4EE8A66E748D523255A ();
// 0x00000018 System.Void ApiExample::GetStyleIt(System.String)
extern void ApiExample_GetStyleIt_mAB5B4159F9F72CF7316FD2C3779B97B582420B9A ();
// 0x00000019 System.Void ApiExample::GetSimilarItems(System.String)
extern void ApiExample_GetSimilarItems_m0EF41B26486AF562AA94C970DB358419A5DC5AFB ();
// 0x0000001A System.Void ApiExample::.ctor()
extern void ApiExample__ctor_m061AF5CFC6883F332A265A8A733691941BA0496C ();
// 0x0000001B System.Void ApiManager::Awake()
extern void ApiManager_Awake_mA377DC3B82D5260B87106E695C44F82012521158 ();
// 0x0000001C System.Void ApiManager::Start()
extern void ApiManager_Start_m7D54DEE32C56D90294D4229CF34C0DE021A6C3C6 ();
// 0x0000001D Proyecto26.RequestHelper ApiManager::ROLatestProduct(System.String,System.String,System.String,System.String)
extern void ApiManager_ROLatestProduct_m9501321C4F69267EA0E00CAB3870830F7B565E51 ();
// 0x0000001E Proyecto26.RequestHelper ApiManager::ROProductDetail(System.String,System.String)
extern void ApiManager_ROProductDetail_mACC478C78FE2428F4ABBBBF19FEB62927BE605D9 ();
// 0x0000001F Proyecto26.RequestHelper ApiManager::ROSimilarProduct(System.String,System.String)
extern void ApiManager_ROSimilarProduct_m596D333C2F8BB21AEDCBE2A6814F9FF2444B831F ();
// 0x00000020 Proyecto26.RequestHelper ApiManager::ROStyleWithIt(System.String,System.String)
extern void ApiManager_ROStyleWithIt_m7C0FE8761C87113507101503AF05BDAF297CC19A ();
// 0x00000021 System.String ApiManager::StyleWithItBody(System.String,System.String)
extern void ApiManager_StyleWithItBody_m06B8F0E5988EE1DF1FA237F6C7686D9F455E937C ();
// 0x00000022 System.String ApiManager::SimilarProductBody(System.String,System.String)
extern void ApiManager_SimilarProductBody_m8B3A843418E08D397770DCCB30D9BB2F67DDDAB0 ();
// 0x00000023 System.String ApiManager::LatestProductBody(System.String,System.String,System.String,System.String)
extern void ApiManager_LatestProductBody_m423F2CF62B4BF8F14589556940D1B4748D3BF0C7 ();
// 0x00000024 System.String ApiManager::ProductDetailBody(System.String,System.String)
extern void ApiManager_ProductDetailBody_m8C2A4DEA8BEDF781E0AEDA7D4A86FF214A40490C ();
// 0x00000025 Intelistyle.LatestProduct[] ApiManager::SaveLatestProduct(System.String)
extern void ApiManager_SaveLatestProduct_m177AB9859FE53640A0833ECE326D30494130116C ();
// 0x00000026 DetailProduct ApiManager::SaveProductDetail(System.String,System.String,System.Int32)
extern void ApiManager_SaveProductDetail_m9206DDE74A252D3DA1B9545641AB046C24F5012B ();
// 0x00000027 System.String ApiManager::FindWeirdChar(System.String)
extern void ApiManager_FindWeirdChar_m2E28BC3D56C53A7ECFB60562DEC07AD67D1D393E ();
// 0x00000028 UnityEngine.Color32[] ApiManager::Encode(System.String,System.Int32,System.Int32)
extern void ApiManager_Encode_mB486F36E89E55962836C8F347C384FE2AC55C8C0 ();
// 0x00000029 UnityEngine.Texture2D ApiManager::generateQR(System.String,UnityEngine.Vector2)
extern void ApiManager_generateQR_m07C4623AC03EDB3F55FB9637F7BFFCB6D84C4B9E ();
// 0x0000002A System.Void ApiManager::ShuffleArray(System.Collections.Generic.List`1<T>)
// 0x0000002B System.Void ApiManager::.ctor()
extern void ApiManager__ctor_m191ED4BD1DAA72F9C42E142FFFE1103F0665E786 ();
// 0x0000002C System.Void DetailProduct::.ctor()
extern void DetailProduct__ctor_m86280B0407EDB756B0E7045F562BD06AC185D0E0 ();
// 0x0000002D System.Void TAG::.ctor()
extern void TAG__ctor_mF4A24B9D1E4D5CCA8D402975E943754F3768DF18 ();
// 0x0000002E System.Void DATA::.ctor()
extern void DATA__ctor_m49281891E81412C15A73B01152D6B47E942ACBC0 ();
// 0x0000002F System.Void LoadingScene::Awake()
extern void LoadingScene_Awake_mCDEEB637850BBAE4FBE307841D0EE50705B3C331 ();
// 0x00000030 System.Void LoadingScene::Start()
extern void LoadingScene_Start_m7341A0A7817AEF05816C2920042FBECA67DE28FC ();
// 0x00000031 System.Void LoadingScene::ErrMessage()
extern void LoadingScene_ErrMessage_m30A479F970DAEB8F7D74BD9A3B3B3A8D20F3B589 ();
// 0x00000032 System.Void LoadingScene::Init(System.Boolean)
extern void LoadingScene_Init_m7F714B8605A97C76444C8DD9E66D94887F346213 ();
// 0x00000033 System.Void LoadingScene::StartDownloading(System.Boolean)
extern void LoadingScene_StartDownloading_m41547C24257C3BE77D2BCBEB04FC81ABDF3A8344 ();
// 0x00000034 System.Void LoadingScene::Update()
extern void LoadingScene_Update_mEB55EF55BBB68FF4363980BDF27D5CB85D77B417 ();
// 0x00000035 System.Void LoadingScene::ChangeColumn()
extern void LoadingScene_ChangeColumn_m2B7B626540DED9F3AE11154145240AB32DDD4B3D ();
// 0x00000036 System.Void LoadingScene::ChangingMode()
extern void LoadingScene_ChangingMode_m6D15BCEBB56901798BC45AFC93F7058EB991C310 ();
// 0x00000037 System.Void LoadingScene::InitAct(System.Boolean)
extern void LoadingScene_InitAct_m9F656A07A56FEC2CBEA6CCFADA78D737F508E2E2 ();
// 0x00000038 System.Void LoadingScene::MultiAct()
extern void LoadingScene_MultiAct_m40CA43F99D1700B00869096840FBF9108B9A6135 ();
// 0x00000039 System.Void LoadingScene::MultiDownloadAPI(Proyecto26.RequestHelper,System.Int32)
extern void LoadingScene_MultiDownloadAPI_m5BD8E8CF054D47CAEEDE98160E114A242B86720B ();
// 0x0000003A System.Void LoadingScene::CheckAPIBool()
extern void LoadingScene_CheckAPIBool_mFB5E5C864BAA0E557F7063B56D24928D2EE92FDC ();
// 0x0000003B System.Void LoadingScene::MultiDownload()
extern void LoadingScene_MultiDownload_mB1C963051D5546C01408D1D676815F67973B2C63 ();
// 0x0000003C System.Void LoadingScene::MultiDownloadDivier(System.Int32,System.Int32)
extern void LoadingScene_MultiDownloadDivier_mD22099F28F919B31E45A3C083003B83EFDB31D75 ();
// 0x0000003D System.Collections.IEnumerator LoadingScene::MultiDownloadNCache(Intelistyle.LatestProduct,System.Int32)
extern void LoadingScene_MultiDownloadNCache_mB637FEC88AFEC768CF8F5BFD2C0A43EE9556BBCA ();
// 0x0000003E UnityEngine.Texture2D LoadingScene::CropTex(UnityEngine.Texture2D)
extern void LoadingScene_CropTex_m70F8E4536A8662A8AD0293E61E1E8D146C4F4054 ();
// 0x0000003F System.Void LoadingScene::MultiCheck()
extern void LoadingScene_MultiCheck_m988668F4C75B95F6601CC4F670BF4D97848EA06D ();
// 0x00000040 System.Collections.IEnumerator LoadingScene::_MultiDownloadDetail(Intelistyle.LatestProduct,System.Int32)
extern void LoadingScene__MultiDownloadDetail_mB2BD4F4E01DC0083E1E9C50A5237CE2A87B29FB5 ();
// 0x00000041 System.Void LoadingScene::MultiDownloadDetail(Intelistyle.LatestProduct,System.Int32)
extern void LoadingScene_MultiDownloadDetail_m92296284AB4A86AD7CB4C90CEB63904CB873B1C6 ();
// 0x00000042 System.Collections.IEnumerator LoadingScene::MultiDLDetailNCache(DetailProduct,System.Int32,Intelistyle.LatestProduct,System.Int32)
extern void LoadingScene_MultiDLDetailNCache_m0F2BADE4A363EA2C026D186EF2D570916251F6DB ();
// 0x00000043 System.Void LoadingScene::GetStyleIt(System.String,System.Int32)
extern void LoadingScene_GetStyleIt_m8F00F766E315DCCE1DEE7430B98C70D7D7C1E268 ();
// 0x00000044 System.Void LoadingScene::GOBtn()
extern void LoadingScene_GOBtn_mDDFE20936BDAD5E465ADF9867132B6828D113F78 ();
// 0x00000045 System.Void LoadingScene::RestartScene()
extern void LoadingScene_RestartScene_mFF0B708A8A850CA8DDE0B1292D4EC91240BA38EF ();
// 0x00000046 System.Collections.Generic.List`1<System.Boolean> LoadingScene::GetDownloadList(System.Int32)
extern void LoadingScene_GetDownloadList_mC770B4D8D2A2AF13CC1C6D41DE886623B5E2E33A ();
// 0x00000047 System.Void LoadingScene::ResetDownloadList()
extern void LoadingScene_ResetDownloadList_m38F60929ADDD0DA0DCA08CA0861296EEAB22106E ();
// 0x00000048 System.Void LoadingScene::.ctor()
extern void LoadingScene__ctor_m04314096CD284A7CD2CDDE1996B412F59B12D4FA ();
// 0x00000049 System.Void LoadingScene::<StartDownloading>b__15_0()
extern void LoadingScene_U3CStartDownloadingU3Eb__15_0_m0B5F9D7D2E22F78A60E2798B2E1B59ACE31228E2 ();
// 0x0000004A System.Void LoadingSceneSlowVersion::Awake()
extern void LoadingSceneSlowVersion_Awake_m0F6414C50FFCD3156683F23D6F73414452FC70D9 ();
// 0x0000004B System.Void LoadingSceneSlowVersion::Start()
extern void LoadingSceneSlowVersion_Start_m8E9CD19BDDA4266614129D5BC3AA13248A48AFF2 ();
// 0x0000004C System.Void LoadingSceneSlowVersion::Init()
extern void LoadingSceneSlowVersion_Init_m9A43E95005F52D2BFE97A1A6AF0681A449DC5C55 ();
// 0x0000004D System.Void LoadingSceneSlowVersion::Update()
extern void LoadingSceneSlowVersion_Update_m22C7F1D52A7284E3835240D0F6C99856FC0CB5CE ();
// 0x0000004E System.Void LoadingSceneSlowVersion::ChangingMode()
extern void LoadingSceneSlowVersion_ChangingMode_mCFBDE2950D15E7FF7E1C82E9B6A49368011689D9 ();
// 0x0000004F System.Void LoadingSceneSlowVersion::InitAct()
extern void LoadingSceneSlowVersion_InitAct_m2A6AF00DADBB38ED1882BBAA93E519F1E7A825BC ();
// 0x00000050 System.Void LoadingSceneSlowVersion::MultiAct()
extern void LoadingSceneSlowVersion_MultiAct_mB14B0541F3133AB399DDC72BD2F1808BBF2BD804 ();
// 0x00000051 System.Void LoadingSceneSlowVersion::DownloadAPI(Proyecto26.RequestHelper,System.Int32)
extern void LoadingSceneSlowVersion_DownloadAPI_m52C284A20BA453E0595B0337B7AE6FA75656B544 ();
// 0x00000052 System.Void LoadingSceneSlowVersion::MultiDownload()
extern void LoadingSceneSlowVersion_MultiDownload_m4D9F9105860CCE540998352E4A5849FB9AA86EA4 ();
// 0x00000053 System.Void LoadingSceneSlowVersion::RestartScene()
extern void LoadingSceneSlowVersion_RestartScene_m98C68E14BE2D1F35B5AA7A194A789782915DEB90 ();
// 0x00000054 System.Void LoadingSceneSlowVersion::.ctor()
extern void LoadingSceneSlowVersion__ctor_m4B33F6A32D2FB6776455AB213E7C81E4930C30A0 ();
// 0x00000055 System.Void StyleIt::.ctor()
extern void StyleIt__ctor_mE5ED8C2885AF921DD246CCBF8F80BE6CFE0D5C3D ();
// 0x00000056 System.Void TextureScale::Point(UnityEngine.Texture2D,System.Int32,System.Int32)
extern void TextureScale_Point_m7D79D2581343446711D6ECEE7D1366B870FD2D21 ();
// 0x00000057 System.Void TextureScale::Bilinear(UnityEngine.Texture2D,System.Int32,System.Int32)
extern void TextureScale_Bilinear_m19634C708F31692207126257D6E3045AEAD163EB ();
// 0x00000058 System.Void TextureScale::ThreadedScale(UnityEngine.Texture2D,System.Int32,System.Int32,System.Boolean)
extern void TextureScale_ThreadedScale_m7A51B5F57C7908ACD9698BB27C43CD1C8A4C9527 ();
// 0x00000059 System.Void TextureScale::BilinearScale(System.Object)
extern void TextureScale_BilinearScale_mCE3B99635F9CD5653B0802466250A722FEA4799B ();
// 0x0000005A System.Void TextureScale::PointScale(System.Object)
extern void TextureScale_PointScale_m0D7A658B33A3BDD5A6E386BB79285B621F672F84 ();
// 0x0000005B UnityEngine.Color TextureScale::ColorLerpUnclamped(UnityEngine.Color,UnityEngine.Color,System.Single)
extern void TextureScale_ColorLerpUnclamped_mB7923465A92B145C734DBCA973508005124AEF63 ();
// 0x0000005C System.Void TextureScale::.ctor()
extern void TextureScale__ctor_m6EBAB8CDD2CC174FE3C4E30C5A8CE1B8136BF5A9 ();
// 0x0000005D System.Void UnityMainThread::Awake()
extern void UnityMainThread_Awake_m1D1ADC061302A2D509A7FD88AEC0F670A7A13C1D ();
// 0x0000005E System.Void UnityMainThread::Update()
extern void UnityMainThread_Update_mED329E025414CF950307678274E0D9B5D0771DA5 ();
// 0x0000005F System.Void UnityMainThread::AddJob(System.Action)
extern void UnityMainThread_AddJob_mD0D37FBE2D3C9E63D93A9E85A866AC349AE12187 ();
// 0x00000060 System.Collections.IEnumerator UnityMainThread::SpeedRenderQueue()
extern void UnityMainThread_SpeedRenderQueue_m0C671D395104C530710236C7973E4301C6EF906C ();
// 0x00000061 System.Void UnityMainThread::.ctor()
extern void UnityMainThread__ctor_mAB5B2022B463D0810D4D6675E9C435351F820009 ();
// 0x00000062 System.Void UnityThread::initUnityThread(System.Boolean)
extern void UnityThread_initUnityThread_m04D8C8A616CA2CFE59936ACD285CA0E6CE97F1CA ();
// 0x00000063 System.Void UnityThread::Awake()
extern void UnityThread_Awake_m042E7A2495E1F4F4956EE606B19D6CF2239F1CC4 ();
// 0x00000064 System.Void UnityThread::executeCoroutine(System.Collections.IEnumerator)
extern void UnityThread_executeCoroutine_m229FB4AE98BF2EF4516041F446593F44559D1A5A ();
// 0x00000065 System.Void UnityThread::executeInUpdate(System.Action)
extern void UnityThread_executeInUpdate_m207EBF73F71FC33F4E336719369A9ECE2412C0E3 ();
// 0x00000066 System.Void UnityThread::Update()
extern void UnityThread_Update_m6FB70D79F2D1E69BEE2D313C6EAD1DB00A0C8AD2 ();
// 0x00000067 System.Void UnityThread::executeInLateUpdate(System.Action)
extern void UnityThread_executeInLateUpdate_m7D81850D6544AB6B572E7C63005AE70E061A1FA4 ();
// 0x00000068 System.Void UnityThread::LateUpdate()
extern void UnityThread_LateUpdate_mF1D62411D4EC9F3BEA9E73C46FFB4ADFB974FE7A ();
// 0x00000069 System.Void UnityThread::executeInFixedUpdate(System.Action)
extern void UnityThread_executeInFixedUpdate_mB10742C263E9C724A08103CD24B62630E81F03F6 ();
// 0x0000006A System.Void UnityThread::FixedUpdate()
extern void UnityThread_FixedUpdate_m3A7FF39632283C0DF329614B01DF6686D65F8B55 ();
// 0x0000006B System.Void UnityThread::OnDisable()
extern void UnityThread_OnDisable_m02FD692EA4251FCA9FE54695E638A7555EDE5CBF ();
// 0x0000006C System.Void UnityThread::.ctor()
extern void UnityThread__ctor_m0FEF37FE1A8C04C56348B3B14196833512BE67F8 ();
// 0x0000006D System.Void UnityThread::.cctor()
extern void UnityThread__cctor_m86A9FB85C688B5F869227516214135999CA14938 ();
// 0x0000006E System.Void OldGUIExamplesCS::Start()
extern void OldGUIExamplesCS_Start_m5C679AD607A16C85DDFAE78B996120E58E8989E9 ();
// 0x0000006F System.Void OldGUIExamplesCS::catMoved()
extern void OldGUIExamplesCS_catMoved_m844C01EC2655FB6AEA6261F74BE3AFE380B195A7 ();
// 0x00000070 System.Void OldGUIExamplesCS::OnGUI()
extern void OldGUIExamplesCS_OnGUI_mE08A1CA5C406551B5D66A4A6BB86E00285F97C30 ();
// 0x00000071 System.Void OldGUIExamplesCS::.ctor()
extern void OldGUIExamplesCS__ctor_m626C9BA19A539E78953626625960526EDB180F4A ();
// 0x00000072 System.Void TestingPunch::Start()
extern void TestingPunch_Start_mB88A524025639F5F9A18D9B82E98E30EB3356D06 ();
// 0x00000073 System.Void TestingPunch::Update()
extern void TestingPunch_Update_m9B5977F7EA9B1FC0F7D69A7E202DC2FA38BBEAA8 ();
// 0x00000074 System.Void TestingPunch::tweenStatically(UnityEngine.GameObject)
extern void TestingPunch_tweenStatically_m8E1F7A46E2EAAB6AEDD62E0C2428EC4EBAD0C7F3 ();
// 0x00000075 System.Void TestingPunch::enterMiniGameStart(System.Object)
extern void TestingPunch_enterMiniGameStart_m2FAE7161DDC3665930335EABF53A611D1DDA65A1 ();
// 0x00000076 System.Void TestingPunch::updateColor(UnityEngine.Color)
extern void TestingPunch_updateColor_mFA69FF82ECDF6CD2EC12354370879F019E15D477 ();
// 0x00000077 System.Void TestingPunch::delayedMethod(System.Object)
extern void TestingPunch_delayedMethod_mF8E7A6CE3054ACE65057D43047ECAB6E458C4E84 ();
// 0x00000078 System.Void TestingPunch::destroyOnComp(System.Object)
extern void TestingPunch_destroyOnComp_m0D7E0E3DA9113B3B5F265EBC69AEB3CC70D63E13 ();
// 0x00000079 System.String TestingPunch::curveToString(UnityEngine.AnimationCurve)
extern void TestingPunch_curveToString_mDE53974600DB58372C612CB078B663510C667F61 ();
// 0x0000007A System.Void TestingPunch::.ctor()
extern void TestingPunch__ctor_m9A8492FDD64928449DB0DB92CD16061D642E4A6A ();
// 0x0000007B System.Void TestingPunch::<Update>b__4_0()
extern void TestingPunch_U3CUpdateU3Eb__4_0_m8FB81D468DBC883BE134CF8314A05D3482BE566C ();
// 0x0000007C System.Void TestingPunch::<Update>b__4_3()
extern void TestingPunch_U3CUpdateU3Eb__4_3_m542BB1341B4AECC188C74DCBE4F49CA2AB8AC4BE ();
// 0x0000007D System.Void TestingPunch::<Update>b__4_7(UnityEngine.Vector2)
extern void TestingPunch_U3CUpdateU3Eb__4_7_m0D94A53B95AF073898CBDEAD58DC696E6E060066 ();
// 0x0000007E System.Void TestingRigidbodyCS::Start()
extern void TestingRigidbodyCS_Start_m936906C87BBDA6BF06D51D430EF08BB8D14E0376 ();
// 0x0000007F System.Void TestingRigidbodyCS::Update()
extern void TestingRigidbodyCS_Update_mFA7EB03B37C1B2DC3C7ABA84488484149155828D ();
// 0x00000080 System.Void TestingRigidbodyCS::.ctor()
extern void TestingRigidbodyCS__ctor_mA1DCB32BECE6491D4E8B9F8E06979C6F2B775516 ();
// 0x00000081 System.Void Following::Start()
extern void Following_Start_mFEEA46D15CD9B280B5E7F418BAB4456C730EF556 ();
// 0x00000082 System.Void Following::Update()
extern void Following_Update_m0909311BA8C83F7F2448084BAFC093C62C012B76 ();
// 0x00000083 System.Void Following::moveArrow()
extern void Following_moveArrow_m6B308968FAA0842DE5E36D17DFF4CFADC87BDE64 ();
// 0x00000084 System.Void Following::.ctor()
extern void Following__ctor_m47A2E77D28030BA448B3705FBD95D3EF1C0EF5DE ();
// 0x00000085 System.Void GeneralAdvancedTechniques::Start()
extern void GeneralAdvancedTechniques_Start_mA1FF0FADF625671F926CC2E4D0CB77B979301564 ();
// 0x00000086 System.Void GeneralAdvancedTechniques::.ctor()
extern void GeneralAdvancedTechniques__ctor_m54D6A82A08BC2E749DA9363763E710312B79E899 ();
// 0x00000087 System.Void GeneralAdvancedTechniques::<Start>b__10_0(System.Single)
extern void GeneralAdvancedTechniques_U3CStartU3Eb__10_0_mBEBB18A1DC341C34F99BEF02138E30A577DC63C0 ();
// 0x00000088 System.Void GeneralBasic::Start()
extern void GeneralBasic_Start_m722ABC7DD46AD33613ED9A99550009B1388A2478 ();
// 0x00000089 System.Void GeneralBasic::advancedExamples()
extern void GeneralBasic_advancedExamples_mC9210C18842768B7D2E99936EC766E18D85C8773 ();
// 0x0000008A System.Void GeneralBasic::.ctor()
extern void GeneralBasic__ctor_mA0A6D418EBB951CF5EF97DCDE5E62CCC6D85D31F ();
// 0x0000008B System.Void GeneralBasic::<advancedExamples>b__2_0()
extern void GeneralBasic_U3CadvancedExamplesU3Eb__2_0_m6A1F674D4183516100E64513ACC50585EBA731E8 ();
// 0x0000008C System.Void GeneralBasics2d::Start()
extern void GeneralBasics2d_Start_mE3215316598A87E2CF0D9833F686D13FDC86DF2C ();
// 0x0000008D UnityEngine.GameObject GeneralBasics2d::createSpriteDude(System.String,UnityEngine.Vector3,System.Boolean)
extern void GeneralBasics2d_createSpriteDude_mBB652175A3D6AE7952BBE8C0E1226864D79928EA ();
// 0x0000008E System.Void GeneralBasics2d::advancedExamples()
extern void GeneralBasics2d_advancedExamples_mAC58CC5F094D9E634ACE4F04656C6A484C6D52C3 ();
// 0x0000008F System.Void GeneralBasics2d::.ctor()
extern void GeneralBasics2d__ctor_mFEB91E190B0738BC50845B33FAE252A2B41D55C7 ();
// 0x00000090 System.Void GeneralBasics2d::<advancedExamples>b__4_0()
extern void GeneralBasics2d_U3CadvancedExamplesU3Eb__4_0_m05957C9477E9A908C2EAF3BD0AD001EE3DC0D881 ();
// 0x00000091 System.Void GeneralCameraShake::Start()
extern void GeneralCameraShake_Start_mB1EE5CBBF521F4D697F8EAF2F53600826F54DABA ();
// 0x00000092 System.Void GeneralCameraShake::bigGuyJump()
extern void GeneralCameraShake_bigGuyJump_mE82096DC9C47AFAB293E2B8BAD0B7F2153063681 ();
// 0x00000093 System.Void GeneralCameraShake::.ctor()
extern void GeneralCameraShake__ctor_mA22AEAD53E34A074E7DBC77BC3164C64814270D8 ();
// 0x00000094 System.Void GeneralEasingTypes::Start()
extern void GeneralEasingTypes_Start_mBDA1C8799607E8C3D6052D0B88E5E96ED8892184 ();
// 0x00000095 System.Void GeneralEasingTypes::demoEaseTypes()
extern void GeneralEasingTypes_demoEaseTypes_mAD63F64427B735F85C75265777B1EA2C6038B422 ();
// 0x00000096 System.Void GeneralEasingTypes::resetLines()
extern void GeneralEasingTypes_resetLines_m46F8B9B5ADFB7348E099993918DB8EAA1108B746 ();
// 0x00000097 System.Void GeneralEasingTypes::.ctor()
extern void GeneralEasingTypes__ctor_mB2B0932931E070FA4BA5C24CFCFBBBF894E534BB ();
// 0x00000098 System.Void GeneralEventsListeners::Awake()
extern void GeneralEventsListeners_Awake_mBAFA4AE2A5056FBFED34D12A4776985220D66AF7 ();
// 0x00000099 System.Void GeneralEventsListeners::Start()
extern void GeneralEventsListeners_Start_m6B12B5EE52523BBA2008F17CA57A49B0ECCB619A ();
// 0x0000009A System.Void GeneralEventsListeners::jumpUp(LTEvent)
extern void GeneralEventsListeners_jumpUp_mC8BCDFB7169C9F7EBC42A5860DBB0BAF05612E70 ();
// 0x0000009B System.Void GeneralEventsListeners::changeColor(LTEvent)
extern void GeneralEventsListeners_changeColor_m6FBDF19D2457EA40303B510C9B9C4206943A8D09 ();
// 0x0000009C System.Void GeneralEventsListeners::OnCollisionEnter(UnityEngine.Collision)
extern void GeneralEventsListeners_OnCollisionEnter_m43F8FDFAC97EC09E53E98849F68469FCA7881F5F ();
// 0x0000009D System.Void GeneralEventsListeners::OnCollisionStay(UnityEngine.Collision)
extern void GeneralEventsListeners_OnCollisionStay_mC15D1654A132C57A85C90F0A9D1A0489481B20D2 ();
// 0x0000009E System.Void GeneralEventsListeners::FixedUpdate()
extern void GeneralEventsListeners_FixedUpdate_mF30FB01A225EFB50EB4714A0110DD6668F9715D0 ();
// 0x0000009F System.Void GeneralEventsListeners::OnMouseDown()
extern void GeneralEventsListeners_OnMouseDown_m7961A64655D9C0BC7401ACCD7A54741F5440C659 ();
// 0x000000A0 System.Void GeneralEventsListeners::.ctor()
extern void GeneralEventsListeners__ctor_mFE5DA13A6BE436986ADB09018AC3C641A1DD5A3A ();
// 0x000000A1 System.Void GeneralEventsListeners::<changeColor>b__8_0(UnityEngine.Color)
extern void GeneralEventsListeners_U3CchangeColorU3Eb__8_0_mF7603717CE44B39A97A3F80DC96F80E43A2DD33B ();
// 0x000000A2 System.Void GeneralSequencer::Start()
extern void GeneralSequencer_Start_mE22252603584E9595014C8CEAC0006FD9B55DBE7 ();
// 0x000000A3 System.Void GeneralSequencer::.ctor()
extern void GeneralSequencer__ctor_mCF1E29ADF0F70414A63D0F950D0FAB09AB1F0C8E ();
// 0x000000A4 System.Void GeneralSequencer::<Start>b__4_0()
extern void GeneralSequencer_U3CStartU3Eb__4_0_mCC4F15FF69FEEA0BFB73E8E2542C8910D2FAA220 ();
// 0x000000A5 System.Void GeneralSimpleUI::Start()
extern void GeneralSimpleUI_Start_mEEF7BDC999733A706FE7C03F54C8B4B83243C221 ();
// 0x000000A6 System.Void GeneralSimpleUI::.ctor()
extern void GeneralSimpleUI__ctor_m9A7A8639A7F2EE5FEAF39E2B6232FA533F58E1CC ();
// 0x000000A7 System.Void GeneralSimpleUI::<Start>b__1_0(UnityEngine.Vector2)
extern void GeneralSimpleUI_U3CStartU3Eb__1_0_mD28AC7A7223ECEB398951A2AAA561A4B9DC6FB66 ();
// 0x000000A8 System.Void GeneralSimpleUI::<Start>b__1_2(UnityEngine.Vector3)
extern void GeneralSimpleUI_U3CStartU3Eb__1_2_mA798C3D21C7EDE1BF5F133F6946478FDCE1B5CA8 ();
// 0x000000A9 System.Void GeneralSimpleUI::<Start>b__1_3(UnityEngine.Color)
extern void GeneralSimpleUI_U3CStartU3Eb__1_3_m759D56CF0CDCA8E27EDEB8BF968FD00AD9932824 ();
// 0x000000AA System.Void GeneralUISpace::Start()
extern void GeneralUISpace_Start_mA7F9726366918F04A038C11E04A5230F0C4AA41D ();
// 0x000000AB System.Void GeneralUISpace::.ctor()
extern void GeneralUISpace__ctor_mD9DD0C8F2377084F9DC2EA074C24E93DF1A3518B ();
// 0x000000AC System.Void LogoCinematic::Awake()
extern void LogoCinematic_Awake_m72CA9A35497E16D50121250F9BCF15F3BDF65138 ();
// 0x000000AD System.Void LogoCinematic::Start()
extern void LogoCinematic_Start_m5DB42CBB808A9C41D32B440B65FEE9F35E1FF86A ();
// 0x000000AE System.Void LogoCinematic::playBoom()
extern void LogoCinematic_playBoom_m1359C13981E4254A26839968D26F0F96E6033E4E ();
// 0x000000AF System.Void LogoCinematic::.ctor()
extern void LogoCinematic__ctor_m87A3D83E1047FD2818C5319C7462F2475468743E ();
// 0x000000B0 System.Void PathBezier2d::Start()
extern void PathBezier2d_Start_m6101865E419D574B55D90AFC87D59C61E2FE6A5A ();
// 0x000000B1 System.Void PathBezier2d::OnDrawGizmos()
extern void PathBezier2d_OnDrawGizmos_m5AF15DD02CC758B94C6988CA1084C7D5C03FC721 ();
// 0x000000B2 System.Void PathBezier2d::.ctor()
extern void PathBezier2d__ctor_m57F2349CACC84E24CFC8D242C81E3DE948CC357E ();
// 0x000000B3 System.Void ExampleSpline::Start()
extern void ExampleSpline_Start_mAD9D6848447333E450D9991F68BDD989B6AE6BFC ();
// 0x000000B4 System.Void ExampleSpline::Update()
extern void ExampleSpline_Update_m0C06D2C4C359A2AA1E1EEE9C4A7CE76B10A774F7 ();
// 0x000000B5 System.Void ExampleSpline::OnDrawGizmos()
extern void ExampleSpline_OnDrawGizmos_m1EC31ED5019C6CAAF409DE171F1D46631C4FEB4B ();
// 0x000000B6 System.Void ExampleSpline::.ctor()
extern void ExampleSpline__ctor_mA777C76A3F034C55804819D09FAD803C22B6945A ();
// 0x000000B7 System.Void PathSpline2d::Start()
extern void PathSpline2d_Start_m322D034A9E1AE896E48FD9C3A83D81E298B101F2 ();
// 0x000000B8 System.Void PathSpline2d::OnDrawGizmos()
extern void PathSpline2d_OnDrawGizmos_m45503986AD7ABD0B14F69C276092F5E99F3508E0 ();
// 0x000000B9 System.Void PathSpline2d::.ctor()
extern void PathSpline2d__ctor_m0C4B7813D0D578B5CAAAF5D2963ADF0E699583B8 ();
// 0x000000BA System.Void PathSplineEndless::Start()
extern void PathSplineEndless_Start_m0D63B5D9F0A5A8953829DBFF1AB80AD1699E5858 ();
// 0x000000BB System.Void PathSplineEndless::Update()
extern void PathSplineEndless_Update_m2168C1E36EB175C86685B1D6438244B9905B0D55 ();
// 0x000000BC UnityEngine.GameObject PathSplineEndless::objectQueue(UnityEngine.GameObject[],System.Int32&)
extern void PathSplineEndless_objectQueue_mDF89CDC0D51CA6B9A84F700BFD011EA5D53D0B3C ();
// 0x000000BD System.Void PathSplineEndless::addRandomTrackPoint()
extern void PathSplineEndless_addRandomTrackPoint_m2A6DF9D7DC969FC9E3E269C8F6815BDB1622552E ();
// 0x000000BE System.Void PathSplineEndless::refreshSpline()
extern void PathSplineEndless_refreshSpline_mE7C260A36EBDE07ED250D752CC2AE13F9640BF84 ();
// 0x000000BF System.Void PathSplineEndless::playSwish()
extern void PathSplineEndless_playSwish_m8DF90B3209DEC3201AD432B013B8CE3D665E70F3 ();
// 0x000000C0 System.Void PathSplineEndless::.ctor()
extern void PathSplineEndless__ctor_m936283AAD1B2ACB8A975A6C5F44E5C49F235D889 ();
// 0x000000C1 System.Void PathSplineEndless::<Start>b__17_0(System.Single)
extern void PathSplineEndless_U3CStartU3Eb__17_0_m6457B9A5A1966F3A5EB50724D4F867D6AB05BDD5 ();
// 0x000000C2 System.Void PathSplinePerformance::Start()
extern void PathSplinePerformance_Start_m1DA9D6816079EA25341277C76A317D94B4168EF3 ();
// 0x000000C3 System.Void PathSplinePerformance::Update()
extern void PathSplinePerformance_Update_m440225D51242DD97344316E466389BB633099B3F ();
// 0x000000C4 System.Void PathSplinePerformance::OnDrawGizmos()
extern void PathSplinePerformance_OnDrawGizmos_m963F417D81A92D1D1260EB172050FEB2C95B9E06 ();
// 0x000000C5 System.Void PathSplinePerformance::playSwish()
extern void PathSplinePerformance_playSwish_mCDD5986D88F473C4C789EBB08013D5C4371B8934 ();
// 0x000000C6 System.Void PathSplinePerformance::.ctor()
extern void PathSplinePerformance__ctor_mF91B2042C6950B108ACFC3CDB4FA0F2F6545377E ();
// 0x000000C7 System.Void PathSplineTrack::Start()
extern void PathSplineTrack_Start_m281BC1BE6C44BADFA80DA3D5C0679D34FEBADE1B ();
// 0x000000C8 System.Void PathSplineTrack::Update()
extern void PathSplineTrack_Update_m789CE9A3C91D635B04AE45C77ADC552CEEB0BE3A ();
// 0x000000C9 System.Void PathSplineTrack::OnDrawGizmos()
extern void PathSplineTrack_OnDrawGizmos_mF8A6C592C8E177043453FAFA09F3917D133E82FC ();
// 0x000000CA System.Void PathSplineTrack::playSwish()
extern void PathSplineTrack_playSwish_m166E8F61FFC8D4DE30A55839EE95413669489D99 ();
// 0x000000CB System.Void PathSplineTrack::.ctor()
extern void PathSplineTrack__ctor_m3D4ECADCEDAC59010FC4B6576F49BCC3083E8DBA ();
// 0x000000CC System.Void PathSplines::OnEnable()
extern void PathSplines_OnEnable_m9A31A2680F7A3A5A786B007758F6B68DEFD47CBF ();
// 0x000000CD System.Void PathSplines::Start()
extern void PathSplines_Start_mBB7ABDB218B114C164C2E61B9BD469CB8D5E76D6 ();
// 0x000000CE System.Void PathSplines::Update()
extern void PathSplines_Update_mFAFAB73B81EA5981B4EEC5C507B72A52D49E593C ();
// 0x000000CF System.Void PathSplines::OnDrawGizmos()
extern void PathSplines_OnDrawGizmos_mD3B3EE01513F4BBF82A00F87DBA40C283384C5EB ();
// 0x000000D0 System.Void PathSplines::.ctor()
extern void PathSplines__ctor_mFFF59F5DECB94D08ECA1765F4AF70F15F1D0F807 ();
// 0x000000D1 System.Void PathSplines::<Start>b__4_0()
extern void PathSplines_U3CStartU3Eb__4_0_mCDFF93FF0B96E66451EDB0D037E46B7F6A0B0D04 ();
// 0x000000D2 System.Void TestingZLegacy::Awake()
extern void TestingZLegacy_Awake_mBCE69D28EF4B5C8DD9623C93150A62690FF6F560 ();
// 0x000000D3 System.Void TestingZLegacy::Start()
extern void TestingZLegacy_Start_mF7B3C1D0359654FACF1A636ED276DA2D7AC2F238 ();
// 0x000000D4 System.Void TestingZLegacy::pauseNow()
extern void TestingZLegacy_pauseNow_m655EADF071BB3053B444C84C0BE35675AD971405 ();
// 0x000000D5 System.Void TestingZLegacy::OnGUI()
extern void TestingZLegacy_OnGUI_m8736E68E13F34D38E820CBDF166DB67989878F22 ();
// 0x000000D6 System.Void TestingZLegacy::endlessCallback()
extern void TestingZLegacy_endlessCallback_m9AEBB3556FEF851B1F6DB08DB8050E8D89D45D17 ();
// 0x000000D7 System.Void TestingZLegacy::cycleThroughExamples()
extern void TestingZLegacy_cycleThroughExamples_mC9AF8FB1FED800DF92B00E6C482380B1ED2EF4A3 ();
// 0x000000D8 System.Void TestingZLegacy::updateValue3Example()
extern void TestingZLegacy_updateValue3Example_mCA94016385AF00299FD43C7778501DB3F51E8786 ();
// 0x000000D9 System.Void TestingZLegacy::updateValue3ExampleUpdate(UnityEngine.Vector3)
extern void TestingZLegacy_updateValue3ExampleUpdate_mC0566B31227122FC6140F7704C48065E4AC046C3 ();
// 0x000000DA System.Void TestingZLegacy::updateValue3ExampleCallback(UnityEngine.Vector3)
extern void TestingZLegacy_updateValue3ExampleCallback_mEA31AFEE5C74647804BDE02889A38307D8096B4B ();
// 0x000000DB System.Void TestingZLegacy::loopTestClamp()
extern void TestingZLegacy_loopTestClamp_mEC3D64101ACA122E2257ED726DA36D5E7E2EE6DD ();
// 0x000000DC System.Void TestingZLegacy::loopTestPingPong()
extern void TestingZLegacy_loopTestPingPong_mC9FC3BFBB6EB66050EB24EA3623777201C131F91 ();
// 0x000000DD System.Void TestingZLegacy::colorExample()
extern void TestingZLegacy_colorExample_mDCABB46692619BF22DD41425D03EA835F6024AC2 ();
// 0x000000DE System.Void TestingZLegacy::moveOnACurveExample()
extern void TestingZLegacy_moveOnACurveExample_m643B475C72457BFC0580809D25206A474786A1AA ();
// 0x000000DF System.Void TestingZLegacy::customTweenExample()
extern void TestingZLegacy_customTweenExample_mE863F3298241D782EBCB333A93B882C572C8F9E6 ();
// 0x000000E0 System.Void TestingZLegacy::moveExample()
extern void TestingZLegacy_moveExample_m8CB97B6FAD6C66EC740EB31D539F78BC5930F02A ();
// 0x000000E1 System.Void TestingZLegacy::rotateExample()
extern void TestingZLegacy_rotateExample_m09C24A6D46978C652A6D8F321F9A071247462DD3 ();
// 0x000000E2 System.Void TestingZLegacy::rotateOnUpdate(System.Single)
extern void TestingZLegacy_rotateOnUpdate_mDB551FEC7A3BC50E5BFCB3A4D86EDBD71343FE97 ();
// 0x000000E3 System.Void TestingZLegacy::rotateFinished(System.Object)
extern void TestingZLegacy_rotateFinished_mEE1B541D31B1E9388A575AEF8FECC8D8FDD3605F ();
// 0x000000E4 System.Void TestingZLegacy::scaleExample()
extern void TestingZLegacy_scaleExample_m8683640627D1DF303D1A55A59C476198A6D5D7D8 ();
// 0x000000E5 System.Void TestingZLegacy::updateValueExample()
extern void TestingZLegacy_updateValueExample_mE0A9F76605CEA59FA59357CE4C5313A32801AAF4 ();
// 0x000000E6 System.Void TestingZLegacy::updateValueExampleCallback(System.Single,System.Object)
extern void TestingZLegacy_updateValueExampleCallback_m4CB6A988D0383234B578C9465B40085A962D92EC ();
// 0x000000E7 System.Void TestingZLegacy::delayedCallExample()
extern void TestingZLegacy_delayedCallExample_mB11B32B256519D1ECFBB54216CEB46E7719BE5E9 ();
// 0x000000E8 System.Void TestingZLegacy::delayedCallExampleCallback()
extern void TestingZLegacy_delayedCallExampleCallback_mE8E45FAD4B3C8A46BA856C0250AF6A4F6606F20C ();
// 0x000000E9 System.Void TestingZLegacy::alphaExample()
extern void TestingZLegacy_alphaExample_m7DEE863B73235D62D7FACC183D9B69A9C22C68F8 ();
// 0x000000EA System.Void TestingZLegacy::moveLocalExample()
extern void TestingZLegacy_moveLocalExample_mD7173FDBB36229E28B108F6AD5195F253326F3B2 ();
// 0x000000EB System.Void TestingZLegacy::rotateAroundExample()
extern void TestingZLegacy_rotateAroundExample_m2D08EC29B26084F4B7D22AAD9D8A4453FFB9659B ();
// 0x000000EC System.Void TestingZLegacy::loopPause()
extern void TestingZLegacy_loopPause_m55A880DF0CB7F8737EAB6CB0202121B258759EE7 ();
// 0x000000ED System.Void TestingZLegacy::loopResume()
extern void TestingZLegacy_loopResume_mE7636F1996095F5B17B155C5BA0350DDEF222F8D ();
// 0x000000EE System.Void TestingZLegacy::punchTest()
extern void TestingZLegacy_punchTest_mE2E20734BDEE2A30E5F643FDF84FE56BD99ACB49 ();
// 0x000000EF System.Void TestingZLegacy::.ctor()
extern void TestingZLegacy__ctor_m54DC8DE5EF69E156FDDF1C34916397E68B0B9F2E ();
// 0x000000F0 System.Void TestingZLegacyExt::Awake()
extern void TestingZLegacyExt_Awake_m97F85D3DD812AE6BFA4E59E5BDB074A11DEFB74D ();
// 0x000000F1 System.Void TestingZLegacyExt::Start()
extern void TestingZLegacyExt_Start_m3FC22E42B4D1C512B8F76260CF90F7584CDE78C9 ();
// 0x000000F2 System.Void TestingZLegacyExt::pauseNow()
extern void TestingZLegacyExt_pauseNow_m6FA2ACC29A279AFFEAC9C1117C615E692109E47E ();
// 0x000000F3 System.Void TestingZLegacyExt::OnGUI()
extern void TestingZLegacyExt_OnGUI_mE335B55E2688BCF03B84575D64CE6905E9EB390C ();
// 0x000000F4 System.Void TestingZLegacyExt::endlessCallback()
extern void TestingZLegacyExt_endlessCallback_mC91F80F402003198552BB5A47773AE91C1CDF3E4 ();
// 0x000000F5 System.Void TestingZLegacyExt::cycleThroughExamples()
extern void TestingZLegacyExt_cycleThroughExamples_m7ADA1FA87F2373C6CA2F710EE09E07C6E6B48DA7 ();
// 0x000000F6 System.Void TestingZLegacyExt::updateValue3Example()
extern void TestingZLegacyExt_updateValue3Example_m56A718E3C7178DB0C59E3E030EF2007D60168950 ();
// 0x000000F7 System.Void TestingZLegacyExt::updateValue3ExampleUpdate(UnityEngine.Vector3)
extern void TestingZLegacyExt_updateValue3ExampleUpdate_mBA7E2C03C01B5CDA6C4DF5EE4E5521FF72644235 ();
// 0x000000F8 System.Void TestingZLegacyExt::updateValue3ExampleCallback(UnityEngine.Vector3)
extern void TestingZLegacyExt_updateValue3ExampleCallback_m83AD25E3BEB33C7F57C68FCD9C1EA9E27F762651 ();
// 0x000000F9 System.Void TestingZLegacyExt::loopTestClamp()
extern void TestingZLegacyExt_loopTestClamp_m402BD2B45CDF06FA5F47E43EE139E3D593D3837E ();
// 0x000000FA System.Void TestingZLegacyExt::loopTestPingPong()
extern void TestingZLegacyExt_loopTestPingPong_m88A111C6F9F4C97A89F6E141862BD22F1723F1A8 ();
// 0x000000FB System.Void TestingZLegacyExt::colorExample()
extern void TestingZLegacyExt_colorExample_mD487DAAD856F9B4E044869A56A9E83F0E5CE686D ();
// 0x000000FC System.Void TestingZLegacyExt::moveOnACurveExample()
extern void TestingZLegacyExt_moveOnACurveExample_m76FBF8744FDD1EDFA144DF0B05F91F341DDE7C86 ();
// 0x000000FD System.Void TestingZLegacyExt::customTweenExample()
extern void TestingZLegacyExt_customTweenExample_m7FD53EA42C543F5CDE51BE17B7B1714B078A3671 ();
// 0x000000FE System.Void TestingZLegacyExt::moveExample()
extern void TestingZLegacyExt_moveExample_m877A1E1840D36A34C7655F995471BA952B64FC2D ();
// 0x000000FF System.Void TestingZLegacyExt::rotateExample()
extern void TestingZLegacyExt_rotateExample_mB882491A5D47DE36B743560863B366C88CF69A2D ();
// 0x00000100 System.Void TestingZLegacyExt::rotateOnUpdate(System.Single)
extern void TestingZLegacyExt_rotateOnUpdate_m60EEE824C1EBA3FA12C60B3B23B6144739697A25 ();
// 0x00000101 System.Void TestingZLegacyExt::rotateFinished(System.Object)
extern void TestingZLegacyExt_rotateFinished_m6885AB18A26592B3844455D756D70BDC20F257E8 ();
// 0x00000102 System.Void TestingZLegacyExt::scaleExample()
extern void TestingZLegacyExt_scaleExample_m3D54AA1EEFBA37C30889E34AFBB350316CEAB41B ();
// 0x00000103 System.Void TestingZLegacyExt::updateValueExample()
extern void TestingZLegacyExt_updateValueExample_m0A08F0B235AD41CC0C4153B431862904C81799C6 ();
// 0x00000104 System.Void TestingZLegacyExt::updateValueExampleCallback(System.Single,System.Object)
extern void TestingZLegacyExt_updateValueExampleCallback_mD07695C332FF263FCFF89F4B305C96A02A786729 ();
// 0x00000105 System.Void TestingZLegacyExt::delayedCallExample()
extern void TestingZLegacyExt_delayedCallExample_m530FFA75198C880599CE65E105D061B133E172F9 ();
// 0x00000106 System.Void TestingZLegacyExt::delayedCallExampleCallback()
extern void TestingZLegacyExt_delayedCallExampleCallback_m0F8E4A9EC78F99065D1B519DA641FEB9B4A686A5 ();
// 0x00000107 System.Void TestingZLegacyExt::alphaExample()
extern void TestingZLegacyExt_alphaExample_m6FEFC52F975614F51524FB7B91547C791FC1A982 ();
// 0x00000108 System.Void TestingZLegacyExt::moveLocalExample()
extern void TestingZLegacyExt_moveLocalExample_mD509C90ADEA3546144652A770965A5C98F88F3C2 ();
// 0x00000109 System.Void TestingZLegacyExt::rotateAroundExample()
extern void TestingZLegacyExt_rotateAroundExample_m4507C9CD41E678846AE918C6D796A4BFC43EF056 ();
// 0x0000010A System.Void TestingZLegacyExt::loopPause()
extern void TestingZLegacyExt_loopPause_mC23D0D95589F74EA510A9E79D35F28C7418F514F ();
// 0x0000010B System.Void TestingZLegacyExt::loopResume()
extern void TestingZLegacyExt_loopResume_mA3FBE26AE1243FE2178C55BEDB45AC6763C42FF9 ();
// 0x0000010C System.Void TestingZLegacyExt::punchTest()
extern void TestingZLegacyExt_punchTest_mBC5D8A5FFF359CFAFF16BFD89387B56B571DA001 ();
// 0x0000010D System.Void TestingZLegacyExt::.ctor()
extern void TestingZLegacyExt__ctor_m918A02678AF6D9E398E17E98FC792A73E8205542 ();
// 0x0000010E UnityEngine.Vector3 LTDescr::get_from()
extern void LTDescr_get_from_m8E7FF7FB5FE65C313FE2CD26C8B1BBAC00BFBDC2 ();
// 0x0000010F System.Void LTDescr::set_from(UnityEngine.Vector3)
extern void LTDescr_set_from_m0C7AA03B3869C1A3B6EEB8ECD16FF0311A19B5FD ();
// 0x00000110 UnityEngine.Vector3 LTDescr::get_to()
extern void LTDescr_get_to_m842DE271937A3E21367D57D4C1A963E98E0AF5E9 ();
// 0x00000111 System.Void LTDescr::set_to(UnityEngine.Vector3)
extern void LTDescr_set_to_m720DB590EE8641E7091375EE626ED38CCA36C63D ();
// 0x00000112 LTDescr_ActionMethodDelegate LTDescr::get_easeInternal()
extern void LTDescr_get_easeInternal_mEDF89A6F240F4770536F1FBC0FBB79A6A55C7BBD ();
// 0x00000113 System.Void LTDescr::set_easeInternal(LTDescr_ActionMethodDelegate)
extern void LTDescr_set_easeInternal_m16C7B8635E5D2FB3E31EBE721E954967EFBF30B7 ();
// 0x00000114 LTDescr_ActionMethodDelegate LTDescr::get_initInternal()
extern void LTDescr_get_initInternal_mE336562DA52E756130ADBB07B3CEF2D7E7EB01D4 ();
// 0x00000115 System.Void LTDescr::set_initInternal(LTDescr_ActionMethodDelegate)
extern void LTDescr_set_initInternal_m0753D24B8455857620589DBBE1EF095A2AFE489B ();
// 0x00000116 UnityEngine.Transform LTDescr::get_toTrans()
extern void LTDescr_get_toTrans_m2B06D495FA4BC4F44D1EAD32F163D62BCC457B5F ();
// 0x00000117 System.String LTDescr::ToString()
extern void LTDescr_ToString_mA312106BDDBB7E356B88C05BF8C767C63964CCC6 ();
// 0x00000118 System.Void LTDescr::.ctor()
extern void LTDescr__ctor_m975E0C19B34E8078C70AFAE95A581777B8D6F4D8 ();
// 0x00000119 LTDescr LTDescr::cancel(UnityEngine.GameObject)
extern void LTDescr_cancel_m944E66783EA1A1BD7781AFE7C7EF7A6117B56930 ();
// 0x0000011A System.Int32 LTDescr::get_uniqueId()
extern void LTDescr_get_uniqueId_m11CA85564A0B0C9BE7FC551ECA3A05111888C7B2 ();
// 0x0000011B System.Int32 LTDescr::get_id()
extern void LTDescr_get_id_m76232B8A21BCE44149F9014942BD1C63F5C6B4E7 ();
// 0x0000011C LTDescrOptional LTDescr::get_optional()
extern void LTDescr_get_optional_mEA6CD7BE31135EB1F2310490F9FAC346767B3020 ();
// 0x0000011D System.Void LTDescr::set_optional(LTDescrOptional)
extern void LTDescr_set_optional_mF78587FE46E7E26D9742F3F156DC92DA51AEA2EE ();
// 0x0000011E System.Void LTDescr::reset()
extern void LTDescr_reset_m16BED571328E24D5385BDA9E4E478508EEB74664 ();
// 0x0000011F LTDescr LTDescr::setFollow()
extern void LTDescr_setFollow_mD35310187FD79F81851CD3635C0376BAAC9EF9FB ();
// 0x00000120 LTDescr LTDescr::setMoveX()
extern void LTDescr_setMoveX_m3B1BB06BE81F8DF47CE145148ADE5D2E63B7D630 ();
// 0x00000121 LTDescr LTDescr::setMoveY()
extern void LTDescr_setMoveY_m62E79AA722E091918379FDA5255DB3F517D447F3 ();
// 0x00000122 LTDescr LTDescr::setMoveZ()
extern void LTDescr_setMoveZ_m5846FCA035E0BC391028178FEAEF74B0C31C998B ();
// 0x00000123 LTDescr LTDescr::setMoveLocalX()
extern void LTDescr_setMoveLocalX_m1DAB1F3CFCF599184B907273B7EEDB58C319177D ();
// 0x00000124 LTDescr LTDescr::setMoveLocalY()
extern void LTDescr_setMoveLocalY_m3EFB302B1C5A842539CCC7D972836D4AA5BEF9C1 ();
// 0x00000125 LTDescr LTDescr::setMoveLocalZ()
extern void LTDescr_setMoveLocalZ_mBCD523EEE6FDE11FBDF9B291FA827EFCAADC055F ();
// 0x00000126 System.Void LTDescr::initFromInternal()
extern void LTDescr_initFromInternal_mA4604276862B5C16B4D0A1F323C5031D37CBE125 ();
// 0x00000127 LTDescr LTDescr::setOffset(UnityEngine.Vector3)
extern void LTDescr_setOffset_m29ED0A18B591990BBCC94E36A22426FEFF0B6F5F ();
// 0x00000128 LTDescr LTDescr::setMoveCurved()
extern void LTDescr_setMoveCurved_mD2FC79FD2FBC09320F29D253187B0FD5E1E3F242 ();
// 0x00000129 LTDescr LTDescr::setMoveCurvedLocal()
extern void LTDescr_setMoveCurvedLocal_m5B19FE68C93FBE105E3D02DA4AABB28183218DA4 ();
// 0x0000012A LTDescr LTDescr::setMoveSpline()
extern void LTDescr_setMoveSpline_m483C9060295F816DDB9F363B2B7D3C7EB2BC709A ();
// 0x0000012B LTDescr LTDescr::setMoveSplineLocal()
extern void LTDescr_setMoveSplineLocal_mE23B6AEEBD002D6D811758CB415DBB8559505DB2 ();
// 0x0000012C LTDescr LTDescr::setScaleX()
extern void LTDescr_setScaleX_mC50D44B615017A9DEC47B21746DA49E6907C2106 ();
// 0x0000012D LTDescr LTDescr::setScaleY()
extern void LTDescr_setScaleY_m3980093C790F98A85A697A6CB76A08CE258A1831 ();
// 0x0000012E LTDescr LTDescr::setScaleZ()
extern void LTDescr_setScaleZ_m1E3D4F892EFB3F884EB006D6314CEAAE2E86B788 ();
// 0x0000012F LTDescr LTDescr::setRotateX()
extern void LTDescr_setRotateX_m9710079249D050C4709F38FF212A8C5A064141A6 ();
// 0x00000130 LTDescr LTDescr::setRotateY()
extern void LTDescr_setRotateY_m0A6206F1BAC1EB1E74373C866BB993A17AC474C7 ();
// 0x00000131 LTDescr LTDescr::setRotateZ()
extern void LTDescr_setRotateZ_m88EB19296F3A5A255B0DDAD63F5556854A3AA6D5 ();
// 0x00000132 LTDescr LTDescr::setRotateAround()
extern void LTDescr_setRotateAround_m5C916E21674683F454CF66A50F1B52688EC5A72F ();
// 0x00000133 LTDescr LTDescr::setRotateAroundLocal()
extern void LTDescr_setRotateAroundLocal_m26621B8F8199CF50CFD97CF32E6DF271543CE142 ();
// 0x00000134 LTDescr LTDescr::setAlpha()
extern void LTDescr_setAlpha_mA82AF444875CEBDC11AF76A6DA33F8832070A7C9 ();
// 0x00000135 LTDescr LTDescr::setTextAlpha()
extern void LTDescr_setTextAlpha_m140059336BABDBE97F7B9EB26E7B07A6A2FECCCF ();
// 0x00000136 LTDescr LTDescr::setAlphaVertex()
extern void LTDescr_setAlphaVertex_m4BE3949864A804651A8E884317090AC0BE9BA543 ();
// 0x00000137 LTDescr LTDescr::setColor()
extern void LTDescr_setColor_m677D553334BCD65A976646538D2E3FB901FEB2F8 ();
// 0x00000138 LTDescr LTDescr::setCallbackColor()
extern void LTDescr_setCallbackColor_m8ED344DF8DA4A35A9323090A9705F338CCEFA6BF ();
// 0x00000139 LTDescr LTDescr::setTextColor()
extern void LTDescr_setTextColor_mB0E1342E1C5604929D621039E74EB5C968E63CA5 ();
// 0x0000013A LTDescr LTDescr::setCanvasAlpha()
extern void LTDescr_setCanvasAlpha_m883942EF674A4FCF4C0621D8BB36F1EB78D9465E ();
// 0x0000013B LTDescr LTDescr::setCanvasGroupAlpha()
extern void LTDescr_setCanvasGroupAlpha_mA08B850710279DF7259A9EA457614BABD29EF28C ();
// 0x0000013C LTDescr LTDescr::setCanvasColor()
extern void LTDescr_setCanvasColor_m8D0A659A765604171C1ECE12ACB617319E179015 ();
// 0x0000013D LTDescr LTDescr::setCanvasMoveX()
extern void LTDescr_setCanvasMoveX_m012C7901BCEC98126783ACDB01B088297D30EB69 ();
// 0x0000013E LTDescr LTDescr::setCanvasMoveY()
extern void LTDescr_setCanvasMoveY_m8124C7761145134D07245BB3031857695501B9EB ();
// 0x0000013F LTDescr LTDescr::setCanvasMoveZ()
extern void LTDescr_setCanvasMoveZ_mFDFC0D71E22EE1097FDA8A2A6F0A9990C5403D6C ();
// 0x00000140 System.Void LTDescr::initCanvasRotateAround()
extern void LTDescr_initCanvasRotateAround_m1205028161B594D223E69013F8C92B766CE75959 ();
// 0x00000141 LTDescr LTDescr::setCanvasRotateAround()
extern void LTDescr_setCanvasRotateAround_m7E04711CE56F7BB9D43E27A0102E0C3C7FDCDDF9 ();
// 0x00000142 LTDescr LTDescr::setCanvasRotateAroundLocal()
extern void LTDescr_setCanvasRotateAroundLocal_m7BA851F76908BF978A682D4E49CD2DB770F27F55 ();
// 0x00000143 LTDescr LTDescr::setCanvasPlaySprite()
extern void LTDescr_setCanvasPlaySprite_m3CDD391E0DC1AB5B60FC4A01F1527184062E0202 ();
// 0x00000144 LTDescr LTDescr::setCanvasMove()
extern void LTDescr_setCanvasMove_m84559A096BC7A140C8CA25DE9DB766D537C9A148 ();
// 0x00000145 LTDescr LTDescr::setCanvasScale()
extern void LTDescr_setCanvasScale_mF06739A6DC21077E3EB7704B3997CB495302F134 ();
// 0x00000146 LTDescr LTDescr::setCanvasSizeDelta()
extern void LTDescr_setCanvasSizeDelta_m6C41930BF6F585DCDDF31EB4B540F3C3163DF0E3 ();
// 0x00000147 System.Void LTDescr::callback()
extern void LTDescr_callback_m9B45B1DCD83B9880A61C16C6C21F0D05C7C6AFA7 ();
// 0x00000148 LTDescr LTDescr::setCallback()
extern void LTDescr_setCallback_m7BD2A7CFAC15C8D9D992E0521B819B41FD9A6CB3 ();
// 0x00000149 LTDescr LTDescr::setValue3()
extern void LTDescr_setValue3_m553FBFB157A9765053F9FB7CF08B59A1E3D976D7 ();
// 0x0000014A LTDescr LTDescr::setMove()
extern void LTDescr_setMove_mDEC044497DD6398395A17A83EDC32ABEB3F8DA88 ();
// 0x0000014B LTDescr LTDescr::setMoveLocal()
extern void LTDescr_setMoveLocal_m4B75F54B72038AF2971A73A8A6E905DE94AA6346 ();
// 0x0000014C LTDescr LTDescr::setMoveToTransform()
extern void LTDescr_setMoveToTransform_m9C07DB063E64D2F9A91054DFA08B1C2594D22F88 ();
// 0x0000014D LTDescr LTDescr::setRotate()
extern void LTDescr_setRotate_m37FB3B6088196BB2A0DD48105125989468400A5B ();
// 0x0000014E LTDescr LTDescr::setRotateLocal()
extern void LTDescr_setRotateLocal_m94A88F95DB9D3BD078305AB3A25039966CF4147C ();
// 0x0000014F LTDescr LTDescr::setScale()
extern void LTDescr_setScale_m003B5242EE6667C4F5AC57C5C06A9A32007F0FBD ();
// 0x00000150 LTDescr LTDescr::setGUIMove()
extern void LTDescr_setGUIMove_mC25E34C2B27930D956DB92F12FF79F25F7D78E9E ();
// 0x00000151 LTDescr LTDescr::setGUIMoveMargin()
extern void LTDescr_setGUIMoveMargin_m96C670A2423CC1E4A2D02021CF0F5CFA51A8E14B ();
// 0x00000152 LTDescr LTDescr::setGUIScale()
extern void LTDescr_setGUIScale_mE8BA06B69FFA9FF14F0C7CD3FB76AE2F12283B0D ();
// 0x00000153 LTDescr LTDescr::setGUIAlpha()
extern void LTDescr_setGUIAlpha_m964F89CFFC7D20A97EE78E6340C9F8236AC2DE6A ();
// 0x00000154 LTDescr LTDescr::setGUIRotate()
extern void LTDescr_setGUIRotate_m9B46A83C43BAFD1DB0C5629D850F1D3424B3AA7B ();
// 0x00000155 LTDescr LTDescr::setDelayedSound()
extern void LTDescr_setDelayedSound_m6223261128689E6768A694E089DE42B67A2BC026 ();
// 0x00000156 LTDescr LTDescr::setTarget(UnityEngine.Transform)
extern void LTDescr_setTarget_m96F2398CBF82B65C2BD373807C75C0E04A39DD29 ();
// 0x00000157 System.Void LTDescr::init()
extern void LTDescr_init_mB964AC9BD3406D19362D8675474F4DC99B33347B ();
// 0x00000158 System.Void LTDescr::initSpeed()
extern void LTDescr_initSpeed_m97D3A943AA4B2F25ABC91FC5C5C055611799AE16 ();
// 0x00000159 LTDescr LTDescr::updateNow()
extern void LTDescr_updateNow_mB7468B5F37D19C21184A2A885C1DF0A55E1F1109 ();
// 0x0000015A System.Boolean LTDescr::updateInternal()
extern void LTDescr_updateInternal_m2295EAEC09FDEF9AD71EB618734DE0DF5570D2A5 ();
// 0x0000015B System.Void LTDescr::callOnCompletes()
extern void LTDescr_callOnCompletes_mE047FE764250E4071E38197BC1B29221B87B8B4B ();
// 0x0000015C LTDescr LTDescr::setFromColor(UnityEngine.Color)
extern void LTDescr_setFromColor_m84DCC218D18266CA0D5167AE546CF54EBF2D38B0 ();
// 0x0000015D System.Void LTDescr::alphaRecursive(UnityEngine.Transform,System.Single,System.Boolean)
extern void LTDescr_alphaRecursive_mA15152C74BCA8D8CC498BF7A84A0481A35AA29EC ();
// 0x0000015E System.Void LTDescr::colorRecursive(UnityEngine.Transform,UnityEngine.Color,System.Boolean)
extern void LTDescr_colorRecursive_mA38B1CB2A51FECCCF49F1D1F50D98312A2296E0E ();
// 0x0000015F System.Void LTDescr::alphaRecursive(UnityEngine.RectTransform,System.Single,System.Int32)
extern void LTDescr_alphaRecursive_m6BAD882CBD0900B2ED3ACD96B49E1AE50DEA791D ();
// 0x00000160 System.Void LTDescr::alphaRecursiveSprite(UnityEngine.Transform,System.Single)
extern void LTDescr_alphaRecursiveSprite_m64AA415027FB1434EFB1E006721B206F47CB1ECA ();
// 0x00000161 System.Void LTDescr::colorRecursiveSprite(UnityEngine.Transform,UnityEngine.Color)
extern void LTDescr_colorRecursiveSprite_mFE805A4F826EC3CFF84CB6E86E43E857B934897D ();
// 0x00000162 System.Void LTDescr::colorRecursive(UnityEngine.RectTransform,UnityEngine.Color)
extern void LTDescr_colorRecursive_mE714E3CB2CB79B4F6C40A0C0A5CF648A5C1E3F94 ();
// 0x00000163 System.Void LTDescr::textAlphaChildrenRecursive(UnityEngine.Transform,System.Single,System.Boolean)
extern void LTDescr_textAlphaChildrenRecursive_m99FA1EC22F4CF7A7743038CF8369298B1F3ADAF2 ();
// 0x00000164 System.Void LTDescr::textAlphaRecursive(UnityEngine.Transform,System.Single,System.Boolean)
extern void LTDescr_textAlphaRecursive_m8EC23765F7904CBA0C35734A7D63F58C879C577C ();
// 0x00000165 System.Void LTDescr::textColorRecursive(UnityEngine.Transform,UnityEngine.Color)
extern void LTDescr_textColorRecursive_mE39D07578A5FCAEE8336AE9D1DE9E841B98FD90C ();
// 0x00000166 UnityEngine.Color LTDescr::tweenColor(LTDescr,System.Single)
extern void LTDescr_tweenColor_mD7F8129343D2677EC17D5740E160A88540685140 ();
// 0x00000167 LTDescr LTDescr::pause()
extern void LTDescr_pause_m516CADEFB418399C0A796883DB35D183F8FBD9D3 ();
// 0x00000168 LTDescr LTDescr::resume()
extern void LTDescr_resume_m1D572322CCB20396E715EED46A1692A6D4F81E80 ();
// 0x00000169 LTDescr LTDescr::setAxis(UnityEngine.Vector3)
extern void LTDescr_setAxis_m6785FFA919508809C7A0A5FB7A998E46E1CB22D1 ();
// 0x0000016A LTDescr LTDescr::setDelay(System.Single)
extern void LTDescr_setDelay_m78E34545BB3EEA43ABF9CD5D29C8FB1CC3D4F464 ();
// 0x0000016B LTDescr LTDescr::setEase(LeanTweenType)
extern void LTDescr_setEase_mCF284E5CA17BB1BECBB5B1CC1013571B108580A3 ();
// 0x0000016C LTDescr LTDescr::setEaseLinear()
extern void LTDescr_setEaseLinear_mB1944DC5601584DF8F68F2FD1E844526B4B57D0A ();
// 0x0000016D LTDescr LTDescr::setEaseSpring()
extern void LTDescr_setEaseSpring_mCD7F60BDFCE5BFF617264B8775C4E80ABED4A465 ();
// 0x0000016E LTDescr LTDescr::setEaseInQuad()
extern void LTDescr_setEaseInQuad_mC03167FBB2CD3CB171D87440DC20604F51248C6E ();
// 0x0000016F LTDescr LTDescr::setEaseOutQuad()
extern void LTDescr_setEaseOutQuad_mC87008E2DA94BFA521BB12A74F4FC0F160ED1291 ();
// 0x00000170 LTDescr LTDescr::setEaseInOutQuad()
extern void LTDescr_setEaseInOutQuad_m22C85D4E11EA5B77869864113758310F097CFDDD ();
// 0x00000171 LTDescr LTDescr::setEaseInCubic()
extern void LTDescr_setEaseInCubic_m0D93D024F3F116E6A7887F86F0F5203D74FAF5FF ();
// 0x00000172 LTDescr LTDescr::setEaseOutCubic()
extern void LTDescr_setEaseOutCubic_mEEAA2F960FB30E6B84B3D52BEB173E406A0A8A6F ();
// 0x00000173 LTDescr LTDescr::setEaseInOutCubic()
extern void LTDescr_setEaseInOutCubic_mACAC52E920C0602EC0D90E6019B3EAE9034D25F9 ();
// 0x00000174 LTDescr LTDescr::setEaseInQuart()
extern void LTDescr_setEaseInQuart_mEFE37EB2D9BB775BAC45A1D55CABB0D41FA679F0 ();
// 0x00000175 LTDescr LTDescr::setEaseOutQuart()
extern void LTDescr_setEaseOutQuart_mEAD18F5546A6BE77FE48294B5BB316769338A461 ();
// 0x00000176 LTDescr LTDescr::setEaseInOutQuart()
extern void LTDescr_setEaseInOutQuart_mAFC9F8155E7348004D99107EE10C10AD51C526B4 ();
// 0x00000177 LTDescr LTDescr::setEaseInQuint()
extern void LTDescr_setEaseInQuint_m8C116A004899207382401BCADC6F39F4EE3B75B7 ();
// 0x00000178 LTDescr LTDescr::setEaseOutQuint()
extern void LTDescr_setEaseOutQuint_mA59F24802CD0E1007E659D34F533F8F1CA7FAD13 ();
// 0x00000179 LTDescr LTDescr::setEaseInOutQuint()
extern void LTDescr_setEaseInOutQuint_mF815B5687F9162E45C9D1AD031C317840712FE19 ();
// 0x0000017A LTDescr LTDescr::setEaseInSine()
extern void LTDescr_setEaseInSine_mF481D539F3DA2FF9AE13F373BB79B2CC93AE82B6 ();
// 0x0000017B LTDescr LTDescr::setEaseOutSine()
extern void LTDescr_setEaseOutSine_mD8439D9AD162893667913F82632A087BB7BF66D0 ();
// 0x0000017C LTDescr LTDescr::setEaseInOutSine()
extern void LTDescr_setEaseInOutSine_m1647757426F728F0A076E73DCB939C469B77E1A9 ();
// 0x0000017D LTDescr LTDescr::setEaseInExpo()
extern void LTDescr_setEaseInExpo_mE27C2F90670FA473FC8D27DD70D3E4F47A4935C2 ();
// 0x0000017E LTDescr LTDescr::setEaseOutExpo()
extern void LTDescr_setEaseOutExpo_m9220EBCED76070AE7B72E997FD5711A68D35371F ();
// 0x0000017F LTDescr LTDescr::setEaseInOutExpo()
extern void LTDescr_setEaseInOutExpo_mA4B0D2B2F98FDD2C5864ECA9000CADFBCE38C8CC ();
// 0x00000180 LTDescr LTDescr::setEaseInCirc()
extern void LTDescr_setEaseInCirc_m75ABE78F120AC863C1BFCE3426ED6940CA9B30BD ();
// 0x00000181 LTDescr LTDescr::setEaseOutCirc()
extern void LTDescr_setEaseOutCirc_m2728C6194974BE6C02932FB34B991EE2110FB14D ();
// 0x00000182 LTDescr LTDescr::setEaseInOutCirc()
extern void LTDescr_setEaseInOutCirc_m75B555F1A463C9A3B59D2DA7642B9A7AD36770F2 ();
// 0x00000183 LTDescr LTDescr::setEaseInBounce()
extern void LTDescr_setEaseInBounce_mFC166FCC2D1359368A01D60AD95F87880E89E22B ();
// 0x00000184 LTDescr LTDescr::setEaseOutBounce()
extern void LTDescr_setEaseOutBounce_mF118AA6B5601E83ED52CCDE9A0EA4C92F30A5626 ();
// 0x00000185 LTDescr LTDescr::setEaseInOutBounce()
extern void LTDescr_setEaseInOutBounce_m1268040122DB6FA1269D298171FADB03DDE8BA9E ();
// 0x00000186 LTDescr LTDescr::setEaseInBack()
extern void LTDescr_setEaseInBack_m3220F0CC60207776341D8612292803E58A8D2175 ();
// 0x00000187 LTDescr LTDescr::setEaseOutBack()
extern void LTDescr_setEaseOutBack_mF300DC1C059B5BB6CCD5BFC2CC71B665334AD717 ();
// 0x00000188 LTDescr LTDescr::setEaseInOutBack()
extern void LTDescr_setEaseInOutBack_mCEA5FB6CF2A0A6335CF1F9F06177A4B663CE1F47 ();
// 0x00000189 LTDescr LTDescr::setEaseInElastic()
extern void LTDescr_setEaseInElastic_m1FE80F0CD5ABAA68172CE15DB1280788BB53B440 ();
// 0x0000018A LTDescr LTDescr::setEaseOutElastic()
extern void LTDescr_setEaseOutElastic_m0AED6CBCC243E3EE10DBCE0973F32BF5DD16A873 ();
// 0x0000018B LTDescr LTDescr::setEaseInOutElastic()
extern void LTDescr_setEaseInOutElastic_m494EE77E1C98A7356847D0B44E4D088A5ACC1F6A ();
// 0x0000018C LTDescr LTDescr::setEasePunch()
extern void LTDescr_setEasePunch_m7B72691172462D818F06010F316087DBF5EDEB49 ();
// 0x0000018D LTDescr LTDescr::setEaseShake()
extern void LTDescr_setEaseShake_m6AE6595FBDF8FFB68838D6D20B29C64EBAC941E9 ();
// 0x0000018E UnityEngine.Vector3 LTDescr::tweenOnCurve()
extern void LTDescr_tweenOnCurve_m84B46DFEB19FDB6D772DE2D47B4366E0A706C4DB ();
// 0x0000018F UnityEngine.Vector3 LTDescr::easeInOutQuad()
extern void LTDescr_easeInOutQuad_m36065001F18BC0B29C29EBA965B08989B028234F ();
// 0x00000190 UnityEngine.Vector3 LTDescr::easeInQuad()
extern void LTDescr_easeInQuad_m802EB621FB9F8EF34C4DD060972E506C7D6CD1F0 ();
// 0x00000191 UnityEngine.Vector3 LTDescr::easeOutQuad()
extern void LTDescr_easeOutQuad_mA2A1E36372772F90ED90F59E97062FE128B6794D ();
// 0x00000192 UnityEngine.Vector3 LTDescr::easeLinear()
extern void LTDescr_easeLinear_mAD6CECCB459D27BE9B3ACEBDC2F7473F6A688A17 ();
// 0x00000193 UnityEngine.Vector3 LTDescr::easeSpring()
extern void LTDescr_easeSpring_m9D0A0D6942FF7A2D9DE855C19487D7E9ED6F26E7 ();
// 0x00000194 UnityEngine.Vector3 LTDescr::easeInCubic()
extern void LTDescr_easeInCubic_mEF13536017E3D96BA908FA0C37028D3BB7FD7304 ();
// 0x00000195 UnityEngine.Vector3 LTDescr::easeOutCubic()
extern void LTDescr_easeOutCubic_mD5A8610D69B8116750EA64A02DFAB3314EC11C78 ();
// 0x00000196 UnityEngine.Vector3 LTDescr::easeInOutCubic()
extern void LTDescr_easeInOutCubic_m2F441E0BDA04412B6746865E4D810E30E830C0E3 ();
// 0x00000197 UnityEngine.Vector3 LTDescr::easeInQuart()
extern void LTDescr_easeInQuart_m865902CA1856CE060535F26E68FC3E48A1B9EC0C ();
// 0x00000198 UnityEngine.Vector3 LTDescr::easeOutQuart()
extern void LTDescr_easeOutQuart_mE87CA7D4DF0135B45C8536A8FC74D4CDE8EE1357 ();
// 0x00000199 UnityEngine.Vector3 LTDescr::easeInOutQuart()
extern void LTDescr_easeInOutQuart_m6D290AB4B5EDF285E1A4AD1945B87864B810E672 ();
// 0x0000019A UnityEngine.Vector3 LTDescr::easeInQuint()
extern void LTDescr_easeInQuint_m3A8922E83FB7FD5921895D0C7C180B251E0D8D87 ();
// 0x0000019B UnityEngine.Vector3 LTDescr::easeOutQuint()
extern void LTDescr_easeOutQuint_mA84CCBDB25A1E58D82C7A446850ADDA258592A0D ();
// 0x0000019C UnityEngine.Vector3 LTDescr::easeInOutQuint()
extern void LTDescr_easeInOutQuint_m73C03ABA976E0D1D40E8835BE9A1FE5DBF7F640B ();
// 0x0000019D UnityEngine.Vector3 LTDescr::easeInSine()
extern void LTDescr_easeInSine_m0608A3B0E9C83EC723B9FB55149DB9837F51AB41 ();
// 0x0000019E UnityEngine.Vector3 LTDescr::easeOutSine()
extern void LTDescr_easeOutSine_mD364D0BBA67752AE94B5752213507D91A41C3CF2 ();
// 0x0000019F UnityEngine.Vector3 LTDescr::easeInOutSine()
extern void LTDescr_easeInOutSine_m5817005F731481A8FB1B8B88E63319B0C08E27AA ();
// 0x000001A0 UnityEngine.Vector3 LTDescr::easeInExpo()
extern void LTDescr_easeInExpo_m8975809F06D63CBAC26760C7243779393D7FCCC5 ();
// 0x000001A1 UnityEngine.Vector3 LTDescr::easeOutExpo()
extern void LTDescr_easeOutExpo_m1793556A051CCF34AD78DCC5FEE153CFEDC857F2 ();
// 0x000001A2 UnityEngine.Vector3 LTDescr::easeInOutExpo()
extern void LTDescr_easeInOutExpo_mF6009DCEAA7D84D5E219F93448BF38340917DF47 ();
// 0x000001A3 UnityEngine.Vector3 LTDescr::easeInCirc()
extern void LTDescr_easeInCirc_m7F525DEA40803C0025FAC510774C26F86AB100DC ();
// 0x000001A4 UnityEngine.Vector3 LTDescr::easeOutCirc()
extern void LTDescr_easeOutCirc_m78859138DEAF3005D845A924841F0EA8BBEAD585 ();
// 0x000001A5 UnityEngine.Vector3 LTDescr::easeInOutCirc()
extern void LTDescr_easeInOutCirc_m16338C8496BAE07CDE37BDA8FCF813BFBA7F19FE ();
// 0x000001A6 UnityEngine.Vector3 LTDescr::easeInBounce()
extern void LTDescr_easeInBounce_m16D1EE3AB1D5E67FEBE2EA6DBE9C95A92F084C3F ();
// 0x000001A7 UnityEngine.Vector3 LTDescr::easeOutBounce()
extern void LTDescr_easeOutBounce_mC4DFF3C3FD156884B92838BBABB56570EFDBADFD ();
// 0x000001A8 UnityEngine.Vector3 LTDescr::easeInOutBounce()
extern void LTDescr_easeInOutBounce_mFD18B378E248EB26725EEE7267C20BD3FBED946C ();
// 0x000001A9 UnityEngine.Vector3 LTDescr::easeInBack()
extern void LTDescr_easeInBack_mD2D30FEF3F06538124A2AB1775BCA2CED07256A8 ();
// 0x000001AA UnityEngine.Vector3 LTDescr::easeOutBack()
extern void LTDescr_easeOutBack_m744F4CCCD32F8F556B68C9577E67B0468BEA3DE8 ();
// 0x000001AB UnityEngine.Vector3 LTDescr::easeInOutBack()
extern void LTDescr_easeInOutBack_m57DB03AEDB85C66A5925B8FF6F15B7C97E2DDFC2 ();
// 0x000001AC UnityEngine.Vector3 LTDescr::easeInElastic()
extern void LTDescr_easeInElastic_mDF8A4B5CDAAA652D432B99F536FDE5F12D29CC13 ();
// 0x000001AD UnityEngine.Vector3 LTDescr::easeOutElastic()
extern void LTDescr_easeOutElastic_m8D73182BD8A48500C5FB89BF4AA84094CCB62F85 ();
// 0x000001AE UnityEngine.Vector3 LTDescr::easeInOutElastic()
extern void LTDescr_easeInOutElastic_m3BFEB7C3B52B2C061544C8C2849BB7AD000E341C ();
// 0x000001AF LTDescr LTDescr::setOvershoot(System.Single)
extern void LTDescr_setOvershoot_m0E935F8EBE21ED62F29A62DD2FC4E7C1378E0544 ();
// 0x000001B0 LTDescr LTDescr::setPeriod(System.Single)
extern void LTDescr_setPeriod_m6D8399405BD7E4C3CE73E90B1DF55991A6EEF6B6 ();
// 0x000001B1 LTDescr LTDescr::setScale(System.Single)
extern void LTDescr_setScale_m75F4440E58BAAACBE7205441A2FDB96E61AFE2AB ();
// 0x000001B2 LTDescr LTDescr::setEase(UnityEngine.AnimationCurve)
extern void LTDescr_setEase_mBF09981854FEEF635C7CD97C0CBAB2A941193DC6 ();
// 0x000001B3 LTDescr LTDescr::setTo(UnityEngine.Vector3)
extern void LTDescr_setTo_m829E53D1E4FA8D3C11D35CC8E736AAAC151A8234 ();
// 0x000001B4 LTDescr LTDescr::setTo(UnityEngine.Transform)
extern void LTDescr_setTo_mB3C80993607B61B638F5EC5D773CA42AE33CDB3A ();
// 0x000001B5 LTDescr LTDescr::setFrom(UnityEngine.Vector3)
extern void LTDescr_setFrom_m869544CF9DE9E93780E6ADAED4B99CD3580058AC ();
// 0x000001B6 LTDescr LTDescr::setFrom(System.Single)
extern void LTDescr_setFrom_m916CEBF6049D5ACC8C00337FF34DF141461951A2 ();
// 0x000001B7 LTDescr LTDescr::setDiff(UnityEngine.Vector3)
extern void LTDescr_setDiff_m4E9CA4C17BEAC2C76720E33912216FE25D816E5D ();
// 0x000001B8 LTDescr LTDescr::setHasInitialized(System.Boolean)
extern void LTDescr_setHasInitialized_mDCBCCB7087C97B20C58181593EB1C7A8587C84C6 ();
// 0x000001B9 LTDescr LTDescr::setId(System.UInt32,System.UInt32)
extern void LTDescr_setId_m4F5E561450C9DD9297F9C0A4920E7F56A57BA8F6 ();
// 0x000001BA LTDescr LTDescr::setPassed(System.Single)
extern void LTDescr_setPassed_m629829107456CBBEA75E743A244D5A8156872ED4 ();
// 0x000001BB LTDescr LTDescr::setTime(System.Single)
extern void LTDescr_setTime_m9DB98DF42D1DE55F45346D9F4F0C0B836EF5BE82 ();
// 0x000001BC LTDescr LTDescr::setSpeed(System.Single)
extern void LTDescr_setSpeed_mD51B284F6FEC68EACC2216BCC1CAE20588B88596 ();
// 0x000001BD LTDescr LTDescr::setRepeat(System.Int32)
extern void LTDescr_setRepeat_m39C1FFE0F3BE7AEA0AB2D3D09B8C5E93471FD528 ();
// 0x000001BE LTDescr LTDescr::setLoopType(LeanTweenType)
extern void LTDescr_setLoopType_m432119442AFE9EC9DCA026A196ACB390CB6E490A ();
// 0x000001BF LTDescr LTDescr::setUseEstimatedTime(System.Boolean)
extern void LTDescr_setUseEstimatedTime_m1DCE69547A9FDA123D41FC468334AAB464A9E27A ();
// 0x000001C0 LTDescr LTDescr::setIgnoreTimeScale(System.Boolean)
extern void LTDescr_setIgnoreTimeScale_m74876A4244E7C476990FD9F49A32F2077733D10E ();
// 0x000001C1 LTDescr LTDescr::setUseFrames(System.Boolean)
extern void LTDescr_setUseFrames_mC5C200981C6D66E22A194C46E4E68F4DEE93A583 ();
// 0x000001C2 LTDescr LTDescr::setUseManualTime(System.Boolean)
extern void LTDescr_setUseManualTime_mA583DBD31325B81D677BFDE10848D522F3B13917 ();
// 0x000001C3 LTDescr LTDescr::setLoopCount(System.Int32)
extern void LTDescr_setLoopCount_m5F4204D6F059EAB72ECF645AFFF1909CE328E9A7 ();
// 0x000001C4 LTDescr LTDescr::setLoopOnce()
extern void LTDescr_setLoopOnce_mC7EF69D620AE0723DBF4F7786DDE722A9CC46914 ();
// 0x000001C5 LTDescr LTDescr::setLoopClamp()
extern void LTDescr_setLoopClamp_m26C0A8ECA2F70ADA1C48A2C92B13B47B58E2B1B0 ();
// 0x000001C6 LTDescr LTDescr::setLoopClamp(System.Int32)
extern void LTDescr_setLoopClamp_m45FC70613BB57B5B72A397242071368CCC539E35 ();
// 0x000001C7 LTDescr LTDescr::setLoopPingPong()
extern void LTDescr_setLoopPingPong_m53E03879D46B899429F463427A5EACEE747F9636 ();
// 0x000001C8 LTDescr LTDescr::setLoopPingPong(System.Int32)
extern void LTDescr_setLoopPingPong_m21667D1F3685ED55D706C025D48849E61D655B0E ();
// 0x000001C9 LTDescr LTDescr::setOnComplete(System.Action)
extern void LTDescr_setOnComplete_mDCB57CB9AFEC6C5FFD63A9B89962DEE80BD1E72C ();
// 0x000001CA LTDescr LTDescr::setOnComplete(System.Action`1<System.Object>)
extern void LTDescr_setOnComplete_m63F5635BC23F15B3B64960290B0D723D36BC1030 ();
// 0x000001CB LTDescr LTDescr::setOnComplete(System.Action`1<System.Object>,System.Object)
extern void LTDescr_setOnComplete_m2D493BA5F77149765D519E685C05ADEC2F9EE5E6 ();
// 0x000001CC LTDescr LTDescr::setOnCompleteParam(System.Object)
extern void LTDescr_setOnCompleteParam_m4172EF1D7B63D5B2BA5B4358BB8282FF55A5D9C2 ();
// 0x000001CD LTDescr LTDescr::setOnUpdate(System.Action`1<System.Single>)
extern void LTDescr_setOnUpdate_m437DAC29C60F0FFBE61C926313DF48FBB1BBDDE7 ();
// 0x000001CE LTDescr LTDescr::setOnUpdateRatio(System.Action`2<System.Single,System.Single>)
extern void LTDescr_setOnUpdateRatio_m004D6332C968E0F0059FF807E34770989AA009F6 ();
// 0x000001CF LTDescr LTDescr::setOnUpdateObject(System.Action`2<System.Single,System.Object>)
extern void LTDescr_setOnUpdateObject_m770A74E823CD5DB5B6448F7A66DED37B968893D1 ();
// 0x000001D0 LTDescr LTDescr::setOnUpdateVector2(System.Action`1<UnityEngine.Vector2>)
extern void LTDescr_setOnUpdateVector2_m7FD189610C25124F725477B4DC948F11A69455B1 ();
// 0x000001D1 LTDescr LTDescr::setOnUpdateVector3(System.Action`1<UnityEngine.Vector3>)
extern void LTDescr_setOnUpdateVector3_m0DDC2E6AEDC0E0EACE3947E4E67A6A6803EEFABA ();
// 0x000001D2 LTDescr LTDescr::setOnUpdateColor(System.Action`1<UnityEngine.Color>)
extern void LTDescr_setOnUpdateColor_mDAB53EF324EB204A7038AC70A88978C5E93D8009 ();
// 0x000001D3 LTDescr LTDescr::setOnUpdateColor(System.Action`2<UnityEngine.Color,System.Object>)
extern void LTDescr_setOnUpdateColor_mF0B148249135750B5B772E6FA0EFC59A9925E69D ();
// 0x000001D4 LTDescr LTDescr::setOnUpdate(System.Action`1<UnityEngine.Color>)
extern void LTDescr_setOnUpdate_mDFE9026BDBF9479028A6D2DF03C3F9E1C391E354 ();
// 0x000001D5 LTDescr LTDescr::setOnUpdate(System.Action`2<UnityEngine.Color,System.Object>)
extern void LTDescr_setOnUpdate_mA5CB1382CE68571CE5DB619406AC5C6EB22729B9 ();
// 0x000001D6 LTDescr LTDescr::setOnUpdate(System.Action`2<System.Single,System.Object>,System.Object)
extern void LTDescr_setOnUpdate_mEFD57B3855E8C131BA76DADD3FD13C41E867F58A ();
// 0x000001D7 LTDescr LTDescr::setOnUpdate(System.Action`2<UnityEngine.Vector3,System.Object>,System.Object)
extern void LTDescr_setOnUpdate_mEFD26172DE9342C6C0FB34248E3EA7F6267A1968 ();
// 0x000001D8 LTDescr LTDescr::setOnUpdate(System.Action`1<UnityEngine.Vector2>,System.Object)
extern void LTDescr_setOnUpdate_m73910A7E30781DFDE8149BF06376A3CB98DDF74B ();
// 0x000001D9 LTDescr LTDescr::setOnUpdate(System.Action`1<UnityEngine.Vector3>,System.Object)
extern void LTDescr_setOnUpdate_mAFDE1872327547292A82B3D393F4B7007913EFDB ();
// 0x000001DA LTDescr LTDescr::setOnUpdateParam(System.Object)
extern void LTDescr_setOnUpdateParam_mB449256AC309484AE7FF7E1E685A8B6B5727D4D0 ();
// 0x000001DB LTDescr LTDescr::setOrientToPath(System.Boolean)
extern void LTDescr_setOrientToPath_m6458FAA1A9AACC5BA13A1C83AEB87A63883CFBE4 ();
// 0x000001DC LTDescr LTDescr::setOrientToPath2d(System.Boolean)
extern void LTDescr_setOrientToPath2d_m135B485E55660A23D7FE3A51AA1C5D3E38804E27 ();
// 0x000001DD LTDescr LTDescr::setRect(LTRect)
extern void LTDescr_setRect_mDE3387640B652E3851AA62B4483FE0150067BCF6 ();
// 0x000001DE LTDescr LTDescr::setRect(UnityEngine.Rect)
extern void LTDescr_setRect_mFF955D0F4C0518F219B3DF1063035FF8AD8B5A4B ();
// 0x000001DF LTDescr LTDescr::setPath(LTBezierPath)
extern void LTDescr_setPath_mD75B1338F2117AEDC0E243C925038014EEC86974 ();
// 0x000001E0 LTDescr LTDescr::setPoint(UnityEngine.Vector3)
extern void LTDescr_setPoint_mE041B263E9365DE9D2C8A12537A69E59DE02D311 ();
// 0x000001E1 LTDescr LTDescr::setDestroyOnComplete(System.Boolean)
extern void LTDescr_setDestroyOnComplete_mD7468721AACBFC786485DC873912EF6DC442D831 ();
// 0x000001E2 LTDescr LTDescr::setAudio(System.Object)
extern void LTDescr_setAudio_m5AA3E45C784E79CC21DB55C4789C4C3B8DB5FE9B ();
// 0x000001E3 LTDescr LTDescr::setOnCompleteOnRepeat(System.Boolean)
extern void LTDescr_setOnCompleteOnRepeat_m6BB648BEEBC71B54A8EA1CD68E4C09C1BD0ED414 ();
// 0x000001E4 LTDescr LTDescr::setOnCompleteOnStart(System.Boolean)
extern void LTDescr_setOnCompleteOnStart_mEF3D29199EA20722C91FD0FA1663A279D46036F0 ();
// 0x000001E5 LTDescr LTDescr::setRect(UnityEngine.RectTransform)
extern void LTDescr_setRect_m24CBF3848B9211ED00193D668308C5849294191D ();
// 0x000001E6 LTDescr LTDescr::setSprites(UnityEngine.Sprite[])
extern void LTDescr_setSprites_mCA611B87878CCED8217DD98E25AC53FB8874B5BA ();
// 0x000001E7 LTDescr LTDescr::setFrameRate(System.Single)
extern void LTDescr_setFrameRate_mA3F0847615F3E5A97C192C548D40BDC1B6BC48F2 ();
// 0x000001E8 LTDescr LTDescr::setOnStart(System.Action)
extern void LTDescr_setOnStart_mAA21647EAE7FBD188227754F8F8E175985CD9379 ();
// 0x000001E9 LTDescr LTDescr::setDirection(System.Single)
extern void LTDescr_setDirection_m1C23D5D973D7CB4391403DFEFBDF12F15A1E75B4 ();
// 0x000001EA LTDescr LTDescr::setRecursive(System.Boolean)
extern void LTDescr_setRecursive_mB6A214753796545145520D40C7AF9C7B85B8DC97 ();
// 0x000001EB System.Void LTDescr::<setMoveX>b__73_0()
extern void LTDescr_U3CsetMoveXU3Eb__73_0_mC1B3A6D25B73B3945BA6BAE2A0B8048B2E7706F4 ();
// 0x000001EC System.Void LTDescr::<setMoveX>b__73_1()
extern void LTDescr_U3CsetMoveXU3Eb__73_1_mB298DE15C613EDB42361E42D3935981846884621 ();
// 0x000001ED System.Void LTDescr::<setMoveY>b__74_0()
extern void LTDescr_U3CsetMoveYU3Eb__74_0_mB45AE75CE0B616D21D397C4600C3FA7E19608E21 ();
// 0x000001EE System.Void LTDescr::<setMoveY>b__74_1()
extern void LTDescr_U3CsetMoveYU3Eb__74_1_mC7C72E8D96740B0F4F476545FF9A9CE6579D773C ();
// 0x000001EF System.Void LTDescr::<setMoveZ>b__75_0()
extern void LTDescr_U3CsetMoveZU3Eb__75_0_m2F90BBD09164CCA80404F98AEBE39E0C7C49E140 ();
// 0x000001F0 System.Void LTDescr::<setMoveZ>b__75_1()
extern void LTDescr_U3CsetMoveZU3Eb__75_1_mB0FCBC7EDEE189C9FC90E38F51C5F36FFB1C6E75 ();
// 0x000001F1 System.Void LTDescr::<setMoveLocalX>b__76_0()
extern void LTDescr_U3CsetMoveLocalXU3Eb__76_0_m2E2D10CEC52049D66A18E61B7A6820F5693D1000 ();
// 0x000001F2 System.Void LTDescr::<setMoveLocalX>b__76_1()
extern void LTDescr_U3CsetMoveLocalXU3Eb__76_1_m2B451B55022DFA5A2149080DCF5EFEA51DD1EF15 ();
// 0x000001F3 System.Void LTDescr::<setMoveLocalY>b__77_0()
extern void LTDescr_U3CsetMoveLocalYU3Eb__77_0_mD773679AD1B3F289E97B8A97FA3C9816B43B0F97 ();
// 0x000001F4 System.Void LTDescr::<setMoveLocalY>b__77_1()
extern void LTDescr_U3CsetMoveLocalYU3Eb__77_1_mD806F3C4BD68BD6A88B7E47DD52B9E8EF67516DA ();
// 0x000001F5 System.Void LTDescr::<setMoveLocalZ>b__78_0()
extern void LTDescr_U3CsetMoveLocalZU3Eb__78_0_mDFB2967D5068D7DA7D1FBD20D8F621C8A7E4A5C7 ();
// 0x000001F6 System.Void LTDescr::<setMoveLocalZ>b__78_1()
extern void LTDescr_U3CsetMoveLocalZU3Eb__78_1_m9FE503FE722E8A8877BF3559FFA4EFF806841A0E ();
// 0x000001F7 System.Void LTDescr::<setMoveCurved>b__81_0()
extern void LTDescr_U3CsetMoveCurvedU3Eb__81_0_m93E47D28F6BC0CAEF79EC1B39091B66D2ABFF67F ();
// 0x000001F8 System.Void LTDescr::<setMoveCurvedLocal>b__82_0()
extern void LTDescr_U3CsetMoveCurvedLocalU3Eb__82_0_m7AB4E3AF580B52EE389B4264B1C3DC08035631EF ();
// 0x000001F9 System.Void LTDescr::<setMoveSpline>b__83_0()
extern void LTDescr_U3CsetMoveSplineU3Eb__83_0_mB704A557E71BAE0EEADA2DBDC7C85EBDE842174D ();
// 0x000001FA System.Void LTDescr::<setMoveSplineLocal>b__84_0()
extern void LTDescr_U3CsetMoveSplineLocalU3Eb__84_0_m71CA556831D55D2294212AF2E49D1C38ACB38C73 ();
// 0x000001FB System.Void LTDescr::<setScaleX>b__85_0()
extern void LTDescr_U3CsetScaleXU3Eb__85_0_mFF1273FC3FC6E6955FDF2804FB5894109E1E6877 ();
// 0x000001FC System.Void LTDescr::<setScaleX>b__85_1()
extern void LTDescr_U3CsetScaleXU3Eb__85_1_mB8660436928C1ABFE1AAA2988DC2AC0BD6CEDFAB ();
// 0x000001FD System.Void LTDescr::<setScaleY>b__86_0()
extern void LTDescr_U3CsetScaleYU3Eb__86_0_m86846124370FA864FD9E0A6F84F3ACFFB73946DB ();
// 0x000001FE System.Void LTDescr::<setScaleY>b__86_1()
extern void LTDescr_U3CsetScaleYU3Eb__86_1_m2A79BFC6964E50B4602F3C4D17A372BA1712F4E2 ();
// 0x000001FF System.Void LTDescr::<setScaleZ>b__87_0()
extern void LTDescr_U3CsetScaleZU3Eb__87_0_m5BB4E20E122944640691C42C328283ACB0E779AA ();
// 0x00000200 System.Void LTDescr::<setScaleZ>b__87_1()
extern void LTDescr_U3CsetScaleZU3Eb__87_1_mDF138908ABE364C4CA46DA854F9C1A01F3A115F8 ();
// 0x00000201 System.Void LTDescr::<setRotateX>b__88_0()
extern void LTDescr_U3CsetRotateXU3Eb__88_0_m493F46830D9AE2D265D85AECD9CC07542CB2D164 ();
// 0x00000202 System.Void LTDescr::<setRotateX>b__88_1()
extern void LTDescr_U3CsetRotateXU3Eb__88_1_mC1D2B816B4365D56F639D50DDA16AE00A3E6FF5F ();
// 0x00000203 System.Void LTDescr::<setRotateY>b__89_0()
extern void LTDescr_U3CsetRotateYU3Eb__89_0_m0C043D7BDF837A3C3FDA107395C85D181B66F13F ();
// 0x00000204 System.Void LTDescr::<setRotateY>b__89_1()
extern void LTDescr_U3CsetRotateYU3Eb__89_1_m4E38375358C4BC49BD7BF69476F9B0FA5525BACF ();
// 0x00000205 System.Void LTDescr::<setRotateZ>b__90_0()
extern void LTDescr_U3CsetRotateZU3Eb__90_0_mA1C23C492D18C89ED39A5E0B2B24F14930DAB66A ();
// 0x00000206 System.Void LTDescr::<setRotateZ>b__90_1()
extern void LTDescr_U3CsetRotateZU3Eb__90_1_mD77CC83BA388F7F3A8E101D31AC5662901F347B3 ();
// 0x00000207 System.Void LTDescr::<setRotateAround>b__91_0()
extern void LTDescr_U3CsetRotateAroundU3Eb__91_0_mC30EB5DCA1B236C2D2BFB1BC8C2F620729399547 ();
// 0x00000208 System.Void LTDescr::<setRotateAround>b__91_1()
extern void LTDescr_U3CsetRotateAroundU3Eb__91_1_m54A375FE4EE6BB07CB382CDA4B91D655E30D8177 ();
// 0x00000209 System.Void LTDescr::<setRotateAroundLocal>b__92_0()
extern void LTDescr_U3CsetRotateAroundLocalU3Eb__92_0_m71580817297D4C28EF66A991FE629314AD3FE249 ();
// 0x0000020A System.Void LTDescr::<setRotateAroundLocal>b__92_1()
extern void LTDescr_U3CsetRotateAroundLocalU3Eb__92_1_mEA423F4B08C78D3F9319DF0CF2C2EA95CFA40595 ();
// 0x0000020B System.Void LTDescr::<setAlpha>b__93_0()
extern void LTDescr_U3CsetAlphaU3Eb__93_0_m62840E0A1F518EEED15E89CA1B8FE51800CB6ACA ();
// 0x0000020C System.Void LTDescr::<setAlpha>b__93_2()
extern void LTDescr_U3CsetAlphaU3Eb__93_2_m74F5E55F3656D02C098B8D5F51444822BCBF619E ();
// 0x0000020D System.Void LTDescr::<setAlpha>b__93_1()
extern void LTDescr_U3CsetAlphaU3Eb__93_1_m5E1833D9D31C62DF5234D7AD9CB657BE909F10C0 ();
// 0x0000020E System.Void LTDescr::<setTextAlpha>b__94_0()
extern void LTDescr_U3CsetTextAlphaU3Eb__94_0_m8DFC387E69EA4F5D71017CA03EBBB3B865041251 ();
// 0x0000020F System.Void LTDescr::<setTextAlpha>b__94_1()
extern void LTDescr_U3CsetTextAlphaU3Eb__94_1_mF29634A706AB1DFF06D7ED9D5314C9CC10998676 ();
// 0x00000210 System.Void LTDescr::<setAlphaVertex>b__95_0()
extern void LTDescr_U3CsetAlphaVertexU3Eb__95_0_m38DB9282A06B18A985B4FA2A3051210DB0796FA3 ();
// 0x00000211 System.Void LTDescr::<setAlphaVertex>b__95_1()
extern void LTDescr_U3CsetAlphaVertexU3Eb__95_1_m9B16A2B73E285ABB14E7AECE55CACB8C04BE2ECB ();
// 0x00000212 System.Void LTDescr::<setColor>b__96_0()
extern void LTDescr_U3CsetColorU3Eb__96_0_mC4C335EE7D3765075D4287E9C5C4E185A010B463 ();
// 0x00000213 System.Void LTDescr::<setColor>b__96_1()
extern void LTDescr_U3CsetColorU3Eb__96_1_mF45002C64332B080B32EE1056139487352147496 ();
// 0x00000214 System.Void LTDescr::<setCallbackColor>b__97_0()
extern void LTDescr_U3CsetCallbackColorU3Eb__97_0_m0C6A0800CAAB148669FB87B0E686A60E9E0E1E50 ();
// 0x00000215 System.Void LTDescr::<setCallbackColor>b__97_1()
extern void LTDescr_U3CsetCallbackColorU3Eb__97_1_mD30A488146A9F8DE2876C79404BF0A69361CFFC8 ();
// 0x00000216 System.Void LTDescr::<setTextColor>b__98_0()
extern void LTDescr_U3CsetTextColorU3Eb__98_0_m3369FE91DC4472C45068C9B348E308991CDB0D92 ();
// 0x00000217 System.Void LTDescr::<setTextColor>b__98_1()
extern void LTDescr_U3CsetTextColorU3Eb__98_1_mAD33FF9EE92E06980CBEB4C7759E32DA69F625EB ();
// 0x00000218 System.Void LTDescr::<setCanvasAlpha>b__99_0()
extern void LTDescr_U3CsetCanvasAlphaU3Eb__99_0_m4AC161231D4D1B57CA69B7BA0323F4E6CF0F4CA2 ();
// 0x00000219 System.Void LTDescr::<setCanvasAlpha>b__99_1()
extern void LTDescr_U3CsetCanvasAlphaU3Eb__99_1_m2B6D509A77B2396A304997DC39DCB3455E4D198F ();
// 0x0000021A System.Void LTDescr::<setCanvasGroupAlpha>b__100_0()
extern void LTDescr_U3CsetCanvasGroupAlphaU3Eb__100_0_mF33DE2956D5D901DC51B5D9924EFD4FD3E38ED57 ();
// 0x0000021B System.Void LTDescr::<setCanvasGroupAlpha>b__100_1()
extern void LTDescr_U3CsetCanvasGroupAlphaU3Eb__100_1_mBA6DC3BCE7F96E4D726A0AC809397EE35FFFF222 ();
// 0x0000021C System.Void LTDescr::<setCanvasColor>b__101_0()
extern void LTDescr_U3CsetCanvasColorU3Eb__101_0_m7054A2F4C59932B279ACCE46880C9832B8A9D889 ();
// 0x0000021D System.Void LTDescr::<setCanvasColor>b__101_1()
extern void LTDescr_U3CsetCanvasColorU3Eb__101_1_m6460741C9E869E7B10CB86D933E4BDCA18D737ED ();
// 0x0000021E System.Void LTDescr::<setCanvasMoveX>b__102_0()
extern void LTDescr_U3CsetCanvasMoveXU3Eb__102_0_mFE234BD5C94E07BCF7414CB6D52994226A53EF0C ();
// 0x0000021F System.Void LTDescr::<setCanvasMoveX>b__102_1()
extern void LTDescr_U3CsetCanvasMoveXU3Eb__102_1_m2DA6E431C160C39E876CD86BFF777B6D8D0C0894 ();
// 0x00000220 System.Void LTDescr::<setCanvasMoveY>b__103_0()
extern void LTDescr_U3CsetCanvasMoveYU3Eb__103_0_m54D0560CC7DACB6DF5B711C9DFF85C1A08524104 ();
// 0x00000221 System.Void LTDescr::<setCanvasMoveY>b__103_1()
extern void LTDescr_U3CsetCanvasMoveYU3Eb__103_1_m25A736629C0A77D4B89641F916444588968FA12B ();
// 0x00000222 System.Void LTDescr::<setCanvasMoveZ>b__104_0()
extern void LTDescr_U3CsetCanvasMoveZU3Eb__104_0_m334C0650BE3F004D3246A37F9703EE58F310AFEF ();
// 0x00000223 System.Void LTDescr::<setCanvasMoveZ>b__104_1()
extern void LTDescr_U3CsetCanvasMoveZU3Eb__104_1_m093E0527B343F64B8D424AFFF2FA73EFF4CD23AC ();
// 0x00000224 System.Void LTDescr::<setCanvasRotateAround>b__106_0()
extern void LTDescr_U3CsetCanvasRotateAroundU3Eb__106_0_m77AE4A4821E0D9DA232D32B3F85AA6D17B102CE1 ();
// 0x00000225 System.Void LTDescr::<setCanvasRotateAroundLocal>b__107_0()
extern void LTDescr_U3CsetCanvasRotateAroundLocalU3Eb__107_0_m5B22A810186C6A02EEA6C3D1F9E7823A58F48A8A ();
// 0x00000226 System.Void LTDescr::<setCanvasPlaySprite>b__108_0()
extern void LTDescr_U3CsetCanvasPlaySpriteU3Eb__108_0_mCCC3A883C06689CE1AF4E56F599E17E6A08063D9 ();
// 0x00000227 System.Void LTDescr::<setCanvasPlaySprite>b__108_1()
extern void LTDescr_U3CsetCanvasPlaySpriteU3Eb__108_1_mEEA14A276656406AA550A39A45E0EF0DBCFCAD00 ();
// 0x00000228 System.Void LTDescr::<setCanvasMove>b__109_0()
extern void LTDescr_U3CsetCanvasMoveU3Eb__109_0_m9B67812E976E616B40413119359E8A358B779DD9 ();
// 0x00000229 System.Void LTDescr::<setCanvasMove>b__109_1()
extern void LTDescr_U3CsetCanvasMoveU3Eb__109_1_m515B75E2D9C1099D35D29062A747D6881F3FD22A ();
// 0x0000022A System.Void LTDescr::<setCanvasScale>b__110_0()
extern void LTDescr_U3CsetCanvasScaleU3Eb__110_0_m66C78DAA27E664E37107383D3F72D9EAE85E93B5 ();
// 0x0000022B System.Void LTDescr::<setCanvasScale>b__110_1()
extern void LTDescr_U3CsetCanvasScaleU3Eb__110_1_m7DED18C9319531415228CB9DAD9E40A0578BA7D6 ();
// 0x0000022C System.Void LTDescr::<setCanvasSizeDelta>b__111_0()
extern void LTDescr_U3CsetCanvasSizeDeltaU3Eb__111_0_m622448B8EA313708FCB496103613BE9368191CFA ();
// 0x0000022D System.Void LTDescr::<setCanvasSizeDelta>b__111_1()
extern void LTDescr_U3CsetCanvasSizeDeltaU3Eb__111_1_mEB78ECCD2C73EDA02A10E61A43F44B0522D99FA8 ();
// 0x0000022E System.Void LTDescr::<setMove>b__115_0()
extern void LTDescr_U3CsetMoveU3Eb__115_0_mA9FC21145770DFE3CD83595E33E2A3F9CBA088A5 ();
// 0x0000022F System.Void LTDescr::<setMove>b__115_1()
extern void LTDescr_U3CsetMoveU3Eb__115_1_m268228A84C3365C7DFD41C66717C908DD0408766 ();
// 0x00000230 System.Void LTDescr::<setMoveLocal>b__116_0()
extern void LTDescr_U3CsetMoveLocalU3Eb__116_0_m81C2B5508318FB8C79015D00599694265975859F ();
// 0x00000231 System.Void LTDescr::<setMoveLocal>b__116_1()
extern void LTDescr_U3CsetMoveLocalU3Eb__116_1_m4FC9E996AA295A46BEAF965E85DC9372A8A1C081 ();
// 0x00000232 System.Void LTDescr::<setMoveToTransform>b__117_0()
extern void LTDescr_U3CsetMoveToTransformU3Eb__117_0_m4A1FC4A154E19A07F743509A9044C9447DB2A24E ();
// 0x00000233 System.Void LTDescr::<setMoveToTransform>b__117_1()
extern void LTDescr_U3CsetMoveToTransformU3Eb__117_1_m1CED29CFFC5D0BE430F5DCC24367B64CEE4E43F3 ();
// 0x00000234 System.Void LTDescr::<setRotate>b__118_0()
extern void LTDescr_U3CsetRotateU3Eb__118_0_m23A4A1AD7B915AC5EA95B9AE5231809F4E7A7D97 ();
// 0x00000235 System.Void LTDescr::<setRotate>b__118_1()
extern void LTDescr_U3CsetRotateU3Eb__118_1_m575493C2DA420D01FEA96EE0AA7E4871430EBD55 ();
// 0x00000236 System.Void LTDescr::<setRotateLocal>b__119_0()
extern void LTDescr_U3CsetRotateLocalU3Eb__119_0_m8A85181B21112A32CE5CCE169692DEDB501B4E23 ();
// 0x00000237 System.Void LTDescr::<setRotateLocal>b__119_1()
extern void LTDescr_U3CsetRotateLocalU3Eb__119_1_m41757196871546467230FABFFB37075B85251B1F ();
// 0x00000238 System.Void LTDescr::<setScale>b__120_0()
extern void LTDescr_U3CsetScaleU3Eb__120_0_mC7315F796459D639557D6B0F54514AEED391BA35 ();
// 0x00000239 System.Void LTDescr::<setScale>b__120_1()
extern void LTDescr_U3CsetScaleU3Eb__120_1_mE6F7F8EAEC18A9A19D4D698E0FD8034E91F763F6 ();
// 0x0000023A System.Void LTDescr::<setGUIMove>b__121_0()
extern void LTDescr_U3CsetGUIMoveU3Eb__121_0_m1224EB2264D882E8A0055B5AE36CEEE740B3F21C ();
// 0x0000023B System.Void LTDescr::<setGUIMove>b__121_1()
extern void LTDescr_U3CsetGUIMoveU3Eb__121_1_m1A995AAE2505372C605DC64A13E5F8D9282307FE ();
// 0x0000023C System.Void LTDescr::<setGUIMoveMargin>b__122_0()
extern void LTDescr_U3CsetGUIMoveMarginU3Eb__122_0_mA08F3D978D14B8FE43A13FD6C51C6EA287D70276 ();
// 0x0000023D System.Void LTDescr::<setGUIMoveMargin>b__122_1()
extern void LTDescr_U3CsetGUIMoveMarginU3Eb__122_1_m6C40E08EF41DE0B32C19DF4483E1D153D1CA04AF ();
// 0x0000023E System.Void LTDescr::<setGUIScale>b__123_0()
extern void LTDescr_U3CsetGUIScaleU3Eb__123_0_m713651C4163C0D725D46C2CE175BEA1E8A541770 ();
// 0x0000023F System.Void LTDescr::<setGUIScale>b__123_1()
extern void LTDescr_U3CsetGUIScaleU3Eb__123_1_mD45B431D66ED1E7AA392547A69C2F14698FC31DD ();
// 0x00000240 System.Void LTDescr::<setGUIAlpha>b__124_0()
extern void LTDescr_U3CsetGUIAlphaU3Eb__124_0_mB1E87936B50CEA5B8066511633A64DC06C437B7D ();
// 0x00000241 System.Void LTDescr::<setGUIAlpha>b__124_1()
extern void LTDescr_U3CsetGUIAlphaU3Eb__124_1_m8E111699E7BB33552DF1284E5B6E793D53A0E32A ();
// 0x00000242 System.Void LTDescr::<setGUIRotate>b__125_0()
extern void LTDescr_U3CsetGUIRotateU3Eb__125_0_m608C2F49CC8E321754F2F2C67186893EC1BCF121 ();
// 0x00000243 System.Void LTDescr::<setGUIRotate>b__125_1()
extern void LTDescr_U3CsetGUIRotateU3Eb__125_1_m3CF50F3F6A1980ECB3712F1A437DF9A3EE9A7A17 ();
// 0x00000244 System.Void LTDescr::<setDelayedSound>b__126_0()
extern void LTDescr_U3CsetDelayedSoundU3Eb__126_0_mC6AFAF26799C7A75AE029A2F1B6ACCFC0EC9C714 ();
// 0x00000245 UnityEngine.Transform LTDescrOptional::get_toTrans()
extern void LTDescrOptional_get_toTrans_mB530A713C24C3F06F8B42AF31CA3FF977AB7EE0C ();
// 0x00000246 System.Void LTDescrOptional::set_toTrans(UnityEngine.Transform)
extern void LTDescrOptional_set_toTrans_mD1A169A39C828FA5536708E3A19F1A1D24370ADA ();
// 0x00000247 UnityEngine.Vector3 LTDescrOptional::get_point()
extern void LTDescrOptional_get_point_m684081D83B6A9895107F41A78E6FB2EF4F871A62 ();
// 0x00000248 System.Void LTDescrOptional::set_point(UnityEngine.Vector3)
extern void LTDescrOptional_set_point_m699F8960E9420CC80E624E3A914294836144D84B ();
// 0x00000249 UnityEngine.Vector3 LTDescrOptional::get_axis()
extern void LTDescrOptional_get_axis_mB136A693C94957550FB4A493D8055AF2A8498950 ();
// 0x0000024A System.Void LTDescrOptional::set_axis(UnityEngine.Vector3)
extern void LTDescrOptional_set_axis_m3B316ED04F0DD39AA46549611DD2A87CB09563A3 ();
// 0x0000024B System.Single LTDescrOptional::get_lastVal()
extern void LTDescrOptional_get_lastVal_mF32405DF5E4967E0526173E12A1D49ED459DEE96 ();
// 0x0000024C System.Void LTDescrOptional::set_lastVal(System.Single)
extern void LTDescrOptional_set_lastVal_m10DF605FAB025AE13E603C2272486F8A4CB90A92 ();
// 0x0000024D UnityEngine.Quaternion LTDescrOptional::get_origRotation()
extern void LTDescrOptional_get_origRotation_m07382A5C09A8A47B7D9C9DE129BD7CF56E59FDBF ();
// 0x0000024E System.Void LTDescrOptional::set_origRotation(UnityEngine.Quaternion)
extern void LTDescrOptional_set_origRotation_m72912AB74F93857BB64706F91607E176B7C5B084 ();
// 0x0000024F LTBezierPath LTDescrOptional::get_path()
extern void LTDescrOptional_get_path_m70C0472D3F2618618EA9A0EA152799AD814CBF56 ();
// 0x00000250 System.Void LTDescrOptional::set_path(LTBezierPath)
extern void LTDescrOptional_set_path_mBA7489CB0E86A582174D0A5E183CC0BE9FF859A0 ();
// 0x00000251 LTSpline LTDescrOptional::get_spline()
extern void LTDescrOptional_get_spline_m8DF792E3FA917F95827962B53715396B14B58CFE ();
// 0x00000252 System.Void LTDescrOptional::set_spline(LTSpline)
extern void LTDescrOptional_set_spline_mE5EEFABD4DE99672B93F6A46B09B90C272EEE01F ();
// 0x00000253 LTRect LTDescrOptional::get_ltRect()
extern void LTDescrOptional_get_ltRect_m6906D85E922AD1856AEEDE0E2736AA2CAD13401C ();
// 0x00000254 System.Void LTDescrOptional::set_ltRect(LTRect)
extern void LTDescrOptional_set_ltRect_m16894F8919730156E369DB2273FDA7EFF3736C1E ();
// 0x00000255 System.Action`1<System.Single> LTDescrOptional::get_onUpdateFloat()
extern void LTDescrOptional_get_onUpdateFloat_m22C332D592FA821F4A74034B3F77061500B9728E ();
// 0x00000256 System.Void LTDescrOptional::set_onUpdateFloat(System.Action`1<System.Single>)
extern void LTDescrOptional_set_onUpdateFloat_m2396684827436DC9B10053C9AC2C10A53DA8AECD ();
// 0x00000257 System.Action`2<System.Single,System.Single> LTDescrOptional::get_onUpdateFloatRatio()
extern void LTDescrOptional_get_onUpdateFloatRatio_m6B22F53C31109E96E7E48E43A9B44651838F7945 ();
// 0x00000258 System.Void LTDescrOptional::set_onUpdateFloatRatio(System.Action`2<System.Single,System.Single>)
extern void LTDescrOptional_set_onUpdateFloatRatio_m94CE022ED233DA9D79AA3F18C8E6054C2D8E6573 ();
// 0x00000259 System.Action`2<System.Single,System.Object> LTDescrOptional::get_onUpdateFloatObject()
extern void LTDescrOptional_get_onUpdateFloatObject_mBCB3D7865F5F9ABAC4C5A03DD91241BF2B1AB601 ();
// 0x0000025A System.Void LTDescrOptional::set_onUpdateFloatObject(System.Action`2<System.Single,System.Object>)
extern void LTDescrOptional_set_onUpdateFloatObject_m81041094C42F5FF531D4C26135A74444B4EAC4EC ();
// 0x0000025B System.Action`1<UnityEngine.Vector2> LTDescrOptional::get_onUpdateVector2()
extern void LTDescrOptional_get_onUpdateVector2_m310F0582519C2897287E219E5AE5EB4A034F3FD1 ();
// 0x0000025C System.Void LTDescrOptional::set_onUpdateVector2(System.Action`1<UnityEngine.Vector2>)
extern void LTDescrOptional_set_onUpdateVector2_m843CED0BD5F49E0B9CADEE1E0D5C1F418A77F5BB ();
// 0x0000025D System.Action`1<UnityEngine.Vector3> LTDescrOptional::get_onUpdateVector3()
extern void LTDescrOptional_get_onUpdateVector3_m1EAF8967B52F428EF0BB00792489D5F9DBC2C959 ();
// 0x0000025E System.Void LTDescrOptional::set_onUpdateVector3(System.Action`1<UnityEngine.Vector3>)
extern void LTDescrOptional_set_onUpdateVector3_m8AB5DC8AFCF8FBA6132342A8E662D0BAD10B22E0 ();
// 0x0000025F System.Action`2<UnityEngine.Vector3,System.Object> LTDescrOptional::get_onUpdateVector3Object()
extern void LTDescrOptional_get_onUpdateVector3Object_m1BC1054A2FBE8FABCE6F6DDB453A60685C8B86B8 ();
// 0x00000260 System.Void LTDescrOptional::set_onUpdateVector3Object(System.Action`2<UnityEngine.Vector3,System.Object>)
extern void LTDescrOptional_set_onUpdateVector3Object_m98BFC4AA1096210EC4D9374E8AD0DC61E927FBEB ();
// 0x00000261 System.Action`1<UnityEngine.Color> LTDescrOptional::get_onUpdateColor()
extern void LTDescrOptional_get_onUpdateColor_m2ED4389F5DCBCE5D9157F6DB12EEFE229D279006 ();
// 0x00000262 System.Void LTDescrOptional::set_onUpdateColor(System.Action`1<UnityEngine.Color>)
extern void LTDescrOptional_set_onUpdateColor_m71FBD24255ADA5C0CBA8E473AFB71F18D4A958CE ();
// 0x00000263 System.Action`2<UnityEngine.Color,System.Object> LTDescrOptional::get_onUpdateColorObject()
extern void LTDescrOptional_get_onUpdateColorObject_m8942669D7FE94243BCF809D11BAF52E289CF3065 ();
// 0x00000264 System.Void LTDescrOptional::set_onUpdateColorObject(System.Action`2<UnityEngine.Color,System.Object>)
extern void LTDescrOptional_set_onUpdateColorObject_m9556E2C730376300992A2388E38161C59B7B87DD ();
// 0x00000265 System.Action LTDescrOptional::get_onComplete()
extern void LTDescrOptional_get_onComplete_mFF2D0179EF228E49F57BE663851C6BFDB45044B6 ();
// 0x00000266 System.Void LTDescrOptional::set_onComplete(System.Action)
extern void LTDescrOptional_set_onComplete_m5337F3EF337F442B0CBE09C1721EAC212FE306BC ();
// 0x00000267 System.Action`1<System.Object> LTDescrOptional::get_onCompleteObject()
extern void LTDescrOptional_get_onCompleteObject_m6CC2CEEEEE1FA541AEC4D71580593EB4A603CDF0 ();
// 0x00000268 System.Void LTDescrOptional::set_onCompleteObject(System.Action`1<System.Object>)
extern void LTDescrOptional_set_onCompleteObject_mB19BF5653BE9F370A1AE846055F9B546C4D74D7B ();
// 0x00000269 System.Object LTDescrOptional::get_onCompleteParam()
extern void LTDescrOptional_get_onCompleteParam_m428D156539D89C6007E0BA22E368A7B8252FD512 ();
// 0x0000026A System.Void LTDescrOptional::set_onCompleteParam(System.Object)
extern void LTDescrOptional_set_onCompleteParam_mEDC4259A922981661AEAF55E9B845B4514BF0FBD ();
// 0x0000026B System.Object LTDescrOptional::get_onUpdateParam()
extern void LTDescrOptional_get_onUpdateParam_mBFC0491E0705696A41C4DCB044F7ED0D70AB1BEC ();
// 0x0000026C System.Void LTDescrOptional::set_onUpdateParam(System.Object)
extern void LTDescrOptional_set_onUpdateParam_m795BC6412556889BC4503D1E571DC2FF722643E0 ();
// 0x0000026D System.Action LTDescrOptional::get_onStart()
extern void LTDescrOptional_get_onStart_mC70E09328F589CFB5965B061923AD4FEB6D4B2C5 ();
// 0x0000026E System.Void LTDescrOptional::set_onStart(System.Action)
extern void LTDescrOptional_set_onStart_m529CC6B3EE612FF8C1F736BC185A090C1F04C2FA ();
// 0x0000026F System.Void LTDescrOptional::reset()
extern void LTDescrOptional_reset_mAE8172FF1FFB9054153167751ACB4A3756853159 ();
// 0x00000270 System.Void LTDescrOptional::callOnUpdate(System.Single,System.Single)
extern void LTDescrOptional_callOnUpdate_m10CF5AF6AE9BDA730C97538C3E164DF2961274AB ();
// 0x00000271 System.Void LTDescrOptional::.ctor()
extern void LTDescrOptional__ctor_m50C4F69D364DD967ACF09DE82F78E20D3B15AB0C ();
// 0x00000272 System.Int32 LTSeq::get_id()
extern void LTSeq_get_id_mF7BDB8790E2BC1B542C0F5A4260AADDF48795D3F ();
// 0x00000273 System.Void LTSeq::reset()
extern void LTSeq_reset_m342DA923D9419644BF3A4793A0606AF0A5C133B1 ();
// 0x00000274 System.Void LTSeq::init(System.UInt32,System.UInt32)
extern void LTSeq_init_m5315CCEC0CACF7895E5B1D5F617F589D41741A1B ();
// 0x00000275 LTSeq LTSeq::addOn()
extern void LTSeq_addOn_mD55953C7985C4A08B02742D62A7E010367F68C2A ();
// 0x00000276 System.Single LTSeq::addPreviousDelays()
extern void LTSeq_addPreviousDelays_mB6198AF3CABBB0D35D0AC220991530A5DD2155EB ();
// 0x00000277 LTSeq LTSeq::append(System.Single)
extern void LTSeq_append_m0B12C0E6C61C2780F758FD94D903C716E72F68C0 ();
// 0x00000278 LTSeq LTSeq::append(System.Action)
extern void LTSeq_append_mC0F7F9A6570CBE8BAA7112CF7217D6E164A413F4 ();
// 0x00000279 LTSeq LTSeq::append(System.Action`1<System.Object>,System.Object)
extern void LTSeq_append_mB7C3B091C37DBD0042777E6F099687007BAF5D17 ();
// 0x0000027A LTSeq LTSeq::append(UnityEngine.GameObject,System.Action)
extern void LTSeq_append_mDF7F824A764228060EFF8BBA69F903D1795D499F ();
// 0x0000027B LTSeq LTSeq::append(UnityEngine.GameObject,System.Action`1<System.Object>,System.Object)
extern void LTSeq_append_mD32A66D2490D4B839B6AAA8B0B577895941E61EE ();
// 0x0000027C LTSeq LTSeq::append(LTDescr)
extern void LTSeq_append_m76F5F63AB647BB18EF33454A98F105D75E9E9B80 ();
// 0x0000027D LTSeq LTSeq::insert(LTDescr)
extern void LTSeq_insert_m2B024BE54107A8CAF3AE6B271FFB3CE86441DD9A ();
// 0x0000027E LTSeq LTSeq::setScale(System.Single)
extern void LTSeq_setScale_m896BA18CF4C685EF34A26175CED8C6906043568E ();
// 0x0000027F System.Void LTSeq::setScaleRecursive(LTSeq,System.Single,System.Int32)
extern void LTSeq_setScaleRecursive_m78C901915A35EB641BDBD9FB639199463C49639B ();
// 0x00000280 LTSeq LTSeq::reverse()
extern void LTSeq_reverse_m777F21FCED971A010923D62CD068626ECAAD9DC5 ();
// 0x00000281 System.Void LTSeq::.ctor()
extern void LTSeq__ctor_mB6D4797A9F32BE64240BC1D4FB9A0A8B6D824E2C ();
// 0x00000282 System.Void LeanAudioStream::.ctor(System.Single[])
extern void LeanAudioStream__ctor_m531CEA7A157EA4A83F540265CA7160894005CFC1 ();
// 0x00000283 System.Void LeanAudioStream::OnAudioRead(System.Single[])
extern void LeanAudioStream_OnAudioRead_m97338215EAA44081B43443314379D323FA30C308 ();
// 0x00000284 System.Void LeanAudioStream::OnAudioSetPosition(System.Int32)
extern void LeanAudioStream_OnAudioSetPosition_m5B845BECDEBA740CB23D0725D335BBAE1B2FA4B4 ();
// 0x00000285 LeanAudioOptions LeanAudio::options()
extern void LeanAudio_options_mB74094BFE3791F34AD4614CCB425AEE5228AAC83 ();
// 0x00000286 LeanAudioStream LeanAudio::createAudioStream(UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,LeanAudioOptions)
extern void LeanAudio_createAudioStream_m86F3E14A3FF84DC88892A8B7F16D92224BD15D8A ();
// 0x00000287 UnityEngine.AudioClip LeanAudio::createAudio(UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,LeanAudioOptions)
extern void LeanAudio_createAudio_mB693792EC007530C3DCA798A57ECCD7BFE67D426 ();
// 0x00000288 System.Int32 LeanAudio::createAudioWave(UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,LeanAudioOptions)
extern void LeanAudio_createAudioWave_m3D9DFFC3319332471839B2D76C5266629AAEED04 ();
// 0x00000289 UnityEngine.AudioClip LeanAudio::createAudioFromWave(System.Int32,LeanAudioOptions)
extern void LeanAudio_createAudioFromWave_m8FFF705E1832195B4E0EAA977C9F4CF61FA33058 ();
// 0x0000028A System.Void LeanAudio::OnAudioSetPosition(System.Int32)
extern void LeanAudio_OnAudioSetPosition_mFF60A18D3230037BCEB08585DDDA7B27F8B71C4F ();
// 0x0000028B UnityEngine.AudioClip LeanAudio::generateAudioFromCurve(UnityEngine.AnimationCurve,System.Int32)
extern void LeanAudio_generateAudioFromCurve_m7E8EFFD1CC78766F82416FD21A47889DC8D31923 ();
// 0x0000028C UnityEngine.AudioSource LeanAudio::play(UnityEngine.AudioClip,System.Single)
extern void LeanAudio_play_mB81F9043651130080D3F0CE28033BED5EB39E0CB ();
// 0x0000028D UnityEngine.AudioSource LeanAudio::play(UnityEngine.AudioClip)
extern void LeanAudio_play_m32A1254BE2452ED5367119EFFF686D9595E4D57B ();
// 0x0000028E UnityEngine.AudioSource LeanAudio::play(UnityEngine.AudioClip,UnityEngine.Vector3)
extern void LeanAudio_play_m3A30E48BBA88B9E8881968633686928B854544F9 ();
// 0x0000028F UnityEngine.AudioSource LeanAudio::play(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern void LeanAudio_play_mCD89719473F04DF4A0CBDE0FACBBD831A8DD2ACF ();
// 0x00000290 UnityEngine.AudioSource LeanAudio::playClipAt(UnityEngine.AudioClip,UnityEngine.Vector3)
extern void LeanAudio_playClipAt_mC5C4CD778BDCF8D897740C2B3EA8415C631BCF3F ();
// 0x00000291 System.Void LeanAudio::printOutAudioClip(UnityEngine.AudioClip,UnityEngine.AnimationCurve&,System.Single)
extern void LeanAudio_printOutAudioClip_m2783A6C26F9A85D837099B9A4010A3523C9BDA90 ();
// 0x00000292 System.Void LeanAudio::.ctor()
extern void LeanAudio__ctor_m88410F3D66A6EDDF53D657CF52DBBCBD40E7E8D8 ();
// 0x00000293 System.Void LeanAudio::.cctor()
extern void LeanAudio__cctor_m5647818341FC7B5272CFCDD6966CA0407EC9D131 ();
// 0x00000294 System.Void LeanAudioOptions::.ctor()
extern void LeanAudioOptions__ctor_mC4A118B38AA350E5502615E56400F51A43B3B60B ();
// 0x00000295 LeanAudioOptions LeanAudioOptions::setFrequency(System.Int32)
extern void LeanAudioOptions_setFrequency_m2534EC3E99ECCB13667B5703931133E97CCB6459 ();
// 0x00000296 LeanAudioOptions LeanAudioOptions::setVibrato(UnityEngine.Vector3[])
extern void LeanAudioOptions_setVibrato_m493C38946EC65F6B1C2E811A1C232CF8C40E8876 ();
// 0x00000297 LeanAudioOptions LeanAudioOptions::setWaveSine()
extern void LeanAudioOptions_setWaveSine_mDADF8446B8A6BB14326F15C6FD010E43A6B5A84B ();
// 0x00000298 LeanAudioOptions LeanAudioOptions::setWaveSquare()
extern void LeanAudioOptions_setWaveSquare_m694508FC09511CF1BF371B7FA456BE92D503322F ();
// 0x00000299 LeanAudioOptions LeanAudioOptions::setWaveSawtooth()
extern void LeanAudioOptions_setWaveSawtooth_mC60BB12911D96EA79544EBF3780588CCB0858EF7 ();
// 0x0000029A LeanAudioOptions LeanAudioOptions::setWaveNoise()
extern void LeanAudioOptions_setWaveNoise_m92B623A5316643EACFC1CCEF761D463EA7F53C15 ();
// 0x0000029B LeanAudioOptions LeanAudioOptions::setWaveStyle(LeanAudioOptions_LeanAudioWaveStyle)
extern void LeanAudioOptions_setWaveStyle_mD446529C32C1D2809CE0BD19558B9B9A1CCC7381 ();
// 0x0000029C LeanAudioOptions LeanAudioOptions::setWaveNoiseScale(System.Single)
extern void LeanAudioOptions_setWaveNoiseScale_mFE6B538697857F28C0077DC7C7F3C34CC00DD5B3 ();
// 0x0000029D LeanAudioOptions LeanAudioOptions::setWaveNoiseInfluence(System.Single)
extern void LeanAudioOptions_setWaveNoiseInfluence_m321FC18AC3DB87F3F22658CB134088F6AE8B0A5D ();
// 0x0000029E System.Single LeanSmooth::damp(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single)
extern void LeanSmooth_damp_mC64F76E7BEB749AE0C6620500B8F7A124C8B2281 ();
// 0x0000029F UnityEngine.Vector3 LeanSmooth::damp(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single,System.Single,System.Single)
extern void LeanSmooth_damp_m2AE19609C34606AAC949DD5737F7C905D6D1FAEC ();
// 0x000002A0 UnityEngine.Color LeanSmooth::damp(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color&,System.Single,System.Single,System.Single)
extern void LeanSmooth_damp_m7155F328C1DE25635DFE88A26F308E83D559C084 ();
// 0x000002A1 System.Single LeanSmooth::spring(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_spring_m13E4D3125D99326FA237E391E6AFC87321D643A7 ();
// 0x000002A2 UnityEngine.Vector3 LeanSmooth::spring(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_spring_mB9D231C3690876EE5299B0261A5B101C0299E3C6 ();
// 0x000002A3 UnityEngine.Color LeanSmooth::spring(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color&,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_spring_m93FC4C89467835E4367BE781881F47A7E7416B36 ();
// 0x000002A4 System.Single LeanSmooth::linear(System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_linear_mE0319D93C18D8C6230B69BB6914BC2171D9991FC ();
// 0x000002A5 UnityEngine.Vector3 LeanSmooth::linear(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanSmooth_linear_mE1E0D6A877174839F0D805E0E23AB7B9869F99BE ();
// 0x000002A6 UnityEngine.Color LeanSmooth::linear(UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanSmooth_linear_m5F142DE9C4019A6163ED38F547F8E26D6FF4C1DF ();
// 0x000002A7 System.Single LeanSmooth::bounceOut(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_bounceOut_mAF91D979E058FDC80D6E7BB35FDBDF5D1C584930 ();
// 0x000002A8 UnityEngine.Vector3 LeanSmooth::bounceOut(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_bounceOut_m213E8992275BA3F7E5DF6DCB584C2E4EAE055163 ();
// 0x000002A9 UnityEngine.Color LeanSmooth::bounceOut(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color&,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanSmooth_bounceOut_mC0BF320EBCFCF7EAD5CF449073C707968FC27F1C ();
// 0x000002AA System.Void LeanSmooth::.ctor()
extern void LeanSmooth__ctor_m14E227157D0426A2F47C06DE30CB17982B72BF66 ();
// 0x000002AB System.Void LeanTester::Start()
extern void LeanTester_Start_m9A9E920B5F046E3372B0366E0BC9DFC84A919F58 ();
// 0x000002AC System.Collections.IEnumerator LeanTester::timeoutCheck()
extern void LeanTester_timeoutCheck_mC313EB95692CB44EBD8BA1A87AA815BD5416B150 ();
// 0x000002AD System.Void LeanTester::.ctor()
extern void LeanTester__ctor_mC69E2DD7669788DEA00909ED2351EAAA6CF317C3 ();
// 0x000002AE System.Void LeanTest::debug(System.String,System.Boolean,System.String)
extern void LeanTest_debug_m6C2E9AD4476177558EF713F252CB2FCEDB14DEE3 ();
// 0x000002AF System.Void LeanTest::expect(System.Boolean,System.String,System.String)
extern void LeanTest_expect_mD39E3D849F6BB08F68658A3EA543DABC2ECC9B77 ();
// 0x000002B0 System.String LeanTest::padRight(System.Int32)
extern void LeanTest_padRight_mD8E69C06CFE450F91AE8B44FD304BAECDCB25F91 ();
// 0x000002B1 System.Single LeanTest::printOutLength(System.String)
extern void LeanTest_printOutLength_m340E6C2B8CE237DD562D42857AA79E56724407C6 ();
// 0x000002B2 System.String LeanTest::formatBC(System.String,System.String)
extern void LeanTest_formatBC_m61FCBB26428987CBC8EAD380847104E662D747CE ();
// 0x000002B3 System.String LeanTest::formatB(System.String)
extern void LeanTest_formatB_m3A16FF8B95ABDFCFEAF5C5B3591F5D84867A7954 ();
// 0x000002B4 System.String LeanTest::formatC(System.String,System.String)
extern void LeanTest_formatC_mF76064C060FB981E2535F8A487393A849D5DD292 ();
// 0x000002B5 System.Void LeanTest::overview()
extern void LeanTest_overview_m54D7E1E1324A5D63C8834BCBDCD9627490C9FC37 ();
// 0x000002B6 System.Void LeanTest::.ctor()
extern void LeanTest__ctor_m08E8F412E4EBF00D54D3AB5D8EE7E0015C3A04EF ();
// 0x000002B7 System.Void LeanTest::.cctor()
extern void LeanTest__cctor_m589610FAD222BE5A0012463E6AF9D88514317EBF ();
// 0x000002B8 System.Void LeanTween::init()
extern void LeanTween_init_m742C83A1FC52BA7DC1A45D77418E2A20C6EA3FA4 ();
// 0x000002B9 System.Int32 LeanTween::get_maxSearch()
extern void LeanTween_get_maxSearch_mCBB6EBACEB7810A13B6BFE5E46FF93D9B576F8DB ();
// 0x000002BA System.Int32 LeanTween::get_maxSimulataneousTweens()
extern void LeanTween_get_maxSimulataneousTweens_m29E755842BECB453B666A30118106AC8FA3ECDA9 ();
// 0x000002BB System.Int32 LeanTween::get_tweensRunning()
extern void LeanTween_get_tweensRunning_m7DAE5C7327AF47F20CEAE9B5D7BE7460C53CDC46 ();
// 0x000002BC System.Void LeanTween::init(System.Int32)
extern void LeanTween_init_mF9EBB839A07AFBCAEBB72C052B6F8EC4CD48D7BA ();
// 0x000002BD System.Void LeanTween::init(System.Int32,System.Int32)
extern void LeanTween_init_m876B8AA3523AB02C0246A87E9E37F5388FFFB35B ();
// 0x000002BE System.Void LeanTween::reset()
extern void LeanTween_reset_m239840212FEA6441ED109FC1E312A2BBE2534AF9 ();
// 0x000002BF System.Void LeanTween::Update()
extern void LeanTween_Update_mF15DAA93C9C32D4BA1645DB4B03AC67F6C3D8F8D ();
// 0x000002C0 System.Void LeanTween::onLevelWasLoaded54(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void LeanTween_onLevelWasLoaded54_m30E5953BBD22DF6BFAD61B4B62081D26ED1E0046 ();
// 0x000002C1 System.Void LeanTween::internalOnLevelWasLoaded(System.Int32)
extern void LeanTween_internalOnLevelWasLoaded_mF01B04BD6D4CCB7D5C896B0C9381BACE2A6996F1 ();
// 0x000002C2 System.Void LeanTween::update()
extern void LeanTween_update_mE2EEEF001DE108CFA101BB549033B6022655C0E8 ();
// 0x000002C3 System.Void LeanTween::removeTween(System.Int32,System.Int32)
extern void LeanTween_removeTween_m47036DA44D4E0AF9443A2A459B10F51C039BC835 ();
// 0x000002C4 System.Void LeanTween::removeTween(System.Int32)
extern void LeanTween_removeTween_m5183FCF6228B6149D948D3175B1906EDE9EB5A2F ();
// 0x000002C5 UnityEngine.Vector3[] LeanTween::add(UnityEngine.Vector3[],UnityEngine.Vector3)
extern void LeanTween_add_m94B30FC92F9702D1C000E53F0481E2547BCEA152 ();
// 0x000002C6 System.Single LeanTween::closestRot(System.Single,System.Single)
extern void LeanTween_closestRot_m425E4814D09E1E33641F79E253706B42A8677DCA ();
// 0x000002C7 System.Void LeanTween::cancelAll()
extern void LeanTween_cancelAll_m57047A3005FF66C0137640192B7BDB3DA6CD2F27 ();
// 0x000002C8 System.Void LeanTween::cancelAll(System.Boolean)
extern void LeanTween_cancelAll_mDFE02D9737D5CFFBBBAB9B7ADEFF4EC8A2EC6BA7 ();
// 0x000002C9 System.Void LeanTween::cancel(UnityEngine.GameObject)
extern void LeanTween_cancel_m5FAAE217AED5E47A774964AD4B49CBF06BB4CFAE ();
// 0x000002CA System.Void LeanTween::cancel(UnityEngine.GameObject,System.Boolean)
extern void LeanTween_cancel_m8C912896B48485DEF6C233BDE9515DD9BCB37F4D ();
// 0x000002CB System.Void LeanTween::cancel(UnityEngine.RectTransform)
extern void LeanTween_cancel_m83465350000117665934A8D6556E5759C60E4BA3 ();
// 0x000002CC System.Void LeanTween::cancel(UnityEngine.GameObject,System.Int32,System.Boolean)
extern void LeanTween_cancel_mC1789BEE750E0F49BBB9A5427837EAAED0F6B362 ();
// 0x000002CD System.Void LeanTween::cancel(LTRect,System.Int32)
extern void LeanTween_cancel_mF06045394FD8DB00302C26CBB06F791FC20BE002 ();
// 0x000002CE System.Void LeanTween::cancel(System.Int32)
extern void LeanTween_cancel_mBDB0EE47F9FEA5CDC99DAE5AB071A317D1646E73 ();
// 0x000002CF System.Void LeanTween::cancel(System.Int32,System.Boolean)
extern void LeanTween_cancel_mFBB7196E53B68C2337C13F0AA1F6F96A9E652995 ();
// 0x000002D0 LTDescr LeanTween::descr(System.Int32)
extern void LeanTween_descr_m673151C22BCD383E070208DC886A48EA5682354E ();
// 0x000002D1 LTDescr LeanTween::description(System.Int32)
extern void LeanTween_description_m614B276C8B85841618BC11CC53EC3F9F998B12CD ();
// 0x000002D2 LTDescr[] LeanTween::descriptions(UnityEngine.GameObject)
extern void LeanTween_descriptions_m15F1EE30DA28ED509BED8B1208D21C6A305D0F7C ();
// 0x000002D3 System.Void LeanTween::pause(UnityEngine.GameObject,System.Int32)
extern void LeanTween_pause_m544CC2BEF9E44BF1FA9C2B02FC8C8D8D840303F5 ();
// 0x000002D4 System.Void LeanTween::pause(System.Int32)
extern void LeanTween_pause_mD334E7016A1280D8612923E19FE0EAC3A99213C7 ();
// 0x000002D5 System.Void LeanTween::pause(UnityEngine.GameObject)
extern void LeanTween_pause_m34D1984074FEAD898CB5A732E153230C05BC0F8A ();
// 0x000002D6 System.Void LeanTween::pauseAll()
extern void LeanTween_pauseAll_m9A08262908FA3B89F629A53310A615960E25681F ();
// 0x000002D7 System.Void LeanTween::resumeAll()
extern void LeanTween_resumeAll_m0CD4B24182F95AB48670CAA51B31356DA0298F02 ();
// 0x000002D8 System.Void LeanTween::resume(UnityEngine.GameObject,System.Int32)
extern void LeanTween_resume_mA7A5417B25090825B935A5A53BBCBC0DD2CA60FA ();
// 0x000002D9 System.Void LeanTween::resume(System.Int32)
extern void LeanTween_resume_m9A7648F170B3EF8AD8CF448578A1423E5AC57DC0 ();
// 0x000002DA System.Void LeanTween::resume(UnityEngine.GameObject)
extern void LeanTween_resume_mED2ED2E734874013D07AB1CA160FA30098BB93CA ();
// 0x000002DB System.Boolean LeanTween::isPaused(UnityEngine.GameObject)
extern void LeanTween_isPaused_mEA3B9A77082F25532A58E01A3B16F0EB6AD3C9D1 ();
// 0x000002DC System.Boolean LeanTween::isPaused(UnityEngine.RectTransform)
extern void LeanTween_isPaused_mF0B834CE3C9278DF00216045E11ACE5EE9D66354 ();
// 0x000002DD System.Boolean LeanTween::isPaused(System.Int32)
extern void LeanTween_isPaused_mB1A22DBCB84A1FAD2A2D5E0B7F2307A77D1C630A ();
// 0x000002DE System.Boolean LeanTween::isTweening(UnityEngine.GameObject)
extern void LeanTween_isTweening_m8565F710EC2593893864BDC02B0F286E58D77928 ();
// 0x000002DF System.Boolean LeanTween::isTweening(UnityEngine.RectTransform)
extern void LeanTween_isTweening_mD5E04C094ED73605C74804ADCF9B13FADDAC9470 ();
// 0x000002E0 System.Boolean LeanTween::isTweening(System.Int32)
extern void LeanTween_isTweening_m7300B17390DAF00D71AF9837EFAE524A0F067607 ();
// 0x000002E1 System.Boolean LeanTween::isTweening(LTRect)
extern void LeanTween_isTweening_mBEB3331D582A3CF1DFC989B9538934CBC8B29F39 ();
// 0x000002E2 System.Void LeanTween::drawBezierPath(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Transform)
extern void LeanTween_drawBezierPath_mFC7D2D0AF1632D71D25BF9B390285DAFB57A1FBA ();
// 0x000002E3 System.Object LeanTween::logError(System.String)
extern void LeanTween_logError_m37F7E5A9E9028A282D3FB22858AE9E751B5B7E17 ();
// 0x000002E4 LTDescr LeanTween::options(LTDescr)
extern void LeanTween_options_mA6EBBE759321A9ED63E09FCA3E19257A5732399C ();
// 0x000002E5 LTDescr LeanTween::options()
extern void LeanTween_options_m96D5FD6CD10BC38781B78A7ABBAFD40FD6896C10 ();
// 0x000002E6 UnityEngine.GameObject LeanTween::get_tweenEmpty()
extern void LeanTween_get_tweenEmpty_mAD2C44A742A9DBD681D325C9EC5708FB4B2DDAD9 ();
// 0x000002E7 LTDescr LeanTween::pushNewTween(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,LTDescr)
extern void LeanTween_pushNewTween_m5B8A1F2059CF7A5AA5A994254FB6FF2325FDB5E8 ();
// 0x000002E8 LTDescr LeanTween::play(UnityEngine.RectTransform,UnityEngine.Sprite[])
extern void LeanTween_play_mCEA864283E853E9E7086662D30B2B35229601255 ();
// 0x000002E9 LTSeq LeanTween::sequence(System.Boolean)
extern void LeanTween_sequence_mF0E50A4160E6E03ED0B1B6A57B25552E195F1E60 ();
// 0x000002EA LTDescr LeanTween::alpha(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_alpha_m03563AAF9FE673DAEAD06EC87B702F6633009459 ();
// 0x000002EB LTDescr LeanTween::alpha(LTRect,System.Single,System.Single)
extern void LeanTween_alpha_m8BD4F21C3DA8E6057BA72EB4500336A8AD2D23D0 ();
// 0x000002EC LTDescr LeanTween::textAlpha(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_textAlpha_m56966F76D08E1D2F771F68FAC336CD26A3789C4B ();
// 0x000002ED LTDescr LeanTween::alphaText(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_alphaText_mDF874DA494D7B483C1AD62D2459795583C87EA50 ();
// 0x000002EE LTDescr LeanTween::alphaCanvas(UnityEngine.CanvasGroup,System.Single,System.Single)
extern void LeanTween_alphaCanvas_mD7760E3B438F098F8E52C0E19BE66AB042700482 ();
// 0x000002EF LTDescr LeanTween::alphaVertex(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_alphaVertex_m9C46F3D3D5156962E5E6DAFD6D3B91D87A86CD77 ();
// 0x000002F0 LTDescr LeanTween::color(UnityEngine.GameObject,UnityEngine.Color,System.Single)
extern void LeanTween_color_m37AE0DBE5DBE82AB6CCF14D76DF1EFC01D95B69F ();
// 0x000002F1 LTDescr LeanTween::textColor(UnityEngine.RectTransform,UnityEngine.Color,System.Single)
extern void LeanTween_textColor_mEF9FEF414B74FE47A423CF17CA79712A0D1FB658 ();
// 0x000002F2 LTDescr LeanTween::colorText(UnityEngine.RectTransform,UnityEngine.Color,System.Single)
extern void LeanTween_colorText_m15B3D867D6A42D0D7770B97CB2E7E200F0D661B2 ();
// 0x000002F3 LTDescr LeanTween::delayedCall(System.Single,System.Action)
extern void LeanTween_delayedCall_m04427AEA4FBB41C4A3CCB898D64BB4AC740BF03C ();
// 0x000002F4 LTDescr LeanTween::delayedCall(System.Single,System.Action`1<System.Object>)
extern void LeanTween_delayedCall_m899A2AE8C3C19487CFA5AB41977F67D01F9DD4E6 ();
// 0x000002F5 LTDescr LeanTween::delayedCall(UnityEngine.GameObject,System.Single,System.Action)
extern void LeanTween_delayedCall_mA05A41E42F1921AB3867C1AA9B43100FC5F57F2E ();
// 0x000002F6 LTDescr LeanTween::delayedCall(UnityEngine.GameObject,System.Single,System.Action`1<System.Object>)
extern void LeanTween_delayedCall_m7A1A97BE2037CF29C9C68D5DC41476904215AAA0 ();
// 0x000002F7 LTDescr LeanTween::destroyAfter(LTRect,System.Single)
extern void LeanTween_destroyAfter_m7641087D50C872EB2A5BEC69287673C0AA63A6C1 ();
// 0x000002F8 LTDescr LeanTween::move(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTween_move_m1191EC5B42C368CBBAA12E7EF8EFC8378980F2A5 ();
// 0x000002F9 LTDescr LeanTween::move(UnityEngine.GameObject,UnityEngine.Vector2,System.Single)
extern void LeanTween_move_m0AFE5CA5D827F49074E9ACA6C73E2E6D6E78E79E ();
// 0x000002FA LTDescr LeanTween::move(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTween_move_m69E063649877CD64A4755003DB27E1608A41C08B ();
// 0x000002FB LTDescr LeanTween::move(UnityEngine.GameObject,LTBezierPath,System.Single)
extern void LeanTween_move_mEDCFBFB4014A966F52A3DC08FE2E952DCDC53E95 ();
// 0x000002FC LTDescr LeanTween::move(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTween_move_m8058A34EDCE00BFEE3336DE5698A3927B796A796 ();
// 0x000002FD LTDescr LeanTween::moveSpline(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTween_moveSpline_mA1A07399D0522C220E7C062CCA41CA1A5F07781B ();
// 0x000002FE LTDescr LeanTween::moveSpline(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTween_moveSpline_mBC72A4210E5EBE1678828E39AA926A53D72C2EEC ();
// 0x000002FF LTDescr LeanTween::moveSplineLocal(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTween_moveSplineLocal_mC1BF1287479F66BEADB1E325D9DED97A6E69E67A ();
// 0x00000300 LTDescr LeanTween::move(LTRect,UnityEngine.Vector2,System.Single)
extern void LeanTween_move_m3ABE07EDBD7AD98100B8545E5AAA864F03A4F6A4 ();
// 0x00000301 LTDescr LeanTween::moveMargin(LTRect,UnityEngine.Vector2,System.Single)
extern void LeanTween_moveMargin_mCFF341DFEC796404C5DC2979392988C15AA31620 ();
// 0x00000302 LTDescr LeanTween::moveX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveX_mC528E208B5A163AA4C5400294A2D03C8E5448DED ();
// 0x00000303 LTDescr LeanTween::moveY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveY_m9E9F3F2DD9FC9791FB42E952BC71A32858E9D894 ();
// 0x00000304 LTDescr LeanTween::moveZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveZ_mA8D3635AAD59B698E00B6AD6981BE2B8E2A381D4 ();
// 0x00000305 LTDescr LeanTween::moveLocal(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTween_moveLocal_m4628772CED24062B5B96C041E6C3FE3B64DE8252 ();
// 0x00000306 LTDescr LeanTween::moveLocal(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTween_moveLocal_m6B50D6459C312416C631F3C391C76022ED19C5D8 ();
// 0x00000307 LTDescr LeanTween::moveLocalX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveLocalX_mA9353C4CDB6A49FC6F7B9568E70D4D3C210033A1 ();
// 0x00000308 LTDescr LeanTween::moveLocalY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveLocalY_m605F33473A6318A1BC2719D5032D048B2206ABA9 ();
// 0x00000309 LTDescr LeanTween::moveLocalZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_moveLocalZ_m0B7FEA9EDE5CDEB065F4187E11F174465D7E8EF6 ();
// 0x0000030A LTDescr LeanTween::moveLocal(UnityEngine.GameObject,LTBezierPath,System.Single)
extern void LeanTween_moveLocal_m52BCB9FDEEFBB74988883648CA96FBA390F991A1 ();
// 0x0000030B LTDescr LeanTween::moveLocal(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTween_moveLocal_mE6FF69270695A740FF6950ABB1C389BD87336445 ();
// 0x0000030C LTDescr LeanTween::move(UnityEngine.GameObject,UnityEngine.Transform,System.Single)
extern void LeanTween_move_mB930433417E02C8708DA33ABFDCB478397AA070B ();
// 0x0000030D LTDescr LeanTween::rotate(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTween_rotate_mA9190F8C72DBB9FF8A0998C936208C97D18236AE ();
// 0x0000030E LTDescr LeanTween::rotate(LTRect,System.Single,System.Single)
extern void LeanTween_rotate_m3D39319C8CA729B58884DED8AAB29903BAD76541 ();
// 0x0000030F LTDescr LeanTween::rotateLocal(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTween_rotateLocal_mB981D5E76B2EA75987333079E8EEEDE7F78740A7 ();
// 0x00000310 LTDescr LeanTween::rotateX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_rotateX_m39C9891518C637C4DFEC9C6E455DF5562AAB1050 ();
// 0x00000311 LTDescr LeanTween::rotateY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_rotateY_mF66A36114A32D24BFE92D94B7944838A19847B90 ();
// 0x00000312 LTDescr LeanTween::rotateZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_rotateZ_mDBBBFF6BFBE5B724FCE59D3A6EF40F377431C1C8 ();
// 0x00000313 LTDescr LeanTween::rotateAround(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTween_rotateAround_m061DF6B0D1F4FB9C9A635C2B5C8D5F0D0486DCF9 ();
// 0x00000314 LTDescr LeanTween::rotateAroundLocal(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTween_rotateAroundLocal_m8498C0D937E0D12B6EF6C43F5F1AA7F4FD721726 ();
// 0x00000315 LTDescr LeanTween::scale(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTween_scale_m4CE3CCD970CF75664CF7FCA1E6286F418FDE2A0F ();
// 0x00000316 LTDescr LeanTween::scale(LTRect,UnityEngine.Vector2,System.Single)
extern void LeanTween_scale_m209552C2E41F5D6B0B1525A25540FE40A33F4ADE ();
// 0x00000317 LTDescr LeanTween::scaleX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_scaleX_mAA4702337267BB96770EA128A25EB894E4C7E2AB ();
// 0x00000318 LTDescr LeanTween::scaleY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_scaleY_m664993B4D0F2296E8BE7BF8168D17A90EEEBB3D5 ();
// 0x00000319 LTDescr LeanTween::scaleZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTween_scaleZ_m4F475E490D515FAF13E36488615B3C9972EAC2F7 ();
// 0x0000031A LTDescr LeanTween::value(UnityEngine.GameObject,System.Single,System.Single,System.Single)
extern void LeanTween_value_m915028F98333F18A1326931DFAC8106B6301D23F ();
// 0x0000031B LTDescr LeanTween::value(System.Single,System.Single,System.Single)
extern void LeanTween_value_m589DDED951D44371EB0589C01EBB679977F47556 ();
// 0x0000031C LTDescr LeanTween::value(UnityEngine.GameObject,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void LeanTween_value_m4887473E05B0379914BD3795CA6697D274C54606 ();
// 0x0000031D LTDescr LeanTween::value(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LeanTween_value_m3EF8D28045541C4387176018135B82C81F1FD38E ();
// 0x0000031E LTDescr LeanTween::value(UnityEngine.GameObject,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanTween_value_m3F8D4D5F95CA382EA0D770B07EC74C270B7EC526 ();
// 0x0000031F LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`1<System.Single>,System.Single,System.Single,System.Single)
extern void LeanTween_value_m115667D1977F3E7BF517B9EA3EA3EE161A8D9D39 ();
// 0x00000320 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`2<System.Single,System.Single>,System.Single,System.Single,System.Single)
extern void LeanTween_value_m169AE41FDE8360E78E0B637CEA5A114E112BA45A ();
// 0x00000321 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`1<UnityEngine.Color>,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanTween_value_m1709E165F0E2491A98561574CE72092EF202C1F5 ();
// 0x00000322 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`2<UnityEngine.Color,System.Object>,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanTween_value_m931E3256745BE0E7AF60C79DEE155EC421C55EC6 ();
// 0x00000323 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`1<UnityEngine.Vector2>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void LeanTween_value_m4FEF278D52CEFC694E29730BC229D3C52F7070C5 ();
// 0x00000324 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`1<UnityEngine.Vector3>,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LeanTween_value_m0846E512D96A497507FAA08EC3747DA89CD6FB7D ();
// 0x00000325 LTDescr LeanTween::value(UnityEngine.GameObject,System.Action`2<System.Single,System.Object>,System.Single,System.Single,System.Single)
extern void LeanTween_value_m0E16AF4A36148D3831CBEF54C0DFD01CE08475DD ();
// 0x00000326 LTDescr LeanTween::delayedSound(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern void LeanTween_delayedSound_mD225DC785D5C479E00DF03A9406C5EF1A145446A ();
// 0x00000327 LTDescr LeanTween::delayedSound(UnityEngine.GameObject,UnityEngine.AudioClip,UnityEngine.Vector3,System.Single)
extern void LeanTween_delayedSound_m17C87E524E93E54A7AC33CDF95F5560831E0A6CA ();
// 0x00000328 LTDescr LeanTween::move(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTween_move_m9EDD3EF2EDA02B6524829C56199400A302CC7BD3 ();
// 0x00000329 LTDescr LeanTween::moveX(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_moveX_mAE925994CB0FAE76F0768A0595CCEEB5C440031B ();
// 0x0000032A LTDescr LeanTween::moveY(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_moveY_m7FC0800EAC2943A5DF5F0652CD254D40EBA15BD1 ();
// 0x0000032B LTDescr LeanTween::moveZ(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_moveZ_m3AF920F4CDB4AE4E359A577F223341EF73CA9F04 ();
// 0x0000032C LTDescr LeanTween::rotate(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_rotate_m15EB5A2DF0974570B758B596AF032A42A1EB5F6A ();
// 0x0000032D LTDescr LeanTween::rotate(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTween_rotate_m362D5FBBA0847D5E7A4C8D0037355D7192F10ABB ();
// 0x0000032E LTDescr LeanTween::rotateAround(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTween_rotateAround_m6149AA8E86FFC604EB792197923D69BA3BF46823 ();
// 0x0000032F LTDescr LeanTween::rotateAroundLocal(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTween_rotateAroundLocal_m20B862CE01E52B5C4533D2512D718EDA1437B57A ();
// 0x00000330 LTDescr LeanTween::scale(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTween_scale_m47628FC09C70C5B77089F8AA97B7682F849EC0A9 ();
// 0x00000331 LTDescr LeanTween::size(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single)
extern void LeanTween_size_mEC9D2E7E59F8EBFC0CA941CADD467CD1A5491B43 ();
// 0x00000332 LTDescr LeanTween::alpha(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTween_alpha_m640B7551150854D49EC0BA41B7CA866B7A6CBF6D ();
// 0x00000333 LTDescr LeanTween::color(UnityEngine.RectTransform,UnityEngine.Color,System.Single)
extern void LeanTween_color_mD14B3C4D1453119045FC7D74FB6746287E9BA037 ();
// 0x00000334 System.Single LeanTween::tweenOnCurve(LTDescr,System.Single)
extern void LeanTween_tweenOnCurve_mF41225AC36913842DA74788977F04689BB139920 ();
// 0x00000335 UnityEngine.Vector3 LeanTween::tweenOnCurveVector(LTDescr,System.Single)
extern void LeanTween_tweenOnCurveVector_m517A4B35EE61BAE6ADB0DF2D84863D280540DA59 ();
// 0x00000336 System.Single LeanTween::easeOutQuadOpt(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutQuadOpt_m3C04BFEED580755A923F99E89B4F023F62450868 ();
// 0x00000337 System.Single LeanTween::easeInQuadOpt(System.Single,System.Single,System.Single)
extern void LeanTween_easeInQuadOpt_mCBC13EA35ADC87B69182C6DDAB43904CE2649BC6 ();
// 0x00000338 System.Single LeanTween::easeInOutQuadOpt(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutQuadOpt_m430D10EAD4D0A1FEC9AAD1E33F67B477EE72FD31 ();
// 0x00000339 UnityEngine.Vector3 LeanTween::easeInOutQuadOpt(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LeanTween_easeInOutQuadOpt_m6496C1F3A68C113F8798961D6F0E357BAA178A34 ();
// 0x0000033A System.Single LeanTween::linear(System.Single,System.Single,System.Single)
extern void LeanTween_linear_mC1ED9F0ADBDA8939C75B3BC0914E50DC34CB4F6C ();
// 0x0000033B System.Single LeanTween::clerp(System.Single,System.Single,System.Single)
extern void LeanTween_clerp_mE62F3DDE55042FAFC1484ECAFC2243E82B86AE62 ();
// 0x0000033C System.Single LeanTween::spring(System.Single,System.Single,System.Single)
extern void LeanTween_spring_mF690BE574938A3A8F8EC8248642F31ACD7785F32 ();
// 0x0000033D System.Single LeanTween::easeInQuad(System.Single,System.Single,System.Single)
extern void LeanTween_easeInQuad_m4D2FD34AC16BDD31B2838E3A5D80916469833DF7 ();
// 0x0000033E System.Single LeanTween::easeOutQuad(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutQuad_mB929599F71CEDB7C7C8579B9C8BA97500166584F ();
// 0x0000033F System.Single LeanTween::easeInOutQuad(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutQuad_m584E55A5AF9BF428B143857479D7D790CA38745C ();
// 0x00000340 System.Single LeanTween::easeInOutQuadOpt2(System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutQuadOpt2_m6F3DE20B7BFF5E0F5B34C1246C40F9AF115F7799 ();
// 0x00000341 System.Single LeanTween::easeInCubic(System.Single,System.Single,System.Single)
extern void LeanTween_easeInCubic_mE4683158D3ECF4DB9FBBDF6187759886DB83F853 ();
// 0x00000342 System.Single LeanTween::easeOutCubic(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutCubic_m42408FA585BA84351F5CD74AD1A3ECED10FD8B91 ();
// 0x00000343 System.Single LeanTween::easeInOutCubic(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutCubic_mFB2A765901D685FA00D1C16E6133E53047B7FE71 ();
// 0x00000344 System.Single LeanTween::easeInQuart(System.Single,System.Single,System.Single)
extern void LeanTween_easeInQuart_m1C37EFBC471397640865AF91771EA21DA0ED492F ();
// 0x00000345 System.Single LeanTween::easeOutQuart(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutQuart_m1A24432DC39B7ADD6D87960129D2FDE0C6D03C12 ();
// 0x00000346 System.Single LeanTween::easeInOutQuart(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutQuart_mBB2A5F122AC70BF79993F1180AC9A0D77D9C139F ();
// 0x00000347 System.Single LeanTween::easeInQuint(System.Single,System.Single,System.Single)
extern void LeanTween_easeInQuint_m1104B28EFA33850E5CB9BDC867D4C67B4D90457B ();
// 0x00000348 System.Single LeanTween::easeOutQuint(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutQuint_m9900891B73DD3AC15F28155E1EF340B260C8C67F ();
// 0x00000349 System.Single LeanTween::easeInOutQuint(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutQuint_mC67EDFA9A9953B9AC7C073D7211D115B37BC7B41 ();
// 0x0000034A System.Single LeanTween::easeInSine(System.Single,System.Single,System.Single)
extern void LeanTween_easeInSine_m81F6144AC64B5E5A4ED990C48A36F5A3D275525C ();
// 0x0000034B System.Single LeanTween::easeOutSine(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutSine_mD6871A62B4F306845FD33F26B9B82E5CC43DDECC ();
// 0x0000034C System.Single LeanTween::easeInOutSine(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutSine_m74089B5D05EA9EA3FB89A14DC48F1D310939320F ();
// 0x0000034D System.Single LeanTween::easeInExpo(System.Single,System.Single,System.Single)
extern void LeanTween_easeInExpo_m3B47C6876FA3DDCDBA8E27516A9D5A92AB8A0F96 ();
// 0x0000034E System.Single LeanTween::easeOutExpo(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutExpo_m90B7C8EFAC973AFD5B0FBC48AC5277F3ED95BA4D ();
// 0x0000034F System.Single LeanTween::easeInOutExpo(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutExpo_m1818C9E2CA4DB499749B6E701C523FE50C57CEB7 ();
// 0x00000350 System.Single LeanTween::easeInCirc(System.Single,System.Single,System.Single)
extern void LeanTween_easeInCirc_m32983AB18E1A8F9632D5D3350FE72AAC235F9DF9 ();
// 0x00000351 System.Single LeanTween::easeOutCirc(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutCirc_m9C4FF7DD334929D7993925BBCBB8FD8D2D198134 ();
// 0x00000352 System.Single LeanTween::easeInOutCirc(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutCirc_m4AF54D1824BB627E59856E1EB3FEEE9801A24926 ();
// 0x00000353 System.Single LeanTween::easeInBounce(System.Single,System.Single,System.Single)
extern void LeanTween_easeInBounce_m4D853643882AA66C8BA82904BF06C8FD89172577 ();
// 0x00000354 System.Single LeanTween::easeOutBounce(System.Single,System.Single,System.Single)
extern void LeanTween_easeOutBounce_m1790E9DEA28BAE58ED7FD4606E999056A1074136 ();
// 0x00000355 System.Single LeanTween::easeInOutBounce(System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutBounce_mDDC1155A0C82DE93397F30C80DF6059685229413 ();
// 0x00000356 System.Single LeanTween::easeInBack(System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeInBack_mF3612B1794D308B659FBF9FF3DFB1BE11C4C5C3E ();
// 0x00000357 System.Single LeanTween::easeOutBack(System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeOutBack_m7B845AD4DA44D5C0B79DFFB66BFE8D1EA78BB57D ();
// 0x00000358 System.Single LeanTween::easeInOutBack(System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutBack_m1A651107C2734CD9259AE6F3422D1544D9C445E2 ();
// 0x00000359 System.Single LeanTween::easeInElastic(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeInElastic_mC0C2B9BD7D1ED6B6624F52DC5D8F6D2E1C1C575D ();
// 0x0000035A System.Single LeanTween::easeOutElastic(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeOutElastic_mDD7A06B6C97A24B25C34A4191312D6F8E64DED7A ();
// 0x0000035B System.Single LeanTween::easeInOutElastic(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_easeInOutElastic_mC2E44F37BF3623A004AD1B60EE3AF7968DB21D2B ();
// 0x0000035C LTDescr LeanTween::followDamp(UnityEngine.Transform,UnityEngine.Transform,LeanProp,System.Single,System.Single)
extern void LeanTween_followDamp_mA9C52B1FBD62D1491E62714A18E092E939948A12 ();
// 0x0000035D LTDescr LeanTween::followSpring(UnityEngine.Transform,UnityEngine.Transform,LeanProp,System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_followSpring_m6A4518BC770FBA7041F96B2D93910A874D8DE95D ();
// 0x0000035E LTDescr LeanTween::followBounceOut(UnityEngine.Transform,UnityEngine.Transform,LeanProp,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanTween_followBounceOut_m30E47CECD44D0928C51E4868BD229F8C3DE5939C ();
// 0x0000035F LTDescr LeanTween::followLinear(UnityEngine.Transform,UnityEngine.Transform,LeanProp,System.Single)
extern void LeanTween_followLinear_mEF3E3F81DB95C8FC7512D9EB79B1A29818537333 ();
// 0x00000360 System.Void LeanTween::addListener(System.Int32,System.Action`1<LTEvent>)
extern void LeanTween_addListener_mEC31B40DB9ED344E2F2BAE152DA564537AB16754 ();
// 0x00000361 System.Void LeanTween::addListener(UnityEngine.GameObject,System.Int32,System.Action`1<LTEvent>)
extern void LeanTween_addListener_m34C97CB48F8B906351A79F49A6CECC6334BBD3C8 ();
// 0x00000362 System.Boolean LeanTween::removeListener(System.Int32,System.Action`1<LTEvent>)
extern void LeanTween_removeListener_m982E421FADD5A0991EBDFBD169F5BE77740ECE3A ();
// 0x00000363 System.Boolean LeanTween::removeListener(System.Int32)
extern void LeanTween_removeListener_m39242CA2654CC5FC27C4D08B45F0F71888164F41 ();
// 0x00000364 System.Boolean LeanTween::removeListener(UnityEngine.GameObject,System.Int32,System.Action`1<LTEvent>)
extern void LeanTween_removeListener_mE054AB523D46CA58B0C59101C79AB1C9E680072A ();
// 0x00000365 System.Void LeanTween::dispatchEvent(System.Int32)
extern void LeanTween_dispatchEvent_m2996C15CD7A7C91A70A896DACBD85AD50596EE74 ();
// 0x00000366 System.Void LeanTween::dispatchEvent(System.Int32,System.Object)
extern void LeanTween_dispatchEvent_m746D9A2E728E4018D7156A8C05A2B2113151E475 ();
// 0x00000367 System.Void LeanTween::.ctor()
extern void LeanTween__ctor_m29640AFA36A452E41D7D7FCE837930805FE524D4 ();
// 0x00000368 System.Void LeanTween::.cctor()
extern void LeanTween__cctor_m4A2CF2E2F8D1A20278F8B86DCF870EBE903F9AD5 ();
// 0x00000369 UnityEngine.Vector3[] LTUtility::reverse(UnityEngine.Vector3[])
extern void LTUtility_reverse_mFA541E145D5DB2CAD1B20BA78057C7A3495556C0 ();
// 0x0000036A System.Void LTUtility::.ctor()
extern void LTUtility__ctor_m04573B783EECEFDD4E2AF1FFD4D7B62E914D0F4D ();
// 0x0000036B System.Void LTBezier::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LTBezier__ctor_m97D899A3D06174612D033E42F804FB14684814D9 ();
// 0x0000036C System.Single LTBezier::map(System.Single)
extern void LTBezier_map_m3DD6B1382C93E0204A339F62ACCFAB5C3A29DD2A ();
// 0x0000036D UnityEngine.Vector3 LTBezier::bezierPoint(System.Single)
extern void LTBezier_bezierPoint_mEB0E0EF802E14A7FD9AB49F23936260D8B0AFC45 ();
// 0x0000036E UnityEngine.Vector3 LTBezier::point(System.Single)
extern void LTBezier_point_m017F4A7A1923793B57C7A8A76682609CCFD0E8D4 ();
// 0x0000036F System.Void LTBezierPath::.ctor()
extern void LTBezierPath__ctor_m3180C1A051F04229804D0BB0F4ECCAE916AA6845 ();
// 0x00000370 System.Void LTBezierPath::.ctor(UnityEngine.Vector3[])
extern void LTBezierPath__ctor_m431529CBFCA2BEBB9158B0E80E9AFDBB2F9EF7C4 ();
// 0x00000371 System.Void LTBezierPath::setPoints(UnityEngine.Vector3[])
extern void LTBezierPath_setPoints_m6717D57148D9DD5B0B3B9F2C67F5060423CC62BF ();
// 0x00000372 System.Single LTBezierPath::get_distance()
extern void LTBezierPath_get_distance_mE03E0C6CAC8CB0D3DC5CD4F21CCE6AF6A2B5615E ();
// 0x00000373 UnityEngine.Vector3 LTBezierPath::point(System.Single)
extern void LTBezierPath_point_m7DD32C57F1078C21BFF31EF88109EF188FBBEA89 ();
// 0x00000374 System.Void LTBezierPath::place2d(UnityEngine.Transform,System.Single)
extern void LTBezierPath_place2d_mA08CEFF1862BCD64FBF0D6DD7821822BED4F56A7 ();
// 0x00000375 System.Void LTBezierPath::placeLocal2d(UnityEngine.Transform,System.Single)
extern void LTBezierPath_placeLocal2d_mBDE2E3BB035F85D3A500AC0B0E36E5038CF3B27A ();
// 0x00000376 System.Void LTBezierPath::place(UnityEngine.Transform,System.Single)
extern void LTBezierPath_place_m080C91568428A12F6CAA5EA5787D0A1EE78569E3 ();
// 0x00000377 System.Void LTBezierPath::place(UnityEngine.Transform,System.Single,UnityEngine.Vector3)
extern void LTBezierPath_place_mA0099B55C6D850148EF3F139A3AAA21115FF6395 ();
// 0x00000378 System.Void LTBezierPath::placeLocal(UnityEngine.Transform,System.Single)
extern void LTBezierPath_placeLocal_m0D2D81B70292CE5B7D595E8E1FECB865E2617F88 ();
// 0x00000379 System.Void LTBezierPath::placeLocal(UnityEngine.Transform,System.Single,UnityEngine.Vector3)
extern void LTBezierPath_placeLocal_mE7093E9870F33F6B93BB03F804B14011A7C0542A ();
// 0x0000037A System.Void LTBezierPath::gizmoDraw(System.Single)
extern void LTBezierPath_gizmoDraw_m32B996E9870AA9A63DD800B3E406C54F697B23ED ();
// 0x0000037B System.Single LTBezierPath::ratioAtPoint(UnityEngine.Vector3,System.Single)
extern void LTBezierPath_ratioAtPoint_mDBAB488E78E8993C759918B979CCCCD9DFDAED19 ();
// 0x0000037C System.Void LTSpline::.ctor(UnityEngine.Vector3[])
extern void LTSpline__ctor_m89DAF8F9A02B744F14DC951BF26998ABE636079E ();
// 0x0000037D System.Void LTSpline::.ctor(UnityEngine.Vector3[],System.Boolean)
extern void LTSpline__ctor_mB9F6B9D5E8250215A98C318B775D0862716E2F62 ();
// 0x0000037E System.Void LTSpline::init(UnityEngine.Vector3[],System.Boolean)
extern void LTSpline_init_m5F8F0ED8EE3B00DFB2020C9B8E7F0589D764296E ();
// 0x0000037F UnityEngine.Vector3 LTSpline::map(System.Single)
extern void LTSpline_map_m8586FE88C55DFE6D7CD897651AEBB8E161D4AAD3 ();
// 0x00000380 UnityEngine.Vector3 LTSpline::interp(System.Single)
extern void LTSpline_interp_m07E5A5D423EC7092EF3CCF7FD65FFD65DEF7704D ();
// 0x00000381 System.Single LTSpline::ratioAtPoint(UnityEngine.Vector3)
extern void LTSpline_ratioAtPoint_m6BE44439D18029959983FA5D960C319BAA0F5EC9 ();
// 0x00000382 UnityEngine.Vector3 LTSpline::point(System.Single)
extern void LTSpline_point_mDCF2C08D8F3F338D5749D4478D652DDE9B2949F9 ();
// 0x00000383 System.Void LTSpline::place2d(UnityEngine.Transform,System.Single)
extern void LTSpline_place2d_m7715D476897D521AC679B2007254773606199FAF ();
// 0x00000384 System.Void LTSpline::placeLocal2d(UnityEngine.Transform,System.Single)
extern void LTSpline_placeLocal2d_m1264CC1B0A061E5B1D399B46F1E6BEE91BE49926 ();
// 0x00000385 System.Void LTSpline::place(UnityEngine.Transform,System.Single)
extern void LTSpline_place_mDBCF580928976DB68CFA586B31B88C23C6D9FD52 ();
// 0x00000386 System.Void LTSpline::place(UnityEngine.Transform,System.Single,UnityEngine.Vector3)
extern void LTSpline_place_m4F75114ECFDE9711D5BC6AA012180139DB21136F ();
// 0x00000387 System.Void LTSpline::placeLocal(UnityEngine.Transform,System.Single)
extern void LTSpline_placeLocal_m47E0DF17D96F80E0EF9E7B2809828BECF1063660 ();
// 0x00000388 System.Void LTSpline::placeLocal(UnityEngine.Transform,System.Single,UnityEngine.Vector3)
extern void LTSpline_placeLocal_mA60E9587B94103C8BD655ADD566F2218D4D2B1B5 ();
// 0x00000389 System.Void LTSpline::gizmoDraw(System.Single)
extern void LTSpline_gizmoDraw_mA538B509F29ED2A1583D44FDC86935B98364FA88 ();
// 0x0000038A System.Void LTSpline::drawGizmo(UnityEngine.Color)
extern void LTSpline_drawGizmo_mD374863782B673CE885A2855910958A2DF60A95E ();
// 0x0000038B System.Void LTSpline::drawGizmo(UnityEngine.Transform[],UnityEngine.Color)
extern void LTSpline_drawGizmo_m3C4516917996D8F6D1B40A5017937C2B2BADB1F1 ();
// 0x0000038C System.Void LTSpline::drawLine(UnityEngine.Transform[],System.Single,UnityEngine.Color)
extern void LTSpline_drawLine_mB44BF9E463BE732E1FC8A178E92FE3230FA79588 ();
// 0x0000038D System.Void LTSpline::drawLinesGLLines(UnityEngine.Material,UnityEngine.Color,System.Single)
extern void LTSpline_drawLinesGLLines_m7E7A763E4DEF72CFF387EAE1B80A1A6D26905590 ();
// 0x0000038E UnityEngine.Vector3[] LTSpline::generateVectors()
extern void LTSpline_generateVectors_m85FEDD714CEEAD04670F854D11CED89775F05AB3 ();
// 0x0000038F System.Void LTSpline::.cctor()
extern void LTSpline__cctor_m6739314E6052E4C78709F02047E8DC2DC9096AAF ();
// 0x00000390 System.Void LTRect::.ctor()
extern void LTRect__ctor_mD2DD65C62E8EDAC7B218018C87391F7E2F36FEF9 ();
// 0x00000391 System.Void LTRect::.ctor(UnityEngine.Rect)
extern void LTRect__ctor_m47F4B741D47E9809C1B03A566F06863E246A2696 ();
// 0x00000392 System.Void LTRect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void LTRect__ctor_mBE81F7E8AAD5FB8A7E8AFCEAECCD66DA9A30F345 ();
// 0x00000393 System.Void LTRect::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LTRect__ctor_m108E96CEDBA3D65FEFBE787F4B17F844CF3638AE ();
// 0x00000394 System.Void LTRect::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LTRect__ctor_m6A1374116E6DC9E18821247FAEDDD60F74FA3758 ();
// 0x00000395 System.Boolean LTRect::get_hasInitiliazed()
extern void LTRect_get_hasInitiliazed_m2D51CC6C1FCF712275E919DFA2E44F1E860DF216 ();
// 0x00000396 System.Int32 LTRect::get_id()
extern void LTRect_get_id_m6F74EEF30562B278C561253B7591297CD389C30A ();
// 0x00000397 System.Void LTRect::setId(System.Int32,System.Int32)
extern void LTRect_setId_mF1BD1C3E2DB5CA883A47FB9E8F644EB16A5B294F ();
// 0x00000398 System.Void LTRect::reset()
extern void LTRect_reset_m53A78C5EA3E9E78CC06DB2F25D1C71A588B06F2D ();
// 0x00000399 System.Void LTRect::resetForRotation()
extern void LTRect_resetForRotation_mA8B3D468DCD205AE911118E6EED6C8A479A674A0 ();
// 0x0000039A System.Single LTRect::get_x()
extern void LTRect_get_x_mD70B7C3B314D4FCDF6AF3BE29F6397C47426BFC6 ();
// 0x0000039B System.Void LTRect::set_x(System.Single)
extern void LTRect_set_x_m640BF975EE3C8DFC498BEAFC41742A094684ACAB ();
// 0x0000039C System.Single LTRect::get_y()
extern void LTRect_get_y_m168437019BB9D70389C282B93AF269634C80EB5A ();
// 0x0000039D System.Void LTRect::set_y(System.Single)
extern void LTRect_set_y_mAD963E12C76DC233063AFACED81EAF3D03F06D27 ();
// 0x0000039E System.Single LTRect::get_width()
extern void LTRect_get_width_m622A9922E21390846F78DA4E37A7EE0675D9F5CE ();
// 0x0000039F System.Void LTRect::set_width(System.Single)
extern void LTRect_set_width_m03AEDBAC9CA96B53E086CDD5DAB57A394B1417B5 ();
// 0x000003A0 System.Single LTRect::get_height()
extern void LTRect_get_height_m1C9CB0CBAEC6888479CDF92240484145730F0E30 ();
// 0x000003A1 System.Void LTRect::set_height(System.Single)
extern void LTRect_set_height_m34DC6F5C00E4A2C16382539CE21524603F6EAB89 ();
// 0x000003A2 UnityEngine.Rect LTRect::get_rect()
extern void LTRect_get_rect_m6C78FB7176F49F9F8FBB80160FDF37C57EEEEA79 ();
// 0x000003A3 System.Void LTRect::set_rect(UnityEngine.Rect)
extern void LTRect_set_rect_m7FBDEB9CFFC6BE529A76996FCA83F69517F0280D ();
// 0x000003A4 LTRect LTRect::setStyle(UnityEngine.GUIStyle)
extern void LTRect_setStyle_m73FC3A26735BB8712D8B2D92357035D9FB8FA1C5 ();
// 0x000003A5 LTRect LTRect::setFontScaleToFit(System.Boolean)
extern void LTRect_setFontScaleToFit_m39043C9072BB81E3DFF6C7697404EFB2C3090984 ();
// 0x000003A6 LTRect LTRect::setColor(UnityEngine.Color)
extern void LTRect_setColor_m260F2239C224794A8C49B970E5F5DBF5CA594779 ();
// 0x000003A7 LTRect LTRect::setAlpha(System.Single)
extern void LTRect_setAlpha_m2B25F81A596DFA252D4C2D0090FAF8ED224AD2CD ();
// 0x000003A8 LTRect LTRect::setLabel(System.String)
extern void LTRect_setLabel_m1FFCB5B75A7AA46B894883AD569AF4ECE8662D5F ();
// 0x000003A9 LTRect LTRect::setUseSimpleScale(System.Boolean,UnityEngine.Rect)
extern void LTRect_setUseSimpleScale_mFD395CF7FAF8EF1E65A4697817CA41A1E4DF22D5 ();
// 0x000003AA LTRect LTRect::setUseSimpleScale(System.Boolean)
extern void LTRect_setUseSimpleScale_m319F90C955999F948E57D8FEFFB3706E9446EDE4 ();
// 0x000003AB LTRect LTRect::setSizeByHeight(System.Boolean)
extern void LTRect_setSizeByHeight_m17196A565268B33273FE50DE6A9D89D71B2C7AB3 ();
// 0x000003AC System.String LTRect::ToString()
extern void LTRect_ToString_m30F891F0B46B40656CD5BC9265D6D2F0AAADEA33 ();
// 0x000003AD System.Void LTEvent::.ctor(System.Int32,System.Object)
extern void LTEvent__ctor_mF1824A2419396C340A300E02A11BD3C710931494 ();
// 0x000003AE System.Void LTGUI::init()
extern void LTGUI_init_m56E76FF99D2B3BA59DC04B0CD796B8672BAD16CB ();
// 0x000003AF System.Void LTGUI::initRectCheck()
extern void LTGUI_initRectCheck_mD1D857FC17862BBF0171D8B30C2629B9FD84D4D8 ();
// 0x000003B0 System.Void LTGUI::reset()
extern void LTGUI_reset_m7CE773801CF53E0AFCA45D4C470DA38930F518F0 ();
// 0x000003B1 System.Void LTGUI::update(System.Int32)
extern void LTGUI_update_m00A3E17E130357C2B8F88B29572E767536C3C097 ();
// 0x000003B2 System.Boolean LTGUI::checkOnScreen(UnityEngine.Rect)
extern void LTGUI_checkOnScreen_m0B39FD2B120D5FB49801098E46F8705837445E6A ();
// 0x000003B3 System.Void LTGUI::destroy(System.Int32)
extern void LTGUI_destroy_m59B5A127738FD610340C642E74C5FC5ED8CDF2C0 ();
// 0x000003B4 System.Void LTGUI::destroyAll(System.Int32)
extern void LTGUI_destroyAll_mEE7BC1026BAC0D8132D6AD8CAAE8CD4B5249807F ();
// 0x000003B5 LTRect LTGUI::label(UnityEngine.Rect,System.String,System.Int32)
extern void LTGUI_label_mA4BFF45B5BC97C4227CC0E47BC2183470B42C5F4 ();
// 0x000003B6 LTRect LTGUI::label(LTRect,System.String,System.Int32)
extern void LTGUI_label_m9016D43E1833A398F9DE235F106DB4B83E4A3EF9 ();
// 0x000003B7 LTRect LTGUI::texture(UnityEngine.Rect,UnityEngine.Texture,System.Int32)
extern void LTGUI_texture_m4EAA8E004AB27F16AF3F70E001F32086C34DF6A6 ();
// 0x000003B8 LTRect LTGUI::texture(LTRect,UnityEngine.Texture,System.Int32)
extern void LTGUI_texture_m6D73FEC3838D61DB4B92333120AF15139EEB2615 ();
// 0x000003B9 LTRect LTGUI::element(LTRect,System.Int32)
extern void LTGUI_element_m5933C737FE6DB8B7F4AC87B9EBEBF338BE4DD456 ();
// 0x000003BA System.Boolean LTGUI::hasNoOverlap(UnityEngine.Rect,System.Int32)
extern void LTGUI_hasNoOverlap_m1174D1999AEEC77DAB75CC5456C4A28268D72663 ();
// 0x000003BB System.Boolean LTGUI::pressedWithinRect(UnityEngine.Rect)
extern void LTGUI_pressedWithinRect_m585FA74045ACAFFF9EDAEC8469B6C61F4191C6BF ();
// 0x000003BC System.Boolean LTGUI::checkWithinRect(UnityEngine.Vector2,UnityEngine.Rect)
extern void LTGUI_checkWithinRect_mE13CE64B1EA92D1287B6B090A75E99832C45E4E8 ();
// 0x000003BD UnityEngine.Vector2 LTGUI::firstTouch()
extern void LTGUI_firstTouch_mF464F4A8A19F65CBC9EA2BF2111236BEBB9E618C ();
// 0x000003BE System.Void LTGUI::.ctor()
extern void LTGUI__ctor_m914864F7BFF6D41D79693EED54116EA572A41429 ();
// 0x000003BF System.Void LTGUI::.cctor()
extern void LTGUI__cctor_m01C5B6F47B1C4B45CA1D6B77B5720BDBAFB049AA ();
// 0x000003C0 LTDescr LeanTweenExt::LeanAlpha(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanAlpha_m461C89B7ADA172C00449BE049ACF50F3CE55D766 ();
// 0x000003C1 LTDescr LeanTweenExt::LeanAlphaVertex(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanAlphaVertex_m78E0D1E1E6D229BB56EAC9F208119463298D79D7 ();
// 0x000003C2 LTDescr LeanTweenExt::LeanAlpha(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTweenExt_LeanAlpha_m62EE99D970FEDB18E72E1CECC8BF3C3C806B8897 ();
// 0x000003C3 LTDescr LeanTweenExt::LeanAlpha(UnityEngine.CanvasGroup,System.Single,System.Single)
extern void LeanTweenExt_LeanAlpha_mDC6296AC255B03B668D291C60FCC3F49788DF8EB ();
// 0x000003C4 LTDescr LeanTweenExt::LeanAlphaText(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTweenExt_LeanAlphaText_m8289E2F78CE20F51FD72D83399F535A6ECE4DCD0 ();
// 0x000003C5 System.Void LeanTweenExt::LeanCancel(UnityEngine.GameObject)
extern void LeanTweenExt_LeanCancel_m258065657CE313E3C76F55578F793097140D9F19 ();
// 0x000003C6 System.Void LeanTweenExt::LeanCancel(UnityEngine.GameObject,System.Boolean)
extern void LeanTweenExt_LeanCancel_m3EAAA4F94BA9D26442BF5503CF1C13F56B136E41 ();
// 0x000003C7 System.Void LeanTweenExt::LeanCancel(UnityEngine.GameObject,System.Int32,System.Boolean)
extern void LeanTweenExt_LeanCancel_m0EBACDA55B64F6124F9863947D3EDFF41FE5BC91 ();
// 0x000003C8 System.Void LeanTweenExt::LeanCancel(UnityEngine.RectTransform)
extern void LeanTweenExt_LeanCancel_m16FFCC4C3A923A8261181F5D40AF48369B766112 ();
// 0x000003C9 LTDescr LeanTweenExt::LeanColor(UnityEngine.GameObject,UnityEngine.Color,System.Single)
extern void LeanTweenExt_LeanColor_mA1A81C489F98EB1520DE95136AB29B46E5EB5D1F ();
// 0x000003CA LTDescr LeanTweenExt::LeanColorText(UnityEngine.RectTransform,UnityEngine.Color,System.Single)
extern void LeanTweenExt_LeanColorText_m23602453BAB11CF15AA4ED5E4F1B3962CF6836C5 ();
// 0x000003CB LTDescr LeanTweenExt::LeanDelayedCall(UnityEngine.GameObject,System.Single,System.Action)
extern void LeanTweenExt_LeanDelayedCall_m67EE573EEA0027172A59DC8E6810F2643050D72F ();
// 0x000003CC LTDescr LeanTweenExt::LeanDelayedCall(UnityEngine.GameObject,System.Single,System.Action`1<System.Object>)
extern void LeanTweenExt_LeanDelayedCall_mB1FF6EE6E33AEAFE689676F8674FDF2F8BC8D395 ();
// 0x000003CD System.Boolean LeanTweenExt::LeanIsPaused(UnityEngine.GameObject)
extern void LeanTweenExt_LeanIsPaused_mE920863734EE45312ECA09EA8B998C524E26BFE1 ();
// 0x000003CE System.Boolean LeanTweenExt::LeanIsPaused(UnityEngine.RectTransform)
extern void LeanTweenExt_LeanIsPaused_m40DD7C17AFF3F2B488C94B529A62F0EC3773B532 ();
// 0x000003CF System.Boolean LeanTweenExt::LeanIsTweening(UnityEngine.GameObject)
extern void LeanTweenExt_LeanIsTweening_m11D59C75569AD0E04DA5CAA351D2F68F38C8E1D6 ();
// 0x000003D0 LTDescr LeanTweenExt::LeanMove(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanMove_mBCF028720C25916836835CBFDDEFBBAE0F747730 ();
// 0x000003D1 LTDescr LeanTweenExt::LeanMove(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanMove_m090CF72208FA48F140219041C8062A46EF3F3ADE ();
// 0x000003D2 LTDescr LeanTweenExt::LeanMove(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanMove_m3D682F2EABA9624478F95DC984962451F4C72335 ();
// 0x000003D3 LTDescr LeanTweenExt::LeanMove(UnityEngine.GameObject,UnityEngine.Vector2,System.Single)
extern void LeanTweenExt_LeanMove_mDE73F1D58AAC128EBF331B493A9B836851AC1589 ();
// 0x000003D4 LTDescr LeanTweenExt::LeanMove(UnityEngine.Transform,UnityEngine.Vector2,System.Single)
extern void LeanTweenExt_LeanMove_m38E623C91DB8CBB6FDAB138979719D2D1837EA1E ();
// 0x000003D5 LTDescr LeanTweenExt::LeanMove(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMove_mCB11792AADB6F2D82EF43C90709B1CBD79FDDB83 ();
// 0x000003D6 LTDescr LeanTweenExt::LeanMove(UnityEngine.GameObject,LTBezierPath,System.Single)
extern void LeanTweenExt_LeanMove_m3CC6348A9E1270D6703F193F8CC9B1C2CE12384B ();
// 0x000003D7 LTDescr LeanTweenExt::LeanMove(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTweenExt_LeanMove_mDD7DB276339BE449389D687CCF4793E275393331 ();
// 0x000003D8 LTDescr LeanTweenExt::LeanMove(UnityEngine.Transform,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMove_m1D02677FF9D2509C2FA77A3BE7C3C69718AA191B ();
// 0x000003D9 LTDescr LeanTweenExt::LeanMove(UnityEngine.Transform,LTBezierPath,System.Single)
extern void LeanTweenExt_LeanMove_m1FC68173632416D0BBB72EFC74D82348B0523D80 ();
// 0x000003DA LTDescr LeanTweenExt::LeanMove(UnityEngine.Transform,LTSpline,System.Single)
extern void LeanTweenExt_LeanMove_m87B72A7F9F763586108B9F5B648D48428B1F8FFD ();
// 0x000003DB LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanMoveLocal_mBD123664B63D8DE32BF36F0ED100E9B87996267E ();
// 0x000003DC LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.GameObject,LTBezierPath,System.Single)
extern void LeanTweenExt_LeanMoveLocal_m3149C4DF54C63DB2384A197A0AC8831EC0845703 ();
// 0x000003DD LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTweenExt_LeanMoveLocal_m97955CB5D282428BC2D2CBE129F6A401B482A8F8 ();
// 0x000003DE LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanMoveLocal_m616AE133C599BAF61CE3159CF579C3DB0EFE7B66 ();
// 0x000003DF LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.Transform,LTBezierPath,System.Single)
extern void LeanTweenExt_LeanMoveLocal_mFDF99A832F9B857E1B16A2E744F9F383C1F90375 ();
// 0x000003E0 LTDescr LeanTweenExt::LeanMoveLocal(UnityEngine.Transform,LTSpline,System.Single)
extern void LeanTweenExt_LeanMoveLocal_mA80CD3FC03494A1744FB551747A732908D9B2D05 ();
// 0x000003E1 LTDescr LeanTweenExt::LeanMoveLocalX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalX_m5C894E1B3E98732358670D3837F1F8D3C4867AB6 ();
// 0x000003E2 LTDescr LeanTweenExt::LeanMoveLocalY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalY_mC4D2C4DFD2A41D0988F10D78AB782834A2D7AEA3 ();
// 0x000003E3 LTDescr LeanTweenExt::LeanMoveLocalZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalZ_m2523332A42D3C35D0EB0983B50290B260BE971D0 ();
// 0x000003E4 LTDescr LeanTweenExt::LeanMoveLocalX(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalX_m8AF36480056D23742446C3D011AB3462FF6E4DE7 ();
// 0x000003E5 LTDescr LeanTweenExt::LeanMoveLocalY(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalY_m402602941FDB4A2F4E4F52723678149A63D28742 ();
// 0x000003E6 LTDescr LeanTweenExt::LeanMoveLocalZ(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveLocalZ_m277E9019194B31C9D2F5FACD5CA5BDB78D14031F ();
// 0x000003E7 LTDescr LeanTweenExt::LeanMoveSpline(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMoveSpline_m1146A15795C039B9B2B03ED4C80AD8D232A3C4F9 ();
// 0x000003E8 LTDescr LeanTweenExt::LeanMoveSpline(UnityEngine.GameObject,LTSpline,System.Single)
extern void LeanTweenExt_LeanMoveSpline_m0C9AE2BAD998E3BC3CD0C3F1671F47B2F769D955 ();
// 0x000003E9 LTDescr LeanTweenExt::LeanMoveSpline(UnityEngine.Transform,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMoveSpline_m65F76F58B8003E56FCB6C2EDF7044D467CE61D4C ();
// 0x000003EA LTDescr LeanTweenExt::LeanMoveSpline(UnityEngine.Transform,LTSpline,System.Single)
extern void LeanTweenExt_LeanMoveSpline_m54CFB8DA58EB1DC4557E6456579F9D3DA7F28C8A ();
// 0x000003EB LTDescr LeanTweenExt::LeanMoveSplineLocal(UnityEngine.GameObject,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMoveSplineLocal_mA26C4FC628C8DBE7393FCEFAEABDDE769F3BA36A ();
// 0x000003EC LTDescr LeanTweenExt::LeanMoveSplineLocal(UnityEngine.Transform,UnityEngine.Vector3[],System.Single)
extern void LeanTweenExt_LeanMoveSplineLocal_m1E899C48EA516A34C9929D7FDF44EF2632AA0D1E ();
// 0x000003ED LTDescr LeanTweenExt::LeanMoveX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveX_m82773527741CA6AE6E241C12A1D8634788BEEB4A ();
// 0x000003EE LTDescr LeanTweenExt::LeanMoveX(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveX_m96FB87B93DDACF291202E912E981B3760B44215D ();
// 0x000003EF LTDescr LeanTweenExt::LeanMoveX(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveX_m3E49D1A15D798CF0DEE8FB9A1D7F2932D636371D ();
// 0x000003F0 LTDescr LeanTweenExt::LeanMoveY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveY_m0372D8F81E13E6B32BCBA2DBE800598DCA71866B ();
// 0x000003F1 LTDescr LeanTweenExt::LeanMoveY(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveY_m821E5633762A108AF0D80528ED060EB0E62DA9E5 ();
// 0x000003F2 LTDescr LeanTweenExt::LeanMoveY(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveY_m1AA13E442CA147D6B062BE8424D669317537F6F5 ();
// 0x000003F3 LTDescr LeanTweenExt::LeanMoveZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveZ_m9939145DDF1A83F4D8547D53A4538912466C008B ();
// 0x000003F4 LTDescr LeanTweenExt::LeanMoveZ(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveZ_m79A272207A6FBEA47E355DE22A106896A0941ECC ();
// 0x000003F5 LTDescr LeanTweenExt::LeanMoveZ(UnityEngine.RectTransform,System.Single,System.Single)
extern void LeanTweenExt_LeanMoveZ_m94F3AB61DB4EBFE4F39B405E5EFE8CD3EC9F2A36 ();
// 0x000003F6 System.Void LeanTweenExt::LeanPause(UnityEngine.GameObject)
extern void LeanTweenExt_LeanPause_m46A64B42348CB04917830211A8C40548EED616E6 ();
// 0x000003F7 LTDescr LeanTweenExt::LeanPlay(UnityEngine.RectTransform,UnityEngine.Sprite[])
extern void LeanTweenExt_LeanPlay_m635E84DBAF3658181BD11102F39B17126695612C ();
// 0x000003F8 System.Void LeanTweenExt::LeanResume(UnityEngine.GameObject)
extern void LeanTweenExt_LeanResume_m3499A30358E3318BB0F73B0B1A07AC9C00E463A4 ();
// 0x000003F9 LTDescr LeanTweenExt::LeanRotate(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanRotate_m3663A0A9B43101BE83E0113F198EDE1DC7A61A4E ();
// 0x000003FA LTDescr LeanTweenExt::LeanRotate(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanRotate_m0050CFBB76AE4B345C300EE00F8F13584DAF39A9 ();
// 0x000003FB LTDescr LeanTweenExt::LeanRotate(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanRotate_m8B46191A435295E93DB9D3F3F982A468D020E54C ();
// 0x000003FC LTDescr LeanTweenExt::LeanRotateAround(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAround_m94E627C88BD4C138F9FD7ADE41FA86F614D4F26B ();
// 0x000003FD LTDescr LeanTweenExt::LeanRotateAround(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAround_m8D2DDA35FF67D85798119A863ED498C97980575A ();
// 0x000003FE LTDescr LeanTweenExt::LeanRotateAround(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAround_m1C2F12BDAD3FB54F4DB76AD02D759ECBCD15289E ();
// 0x000003FF LTDescr LeanTweenExt::LeanRotateAroundLocal(UnityEngine.GameObject,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAroundLocal_m6939D0BB618449C6DB007BD42F9A4C898BE97618 ();
// 0x00000400 LTDescr LeanTweenExt::LeanRotateAroundLocal(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAroundLocal_m3977508F1548419CF11A64412C95D747F1C5BE71 ();
// 0x00000401 LTDescr LeanTweenExt::LeanRotateAroundLocal(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateAroundLocal_m83B8A190A95128CD31886544515865EE7553C0E0 ();
// 0x00000402 LTDescr LeanTweenExt::LeanRotateX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateX_m1945A6153A434154D6ADB55FDDECAC9CCC46BDB7 ();
// 0x00000403 LTDescr LeanTweenExt::LeanRotateX(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateX_mD17F3CCE10C947CB82E9EC222B0810EEA727FCFA ();
// 0x00000404 LTDescr LeanTweenExt::LeanRotateY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateY_mA27D8888248D81C3B8EB606E0E5DBD47716C3A7A ();
// 0x00000405 LTDescr LeanTweenExt::LeanRotateY(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateY_m9984A206D938B5D53BAC015F467C919DA595BEDA ();
// 0x00000406 LTDescr LeanTweenExt::LeanRotateZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateZ_mA27C015E2FEA84D212B0C3D03D6E511035E133CB ();
// 0x00000407 LTDescr LeanTweenExt::LeanRotateZ(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanRotateZ_m8CAA7A563407C9E73FA9B514AC08A9B2C0225A97 ();
// 0x00000408 LTDescr LeanTweenExt::LeanScale(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanScale_mB0084B9F5FC69561F247954CBED4BA70511D1F96 ();
// 0x00000409 LTDescr LeanTweenExt::LeanScale(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanScale_mAE75E6B1234A072C02D7354461EAA9EB3D485E76 ();
// 0x0000040A LTDescr LeanTweenExt::LeanScale(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanScale_m4ED118613DFC434764355605FD977578EC755910 ();
// 0x0000040B LTDescr LeanTweenExt::LeanScaleX(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleX_mB4F38759A8A24E581E837A8A696D9AB275172103 ();
// 0x0000040C LTDescr LeanTweenExt::LeanScaleX(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleX_mB89FF0BBCE3BE85F916A6A7B1D8E6AE9E9E88A7A ();
// 0x0000040D LTDescr LeanTweenExt::LeanScaleY(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleY_m64BF74A2E7E6B81DA3DA69F7F29AD5EB6DC894C4 ();
// 0x0000040E LTDescr LeanTweenExt::LeanScaleY(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleY_mBCBD993A50FC95AF46ADD7992EB634D05875AF55 ();
// 0x0000040F LTDescr LeanTweenExt::LeanScaleZ(UnityEngine.GameObject,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleZ_m42B1B4A160D482743F667A0E398885BB3C976787 ();
// 0x00000410 LTDescr LeanTweenExt::LeanScaleZ(UnityEngine.Transform,System.Single,System.Single)
extern void LeanTweenExt_LeanScaleZ_m821101BF3F56706E65FE11C856D03C1F4C02393E ();
// 0x00000411 LTDescr LeanTweenExt::LeanSize(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single)
extern void LeanTweenExt_LeanSize_m99C3564D6FB42626360A9BB25BD965F40F61B376 ();
// 0x00000412 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanTweenExt_LeanValue_m0ABC90AD97B6278EBAE75887B295D8EB1A4CA6E1 ();
// 0x00000413 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Single,System.Single,System.Single)
extern void LeanTweenExt_LeanValue_m6192DF3CB7E8ABD9827718DF1C26F4892DCCA3FA ();
// 0x00000414 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void LeanTweenExt_LeanValue_mC3261ED9FF392F50DD1713982ADD612BBAD5266E ();
// 0x00000415 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanValue_m5757F21E978B62AF23CC5E142AC222BB4A081784 ();
// 0x00000416 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`1<System.Single>,System.Single,System.Single,System.Single)
extern void LeanTweenExt_LeanValue_m49CCF82C65BDB92A503F5C61B0EA0121BEEA646B ();
// 0x00000417 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`2<System.Single,System.Single>,System.Single,System.Single,System.Single)
extern void LeanTweenExt_LeanValue_mD59F8CFB5C81AF1061A856CDB0F42244708BC1D6 ();
// 0x00000418 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`2<System.Single,System.Object>,System.Single,System.Single,System.Single)
extern void LeanTweenExt_LeanValue_mB5C28F3E39E0871176731D189EF33CE24E7A6DD3 ();
// 0x00000419 LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`1<UnityEngine.Color>,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void LeanTweenExt_LeanValue_m6062D935C55251AC83C7D7D71706FBB350CD375B ();
// 0x0000041A LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`1<UnityEngine.Vector2>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void LeanTweenExt_LeanValue_m7F3A44B64D4EB8824AF830E9BCEDA6B5463099AB ();
// 0x0000041B LTDescr LeanTweenExt::LeanValue(UnityEngine.GameObject,System.Action`1<UnityEngine.Vector3>,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void LeanTweenExt_LeanValue_m0663B7A32820E3685F4447EFC55951C507170CF5 ();
// 0x0000041C System.Void LeanTweenExt::LeanSetPosX(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetPosX_mB614BC76AA5B0934B2F156A9A781DE7DB2A9AEB2 ();
// 0x0000041D System.Void LeanTweenExt::LeanSetPosY(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetPosY_m8935780D650702F87FB47C62CB3BA667F4153BAE ();
// 0x0000041E System.Void LeanTweenExt::LeanSetPosZ(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetPosZ_m2D663CFB259143ECA22EEF0957396362A0648A7F ();
// 0x0000041F System.Void LeanTweenExt::LeanSetLocalPosX(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetLocalPosX_mC5B1B50F60706CF6FF542A63287C1B4DB534691E ();
// 0x00000420 System.Void LeanTweenExt::LeanSetLocalPosY(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetLocalPosY_m9D85F225670E5804C0E9E944161F520536DD81C8 ();
// 0x00000421 System.Void LeanTweenExt::LeanSetLocalPosZ(UnityEngine.Transform,System.Single)
extern void LeanTweenExt_LeanSetLocalPosZ_mDEF497EF11DC45905E787A89A835EDC0BB24CE40 ();
// 0x00000422 UnityEngine.Color LeanTweenExt::LeanColor(UnityEngine.Transform)
extern void LeanTweenExt_LeanColor_m3C101DA3D605DD4228FD3E6BD67DA7B2C6D61438 ();
// 0x00000423 System.Void CFX2_Demo::OnMouseDown()
extern void CFX2_Demo_OnMouseDown_mD6B1074EECF2FCF1D56D48B5686D16D579F081DB ();
// 0x00000424 UnityEngine.GameObject CFX2_Demo::spawnParticle()
extern void CFX2_Demo_spawnParticle_m454C46458ED3CAD9F0766C01CD4374B09158C1CF ();
// 0x00000425 System.Void CFX2_Demo::OnGUI()
extern void CFX2_Demo_OnGUI_m198082F45AB35B29FFC863367860B4FC1236EC2B ();
// 0x00000426 System.Collections.IEnumerator CFX2_Demo::RandomSpawnsCoroutine()
extern void CFX2_Demo_RandomSpawnsCoroutine_mCEC8290770FB2A6AF7C2AE9A971516C4F2B4F38E ();
// 0x00000427 System.Void CFX2_Demo::Update()
extern void CFX2_Demo_Update_mFD7432FC01D62E3CBBB7B9CF2C43CD43D74C1B12 ();
// 0x00000428 System.Void CFX2_Demo::prevParticle()
extern void CFX2_Demo_prevParticle_m8954469C9EBCE0E55269709D5C555F46933EB57C ();
// 0x00000429 System.Void CFX2_Demo::nextParticle()
extern void CFX2_Demo_nextParticle_m79B7E31DF6A58936EEA308DB28F799E9915B8ADA ();
// 0x0000042A System.Void CFX2_Demo::.ctor()
extern void CFX2_Demo__ctor_m934DD748B71A67C218215894636FF8810C04FDFE ();
// 0x0000042B System.Void CFX_Demo_RandomDir::Awake()
extern void CFX_Demo_RandomDir_Awake_m28D19806B87ACE5DAA109ACFC0BE30A7C29D32AF ();
// 0x0000042C System.Void CFX_Demo_RandomDir::.ctor()
extern void CFX_Demo_RandomDir__ctor_m68DE80058501E3F4557491BA0511288EA4365931 ();
// 0x0000042D System.Void CFX_Demo_RotateCamera::Update()
extern void CFX_Demo_RotateCamera_Update_m63467A79E286EFE052F430CBC2C92D97C49B5EA4 ();
// 0x0000042E System.Void CFX_Demo_RotateCamera::.ctor()
extern void CFX_Demo_RotateCamera__ctor_m31569C8D4A062669CAC67506ABB7128ED09D03D2 ();
// 0x0000042F System.Void CFX_Demo_RotateCamera::.cctor()
extern void CFX_Demo_RotateCamera__cctor_m141862E78433EA4544705007E8C1329550E8982B ();
// 0x00000430 System.Void CFX2_AutoRotate::Start()
extern void CFX2_AutoRotate_Start_mDD7E24968E8CCF6D1B3F4AAAD677A1A022BF3F14 ();
// 0x00000431 System.Void CFX2_AutoRotate::Update()
extern void CFX2_AutoRotate_Update_m9FAD02A93B133E508FAFA70109F789E9CF9D5A52 ();
// 0x00000432 System.Void CFX2_AutoRotate::.ctor()
extern void CFX2_AutoRotate__ctor_mCE858ED05692AF1622B501C5DC9D0AD3D3CD58DF ();
// 0x00000433 System.Void CFX_AutoDestructShuriken::OnEnable()
extern void CFX_AutoDestructShuriken_OnEnable_m820E068B952492BE214932B6E6981DC9F4179607 ();
// 0x00000434 System.Collections.IEnumerator CFX_AutoDestructShuriken::CheckIfAlive()
extern void CFX_AutoDestructShuriken_CheckIfAlive_m73EC45CBDF4C2556242F3C5D6E34FA7ACDC32885 ();
// 0x00000435 System.Void CFX_AutoDestructShuriken::.ctor()
extern void CFX_AutoDestructShuriken__ctor_mAE78A3C4A17595164EFAE7BB23A5B0D7B12A4DDA ();
// 0x00000436 System.Void CFX_AutodestructWhenNoChildren::Update()
extern void CFX_AutodestructWhenNoChildren_Update_mB48205AD814F8EB7CA47BE78C274EB7EF1ABED9C ();
// 0x00000437 System.Void CFX_AutodestructWhenNoChildren::.ctor()
extern void CFX_AutodestructWhenNoChildren__ctor_m9B85A5FB0FE2C28EF290D11D7ECD3608153F28A7 ();
// 0x00000438 System.Void CFX_LightIntensityFade::Start()
extern void CFX_LightIntensityFade_Start_m24228E934E3D33407ECAAE11D847B8D039EDB8AE ();
// 0x00000439 System.Void CFX_LightIntensityFade::OnEnable()
extern void CFX_LightIntensityFade_OnEnable_m0790466897F96BE7FE251C42266202EF212BB5FB ();
// 0x0000043A System.Void CFX_LightIntensityFade::Update()
extern void CFX_LightIntensityFade_Update_m7A8519E7F13A95015FFDF3286E4D36187439F4FE ();
// 0x0000043B System.Void CFX_LightIntensityFade::.ctor()
extern void CFX_LightIntensityFade__ctor_m21702BB3DE6D56EA026FAF1EB52C373CCFCF23A8 ();
// 0x0000043C System.Void CFX_ShurikenThreadFix::Awake()
extern void CFX_ShurikenThreadFix_Awake_mF7F575D5E2C6A9F52F3AC2F692B28CF9659CFEC5 ();
// 0x0000043D System.Collections.IEnumerator CFX_ShurikenThreadFix::WaitFrame()
extern void CFX_ShurikenThreadFix_WaitFrame_mA41CF60521F45DFF968C6C932823B9D45579E015 ();
// 0x0000043E System.Void CFX_ShurikenThreadFix::.ctor()
extern void CFX_ShurikenThreadFix__ctor_mF832E2EFBFFB98EED172E417BCADC6D7E1C38BA1 ();
// 0x0000043F UnityEngine.GameObject CFX_SpawnSystem::GetNextObject(UnityEngine.GameObject,System.Boolean)
extern void CFX_SpawnSystem_GetNextObject_m7E6CD3C70DE44AF7BFC8CC5A66500C55C27F9CB7 ();
// 0x00000440 System.Void CFX_SpawnSystem::PreloadObject(UnityEngine.GameObject,System.Int32)
extern void CFX_SpawnSystem_PreloadObject_m428725AFB10A106AE042C732963EDB9EB63F6222 ();
// 0x00000441 System.Void CFX_SpawnSystem::UnloadObjects(UnityEngine.GameObject)
extern void CFX_SpawnSystem_UnloadObjects_m252B7E272A76A61F7EA0BA567681061B0F77ECC6 ();
// 0x00000442 System.Boolean CFX_SpawnSystem::get_AllObjectsLoaded()
extern void CFX_SpawnSystem_get_AllObjectsLoaded_mBC6802FE5B19C419CEE20B3B5B68FC55D975EB0B ();
// 0x00000443 System.Void CFX_SpawnSystem::addObjectToPool(UnityEngine.GameObject,System.Int32)
extern void CFX_SpawnSystem_addObjectToPool_mC6F9F7883E30FEFF6429322325117BDF4353DF45 ();
// 0x00000444 System.Void CFX_SpawnSystem::removeObjectsFromPool(UnityEngine.GameObject)
extern void CFX_SpawnSystem_removeObjectsFromPool_mB40EFBC56E00F16B44A3760BFDCC017807478299 ();
// 0x00000445 System.Void CFX_SpawnSystem::Awake()
extern void CFX_SpawnSystem_Awake_m1440873E859D18C88AC10A1342B5F152A6D8913C ();
// 0x00000446 System.Void CFX_SpawnSystem::Start()
extern void CFX_SpawnSystem_Start_m3F2BBCD24AEAD78957401BF7E7A89D3AFC80563A ();
// 0x00000447 System.Void CFX_SpawnSystem::.ctor()
extern void CFX_SpawnSystem__ctor_mC9A1988CE77CC7860268B84CF1F5B62A09A613F5 ();
// 0x00000448 System.Void MainScript::LogMessage(System.String,System.String)
extern void MainScript_LogMessage_mE71A5E26022CB5670420942BB736618CF85C3F0B ();
// 0x00000449 System.Void MainScript::Get()
extern void MainScript_Get_m40F54A362B3BD2AF9D9D4C11FC793BFFCBC0D829 ();
// 0x0000044A System.Void MainScript::Post()
extern void MainScript_Post_m5CBD6BB2A8AD379C6E839DC31AB4BC02696833AF ();
// 0x0000044B System.Void MainScript::Put()
extern void MainScript_Put_m81EEF338104C4BF925D074BEFA99AED7169D1E92 ();
// 0x0000044C System.Void MainScript::Delete()
extern void MainScript_Delete_m61C09220BC359F4A722F86E1861CF43B09AFE389 ();
// 0x0000044D System.Void MainScript::AbortRequest()
extern void MainScript_AbortRequest_mFD9F086798CC3C77B99B7E539F42555587358445 ();
// 0x0000044E System.Void MainScript::DownloadFile()
extern void MainScript_DownloadFile_mD1AC16E006883F0859EEA1191E5441C194257181 ();
// 0x0000044F System.Void MainScript::.ctor()
extern void MainScript__ctor_m0CE0E3FC499A77C04A65041CF01C7FF3B077EB1F ();
// 0x00000450 System.Void MainScript::<Post>b__4_0(Models.Post)
extern void MainScript_U3CPostU3Eb__4_0_m18C7C32083A6F5DF7D5C0662330CCD6E30FB2F41 ();
// 0x00000451 System.Void MainScript::<Post>b__4_1(System.Exception)
extern void MainScript_U3CPostU3Eb__4_1_mB08CD54A3F07695236A44128B16030A4D601AD2E ();
// 0x00000452 System.Void MainScript::<Put>b__5_0(Proyecto26.RequestException,Proyecto26.ResponseHelper,Models.Post)
extern void MainScript_U3CPutU3Eb__5_0_mAA12574E51D788B962126A2E3985E2C0E30E418F ();
// 0x00000453 System.Void MainScript::<Delete>b__6_0(Proyecto26.RequestException,Proyecto26.ResponseHelper)
extern void MainScript_U3CDeleteU3Eb__6_0_m7184949EA26F4C9A4F50E9D9ECBA3F6E1ECD92F5 ();
// 0x00000454 System.Void MainScript::<DownloadFile>b__8_0(Proyecto26.ResponseHelper)
extern void MainScript_U3CDownloadFileU3Eb__8_0_m5C29038407FD94F4368ABE37CE65EC3338913300 ();
// 0x00000455 System.Void MainScript::<DownloadFile>b__8_1(System.Exception)
extern void MainScript_U3CDownloadFileU3Eb__8_1_m5A1A1F5375A3DE53A686B7F4240D07DB5EC2E81F ();
// 0x00000456 System.Void AppReg::ChangeBaseURLAPI(System.Boolean)
extern void AppReg_ChangeBaseURLAPI_mFBC2C33065F8228F189CABD02FDE2AC64070DA7E ();
// 0x00000457 System.String AppReg::MixCategories(System.Int32[])
extern void AppReg_MixCategories_mA238CA4A8159A1C94BD0DAFFDF7F8125A3349B62 ();
// 0x00000458 System.String AppReg::GetBU(System.Int32)
extern void AppReg_GetBU_mE3223374432E1D99C5E3C31EA6AA3BCF889EE566 ();
// 0x00000459 System.String[] AppReg::GetCat(System.Int32)
extern void AppReg_GetCat_m7524AD82BFC2B054F34E25AAA640AB953F9680F6 ();
// 0x0000045A System.Void AppReg::.ctor()
extern void AppReg__ctor_mA2EAEB436A78BCB729C34BD4723730C2B1EF81F2 ();
// 0x0000045B System.Void AppReg::.cctor()
extern void AppReg__cctor_m3C8ECFAFC08C6652A01CAB185C6EEAE583C04083 ();
// 0x0000045C System.Void CanvasConfig::Awake()
extern void CanvasConfig_Awake_m518A2EDB4E9B128176B7833EFD34415532C030E6 ();
// 0x0000045D System.Void CanvasConfig::ChineseSetting()
extern void CanvasConfig_ChineseSetting_m544C33A7E68EDF4E28B1331F3C4FF30C354F7868 ();
// 0x0000045E System.Collections.Generic.List`1<UnityEngine.Touch> CanvasConfig::ObjTapOnCanvas(System.Int32)
extern void CanvasConfig_ObjTapOnCanvas_m2E093D60BA786014047E6774CBEF939A5ACFBE61 ();
// 0x0000045F System.Int32 CanvasConfig::DetectHowManyTouchOnCanvas(System.Int32)
extern void CanvasConfig_DetectHowManyTouchOnCanvas_mF2D1DE869C35437C68282212F531B4EA146BE68B ();
// 0x00000460 System.Void CanvasConfig::SetSizePerCanvas()
extern void CanvasConfig_SetSizePerCanvas_mE5543AC225B917E0456544E2B202CC50929636AC ();
// 0x00000461 System.Void CanvasConfig::SetDistance()
extern void CanvasConfig_SetDistance_m4216BF0760941D9EA7895EF37E2923445D7A2DE4 ();
// 0x00000462 System.Int32 CanvasConfig::DownPoint(UnityEngine.GameObject)
extern void CanvasConfig_DownPoint_m41458F65287CFE57CF03F4B6DB9F7EDC860FD478 ();
// 0x00000463 System.Boolean CanvasConfig::CheckDownPoint(UnityEngine.GameObject)
extern void CanvasConfig_CheckDownPoint_m21805350BD31467074CF18307D7C2FBD2992085A ();
// 0x00000464 System.Boolean CanvasConfig::CheckUpPoint(UnityEngine.GameObject)
extern void CanvasConfig_CheckUpPoint_m0967FBDF75CC654F7D8E87AA9D71E407B4AE612E ();
// 0x00000465 UnityEngine.Transform CanvasConfig::GetParentItem(System.Int32,System.Int32)
extern void CanvasConfig_GetParentItem_mB4DB5ADFF95AA6140F398569D642A341EC19BCCB ();
// 0x00000466 System.Void CanvasConfig::TeaserOpenUp(UnityEngine.GameObject,System.Int32)
extern void CanvasConfig_TeaserOpenUp_mFA2139579332FB9D781F8D010116EBA284C42082 ();
// 0x00000467 System.Void CanvasConfig::TeaserCloseDown(UnityEngine.GameObject,System.Int32)
extern void CanvasConfig_TeaserCloseDown_mE6AAE2F349F676CD9FA502A1A59209B2471ADAD6 ();
// 0x00000468 System.Void CanvasConfig::SetTouchToDiscover(System.Int32)
extern void CanvasConfig_SetTouchToDiscover_m0FA62590AA6512DDE61D84F1CCE60F1528F86E5F ();
// 0x00000469 System.Void CanvasConfig::CountDown(System.Int32)
extern void CanvasConfig_CountDown_m698E971A80DB00291215897804F2C3F58FB18B7F ();
// 0x0000046A System.Collections.IEnumerator CanvasConfig::StartCountDown(System.Int32)
extern void CanvasConfig_StartCountDown_mE795ECE7B990D91CAE263E48BD93785A42456A72 ();
// 0x0000046B System.Void CanvasConfig::IdleTimer(System.Int32)
extern void CanvasConfig_IdleTimer_m1BE0C46E819E57D213D91C3874997EF3BC7DC446 ();
// 0x0000046C System.Collections.IEnumerator CanvasConfig::StartIdlingTimer(System.Int32)
extern void CanvasConfig_StartIdlingTimer_mFB5A4E9FE2F901B4307DD42722E03B1E3C827DD2 ();
// 0x0000046D System.Void CanvasConfig::SetCanvasSize(System.Int32)
extern void CanvasConfig_SetCanvasSize_m08F400E9D37D9075EED9382F5D26146A322D3EC3 ();
// 0x0000046E System.Single CanvasConfig::MidPoint(System.Int32)
extern void CanvasConfig_MidPoint_mCD2AF0C6398F8BBFB81C8F23F6CFB4399390DDFC ();
// 0x0000046F System.Void CanvasConfig::SetTapEffect(System.Int32)
extern void CanvasConfig_SetTapEffect_m999990D08E6F7AC9FF8D7114916985029C958853 ();
// 0x00000470 System.Void CanvasConfig::TapEffect(UnityEngine.GameObject)
extern void CanvasConfig_TapEffect_m30EDF52DC0760413A84A5377D22EC8743604C98E ();
// 0x00000471 System.Void CanvasConfig::CheckFreezeImages(System.Int32)
extern void CanvasConfig_CheckFreezeImages_m5D4E655E024842C42DE27BFCC74EC164667C2A16 ();
// 0x00000472 System.Void CanvasConfig::CancelCheckFreeze(System.Int32)
extern void CanvasConfig_CancelCheckFreeze_m3E1C8CF54D02DDD0D34E04F63C04FBF233D63023 ();
// 0x00000473 UnityEngine.Vector2 CanvasConfig::ReturnCanvasPos(UnityEngine.Touch)
extern void CanvasConfig_ReturnCanvasPos_mB86C00A243846704AD8ECF47E730F8B23AD9A495 ();
// 0x00000474 UnityEngine.Vector2 CanvasConfig::ReturnCanvasPos_M(UnityEngine.Touch)
extern void CanvasConfig_ReturnCanvasPos_M_m40ECECBDC474F1FFA2468E0DA12B7810D4897C1F ();
// 0x00000475 System.Void CanvasConfig::DeactivateColliderWhileSPV(UnityEngine.GameObject,System.Int32)
extern void CanvasConfig_DeactivateColliderWhileSPV_m07CC0C189A02964441382573EE440EB5F1621F2E ();
// 0x00000476 System.Void CanvasConfig::.ctor()
extern void CanvasConfig__ctor_m09F97D333BF67726723D4DE6FBBEC4D348C83443 ();
// 0x00000477 System.Void CodeTexture::.ctor()
extern void CodeTexture__ctor_m2B7B0E61AAA2B356C4B6680DC74C29E73592E650 ();
// 0x00000478 System.Void ImageConfig::Awake()
extern void ImageConfig_Awake_m362FAF1147F20671CDD95640EAD1C5B3C39E4D5A ();
// 0x00000479 System.Void ImageConfig::Init(UnityEngine.GameObject)
extern void ImageConfig_Init_m37607F48D6090A485CD7C4C3B28CE3332CA5BA3D ();
// 0x0000047A System.Void ImageConfig::SpawnAtCol(System.Int32)
extern void ImageConfig_SpawnAtCol_m6EAD39B7E92CDC8A503BEB5D8555C947399A6F40 ();
// 0x0000047B System.Void ImageConfig::FirstSpawn()
extern void ImageConfig_FirstSpawn_mA3FD0AFA86A5A29E44FBD17B1D1760F0FD28F226 ();
// 0x0000047C System.Void ImageConfig::ReadImageAsync(System.Int32,System.Int32,System.Int32)
extern void ImageConfig_ReadImageAsync_mAC8269D3D1E9862F4FDEFC1185CE5220F896AE62 ();
// 0x0000047D System.String ImageConfig::FilePath(System.Int32,System.Int32)
extern void ImageConfig_FilePath_m5E209C92DF489163A1144F8D1374FA597F56A806 ();
// 0x0000047E System.Void ImageConfig::StreamRouter(System.Int32,System.Int32,System.Int32)
extern void ImageConfig_StreamRouter_m34FA4543CCAA0C871AE5551176073A9E42750B1E ();
// 0x0000047F System.Collections.IEnumerator ImageConfig::LoadTextures(Intelistyle.LatestProduct,System.Int32,System.Int32,System.Int32)
extern void ImageConfig_LoadTextures_m1407043ECC4B34FC98438757E4DC45DA39590204 ();
// 0x00000480 UnityEngine.Texture2D ImageConfig::AddTexture(UnityEngine.Texture2D,System.String,System.Boolean)
extern void ImageConfig_AddTexture_m0BA5672931AA5EA738A09E27FD3559F8CCB5D542 ();
// 0x00000481 System.Void ImageConfig::SpawnImg(System.Int32,System.Int32,System.Int32)
extern void ImageConfig_SpawnImg_m09D0E48D67B4BCFA01E7B1F4F514423FB72945BD ();
// 0x00000482 UnityEngine.Texture2D ImageConfig::CheckTexture(System.Int32,System.Int32)
extern void ImageConfig_CheckTexture_mE0F068384BEECB350EE9A5B601370B28FE0AA9A1 ();
// 0x00000483 System.Void ImageConfig::SpawnTexHorizontal(UnityEngine.Texture2D,System.Int32,System.Int32,System.Int32,Intelistyle.LatestProduct)
extern void ImageConfig_SpawnTexHorizontal_m8A14796B140B2BA817AED6EFFC916F132D2E8A3B ();
// 0x00000484 System.Void ImageConfig::SpawnTex(UnityEngine.Texture2D,System.Int32,System.Int32,System.Int32,Intelistyle.LatestProduct,System.Boolean,System.Int32)
extern void ImageConfig_SpawnTex_mF66039360B1B8DDBB5E544081AADC3B04EA20ECD ();
// 0x00000485 System.Void ImageConfig::SmartDetect(System.Int32,System.Int32,System.Int32)
extern void ImageConfig_SmartDetect_m756E1E4BB325171598166257879B00B1CAD7BBCC ();
// 0x00000486 System.Void ImageConfig::DownloadNextJson(System.Int32,System.Int32)
extern void ImageConfig_DownloadNextJson_mFEDE9D52646DF353342B3721CADD704E5CFBA82E ();
// 0x00000487 System.Void ImageConfig::DownloadingJSONDetailProduct(Intelistyle.LatestProduct,System.Int32)
extern void ImageConfig_DownloadingJSONDetailProduct_m63F249B140008F9BF48CF00010263650EB21AC29 ();
// 0x00000488 System.Void ImageConfig::SetSPVLImage(DetailProduct,System.Int32,System.String,System.Int32,System.Int32)
extern void ImageConfig_SetSPVLImage_mEEE82BE5D097A69D3B26B6A1084CFC42827E7ABC ();
// 0x00000489 System.Collections.IEnumerator ImageConfig::SetingSPVLImage(DetailProduct,System.Int32,System.String,System.Int32,System.Int32)
extern void ImageConfig_SetingSPVLImage_m5E018814133510EF24A217CEE5DAEF1838758D3B ();
// 0x0000048A System.Void ImageConfig::DownloadSPVLImage(DetailProduct,System.Int32,System.String,System.Int32,System.Int32)
extern void ImageConfig_DownloadSPVLImage_mF707F333E5C609AF435411D68527986CB10B43FD ();
// 0x0000048B System.Collections.IEnumerator ImageConfig::DownloadingSPVLimage(DetailProduct,System.Int32,System.String,System.Int32,System.Int32)
extern void ImageConfig_DownloadingSPVLimage_m1432B819FE8ED1B653EC4AADE5456F011D325D89 ();
// 0x0000048C System.String ImageConfig::ReturnXL_Link(System.String)
extern void ImageConfig_ReturnXL_Link_m755799ABC61E0E529BECF9C15B9F8237FC90849F ();
// 0x0000048D System.Void ImageConfig::DownloadSPVSingle(DetailProduct,System.Int32,System.String,System.Int32)
extern void ImageConfig_DownloadSPVSingle_mC9C9CA33C253F29B58A1D4389A38594AF09F8151 ();
// 0x0000048E System.Collections.IEnumerator ImageConfig::DownloadingSPVSingle(DetailProduct,System.Int32,System.String,System.Int32)
extern void ImageConfig_DownloadingSPVSingle_mA5C019FB2C58CE816CA786183D8F6A360AC09A32 ();
// 0x0000048F System.Void ImageConfig::ChangeSPVImage(System.Int32)
extern void ImageConfig_ChangeSPVImage_mA272A63CEBA876C3E28A79FF6FA798D72D6EDA80 ();
// 0x00000490 System.Void ImageConfig::DeChangeSPVImage(System.Int32)
extern void ImageConfig_DeChangeSPVImage_mF8F9EF8D14357D4F9DA0DE7D951FE7FA4618644C ();
// 0x00000491 System.Void ImageConfig::ResetSPVImage(System.Int32)
extern void ImageConfig_ResetSPVImage_mC45BA6695CE543186BB8D34C0EC269A0B0126822 ();
// 0x00000492 System.Void ImageConfig::DownloadingJSON(Proyecto26.RequestHelper,System.Int32)
extern void ImageConfig_DownloadingJSON_m34188416FD94BC90E1596576BEB4A1C451602870 ();
// 0x00000493 System.Void ImageConfig::SetSize(UnityEngine.GameObject)
extern void ImageConfig_SetSize_m1ED3DC909622830703E4681ADC9D93629E6B9BF1 ();
// 0x00000494 System.Void ImageConfig::WaitLoader(UnityEngine.GameObject)
extern void ImageConfig_WaitLoader_mE90BD0F058DEEA11513950873282809813765016 ();
// 0x00000495 System.Single ImageConfig::GetLineSize()
extern void ImageConfig_GetLineSize_mFAE45B88BF681D071D4A9FECC805F780120F20F1 ();
// 0x00000496 System.Single ImageConfig::GetMidPoint(System.Int32,System.Int32)
extern void ImageConfig_GetMidPoint_mA4F9CA7F7441FB05939FF48CE5269075260E876A ();
// 0x00000497 System.Void ImageConfig::SetPos(UnityEngine.GameObject,System.Int32,System.Int32,System.Boolean,UnityEngine.GameObject)
extern void ImageConfig_SetPos_m832C8E0D5CF727D39C04E9098B1CFF19D62D363D ();
// 0x00000498 System.Void ImageConfig::SetPosSimilar(UnityEngine.GameObject,System.Int32,System.Int32)
extern void ImageConfig_SetPosSimilar_mCC1BE89F2FF3D0E9F9867CF1E0E34A7AA0912A95 ();
// 0x00000499 System.Void ImageConfig::Finish(UnityEngine.GameObject)
extern void ImageConfig_Finish_m9D41DC2F8F91470219723990DAAD0E22D7498447 ();
// 0x0000049A System.Boolean ImageConfig::CheckActiveRow(System.Int32)
extern void ImageConfig_CheckActiveRow_m292594F2DAB33DC0874E9C02CE97AFA4FF5657A8 ();
// 0x0000049B System.Collections.IEnumerator ImageConfig::DownloadInBG(System.Int32,System.Int32)
extern void ImageConfig_DownloadInBG_mD3E9B2A4F266F0439353A7C2BCF5E0987721FE79 ();
// 0x0000049C System.Void ImageConfig::CheckToMove(System.Int32)
extern void ImageConfig_CheckToMove_m0BB58B408B7848539BF532389942E8D9A1EF218F ();
// 0x0000049D System.Void ImageConfig::CheckDetailProduct(ImageMover)
extern void ImageConfig_CheckDetailProduct_mF81E7E1C7E3D7B78741ABAF48CA14B071974E42F ();
// 0x0000049E CodeTexture ImageConfig::GetTex(System.String)
extern void ImageConfig_GetTex_m45F9D8DB111D8EC2FD9D2510B8307BA5D94CE1C1 ();
// 0x0000049F UnityEngine.Texture2D ImageConfig::MakeTrilinear(UnityEngine.Texture2D)
extern void ImageConfig_MakeTrilinear_m28D3678A248104090EC84964E5098443AFA21ED5 ();
// 0x000004A0 System.Void ImageConfig::.ctor()
extern void ImageConfig__ctor_m909582D25EAB870E4E80D2530267DD47E72FBF6E ();
// 0x000004A1 System.Void ImageMover::Start()
extern void ImageMover_Start_m386EC86442B43C5AC0E273B153E27DB9C1658C9D ();
// 0x000004A2 System.Void ImageMover::Update()
extern void ImageMover_Update_mEFCB9EECF1B31AD1DDD41F744211C154C19AD746 ();
// 0x000004A3 System.Void ImageMover::MoveDirection(UnityEngine.RectTransform)
extern void ImageMover_MoveDirection_m0F60E4FD7FFA13A9DEA522594DBEF0846A3636B0 ();
// 0x000004A4 System.Void ImageMover::CekPos(UnityEngine.Vector2)
extern void ImageMover_CekPos_m44D60B7CFD6FB29A4DFCA03F5498E9FD3EA0682D ();
// 0x000004A5 System.Void ImageMover::.ctor()
extern void ImageMover__ctor_m63461C877EFA6D15F1D61CAD0B8D9C9CD367377D ();
// 0x000004A6 System.Void InputHandler::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void InputHandler_OnPointerDown_m73A421C5F4898DEB87CE0B7D648C6701870EE0B0 ();
// 0x000004A7 System.Void InputHandler::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void InputHandler_OnPointerUp_mEB2ACFD04FC731CC92DFCEDB9AE6FDA4356C16EA ();
// 0x000004A8 System.Void InputHandler::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void InputHandler_OnEndDrag_mCA09C2C53F68323B5C7726C9D2D87067059AC063 ();
// 0x000004A9 System.Void InputHandler::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void InputHandler_OnDrag_mEEF399021E12B48F8D90BBCB2044B181805359B0 ();
// 0x000004AA System.Void InputHandler::.ctor()
extern void InputHandler__ctor_mC0430AF0D0189A36436A3CA392BBCE44D470AA6A ();
// 0x000004AB System.Void InputManager::Awake()
extern void InputManager_Awake_m622BD99CCA0EDF4523438C59FD499A06C0B05F21 ();
// 0x000004AC System.Void InputManager::TouchManager()
extern void InputManager_TouchManager_mC0C244B8F117CD60723B806B0371AFFBBFA368EA ();
// 0x000004AD System.Boolean InputManager::ClickCarasoul(System.Int32,UnityEngine.Touch)
extern void InputManager_ClickCarasoul_mFC7E29A76B1E5FBD05132F83A328B03F16484D18 ();
// 0x000004AE System.Void InputManager::SetMoveDown(System.Int32,System.Int32,System.Boolean)
extern void InputManager_SetMoveDown_m11CC3536C1FFDBDD83CEF63D8BDEA7E5FD01DF41 ();
// 0x000004AF System.Void InputManager::SetUserActive(System.Boolean,System.Int32)
extern void InputManager_SetUserActive_mCC502F375515EEA0E3BC64A02ABBD98C48F200AD ();
// 0x000004B0 System.Void InputManager::ScrollingThrough(System.Boolean,System.Int32,UnityEngine.GameObject,System.Int32,System.Int32)
extern void InputManager_ScrollingThrough_mC0D2F96AF1EB40D2E77889F287EAE892763116B4 ();
// 0x000004B1 System.Boolean InputManager::DetectFingerSwipeHorizontal(UnityEngine.Vector2,UnityEngine.Vector2)
extern void InputManager_DetectFingerSwipeHorizontal_m0A81F6002B9885A78E1D83D5A5D1AF960DBF62CD ();
// 0x000004B2 System.Int32 InputManager::DetectFingerOnColumn(UnityEngine.Vector2)
extern void InputManager_DetectFingerOnColumn_m7F95772E03F3E2F690DEBFFE04E6EF0D2DD88FAE ();
// 0x000004B3 System.Int32 InputManager::DetectFingerOnRow(UnityEngine.Vector2)
extern void InputManager_DetectFingerOnRow_mF1CC2C065D5E9EBEF10673F1DA374139A46B5752 ();
// 0x000004B4 System.Boolean InputManager::GetDragDownStatus(System.Int32)
extern void InputManager_GetDragDownStatus_mB2BED5FEDFE308B8A5B0D7905DBEED14899C028F ();
// 0x000004B5 System.Int32 InputManager::GetIndex(ImageMover,System.Int32,System.Boolean,System.Boolean)
extern void InputManager_GetIndex_mA57531DFB4007B694B5D3989F8A5E24628217C42 ();
// 0x000004B6 System.Void InputManager::InitResponsiveScrolling(System.Int32,UnityEngine.RaycastHit2D,UnityEngine.Vector2,System.Int32)
extern void InputManager_InitResponsiveScrolling_m6AC7015F27D86F8FF56667E42C5281CF9E1DCEB2 ();
// 0x000004B7 System.Void InputManager::EndResponsiveScrolling(System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.GameObject,System.Boolean,UnityEngine.GameObject)
extern void InputManager_EndResponsiveScrolling_m81C6DE9A946B8C723FD5BFEA47E6CD4C6FE76DE4 ();
// 0x000004B8 System.Void InputManager::ResponsiveScrolling(System.Int32,UnityEngine.Vector2,System.Int32)
extern void InputManager_ResponsiveScrolling_m81F1BE1B5621CC323D05F47D94816D41840C7634 ();
// 0x000004B9 System.Void InputManager::CheckSwipeResponsive(System.Int32)
extern void InputManager_CheckSwipeResponsive_m354759FEE70B330FA6A8B5B0E07F49F19E0BE735 ();
// 0x000004BA System.Void InputManager::SwipeUpRes(System.Int32)
extern void InputManager_SwipeUpRes_m6C66BB036D76FA3478A27E514C79A96372776677 ();
// 0x000004BB System.Void InputManager::SwipeDownRes(System.Int32)
extern void InputManager_SwipeDownRes_m92887E85A0EAE3B9E973A54C3DBD9FDB27B9677A ();
// 0x000004BC System.Void InputManager::SwipeLeftRes()
extern void InputManager_SwipeLeftRes_m9CD08E1A3B1A15818914FEF049ADA0C176198AF4 ();
// 0x000004BD System.Void InputManager::SwipeRightRes()
extern void InputManager_SwipeRightRes_m23A9D82F458AC23A42476EA3C1FC7AF633D1F536 ();
// 0x000004BE System.Void InputManager::checkSwipe(System.Int32)
extern void InputManager_checkSwipe_m870FEFB1C9FE48F6A17BB1EAD7A1D8B22A145AB9 ();
// 0x000004BF System.Single InputManager::verticalMove(System.Int32)
extern void InputManager_verticalMove_mBD4ED32305C4F87443600DA49EDF9061B602A1C7 ();
// 0x000004C0 System.Single InputManager::horizontalValMove(System.Int32)
extern void InputManager_horizontalValMove_mED45F02C9EE2F8ADCCD931B4B2D41A49F9347F11 ();
// 0x000004C1 System.Void InputManager::OnSwipeUp(System.Int32)
extern void InputManager_OnSwipeUp_m19E8C99969D7E8E5408F1209DCA4125931F8E863 ();
// 0x000004C2 System.Void InputManager::OnSwipeDown(System.Int32)
extern void InputManager_OnSwipeDown_m563F2D6BB7296D11966D64BFA4AADE69F40D572E ();
// 0x000004C3 System.Void InputManager::OnSwipeLeft(System.Int32)
extern void InputManager_OnSwipeLeft_m5A307541C07CB2B32E3F573BAB343FA3DD8B261D ();
// 0x000004C4 System.Void InputManager::OnSwipeRight(System.Int32)
extern void InputManager_OnSwipeRight_m462C016E2D46BDED0438EDF1C1E2CA0E40C87E69 ();
// 0x000004C5 System.Void InputManager::.ctor()
extern void InputManager__ctor_m92E656DB5CA317F9006F3CD8CE443B7D15EE2A47 ();
// 0x000004C6 System.Void Main::Awake()
extern void Main_Awake_mF1A75A989CFFED83051CD144FF853DDD08C422C9 ();
// 0x000004C7 System.Void Main::Start()
extern void Main_Start_mF306944EE9F427229AA0BB76EAE0D3CBA61C026C ();
// 0x000004C8 System.Void Main::FixedUpdate()
extern void Main_FixedUpdate_m05FEE8510ED94BB359F543D57B04DF161856DFDF ();
// 0x000004C9 System.Void Main::SetOrientation()
extern void Main_SetOrientation_m39E8A202680C07DFF3091CA9407405D7CD3A3A89 ();
// 0x000004CA System.Void Main::Update()
extern void Main_Update_m43D4349D9BDBB3F441E461282AC09404734F904F ();
// 0x000004CB System.Void Main::ResetSettings()
extern void Main_ResetSettings_m756F2EBD4CCB8880EB41D70714F7BE6AA83C1EB0 ();
// 0x000004CC System.Void Main::InitMain()
extern void Main_InitMain_m3B4EFF7B0F091F065E1BC61EF7808300D700E79E ();
// 0x000004CD System.Void Main::CheckHomeAppliancesLP()
extern void Main_CheckHomeAppliancesLP_mC73A4AA2F3D8B8BEFD01D4C09B79CB3DC14E7632 ();
// 0x000004CE System.Void Main::AllCollider(System.Int32,System.Boolean)
extern void Main_AllCollider_m48ABB727C59E408280AE78DADA4AB047E5F4B41E ();
// 0x000004CF System.Collections.Generic.List`1<UnityEngine.Vector2[]> Main::GetItemFirstPos(System.Int32)
extern void Main_GetItemFirstPos_mC44AFA137B87716A10AC33565948903FFB0FFAC8 ();
// 0x000004D0 System.Void Main::SetFirstPos(System.Int32)
extern void Main_SetFirstPos_mEA5A7B3B773DC916431DC333B2281F29CC874EC6 ();
// 0x000004D1 System.Void Main::ZeroSpeedAtCol(System.Int32)
extern void Main_ZeroSpeedAtCol_m2DB265EF5A14CCE068FDF11B089FBB41D42E97E8 ();
// 0x000004D2 System.Void Main::NormalSpeedAtRow(System.Int32,System.Int32)
extern void Main_NormalSpeedAtRow_mA0351FCD6E7D091162F10ABBC03AC24227F2383E ();
// 0x000004D3 System.Void Main::NormalSpeed(System.Int32)
extern void Main_NormalSpeed_mA4FDBA20A07E4EB41A56DFBFF292F46ABD279A3F ();
// 0x000004D4 System.Void Main::SetAllSpeed()
extern void Main_SetAllSpeed_m19A3E02AC5D12CCB519B02E4DA00A9B5663EB825 ();
// 0x000004D5 UnityEngine.GameObject[] Main::GetArrTaps()
extern void Main_GetArrTaps_m9D01873DC80412319BA84C70EE38ECB2C20E3D45 ();
// 0x000004D6 System.Void Main::SetFirstPos(System.Int32,UnityEngine.Vector2)
extern void Main_SetFirstPos_m3DC89DBF5DD787681A78C2D7ACE7B96ACF4D5FEA ();
// 0x000004D7 UnityEngine.Vector2 Main::GetFirstPos(System.Int32)
extern void Main_GetFirstPos_m50BD0B4B9CA72854A40BFB6B0854FE0C1B9E33B9 ();
// 0x000004D8 System.Void Main::SetRays(System.Int32,UnityEngine.RaycastHit2D)
extern void Main_SetRays_m94AB5CBB0C23339F94726E1F80F9C963AED7D538 ();
// 0x000004D9 UnityEngine.RaycastHit2D Main::GetRays(System.Int32)
extern void Main_GetRays_mD945E757D2276ADA78AA66D2624063EC63CD47AA ();
// 0x000004DA UnityEngine.GameObject Main::GetTaps(System.Int32)
extern void Main_GetTaps_mB83E48E70DF647F0915A082070DE9044AAB28D98 ();
// 0x000004DB System.Void Main::SetTaps(System.Int32,UnityEngine.GameObject)
extern void Main_SetTaps_mA1CB8CAB381432111C419AA8CFE4980774D9FB9D ();
// 0x000004DC System.Single Main::GetSpeed(System.Int32,System.Int32)
extern void Main_GetSpeed_mEE9BE437FC4D01F6D1EFB5EC155C429D8936F2C2 ();
// 0x000004DD System.Void Main::SetSpeed(System.Int32,System.Single,System.Int32)
extern void Main_SetSpeed_m4A45141A3D45FAA4A07EDD9DBCC88047D128E0B3 ();
// 0x000004DE System.Single Main::GetFactorialScrolling(System.Single)
extern void Main_GetFactorialScrolling_mF2F32EA1D77F51EC17B00D9BE9209981236EB467 ();
// 0x000004DF System.Void Main::CheckCloseToDestroy()
extern void Main_CheckCloseToDestroy_mB5D8D164EA9CE303ED8C60926DD192B81C5FA692 ();
// 0x000004E0 System.Void Main::.ctor()
extern void Main__ctor_mCCF4399D4778FA3A061E6367C22EF40014C36EC8 ();
// 0x000004E1 System.Void SPVImages::.ctor(System.Int32,UnityEngine.Texture2D)
extern void SPVImages__ctor_mB899A6349B45AFCDDB82D9402629FADA2CE5F782 ();
// 0x000004E2 System.Void ModalConfig::Awake()
extern void ModalConfig_Awake_m029DA2C6A18E34F371C7ECF0AC8A6FFB873E2F20 ();
// 0x000004E3 System.Void ModalConfig::ResetIndexTM(System.Int32)
extern void ModalConfig_ResetIndexTM_m93DF5B981613936A5C416E3EA74A0091CA003BAE ();
// 0x000004E4 System.Void ModalConfig::ResetTMPos(System.Int32)
extern void ModalConfig_ResetTMPos_m76783FB00146A51AD88CAEA9B395CE76DA5A8E1C ();
// 0x000004E5 System.Void ModalConfig::ChangeModalFromCarasouls(TopModal,System.Int32)
extern void ModalConfig_ChangeModalFromCarasouls_m016624A470ABD6E7C37BDA9C5775F29B4AFCA256 ();
// 0x000004E6 System.Void ModalConfig::SetSideModalImg(UnityEngine.Transform,UnityEngine.Texture2D[],System.Int32)
extern void ModalConfig_SetSideModalImg_m15669B7BFF1F329B7761E136FEAA0BB578387B76 ();
// 0x000004E7 System.Void ModalConfig::ActivatePinchIcon(System.Int32)
extern void ModalConfig_ActivatePinchIcon_mAF7B6DA149091FA262525ECAD31BE7372479D6BD ();
// 0x000004E8 System.Void ModalConfig::SetPriceBox(UnityEngine.RectTransform,UnityEngine.RectTransform,UnityEngine.RectTransform,UnityEngine.RectTransform,UnityEngine.RectTransform,UnityEngine.RectTransform,UnityEngine.RectTransform,System.Boolean)
extern void ModalConfig_SetPriceBox_mA596FDDD0ACE63E11DE1071DFD0ACC8BEB77ADF4 ();
// 0x000004E9 System.Void ModalConfig::SetBrandBoxPosition(UnityEngine.RectTransform,UnityEngine.RectTransform,UnityEngine.RectTransform,UnityEngine.RectTransform,System.Single,System.Boolean)
extern void ModalConfig_SetBrandBoxPosition_mE333106F90E846556C4CF450366C6ADFC3CAC461 ();
// 0x000004EA System.Void ModalConfig::SetSPVBG(System.Int32)
extern void ModalConfig_SetSPVBG_m1E4EE9F3A376B3E0EB27062C9D7CF0EDA3C061CF ();
// 0x000004EB System.Void ModalConfig::SetBotPagination(System.Int32)
extern void ModalConfig_SetBotPagination_mFFCFA5E1DEAB671DCFFE0B4AB8F52E0789DD90BF ();
// 0x000004EC System.Void ModalConfig::SetTopPagination(UnityEngine.Transform,System.Boolean,System.Int32,System.Boolean,System.Int32)
extern void ModalConfig_SetTopPagination_m2892F518808F17EB18EDCC6E0723A25B14E32606 ();
// 0x000004ED System.Void ModalConfig::HideSideTM(DetailProduct,System.Int32,System.String)
extern void ModalConfig_HideSideTM_m15CE44875FB3B0713758E93FABE6711C45E73D27 ();
// 0x000004EE System.Void ModalConfig::ActivateSPV(ImageMover)
extern void ModalConfig_ActivateSPV_m5CFFE9654DC58CCE3CBAAA98CC3DD6DA61E104BD ();
// 0x000004EF System.Void ModalConfig::BackModal(System.Int32,UnityEngine.GameObject,UnityEngine.Transform)
extern void ModalConfig_BackModal_mE517F7F36BB62CE7A38A17A73714B4AF633C8FE1 ();
// 0x000004F0 System.Void ModalConfig::SetBottomModal(System.Int32,UnityEngine.Transform)
extern void ModalConfig_SetBottomModal_m56DC57C700A1AA2B412264E245F83D61D529CC8C ();
// 0x000004F1 System.Void ModalConfig::SetBottomText(UnityEngine.Transform,DetailProduct)
extern void ModalConfig_SetBottomText_mBCFF036A788D275F5A825E239FF22F742B0ADEC1 ();
// 0x000004F2 System.Void ModalConfig::FillTopModalInfo(System.Int32,UnityEngine.GameObject,System.String,System.String,System.Int32,System.String,System.String[])
extern void ModalConfig_FillTopModalInfo_mBDD5AE241B39ED4C0290FB261C43E899A0F0B5F3 ();
// 0x000004F3 System.Void ModalConfig::BottomModalSpecialCase(UnityEngine.GameObject,System.Int32)
extern void ModalConfig_BottomModalSpecialCase_m9E056D0F80133FFA51BAEE8E938DFC76732CF395 ();
// 0x000004F4 System.Void ModalConfig::AddCollider(UnityEngine.Transform,System.Boolean)
extern void ModalConfig_AddCollider_m46165DA4B12E431B9CC00D60BFA0FD7FCB985653 ();
// 0x000004F5 System.Void ModalConfig::DeactivateSPV(System.Int32)
extern void ModalConfig_DeactivateSPV_mE90A4F79F7B3F6E006A5807AAC79E8AEED8329AD ();
// 0x000004F6 System.Void ModalConfig::FlippingStatus(System.Int32,AppReg_HorizontalSwipe,UnityEngine.GameObject,System.Single,System.Boolean)
extern void ModalConfig_FlippingStatus_m34A1147EC45BAC9D00A55FFB23587A76C0F1B8CD ();
// 0x000004F7 System.Void ModalConfig::MovingModal(System.Int32,UnityEngine.GameObject,System.Single)
extern void ModalConfig_MovingModal_m2A38A035C0DAC8580CCF14A99DEEAD88CC0B1EFD ();
// 0x000004F8 System.Void ModalConfig::DetectModalOutside()
extern void ModalConfig_DetectModalOutside_mB396EDD98CD542BC6BBAE6216EDC1EBEE7663308 ();
// 0x000004F9 System.Void ModalConfig::CheckSPVLimit(System.Int32)
extern void ModalConfig_CheckSPVLimit_m70CDB41405C1C5F8C31C86D1AD0270B17B278548 ();
// 0x000004FA System.Void ModalConfig::CheckSPVOutside(System.Int32)
extern void ModalConfig_CheckSPVOutside_mDE1CF5340F6E675AF1436F9A22F3E5E846FA7E0A ();
// 0x000004FB System.Void ModalConfig::SetSPVsImage(System.Int32,System.Boolean)
extern void ModalConfig_SetSPVsImage_mE0A62095C2EDA27A3F25F793A386628BFD76CBF3 ();
// 0x000004FC System.Void ModalConfig::SetALLBotAlternative(System.Int32,System.Int32)
extern void ModalConfig_SetALLBotAlternative_mCECA586ADD0B8DF389DB932BD6A8D09BD40D92CB ();
// 0x000004FD System.Void ModalConfig::SetALLSPVImage(System.Int32,System.Int32)
extern void ModalConfig_SetALLSPVImage_m52AEE77B76A3FEC0DD26008D5B92B69721959683 ();
// 0x000004FE System.Void ModalConfig::SetBoldPagination(UnityEngine.Transform)
extern void ModalConfig_SetBoldPagination_m04D3722D50C68B3921E257524FFAF53EE4704B2D ();
// 0x000004FF System.Void ModalConfig::FlipingAnimation(System.Int32,System.Boolean,System.Boolean,System.Boolean)
extern void ModalConfig_FlipingAnimation_m9AFF285A4C69EB5A177E4C6715C3CC7070C64422 ();
// 0x00000500 System.Void ModalConfig::LeftModal(System.Int32,System.Boolean,System.Boolean,System.Boolean)
extern void ModalConfig_LeftModal_mBDA836C4AAFDD9BFEE1D344E2EDC2AB8559166AD ();
// 0x00000501 System.Void ModalConfig::RightModal(System.Int32,System.Boolean,System.Boolean,System.Boolean)
extern void ModalConfig_RightModal_mE3911E0DCDB19879055F01D1E88F4F84FCE96552 ();
// 0x00000502 System.Void ModalConfig::CheckExitBtnOnTopBox(System.Int32,System.Boolean)
extern void ModalConfig_CheckExitBtnOnTopBox_mA8124600BE4A9B71F6F22BE00F5CD6E58F3F06F9 ();
// 0x00000503 System.Void ModalConfig::MidModal(System.Int32,System.Boolean,System.Boolean,System.Boolean)
extern void ModalConfig_MidModal_m69D680968A4AC791DD34F09A59211125BC103550 ();
// 0x00000504 System.Void ModalConfig::InstantiateBack(UnityEngine.Transform,System.Int32)
extern void ModalConfig_InstantiateBack_mDCEE85C6C42152E29397A0A1AC33C7F8779B4F3D ();
// 0x00000505 System.Void ModalConfig::InstantiateClose(UnityEngine.Transform)
extern void ModalConfig_InstantiateClose_m2955CA4F3F43CB81E6D994D8397DBCCFD739262C ();
// 0x00000506 UnityEngine.Texture2D ModalConfig::GetTexture2D(System.Int32,System.Int32)
extern void ModalConfig_GetTexture2D_mBC8B9195F009705615F91C0E714485EB1646F24A ();
// 0x00000507 System.Void ModalConfig::AddTexture2dSPV(System.Int32,System.Int32,UnityEngine.Texture2D)
extern void ModalConfig_AddTexture2dSPV_m1CE8BB496F4D9A94EBBA64F542A694171D28FDB1 ();
// 0x00000508 System.Void ModalConfig::AddBigSPV(System.Int32,System.Int32,UnityEngine.Texture2D)
extern void ModalConfig_AddBigSPV_mC3A1D2B52DC056D18E7E4203ABEC96DA2A7788F9 ();
// 0x00000509 SPVImages ModalConfig::GetBigSize(System.Int32,System.Int32)
extern void ModalConfig_GetBigSize_m6D6B4ECF4D767EA9CEC235EC849B0D084FE08941 ();
// 0x0000050A System.Collections.Generic.List`1<SPVImages> ModalConfig::GetBigSizeSPV(System.Int32)
extern void ModalConfig_GetBigSizeSPV_m7486D02AD01799576244985506ADC5DF25654CD1 ();
// 0x0000050B System.Collections.Generic.List`1<SPVImages> ModalConfig::GetTexture2DSPV(System.Int32)
extern void ModalConfig_GetTexture2DSPV_m2FBCB1BFCB9D0DB202F6BE0BC5B78DC83CD8F3C8 ();
// 0x0000050C UnityEngine.Transform ModalConfig::GetBotModal(System.Int32,ModalConfig_BotModalSide)
extern void ModalConfig_GetBotModal_m3FE90D15FB08D60134E0060A49A14BA1BC22A53D ();
// 0x0000050D System.Void ModalConfig::ResetScollBarPosition(System.Int32)
extern void ModalConfig_ResetScollBarPosition_m527D23CA5DE6E6AF26F1DAFD9590238103BA4AFF ();
// 0x0000050E UnityEngine.Transform ModalConfig::GetTopModal(System.Int32,ModalConfig_TopModalSide)
extern void ModalConfig_GetTopModal_m6D82D8F3FFC9B723B78CCCB59B1080D98BC08AF4 ();
// 0x0000050F System.Void ModalConfig::SetImageSizeTM(UnityEngine.Transform,UnityEngine.RectTransform)
extern void ModalConfig_SetImageSizeTM_mBD4B7D7D30C2DB38C6F30674979987C8C543DC28 ();
// 0x00000510 System.Void ModalConfig::SetImageSizeBig(UnityEngine.Transform,UnityEngine.RectTransform)
extern void ModalConfig_SetImageSizeBig_mFD571B30110A894D49B8FB63741FBDB3CEACB06E ();
// 0x00000511 System.Void ModalConfig::.ctor()
extern void ModalConfig__ctor_m3EF58744BB846652CBCCA690F449A950FA49B686 ();
// 0x00000512 System.Void ModalConfig::.cctor()
extern void ModalConfig__cctor_m551684EE6D1D8247F9AF65E9AF2B860E87918F1C ();
// 0x00000513 System.Void MouseManager::Awake()
extern void MouseManager_Awake_mD369B4BF2D650F29418D181F951A94121D93657E ();
// 0x00000514 System.Void MouseManager::Start()
extern void MouseManager_Start_m99014BB61EFA2CACB12CA5545C1C5769BFA1F277 ();
// 0x00000515 System.Void MouseManager::_MouseManager()
extern void MouseManager__MouseManager_mD63E6C8E3316C4C39335D02BBEDD6CB87986890F ();
// 0x00000516 System.Boolean MouseManager::ClickCarasoul(System.Int32)
extern void MouseManager_ClickCarasoul_m6C1CD2130E0B8341609140369B9AFEEB0992B1C9 ();
// 0x00000517 System.Void MouseManager::SwipeBotAlternative(UnityEngine.GameObject,System.Int32,UnityEngine.Vector2)
extern void MouseManager_SwipeBotAlternative_m88340B722DB13A3D9D3730627EE0A0EEA55A53C4 ();
// 0x00000518 System.Void MouseManager::SwipeTopAlternative(UnityEngine.GameObject,System.Int32,UnityEngine.Vector2)
extern void MouseManager_SwipeTopAlternative_mC2458054E15FDF756CCB53759BD29409B86BE458 ();
// 0x00000519 System.Void MouseManager::ScrollBarStatus(System.Int32,System.Boolean,System.String)
extern void MouseManager_ScrollBarStatus_m48BC0F6144391CE693E79651309C9D62456B6255 ();
// 0x0000051A System.String MouseManager::GetLastChild(System.Int32)
extern void MouseManager_GetLastChild_m75902EF8B006C4E24975C6C33766001E67A22D1F ();
// 0x0000051B System.Void MouseManager::ScrollingDescText(System.Int32,System.String)
extern void MouseManager_ScrollingDescText_m56D390C79491AAC678E4613B22E6318E80341ADD ();
// 0x0000051C System.Void MouseManager::ScrollingThroughMouse(System.Int32,System.Int32,System.Boolean)
extern void MouseManager_ScrollingThroughMouse_m8C9D38E0910EA56A6AB4A595E1B71FA1529DAA86 ();
// 0x0000051D System.Void MouseManager::ButtonClose(System.Int32)
extern void MouseManager_ButtonClose_m759EAF36C6E6105F986A3E7DB327DDF2888B6FF5 ();
// 0x0000051E System.Int32 MouseManager::GetIndex(ImageMover,System.Int32,System.Boolean,System.Boolean)
extern void MouseManager_GetIndex_mA68470DCB350C2DD71E599F0660FBCB9CE746149 ();
// 0x0000051F System.Void MouseManager::EndResponsiveScrolling(System.Int32,System.Int32,System.Boolean,System.Boolean,UnityEngine.GameObject)
extern void MouseManager_EndResponsiveScrolling_m4C70A8DEFEF0181E1A54326C8BDB9CEDA27EBB85 ();
// 0x00000520 System.Void MouseManager::InitResponsiveScrolling(System.Int32,UnityEngine.RaycastHit2D,UnityEngine.Vector2)
extern void MouseManager_InitResponsiveScrolling_m536D3CCAE6B28EC7AD2D3C24A2E0014E22678A19 ();
// 0x00000521 System.Void MouseManager::ResponsiveScrolling(System.Int32,UnityEngine.Vector2)
extern void MouseManager_ResponsiveScrolling_m7B05D7EF52E71F4979A18814FDC954E512A04A6E ();
// 0x00000522 System.Void MouseManager::CheckSwipeResponsive()
extern void MouseManager_CheckSwipeResponsive_m46F795C54AB1C5305F57F768BBA60F1B0D34E5F2 ();
// 0x00000523 System.Void MouseManager::SwipeUpRes()
extern void MouseManager_SwipeUpRes_m198F9EF5195AB900BF42CB9CAC275EAF0C046313 ();
// 0x00000524 System.Void MouseManager::SwipeDownRes()
extern void MouseManager_SwipeDownRes_m8B849E0188946011BEFE9486BD32B211B5853E9D ();
// 0x00000525 System.Void MouseManager::SwipeLeftRes()
extern void MouseManager_SwipeLeftRes_mDAA31E4973910860A6484556D13F2200A8FC5866 ();
// 0x00000526 System.Void MouseManager::SwipeRightRes()
extern void MouseManager_SwipeRightRes_m273F096944E51088675512D2378629A1C095E165 ();
// 0x00000527 System.Void MouseManager::checkSwipe()
extern void MouseManager_checkSwipe_m47973D210553A22241A8A8AEE30566DABDEBF7B1 ();
// 0x00000528 System.Single MouseManager::verticalMove()
extern void MouseManager_verticalMove_m87C1FFA56173D5E757BCBC5D68C5C411E1090DB9 ();
// 0x00000529 System.Single MouseManager::horizontalValMove()
extern void MouseManager_horizontalValMove_m2AEB31DF7A62D19DE709389666A827DA39FD67B5 ();
// 0x0000052A System.Void MouseManager::OnSwipeUp()
extern void MouseManager_OnSwipeUp_m2F681729CBD59DBF657CCADB146631A29FADE129 ();
// 0x0000052B System.Void MouseManager::OnSwipeDown()
extern void MouseManager_OnSwipeDown_m0B1D400E8B0D1124E253E72604E38137306DFCB1 ();
// 0x0000052C System.Void MouseManager::OnSwipeLeft()
extern void MouseManager_OnSwipeLeft_mDAB724D596310948E868B054DDF013D130D6FBEE ();
// 0x0000052D System.Void MouseManager::OnSwipeRight()
extern void MouseManager_OnSwipeRight_m0B0F54D10186030BF5A07E15F2F7A53BA05AD304 ();
// 0x0000052E System.Void MouseManager::.ctor()
extern void MouseManager__ctor_mF030DEE44FED499E3F86AE86DDCC0D1729084F28 ();
// 0x0000052F System.Void MouseManager::.cctor()
extern void MouseManager__cctor_m5B805DE586BFCA851E46F1CB5896702E96846158 ();
// 0x00000530 System.Void Product_Registry::.ctor()
extern void Product_Registry__ctor_m94B94BDFDFAD40552DF6E178DB14A045FE43C905 ();
// 0x00000531 System.Int32 ProductClass::get_Index()
extern void ProductClass_get_Index_m2901BF7277598F2E7019070B169454918F9BF980 ();
// 0x00000532 System.String ProductClass::get_Link()
extern void ProductClass_get_Link_m25DC63D18C6E6FBE4722AF4F85A4A49A68D4DA18 ();
// 0x00000533 System.Void ProductClass::.ctor()
extern void ProductClass__ctor_m7D8D4B89873EED9188357090CF39D85820EB3073 ();
// 0x00000534 System.Void SPVDetailProduct::OnEnable()
extern void SPVDetailProduct_OnEnable_m7ECE051EDE61C92CCF40B870A560F08EDA1CEF06 ();
// 0x00000535 System.Void SPVDetailProduct::RetryDP()
extern void SPVDetailProduct_RetryDP_m4A4562DF063B5D36138324A0117BB02F1829F799 ();
// 0x00000536 System.Void SPVDetailProduct::OnDisable()
extern void SPVDetailProduct_OnDisable_m442F60A9818A0B5A13473DA90C41CCA0EE478652 ();
// 0x00000537 System.Void SPVDetailProduct::.ctor()
extern void SPVDetailProduct__ctor_m0BC08056B5B0C0AE48A62447F98F4C395444D9C9 ();
// 0x00000538 System.Void Scroll_Update::Awake()
extern void Scroll_Update_Awake_m9EB9A06BDC2B7A27D79507E048AB18B259E9892B ();
// 0x00000539 System.Void Scroll_Update::Update()
extern void Scroll_Update_Update_m2B8378E682FEE6C37BCDE15389A6CE3E3FA1A100 ();
// 0x0000053A System.Void Scroll_Update::SetDelayNSpeed(System.Int32,System.Int32,System.Boolean)
extern void Scroll_Update_SetDelayNSpeed_m52B8346CC28F66EA0A4311A98F2611864D9488BF ();
// 0x0000053B System.Void Scroll_Update::ScrollHubber()
extern void Scroll_Update_ScrollHubber_m5250BEBA7DC2C86943F932B0DA0B93308EB0CA15 ();
// 0x0000053C System.Void Scroll_Update::ScrollRow1Col1()
extern void Scroll_Update_ScrollRow1Col1_m08ED0EC932E873728EC35BA4C6DDE48F228731AF ();
// 0x0000053D System.Void Scroll_Update::ScrollRow2Col1()
extern void Scroll_Update_ScrollRow2Col1_m2D4C7697F164AA74A22542FA0F2107A5DBA63A88 ();
// 0x0000053E System.Void Scroll_Update::ScrollRow3Col1()
extern void Scroll_Update_ScrollRow3Col1_m3BDD4BDC8415F9EE7811C6D0453A6B3D9CB561F8 ();
// 0x0000053F System.Void Scroll_Update::ScrollRow4Col1()
extern void Scroll_Update_ScrollRow4Col1_m748897E1C8E472E3A8950C7F526019C74634C8E2 ();
// 0x00000540 System.Void Scroll_Update::ScrollRow5Col1()
extern void Scroll_Update_ScrollRow5Col1_m00B654B87CE11831A2BD6AB115F609F927E4867D ();
// 0x00000541 System.Void Scroll_Update::ScrollRow6Col1()
extern void Scroll_Update_ScrollRow6Col1_mF794BEA144324DA4747C596561228C68FC5581C9 ();
// 0x00000542 System.Void Scroll_Update::ScrollRow7Col1()
extern void Scroll_Update_ScrollRow7Col1_m3003355E4535E6B718DA852DB863F0006ABE1836 ();
// 0x00000543 System.Void Scroll_Update::ScrollRow1Col2()
extern void Scroll_Update_ScrollRow1Col2_mA0D91BBEE1B241D2171B981914CFB7D8539548A6 ();
// 0x00000544 System.Void Scroll_Update::ScrollRow2Col2()
extern void Scroll_Update_ScrollRow2Col2_m78D61AD64ACF1A7076CA26B03EBD300F7A430A16 ();
// 0x00000545 System.Void Scroll_Update::ScrollRow3Col2()
extern void Scroll_Update_ScrollRow3Col2_m50F796928F7871611C14C07E9077A3064F4C6BD0 ();
// 0x00000546 System.Void Scroll_Update::ScrollRow4Col2()
extern void Scroll_Update_ScrollRow4Col2_mDCECF2B64D353B36DF14F3600B3018F638219DF5 ();
// 0x00000547 System.Void Scroll_Update::ScrollRow5Col2()
extern void Scroll_Update_ScrollRow5Col2_mA42B2FCA92F4E43BDF526DDB4F072A1D785F9FAF ();
// 0x00000548 System.Void Scroll_Update::ScrollRow6Col2()
extern void Scroll_Update_ScrollRow6Col2_mAFB23F505763D76E153832A83DFC912364DDC847 ();
// 0x00000549 System.Void Scroll_Update::ScrollRow7Col2()
extern void Scroll_Update_ScrollRow7Col2_m38B51E814628E90C01BE3E38C88437EBE43055BF ();
// 0x0000054A System.Void Scroll_Update::ScrollRow1Col3()
extern void Scroll_Update_ScrollRow1Col3_mF7CECC18007361C04BCD99D3DE85350E694E9BCD ();
// 0x0000054B System.Void Scroll_Update::ScrollRow2Col3()
extern void Scroll_Update_ScrollRow2Col3_mDFB0C16530BA8E55CFE1DA770D8629DA6756F5D6 ();
// 0x0000054C System.Void Scroll_Update::ScrollRow3Col3()
extern void Scroll_Update_ScrollRow3Col3_mCFC6879F603EDC518940498E2B43C3984F2A9C6E ();
// 0x0000054D System.Void Scroll_Update::ScrollRow4Col3()
extern void Scroll_Update_ScrollRow4Col3_mD4FE634FA0A69DFAAAC3E0E0A8557B2C707181F8 ();
// 0x0000054E System.Void Scroll_Update::ScrollRow5Col3()
extern void Scroll_Update_ScrollRow5Col3_mC09EB6CE228CBCD3333E432A34658CC8ADA5AA83 ();
// 0x0000054F System.Void Scroll_Update::ScrollRow6Col3()
extern void Scroll_Update_ScrollRow6Col3_m7A7FFE8FA642F8E226255EF4428B64A03EDE3DF4 ();
// 0x00000550 System.Void Scroll_Update::ScrollRow7Col3()
extern void Scroll_Update_ScrollRow7Col3_mF8CB04A184F07233D0F188886124CAFB6304FED0 ();
// 0x00000551 System.Void Scroll_Update::.ctor()
extern void Scroll_Update__ctor_mEE53421C7561104C94AC86379B8C6BC1199FE99C ();
// 0x00000552 System.Void ErrorItems::.ctor()
extern void ErrorItems__ctor_mE2C03654FCC4F21BBD89BC776341BFD19356C7AD ();
// 0x00000553 System.Void SimilarItems::Awake()
extern void SimilarItems_Awake_mDEB4D3F8C2F37F959EBB504AA7B9E6A531C7D86F ();
// 0x00000554 System.Void SimilarItems::DownloadAPIStyleIT(System.String)
extern void SimilarItems_DownloadAPIStyleIT_mED8B878E31CD172F613A5363CA5B552C323A39B4 ();
// 0x00000555 System.Void SimilarItems::GetAPIStyleIt(System.String,System.Int32,System.Int32)
extern void SimilarItems_GetAPIStyleIt_m8B0FD86780866A301F0395D5A99971C7A2D6D68E ();
// 0x00000556 System.Void SimilarItems::DownloadStyleIt(Intelistyle.LatestProduct[],System.Int32,System.Int32)
extern void SimilarItems_DownloadStyleIt_mD6C3FB558985ED5F12B9195404FF945E249BB4D1 ();
// 0x00000557 System.Collections.IEnumerator SimilarItems::DownloadingStyleIt2(Intelistyle.LatestProduct,System.Int32,System.Int32,UnityEngine.GameObject)
extern void SimilarItems_DownloadingStyleIt2_mDC3B8536F4F55DB1DCB4051313A59CBEB6942A0D ();
// 0x00000558 System.Void SimilarItems::SetStyleWithItLength(System.Int32,System.Int32,Intelistyle.LatestProduct[])
extern void SimilarItems_SetStyleWithItLength_m2A95CB0198330C91112B8BBA170EC663C8D0627D ();
// 0x00000559 System.Void SimilarItems::SetStyleToProduct(UnityEngine.GameObject,System.Int32,System.Int32,UnityEngine.Texture2D,Intelistyle.LatestProduct)
extern void SimilarItems_SetStyleToProduct_m0EF8DBE1C51FD594D0C2A3FF9AC6B073D801A096 ();
// 0x0000055A System.Void SimilarItems::RePositionCarasoul(System.Int32)
extern void SimilarItems_RePositionCarasoul_m865595E2766EB58059467A10DA9A3D03B7386ED5 ();
// 0x0000055B System.Void SimilarItems::DownloadDetailCarasoul(Intelistyle.LatestProduct,System.Int32)
extern void SimilarItems_DownloadDetailCarasoul_m10B59A7A5A27703D7F1EF544F471201169194BFA ();
// 0x0000055C System.Void SimilarItems::SubtlyRain(System.String,System.Int32,System.Int32)
extern void SimilarItems_SubtlyRain_m153D4ED3FA81F3C95CDBB58FB511279FA6F69DBF ();
// 0x0000055D System.Void SimilarItems::DownloadSubtly(Proyecto26.RequestHelper,System.Int32)
extern void SimilarItems_DownloadSubtly_mD7DB25BE800A386878C5FFC71B3058511350D644 ();
// 0x0000055E System.Void SimilarItems::GetTexForGOFwd(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.GameObject)
extern void SimilarItems_GetTexForGOFwd_m776D1D8443009F55089861F4F2C5AD5688F2183A ();
// 0x0000055F System.Void SimilarItems::GetTexForGOBACK(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.GameObject)
extern void SimilarItems_GetTexForGOBACK_m3244C0DAEFDCA7ED645049EC894DF75BA26C89FB ();
// 0x00000560 System.Collections.IEnumerator SimilarItems::SetLPAndTex(System.Int32,System.Int32,UnityEngine.Transform,System.Int32,System.Int32,System.Boolean)
extern void SimilarItems_SetLPAndTex_mDA2B79B374839CE33EBCBA3D9A613E54A7241210 ();
// 0x00000561 System.Void SimilarItems::GetDPSimilarAndContinue(Intelistyle.LatestProduct,UnityEngine.Transform,UnityEngine.Texture2D,System.Int32,System.Int32,System.Boolean,System.Boolean,System.Boolean)
extern void SimilarItems_GetDPSimilarAndContinue_mA0FA76F0563B601C765589221979216844D3C37A ();
// 0x00000562 System.Collections.IEnumerator SimilarItems::_GetDPSimilarAndContinue(Intelistyle.LatestProduct,UnityEngine.Transform,UnityEngine.Texture2D,System.Int32,System.Int32,System.Boolean,System.Boolean,System.Boolean)
extern void SimilarItems__GetDPSimilarAndContinue_m644F27F810A6CA9BB7E15252748E01344E7C12E8 ();
// 0x00000563 System.Void SimilarItems::DPFailed(System.Boolean,System.Boolean,ImageMover,System.Int32,UnityEngine.Transform,System.Int32,System.Boolean)
extern void SimilarItems_DPFailed_mE910CB1DD5961E6E1621027D4E4CA33BF16FCCC6 ();
// 0x00000564 System.Void SimilarItems::Downloading5Img(System.Int32,System.Int32,UnityEngine.Transform,System.Int32,System.Boolean)
extern void SimilarItems_Downloading5Img_m40BE7F13279E89768D5274A4C1922A3267003ABD ();
// 0x00000565 System.Collections.IEnumerator SimilarItems::Download5FirstImg(System.Int32,System.Int32,UnityEngine.Transform,System.Int32,System.Int32,System.Boolean,System.Boolean)
extern void SimilarItems_Download5FirstImg_m4D21C294228C217EE3282E844569C7CC537AB7D3 ();
// 0x00000566 System.Void SimilarItems::DownloadImgSubtly(System.Int32,System.Int32,System.Int32)
extern void SimilarItems_DownloadImgSubtly_m4B717DF8D578CABD2DEDEE4D0F9EE540B9C3690A ();
// 0x00000567 System.Void SimilarItems::GetStyleITSimilar(System.Int32,System.Int32)
extern void SimilarItems_GetStyleITSimilar_mB6F9EAA80769261BA2FB537EC20DDD6D361BF93B ();
// 0x00000568 System.Collections.IEnumerator SimilarItems::DownloadingImgSubtly(System.Int32,System.Int32)
extern void SimilarItems_DownloadingImgSubtly_m54FB937E24BD69E1840AFAC871934A0B1F3A59A8 ();
// 0x00000569 System.Void SimilarItems::SetSimilarProductsArr(Intelistyle.LatestProduct[],System.Int32,System.Int32)
extern void SimilarItems_SetSimilarProductsArr_m366A8505D019F52EB6AA3911928AB09395956DA9 ();
// 0x0000056A System.Void SimilarItems::SetSimilarWithOutGoBack(Intelistyle.LatestProduct[],System.Int32)
extern void SimilarItems_SetSimilarWithOutGoBack_m3ED4EAEFB54FE7663A41617D6538CC19D5EA52A9 ();
// 0x0000056B Intelistyle.LatestProduct SimilarItems::_GetLPGOBack(System.Int32,System.Int32)
extern void SimilarItems__GetLPGOBack_mD748407FCA2CFF8E2725AEDFA2D298B6B7664F48 ();
// 0x0000056C Intelistyle.LatestProduct[] SimilarItems::GetLPGoback(System.Int32)
extern void SimilarItems_GetLPGoback_mAC62743925F3F87E9BC80CD601B75A650F09205F ();
// 0x0000056D System.Void SimilarItems::ClearScrollBack(System.Int32)
extern void SimilarItems_ClearScrollBack_m9441EBB15ABE2C14620A7D8843D08D8DF41FB7E0 ();
// 0x0000056E System.Void SimilarItems::ClearLPSimilar(System.Int32)
extern void SimilarItems_ClearLPSimilar_mA55445263C65D99C58639E640FB3902CC94EC5E9 ();
// 0x0000056F Intelistyle.LatestProduct[] SimilarItems::GetSimilarProducts(System.Int32)
extern void SimilarItems_GetSimilarProducts_m466F87B10FA5CF91F45E4E65076C8485A6D03385 ();
// 0x00000570 System.Collections.Generic.List`1<Intelistyle.LatestProduct[]> SimilarItems::GetLPScrollBack(System.Int32)
extern void SimilarItems_GetLPScrollBack_m1444F3E3C095BE342B01E143CB902F84CFD7A6B9 ();
// 0x00000571 System.Collections.Generic.List`1<System.Int32[]> SimilarItems::GetLPScrollIndex(System.Int32)
extern void SimilarItems_GetLPScrollIndex_mCBDC1E1B5B9EB1ECFC7DB52D0DD1444414EA4408 ();
// 0x00000572 System.Void SimilarItems::AddET(System.String,System.Int32)
extern void SimilarItems_AddET_m8AE7ACC21B1602B077F8FA6D1A7A0DE911401112 ();
// 0x00000573 System.Void SimilarItems::AddErrorItems(ErrorItems)
extern void SimilarItems_AddErrorItems_m90003561AC865E97DF577B6EFCF9932A542716B5 ();
// 0x00000574 ErrorItems SimilarItems::GetErrorItem(System.String)
extern void SimilarItems_GetErrorItem_m44C1DD59E1D6AB696C7B0C0F654DB33AFB0CADB7 ();
// 0x00000575 UnityEngine.Texture2D SimilarItems::AddTextureSimilar(UnityEngine.Texture2D,System.String,System.Int32,System.Boolean)
extern void SimilarItems_AddTextureSimilar_m145CEDB83C33DEE6FD2C1B5151AAA5E8FB9B4C3D ();
// 0x00000576 UnityEngine.Texture2D SimilarItems::AddTextureUnSimilar(UnityEngine.Texture2D,System.String,System.Int32,System.Boolean)
extern void SimilarItems_AddTextureUnSimilar_m735FDADC0D662CA1FFD28A5C32DDD41F8581673E ();
// 0x00000577 System.Void SimilarItems::AddSimilarCT(System.Int32,System.String,UnityEngine.Texture2D)
extern void SimilarItems_AddSimilarCT_m7BE7A44EF01FD092CA787993B6660F34AA744ED8 ();
// 0x00000578 System.Void SimilarItems::AddUnsimilarCT(System.Int32,System.String,UnityEngine.Texture2D)
extern void SimilarItems_AddUnsimilarCT_m2C36CBA3E804C8975A56682EF5D82DC1DAA36161 ();
// 0x00000579 CodeTexture SimilarItems::GetUnSimilarCT(System.Int32,System.String)
extern void SimilarItems_GetUnSimilarCT_m60FCDB7E96B087FB8D92A77EA60EE8D928DE9590 ();
// 0x0000057A CodeTexture SimilarItems::GetSimilarCT(System.Int32,System.String)
extern void SimilarItems_GetSimilarCT_m4FAEC55474747A0240C9591CE3AD11A008A081DE ();
// 0x0000057B System.Void SimilarItems::ResetSimilarCT(System.Int32)
extern void SimilarItems_ResetSimilarCT_mA58BDD28344BF14DAD01403AE1202C8219242578 ();
// 0x0000057C System.Collections.Generic.List`1<CodeTexture> SimilarItems::GetSimilarCTs(System.Int32)
extern void SimilarItems_GetSimilarCTs_mBDEDF69EBCB47E75D61239D384DA8B9BA0981EEC ();
// 0x0000057D System.Collections.Generic.List`1<CodeTexture> SimilarItems::GetUnSimilarCTs(System.Int32)
extern void SimilarItems_GetUnSimilarCTs_mA2103A5DE8184E837471443366065423F09207A9 ();
// 0x0000057E System.Void SimilarItems::.ctor()
extern void SimilarItems__ctor_mD464FD848F21046A20A37EE79E084AC4C53FF795 ();
// 0x0000057F System.Void DownloadGOBackKeperluan::.ctor()
extern void DownloadGOBackKeperluan__ctor_mD9922444CD25729D36F22B7C6E09C8BAA2A66449 ();
// 0x00000580 System.Void StartOver::Awake()
extern void StartOver_Awake_mD22540DBEDEEC58C9286DA8670A94C610F464C33 ();
// 0x00000581 System.Void StartOver::CancelMaskTimer(System.Int32)
extern void StartOver_CancelMaskTimer_mB79FD78D70BF4483C094875081BBCBC70D1C29FE ();
// 0x00000582 System.Void StartOver::MaskCanvasTimer(System.Int32)
extern void StartOver_MaskCanvasTimer_m2ED222196AD29522274D3D50AD46F04F6EB176DF ();
// 0x00000583 System.Void StartOver::StartCountingDownIdleSPV(System.Int32)
extern void StartOver_StartCountingDownIdleSPV_m5C5688CD93E4BA8CD4898DD4C7D232D0187BC162 ();
// 0x00000584 System.Single StartOver::MidPoint(System.Int32)
extern void StartOver_MidPoint_mC3F810B6A1B978879A84AD3AD51AC8F7673A0DDB ();
// 0x00000585 System.Void StartOver::CancelngCDSPV(System.Int32)
extern void StartOver_CancelngCDSPV_mCA8A90344376DAFFDC65610B9F4E5F121B6ADD82 ();
// 0x00000586 System.Void StartOver::CoolingDownAfterSPV(System.Int32)
extern void StartOver_CoolingDownAfterSPV_m1EEBA8276382F7C00D7CF901B5CB9A27CBD2387D ();
// 0x00000587 System.Void StartOver::GoBackObj(System.Int32)
extern void StartOver_GoBackObj_m488AD3425CB61BE15FAB3620505D3CA86DD23E4E ();
// 0x00000588 System.Void StartOver::SetSPVPosIM(System.Int32)
extern void StartOver_SetSPVPosIM_mB86B1D5FBB379681190B7DAF84139CE4A589F679 ();
// 0x00000589 System.Void StartOver::Swiper(System.Int32,System.Boolean)
extern void StartOver_Swiper_mC7FAE7BAFDE8CF02C19DE72F113880EEDC193FE5 ();
// 0x0000058A System.Void StartOver::GoToDefaultState(System.Int32,System.Single,System.Boolean)
extern void StartOver_GoToDefaultState_mA041F9633FA88C3E498EEE74BD486DC68790CEDB ();
// 0x0000058B System.Void StartOver::SpawnGoBackItems(System.Int32,System.Int32,System.Int32)
extern void StartOver_SpawnGoBackItems_m62BC373D30779AFCEC9371C49BE40639C9CF44D5 ();
// 0x0000058C System.Collections.IEnumerator StartOver::DownloadImgGoBack(System.Int32,System.Int32,System.Int32)
extern void StartOver_DownloadImgGoBack_mDDE3FC3AFDAC65B86742459FFB95E4CDA3C7EE60 ();
// 0x0000058D System.Void StartOver::DownloadDPGoBack(System.Int32,System.Int32,System.Int32,UnityEngine.Texture2D)
extern void StartOver_DownloadDPGoBack_m16E5F9EEAF08EECA61CACA53387D40D931DA7CA4 ();
// 0x0000058E System.Void StartOver::SpawnTex(System.Int32,System.Int32,System.Int32,UnityEngine.Texture2D,Intelistyle.LatestProduct)
extern void StartOver_SpawnTex_mA76331A2DB25654E07DF255CF59AF3F82B049032 ();
// 0x0000058F System.Void StartOver::CheckSpawnHorizontal(System.Int32,System.Int32)
extern void StartOver_CheckSpawnHorizontal_m596B906B33EB4A43E0612294655EA6AB89784D5A ();
// 0x00000590 System.Void StartOver::FluidAwayRow(UnityEngine.Vector2)
extern void StartOver_FluidAwayRow_m8FEF81B7668EF7B6234E61174B94763C796A2367 ();
// 0x00000591 System.Void StartOver::ComebackFromFluid(System.Int32,System.Int32)
extern void StartOver_ComebackFromFluid_m497282C86C7074B14BBBB3044EAD1DCFB6E19B7F ();
// 0x00000592 System.Void StartOver::MoveAwayRow(System.Int32,System.Boolean)
extern void StartOver_MoveAwayRow_mA93F9D9C4B83233320EE136564DDF62C11989F73 ();
// 0x00000593 System.Void StartOver::ComebackRow(System.Int32,System.Boolean)
extern void StartOver_ComebackRow_m2FDED8E08B6D9420D65CFBCF10BAB8AA7D2C86FE ();
// 0x00000594 System.Void StartOver::CollingDownOnCol01(System.Single,System.Single)
extern void StartOver_CollingDownOnCol01_m41857C3145500B0FB0C5B0B61FAD360157FB8143 ();
// 0x00000595 System.Void StartOver::CollingDownOnCol11(System.Single,System.Single)
extern void StartOver_CollingDownOnCol11_m7F374115BA28D87FD2EE7C64115B22CDEE32CB58 ();
// 0x00000596 System.Void StartOver::CollingDownOnCol21(System.Single,System.Single)
extern void StartOver_CollingDownOnCol21_mCC04531946A3810253C803325566B3E7403354EA ();
// 0x00000597 System.Void StartOver::CollingDownOnCol31(System.Single,System.Single)
extern void StartOver_CollingDownOnCol31_m983BFA9BAD23B851CFED235F179F6815D963D00E ();
// 0x00000598 System.Void StartOver::CollingDownOnCol41(System.Single,System.Single)
extern void StartOver_CollingDownOnCol41_mFA7BF63DCA4890CD8E7B3C812085748EAF98C140 ();
// 0x00000599 System.Void StartOver::CollingDownOnCol02(System.Single,System.Single)
extern void StartOver_CollingDownOnCol02_mAA707305328A77E3EA0E6719063B2FF5C9B667BF ();
// 0x0000059A System.Void StartOver::CollingDownOnCol12(System.Single,System.Single)
extern void StartOver_CollingDownOnCol12_m713ED93F2738C444B022BE9D589D3E018C2CCFBF ();
// 0x0000059B System.Void StartOver::CollingDownOnCol22(System.Single,System.Single)
extern void StartOver_CollingDownOnCol22_m9689A47781A31074AB52BEE84A195012C7449EF6 ();
// 0x0000059C System.Void StartOver::CollingDownOnCol32(System.Single,System.Single)
extern void StartOver_CollingDownOnCol32_m3D04B89E1092486E8723471EA543C739CC0C2F84 ();
// 0x0000059D System.Void StartOver::CollingDownOnCol42(System.Single,System.Single)
extern void StartOver_CollingDownOnCol42_mE2D9D66FF580CD098F1539653DCBBFEE2494C6C6 ();
// 0x0000059E System.Void StartOver::CollingDownOnCol03(System.Single,System.Single)
extern void StartOver_CollingDownOnCol03_mFF072B1CC85947FCE9689EC762DA547C60D51D00 ();
// 0x0000059F System.Void StartOver::CollingDownOnCol13(System.Single,System.Single)
extern void StartOver_CollingDownOnCol13_m28A5F1F852FAC1AE6B95F7B6AB58F60BF4852C75 ();
// 0x000005A0 System.Void StartOver::CollingDownOnCol23(System.Single,System.Single)
extern void StartOver_CollingDownOnCol23_m77B8030F429DDC5802718D252374A66FB28A30F8 ();
// 0x000005A1 System.Void StartOver::CollingDownOnCol33(System.Single,System.Single)
extern void StartOver_CollingDownOnCol33_mC3808A1D099D01225E42AE9B3EFCC4DF56EC2496 ();
// 0x000005A2 System.Void StartOver::CollingDownOnCol43(System.Single,System.Single)
extern void StartOver_CollingDownOnCol43_m3C3D9D367E6CC9156999969F9496612649463709 ();
// 0x000005A3 System.Void StartOver::.ctor()
extern void StartOver__ctor_m6E7E8728BE662BF3B97B10A918AE8B2229F15F87 ();
// 0x000005A4 System.Void QueueRenderTexture::.ctor(System.String,UnityEngine.Texture2D)
extern void QueueRenderTexture__ctor_m1A92F453D94863226640588129902BCC795E3DED ();
// 0x000005A5 System.Void QueueRenderTexture::.cctor()
extern void QueueRenderTexture__cctor_m1775B3861B7FBDDF7608F64B5E322FF138AF1B5A ();
// 0x000005A6 System.Collections.Generic.List`1<DetailProduct> StaticStorage::GetDP(System.Int32)
extern void StaticStorage_GetDP_m344791E9D9B120D46F73FF96097810233CAF1EAB ();
// 0x000005A7 System.Collections.Generic.List`1<Intelistyle.LatestProduct> StaticStorage::GetLPs(System.Int32)
extern void StaticStorage_GetLPs_m421F3B0061D5580C7E04FAB51BC9122C0A6AE0AA ();
// 0x000005A8 Intelistyle.LatestProduct StaticStorage::GetLP(System.Int32,System.Int32)
extern void StaticStorage_GetLP_mC83A1BA9762979C0D8AD252A1F2BDBD37BEF10E4 ();
// 0x000005A9 System.String StaticStorage::GetPosString(System.Int32)
extern void StaticStorage_GetPosString_mD96C829C4C2258A6B43BEE3B10934C0A81A3CEB3 ();
// 0x000005AA System.String StaticStorage::GetLPosString(System.Int32)
extern void StaticStorage_GetLPosString_m8B2E834577A3D58AD92E326066625882552ACAC1 ();
// 0x000005AB Intelistyle.LatestProduct[] StaticStorage::ShuffleLPArr(Intelistyle.LatestProduct[])
extern void StaticStorage_ShuffleLPArr_m2DF0F49B95CD122DB93937E442A9A7866F7F2893 ();
// 0x000005AC Intelistyle.LatestProduct[] StaticStorage::SortingLPArr(Intelistyle.LatestProduct[],System.Int32)
extern void StaticStorage_SortingLPArr_mAC511FBA78A1C037263325EAC5FAB5AA73F19A1D ();
// 0x000005AD DetailProduct StaticStorage::GetDetailProduct(System.String,System.Int32)
extern void StaticStorage_GetDetailProduct_mB1603A9756BF0128D4303AFF08277F003657EF9F ();
// 0x000005AE StyleIt StaticStorage::GetStyleIt(System.String)
extern void StaticStorage_GetStyleIt_mF63887CB49B7CAB400D4FC31427460B49B87C66A ();
// 0x000005AF System.Void StaticStorage::SetFPS()
extern void StaticStorage_SetFPS_m58826B2F44BD0A51C59939B4121A6900EFFE8EE3 ();
// 0x000005B0 System.Void StaticStorage::ResetStaticStorage()
extern void StaticStorage_ResetStaticStorage_m3CBB7105F1952826ED5B9523BB22F2112965F686 ();
// 0x000005B1 System.Void StaticStorage::DownloadStyleIt(System.String)
extern void StaticStorage_DownloadStyleIt_m777CD838AA4597E9A2A86DDDFCE862FBDCF0BE75 ();
// 0x000005B2 System.Void StaticStorage::AddCodeTextures(System.String,UnityEngine.Texture2D)
extern void StaticStorage_AddCodeTextures_mD7A22C424EA0EA3E801A760289D3B84FF82518B7 ();
// 0x000005B3 CodeTexture StaticStorage::GetCodeTextures(System.String)
extern void StaticStorage_GetCodeTextures_m6FAE6602091BAC13CBCE792894EF95DA37DBF547 ();
// 0x000005B4 System.Void StaticStorage::SaveSmallTex(System.String,UnityEngine.Texture2D)
extern void StaticStorage_SaveSmallTex_m624691660B086A23B9527686CD0967AB186B8512 ();
// 0x000005B5 CodeTexture StaticStorage::GetSmallCodeTex(System.String)
extern void StaticStorage_GetSmallCodeTex_mD4A80B277D442E10E6556B5939D196E2898510CD ();
// 0x000005B6 System.Collections.Generic.List`1<CodeTexture> StaticStorage::CT()
extern void StaticStorage_CT_m82C539A74DD3114877C196933FB6A70E18F41CBC ();
// 0x000005B7 UnityEngine.Texture2D StaticStorage::CropTex(UnityEngine.Texture2D)
extern void StaticStorage_CropTex_m1CD18B6BA44258FF6255E049EBCEF8C52704E6EC ();
// 0x000005B8 System.Void StaticStorage::.ctor()
extern void StaticStorage__ctor_m43D48E8DC321836A5CD51D2C242B5D441ADF91AC ();
// 0x000005B9 System.Void StaticStorage::.cctor()
extern void StaticStorage__cctor_m22F85484DFD238692F57228F04E06166C0AFC831 ();
// 0x000005BA System.Void TopModal::.ctor()
extern void TopModal__ctor_m48BA2C9F31D56555A4E9E75EDC79502A777AEBF6 ();
// 0x000005BB System.Void Tracker::ClickOnProduct(System.String,System.Int32,DetailProduct,System.String[],Tracker_ProductFrom)
extern void Tracker_ClickOnProduct_mFCBCA2C031710B9D01BEB1A022C54CA77E8683FE ();
// 0x000005BC System.Void Tracker::CatchError(System.String)
extern void Tracker_CatchError_m84078CF53F5093251E012B5665BAB26BDCD7C271 ();
// 0x000005BD System.Void Tracker::SwipeToHome()
extern void Tracker_SwipeToHome_m51F01F5E1F5996A9E0902CF8AFD29DE1C637E612 ();
// 0x000005BE System.Void Tracker::SwipeToBack()
extern void Tracker_SwipeToBack_m50D66FD8627DE9116EADBA3A44FC2FBB710F9498 ();
// 0x000005BF System.Void Tracker::SwipeLargeImage(System.Boolean)
extern void Tracker_SwipeLargeImage_m145F8D5C62B5F1188B935AF5E3BF576BE5FDCBB9 ();
// 0x000005C0 System.Void Tracker::BotViewed(System.String)
extern void Tracker_BotViewed_mC944A210369318B2819360D15C85033242229376 ();
// 0x000005C1 System.Void Tracker::BotProductDesc()
extern void Tracker_BotProductDesc_m2D5F5CF04DA31422F49049911BD01233B4124001 ();
// 0x000005C2 System.Void Tracker::BotQR()
extern void Tracker_BotQR_m725231CBD4FC4BD49B20A63A00930845DE7A56B4 ();
// 0x000005C3 System.Void Tracker::BotStyleIt()
extern void Tracker_BotStyleIt_m0807B4B78B848251AF7F4B63953255A3B36CC1E2 ();
// 0x000005C4 System.Void Tracker::PopupIdle()
extern void Tracker_PopupIdle_m8A68C56D9B93983776CA4899A5B9F6884B6B590B ();
// 0x000005C5 System.Void Tracker::.ctor()
extern void Tracker__ctor_mCB1738FF4A8747740E90BB2D4604CF7EDB8C8158 ();
// 0x000005C6 System.Void CheckScene::Start()
extern void CheckScene_Start_m0742F789CD58A7BB043F7DF5A27EF8C840B0040D ();
// 0x000005C7 System.Void CheckScene::Update()
extern void CheckScene_Update_mDA1218E2B9270CB6BEEAF78A45868413A0AAE29C ();
// 0x000005C8 System.Void CheckScene::ChangeAPIStatus()
extern void CheckScene_ChangeAPIStatus_m78D3917772289763892610ADBBEB4818B40D47DE ();
// 0x000005C9 System.Void CheckScene::ChangeAPIServerStatus()
extern void CheckScene_ChangeAPIServerStatus_m37DA4AE0A2FEEA7A32D63F02E6A9369EC20564CF ();
// 0x000005CA System.Void CheckScene::LOADAPI()
extern void CheckScene_LOADAPI_m3D87202122347AE9557D0D8C9A90453CBAB60D98 ();
// 0x000005CB System.Void CheckScene::.ctor()
extern void CheckScene__ctor_m5DFFBD6998D29281C55AF718086E2D5855D060B8 ();
// 0x000005CC System.Void ColliderTools::Start()
extern void ColliderTools_Start_mA8613F37B9F5BE47B527E16612A168DF2A33A833 ();
// 0x000005CD System.Void ColliderTools::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void ColliderTools_OnCollisionEnter2D_m78F7FABF5B56F5729C6F6A2D1D2258D4DF913BAA ();
// 0x000005CE System.Void ColliderTools::OnCollisionStay2D(UnityEngine.Collision2D)
extern void ColliderTools_OnCollisionStay2D_m2B49E7A46A4A07A4FC1B7D61C0D91075B2C4B079 ();
// 0x000005CF System.Void ColliderTools::OnCollisionExit2D(UnityEngine.Collision2D)
extern void ColliderTools_OnCollisionExit2D_m27BFEDDDB24FAD02D09E86D1648B61F14222ED5D ();
// 0x000005D0 System.Void ColliderTools::.ctor()
extern void ColliderTools__ctor_m0E4BDD0969C98629843795FE8F6B1B8688BBCAFD ();
// 0x000005D1 System.Void CsvReadWrite::Awake()
extern void CsvReadWrite_Awake_m872B69E0430289A4EC4AF0ACF2357CE9B10B4691 ();
// 0x000005D2 System.Void CsvReadWrite::SaveTxt(System.String,System.String,System.String)
extern void CsvReadWrite_SaveTxt_m3236282BB50991E8B7CB7C814A5D31633F0BC911 ();
// 0x000005D3 System.Void CsvReadWrite::AddLoadingDataTxt(System.String,System.String,System.String)
extern void CsvReadWrite_AddLoadingDataTxt_mFA5160BDA26E7C2B1F15B4FF51274B345EDB527F ();
// 0x000005D4 System.Void CsvReadWrite::AddDataCSV(System.String,System.String,System.String)
extern void CsvReadWrite_AddDataCSV_m5C61BFC4763E402E2C3B6CFDA7DBBB12A726E2D4 ();
// 0x000005D5 System.Void CsvReadWrite::Init()
extern void CsvReadWrite_Init_mD5E76E4A28FF2673801740533A896451A18958AC ();
// 0x000005D6 System.Void CsvReadWrite::Save(System.String[])
extern void CsvReadWrite_Save_m12D7EFF412E125D816709AFF871DFBDAA8E9EC57 ();
// 0x000005D7 System.String CsvReadWrite::GetFilePath()
extern void CsvReadWrite_GetFilePath_m67D4DDFA70BDE787D214A09D3304AA7D2B5239DA ();
// 0x000005D8 System.String CsvReadWrite::getPath()
extern void CsvReadWrite_getPath_m9E71202C86B4321BB3929DDD4A52A53B6B52A7C5 ();
// 0x000005D9 System.Void CsvReadWrite::.ctor()
extern void CsvReadWrite__ctor_m6A795679813F744C663778CAEADD87D3525CE042 ();
// 0x000005DA System.Void DetectSFX::Start()
extern void DetectSFX_Start_m5E961CB91E6DC0E61C561D34EF0A58C0684034D3 ();
// 0x000005DB System.Void DetectSFX::Update()
extern void DetectSFX_Update_m1C11F1CD31EA4B11110DD791B31674F6F93845D9 ();
// 0x000005DC System.Void DetectSFX::.ctor()
extern void DetectSFX__ctor_m66841804139887B9D6C7F6E6D4F2E6EA0ED5E03B ();
// 0x000005DD System.Void DetectTouchMovement::Calculate()
extern void DetectTouchMovement_Calculate_m959665980E52112D4215C53D39EBA6D10AFCA984 ();
// 0x000005DE System.Single DetectTouchMovement::Angle(UnityEngine.Vector2,UnityEngine.Vector2)
extern void DetectTouchMovement_Angle_m92E0D73E6D983DD03541403A4242A4235501F0E8 ();
// 0x000005DF System.Void DetectTouchMovement::ApplyTo(UnityEngine.GameObject)
extern void DetectTouchMovement_ApplyTo_mE3D11AAFB852BF54221CE7DEADFEC1CE6984A165 ();
// 0x000005E0 System.Void DetectTouchMovement::.ctor()
extern void DetectTouchMovement__ctor_m71C55ED87D74AF305CD7ACBB4D41D7DC6C089E17 ();
// 0x000005E1 System.Void DragTools::Start()
extern void DragTools_Start_m706E26A5D0B308D6BBB75A969DA4D34F3CE685CD ();
// 0x000005E2 System.Void DragTools::OnMouseDown()
extern void DragTools_OnMouseDown_mC054A130FB4CF57B39DD44ED5BE7D14AC6D03E03 ();
// 0x000005E3 System.Void DragTools::OnMouseDrag()
extern void DragTools_OnMouseDrag_mA873A4CC83B41703E36F7F26C50BEB0D9DB12884 ();
// 0x000005E4 System.Void DragTools::OnMouseUp()
extern void DragTools_OnMouseUp_mA1761DF1890D0DF7E535A62A8A563CDC5BA18BAA ();
// 0x000005E5 System.Void DragTools::.ctor()
extern void DragTools__ctor_m0AC0D55FEC838B1EA0CD6D84FF7D04D676D62DEF ();
// 0x000005E6 UnityEngine.GameObject DragUI::get_Current()
extern void DragUI_get_Current_m6D53D9D69B102F96EA5FBA9CCDB8581E9EE547DE ();
// 0x000005E7 System.Single DragUI::get_TopBound()
extern void DragUI_get_TopBound_m282534A181D96F60C9735D060A14FDF245769CAA ();
// 0x000005E8 System.Single DragUI::get_BottomBound()
extern void DragUI_get_BottomBound_m8EA2CEB1915668195AB6B6549933FED83679DD8C ();
// 0x000005E9 System.Single DragUI::get_RightBound()
extern void DragUI_get_RightBound_mB23CFA8A024086A9638B3B925425D5E767C96ED0 ();
// 0x000005EA System.Single DragUI::get_LeftBound()
extern void DragUI_get_LeftBound_m98B2586E6B165AF56DA9B2D22F16B0C5CC66F633 ();
// 0x000005EB UnityEngine.Canvas DragUI::get_ParentCanvas()
extern void DragUI_get_ParentCanvas_mFCBD06BEC946F664074147135DC01A575247EDAF ();
// 0x000005EC System.Boolean DragUI::get_IsOverlayMode()
extern void DragUI_get_IsOverlayMode_mD77BBFF67093EDF3A8D3EFCFB6744EDDAA56E98C ();
// 0x000005ED System.Void DragUI::Reset()
extern void DragUI_Reset_mAB16AA6CFAB5228F039E1F03FD33117CB4AE7E83 ();
// 0x000005EE System.Void DragUI::AttachBoxCollider()
extern void DragUI_AttachBoxCollider_m911CF4714DAC0683D2E72CC478A09875ECD1BBF3 ();
// 0x000005EF System.Void DragUI::Start()
extern void DragUI_Start_m9874AAE51FFDFF3299C79A39D0F6A9FB3FD4FC29 ();
// 0x000005F0 System.Void DragUI::Update()
extern void DragUI_Update_m199289A2C2730A00D796FACDEC68AA7A1753050D ();
// 0x000005F1 System.Void DragUI::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void DragUI_OnPointerDown_m977E5D65EF8A705570C3ADDF2CE7A78016CC1CFE ();
// 0x000005F2 System.Void DragUI::UpdateDefaultPosition()
extern void DragUI_UpdateDefaultPosition_m7437364959B8C21B516D21B0E9A573C009DB442F ();
// 0x000005F3 System.Void DragUI::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragUI_OnDrag_m0BD970B833BD7603F1CF61069C623970E84BC33D ();
// 0x000005F4 System.Void DragUI::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void DragUI_OnPointerUp_mA93DDABCA43BC93CF130786B01D0370262DCE7EC ();
// 0x000005F5 System.Void DragUI::BackToDefaultPosition()
extern void DragUI_BackToDefaultPosition_m2DBDFAE6592BE1A8926D1D3615BB8B82E15BF3BF ();
// 0x000005F6 System.Collections.IEnumerator DragUI::BackAnimated()
extern void DragUI_BackAnimated_mD28BF8926495CF4384093886AC1F2C6E98667FB9 ();
// 0x000005F7 System.Void DragUI::BackToDefaultSiblingIndex()
extern void DragUI_BackToDefaultSiblingIndex_mCE1F3EDE9446588795E6580AE6F5D2DBEB97A3E1 ();
// 0x000005F8 System.Void DragUI::OnDisable()
extern void DragUI_OnDisable_m63A353686AA2FE8135FDF09F63C6D485C6CF951D ();
// 0x000005F9 System.Void DragUI::OnDestroy()
extern void DragUI_OnDestroy_m3A610FAD2886BA057354DDEC9AE6BB1296AC6874 ();
// 0x000005FA System.Void DragUI::LockMovements()
extern void DragUI_LockMovements_m66C63D7F9A1515687327B2E1F5BD376B82A743C0 ();
// 0x000005FB System.Void DragUI::.ctor()
extern void DragUI__ctor_mE72E9308ED94411A1F14BE528593765FDCE37E6F ();
// 0x000005FC System.Void FPSDisplay::Start()
extern void FPSDisplay_Start_m9DE167885FC272684751D270809F5CD7CBE56661 ();
// 0x000005FD System.Void FPSDisplay::Update()
extern void FPSDisplay_Update_mE45C5E9F8AA0815DBDBC36FFE099E4E174985930 ();
// 0x000005FE System.Void FPSDisplay::OnGUI()
extern void FPSDisplay_OnGUI_mBD59BE8E75936F4CEEA5D97FC32AD8BE1F1FEFEF ();
// 0x000005FF System.Void FPSDisplay::.ctor()
extern void FPSDisplay__ctor_mE5C50AF5EDE70F699D1BDC3B56BF31972E2215CE ();
// 0x00000600 System.Void ImageResources::Awake()
extern void ImageResources_Awake_m962A6D29E957F012BABB5FB4ECF540A237142040 ();
// 0x00000601 System.Int32 ImageResources::TshirtCounter()
extern void ImageResources_TshirtCounter_mEAA7737D70491917EF97E8856DF65BE6B9024B7B ();
// 0x00000602 System.Void ImageResources::.ctor()
extern void ImageResources__ctor_mE3723A8BE67EEB3E7DE71BED67DD8BE2A5CED1C9 ();
// 0x00000603 System.Void LoadingScene2::Awake()
extern void LoadingScene2_Awake_m1C9592348C66012A53681CC1230B3A976129ABF2 ();
// 0x00000604 System.Void LoadingScene2::Start()
extern void LoadingScene2_Start_m50934A31EBFC6FDA1A51919930F9C660A9BC90B1 ();
// 0x00000605 System.Void LoadingScene2::Update()
extern void LoadingScene2_Update_m8CE78A89BABF32282279D71383F5E9C7ECED796B ();
// 0x00000606 System.Void LoadingScene2::GOBtn()
extern void LoadingScene2_GOBtn_mBCE2090928CD92A39EA9843483D2D36979EFC1CD ();
// 0x00000607 System.Void LoadingScene2::ChangeColumn()
extern void LoadingScene2_ChangeColumn_mE40D9E211D0916F76D5EA41D6F078230A4255B3B ();
// 0x00000608 System.Collections.IEnumerator LoadingScene2::BranchDownloadAPI(System.String,System.Int32)
extern void LoadingScene2_BranchDownloadAPI_m9E3D346366D4147415EF066243B96404A5550C00 ();
// 0x00000609 System.Void LoadingScene2::MultiDownloadAPI(Proyecto26.RequestHelper,System.Int32,System.Int32)
extern void LoadingScene2_MultiDownloadAPI_m0C3BB5CE22711E71B81D09EA2B8FC36BB08F7FB5 ();
// 0x0000060A System.Void LoadingScene2::CheckAPIBool()
extern void LoadingScene2_CheckAPIBool_m6E59F3055224CE5ECC144078FF70F05DDD618463 ();
// 0x0000060B System.Void LoadingScene2::MultiDownload()
extern void LoadingScene2_MultiDownload_mDB8C7E6992A667762A5F6AEC63E327BB369D0173 ();
// 0x0000060C System.Collections.IEnumerator LoadingScene2::MultiDownloadNCache(Intelistyle.LatestProduct,System.Int32,System.Int32)
extern void LoadingScene2_MultiDownloadNCache_m250FFDE279EF8AE50CC7FAC73778A2D11476CC2C ();
// 0x0000060D System.Void LoadingScene2::MultiDownloadDetail(Intelistyle.LatestProduct,System.Int32,System.Int32)
extern void LoadingScene2_MultiDownloadDetail_m539528AE838A65B6A667D8DABE549ACBEF0355A4 ();
// 0x0000060E System.Collections.IEnumerator LoadingScene2::MultiDLDetailNCache(DetailProduct,Intelistyle.LatestProduct,System.Int32,System.Int32,System.Int32)
extern void LoadingScene2_MultiDLDetailNCache_m9F15C8D7C5FC0B64585DA8677881D0420DFA685B ();
// 0x0000060F System.Void LoadingScene2::CheckStep1(System.Int32,System.Int32)
extern void LoadingScene2_CheckStep1_mC3A64D25C0068407CFA84D2CB17AF0101697ADFE ();
// 0x00000610 System.Void LoadingScene2::CheckStep2(System.Int32,System.Int32,System.Boolean)
extern void LoadingScene2_CheckStep2_mF70FAD1EC76D06879D87C1474FBE347104384427 ();
// 0x00000611 System.Void LoadingScene2::CheckStep2_Failed(System.Int32,System.Int32)
extern void LoadingScene2_CheckStep2_Failed_mBF1A493E588B70FF98E3962AE24D3778E46AECD2 ();
// 0x00000612 System.Void LoadingScene2::FinalCheck()
extern void LoadingScene2_FinalCheck_m36828570AE9085F486B351F3E693D73141C9897A ();
// 0x00000613 System.Void LoadingScene2::.ctor()
extern void LoadingScene2__ctor_m24844DB1692771491E27ABD3E194EC82BB337306 ();
// 0x00000614 System.Void MobileInputHandler::Start()
extern void MobileInputHandler_Start_m6AB3FE268655F136C6BF653785E17A3988F71AC2 ();
// 0x00000615 System.Void MobileInputHandler::Update()
extern void MobileInputHandler_Update_m6127D687BD849FC6C0C96C953C7A7615086AEF5F ();
// 0x00000616 System.Void MobileInputHandler::OnPressBackKey()
extern void MobileInputHandler_OnPressBackKey_mD068FD19E121CE693A062E2725653AD2365108B3 ();
// 0x00000617 System.Void MobileInputHandler::.ctor()
extern void MobileInputHandler__ctor_m5874BB0CA421ADC81FC1F40E43B2994D4FB1FF9B ();
// 0x00000618 System.Boolean PlayerPrefsX::SetBool(System.String,System.Boolean)
extern void PlayerPrefsX_SetBool_m65A3486B9543DC5EB90ABC94AECA79917B99782C ();
// 0x00000619 System.Boolean PlayerPrefsX::GetBool(System.String)
extern void PlayerPrefsX_GetBool_m79C50F07E41D786E7AEC4A2F9F12699B8653C0AA ();
// 0x0000061A System.Boolean PlayerPrefsX::GetBool(System.String,System.Boolean)
extern void PlayerPrefsX_GetBool_mD8D779122D1FE8E5D2194E0F5907E16BA53C28C9 ();
// 0x0000061B System.Int64 PlayerPrefsX::GetLong(System.String,System.Int64)
extern void PlayerPrefsX_GetLong_mF623D746F8E99ECC43AF74853D434751AAC52251 ();
// 0x0000061C System.Int64 PlayerPrefsX::GetLong(System.String)
extern void PlayerPrefsX_GetLong_m12B4F39533853341DCD561621437264EE7A0E224 ();
// 0x0000061D System.Void PlayerPrefsX::SplitLong(System.Int64,System.Int32&,System.Int32&)
extern void PlayerPrefsX_SplitLong_mEE5B8C5473668332C1F35150FA58A296E4600836 ();
// 0x0000061E System.Void PlayerPrefsX::SetLong(System.String,System.Int64)
extern void PlayerPrefsX_SetLong_m3F01DD141F8854F17D37DDD03338A81918ED5789 ();
// 0x0000061F System.Boolean PlayerPrefsX::SetVector2(System.String,UnityEngine.Vector2)
extern void PlayerPrefsX_SetVector2_m55F95C8CB796DF7C8CB9DC56AA7593EBCB0810EA ();
// 0x00000620 UnityEngine.Vector2 PlayerPrefsX::GetVector2(System.String)
extern void PlayerPrefsX_GetVector2_mA1134460C558BF1DB8AA21847F4CE259D756A3E9 ();
// 0x00000621 UnityEngine.Vector2 PlayerPrefsX::GetVector2(System.String,UnityEngine.Vector2)
extern void PlayerPrefsX_GetVector2_m54BCC7208903ED101AB18E1F365661738E8D7C33 ();
// 0x00000622 System.Boolean PlayerPrefsX::SetVector3(System.String,UnityEngine.Vector3)
extern void PlayerPrefsX_SetVector3_m19D8C5F14F50E1043CD2A65BDF248E7E92246520 ();
// 0x00000623 UnityEngine.Vector3 PlayerPrefsX::GetVector3(System.String)
extern void PlayerPrefsX_GetVector3_m3864F748022CB6918D18A830FCA406D6F5A6407D ();
// 0x00000624 UnityEngine.Vector3 PlayerPrefsX::GetVector3(System.String,UnityEngine.Vector3)
extern void PlayerPrefsX_GetVector3_m067939EBC9967B8FCD4583394D7AFC5C659BBAF2 ();
// 0x00000625 System.Boolean PlayerPrefsX::SetQuaternion(System.String,UnityEngine.Quaternion)
extern void PlayerPrefsX_SetQuaternion_mD20099D11CC4FEE41D23D5B33CD7CDCA681FCCA4 ();
// 0x00000626 UnityEngine.Quaternion PlayerPrefsX::GetQuaternion(System.String)
extern void PlayerPrefsX_GetQuaternion_m0DEBA1DC49DD8B10F8319BE51D536A79D037946E ();
// 0x00000627 UnityEngine.Quaternion PlayerPrefsX::GetQuaternion(System.String,UnityEngine.Quaternion)
extern void PlayerPrefsX_GetQuaternion_m2B57F04CBFBC2A93C668AB99D69DF2CA94709ADD ();
// 0x00000628 System.Boolean PlayerPrefsX::SetColor(System.String,UnityEngine.Color)
extern void PlayerPrefsX_SetColor_m1DF6AEB4C0471EDB7DE54950DED558BECE54FD85 ();
// 0x00000629 UnityEngine.Color PlayerPrefsX::GetColor(System.String)
extern void PlayerPrefsX_GetColor_mFEDCBD770BA05E6EDB8417BC80DDA0994B2215DD ();
// 0x0000062A UnityEngine.Color PlayerPrefsX::GetColor(System.String,UnityEngine.Color)
extern void PlayerPrefsX_GetColor_mD6C55716F9715ECBA5F58EBCF1BFAC9AE8C3317C ();
// 0x0000062B System.Boolean PlayerPrefsX::SetBoolArray(System.String,System.Boolean[])
extern void PlayerPrefsX_SetBoolArray_m0053952131A28788A57EA17A35AD317776B94BEB ();
// 0x0000062C System.Boolean[] PlayerPrefsX::GetBoolArray(System.String)
extern void PlayerPrefsX_GetBoolArray_mCA6912469F93CE770BCA202140668908831E687F ();
// 0x0000062D System.Boolean[] PlayerPrefsX::GetBoolArray(System.String,System.Boolean,System.Int32)
extern void PlayerPrefsX_GetBoolArray_mB27C61D2742FCCC22125D5C9A26AB329235040F4 ();
// 0x0000062E System.Boolean PlayerPrefsX::SetStringArray(System.String,System.String[])
extern void PlayerPrefsX_SetStringArray_mE7F9544383E91AEBED66859ED957371EA9897171 ();
// 0x0000062F System.String[] PlayerPrefsX::GetStringArray(System.String)
extern void PlayerPrefsX_GetStringArray_m56A0F4FBAA6AA47D35CFDFD09D46898D42FFB89C ();
// 0x00000630 System.String[] PlayerPrefsX::GetStringArray(System.String,System.String,System.Int32)
extern void PlayerPrefsX_GetStringArray_m51D364FBA310FD2EA3BAF2456878CBEA0569D5D2 ();
// 0x00000631 System.Boolean PlayerPrefsX::SetIntArray(System.String,System.Int32[])
extern void PlayerPrefsX_SetIntArray_mA76A96BDC8F678C853B9470B5D013881794ABEFD ();
// 0x00000632 System.Boolean PlayerPrefsX::SetFloatArray(System.String,System.Single[])
extern void PlayerPrefsX_SetFloatArray_m206F2B51F9C4D4BB8461A2AAAC45ED2C5284920B ();
// 0x00000633 System.Boolean PlayerPrefsX::SetVector2Array(System.String,UnityEngine.Vector2[])
extern void PlayerPrefsX_SetVector2Array_m3DBFB4A55E125962B52A50598B0A1B15F59AE5D8 ();
// 0x00000634 System.Boolean PlayerPrefsX::SetVector3Array(System.String,UnityEngine.Vector3[])
extern void PlayerPrefsX_SetVector3Array_m5CDF8EBBE07D2769764E96457BAE346726F55CE1 ();
// 0x00000635 System.Boolean PlayerPrefsX::SetQuaternionArray(System.String,UnityEngine.Quaternion[])
extern void PlayerPrefsX_SetQuaternionArray_mCAD0DB1DC55D9FDA0C94F71BE01D48BB65DEAE8E ();
// 0x00000636 System.Boolean PlayerPrefsX::SetColorArray(System.String,UnityEngine.Color[])
extern void PlayerPrefsX_SetColorArray_mAF3268DC430B09CAE7C9EB572485252FFB66D92A ();
// 0x00000637 System.Boolean PlayerPrefsX::SetValue(System.String,T,PlayerPrefsX_ArrayType,System.Int32,System.Action`3<T,System.Byte[],System.Int32>)
// 0x00000638 System.Void PlayerPrefsX::ConvertFromInt(System.Int32[],System.Byte[],System.Int32)
extern void PlayerPrefsX_ConvertFromInt_mA214B13EDB658E59C6D76383F0FB334506A4792E ();
// 0x00000639 System.Void PlayerPrefsX::ConvertFromFloat(System.Single[],System.Byte[],System.Int32)
extern void PlayerPrefsX_ConvertFromFloat_mCD49916CDB39ED7A1B035F03046B04BDE0610952 ();
// 0x0000063A System.Void PlayerPrefsX::ConvertFromVector2(UnityEngine.Vector2[],System.Byte[],System.Int32)
extern void PlayerPrefsX_ConvertFromVector2_m2F6011CBC58DEA02436B612E426953C3F236B6AF ();
// 0x0000063B System.Void PlayerPrefsX::ConvertFromVector3(UnityEngine.Vector3[],System.Byte[],System.Int32)
extern void PlayerPrefsX_ConvertFromVector3_m9509552575513A02DB9DA0364B8B294F8E195364 ();
// 0x0000063C System.Void PlayerPrefsX::ConvertFromQuaternion(UnityEngine.Quaternion[],System.Byte[],System.Int32)
extern void PlayerPrefsX_ConvertFromQuaternion_m2AB8ECFEC0C300AD267EF8FB381D199E780F5F2E ();
// 0x0000063D System.Void PlayerPrefsX::ConvertFromColor(UnityEngine.Color[],System.Byte[],System.Int32)
extern void PlayerPrefsX_ConvertFromColor_m5DAA4AA3700BD4E4F653784C17E80B597A65E27B ();
// 0x0000063E System.Int32[] PlayerPrefsX::GetIntArray(System.String)
extern void PlayerPrefsX_GetIntArray_m349AB786A0F916179B9E3E44AC0E022988FE21AB ();
// 0x0000063F System.Int32[] PlayerPrefsX::GetIntArray(System.String,System.Int32,System.Int32)
extern void PlayerPrefsX_GetIntArray_m2BE03C4CCE0B4F5F4405CF30069590834FFFB18F ();
// 0x00000640 System.Single[] PlayerPrefsX::GetFloatArray(System.String)
extern void PlayerPrefsX_GetFloatArray_m65613451D241A80297A79FA77BC505EF9CC187F8 ();
// 0x00000641 System.Single[] PlayerPrefsX::GetFloatArray(System.String,System.Single,System.Int32)
extern void PlayerPrefsX_GetFloatArray_mCA6266CF0B77F7A536D6A5BB5AFAA80859256D0F ();
// 0x00000642 UnityEngine.Vector2[] PlayerPrefsX::GetVector2Array(System.String)
extern void PlayerPrefsX_GetVector2Array_mF690D2104BF67B5058A19494D9A78D37F20032C0 ();
// 0x00000643 UnityEngine.Vector2[] PlayerPrefsX::GetVector2Array(System.String,UnityEngine.Vector2,System.Int32)
extern void PlayerPrefsX_GetVector2Array_m3C2181DC09CF31EC4D8E639381E796E52116FEC0 ();
// 0x00000644 UnityEngine.Vector3[] PlayerPrefsX::GetVector3Array(System.String)
extern void PlayerPrefsX_GetVector3Array_mE1BC5E69B68E3D6F9C04AC1CFBFBF171132FEBE5 ();
// 0x00000645 UnityEngine.Vector3[] PlayerPrefsX::GetVector3Array(System.String,UnityEngine.Vector3,System.Int32)
extern void PlayerPrefsX_GetVector3Array_m01A7E338CDC2F7B57D827598B7374DCC2C87586B ();
// 0x00000646 UnityEngine.Quaternion[] PlayerPrefsX::GetQuaternionArray(System.String)
extern void PlayerPrefsX_GetQuaternionArray_mC1935A7AE43DC4335515B649F14F57C1E4AB583B ();
// 0x00000647 UnityEngine.Quaternion[] PlayerPrefsX::GetQuaternionArray(System.String,UnityEngine.Quaternion,System.Int32)
extern void PlayerPrefsX_GetQuaternionArray_m8B17BDD7ABD71430182AA9EEB98B8BEEEF505460 ();
// 0x00000648 UnityEngine.Color[] PlayerPrefsX::GetColorArray(System.String)
extern void PlayerPrefsX_GetColorArray_m5A3F14CD6E3FDA3E6C8BA8708DE563B187A1F695 ();
// 0x00000649 UnityEngine.Color[] PlayerPrefsX::GetColorArray(System.String,UnityEngine.Color,System.Int32)
extern void PlayerPrefsX_GetColorArray_m488B0F0D9175D92C065E0EAFFA843CE107FE36A0 ();
// 0x0000064A System.Void PlayerPrefsX::GetValue(System.String,T,PlayerPrefsX_ArrayType,System.Int32,System.Action`2<T,System.Byte[]>)
// 0x0000064B System.Void PlayerPrefsX::ConvertToInt(System.Collections.Generic.List`1<System.Int32>,System.Byte[])
extern void PlayerPrefsX_ConvertToInt_mD3E07E6E9D9101165A89EA99245448A3473C2EAA ();
// 0x0000064C System.Void PlayerPrefsX::ConvertToFloat(System.Collections.Generic.List`1<System.Single>,System.Byte[])
extern void PlayerPrefsX_ConvertToFloat_mEA9482480CBF847B60571B5ED6191B79D51D1073 ();
// 0x0000064D System.Void PlayerPrefsX::ConvertToVector2(System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Byte[])
extern void PlayerPrefsX_ConvertToVector2_m27012AE8FD4BD9FC1E4C908C931D212893B8EC58 ();
// 0x0000064E System.Void PlayerPrefsX::ConvertToVector3(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Byte[])
extern void PlayerPrefsX_ConvertToVector3_m1514F7034540CF554D1A9C45E3FDFDDF660B5F51 ();
// 0x0000064F System.Void PlayerPrefsX::ConvertToQuaternion(System.Collections.Generic.List`1<UnityEngine.Quaternion>,System.Byte[])
extern void PlayerPrefsX_ConvertToQuaternion_mF46BD45044218548093B772806EAFFCD8D03CA99 ();
// 0x00000650 System.Void PlayerPrefsX::ConvertToColor(System.Collections.Generic.List`1<UnityEngine.Color>,System.Byte[])
extern void PlayerPrefsX_ConvertToColor_m4B068B45621719E49D069515D317C7B27735F4EE ();
// 0x00000651 System.Void PlayerPrefsX::ShowArrayType(System.String)
extern void PlayerPrefsX_ShowArrayType_mD21DAF3E943D67D359CDC5E66548A8F1439A68C4 ();
// 0x00000652 System.Void PlayerPrefsX::Initialize()
extern void PlayerPrefsX_Initialize_mA42B73D4CEB5E787232081316795EAE682FDF968 ();
// 0x00000653 System.Boolean PlayerPrefsX::SaveBytes(System.String,System.Byte[])
extern void PlayerPrefsX_SaveBytes_m3E5803A2E2510AC5CA8AE227BF311BB5FE420F60 ();
// 0x00000654 System.Void PlayerPrefsX::ConvertFloatToBytes(System.Single,System.Byte[])
extern void PlayerPrefsX_ConvertFloatToBytes_m40CA01A5ABE435B40949FA52C15216157F08583B ();
// 0x00000655 System.Single PlayerPrefsX::ConvertBytesToFloat(System.Byte[])
extern void PlayerPrefsX_ConvertBytesToFloat_m5E30741F2EFAD90022D4CF082D8A65FFE4A70E6F ();
// 0x00000656 System.Void PlayerPrefsX::ConvertInt32ToBytes(System.Int32,System.Byte[])
extern void PlayerPrefsX_ConvertInt32ToBytes_m7E0964FFC5EB3EDB4D8C861CD2273BFEB436413C ();
// 0x00000657 System.Int32 PlayerPrefsX::ConvertBytesToInt32(System.Byte[])
extern void PlayerPrefsX_ConvertBytesToInt32_m12C722E4494600F4792ACFF8996EF582CF235680 ();
// 0x00000658 System.Void PlayerPrefsX::ConvertTo4Bytes(System.Byte[])
extern void PlayerPrefsX_ConvertTo4Bytes_m0E98C08BFDA3B7CBBBBD6ABDB5E7BCB54DBF4E5A ();
// 0x00000659 System.Void PlayerPrefsX::ConvertFrom4Bytes(System.Byte[])
extern void PlayerPrefsX_ConvertFrom4Bytes_mB7B5773E1AE2ACB18A1CB29A3B3FBBAC403E23AD ();
// 0x0000065A System.Void PlayerPrefsX::.ctor()
extern void PlayerPrefsX__ctor_m986F8E45BDB9448BFA635CBC3F9898AE8646CD9E ();
// 0x0000065B System.Void StarFX::Start()
extern void StarFX_Start_m3BD70D045D73EDA70F2AAA59076BC1462A2D2130 ();
// 0x0000065C System.Void StarFX::Update()
extern void StarFX_Update_m5D991ACD005AB9882B7A31330001369DF5C63144 ();
// 0x0000065D System.Void StarFX::Star()
extern void StarFX_Star_m23E114883BD322EAEA93E0F1338326D3CEDA72BE ();
// 0x0000065E System.Void StarFX::.ctor()
extern void StarFX__ctor_m63D49EBEFFE4FB745D0A77ADAE27D769DA035ED6 ();
// 0x0000065F System.Void SwipeDetector::Update()
extern void SwipeDetector_Update_mA095F65D05FE0B2FC84E88D00DCE273D74A13BEC ();
// 0x00000660 System.Void SwipeDetector::checkSwipe()
extern void SwipeDetector_checkSwipe_m2A607CE45F221869AED7243D928F344BBC05CCC4 ();
// 0x00000661 System.Single SwipeDetector::verticalMove()
extern void SwipeDetector_verticalMove_mE585D1F8858FE0459C8472EF578A61EF1C6844C0 ();
// 0x00000662 System.Single SwipeDetector::horizontalValMove()
extern void SwipeDetector_horizontalValMove_mD02D67A2CE78F9F9B3EE2979617ED7C1177919F2 ();
// 0x00000663 System.Void SwipeDetector::OnSwipeUp()
extern void SwipeDetector_OnSwipeUp_m091A03F545820C1B69AC974BC293F91C38E24C68 ();
// 0x00000664 System.Void SwipeDetector::OnSwipeDown()
extern void SwipeDetector_OnSwipeDown_m376B53DECF679B05FD81ED72B8D9F6F4D3996EDF ();
// 0x00000665 System.Void SwipeDetector::OnSwipeLeft()
extern void SwipeDetector_OnSwipeLeft_m0FF32BE2CEB5FC86F17F1CD83728E3AC33E0AE93 ();
// 0x00000666 System.Void SwipeDetector::OnSwipeRight()
extern void SwipeDetector_OnSwipeRight_mB47E4809A73F15744B763903180B5189F2305652 ();
// 0x00000667 System.Void SwipeDetector::.ctor()
extern void SwipeDetector__ctor_m2411490382B3EAD042053E6843814D951CE9D535 ();
// 0x00000668 System.Void TouchDownInput::Start()
extern void TouchDownInput_Start_m88037C0A05D9F7358576F23F793DC83A40C14596 ();
// 0x00000669 System.Void TouchDownInput::OnMouseDown()
extern void TouchDownInput_OnMouseDown_mBB2927D4E7BC8042A1BB73BD30B1B971BEE9A5BE ();
// 0x0000066A System.Void TouchDownInput::.ctor()
extern void TouchDownInput__ctor_m142C28FCBE9D80F4A6EF7CB85953C5FFD994C9B6 ();
// 0x0000066B System.Void TouchUpInput::Start()
extern void TouchUpInput_Start_mCC82E84E3690092935B6CF7328CEC93958AE1EFF ();
// 0x0000066C System.Void TouchUpInput::OnMouseUp()
extern void TouchUpInput_OnMouseUp_mE76D5806545096D42D48022FBB7BC940D4485706 ();
// 0x0000066D System.Void TouchUpInput::.ctor()
extern void TouchUpInput__ctor_m014132CC5987A9D586A420B93C566787E6EE0DAF ();
// 0x0000066E System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_mD13A02B63932BDA275E1A788FD72D86D69B9E440 ();
// 0x0000066F System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_mD31D1ED1B2C3C82986BFBD18FA584D45167431F3 ();
// 0x00000670 System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m01CC3E959ACECC222DFD8541231EC1E00C024194 ();
// 0x00000671 System.Void ChatController::.ctor()
extern void ChatController__ctor_mF4343BA56301C6825EB0A71EBF9600525B437BCD ();
// 0x00000672 System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_mAB3C67FA11192EFB31545F42D3D8AAB2A662FEB2 ();
// 0x00000673 System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_m21098EFD0904DFD43A3CB2FF536E83F1593C2412 ();
// 0x00000674 System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_mBA9215F94AB29FBA4D3335AEA6FAB50567509E74 ();
// 0x00000675 System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_mD139A23C440A026E47FA8E3AD01AD6FEF7713C3D ();
// 0x00000676 System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_mF6477F5EB75EC15CD6B81ACD85271F854BABC5D6 ();
// 0x00000677 System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mCA5EA200223A9F224F2F4DBD306DAE038C71A35F ();
// 0x00000678 System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_mBB38130850945A40631821275F07C19720E0C55E ();
// 0x00000679 TMPro.TMP_TextEventHandler_CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_mF70DBE3FF43B3D6E64053D37A2FADF802533E1FF ();
// 0x0000067A System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler_CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m237C99FE66E4E16518DAE68FF9CBF1A52E816AD2 ();
// 0x0000067B TMPro.TMP_TextEventHandler_SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m395603314F8CD073897DCAB5513270C6ADD94BF4 ();
// 0x0000067C System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler_SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_mAAE4B440E34EE5736D43D6A8A7D3A7CEE0503D69 ();
// 0x0000067D TMPro.TMP_TextEventHandler_WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_m415F4479934B1739658356B47DF4C2E90496AE2E ();
// 0x0000067E System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler_WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_m6062C0AF2FDD8752DC4A75663EE8E5C128504698 ();
// 0x0000067F TMPro.TMP_TextEventHandler_LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_m8E724700CC5DF1197B103F87156576A52F62AB2B ();
// 0x00000680 System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler_LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m1A8E37D2069EF684EF930D4F1ABE764AE17D9A62 ();
// 0x00000681 TMPro.TMP_TextEventHandler_LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_m221527467F0606DD3561E0FB0D7678AA8329AD5D ();
// 0x00000682 System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler_LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m1376CC9B70177B0C25ACEDF91D5B94BC4B8CF71D ();
// 0x00000683 System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_m9A353CC9705A9E824A60C3D2D026A7FD96B41D74 ();
// 0x00000684 System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_m2F3241223A91F9C50E11B27F67BA2B6D19328B72 ();
// 0x00000685 System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_m1827A9D3F08839023DE71352202FE5F744E150EF ();
// 0x00000686 System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_m788B93D2C3B54BCF09475675B274BCB047D449FB ();
// 0x00000687 System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_mBC44C107A6FB8C43F7C6629D4A15CA85471A28B2 ();
// 0x00000688 System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_mEF24BCE06B0CE4450B6AE9561EC4B5052DAF00F6 ();
// 0x00000689 System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_m7C4D266070EE2ADC66BCCFD50EB74FEB4923B77E ();
// 0x0000068A System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mAAF4AF44929D0C9FD73C89E5266028908074AEB1 ();
// 0x0000068B System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m082D12F7D044456D8514E4D31944C6900F8262C0 ();
// 0x0000068C System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_mEA56AE9489B50CF5E5FC682AA18D1CE9AF8E1F8B ();
// 0x0000068D System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_mC0055F208B85783F0B3DB942137439C897552571 ();
// 0x0000068E System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_m2FA501D2C572C46A61635E0A3E2FF45EC3A3749C ();
// 0x0000068F System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m976ED5172DEAFC628DE7C4C51DF25B1373C7846A ();
// 0x00000690 System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_mD92CA5A254960EB149966C2A3243514596C96EAD ();
// 0x00000691 System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_m2D028BFC6EFB4C84C1A7A98B87A509B27E75BA06 ();
// 0x00000692 System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_m7CA524C53D9E88510EE7987E680F49E8353E4B64 ();
// 0x00000693 System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_m8D1A987C39FD4756642011D01F35BDC3B1F99403 ();
// 0x00000694 System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_m73F65BA012D86A6BE17E82012AE8E2339CA5D550 ();
// 0x00000695 System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m9A5E67EA64AAC56D56C7D269CC9685E78276360A ();
// 0x00000696 System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_m22D98FCFC356D5CD7F401DE7EDCDAF7AE0219402 ();
// 0x00000697 System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_mDAC3E3BE80C9236562EFB6E74DEBE67D8713101D ();
// 0x00000698 System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m581B79998DB6946746CBF7380AFCC7F2B75D99F7 ();
// 0x00000699 System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_mA9D72DB0BB6E4F72192DA91BC9F8918A9C61B676 ();
// 0x0000069A System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_mDC862C8119AB0B4807245CC3482F027842EAB425 ();
// 0x0000069B System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m198C209AC84EF7A2437ADB2B67F6B78D12AB9216 ();
// 0x0000069C System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_m2108A608ABD8EA7FD2B47EE40C07F4117BB7607E ();
// 0x0000069D System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mDA26D26457D277CC2D9042F3BD623D48849440C4 ();
// 0x0000069E System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_mC50AAC1AF75B07CD6753EA3224C369E43001791B ();
// 0x0000069F System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_m2832B9D713355ECF861642D115F86AA64A6F119E ();
// 0x000006A0 System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_m8E01638EBFE80CC0B9E4A97AB809B91E3C6956BE ();
// 0x000006A1 System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_m24F4FADC328B0C76264DE24663CFA914EA94D1FD ();
// 0x000006A2 System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_mC45F318132D23804CBF73EA2445EF7589C2333E9 ();
// 0x000006A3 System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_mC3894CE97A12F50FB225CA8F6F05A7B3CA4B0623 ();
// 0x000006A4 System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_m22A3AE8E48128DF849EE2957F4EF881A433CA8CB ();
// 0x000006A5 System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_m742F828A2245E8CC29BC045A999C5E527931DFF1 ();
// 0x000006A6 System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mA2284F621031B4D494AC06B687AF43D2D1D89BD7 ();
// 0x000006A7 System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m51C217E0CB26C2E627BA01599147F69B893EF189 ();
// 0x000006A8 System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m6A9CEFA12DB252E297E41E256698DD4E90809F6A ();
// 0x000006A9 UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_m3CE7B666BEF4CFFE9EB110C8D57D9A5F6385720B ();
// 0x000006AA System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m4A69C47EA665D49482B930F924E49C8E70FAC225 ();
// 0x000006AB System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m11DC90EB1A059F4201457E33C4422A7BDA90F099 ();
// 0x000006AC System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m9CE8A9F929B99B2318A6F8598EE20E1D4E842ECD ();
// 0x000006AD System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m8F48CBCC48D4CD26F731BA82ECBAC9DC0392AE0D ();
// 0x000006AE System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m9F5CE74EDA110F7539B4081CF3EE6B9FCF40D4A7 ();
// 0x000006AF System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m906CC32CE5FE551DF29928581FFF7DE589C501F2 ();
// 0x000006B0 System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_m614FA9DE53ECB1CF4C6AF6BBC58CE35CA904EB32 ();
// 0x000006B1 System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m16AB65EF6AB38F237F5A6D2D412AB7E5BF7B1349 ();
// 0x000006B2 System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter_FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_m537A709F25C3AA752437A025BEE741BD2F71320E ();
// 0x000006B3 System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mD86AC3A8D918D14200BF80A354E0E43DC5A565A2 ();
// 0x000006B4 System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_m22D9B03F3E1269B8B104E76DA083ED105029258A ();
// 0x000006B5 System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m42813B343A1FDD155C6BFBFCB514E084FB528DA0 ();
// 0x000006B6 System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mC6992B7B1B6A441DEC5315185E3CE022BB567D61 ();
// 0x000006B7 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mEC541297C2228C26AB54F825705F0476D45F877A ();
// 0x000006B8 System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_mA1170F805C77CC89B818D8FBEE533846AF66509C ();
// 0x000006B9 System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mB871339347DCB016E019F509A00BDE9A58105822 ();
// 0x000006BA System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m44A79DDBDF03F254BAFB97BE3E42845B769136C5 ();
// 0x000006BB System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_mA67343988C9E9B71C981A9FFAD620C4A9A6AA267 ();
// 0x000006BC System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_m1EA6A5E31F88A1C7E20167A3BCCE427E9E828116 ();
// 0x000006BD System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_m82972EF3AF67EAAFD94A5EE3EA852CE15BE37FC1 ();
// 0x000006BE System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_m40594D716F53E6E5BC0ECD2FFE8ECA44FAA5C8E4 ();
// 0x000006BF System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_m8462C2DC4F71BDE295BE446B213B73F78442E264 ();
// 0x000006C0 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_m3AC7467ECE689A58590DA325F8B300B08C1E1B5D ();
// 0x000006C1 System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_m081D44F31AA16E345F914869A07BD47D118707DF ();
// 0x000006C2 System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m217B6E2FC4029A304908EE9DC1E4AA2885CBF8A3 ();
// 0x000006C3 System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m24A7CCA0D93F17AC1A12A340277C706B5C2F9BAB ();
// 0x000006C4 System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_m6088452529A70A6684BD8936872B71451779A2F4 ();
// 0x000006C5 System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m79EEE4DF7792F553F5DEDCF0094DAC6F2A58137A ();
// 0x000006C6 System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_m745577078D86EF6C23B914BD03EA1A1D169B9B7B ();
// 0x000006C7 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_mF6C09A2C64F5D2619014ADD50039358FAD24DB3E ();
// 0x000006C8 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_mB0AAA8D034FC575EB3BCF7B0D4514BD110178AD3 ();
// 0x000006C9 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m13B20506F762769F099DE10B3CCA2DF194192B42 ();
// 0x000006CA System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_mD53FD60E0C5930231FB16BDEA37165FF46D85F6E ();
// 0x000006CB System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m1D03D0E14D6054D292334C19030256B666ACDA0E ();
// 0x000006CC System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_m494C501CF565B1ED7C8CB2951EB6FB4F8505637F ();
// 0x000006CD System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m255A7821E5BA4A7A75B9276E07BC9EA7331B5AA6 ();
// 0x000006CE System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_mA4A02EB5C853A44F251F43F0AD5967AE914E2B0F ();
// 0x000006CF System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m04555F8DF147C553FA2D59E33E744901D811B615 ();
// 0x000006D0 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter_FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_mDF0FDFBCD11955E0E1D1C9E961B6AD0690C669ED ();
// 0x000006D1 System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_m99ACA1D2410917C5837321FC5AC84EAED676D4CC ();
// 0x000006D2 System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m639E300B56757BDB94766447365E1C94B5B83ACD ();
// 0x000006D3 System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay_FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m74B8F0AE15DA6C9968C482981EBCF5CC9DB1F43D ();
// 0x000006D4 System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m570C4B4CB3126622D6DFF71158336313C45C717A ();
// 0x000006D5 System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m5F758974DA88ED8187E71A5100D2D9E47985E359 ();
// 0x000006D6 System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_mC32B726B6202883E12E1A62A52E50092E7E9D9F0 ();
// 0x000006D7 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m6CDFDC88D47FE66021C133974C8CB0E16B08A00E ();
// 0x000006D8 System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_m4F61F06DFE11CFAF9B064CCA5B2D6423D5CFC302 ();
// 0x000006D9 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m572903C9070A8AD276D2CB14DF7659AE551C75B3 ();
// 0x000006DA System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_m1A36B043E4EDD945C93DFC49F7FAFB7034728593 ();
// 0x000006DB System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m6F9BE1975CB15EE559D3B617E8972C8812B41325 ();
// 0x000006DC System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m73C6B3DAA27778B666B9B3B75C9D4641FC1BEC8A ();
// 0x000006DD System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mC24F1D67B99F0AFE7535143BB60C530C8E8735F0 ();
// 0x000006DE System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_m68CAA9BDC1DF3454ECB7C5496A5A2020F84027B8 ();
// 0x000006DF System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_m4DB9B9E3836D192AA7F42B7EBDC31883E39610E9 ();
// 0x000006E0 System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_mD99CC6A70945373DA699327F5A0D0C4516A7D02A ();
// 0x000006E1 System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m3286BB7639F6042AD4437D41BF4633326C4290BE ();
// 0x000006E2 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_m61ABFBAA95ED83A248FBCC3F5823907824434D34 ();
// 0x000006E3 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_m65A61DF0BA640F2787DD87E93B8FD9DEC14AACB4 ();
// 0x000006E4 System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_m11DBC535BA8001B48A06F61515C0787C3A86611F ();
// 0x000006E5 System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_mB3C405B4856E9B437E13E4BD85DAE73FFF1F6561 ();
// 0x000006E6 System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m436D4CD2A82C0F95B8D942DF4CCFCE09792EDF0F ();
// 0x000006E7 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_mDB693DABF1C3BA92B7A3A4E1460F28E3FAFB444D ();
// 0x000006E8 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_mAEEAA831C084B447DFD0C91A5FA606CB26D3E22A ();
// 0x000006E9 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_mF01B64B3E5FE5B648DE2EED031962D9183C2D238 ();
// 0x000006EA System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m9CDB87631C324FB89FA7D52F5BC910146F3DDEB7 ();
// 0x000006EB System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m9B68D69B87E07DC0154344E5276EFC5B11205718 ();
// 0x000006EC System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m9699A62E72D3A262EDFDF7AC63ABD33E53F179B6 ();
// 0x000006ED System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_m1B592E7AC81C7F17D0A59325EBDE71E067178E8A ();
// 0x000006EE System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_m5567B541D1602AD54B0F437D4710BCD1951FE2C5 ();
// 0x000006EF System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_mB698E212B8C3B7C66C71C88F4761A4F33FCE4683 ();
// 0x000006F0 System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_m09CFD4E872042E7377BEC8B95D34F22F62ABC45B ();
// 0x000006F1 System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_m1CCF60CA14B2FF530950D366FF078281ADC48FF4 ();
// 0x000006F2 System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_mC19C148659C8C97357DB56F36E14914133DA93CF ();
// 0x000006F3 System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_mD2ABEB338822E0DBCFDEC1DB46EC20BB2C44A8C2 ();
// 0x000006F4 System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m60947EACA10408B6EAD2EC7AC77B4E54E2666DD8 ();
// 0x000006F5 System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_mF5AF9069E523C0DF610EF3BE2017E73A4609E3CC ();
// 0x000006F6 System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_m96D17C13A279F9B442588F6448672BCF07E4A409 ();
// 0x000006F7 System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_mE761EE13D2F9573841EFA5DEE82E545545280BFA ();
// 0x000006F8 System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_mC97FED540BDE59254AB30C5E6BCF1F53B766945F ();
// 0x000006F9 System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_mD84B9A0167705D5D3C8CA024D479DC7B5362E67B ();
// 0x000006FA System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_mA59F3CA197B3A474F4D795E2B3F182179FF633CF ();
// 0x000006FB System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m8D4DD5CA81E5B0C02937D43C1782533D54F34B3F ();
// 0x000006FC System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m4473873008D4A843A26F0D2353FC36ABE74CEED2 ();
// 0x000006FD System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m24BEF670ABD1CCC864FAFE04750FEB83D916D0DF ();
// 0x000006FE System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_mCC864EE9569AF68F53F6EAEB2CE8077CFEAF9E53 ();
// 0x000006FF System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_mAF3B23F4DD98AC3E641700B994B2994C14F0E12C ();
// 0x00000700 System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_mDCAEC737A20F161914DD12A2FCBAB6AB7C64FEF2 ();
// 0x00000701 System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m5F98434E568929859E250743BA214C0782D17F2B ();
// 0x00000702 System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_m01E35B4259BA0B13EC5DA18A11535C2EC6344123 ();
// 0x00000703 System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_mACD450919605863EC4C36DA360898BAEBBF3DDDB ();
// 0x00000704 System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_m6910FF4AC88466E3B7DB867AD553429F1745320D ();
// 0x00000705 System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mBFB58FE53145750AD747B38D9D1E7E00F8DBF9A0 ();
// 0x00000706 System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_m328088EC0F893B3E60BB072F77ADF939B8566E4D ();
// 0x00000707 System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_m67833553C2739616BF059C661F92A032885510B2 ();
// 0x00000708 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_m9643904751E2DF0A7DF536847AEA1A2B0774DD20 ();
// 0x00000709 System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_mDF931C271901519BF21FD356F706FA8CDE236406 ();
// 0x0000070A UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m2C738EA265E2B35868110EE1D8FCBD4F1D61C038 ();
// 0x0000070B System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mAAAB1687869E0121C858C65BDB2D294D55CFB67A ();
// 0x0000070C System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_mC3EAA1AE81FB3DA4B25F20E557EC6627A06DCB31 ();
// 0x0000070D System.Int32 RSG.IPromise`1::get_Id()
// 0x0000070E RSG.IPromise`1<PromisedT> RSG.IPromise`1::WithName(System.String)
// 0x0000070F System.Void RSG.IPromise`1::Done(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x00000710 System.Void RSG.IPromise`1::Done(System.Action`1<PromisedT>)
// 0x00000711 System.Void RSG.IPromise`1::Done()
// 0x00000712 RSG.IPromise RSG.IPromise`1::Catch(System.Action`1<System.Exception>)
// 0x00000713 RSG.IPromise`1<PromisedT> RSG.IPromise`1::Catch(System.Func`2<System.Exception,PromisedT>)
// 0x00000714 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>)
// 0x00000715 RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>)
// 0x00000716 RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>)
// 0x00000717 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x00000718 RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>)
// 0x00000719 RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x0000071A RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x0000071B RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x0000071C RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x0000071D RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,ConvertedT>)
// 0x0000071E RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.IPromise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x0000071F RSG.IPromise RSG.IPromise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x00000720 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x00000721 RSG.IPromise RSG.IPromise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x00000722 RSG.IPromise`1<PromisedT> RSG.IPromise`1::Finally(System.Action)
// 0x00000723 RSG.IPromise RSG.IPromise`1::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x00000724 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x00000725 RSG.IPromise`1<PromisedT> RSG.IPromise`1::Progress(System.Action`1<System.Single>)
// 0x00000726 System.Void RSG.IRejectable::Reject(System.Exception)
// 0x00000727 System.Int32 RSG.IPendingPromise`1::get_Id()
// 0x00000728 System.Void RSG.IPendingPromise`1::Resolve(PromisedT)
// 0x00000729 System.Void RSG.IPendingPromise`1::ReportProgress(System.Single)
// 0x0000072A System.Int32 RSG.Promise`1::get_Id()
// 0x0000072B System.String RSG.Promise`1::get_Name()
// 0x0000072C System.Void RSG.Promise`1::set_Name(System.String)
// 0x0000072D RSG.PromiseState RSG.Promise`1::get_CurState()
// 0x0000072E System.Void RSG.Promise`1::set_CurState(RSG.PromiseState)
// 0x0000072F System.Void RSG.Promise`1::.ctor()
// 0x00000730 System.Void RSG.Promise`1::.ctor(System.Action`2<System.Action`1<PromisedT>,System.Action`1<System.Exception>>)
// 0x00000731 System.Void RSG.Promise`1::AddRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable)
// 0x00000732 System.Void RSG.Promise`1::AddResolveHandler(System.Action`1<PromisedT>,RSG.IRejectable)
// 0x00000733 System.Void RSG.Promise`1::AddProgressHandler(System.Action`1<System.Single>,RSG.IRejectable)
// 0x00000734 System.Void RSG.Promise`1::InvokeHandler(System.Action`1<T>,RSG.IRejectable,T)
// 0x00000735 System.Void RSG.Promise`1::ClearHandlers()
// 0x00000736 System.Void RSG.Promise`1::InvokeRejectHandlers(System.Exception)
// 0x00000737 System.Void RSG.Promise`1::InvokeResolveHandlers(PromisedT)
// 0x00000738 System.Void RSG.Promise`1::InvokeProgressHandlers(System.Single)
// 0x00000739 System.Void RSG.Promise`1::Reject(System.Exception)
// 0x0000073A System.Void RSG.Promise`1::Resolve(PromisedT)
// 0x0000073B System.Void RSG.Promise`1::ReportProgress(System.Single)
// 0x0000073C System.Void RSG.Promise`1::Done(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x0000073D System.Void RSG.Promise`1::Done(System.Action`1<PromisedT>)
// 0x0000073E System.Void RSG.Promise`1::Done()
// 0x0000073F RSG.IPromise`1<PromisedT> RSG.Promise`1::WithName(System.String)
// 0x00000740 RSG.IPromise RSG.Promise`1::Catch(System.Action`1<System.Exception>)
// 0x00000741 RSG.IPromise`1<PromisedT> RSG.Promise`1::Catch(System.Func`2<System.Exception,PromisedT>)
// 0x00000742 RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>)
// 0x00000743 RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>)
// 0x00000744 RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>)
// 0x00000745 RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x00000746 RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>)
// 0x00000747 RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x00000748 RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x00000749 RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x0000074A RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x0000074B RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,ConvertedT>)
// 0x0000074C System.Void RSG.Promise`1::ActionHandlers(RSG.IRejectable,System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x0000074D System.Void RSG.Promise`1::ProgressHandlers(RSG.IRejectable,System.Action`1<System.Single>)
// 0x0000074E RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x0000074F RSG.IPromise RSG.Promise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x00000750 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<PromisedT>> RSG.Promise`1::All(RSG.IPromise`1<PromisedT>[])
// 0x00000751 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<PromisedT>> RSG.Promise`1::All(System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<PromisedT>>)
// 0x00000752 RSG.IPromise`1<ConvertedT> RSG.Promise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x00000753 RSG.IPromise RSG.Promise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x00000754 RSG.IPromise`1<PromisedT> RSG.Promise`1::Race(RSG.IPromise`1<PromisedT>[])
// 0x00000755 RSG.IPromise`1<PromisedT> RSG.Promise`1::Race(System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<PromisedT>>)
// 0x00000756 RSG.IPromise`1<PromisedT> RSG.Promise`1::Resolved(PromisedT)
// 0x00000757 RSG.IPromise`1<PromisedT> RSG.Promise`1::Rejected(System.Exception)
// 0x00000758 RSG.IPromise`1<PromisedT> RSG.Promise`1::Finally(System.Action)
// 0x00000759 RSG.IPromise RSG.Promise`1::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x0000075A RSG.IPromise`1<ConvertedT> RSG.Promise`1::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x0000075B RSG.IPromise`1<PromisedT> RSG.Promise`1::Progress(System.Action`1<System.Single>)
// 0x0000075C System.Void RSG.Promise`1::<Done>b__30_0(System.Exception)
// 0x0000075D System.Void RSG.Promise`1::<Done>b__31_0(System.Exception)
// 0x0000075E System.Void RSG.Promise`1::<Done>b__32_0(System.Exception)
// 0x0000075F RSG.IPromise`1<RSG.Tuple`2<T1,T2>> RSG.PromiseHelpers::All(RSG.IPromise`1<T1>,RSG.IPromise`1<T2>)
// 0x00000760 RSG.IPromise`1<RSG.Tuple`3<T1,T2,T3>> RSG.PromiseHelpers::All(RSG.IPromise`1<T1>,RSG.IPromise`1<T2>,RSG.IPromise`1<T3>)
// 0x00000761 RSG.IPromise`1<RSG.Tuple`4<T1,T2,T3,T4>> RSG.PromiseHelpers::All(RSG.IPromise`1<T1>,RSG.IPromise`1<T2>,RSG.IPromise`1<T3>,RSG.IPromise`1<T4>)
// 0x00000762 System.Void RSG.PromiseCancelledException::.ctor()
extern void PromiseCancelledException__ctor_m030697F2BC9A3E445CCA7C2C8C2A929815AF9F78 ();
// 0x00000763 System.Void RSG.PromiseCancelledException::.ctor(System.String)
extern void PromiseCancelledException__ctor_m323888449BD9E45FAC65ED965B9A2BB2FC95542A ();
// 0x00000764 System.Void RSG.PredicateWait::.ctor()
extern void PredicateWait__ctor_mC08AF6709F2CBE2195CA76D97541173CB67F29D7 ();
// 0x00000765 RSG.IPromise RSG.IPromiseTimer::WaitFor(System.Single)
// 0x00000766 RSG.IPromise RSG.IPromiseTimer::WaitUntil(System.Func`2<RSG.TimeData,System.Boolean>)
// 0x00000767 RSG.IPromise RSG.IPromiseTimer::WaitWhile(System.Func`2<RSG.TimeData,System.Boolean>)
// 0x00000768 System.Void RSG.IPromiseTimer::Update(System.Single)
// 0x00000769 System.Boolean RSG.IPromiseTimer::Cancel(RSG.IPromise)
// 0x0000076A RSG.IPromise RSG.PromiseTimer::WaitFor(System.Single)
extern void PromiseTimer_WaitFor_m0529FE7F8390A993817A2A2BBB3681E4A3CB8884 ();
// 0x0000076B RSG.IPromise RSG.PromiseTimer::WaitWhile(System.Func`2<RSG.TimeData,System.Boolean>)
extern void PromiseTimer_WaitWhile_m4D17DB0C6BAF26BDDB218B85E39ED46187C59942 ();
// 0x0000076C RSG.IPromise RSG.PromiseTimer::WaitUntil(System.Func`2<RSG.TimeData,System.Boolean>)
extern void PromiseTimer_WaitUntil_m566F62EAF61CC80FEB9D55C4E280A983F7B433DE ();
// 0x0000076D System.Boolean RSG.PromiseTimer::Cancel(RSG.IPromise)
extern void PromiseTimer_Cancel_mB9D64EE0A89E91089E2F9CA5767E4608EDD33436 ();
// 0x0000076E System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait> RSG.PromiseTimer::FindInWaiting(RSG.IPromise)
extern void PromiseTimer_FindInWaiting_mEFABFD6D1047E169ED375A258A9D2EAA8E0DFD86 ();
// 0x0000076F System.Void RSG.PromiseTimer::Update(System.Single)
extern void PromiseTimer_Update_m65584A695C723DDE11E807CAB9DD87AE0DB717E9 ();
// 0x00000770 System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait> RSG.PromiseTimer::RemoveNode(System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait>)
extern void PromiseTimer_RemoveNode_mA5D117461EAD340EC4BE54013BE1D5D47A50B96F ();
// 0x00000771 System.Void RSG.PromiseTimer::.ctor()
extern void PromiseTimer__ctor_m5E4CD41451C70CB626BD9855456A3D364C35A188 ();
// 0x00000772 System.Int32 RSG.IPromise::get_Id()
// 0x00000773 RSG.IPromise RSG.IPromise::WithName(System.String)
// 0x00000774 System.Void RSG.IPromise::Done(System.Action,System.Action`1<System.Exception>)
// 0x00000775 System.Void RSG.IPromise::Done(System.Action)
// 0x00000776 System.Void RSG.IPromise::Done()
// 0x00000777 RSG.IPromise RSG.IPromise::Catch(System.Action`1<System.Exception>)
// 0x00000778 RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x00000779 RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>)
// 0x0000077A RSG.IPromise RSG.IPromise::Then(System.Action)
// 0x0000077B RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x0000077C RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>)
// 0x0000077D RSG.IPromise RSG.IPromise::Then(System.Action,System.Action`1<System.Exception>)
// 0x0000077E RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x0000077F RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x00000780 RSG.IPromise RSG.IPromise::Then(System.Action,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x00000781 RSG.IPromise RSG.IPromise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x00000782 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.IPromise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x00000783 RSG.IPromise RSG.IPromise::ThenSequence(System.Func`1<System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>>)
// 0x00000784 RSG.IPromise RSG.IPromise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x00000785 RSG.IPromise`1<ConvertedT> RSG.IPromise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x00000786 RSG.IPromise RSG.IPromise::Finally(System.Action)
// 0x00000787 RSG.IPromise RSG.IPromise::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x00000788 RSG.IPromise`1<ConvertedT> RSG.IPromise::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x00000789 RSG.IPromise RSG.IPromise::Progress(System.Action`1<System.Single>)
// 0x0000078A System.Int32 RSG.IPendingPromise::get_Id()
// 0x0000078B System.Void RSG.IPendingPromise::Resolve()
// 0x0000078C System.Void RSG.IPendingPromise::ReportProgress(System.Single)
// 0x0000078D System.Int32 RSG.IPromiseInfo::get_Id()
// 0x0000078E System.String RSG.IPromiseInfo::get_Name()
// 0x0000078F System.Void RSG.ExceptionEventArgs::.ctor(System.Exception)
extern void ExceptionEventArgs__ctor_m93824CCD3489EB59D01C6CFFE7B43A53B7F30C90 ();
// 0x00000790 System.Exception RSG.ExceptionEventArgs::get_Exception()
extern void ExceptionEventArgs_get_Exception_m6C8716CA4FB7C4BC8A73ECFC575F105AB95E435E ();
// 0x00000791 System.Void RSG.ExceptionEventArgs::set_Exception(System.Exception)
extern void ExceptionEventArgs_set_Exception_m877F36F858B5D027D965CDFC78F3368BB2D7B0D4 ();
// 0x00000792 System.Void RSG.Promise::add_UnhandledException(System.EventHandler`1<RSG.ExceptionEventArgs>)
extern void Promise_add_UnhandledException_m941463FE821F0D72B1BFB10931A8BA9334796AF2 ();
// 0x00000793 System.Void RSG.Promise::remove_UnhandledException(System.EventHandler`1<RSG.ExceptionEventArgs>)
extern void Promise_remove_UnhandledException_m300D2B53F6534C0B3814F86B4DAC54E6AF8B26DD ();
// 0x00000794 System.Collections.Generic.IEnumerable`1<RSG.IPromiseInfo> RSG.Promise::GetPendingPromises()
extern void Promise_GetPendingPromises_m95C0BBD9DBB94BC438E394562C1CF780AF58F2F5 ();
// 0x00000795 System.Int32 RSG.Promise::get_Id()
extern void Promise_get_Id_m1B96CB48D753887FA47004BDF844BEC9D7C07D27 ();
// 0x00000796 System.String RSG.Promise::get_Name()
extern void Promise_get_Name_m6C739D83EBE47A29C224B6D429A68DF7679D152E ();
// 0x00000797 System.Void RSG.Promise::set_Name(System.String)
extern void Promise_set_Name_m6D430D307BEFB602FFACC551FBD0A1D807C5FF1A ();
// 0x00000798 RSG.PromiseState RSG.Promise::get_CurState()
extern void Promise_get_CurState_m6BBB58E657D13BF0CD04C9655F6F21B9ED43C41D ();
// 0x00000799 System.Void RSG.Promise::set_CurState(RSG.PromiseState)
extern void Promise_set_CurState_m32B7787EC17864659AFDD172596A29D83F6B5276 ();
// 0x0000079A System.Void RSG.Promise::.ctor()
extern void Promise__ctor_m74479ED63EA3F49B8359E3EE719F5908EBF09827 ();
// 0x0000079B System.Void RSG.Promise::.ctor(System.Action`2<System.Action,System.Action`1<System.Exception>>)
extern void Promise__ctor_mF031BEF18EAD28846907566FE0C342BCB2F25582 ();
// 0x0000079C System.Int32 RSG.Promise::NextId()
extern void Promise_NextId_mAD3EC5C27AC9D6D63BC742AEF2AC10457B6D1264 ();
// 0x0000079D System.Void RSG.Promise::AddRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable)
extern void Promise_AddRejectHandler_m727EC90B67554F44C4ACC0C9EC271B35528CC964 ();
// 0x0000079E System.Void RSG.Promise::AddResolveHandler(System.Action,RSG.IRejectable)
extern void Promise_AddResolveHandler_m9A302DBCBEE45AF916835B674404517018732093 ();
// 0x0000079F System.Void RSG.Promise::AddProgressHandler(System.Action`1<System.Single>,RSG.IRejectable)
extern void Promise_AddProgressHandler_mE0304DED3ED13E1F9C097EEF03A22054B34245B2 ();
// 0x000007A0 System.Void RSG.Promise::InvokeRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable,System.Exception)
extern void Promise_InvokeRejectHandler_mEAC453FA262BC879419E0133D02BA4CE571265C8 ();
// 0x000007A1 System.Void RSG.Promise::InvokeResolveHandler(System.Action,RSG.IRejectable)
extern void Promise_InvokeResolveHandler_m07E8086C139F2B59867859A4F0C441B633F5BCA2 ();
// 0x000007A2 System.Void RSG.Promise::InvokeProgressHandler(System.Action`1<System.Single>,RSG.IRejectable,System.Single)
extern void Promise_InvokeProgressHandler_m5F9131DA45C48F195CDEDDB3AEA8BF9B4AA4221C ();
// 0x000007A3 System.Void RSG.Promise::ClearHandlers()
extern void Promise_ClearHandlers_mC9D48501996B8DE89E2482A2B9D5FBC131408E0E ();
// 0x000007A4 System.Void RSG.Promise::InvokeRejectHandlers(System.Exception)
extern void Promise_InvokeRejectHandlers_m633D8D6BD825251FE3B80773BA7CCD3891763B09 ();
// 0x000007A5 System.Void RSG.Promise::InvokeResolveHandlers()
extern void Promise_InvokeResolveHandlers_m960BB0612F17CF8C11E51AEB39B8D992075AECA8 ();
// 0x000007A6 System.Void RSG.Promise::InvokeProgressHandlers(System.Single)
extern void Promise_InvokeProgressHandlers_m3DCA143530F7A34478B6857C6BBE317335158B07 ();
// 0x000007A7 System.Void RSG.Promise::Reject(System.Exception)
extern void Promise_Reject_mBD86521F8A2146603C73294A82CFDCEFAA16CE37 ();
// 0x000007A8 System.Void RSG.Promise::Resolve()
extern void Promise_Resolve_m472421BD74A2F4B26381B2647E41D9E9213C8DC4 ();
// 0x000007A9 System.Void RSG.Promise::ReportProgress(System.Single)
extern void Promise_ReportProgress_mBFE7ADC3DD1288940A3FD8C22D47721A63E4095A ();
// 0x000007AA System.Void RSG.Promise::Done(System.Action,System.Action`1<System.Exception>)
extern void Promise_Done_m4993F845C81D3E879B4F0A5FD7707AAB7CF3EDCA ();
// 0x000007AB System.Void RSG.Promise::Done(System.Action)
extern void Promise_Done_m62C247DA7CD5916E319B453E928F9CF10677F1E0 ();
// 0x000007AC System.Void RSG.Promise::Done()
extern void Promise_Done_m6214125B2C3D5E1E4C605E7E63027D9A5B9EEF9E ();
// 0x000007AD RSG.IPromise RSG.Promise::WithName(System.String)
extern void Promise_WithName_m056817CB1DFC0AECA1E6F89B7F33B8751E2BC641 ();
// 0x000007AE RSG.IPromise RSG.Promise::Catch(System.Action`1<System.Exception>)
extern void Promise_Catch_mD44B946DA04440FB389D4F3D824B3F6D0ECC6436 ();
// 0x000007AF RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x000007B0 RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>)
extern void Promise_Then_mAE61A3C357AFD1DA2DFC968DC97D62E5D2485354 ();
// 0x000007B1 RSG.IPromise RSG.Promise::Then(System.Action)
extern void Promise_Then_m882CBA16CDC16CCB804EE7A04A845CF124890F8A ();
// 0x000007B2 RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x000007B3 RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>)
extern void Promise_Then_m52095C7766399A71B875EFACF38B1A796F4F8C67 ();
// 0x000007B4 RSG.IPromise RSG.Promise::Then(System.Action,System.Action`1<System.Exception>)
extern void Promise_Then_m775060BCFE154B25F9CEDDBD48F7D414C3CF8286 ();
// 0x000007B5 RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x000007B6 RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
extern void Promise_Then_m60CADBEDBB3AAFC26BD4EC6D538400058DD4CC1B ();
// 0x000007B7 RSG.IPromise RSG.Promise::Then(System.Action,System.Action`1<System.Exception>,System.Action`1<System.Single>)
extern void Promise_Then_m2C3A022FCE5E18FB1076017E4F60F8E7DDBE7409 ();
// 0x000007B8 System.Void RSG.Promise::ActionHandlers(RSG.IRejectable,System.Action,System.Action`1<System.Exception>)
extern void Promise_ActionHandlers_m0F8C97FC81D290E1CEB3B90EFC49881B17CCDD0C ();
// 0x000007B9 System.Void RSG.Promise::ProgressHandlers(RSG.IRejectable,System.Action`1<System.Single>)
extern void Promise_ProgressHandlers_m606A9EEA6147508575B64DA430B847DF5DC0BDF8 ();
// 0x000007BA RSG.IPromise RSG.Promise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
extern void Promise_ThenAll_m3015B5D9AC9E6993B8DFD4D9B55DE6F3DD3C032C ();
// 0x000007BB RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000007BC RSG.IPromise RSG.Promise::All(RSG.IPromise[])
extern void Promise_All_m82839CDADB53DD91569988C65EC287EED839ECF4 ();
// 0x000007BD RSG.IPromise RSG.Promise::All(System.Collections.Generic.IEnumerable`1<RSG.IPromise>)
extern void Promise_All_m7981EBD01C3DD56996F970D3B8EF61FEAD7FDC23 ();
// 0x000007BE RSG.IPromise RSG.Promise::ThenSequence(System.Func`1<System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>>)
extern void Promise_ThenSequence_m5A5323085CD776CB5E8056598851FC5F13B614E1 ();
// 0x000007BF RSG.IPromise RSG.Promise::Sequence(System.Func`1<RSG.IPromise>[])
extern void Promise_Sequence_mCDCB22689B059501F850EF021209D37956F933B5 ();
// 0x000007C0 RSG.IPromise RSG.Promise::Sequence(System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>)
extern void Promise_Sequence_mE48B325D21BE25E5BED812EF4B0F85445F03CD67 ();
// 0x000007C1 RSG.IPromise RSG.Promise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
extern void Promise_ThenRace_m5F2C5506CB757272201882A203B0A891105D316D ();
// 0x000007C2 RSG.IPromise`1<ConvertedT> RSG.Promise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000007C3 RSG.IPromise RSG.Promise::Race(RSG.IPromise[])
extern void Promise_Race_m78DEF2E794FF3713EB41095CB056B7DE90F61329 ();
// 0x000007C4 RSG.IPromise RSG.Promise::Race(System.Collections.Generic.IEnumerable`1<RSG.IPromise>)
extern void Promise_Race_m044CC5D4C0B92A1E6F5B3F664238B8F149E4C787 ();
// 0x000007C5 RSG.IPromise RSG.Promise::Resolved()
extern void Promise_Resolved_m68DB50577F8EEF1CA2536EDDB26928A5A85DE6A3 ();
// 0x000007C6 RSG.IPromise RSG.Promise::Rejected(System.Exception)
extern void Promise_Rejected_mB2B4B779181CFB7A0E1990FFA140A233514AF2B8 ();
// 0x000007C7 RSG.IPromise RSG.Promise::Finally(System.Action)
extern void Promise_Finally_mC960B8E819FF3975B3C58B88BA8C886E5AE8361B ();
// 0x000007C8 RSG.IPromise RSG.Promise::ContinueWith(System.Func`1<RSG.IPromise>)
extern void Promise_ContinueWith_mD3A2C762DEF611012BBA39BDFB076ED25CC81AF9 ();
// 0x000007C9 RSG.IPromise`1<ConvertedT> RSG.Promise::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x000007CA RSG.IPromise RSG.Promise::Progress(System.Action`1<System.Single>)
extern void Promise_Progress_m66861F86E45D527A1D4371EB5A3B26C41FCF1DE7 ();
// 0x000007CB System.Void RSG.Promise::PropagateUnhandledException(System.Object,System.Exception)
extern void Promise_PropagateUnhandledException_mDAF1E6FD53CD7F1E470BD091CEABC3FC6F819A8B ();
// 0x000007CC System.Void RSG.Promise::.cctor()
extern void Promise__cctor_mDF7E11D37225ACD6A42CC2FE1E4AD32671A16050 ();
// 0x000007CD System.Void RSG.Promise::<InvokeResolveHandlers>b__35_0(RSG.Promise_ResolveHandler)
extern void Promise_U3CInvokeResolveHandlersU3Eb__35_0_m5EE7EC2D8D0D87277E2B6D0615FDA4E88BA55E93 ();
// 0x000007CE System.Void RSG.Promise::<Done>b__40_0(System.Exception)
extern void Promise_U3CDoneU3Eb__40_0_m56A2395BBD3D2807B926F86AEA92D0B4867CD8F0 ();
// 0x000007CF System.Void RSG.Promise::<Done>b__41_0(System.Exception)
extern void Promise_U3CDoneU3Eb__41_0_m4CD266F731687F1277C63E96B638B9E83EEE27EB ();
// 0x000007D0 System.Void RSG.Promise::<Done>b__42_0(System.Exception)
extern void Promise_U3CDoneU3Eb__42_0_m87C455CEA250E7327645A69DF866C4EEC4531ADB ();
// 0x000007D1 RSG.Tuple`2<T1,T2> RSG.Tuple::Create(T1,T2)
// 0x000007D2 RSG.Tuple`3<T1,T2,T3> RSG.Tuple::Create(T1,T2,T3)
// 0x000007D3 RSG.Tuple`4<T1,T2,T3,T4> RSG.Tuple::Create(T1,T2,T3,T4)
// 0x000007D4 System.Void RSG.Tuple::.ctor()
extern void Tuple__ctor_m93D270330C9264FC3AB9AC7994C49DE719240BA1 ();
// 0x000007D5 System.Void RSG.Tuple`2::.ctor(T1,T2)
// 0x000007D6 T1 RSG.Tuple`2::get_Item1()
// 0x000007D7 System.Void RSG.Tuple`2::set_Item1(T1)
// 0x000007D8 T2 RSG.Tuple`2::get_Item2()
// 0x000007D9 System.Void RSG.Tuple`2::set_Item2(T2)
// 0x000007DA System.Void RSG.Tuple`3::.ctor(T1,T2,T3)
// 0x000007DB T1 RSG.Tuple`3::get_Item1()
// 0x000007DC System.Void RSG.Tuple`3::set_Item1(T1)
// 0x000007DD T2 RSG.Tuple`3::get_Item2()
// 0x000007DE System.Void RSG.Tuple`3::set_Item2(T2)
// 0x000007DF T3 RSG.Tuple`3::get_Item3()
// 0x000007E0 System.Void RSG.Tuple`3::set_Item3(T3)
// 0x000007E1 System.Void RSG.Tuple`4::.ctor(T1,T2,T3,T4)
// 0x000007E2 T1 RSG.Tuple`4::get_Item1()
// 0x000007E3 System.Void RSG.Tuple`4::set_Item1(T1)
// 0x000007E4 T2 RSG.Tuple`4::get_Item2()
// 0x000007E5 System.Void RSG.Tuple`4::set_Item2(T2)
// 0x000007E6 T3 RSG.Tuple`4::get_Item3()
// 0x000007E7 System.Void RSG.Tuple`4::set_Item3(T3)
// 0x000007E8 T4 RSG.Tuple`4::get_Item4()
// 0x000007E9 System.Void RSG.Tuple`4::set_Item4(T4)
// 0x000007EA System.Void RSG.Exceptions.PromiseException::.ctor()
extern void PromiseException__ctor_m0B6078A58B2840086737C7E6431C15EC9546E73E ();
// 0x000007EB System.Void RSG.Exceptions.PromiseException::.ctor(System.String)
extern void PromiseException__ctor_mD633D198CE0C78BAA84BD2BB9D03F09EFA4F8931 ();
// 0x000007EC System.Void RSG.Exceptions.PromiseException::.ctor(System.String,System.Exception)
extern void PromiseException__ctor_m3B3A828C1DBEA521CE7FADC387480191C26B211D ();
// 0x000007ED System.Void RSG.Exceptions.PromiseStateException::.ctor()
extern void PromiseStateException__ctor_m3F47AB95336E2494D42CA7D7F82ED5931C699FA8 ();
// 0x000007EE System.Void RSG.Exceptions.PromiseStateException::.ctor(System.String)
extern void PromiseStateException__ctor_mE5A772777FDF0B4CED17F8DEC8B33078302D5C43 ();
// 0x000007EF System.Void RSG.Exceptions.PromiseStateException::.ctor(System.String,System.Exception)
extern void PromiseStateException__ctor_m60FCCFAA1B0C7DB848B551706DF46B0580F24B04 ();
// 0x000007F0 System.Void RSG.Promises.EnumerableExt::Each(System.Collections.Generic.IEnumerable`1<T>,System.Action`1<T>)
// 0x000007F1 System.Void RSG.Promises.EnumerableExt::Each(System.Collections.Generic.IEnumerable`1<T>,System.Action`2<T,System.Int32>)
// 0x000007F2 System.Collections.Generic.IEnumerable`1<T> RSG.Promises.EnumerableExt::FromItems(T[])
// 0x000007F3 System.Collections.IEnumerator Proyecto26.HttpBase::CreateRequestAndRetry(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void HttpBase_CreateRequestAndRetry_mC2DC93BB1F8E3DBDAB19DBAAE80BF0323E48D4C6 ();
// 0x000007F4 UnityEngine.Networking.UnityWebRequest Proyecto26.HttpBase::CreateRequest(Proyecto26.RequestHelper)
extern void HttpBase_CreateRequest_m275DB6EB32A338391A7422933B486595D82DB9D2 ();
// 0x000007F5 Proyecto26.RequestException Proyecto26.HttpBase::CreateException(UnityEngine.Networking.UnityWebRequest)
extern void HttpBase_CreateException_m194CF2E26B1376590E2E20D91A012AF5556DE4F3 ();
// 0x000007F6 System.Void Proyecto26.HttpBase::DebugLog(System.Boolean,System.Object,System.Boolean)
extern void HttpBase_DebugLog_m280EFB4540C3CCB190B890A1631862150E991A4B ();
// 0x000007F7 System.Collections.IEnumerator Proyecto26.HttpBase::DefaultUnityWebRequest(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void HttpBase_DefaultUnityWebRequest_m2CF8E2BFE0119EA9B76B4FF7896C5D47C29292AA ();
// 0x000007F8 System.Collections.IEnumerator Proyecto26.HttpBase::DefaultUnityWebRequest(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,TResponse>)
// 0x000007F9 System.Collections.IEnumerator Proyecto26.HttpBase::DefaultUnityWebRequest(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,TResponse[]>)
// 0x000007FA T[] Proyecto26.JsonHelper::ArrayFromJson(System.String)
// 0x000007FB T[] Proyecto26.JsonHelper::FromJsonString(System.String)
// 0x000007FC System.String Proyecto26.JsonHelper::ArrayToJsonString(T[])
// 0x000007FD System.String Proyecto26.JsonHelper::ArrayToJsonString(T[],System.Boolean)
// 0x000007FE System.Boolean Proyecto26.RequestException::get_IsHttpError()
extern void RequestException_get_IsHttpError_mFF87E7CAB853D22BD0345719750444487D1736B5 ();
// 0x000007FF System.Void Proyecto26.RequestException::set_IsHttpError(System.Boolean)
extern void RequestException_set_IsHttpError_mF5A40E18BE001DDEE41D4B5FBA9E0DED104F44EB ();
// 0x00000800 System.Boolean Proyecto26.RequestException::get_IsNetworkError()
extern void RequestException_get_IsNetworkError_m017D90CE96C6FA0165454BCD9C1E0BDC33044941 ();
// 0x00000801 System.Void Proyecto26.RequestException::set_IsNetworkError(System.Boolean)
extern void RequestException_set_IsNetworkError_m347A0E2913201C77B6D2CE644694F1BACFB9B43A ();
// 0x00000802 System.Int64 Proyecto26.RequestException::get_StatusCode()
extern void RequestException_get_StatusCode_m814AA8E99FD8EE2052D6224666D5092966E7AF05 ();
// 0x00000803 System.Void Proyecto26.RequestException::set_StatusCode(System.Int64)
extern void RequestException_set_StatusCode_mADD8E870A5939F0BA68B47BA77BEEF4EE5556BF2 ();
// 0x00000804 System.String Proyecto26.RequestException::get_ServerMessage()
extern void RequestException_get_ServerMessage_mE37BBA653D256A6D0F758463ACAD369939FE9623 ();
// 0x00000805 System.Void Proyecto26.RequestException::set_ServerMessage(System.String)
extern void RequestException_set_ServerMessage_m60F4D169E2007D87AFDD8D221B6555DC452DB8AD ();
// 0x00000806 System.String Proyecto26.RequestException::get_Response()
extern void RequestException_get_Response_mECD4188FFA6183AF6524C1A9D7427A61F498E0C3 ();
// 0x00000807 System.Void Proyecto26.RequestException::set_Response(System.String)
extern void RequestException_set_Response_m72C53532E5A721556BE396D4454127B2B43BA8BA ();
// 0x00000808 System.Void Proyecto26.RequestException::.ctor()
extern void RequestException__ctor_mD06A4E9B88C6CF90EF35A84EAA108C119BE0CAD7 ();
// 0x00000809 System.Void Proyecto26.RequestException::.ctor(System.String)
extern void RequestException__ctor_m30814F6D6E2C849E3DD4B9F80EA517986AC34AB8 ();
// 0x0000080A System.Void Proyecto26.RequestException::.ctor(System.String,System.Object[])
extern void RequestException__ctor_m0D1B1BD0F895C3D46BE0044FD92DA7C583308EB1 ();
// 0x0000080B System.Void Proyecto26.RequestException::.ctor(System.String,System.Boolean,System.Boolean,System.Int64,System.String)
extern void RequestException__ctor_mEB51E7ADCFD3D9DD7742F99201CBE3FCB98DFC41 ();
// 0x0000080C System.String Proyecto26.RequestHelper::get_Uri()
extern void RequestHelper_get_Uri_mA0F26E3CEFC06E4296FD778AD134D28B6C9F2F0E ();
// 0x0000080D System.Void Proyecto26.RequestHelper::set_Uri(System.String)
extern void RequestHelper_set_Uri_m5084F40F17E97D15B9DBAFF2C5842F9C23F37125 ();
// 0x0000080E System.String Proyecto26.RequestHelper::get_Method()
extern void RequestHelper_get_Method_mE7D65CD2E38840BAA7152E6C479505AB5785C239 ();
// 0x0000080F System.Void Proyecto26.RequestHelper::set_Method(System.String)
extern void RequestHelper_set_Method_m0271B5138A9F42F647934636114EF52554A2E135 ();
// 0x00000810 System.Object Proyecto26.RequestHelper::get_Body()
extern void RequestHelper_get_Body_mF61A9E742CC917DBE6D34AA01215FE275C538696 ();
// 0x00000811 System.Void Proyecto26.RequestHelper::set_Body(System.Object)
extern void RequestHelper_set_Body_m2DA99D4521DDC5E9AFCC56A7CA17B1AB4B04F772 ();
// 0x00000812 System.String Proyecto26.RequestHelper::get_BodyString()
extern void RequestHelper_get_BodyString_m653C16D59BCD18AA6FAF96D95E2BBDA61FD7A6D2 ();
// 0x00000813 System.Void Proyecto26.RequestHelper::set_BodyString(System.String)
extern void RequestHelper_set_BodyString_mE89CA3FA1126508706C73FAD358B0288775A2CED ();
// 0x00000814 System.Byte[] Proyecto26.RequestHelper::get_BodyRaw()
extern void RequestHelper_get_BodyRaw_m266FF4343D3F90EE294C25F9D6905B863E7E9BC9 ();
// 0x00000815 System.Void Proyecto26.RequestHelper::set_BodyRaw(System.Byte[])
extern void RequestHelper_set_BodyRaw_m94F63910CCB1D202251DFB86C70634700280434A ();
// 0x00000816 System.Nullable`1<System.Int32> Proyecto26.RequestHelper::get_Timeout()
extern void RequestHelper_get_Timeout_m0217319BD8221533E25AF67D92BC37AE5D181FCD ();
// 0x00000817 System.Void Proyecto26.RequestHelper::set_Timeout(System.Nullable`1<System.Int32>)
extern void RequestHelper_set_Timeout_m2CF3BE56E39C366C09A273CC723F758EA5D2432A ();
// 0x00000818 System.String Proyecto26.RequestHelper::get_ContentType()
extern void RequestHelper_get_ContentType_m86014A7728C445B47A8E1A48A58B1661FABEA809 ();
// 0x00000819 System.Void Proyecto26.RequestHelper::set_ContentType(System.String)
extern void RequestHelper_set_ContentType_mD3FDDEDC31A8590CEF738B93068413D24CAF3927 ();
// 0x0000081A System.Int32 Proyecto26.RequestHelper::get_Retries()
extern void RequestHelper_get_Retries_mE6A90798726222C0C7852A068A3DE5EDC6ED3ADC ();
// 0x0000081B System.Void Proyecto26.RequestHelper::set_Retries(System.Int32)
extern void RequestHelper_set_Retries_m45A0714E13CE97BA7AD480FEB5A9A1075BC47425 ();
// 0x0000081C System.Single Proyecto26.RequestHelper::get_RetrySecondsDelay()
extern void RequestHelper_get_RetrySecondsDelay_m78A4C284E0282F6C3090BE1ED74D631EC4C850C5 ();
// 0x0000081D System.Void Proyecto26.RequestHelper::set_RetrySecondsDelay(System.Single)
extern void RequestHelper_set_RetrySecondsDelay_m984D36BA1BF60CF8C800FA48BF2DBE44EDEF7A19 ();
// 0x0000081E System.Action`2<Proyecto26.RequestException,System.Int32> Proyecto26.RequestHelper::get_RetryCallback()
extern void RequestHelper_get_RetryCallback_m4D106F45CFA6C9FCF7A348A02A61C5D8BB92220B ();
// 0x0000081F System.Void Proyecto26.RequestHelper::set_RetryCallback(System.Action`2<Proyecto26.RequestException,System.Int32>)
extern void RequestHelper_set_RetryCallback_m2C497361D572027AB76B25C3639EFFB9BD923AE8 ();
// 0x00000820 System.Boolean Proyecto26.RequestHelper::get_EnableDebug()
extern void RequestHelper_get_EnableDebug_mB32C7653514C62DF73CCB037B8FDA79246B1DBDE ();
// 0x00000821 System.Void Proyecto26.RequestHelper::set_EnableDebug(System.Boolean)
extern void RequestHelper_set_EnableDebug_m806F36DEF5921A1AFE2A33BDD0A7D222CA0F5B13 ();
// 0x00000822 System.Nullable`1<System.Boolean> Proyecto26.RequestHelper::get_ChunkedTransfer()
extern void RequestHelper_get_ChunkedTransfer_m622B618F6755E8A9D4CE9BDDC2150AA2B9F6FBE2 ();
// 0x00000823 System.Void Proyecto26.RequestHelper::set_ChunkedTransfer(System.Nullable`1<System.Boolean>)
extern void RequestHelper_set_ChunkedTransfer_mBCCD30B3D6DC5547F0DCA5A22059D01B73BF1DD3 ();
// 0x00000824 System.Nullable`1<System.Boolean> Proyecto26.RequestHelper::get_UseHttpContinue()
extern void RequestHelper_get_UseHttpContinue_m7645253CE5AC91E02D6C3D812621A8A3DD1A1289 ();
// 0x00000825 System.Void Proyecto26.RequestHelper::set_UseHttpContinue(System.Nullable`1<System.Boolean>)
extern void RequestHelper_set_UseHttpContinue_m87D10B37D75B5A2A5AE462E217D9CA1675E02B0D ();
// 0x00000826 System.Nullable`1<System.Int32> Proyecto26.RequestHelper::get_RedirectLimit()
extern void RequestHelper_get_RedirectLimit_m488E83B4F9D57B43FE5490BF244F3ACD9E604480 ();
// 0x00000827 System.Void Proyecto26.RequestHelper::set_RedirectLimit(System.Nullable`1<System.Int32>)
extern void RequestHelper_set_RedirectLimit_mDE3645EC1C530E6F37EE6464A29F2D98DC6286F2 ();
// 0x00000828 System.Boolean Proyecto26.RequestHelper::get_IgnoreHttpException()
extern void RequestHelper_get_IgnoreHttpException_m1482402406DA715D76A3B25CBE750F5F202915A5 ();
// 0x00000829 System.Void Proyecto26.RequestHelper::set_IgnoreHttpException(System.Boolean)
extern void RequestHelper_set_IgnoreHttpException_m92B08A788BB48F93F0D4392D4F62D4346889EE4E ();
// 0x0000082A UnityEngine.WWWForm Proyecto26.RequestHelper::get_FormData()
extern void RequestHelper_get_FormData_m0207D1A53E755E7031AC4C4B5B8D511071F4F478 ();
// 0x0000082B System.Void Proyecto26.RequestHelper::set_FormData(UnityEngine.WWWForm)
extern void RequestHelper_set_FormData_m2A30257174CF3BF3CCD3B1CAC82FBA1084D38AC1 ();
// 0x0000082C System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RequestHelper::get_SimpleForm()
extern void RequestHelper_get_SimpleForm_m7238AB9746C2CC01E8983C21BC186BB7F5A4774E ();
// 0x0000082D System.Void Proyecto26.RequestHelper::set_SimpleForm(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RequestHelper_set_SimpleForm_m212A29BA698C9AB66D4D960A704F73D9EBAEE8D6 ();
// 0x0000082E System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection> Proyecto26.RequestHelper::get_FormSections()
extern void RequestHelper_get_FormSections_mA3311E750CEE3519DFB762FB959EFB316F12714B ();
// 0x0000082F System.Void Proyecto26.RequestHelper::set_FormSections(System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection>)
extern void RequestHelper_set_FormSections_mDBBF28D05F7B52B8AD569F7CDFAD521012E3151F ();
// 0x00000830 UnityEngine.Networking.CertificateHandler Proyecto26.RequestHelper::get_CertificateHandler()
extern void RequestHelper_get_CertificateHandler_m7D5D164ACD676DC4B29297D75A4EC3E191F9A5E5 ();
// 0x00000831 System.Void Proyecto26.RequestHelper::set_CertificateHandler(UnityEngine.Networking.CertificateHandler)
extern void RequestHelper_set_CertificateHandler_m384C483B84E223958C99282C06F6E27AD9EEC9A8 ();
// 0x00000832 UnityEngine.Networking.UploadHandler Proyecto26.RequestHelper::get_UploadHandler()
extern void RequestHelper_get_UploadHandler_mC39A192F8F8205937A2E3429A3585B23C6179815 ();
// 0x00000833 System.Void Proyecto26.RequestHelper::set_UploadHandler(UnityEngine.Networking.UploadHandler)
extern void RequestHelper_set_UploadHandler_mB7B0E07D289B2A7B2D2BDF0F541915906640CE91 ();
// 0x00000834 UnityEngine.Networking.DownloadHandler Proyecto26.RequestHelper::get_DownloadHandler()
extern void RequestHelper_get_DownloadHandler_m16A51AA814F360D0B86C9AD5AB1487931049603B ();
// 0x00000835 System.Void Proyecto26.RequestHelper::set_DownloadHandler(UnityEngine.Networking.DownloadHandler)
extern void RequestHelper_set_DownloadHandler_m8A008CEFD7D4CBA555D1248AD1A5DB75A58EA5E6 ();
// 0x00000836 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RequestHelper::get_Headers()
extern void RequestHelper_get_Headers_m000261B804D7400A61C788013FC41EA0CFEFB8CA ();
// 0x00000837 System.Void Proyecto26.RequestHelper::set_Headers(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RequestHelper_set_Headers_m28FA528C86977AADCBAB150BB0E4C5097CAB3A08 ();
// 0x00000838 UnityEngine.Networking.UnityWebRequest Proyecto26.RequestHelper::get_Request()
extern void RequestHelper_get_Request_mC869E47DC8EEEFF9894D23786D2BF266FAC6E254 ();
// 0x00000839 System.Void Proyecto26.RequestHelper::set_Request(UnityEngine.Networking.UnityWebRequest)
extern void RequestHelper_set_Request_m85340DEFF640624B8BE3C17264140C7A5E3A1847 ();
// 0x0000083A System.Single Proyecto26.RequestHelper::get_UploadProgress()
extern void RequestHelper_get_UploadProgress_m58689E2655FC874DD9F425F8A867941FCB57D954 ();
// 0x0000083B System.UInt64 Proyecto26.RequestHelper::get_UploadedBytes()
extern void RequestHelper_get_UploadedBytes_m8025EF075FE7C9EDC130533C24202577F7C6E208 ();
// 0x0000083C System.Single Proyecto26.RequestHelper::get_DownloadProgress()
extern void RequestHelper_get_DownloadProgress_mF13E879706FDABA0E0D1877BD23FB23BDCD8C144 ();
// 0x0000083D System.UInt64 Proyecto26.RequestHelper::get_DownloadedBytes()
extern void RequestHelper_get_DownloadedBytes_mD168B4F5340F315E0CBD35649A8E7FF4F9B6672D ();
// 0x0000083E System.String Proyecto26.RequestHelper::GetHeader(System.String)
extern void RequestHelper_GetHeader_mFA543B7D28A98472859DC8AB7F78DDE6C63C2703 ();
// 0x0000083F System.Boolean Proyecto26.RequestHelper::get_IsAborted()
extern void RequestHelper_get_IsAborted_m4270E2F4B4A1A53CA564C80C107BE4FA1EF5F381 ();
// 0x00000840 System.Void Proyecto26.RequestHelper::set_IsAborted(System.Boolean)
extern void RequestHelper_set_IsAborted_m7E6EFCC0F4F6D8E0ECBC2645F38EF5E2F3965C37 ();
// 0x00000841 System.Boolean Proyecto26.RequestHelper::get_DefaultContentType()
extern void RequestHelper_get_DefaultContentType_m11ACAF07CCCC9C0F8E9A3DE25BA5AA2A63633190 ();
// 0x00000842 System.Void Proyecto26.RequestHelper::set_DefaultContentType(System.Boolean)
extern void RequestHelper_set_DefaultContentType_mAB4BE4E5E5777C5E8DC1EF0A09A99EC22499E8EF ();
// 0x00000843 System.Void Proyecto26.RequestHelper::Abort()
extern void RequestHelper_Abort_m272B799532ECD8A4C921504A053B16103F1B0735 ();
// 0x00000844 System.Void Proyecto26.RequestHelper::.ctor()
extern void RequestHelper__ctor_m7D4C8C4EB6C100C23A9F5A494B9FF5B7D287FAB9 ();
// 0x00000845 UnityEngine.Networking.UnityWebRequest Proyecto26.ResponseHelper::get_Request()
extern void ResponseHelper_get_Request_m7D2000B276199E7012E28DF03E784C7862CFA55B ();
// 0x00000846 System.Void Proyecto26.ResponseHelper::set_Request(UnityEngine.Networking.UnityWebRequest)
extern void ResponseHelper_set_Request_mB82C191ED94F1BCE8E5C2D65CC9F1045676362A8 ();
// 0x00000847 System.Void Proyecto26.ResponseHelper::.ctor(UnityEngine.Networking.UnityWebRequest)
extern void ResponseHelper__ctor_m2045EE543D8AAABC9480B8B39A64BC1946F635EA ();
// 0x00000848 System.Int64 Proyecto26.ResponseHelper::get_StatusCode()
extern void ResponseHelper_get_StatusCode_m130E1F760BE225F2CC4FEF905D601EFD60CB75E7 ();
// 0x00000849 System.Byte[] Proyecto26.ResponseHelper::get_Data()
extern void ResponseHelper_get_Data_m68A42948C0B93963C8344E7B978066347D38196E ();
// 0x0000084A System.String Proyecto26.ResponseHelper::get_Text()
extern void ResponseHelper_get_Text_mA285A72FBA91A37ED55E48088DFE8E53EA3B733D ();
// 0x0000084B System.String Proyecto26.ResponseHelper::get_Error()
extern void ResponseHelper_get_Error_mA0B2D9B4B18E045BAB68FCC26FF28ADF8BA69960 ();
// 0x0000084C System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.ResponseHelper::get_Headers()
extern void ResponseHelper_get_Headers_m6E5EF8D44E4EBBF6D1E378B9812C79FCE8D64503 ();
// 0x0000084D System.String Proyecto26.ResponseHelper::ToString()
extern void ResponseHelper_ToString_mCC1393BA950333F550C38F397F49E08A32967DFA ();
// 0x0000084E Proyecto26.StaticCoroutine_CoroutineHolder Proyecto26.StaticCoroutine::get_runner()
extern void StaticCoroutine_get_runner_mCDEDC96828F412E908516930AA0A07628AD45941 ();
// 0x0000084F UnityEngine.Coroutine Proyecto26.StaticCoroutine::StartCoroutine(System.Collections.IEnumerator)
extern void StaticCoroutine_StartCoroutine_mFAC8FFD7E197092660CF9CD8C31064F96B0CF0C8 ();
// 0x00000850 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RestClient::get_DefaultRequestHeaders()
extern void RestClient_get_DefaultRequestHeaders_m2F0D8796DC7CA980429E020C9AA4EA6255C7E387 ();
// 0x00000851 System.Void Proyecto26.RestClient::set_DefaultRequestHeaders(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RestClient_set_DefaultRequestHeaders_mE3F99AE2A63B087395ED82D30E2BBDC986376B0C ();
// 0x00000852 System.Void Proyecto26.RestClient::CleanDefaultHeaders()
extern void RestClient_CleanDefaultHeaders_m7AC65C774FD9AA128697A8EDA24D6588A472CC82 ();
// 0x00000853 System.Void Proyecto26.RestClient::Request(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Request_m72295846A2779920152B206CE3B3CC94D124B154 ();
// 0x00000854 System.Void Proyecto26.RestClient::Request(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000855 System.Void Proyecto26.RestClient::Get(System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Get_mDD2DAC2B3E217CCFF9ED88DAA9DE1FB6A5A755C2 ();
// 0x00000856 System.Void Proyecto26.RestClient::Get(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Get_m704A16C01784F9E871CA46DFC0524D49BE0D799A ();
// 0x00000857 System.Void Proyecto26.RestClient::Get(System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000858 System.Void Proyecto26.RestClient::Get(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000859 System.Void Proyecto26.RestClient::GetArray(System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x0000085A System.Void Proyecto26.RestClient::GetArray(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x0000085B System.Void Proyecto26.RestClient::Post(System.String,System.Object,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Post_m61D63B57FBF0B4553D01AFE74CC20EEE83B9B66C ();
// 0x0000085C System.Void Proyecto26.RestClient::Post(System.String,System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Post_mA52CAE06D96B1BD156639CF4A5CEF34E8047EBC9 ();
// 0x0000085D System.Void Proyecto26.RestClient::Post(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Post_m26AF6D3E2CE5DBCB3F1E96B0F72390212038BD82 ();
// 0x0000085E System.Void Proyecto26.RestClient::Post(System.String,System.Object,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x0000085F System.Void Proyecto26.RestClient::Post(System.String,System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000860 System.Void Proyecto26.RestClient::Post(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000861 System.Void Proyecto26.RestClient::Put(System.String,System.Object,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Put_m39AC49D20CFB40AFD2539092D36836F7EE59B539 ();
// 0x00000862 System.Void Proyecto26.RestClient::Put(System.String,System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Put_m49CA1B70D92D456B5B49453774638D2FA6BD593B ();
// 0x00000863 System.Void Proyecto26.RestClient::Put(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Put_mA25CA5AAD568FBE40F0A502A7ECE03B5EDE78861 ();
// 0x00000864 System.Void Proyecto26.RestClient::Put(System.String,System.Object,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000865 System.Void Proyecto26.RestClient::Put(System.String,System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000866 System.Void Proyecto26.RestClient::Put(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x00000867 System.Void Proyecto26.RestClient::Delete(System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Delete_m283D55CA7A7929275D1EAC7C030CD2B14620CEFE ();
// 0x00000868 System.Void Proyecto26.RestClient::Delete(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Delete_mD10497E9FC59164F3B0CFEBFBFB6B18D181CB869 ();
// 0x00000869 System.Void Proyecto26.RestClient::Head(System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Head_m95150CFFFFC7CCF920E514E6F36619A9F2F31084 ();
// 0x0000086A System.Void Proyecto26.RestClient::Head(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Head_m9D3F8FCF9EEA02E9FF5EEB33800D8CB3B5607FF6 ();
// 0x0000086B RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Request(Proyecto26.RequestHelper)
extern void RestClient_Request_m0BA554BC9023E4726CE1777F868B0FD55F623206 ();
// 0x0000086C RSG.IPromise`1<T> Proyecto26.RestClient::Request(Proyecto26.RequestHelper)
// 0x0000086D RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Get(System.String)
extern void RestClient_Get_mB7C87B45B0AC6B96336314E2C6F9BF16446565A7 ();
// 0x0000086E RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Get(Proyecto26.RequestHelper)
extern void RestClient_Get_m0D9E2C1EB049974F78BE7BA8F92063BC842AAB88 ();
// 0x0000086F RSG.IPromise`1<T> Proyecto26.RestClient::Get(System.String)
// 0x00000870 RSG.IPromise`1<T> Proyecto26.RestClient::Get(Proyecto26.RequestHelper)
// 0x00000871 RSG.IPromise`1<T[]> Proyecto26.RestClient::GetArray(System.String)
// 0x00000872 RSG.IPromise`1<T[]> Proyecto26.RestClient::GetArray(Proyecto26.RequestHelper)
// 0x00000873 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Post(System.String,System.Object)
extern void RestClient_Post_mFA0B3D897E0659CD912BDD688EE11EBDF486EBA6 ();
// 0x00000874 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Post(System.String,System.String)
extern void RestClient_Post_mB2294872D87C70BE710630EAF8ACB38D97EBC045 ();
// 0x00000875 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Post(Proyecto26.RequestHelper)
extern void RestClient_Post_mC632B22BFA78D0B3C346CC608EAC730173AB69B3 ();
// 0x00000876 RSG.IPromise`1<T> Proyecto26.RestClient::Post(System.String,System.Object)
// 0x00000877 RSG.IPromise`1<T> Proyecto26.RestClient::Post(System.String,System.String)
// 0x00000878 RSG.IPromise`1<T> Proyecto26.RestClient::Post(Proyecto26.RequestHelper)
// 0x00000879 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Put(System.String,System.Object)
extern void RestClient_Put_mDA697668DBD06112E4E5B05C4741DD46C76387A4 ();
// 0x0000087A RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Put(System.String,System.String)
extern void RestClient_Put_m0AFED5E547B0CD8E5093BF2A31FD015C7C63B890 ();
// 0x0000087B RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Put(Proyecto26.RequestHelper)
extern void RestClient_Put_mF3DFA649A055F6C9B3ADB56226F63089F279DACE ();
// 0x0000087C RSG.IPromise`1<T> Proyecto26.RestClient::Put(System.String,System.Object)
// 0x0000087D RSG.IPromise`1<T> Proyecto26.RestClient::Put(System.String,System.String)
// 0x0000087E RSG.IPromise`1<T> Proyecto26.RestClient::Put(Proyecto26.RequestHelper)
// 0x0000087F RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Delete(System.String)
extern void RestClient_Delete_m0D116A07266EE3536822E2635109915650AC471D ();
// 0x00000880 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Delete(Proyecto26.RequestHelper)
extern void RestClient_Delete_m6AA933C3F377A10BBB0E7E7A848B2A53C7AEFD8D ();
// 0x00000881 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Head(System.String)
extern void RestClient_Head_mAAFC13D98D744BB34E2E062C1D2F0254B7F12C07 ();
// 0x00000882 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Head(Proyecto26.RequestHelper)
extern void RestClient_Head_m559D8243A556974E9D847DB3E3128F051A666564 ();
// 0x00000883 System.Void Proyecto26.RestClient::Promisify(RSG.Promise`1<T>,Proyecto26.RequestException,T)
// 0x00000884 System.Void Proyecto26.RestClient::Promisify(RSG.Promise`1<T>,Proyecto26.RequestException,Proyecto26.ResponseHelper,T)
// 0x00000885 System.String Proyecto26.Common.Common::GetFormSectionsContentType(System.Byte[]&,Proyecto26.RequestHelper)
extern void Common_GetFormSectionsContentType_mE6E8DB71813E34A714D9EB6DF09F82CA9923566E ();
// 0x00000886 System.Void Proyecto26.Common.Common::ConfigureWebRequestWithOptions(UnityEngine.Networking.UnityWebRequest,System.Byte[],System.String,Proyecto26.RequestHelper)
extern void Common_ConfigureWebRequestWithOptions_m0B52F2B99763C3F647A5905EB223C4D2C95108E9 ();
// 0x00000887 System.Collections.IEnumerator Proyecto26.Common.Common::SendWebRequestWithOptions(UnityEngine.Networking.UnityWebRequest,Proyecto26.RequestHelper)
extern void Common_SendWebRequestWithOptions_m3B484321BD8529D5C5663E99619D00F542AE0D74 ();
// 0x00000888 Proyecto26.ResponseHelper Proyecto26.Common.Extensions::CreateWebResponse(UnityEngine.Networking.UnityWebRequest)
extern void Extensions_CreateWebResponse_mCBE6A32B2E275A121212E0BC04B33455D3252BF9 ();
// 0x00000889 System.Boolean Proyecto26.Common.Extensions::IsValidRequest(UnityEngine.Networking.UnityWebRequest,Proyecto26.RequestHelper)
extern void Extensions_IsValidRequest_mF5A8D8632EF8A666305D1CE76DA0951D86ADA759 ();
// 0x0000088A System.String Models.Photo::ToString()
extern void Photo_ToString_mF42702D13A2A3D85EB60023A1B9E8588290B8C56 ();
// 0x0000088B System.Void Models.Photo::.ctor()
extern void Photo__ctor_mDEE2043FEDE45230489EC99DB57D76B42F97BDA1 ();
// 0x0000088C System.String Models.Post::ToString()
extern void Post_ToString_mA7FAD7DD3CE9A69561D6314A19190F103DB0B3ED ();
// 0x0000088D System.Void Models.Post::.ctor()
extern void Post__ctor_m94FB3E59A55B159FA9B3C5E3A6592813AA0660A2 ();
// 0x0000088E System.String Models.Todo::ToString()
extern void Todo_ToString_m9AD1D2F35055C98E42FC04A6696AB0C2455B1972 ();
// 0x0000088F System.Void Models.Todo::.ctor()
extern void Todo__ctor_m2D95DAA38846943DC4F5D7D5B6954C9055487C49 ();
// 0x00000890 System.String Models.User::ToString()
extern void User_ToString_m1B4F9151670216FFDFC95734956CF9B33BA6093D ();
// 0x00000891 System.Void Models.User::.ctor()
extern void User__ctor_mD17E5AF774F035FE0A0F26BE4C757AF27668760C ();
// 0x00000892 System.Void DentedPixel.LeanDummy::.ctor()
extern void LeanDummy__ctor_mAD9437EB3765999702C790EAC2DE9FEB4442E092 ();
// 0x00000893 System.Void DentedPixel.LTExamples.PathBezier::OnEnable()
extern void PathBezier_OnEnable_m56E18FAFF5DFB0ED61856F90EAFEF6BDD116E2DD ();
// 0x00000894 System.Void DentedPixel.LTExamples.PathBezier::Start()
extern void PathBezier_Start_mDAF11682189F6675D4FDD93D652A898A3347B50F ();
// 0x00000895 System.Void DentedPixel.LTExamples.PathBezier::Update()
extern void PathBezier_Update_m3CC2AC4CACBB72CE7640DC38DFC4F19689EAB564 ();
// 0x00000896 System.Void DentedPixel.LTExamples.PathBezier::OnDrawGizmos()
extern void PathBezier_OnDrawGizmos_m345FFC89DBF05FCF81AA56654D2764EB6199F256 ();
// 0x00000897 System.Void DentedPixel.LTExamples.PathBezier::.ctor()
extern void PathBezier__ctor_mB9E6AE4BEEE953C926C93944334E5F099FC262CA ();
// 0x00000898 System.Void DentedPixel.LTExamples.TestingUnitTests::Awake()
extern void TestingUnitTests_Awake_m3084D275E0537EAC8B54FE41937CF217BB332504 ();
// 0x00000899 System.Void DentedPixel.LTExamples.TestingUnitTests::Start()
extern void TestingUnitTests_Start_mE8442EF073840E58B91FD7047EC0D8708D70D928 ();
// 0x0000089A UnityEngine.GameObject DentedPixel.LTExamples.TestingUnitTests::cubeNamed(System.String)
extern void TestingUnitTests_cubeNamed_m46762B53A8B1B8EC91E95B1B886E0BEDA7F59270 ();
// 0x0000089B System.Collections.IEnumerator DentedPixel.LTExamples.TestingUnitTests::timeBasedTesting()
extern void TestingUnitTests_timeBasedTesting_m5510CA65F550F87AAD701A457ADAF778B46B5758 ();
// 0x0000089C System.Collections.IEnumerator DentedPixel.LTExamples.TestingUnitTests::lotsOfCancels()
extern void TestingUnitTests_lotsOfCancels_mF5CDB314DC950F82FF777A507914DAA972E57D08 ();
// 0x0000089D System.Collections.IEnumerator DentedPixel.LTExamples.TestingUnitTests::pauseTimeNow()
extern void TestingUnitTests_pauseTimeNow_m038EABEB7FE1D5E01BAD36F26EDAD62253864249 ();
// 0x0000089E System.Void DentedPixel.LTExamples.TestingUnitTests::rotateRepeatFinished()
extern void TestingUnitTests_rotateRepeatFinished_m75177B46DFB956A9AC0870AC6E1E767A09E41A65 ();
// 0x0000089F System.Void DentedPixel.LTExamples.TestingUnitTests::rotateRepeatAllFinished()
extern void TestingUnitTests_rotateRepeatAllFinished_mE2C3707783D21F0E24E390907EC41AAD46537D53 ();
// 0x000008A0 System.Void DentedPixel.LTExamples.TestingUnitTests::eventGameObjectCalled(LTEvent)
extern void TestingUnitTests_eventGameObjectCalled_m696EE506C9313CDC2955B76C394C7D8DBAFA10D6 ();
// 0x000008A1 System.Void DentedPixel.LTExamples.TestingUnitTests::eventGeneralCalled(LTEvent)
extern void TestingUnitTests_eventGeneralCalled_m2ACEC83AC54AF7D79C1BFB26F9D63F2201886540 ();
// 0x000008A2 System.Void DentedPixel.LTExamples.TestingUnitTests::.ctor()
extern void TestingUnitTests__ctor_m1551B3D3BCD78DA068595A4F9CA1934061711DA2 ();
// 0x000008A3 System.Void DentedPixel.LTExamples.TestingUnitTests::<lotsOfCancels>b__25_0()
extern void TestingUnitTests_U3ClotsOfCancelsU3Eb__25_0_m22BEE01EA5BDFFF8A1DBF6DB397911D91B74FABA ();
// 0x000008A4 System.Void DentedPixel.LTExamples.TestingUnitTests::<pauseTimeNow>b__26_1()
extern void TestingUnitTests_U3CpauseTimeNowU3Eb__26_1_m4F3F229D6D22317DC30FF68B332B22A3CB6CFE68 ();
// 0x000008A5 System.Void Lean.Touch.LeanCameraMove::SnapToSelection()
extern void LeanCameraMove_SnapToSelection_mCF45F65928E94714CCC18F2B99A11C94ABCEAADD ();
// 0x000008A6 System.Void Lean.Touch.LeanCameraMove::LateUpdate()
extern void LeanCameraMove_LateUpdate_m8199A04A20DD363CEBF44F42AE447273255B17F2 ();
// 0x000008A7 System.Void Lean.Touch.LeanCameraMove::.ctor()
extern void LeanCameraMove__ctor_m9D8AB4604EADE8E50CE3189CE0F523456C478E52 ();
// 0x000008A8 System.Void Lean.Touch.LeanCameraZoom::ContinuouslyZoom(System.Single)
extern void LeanCameraZoom_ContinuouslyZoom_mFCDB903A5873AF92EFB4EEEBA092D390ADF00DC3 ();
// 0x000008A9 System.Void Lean.Touch.LeanCameraZoom::Start()
extern void LeanCameraZoom_Start_m1D63AD2606F070894C08C5A5B93C57263620B9B1 ();
// 0x000008AA System.Void Lean.Touch.LeanCameraZoom::LateUpdate()
extern void LeanCameraZoom_LateUpdate_m9A35BCE30EA1C6668BFD25605C378762E41E5268 ();
// 0x000008AB System.Void Lean.Touch.LeanCameraZoom::Translate(System.Single,UnityEngine.Vector2)
extern void LeanCameraZoom_Translate_m503BC4885B7298971844A7C93ED6CEC2EA752773 ();
// 0x000008AC System.Void Lean.Touch.LeanCameraZoom::SetZoom(System.Single)
extern void LeanCameraZoom_SetZoom_m4D4A8D5C5518F93B51007EC5F6312A2C1695F11B ();
// 0x000008AD System.Void Lean.Touch.LeanCameraZoom::.ctor()
extern void LeanCameraZoom__ctor_m218380820F05FE77DF33F3CBA47808275641BAAF ();
// 0x000008AE System.Void Lean.Touch.LeanCanvasArrow::RotateToDelta(UnityEngine.Vector2)
extern void LeanCanvasArrow_RotateToDelta_mA75E4316B72DA3E69CDA2D18226788AF2F3242AF ();
// 0x000008AF System.Void Lean.Touch.LeanCanvasArrow::Update()
extern void LeanCanvasArrow_Update_m6E5E83648EB353C309E72A2BCD1884767210F056 ();
// 0x000008B0 System.Void Lean.Touch.LeanCanvasArrow::.ctor()
extern void LeanCanvasArrow__ctor_m86D6586CFE6221C16D3AE58A5374D3E27145F877 ();
// 0x000008B1 System.Void Lean.Touch.LeanDestroy::Update()
extern void LeanDestroy_Update_m96BE8B43BD546EB19CCDBE8DF833945E42E49DB8 ();
// 0x000008B2 System.Void Lean.Touch.LeanDestroy::DestroyNow()
extern void LeanDestroy_DestroyNow_m05F2B50717B2FFB345324A847F44E7585AF13DDD ();
// 0x000008B3 System.Void Lean.Touch.LeanDestroy::.ctor()
extern void LeanDestroy__ctor_mAF42A9F0629FE42B0C0B2890E3368419ADC876B2 ();
// 0x000008B4 T Lean.Touch.LeanFingerData::Find(System.Collections.Generic.List`1<T>&,Lean.Touch.LeanFinger)
// 0x000008B5 T Lean.Touch.LeanFingerData::FindOrCreate(System.Collections.Generic.List`1<T>&,Lean.Touch.LeanFinger)
// 0x000008B6 System.Void Lean.Touch.LeanFingerData::.ctor()
extern void LeanFingerData__ctor_mD98951F55492E2F926528BCB4FA0C4AC4A616D4F ();
// 0x000008B7 Lean.Touch.LeanFingerDown_LeanFingerEvent Lean.Touch.LeanFingerDown::get_OnDown()
extern void LeanFingerDown_get_OnDown_m01F417F5AD2ED8DF51C64410A8F4E9469C77FC2A ();
// 0x000008B8 System.Void Lean.Touch.LeanFingerDown::Start()
extern void LeanFingerDown_Start_m79C6D331F0908B3C444D05687B9F9039681E6596 ();
// 0x000008B9 System.Void Lean.Touch.LeanFingerDown::OnEnable()
extern void LeanFingerDown_OnEnable_m7534989DC473AE33B0D633EB0D8F813EB06871CC ();
// 0x000008BA System.Void Lean.Touch.LeanFingerDown::OnDisable()
extern void LeanFingerDown_OnDisable_m2C8843299E5D451A0453F2DA3CA4B98BAC849E5D ();
// 0x000008BB System.Void Lean.Touch.LeanFingerDown::FingerDown(Lean.Touch.LeanFinger)
extern void LeanFingerDown_FingerDown_m9A872EE6C7A89E5B2E361BD7668BF390C7CDB809 ();
// 0x000008BC System.Void Lean.Touch.LeanFingerDown::.ctor()
extern void LeanFingerDown__ctor_m71B424F8281F46219ABA7D3DD9684D6BC375074F ();
// 0x000008BD Lean.Touch.LeanFingerHeld_LeanFingerEvent Lean.Touch.LeanFingerHeld::get_OnHeldDown()
extern void LeanFingerHeld_get_OnHeldDown_mDCBD079B5AD9F887A793638962503E4C930E6ACB ();
// 0x000008BE Lean.Touch.LeanFingerHeld_LeanFingerEvent Lean.Touch.LeanFingerHeld::get_OnHeldSet()
extern void LeanFingerHeld_get_OnHeldSet_mF422114A9A1F28F8566C4BD76428D9965D755FF5 ();
// 0x000008BF Lean.Touch.LeanFingerHeld_LeanFingerEvent Lean.Touch.LeanFingerHeld::get_OnSelect()
extern void LeanFingerHeld_get_OnSelect_m864311026D38721DB0AC470CCA486C756DB0576E ();
// 0x000008C0 System.Void Lean.Touch.LeanFingerHeld::Start()
extern void LeanFingerHeld_Start_m2A5D07E7E0C66AB5CA95BBA038E3226667F4E292 ();
// 0x000008C1 System.Void Lean.Touch.LeanFingerHeld::OnEnable()
extern void LeanFingerHeld_OnEnable_mEF1F4AA2EDFF4CB1AE72D06B4799F89B8A91DC5A ();
// 0x000008C2 System.Void Lean.Touch.LeanFingerHeld::OnDisable()
extern void LeanFingerHeld_OnDisable_m8DBB4B94F213878ED999DE062A9E241F8C84B8E7 ();
// 0x000008C3 System.Void Lean.Touch.LeanFingerHeld::OnFingerDown(Lean.Touch.LeanFinger)
extern void LeanFingerHeld_OnFingerDown_mC9774E22CEA56124942EABBEDC9D7DB12B64362B ();
// 0x000008C4 System.Void Lean.Touch.LeanFingerHeld::OnFingerSet(Lean.Touch.LeanFinger)
extern void LeanFingerHeld_OnFingerSet_m0EEA11DC109D56A57EDBE8036A6D0405CDDB64D7 ();
// 0x000008C5 System.Void Lean.Touch.LeanFingerHeld::OnFingerUp(Lean.Touch.LeanFinger)
extern void LeanFingerHeld_OnFingerUp_m02CF2279B2AFF7227D774D69CD806BA647C205F6 ();
// 0x000008C6 Lean.Touch.LeanFingerHeld_Link Lean.Touch.LeanFingerHeld::FindLink(Lean.Touch.LeanFinger,System.Boolean)
extern void LeanFingerHeld_FindLink_m86986ED6CBAF01EC93D4ACB36C8EB465DDA631EF ();
// 0x000008C7 System.Void Lean.Touch.LeanFingerHeld::.ctor()
extern void LeanFingerHeld__ctor_m371FAB44F0431B9A5682CE7556375825A513497A ();
// 0x000008C8 Lean.Touch.LeanFingerLine_Vector3Vector3Event Lean.Touch.LeanFingerLine::get_OnReleasedFromTo()
extern void LeanFingerLine_get_OnReleasedFromTo_m5534DEE35985E3E7D16989063071696B2276675D ();
// 0x000008C9 Lean.Touch.LeanFingerLine_Vector3Event Lean.Touch.LeanFingerLine::get_OnReleasedTo()
extern void LeanFingerLine_get_OnReleasedTo_m94D7EA4A1748ACCCD93E4FBDD126CD27229D809D ();
// 0x000008CA System.Void Lean.Touch.LeanFingerLine::LinkFingerUp(Lean.Touch.LeanFingerTrail_FingerData)
extern void LeanFingerLine_LinkFingerUp_m15040ACA7A6EA5618BE4165AC34F0AABFF1A471E ();
// 0x000008CB System.Void Lean.Touch.LeanFingerLine::WritePositions(UnityEngine.LineRenderer,Lean.Touch.LeanFinger)
extern void LeanFingerLine_WritePositions_m7E3BCC076B9366D51DE2B8C5F2B69B3AF3D39EF2 ();
// 0x000008CC UnityEngine.Vector3 Lean.Touch.LeanFingerLine::GetStartPoint(Lean.Touch.LeanFinger)
extern void LeanFingerLine_GetStartPoint_m6F9BA6663B2AB003DDC6128E81ABDD2D4B9635C5 ();
// 0x000008CD UnityEngine.Vector3 Lean.Touch.LeanFingerLine::GetEndPoint(Lean.Touch.LeanFinger,UnityEngine.Vector3)
extern void LeanFingerLine_GetEndPoint_mC2BA5AAA44BF50468839BA5D48CCA55E14E5E321 ();
// 0x000008CE System.Void Lean.Touch.LeanFingerLine::.ctor()
extern void LeanFingerLine__ctor_m90A3A891A197E42FC707797E53584EBCA9C43AB6 ();
// 0x000008CF Lean.Touch.LeanFingerSet_LeanFingerEvent Lean.Touch.LeanFingerSet::get_OnSet()
extern void LeanFingerSet_get_OnSet_mB08E734D9EF0666360880A2AD833189F42BBF5EA ();
// 0x000008D0 Lean.Touch.LeanFingerSet_Vector2Event Lean.Touch.LeanFingerSet::get_OnSetDelta()
extern void LeanFingerSet_get_OnSetDelta_mCC630467A7745C06FCDE46088F2EA3485E17CEEE ();
// 0x000008D1 System.Void Lean.Touch.LeanFingerSet::Start()
extern void LeanFingerSet_Start_mC5A683576EC2E7A6B797469265D481CE4455E9EA ();
// 0x000008D2 System.Void Lean.Touch.LeanFingerSet::OnEnable()
extern void LeanFingerSet_OnEnable_mEE8D62914B914A5C364735057E16672F91BEDCAF ();
// 0x000008D3 System.Void Lean.Touch.LeanFingerSet::OnDisable()
extern void LeanFingerSet_OnDisable_mF115EABA7CD0D95DB2FD232079B4368C9FDA7E9E ();
// 0x000008D4 System.Void Lean.Touch.LeanFingerSet::FingerSet(Lean.Touch.LeanFinger)
extern void LeanFingerSet_FingerSet_m9613C2BFAA7ADFEBF3117288C9A639497D6BDFF9 ();
// 0x000008D5 System.Void Lean.Touch.LeanFingerSet::.ctor()
extern void LeanFingerSet__ctor_m8E66E7826E3B8905B3A9BEA6676CAC4A5F6033FF ();
// 0x000008D6 Lean.Touch.LeanFingerSwipe_LeanFingerEvent Lean.Touch.LeanFingerSwipe::get_OnSwipe()
extern void LeanFingerSwipe_get_OnSwipe_m1CBD2D067DC31447D8FE3273D0A3CBA215D62864 ();
// 0x000008D7 Lean.Touch.LeanFingerSwipe_Vector2Event Lean.Touch.LeanFingerSwipe::get_OnSwipeDelta()
extern void LeanFingerSwipe_get_OnSwipeDelta_m5263E0F14C0AFBF56B27AC836CFAFB2D9A2B4E34 ();
// 0x000008D8 System.Boolean Lean.Touch.LeanFingerSwipe::CheckSwipe(Lean.Touch.LeanFinger,UnityEngine.Vector2)
extern void LeanFingerSwipe_CheckSwipe_m57B659175B45E5789F4AE4A18DC800A4BECCE185 ();
// 0x000008D9 System.Void Lean.Touch.LeanFingerSwipe::OnEnable()
extern void LeanFingerSwipe_OnEnable_m505603566376B408AA7117659729C15B262501E7 ();
// 0x000008DA System.Void Lean.Touch.LeanFingerSwipe::Start()
extern void LeanFingerSwipe_Start_mEB69138725B360D34EE6378A445000227C5EB606 ();
// 0x000008DB System.Void Lean.Touch.LeanFingerSwipe::FingerSwipe(Lean.Touch.LeanFinger)
extern void LeanFingerSwipe_FingerSwipe_m947DCA328E5B8695AF8D826574C7124EADBBE2D8 ();
// 0x000008DC System.Void Lean.Touch.LeanFingerSwipe::.ctor()
extern void LeanFingerSwipe__ctor_m6A48ED5E0A22AE4E4E7196A25670CCB747A6A512 ();
// 0x000008DD Lean.Touch.LeanFingerTap_LeanFingerEvent Lean.Touch.LeanFingerTap::get_OnTap()
extern void LeanFingerTap_get_OnTap_m7D397EDB21F2C2AF855702D02EFF4ECC620BE4A2 ();
// 0x000008DE System.Void Lean.Touch.LeanFingerTap::Start()
extern void LeanFingerTap_Start_m3A4EDF8069308719FEAB9AB1097CB81E49BC4DDB ();
// 0x000008DF System.Void Lean.Touch.LeanFingerTap::OnEnable()
extern void LeanFingerTap_OnEnable_m683BDD1CFFE951877D19858670DA30CFBD49BE6F ();
// 0x000008E0 System.Void Lean.Touch.LeanFingerTap::OnDisable()
extern void LeanFingerTap_OnDisable_m6F68FA245FDC28B8C650F37C647867E334A4C3AF ();
// 0x000008E1 System.Void Lean.Touch.LeanFingerTap::FingerTap(Lean.Touch.LeanFinger)
extern void LeanFingerTap_FingerTap_m9D40017293211FE06102396B8AF78F4FB80FDDE7 ();
// 0x000008E2 System.Void Lean.Touch.LeanFingerTap::.ctor()
extern void LeanFingerTap__ctor_mED989C7A98A2581454D6D5A9CCD506DD8C182195 ();
// 0x000008E3 System.Void Lean.Touch.LeanFingerTrail::Start()
extern void LeanFingerTrail_Start_mA1E93A0C76592EB205D7D497CDE2A7176310D2FF ();
// 0x000008E4 System.Void Lean.Touch.LeanFingerTrail::OnEnable()
extern void LeanFingerTrail_OnEnable_m4564F591C913E426F842E99119BAB012053B0BCA ();
// 0x000008E5 System.Void Lean.Touch.LeanFingerTrail::OnDisable()
extern void LeanFingerTrail_OnDisable_m0D041A6FCEFAC46478BD114166167E332669217E ();
// 0x000008E6 System.Void Lean.Touch.LeanFingerTrail::WritePositions(UnityEngine.LineRenderer,Lean.Touch.LeanFinger)
extern void LeanFingerTrail_WritePositions_m24F8F3789AED66AAF453BC1EE2C170B6C1C47C50 ();
// 0x000008E7 System.Void Lean.Touch.LeanFingerTrail::FingerSet(Lean.Touch.LeanFinger)
extern void LeanFingerTrail_FingerSet_m8E016D553A70E984F092254E3A14AE5621FA5E65 ();
// 0x000008E8 System.Void Lean.Touch.LeanFingerTrail::FingerUp(Lean.Touch.LeanFinger)
extern void LeanFingerTrail_FingerUp_m79F07D80D5E57681A5B82D93C8D8AE69790A0E8A ();
// 0x000008E9 System.Void Lean.Touch.LeanFingerTrail::LinkFingerUp(Lean.Touch.LeanFingerTrail_FingerData)
extern void LeanFingerTrail_LinkFingerUp_m508B6D2383E5E2AB1BD9F6EBE2703C60DCD19103 ();
// 0x000008EA System.Void Lean.Touch.LeanFingerTrail::.ctor()
extern void LeanFingerTrail__ctor_m858BECF14573DE145CE58137DDEFB7EC60D6179B ();
// 0x000008EB Lean.Touch.LeanFingerUp_LeanFingerEvent Lean.Touch.LeanFingerUp::get_OnUp()
extern void LeanFingerUp_get_OnUp_m72D51BFBD27576DD1BB89E6EEF8FD343F0D0B57C ();
// 0x000008EC System.Void Lean.Touch.LeanFingerUp::Start()
extern void LeanFingerUp_Start_m270CAA8585511FF787C38EE206F884B48293E11A ();
// 0x000008ED System.Void Lean.Touch.LeanFingerUp::OnEnable()
extern void LeanFingerUp_OnEnable_mD0B304DB1C2BDF052FB8BF3A11CD9E7AF7B217C1 ();
// 0x000008EE System.Void Lean.Touch.LeanFingerUp::OnDisable()
extern void LeanFingerUp_OnDisable_mAB9285F0A342A4C57F6576057127D47811A4725C ();
// 0x000008EF System.Void Lean.Touch.LeanFingerUp::FingerUp(Lean.Touch.LeanFinger)
extern void LeanFingerUp_FingerUp_m5BD8E7AF8F3C42A010B3EE0101F6F59FA760E89E ();
// 0x000008F0 System.Void Lean.Touch.LeanFingerUp::.ctor()
extern void LeanFingerUp__ctor_mA53E151EF5555BBEFB6306882E46E2ABC459C15B ();
// 0x000008F1 System.Void Lean.Touch.LeanOpenUrl::Open(System.String)
extern void LeanOpenUrl_Open_m41B624E57AA32174314ED36111FE95EC2D0E2D07 ();
// 0x000008F2 System.Void Lean.Touch.LeanOpenUrl::Update()
extern void LeanOpenUrl_Update_mA1F50D91BB67CDC4DCE1E556FEBA58F4BBB1718D ();
// 0x000008F3 System.Void Lean.Touch.LeanOpenUrl::.ctor()
extern void LeanOpenUrl__ctor_mF8D82EA66ED45F7AE0F46951139B338693F90512 ();
// 0x000008F4 System.Int32 Lean.Touch.LeanPath::get_PointCount()
extern void LeanPath_get_PointCount_m9045E672BD2FF353999596E6E261270513F746D2 ();
// 0x000008F5 System.Int32 Lean.Touch.LeanPath::GetPointCount(System.Int32)
extern void LeanPath_GetPointCount_m31A0FB489FA54C2630C7A8310CFE22143A57E8D9 ();
// 0x000008F6 UnityEngine.Vector3 Lean.Touch.LeanPath::GetSmoothedPoint(System.Single)
extern void LeanPath_GetSmoothedPoint_mC90B314DB35373DD33ABFA20E4674BDCA6B75486 ();
// 0x000008F7 UnityEngine.Vector3 Lean.Touch.LeanPath::GetPoint(System.Int32,System.Int32)
extern void LeanPath_GetPoint_m20E43752CA69EE2701E20265DD15C17478915F97 ();
// 0x000008F8 UnityEngine.Vector3 Lean.Touch.LeanPath::GetPointRaw(System.Int32,System.Int32)
extern void LeanPath_GetPointRaw_m2740E3BCBCC28875BFC13AADB9CD569F9A49357E ();
// 0x000008F9 System.Void Lean.Touch.LeanPath::SetLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanPath_SetLine_mCA222133037FBD6E37D167DE7468C283B3656716 ();
// 0x000008FA System.Boolean Lean.Touch.LeanPath::TryGetClosest(UnityEngine.Vector3,UnityEngine.Vector3&,System.Int32)
extern void LeanPath_TryGetClosest_m603575984CA3AF91D809EDDDCE7720BC3DDC2695 ();
// 0x000008FB System.Boolean Lean.Touch.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32)
extern void LeanPath_TryGetClosest_mCBAD5EF228A7EA7B179312CA0AA6384DECCE854A ();
// 0x000008FC UnityEngine.Vector3 Lean.Touch.LeanPath::GetClosestPoint(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanPath_GetClosestPoint_m1EA337BBAD860D0647F1AE0025125CB3484A9B56 ();
// 0x000008FD UnityEngine.Vector3 Lean.Touch.LeanPath::GetClosestPoint(UnityEngine.Ray,UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanPath_GetClosestPoint_mCFCCBDC61362E4092AE6C9DBF91608F8D12F7B5A ();
// 0x000008FE System.Single Lean.Touch.LeanPath::GetClosestDistance(UnityEngine.Ray,UnityEngine.Vector3)
extern void LeanPath_GetClosestDistance_mFFED298C41C661C1301C8F7A6A8EDE1B0B94F611 ();
// 0x000008FF System.Int32 Lean.Touch.LeanPath::Mod(System.Int32,System.Int32)
extern void LeanPath_Mod_mF6965EBB3AE58BBD2086ED85352B9BADDA9ED8E6 ();
// 0x00000900 System.Single Lean.Touch.LeanPath::CubicInterpolate(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanPath_CubicInterpolate_mAA20DA9E3C90657AF2AACCA1359BD6CC294CB5CD ();
// 0x00000901 System.Void Lean.Touch.LeanPath::.ctor()
extern void LeanPath__ctor_mCF6DCDFBB71DFF5C0EE11C9DEFBB7F64DA6ADD2F ();
// 0x00000902 System.Void Lean.Touch.LeanPath::.cctor()
extern void LeanPath__cctor_m000BACFE90EEBE48EED93CC532C21F00072513EF ();
// 0x00000903 UnityEngine.Vector3 Lean.Touch.LeanPlane::GetClosest(UnityEngine.Vector3,System.Single)
extern void LeanPlane_GetClosest_m7683693B13E0F8118E395660C6BF8B6A55F09BE1 ();
// 0x00000904 System.Boolean Lean.Touch.LeanPlane::TryRaycast(UnityEngine.Ray,UnityEngine.Vector3&,System.Single)
extern void LeanPlane_TryRaycast_mFE7C5228953B5803B2E2AC56AE723AE15C93F986 ();
// 0x00000905 UnityEngine.Vector3 Lean.Touch.LeanPlane::GetClosest(UnityEngine.Ray)
extern void LeanPlane_GetClosest_m7CC90569CD40DCAC36CB4E236DBD9D35134BBC69 ();
// 0x00000906 System.Boolean Lean.Touch.LeanPlane::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Ray,System.Single&)
extern void LeanPlane_Raycast_m6DC4A90A167D648C34B7C8B2A0FFA625F5E2771D ();
// 0x00000907 System.Void Lean.Touch.LeanPlane::.ctor()
extern void LeanPlane__ctor_m193015630E686140C5E7426BB91742AFD82FD881 ();
// 0x00000908 System.Void Lean.Touch.LeanRotate::Start()
extern void LeanRotate_Start_mDC240D0B3C7D54B1A891BF3A9A3C1C884AB13152 ();
// 0x00000909 System.Void Lean.Touch.LeanRotate::Update()
extern void LeanRotate_Update_m3DD0A6274590A5E2B94819E61D2E37F648FD38AB ();
// 0x0000090A System.Void Lean.Touch.LeanRotate::TranslateUI(System.Single,UnityEngine.Vector2)
extern void LeanRotate_TranslateUI_mC4004FF7236DCE86BA564B899706468AE850CE49 ();
// 0x0000090B System.Void Lean.Touch.LeanRotate::Translate(System.Single,UnityEngine.Vector2)
extern void LeanRotate_Translate_m4DFEE2D4E93F934F9436404BE570EA53C49977A5 ();
// 0x0000090C System.Void Lean.Touch.LeanRotate::RotateUI(System.Single)
extern void LeanRotate_RotateUI_mF97E20F6A11F4F424443B77BDE05D1A65BAAF891 ();
// 0x0000090D System.Void Lean.Touch.LeanRotate::Rotate(System.Single)
extern void LeanRotate_Rotate_m69CA06783A10450AB613081EA2289EAF0AF249F1 ();
// 0x0000090E System.Void Lean.Touch.LeanRotate::.ctor()
extern void LeanRotate__ctor_m9CBCD0467270F8099F3B9435242A19252F1212F3 ();
// 0x0000090F System.Void Lean.Touch.LeanRotateCustomAxis::Start()
extern void LeanRotateCustomAxis_Start_m6CB431470B1165A30548BFD7C0969862E822C52C ();
// 0x00000910 System.Void Lean.Touch.LeanRotateCustomAxis::Update()
extern void LeanRotateCustomAxis_Update_mBB7052DCA8F2ECCF03654C53C81DFD358F0E1816 ();
// 0x00000911 System.Void Lean.Touch.LeanRotateCustomAxis::.ctor()
extern void LeanRotateCustomAxis__ctor_m14D55B95AB5EF12807C40C6FBD0F2138913BFC33 ();
// 0x00000912 System.Void Lean.Touch.LeanScale::Start()
extern void LeanScale_Start_m09D3E4BE4BA85EC37B2056BD577354FECB69F742 ();
// 0x00000913 System.Void Lean.Touch.LeanScale::Update()
extern void LeanScale_Update_m7416C2BEAA82D5A9C9913F85D07ED6BCD586B11F ();
// 0x00000914 System.Void Lean.Touch.LeanScale::TranslateUI(System.Single,UnityEngine.Vector2)
extern void LeanScale_TranslateUI_mAB9C06AE8C10C9CEDE09119FEB054F857CA38E42 ();
// 0x00000915 System.Void Lean.Touch.LeanScale::Translate(System.Single,UnityEngine.Vector2)
extern void LeanScale_Translate_m85A244A9143F0C3CC19CBCB5CDCC162035F85E89 ();
// 0x00000916 System.Void Lean.Touch.LeanScale::Scale(UnityEngine.Vector3)
extern void LeanScale_Scale_mF1E6B056A9049302D6D31CA2410F3161C3206532 ();
// 0x00000917 System.Void Lean.Touch.LeanScale::.ctor()
extern void LeanScale__ctor_m0C60988D6FC39475549BD3BE8F7943D2540C6EA2 ();
// 0x00000918 UnityEngine.Vector3 Lean.Touch.LeanScreenDepth::Convert(UnityEngine.Vector2,UnityEngine.GameObject,UnityEngine.Transform)
extern void LeanScreenDepth_Convert_m17FA6C55BC020C621949587EE638A6119BFDF5BB_AdjustorThunk ();
// 0x00000919 UnityEngine.Vector3 Lean.Touch.LeanScreenDepth::ConvertDelta(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.GameObject,UnityEngine.Transform)
extern void LeanScreenDepth_ConvertDelta_m7B2487D484E8A5F5A72A1E105B804EF1C59D1517_AdjustorThunk ();
// 0x0000091A System.Boolean Lean.Touch.LeanScreenDepth::TryConvert(UnityEngine.Vector3&,UnityEngine.Vector2,UnityEngine.GameObject,UnityEngine.Transform)
extern void LeanScreenDepth_TryConvert_m2578623A359392A87C58035883C3D7E2933BF9E5_AdjustorThunk ();
// 0x0000091B System.Boolean Lean.Touch.LeanScreenDepth::IsChildOf(UnityEngine.Transform,UnityEngine.Transform)
extern void LeanScreenDepth_IsChildOf_m034EC0E242D644C8332165A4DD8E72F44533B229 ();
// 0x0000091C System.Void Lean.Touch.LeanScreenDepth::.cctor()
extern void LeanScreenDepth__cctor_mBA240A88458C9D0F80FF060BDDD4EB4A5861FC03 ();
// 0x0000091D System.Void Lean.Touch.LeanSelect::SelectStartScreenPosition(Lean.Touch.LeanFinger)
extern void LeanSelect_SelectStartScreenPosition_m1EF260E5A794918BB3F6CB0533FCB7E5EA6B251E ();
// 0x0000091E System.Void Lean.Touch.LeanSelect::SelectScreenPosition(Lean.Touch.LeanFinger)
extern void LeanSelect_SelectScreenPosition_mAAFDAE9F146DE1E568F753EC4D080BEB639C145F ();
// 0x0000091F System.Void Lean.Touch.LeanSelect::SelectScreenPosition(Lean.Touch.LeanFinger,UnityEngine.Vector2)
extern void LeanSelect_SelectScreenPosition_m9879B433CDA1B04E751186E1FAD3B338D781B311 ();
// 0x00000920 System.Void Lean.Touch.LeanSelect::TryGetComponent(Lean.Touch.LeanSelect_SelectType,UnityEngine.Vector2,UnityEngine.Component&)
extern void LeanSelect_TryGetComponent_mDC20B49902DEDE331C20CCD5764F460C975AB4D1 ();
// 0x00000921 System.Void Lean.Touch.LeanSelect::Select(Lean.Touch.LeanFinger,UnityEngine.Component)
extern void LeanSelect_Select_m60F8F5F4530958F5F66F16F15A1480A65BC8A143 ();
// 0x00000922 System.Void Lean.Touch.LeanSelect::Select(Lean.Touch.LeanFinger,Lean.Touch.LeanSelectable)
extern void LeanSelect_Select_mC1830C9BE70AB16D1E0723C0C41EF81308B54439 ();
// 0x00000923 System.Void Lean.Touch.LeanSelect::DeselectAll()
extern void LeanSelect_DeselectAll_m1B290CA1890634909F924CC8DC79131C0866DE9A ();
// 0x00000924 System.Void Lean.Touch.LeanSelect::OnEnable()
extern void LeanSelect_OnEnable_m167EA7A97777591DB6E9E96F891523E127122DBB ();
// 0x00000925 System.Void Lean.Touch.LeanSelect::OnDisable()
extern void LeanSelect_OnDisable_m248827E8DC9C4826013BFB3B054E381373C9AD2B ();
// 0x00000926 System.Void Lean.Touch.LeanSelect::.ctor()
extern void LeanSelect__ctor_m2805372C7A136AC720229354F6EFF9B5164F9EAB ();
// 0x00000927 System.Void Lean.Touch.LeanSelect::.cctor()
extern void LeanSelect__cctor_mF8DAF1D094796D8083F49ABEF1DA8887C5AD94CF ();
// 0x00000928 Lean.Touch.LeanSelectable_LeanFingerEvent Lean.Touch.LeanSelectable::get_OnSelect()
extern void LeanSelectable_get_OnSelect_m5C9024D82B653F01DE73B8B0A090CC3929D41F30 ();
// 0x00000929 Lean.Touch.LeanSelectable_LeanFingerEvent Lean.Touch.LeanSelectable::get_OnSelectSet()
extern void LeanSelectable_get_OnSelectSet_m27616098FCCF6DFFC7A9B0E0089D0CBD00D43261 ();
// 0x0000092A Lean.Touch.LeanSelectable_LeanFingerEvent Lean.Touch.LeanSelectable::get_OnSelectUp()
extern void LeanSelectable_get_OnSelectUp_m817AD57D6B429B409CC79190907F575E9432FFDB ();
// 0x0000092B UnityEngine.Events.UnityEvent Lean.Touch.LeanSelectable::get_OnDeselect()
extern void LeanSelectable_get_OnDeselect_m7C7EDEA2304E185949B9C074136D136215947EEF ();
// 0x0000092C System.Void Lean.Touch.LeanSelectable::set_IsSelected(System.Boolean)
extern void LeanSelectable_set_IsSelected_m517C73DEC280EA11BF65BCC493640771DCA27826 ();
// 0x0000092D System.Boolean Lean.Touch.LeanSelectable::get_IsSelected()
extern void LeanSelectable_get_IsSelected_m0E95E231CEEEA2368306D5D05AFE9537D67FD99D ();
// 0x0000092E System.Boolean Lean.Touch.LeanSelectable::get_IsSelectedRaw()
extern void LeanSelectable_get_IsSelectedRaw_mB36FF9B2F6526FF3DED4E82A9BBB226AE71206FA ();
// 0x0000092F System.Int32 Lean.Touch.LeanSelectable::get_IsSelectedCount()
extern void LeanSelectable_get_IsSelectedCount_m9BD8C3ED56E289D504F871B04B9D3AAFCFF40376 ();
// 0x00000930 Lean.Touch.LeanFinger Lean.Touch.LeanSelectable::get_SelectingFinger()
extern void LeanSelectable_get_SelectingFinger_m5D91B7F92F5CEC495CC9892458F40C0A9314C9A9 ();
// 0x00000931 System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanSelectable::get_SelectingFingers()
extern void LeanSelectable_get_SelectingFingers_m48C74C7A3EB4F4F52423B546EABE3913FD196ED0 ();
// 0x00000932 System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanSelectable::GetFingers(System.Boolean,System.Boolean,System.Int32,Lean.Touch.LeanSelectable)
extern void LeanSelectable_GetFingers_m0D64345B7E411E38DCD5BC02B7547F49D04C8DAC ();
// 0x00000933 System.Void Lean.Touch.LeanSelectable::Cull(System.Int32)
extern void LeanSelectable_Cull_m8EFC431440561FA762B70BB11FF8731580B3EC02 ();
// 0x00000934 Lean.Touch.LeanSelectable Lean.Touch.LeanSelectable::FindSelectable(Lean.Touch.LeanFinger)
extern void LeanSelectable_FindSelectable_m5C62AC00DB6109196486CBB558B92DC0A8C81149 ();
// 0x00000935 System.Void Lean.Touch.LeanSelectable::ReplaceSelection(Lean.Touch.LeanFinger,System.Collections.Generic.List`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_ReplaceSelection_m50AA185BDC77E90F11DEF0DECEFE1340AC746D67 ();
// 0x00000936 System.Boolean Lean.Touch.LeanSelectable::IsSelectedBy(Lean.Touch.LeanFinger)
extern void LeanSelectable_IsSelectedBy_m2BC0DEF99EA824C4CE6FC8907CDAFA6246342087 ();
// 0x00000937 System.Boolean Lean.Touch.LeanSelectable::GetIsSelected(System.Boolean)
extern void LeanSelectable_GetIsSelected_m1410B16332E61C7EDB9D065F36ECBAC9AE8A2C83 ();
// 0x00000938 System.Void Lean.Touch.LeanSelectable::Select()
extern void LeanSelectable_Select_mAC1825426C13E9054BC04C5D46165E9257DCDD17 ();
// 0x00000939 System.Void Lean.Touch.LeanSelectable::Select(Lean.Touch.LeanFinger)
extern void LeanSelectable_Select_mD9F5699A7FC88903C3EA3AEA7A1F1BAA795EA1C5 ();
// 0x0000093A System.Void Lean.Touch.LeanSelectable::Deselect()
extern void LeanSelectable_Deselect_m44C95DD63A74584A8A8E63EC9AE23927EEE63019 ();
// 0x0000093B System.Void Lean.Touch.LeanSelectable::DeselectAll()
extern void LeanSelectable_DeselectAll_m3D6B37079735D596BF3EF9AE9A3669939D23C8BF ();
// 0x0000093C System.Void Lean.Touch.LeanSelectable::OnEnable()
extern void LeanSelectable_OnEnable_m663499A5FCF163550A4BAF9260DCA3C45947649F ();
// 0x0000093D System.Void Lean.Touch.LeanSelectable::OnDisable()
extern void LeanSelectable_OnDisable_m5613E5067F8A2BADCD01BAC8D0F4481CE6A07AB0 ();
// 0x0000093E System.Void Lean.Touch.LeanSelectable::LateUpdate()
extern void LeanSelectable_LateUpdate_mB01A4277760B945B6E90969535B02EFEAECC3DC8 ();
// 0x0000093F System.Void Lean.Touch.LeanSelectable::FingerSet(Lean.Touch.LeanFinger)
extern void LeanSelectable_FingerSet_m5D922C3E1AA78E4269BEC4ACCC3B90C83F50FAB1 ();
// 0x00000940 System.Void Lean.Touch.LeanSelectable::FingerUp(Lean.Touch.LeanFinger)
extern void LeanSelectable_FingerUp_m25AFEE6D4A6B95A7D3A0FCE605AFFF2C4AED3F6A ();
// 0x00000941 System.Void Lean.Touch.LeanSelectable::.ctor()
extern void LeanSelectable__ctor_m3097F83B158F3D4D1456604CEFC2E13A070202FB ();
// 0x00000942 System.Void Lean.Touch.LeanSelectable::.cctor()
extern void LeanSelectable__cctor_mAA297FEAD5E6134B121C5CFB51FF00A9898C169A ();
// 0x00000943 Lean.Touch.LeanSelectable Lean.Touch.LeanSelectableBehaviour::get_Selectable()
extern void LeanSelectableBehaviour_get_Selectable_mBDF53AAD90E9A4FB7BE1D0EE9B84447D7E6450FF ();
// 0x00000944 System.Void Lean.Touch.LeanSelectableBehaviour::OnEnable()
extern void LeanSelectableBehaviour_OnEnable_mB2BE80ABC4CBAB9C399E554CE712F5D4C224B105 ();
// 0x00000945 System.Void Lean.Touch.LeanSelectableBehaviour::OnDisable()
extern void LeanSelectableBehaviour_OnDisable_m2C1F94BA33165DF721FBACBB5D3FA57B342AEC73 ();
// 0x00000946 System.Void Lean.Touch.LeanSelectableBehaviour::OnSelect(Lean.Touch.LeanFinger)
extern void LeanSelectableBehaviour_OnSelect_m9BA3A1147A76D5172648ECFA90449B86B83F7145 ();
// 0x00000947 System.Void Lean.Touch.LeanSelectableBehaviour::OnSelectUp(Lean.Touch.LeanFinger)
extern void LeanSelectableBehaviour_OnSelectUp_mE691446CBD739570F9A45B99CFFD2201C62EF2F5 ();
// 0x00000948 System.Void Lean.Touch.LeanSelectableBehaviour::OnDeselect()
extern void LeanSelectableBehaviour_OnDeselect_m0D5370300D2FE51038F8EAEFCAA68C29C011BD40 ();
// 0x00000949 System.Void Lean.Touch.LeanSelectableBehaviour::UpdateSelectable()
extern void LeanSelectableBehaviour_UpdateSelectable_mF9E3FA1C93F2DF075EEAA0CE9F859EDFAAE107D2 ();
// 0x0000094A System.Void Lean.Touch.LeanSelectableBehaviour::.ctor()
extern void LeanSelectableBehaviour__ctor_m995A2C7ADD6DD197410310E270424723A8FD6F77 ();
// 0x0000094B System.Void Lean.Touch.LeanSelectableGraphicColor::Start()
extern void LeanSelectableGraphicColor_Start_m98DB08FC0228058861063846BE895292415FC3A7 ();
// 0x0000094C System.Void Lean.Touch.LeanSelectableGraphicColor::OnSelect(Lean.Touch.LeanFinger)
extern void LeanSelectableGraphicColor_OnSelect_m82AED3F8866350F39A61FEE6AF4D0DDAFDBCC127 ();
// 0x0000094D System.Void Lean.Touch.LeanSelectableGraphicColor::OnDeselect()
extern void LeanSelectableGraphicColor_OnDeselect_mFC78EDD601DA200C17B7F51CA692FB47A3054F83 ();
// 0x0000094E System.Void Lean.Touch.LeanSelectableGraphicColor::ChangeColor(UnityEngine.Color)
extern void LeanSelectableGraphicColor_ChangeColor_mC7D163C985960AD1C3FE21C2475BEE2FED025BC8 ();
// 0x0000094F System.Void Lean.Touch.LeanSelectableGraphicColor::.ctor()
extern void LeanSelectableGraphicColor__ctor_m0316DB094111DF284B397AB57BF984023EA0E52E ();
// 0x00000950 System.Void Lean.Touch.LeanSelectableRendererColor::Awake()
extern void LeanSelectableRendererColor_Awake_mBBCF64F8E2A781456CC31A7B78B448702BFEBDC2 ();
// 0x00000951 System.Void Lean.Touch.LeanSelectableRendererColor::OnSelect(Lean.Touch.LeanFinger)
extern void LeanSelectableRendererColor_OnSelect_mA7E55C46C0E916C3F36E244C29B16B37F6555D3B ();
// 0x00000952 System.Void Lean.Touch.LeanSelectableRendererColor::OnDeselect()
extern void LeanSelectableRendererColor_OnDeselect_mC7F5806A8311EF2DF68386DFC9689E38DEF4A5EA ();
// 0x00000953 System.Void Lean.Touch.LeanSelectableRendererColor::ChangeColor(UnityEngine.Color)
extern void LeanSelectableRendererColor_ChangeColor_mEE38535524FFA325D44EA70BED83E7DE9CA3F4F3 ();
// 0x00000954 System.Void Lean.Touch.LeanSelectableRendererColor::.ctor()
extern void LeanSelectableRendererColor__ctor_m90261910773A2F8AC48269B9804FF18BDB231C2E ();
// 0x00000955 System.Void Lean.Touch.LeanSelectableSpriteRendererColor::Start()
extern void LeanSelectableSpriteRendererColor_Start_m1F115B54BC5C928D5BF7A57A3B504CA8693168FD ();
// 0x00000956 System.Void Lean.Touch.LeanSelectableSpriteRendererColor::OnSelect(Lean.Touch.LeanFinger)
extern void LeanSelectableSpriteRendererColor_OnSelect_mA90FDB561F0522CC065A2204FDBF7DAACEDF4490 ();
// 0x00000957 System.Void Lean.Touch.LeanSelectableSpriteRendererColor::OnDeselect()
extern void LeanSelectableSpriteRendererColor_OnDeselect_m88752CD3DD604BEB223FC10B9CA8062D52667672 ();
// 0x00000958 System.Void Lean.Touch.LeanSelectableSpriteRendererColor::ChangeColor(UnityEngine.Color)
extern void LeanSelectableSpriteRendererColor_ChangeColor_mD2A6F9007453DE9295508B98271D05EB0A9C3F2C ();
// 0x00000959 System.Void Lean.Touch.LeanSelectableSpriteRendererColor::.ctor()
extern void LeanSelectableSpriteRendererColor__ctor_m3EAD21A8B36BC85E518BB41128E8F952A900F038 ();
// 0x0000095A System.Void Lean.Touch.LeanSpawn::Spawn(Lean.Touch.LeanFinger)
extern void LeanSpawn_Spawn_m7E653322FDA83F6D2B7264B238D952C65A1B86FB ();
// 0x0000095B System.Boolean Lean.Touch.LeanSpawn::TrySpawn(Lean.Touch.LeanFinger,UnityEngine.Transform&)
extern void LeanSpawn_TrySpawn_m72FE5AE72230C5915D61F1FA5DFB41955CF8CB4F ();
// 0x0000095C System.Void Lean.Touch.LeanSpawn::UpdateSpawnedTransform(Lean.Touch.LeanFinger,UnityEngine.Transform)
extern void LeanSpawn_UpdateSpawnedTransform_m75E8A403823654A949383440E4636C78F18BD223 ();
// 0x0000095D System.Void Lean.Touch.LeanSpawn::.ctor()
extern void LeanSpawn__ctor_m7D5D14262983BCC8E46A2C2F6C3682AE64A21CFB ();
// 0x0000095E System.Void Lean.Touch.LeanTouchEvents::OnEnable()
extern void LeanTouchEvents_OnEnable_mFE78D00228E54D211EE9B533D5C23B83F528A0F2 ();
// 0x0000095F System.Void Lean.Touch.LeanTouchEvents::OnDisable()
extern void LeanTouchEvents_OnDisable_m4D6EED9A5EA60C3D4A556581C9A339FE05C9150F ();
// 0x00000960 System.Void Lean.Touch.LeanTouchEvents::OnFingerDown(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_OnFingerDown_mA543BE8CCA72241C2200977CB2A50AB848E9E4A5 ();
// 0x00000961 System.Void Lean.Touch.LeanTouchEvents::OnFingerSet(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_OnFingerSet_mE21B49D33910ABAEEE9B6F5E6CC9FF13B027E8F7 ();
// 0x00000962 System.Void Lean.Touch.LeanTouchEvents::OnFingerUp(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_OnFingerUp_mD90E32290811E523013D8D275938BA349148490D ();
// 0x00000963 System.Void Lean.Touch.LeanTouchEvents::OnFingerTap(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_OnFingerTap_mCE1D9C7A44602C5F854E119D77D8C2B10956EFAA ();
// 0x00000964 System.Void Lean.Touch.LeanTouchEvents::OnFingerSwipe(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_OnFingerSwipe_m2516A40BB5C4DB78CA09DCDB43A020EE407C4A4A ();
// 0x00000965 System.Void Lean.Touch.LeanTouchEvents::OnGesture(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanTouchEvents_OnGesture_m0E568D97D5A178870644E691AE53E30A24B700D8 ();
// 0x00000966 System.Void Lean.Touch.LeanTouchEvents::.ctor()
extern void LeanTouchEvents__ctor_m982C62FE308EA74978D38336B2B5C3D1B8625598 ();
// 0x00000967 System.Void Lean.Touch.LeanTranslate::Start()
extern void LeanTranslate_Start_mBFB6F3AD46E747C9F2EB77D21282844E262C87C1 ();
// 0x00000968 System.Void Lean.Touch.LeanTranslate::Update()
extern void LeanTranslate_Update_m49730E2C618A017F541B65FE7922A0C8290EF92D ();
// 0x00000969 System.Void Lean.Touch.LeanTranslate::TranslateUI(UnityEngine.Vector2)
extern void LeanTranslate_TranslateUI_m46365A1760FE0399C813EB35AC3A7BC21DE85F89 ();
// 0x0000096A System.Void Lean.Touch.LeanTranslate::Translate(UnityEngine.Vector2)
extern void LeanTranslate_Translate_mB2A5748D8874FD8B43E29208B819DE4106105629 ();
// 0x0000096B System.Void Lean.Touch.LeanTranslate::.ctor()
extern void LeanTranslate__ctor_m3565AFF03C61DC548137F6D676719D1D55F9296D ();
// 0x0000096C System.Boolean Lean.Touch.LeanFinger::get_IsActive()
extern void LeanFinger_get_IsActive_mED08E1A0FE9E5BC0F4E9414FE8123CE4E933EA1A ();
// 0x0000096D System.Single Lean.Touch.LeanFinger::get_SnapshotDuration()
extern void LeanFinger_get_SnapshotDuration_mDED148AEBE2DE6A6572B8D803D054C2DD2F78476 ();
// 0x0000096E System.Boolean Lean.Touch.LeanFinger::get_IsOverGui()
extern void LeanFinger_get_IsOverGui_mFAF99A6D9105F1B786CCEF8F2B49482203EAC860 ();
// 0x0000096F System.Boolean Lean.Touch.LeanFinger::get_Down()
extern void LeanFinger_get_Down_m1805E57A7EA45C1614F0F48D094F6CD8BFE473A0 ();
// 0x00000970 System.Boolean Lean.Touch.LeanFinger::get_Up()
extern void LeanFinger_get_Up_m25EAFB017A1CDBD559DA0A04A8CED5DAFF9F00FF ();
// 0x00000971 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_LastSnapshotScreenDelta()
extern void LeanFinger_get_LastSnapshotScreenDelta_m6BABA22BA9D64BB18B275DE986B70226ACD35A78 ();
// 0x00000972 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_LastSnapshotScaledDelta()
extern void LeanFinger_get_LastSnapshotScaledDelta_m779031A0C31B6679ABAA9728473F75753D02CB32 ();
// 0x00000973 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_ScreenDelta()
extern void LeanFinger_get_ScreenDelta_m425001057EEA27D3A688D4F7A6DB9A0F0C5F9AAF ();
// 0x00000974 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_ScaledDelta()
extern void LeanFinger_get_ScaledDelta_mFA2EE55C42279487140F746561DCCF65B4E71556 ();
// 0x00000975 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_SwipeScreenDelta()
extern void LeanFinger_get_SwipeScreenDelta_mE93F503A6938BDBA414E4FDD966164348395A55E ();
// 0x00000976 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_SwipeScaledDelta()
extern void LeanFinger_get_SwipeScaledDelta_m309305C36E8E716951EC2B69455A0D7A14F7F540 ();
// 0x00000977 UnityEngine.Ray Lean.Touch.LeanFinger::GetRay(UnityEngine.Camera)
extern void LeanFinger_GetRay_mF6E2731A4B05E6F4F4F046047AB1E4E6AE8C5622 ();
// 0x00000978 UnityEngine.Ray Lean.Touch.LeanFinger::GetStartRay(UnityEngine.Camera)
extern void LeanFinger_GetStartRay_m5C7D7FB8817B5758D0E407F8B7841AF4E3A296FC ();
// 0x00000979 UnityEngine.Vector2 Lean.Touch.LeanFinger::GetSnapshotScreenDelta(System.Single)
extern void LeanFinger_GetSnapshotScreenDelta_m0C9165D27D71D373CA93233235F108D097125BC1 ();
// 0x0000097A UnityEngine.Vector2 Lean.Touch.LeanFinger::GetSnapshotScaledDelta(System.Single)
extern void LeanFinger_GetSnapshotScaledDelta_m132FCF360F231F0CEB40EF6F784C31A75291D902 ();
// 0x0000097B UnityEngine.Vector2 Lean.Touch.LeanFinger::GetSnapshotScreenPosition(System.Single)
extern void LeanFinger_GetSnapshotScreenPosition_m92274CF51619130B789E9C79B63D815280FA67F2 ();
// 0x0000097C UnityEngine.Vector3 Lean.Touch.LeanFinger::GetSnapshotWorldPosition(System.Single,System.Single,UnityEngine.Camera)
extern void LeanFinger_GetSnapshotWorldPosition_mBFB7D50E8575FCF1A55EB8E1742671CBF360C66C ();
// 0x0000097D System.Single Lean.Touch.LeanFinger::GetRadians(UnityEngine.Vector2)
extern void LeanFinger_GetRadians_m0A483C7927A7509EA561667A82588D782E8F9751 ();
// 0x0000097E System.Single Lean.Touch.LeanFinger::GetDegrees(UnityEngine.Vector2)
extern void LeanFinger_GetDegrees_m06D7309A14F837C7C0FD0B10F4901F57FBDD0FF8 ();
// 0x0000097F System.Single Lean.Touch.LeanFinger::GetLastRadians(UnityEngine.Vector2)
extern void LeanFinger_GetLastRadians_m3996A159E8B08111844E7F3CA60847B7042725C6 ();
// 0x00000980 System.Single Lean.Touch.LeanFinger::GetLastDegrees(UnityEngine.Vector2)
extern void LeanFinger_GetLastDegrees_m5E8A26889D1908FEE6DAFD5A67547882A658D137 ();
// 0x00000981 System.Single Lean.Touch.LeanFinger::GetDeltaRadians(UnityEngine.Vector2)
extern void LeanFinger_GetDeltaRadians_m6C3C6021D48DB3E237672D0171BF9EBF44036CBA ();
// 0x00000982 System.Single Lean.Touch.LeanFinger::GetDeltaRadians(UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanFinger_GetDeltaRadians_m876E8B4F503EA84BCDF7021F30BD4A4DEE045AFB ();
// 0x00000983 System.Single Lean.Touch.LeanFinger::GetDeltaDegrees(UnityEngine.Vector2)
extern void LeanFinger_GetDeltaDegrees_m495E1B8C20C0DB6E3D189896336419DFD6BD43E8 ();
// 0x00000984 System.Single Lean.Touch.LeanFinger::GetDeltaDegrees(UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanFinger_GetDeltaDegrees_m2CE7770F72FDC57C6F3EB5B0E5CF5CBF3B6D7BC8 ();
// 0x00000985 System.Single Lean.Touch.LeanFinger::GetScreenDistance(UnityEngine.Vector2)
extern void LeanFinger_GetScreenDistance_mD33CA7C4AD147F33A3D339C246EDB9DAC22E1DD5 ();
// 0x00000986 System.Single Lean.Touch.LeanFinger::GetScaledDistance(UnityEngine.Vector2)
extern void LeanFinger_GetScaledDistance_m311DABF41EEA19DE3225F103E97E2D4DC4348C63 ();
// 0x00000987 System.Single Lean.Touch.LeanFinger::GetLastScreenDistance(UnityEngine.Vector2)
extern void LeanFinger_GetLastScreenDistance_m209656E8E8BF4CFFD4DC1F4DCBFD1FF7675291A1 ();
// 0x00000988 System.Single Lean.Touch.LeanFinger::GetLastScaledDistance(UnityEngine.Vector2)
extern void LeanFinger_GetLastScaledDistance_mE6C9C22AF38BB1D9C973CBB3796C34D704EB9965 ();
// 0x00000989 System.Single Lean.Touch.LeanFinger::GetStartScreenDistance(UnityEngine.Vector2)
extern void LeanFinger_GetStartScreenDistance_mE210C3037748F2AA73BFE54C130C7D14479D0E59 ();
// 0x0000098A System.Single Lean.Touch.LeanFinger::GetStartScaledDistance(UnityEngine.Vector2)
extern void LeanFinger_GetStartScaledDistance_mF9031456E65F7025048CFC6346AF4C890EDAEB3D ();
// 0x0000098B UnityEngine.Vector3 Lean.Touch.LeanFinger::GetStartWorldPosition(System.Single,UnityEngine.Camera)
extern void LeanFinger_GetStartWorldPosition_m3A354623C2E4CCAAD745808E709DF99F6D2E441B ();
// 0x0000098C UnityEngine.Vector3 Lean.Touch.LeanFinger::GetLastWorldPosition(System.Single,UnityEngine.Camera)
extern void LeanFinger_GetLastWorldPosition_m517937BFACA47CA557AA262853EEEAC890B96F21 ();
// 0x0000098D UnityEngine.Vector3 Lean.Touch.LeanFinger::GetWorldPosition(System.Single,UnityEngine.Camera)
extern void LeanFinger_GetWorldPosition_m0783C1FB5C3F6ABAC425A3580782749F0F6102C9 ();
// 0x0000098E UnityEngine.Vector3 Lean.Touch.LeanFinger::GetWorldDelta(System.Single,UnityEngine.Camera)
extern void LeanFinger_GetWorldDelta_mA2D0BCA07D501C0BADC34644119A5428C1C38B38 ();
// 0x0000098F UnityEngine.Vector3 Lean.Touch.LeanFinger::GetWorldDelta(System.Single,System.Single,UnityEngine.Camera)
extern void LeanFinger_GetWorldDelta_m781C6A434D62E719DB3B45A043EC59A472D5504C ();
// 0x00000990 System.Void Lean.Touch.LeanFinger::ClearSnapshots(System.Int32)
extern void LeanFinger_ClearSnapshots_mD96270D2187996E5567071AE7AD0C6F5B2A0CF0D ();
// 0x00000991 System.Void Lean.Touch.LeanFinger::RecordSnapshot()
extern void LeanFinger_RecordSnapshot_mE7ADF663EAE2C265E7FBAC5475F22D711BE48F3F ();
// 0x00000992 System.Void Lean.Touch.LeanFinger::.ctor()
extern void LeanFinger__ctor_mF28F6A57F6CF80EAB8647D2983EBD0109DE5D559 ();
// 0x00000993 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScreenCenter()
extern void LeanGesture_GetScreenCenter_m6F583702C5D811B23523ED40C53B3710690022D7 ();
// 0x00000994 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScreenCenter_mBEC4DE64D37ED43EB82B6E85F52A96148A42F080 ();
// 0x00000995 System.Boolean Lean.Touch.LeanGesture::TryGetScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetScreenCenter_m0660DC297D23E080D1B8621A80A233F2F931B377 ();
// 0x00000996 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetLastScreenCenter()
extern void LeanGesture_GetLastScreenCenter_m66FDBECCF89954AC020CC60B0352C19B9906D524 ();
// 0x00000997 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetLastScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetLastScreenCenter_m987FFE8EEDB32AAA69A57B5CBCED54276B212198 ();
// 0x00000998 System.Boolean Lean.Touch.LeanGesture::TryGetLastScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetLastScreenCenter_m5856882DA0C327EEEDD3698639A5F684FDBFC806 ();
// 0x00000999 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetStartScreenCenter()
extern void LeanGesture_GetStartScreenCenter_m58784E301770003075BD42704E38650B76EE4674 ();
// 0x0000099A UnityEngine.Vector2 Lean.Touch.LeanGesture::GetStartScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetStartScreenCenter_m49D20E336A531719B7139567EDD26431438E318F ();
// 0x0000099B System.Boolean Lean.Touch.LeanGesture::TryGetStartScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetStartScreenCenter_mB5219AB78347B05980039C19069F88196A791CE0 ();
// 0x0000099C UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScreenDelta()
extern void LeanGesture_GetScreenDelta_mFE2D30E45982C09F1B8033C0B162F1C2534E0202 ();
// 0x0000099D UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScreenDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScreenDelta_mDEF2602190CCE528E2B82D389003D776E903FB50 ();
// 0x0000099E System.Boolean Lean.Touch.LeanGesture::TryGetScreenDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetScreenDelta_m99C52693AD179253B538F307C6D5424C349E486E ();
// 0x0000099F UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScaledDelta()
extern void LeanGesture_GetScaledDelta_mFBD949167DDAF3EEC3794841906949261DB303F9 ();
// 0x000009A0 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScaledDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScaledDelta_mEBCF12934A62E30D4CCF7C1E105CCD7CDDBD8722 ();
// 0x000009A1 System.Boolean Lean.Touch.LeanGesture::TryGetScaledDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetScaledDelta_mA82ADE140404DF591535F14ECC57AC48C8F116EC ();
// 0x000009A2 UnityEngine.Vector3 Lean.Touch.LeanGesture::GetWorldDelta(System.Single,UnityEngine.Camera)
extern void LeanGesture_GetWorldDelta_m803890CA93315D14D4B42ED528B76376616B93BA ();
// 0x000009A3 UnityEngine.Vector3 Lean.Touch.LeanGesture::GetWorldDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,System.Single,UnityEngine.Camera)
extern void LeanGesture_GetWorldDelta_m18A2D76FD00C156F50FFEB13D58F8CA77FA360EF ();
// 0x000009A4 System.Boolean Lean.Touch.LeanGesture::TryGetWorldDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,System.Single,UnityEngine.Vector3&,UnityEngine.Camera)
extern void LeanGesture_TryGetWorldDelta_m00C6ADBB133D5E2013B73CE1E7EC4CDB64DC53DF ();
// 0x000009A5 System.Single Lean.Touch.LeanGesture::GetScreenDistance()
extern void LeanGesture_GetScreenDistance_m4491038BC481923F9C1189192B94570B628173ED ();
// 0x000009A6 System.Single Lean.Touch.LeanGesture::GetScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScreenDistance_m49234CF02121AC2E0B590C613AF2984FBC9227BA ();
// 0x000009A7 System.Single Lean.Touch.LeanGesture::GetScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetScreenDistance_mA3F575986D4FBD51409E97F0906322E971C45C8E ();
// 0x000009A8 System.Boolean Lean.Touch.LeanGesture::TryGetScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetScreenDistance_m05405D9D6EB3F296E9A32F1B0F6DEB572B5D56F5 ();
// 0x000009A9 System.Single Lean.Touch.LeanGesture::GetScaledDistance()
extern void LeanGesture_GetScaledDistance_m5CF1C4ED100296BD5827E7E7E808969E54594BD5 ();
// 0x000009AA System.Single Lean.Touch.LeanGesture::GetScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScaledDistance_m25BBEB85F0B2E9C61A28A432F82F26D4092A9EA2 ();
// 0x000009AB System.Single Lean.Touch.LeanGesture::GetScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetScaledDistance_m5AC1FD1B6E0F751D1F184B0D5CEBC67A3B290F16 ();
// 0x000009AC System.Boolean Lean.Touch.LeanGesture::TryGetScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetScaledDistance_m7241C7137635CC6D4A1A08AF24995568D55A6089 ();
// 0x000009AD System.Single Lean.Touch.LeanGesture::GetLastScreenDistance()
extern void LeanGesture_GetLastScreenDistance_mB16AE83227A86ED36B24896BC000AC0EBB8E7BFE ();
// 0x000009AE System.Single Lean.Touch.LeanGesture::GetLastScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetLastScreenDistance_m73169594EF4D8CF707159010C725A1890D25B42D ();
// 0x000009AF System.Single Lean.Touch.LeanGesture::GetLastScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetLastScreenDistance_m7F14A08E28E683BBD7AB9DC36BDFFD66432EFEF4 ();
// 0x000009B0 System.Boolean Lean.Touch.LeanGesture::TryGetLastScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetLastScreenDistance_m515DEE90923C17670475A4DCB5E907B83041CA1F ();
// 0x000009B1 System.Single Lean.Touch.LeanGesture::GetLastScaledDistance()
extern void LeanGesture_GetLastScaledDistance_mA9B68D5E00645335555ACF88DB15CF1F47E7536C ();
// 0x000009B2 System.Single Lean.Touch.LeanGesture::GetLastScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetLastScaledDistance_mF21CA58E54E66DBB7141365A62F18B3A86F84B54 ();
// 0x000009B3 System.Single Lean.Touch.LeanGesture::GetLastScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetLastScaledDistance_m903CBB366E704F9A92D215246A3DB101B95F3DA7 ();
// 0x000009B4 System.Boolean Lean.Touch.LeanGesture::TryGetLastScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetLastScaledDistance_mCDE66D39C5A0330B5B52ECE33ED7A1ED97CE099B ();
// 0x000009B5 System.Single Lean.Touch.LeanGesture::GetStartScreenDistance()
extern void LeanGesture_GetStartScreenDistance_m1B1B7D6A85518D33125EB7980F5D27B1B082C2A5 ();
// 0x000009B6 System.Single Lean.Touch.LeanGesture::GetStartScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetStartScreenDistance_m6F6C0358ED91F801E159A2A4E13E231C4B17C2D5 ();
// 0x000009B7 System.Single Lean.Touch.LeanGesture::GetStartScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetStartScreenDistance_mB138A7DB14B67AB5448BC24DE913ACBBD3F4A9CA ();
// 0x000009B8 System.Boolean Lean.Touch.LeanGesture::TryGetStartScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetStartScreenDistance_m2C9895EAA8769EBDB40B44AB383EE8F8F66B429B ();
// 0x000009B9 System.Single Lean.Touch.LeanGesture::GetStartScaledDistance()
extern void LeanGesture_GetStartScaledDistance_m2844DA256DAF2456FC7ABD37677F289E085484BD ();
// 0x000009BA System.Single Lean.Touch.LeanGesture::GetStartScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetStartScaledDistance_m2705202B5F71D2C1AD996D9B9C8239F007205455 ();
// 0x000009BB System.Single Lean.Touch.LeanGesture::GetStartScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetStartScaledDistance_m7DF4516FDD55B94E8A62A85A074FD2AA674AC2E9 ();
// 0x000009BC System.Boolean Lean.Touch.LeanGesture::TryGetStartScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetStartScaledDistance_m319D5142563436FD67BBD10CFE646ED8DDC916E5 ();
// 0x000009BD System.Single Lean.Touch.LeanGesture::GetPinchScale(System.Single)
extern void LeanGesture_GetPinchScale_m7371A033DC946C7666BB9FEA5F2B90B01FAA9514 ();
// 0x000009BE System.Single Lean.Touch.LeanGesture::GetPinchScale(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,System.Single)
extern void LeanGesture_GetPinchScale_m8FACA26AA33EF9F3C740A7675961CFD1B52AD218 ();
// 0x000009BF System.Boolean Lean.Touch.LeanGesture::TryGetPinchScale(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&,System.Single)
extern void LeanGesture_TryGetPinchScale_m184B99758343C54491B349599CA86E4F6E78B13D ();
// 0x000009C0 System.Single Lean.Touch.LeanGesture::GetPinchRatio(System.Single)
extern void LeanGesture_GetPinchRatio_m831DF88BF1BCCB563596402688221921BB4C856E ();
// 0x000009C1 System.Single Lean.Touch.LeanGesture::GetPinchRatio(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,System.Single)
extern void LeanGesture_GetPinchRatio_m67B78766805397BE267C10FE7CC35DD469CBE563 ();
// 0x000009C2 System.Boolean Lean.Touch.LeanGesture::TryGetPinchRatio(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&,System.Single)
extern void LeanGesture_TryGetPinchRatio_m263528E7637E8B8A71F33A1220AAAA8694E16E5E ();
// 0x000009C3 System.Single Lean.Touch.LeanGesture::GetTwistDegrees()
extern void LeanGesture_GetTwistDegrees_m84E94A9ACEECCC0E6438BD51919869B4FB59FA76 ();
// 0x000009C4 System.Single Lean.Touch.LeanGesture::GetTwistDegrees(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetTwistDegrees_mEF59A73DC08474FFD965A451DF1CC11E64549EEA ();
// 0x000009C5 System.Single Lean.Touch.LeanGesture::GetTwistDegrees(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanGesture_GetTwistDegrees_m36260508D01D334AD41479C59D17CBA206CD16CF ();
// 0x000009C6 System.Boolean Lean.Touch.LeanGesture::TryGetTwistDegrees(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetTwistDegrees_m73EB6ADA284F4BEB85971D7D45E720BD23F65E00 ();
// 0x000009C7 System.Single Lean.Touch.LeanGesture::GetTwistRadians()
extern void LeanGesture_GetTwistRadians_m6DEC4CB7A335B6A21635877DDF3BE4EEED93D466 ();
// 0x000009C8 System.Single Lean.Touch.LeanGesture::GetTwistRadians(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetTwistRadians_m08BB2C3EBFC89B00A79299F81136A3413EB4156F ();
// 0x000009C9 System.Single Lean.Touch.LeanGesture::GetTwistRadians(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanGesture_GetTwistRadians_m2CD5C1EC8140ED6930025FCBB0DE11964DA77DE5 ();
// 0x000009CA System.Boolean Lean.Touch.LeanGesture::TryGetTwistRadians(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetTwistRadians_mA24D7D75153F45E04AD8889AB7403C822BD79AE3 ();
// 0x000009CB UnityEngine.Vector3 Lean.Touch.LeanSnapshot::GetWorldPosition(System.Single,UnityEngine.Camera)
extern void LeanSnapshot_GetWorldPosition_m1C53DCA33B47EE047EAE80128377E06F484C9858 ();
// 0x000009CC Lean.Touch.LeanSnapshot Lean.Touch.LeanSnapshot::Pop()
extern void LeanSnapshot_Pop_m38C4A797FAA9486660642A6AD100344869757AEF ();
// 0x000009CD System.Boolean Lean.Touch.LeanSnapshot::TryGetScreenPosition(System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot>,System.Single,UnityEngine.Vector2&)
extern void LeanSnapshot_TryGetScreenPosition_m4801A9FBDF9604E13E3E133BB4DD63A40A422869 ();
// 0x000009CE System.Boolean Lean.Touch.LeanSnapshot::TryGetSnapshot(System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot>,System.Int32,System.Single&,UnityEngine.Vector2&)
extern void LeanSnapshot_TryGetSnapshot_mDCF40DD73B05287AFE77AD595EAF7E89067A6B2D ();
// 0x000009CF System.Int32 Lean.Touch.LeanSnapshot::GetLowerIndex(System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot>,System.Single)
extern void LeanSnapshot_GetLowerIndex_m66494922A5DCB5E4DCB9DF7A1AE6608ABF1A6D8F ();
// 0x000009D0 System.Void Lean.Touch.LeanSnapshot::.ctor()
extern void LeanSnapshot__ctor_m03099624F50615D694FB6349E8939BC26021A1CD ();
// 0x000009D1 System.Void Lean.Touch.LeanSnapshot::.cctor()
extern void LeanSnapshot__cctor_m33057BEE87A9F720AE236AE9FEC88C39C4C7CD93 ();
// 0x000009D2 System.Single Lean.Touch.LeanTouch::get_CurrentTapThreshold()
extern void LeanTouch_get_CurrentTapThreshold_m30269461E945AD3E2A5FAB6ECFEEA21F3E6D4B0E ();
// 0x000009D3 System.Single Lean.Touch.LeanTouch::get_CurrentSwipeThreshold()
extern void LeanTouch_get_CurrentSwipeThreshold_mA5D4266D0DAB77E81733078185ADAC2E1789DCA5 ();
// 0x000009D4 System.Int32 Lean.Touch.LeanTouch::get_CurrentReferenceDpi()
extern void LeanTouch_get_CurrentReferenceDpi_m7801C2A66E53F2A7CA5B573DEE1095BD7BD9B6E7 ();
// 0x000009D5 UnityEngine.LayerMask Lean.Touch.LeanTouch::get_CurrentGuiLayers()
extern void LeanTouch_get_CurrentGuiLayers_mE40055092CF899FE5554763D311F3363CA180A6E ();
// 0x000009D6 Lean.Touch.LeanTouch Lean.Touch.LeanTouch::get_Instance()
extern void LeanTouch_get_Instance_m931EA1A57E10617F1381C2191AD1C675C0E7A9BC ();
// 0x000009D7 System.Single Lean.Touch.LeanTouch::get_ScalingFactor()
extern void LeanTouch_get_ScalingFactor_m83C3989FF8B07240B2B77D8DF86775045B050635 ();
// 0x000009D8 System.Boolean Lean.Touch.LeanTouch::get_AnyMouseButtonSet()
extern void LeanTouch_get_AnyMouseButtonSet_m74CA0055BB7A8AF81009762C28555160FDA0B8AA ();
// 0x000009D9 System.Boolean Lean.Touch.LeanTouch::get_GuiInUse()
extern void LeanTouch_get_GuiInUse_mD832B00C159C7D800F6535FDE3501A34CC9FC1C2 ();
// 0x000009DA UnityEngine.Camera Lean.Touch.LeanTouch::GetCamera(UnityEngine.Camera,UnityEngine.GameObject)
extern void LeanTouch_GetCamera_mC9C5B0EC3E9D5C79724BE049DA38D90B329CF389 ();
// 0x000009DB System.Single Lean.Touch.LeanTouch::GetDampenFactor(System.Single,System.Single)
extern void LeanTouch_GetDampenFactor_mCEE40A0D99DD2A50B886C7F7898A962F64B63BF9 ();
// 0x000009DC System.Boolean Lean.Touch.LeanTouch::PointOverGui(UnityEngine.Vector2)
extern void LeanTouch_PointOverGui_m6094AD4CE04424942661B2AC1BB7AB2D1E42A18E ();
// 0x000009DD System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> Lean.Touch.LeanTouch::RaycastGui(UnityEngine.Vector2)
extern void LeanTouch_RaycastGui_mC1F9A41639819F278217FAACE2D1F4911CB1ADEB ();
// 0x000009DE System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> Lean.Touch.LeanTouch::RaycastGui(UnityEngine.Vector2,UnityEngine.LayerMask)
extern void LeanTouch_RaycastGui_mC963F9772C011AE8AF0127DB33BAA43F1A309D51 ();
// 0x000009DF System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::GetFingers(System.Boolean,System.Boolean,System.Int32)
extern void LeanTouch_GetFingers_m9E6D5C938A65135B101FE7B0AC0727CA3AE73D3B ();
// 0x000009E0 System.Void Lean.Touch.LeanTouch::Awake()
extern void LeanTouch_Awake_m8A631DE9493E60D9F822440FC12BC67257F89C2C ();
// 0x000009E1 System.Void Lean.Touch.LeanTouch::OnEnable()
extern void LeanTouch_OnEnable_mEE0A9053210AF5FD0E188942BF1371CB332E7B10 ();
// 0x000009E2 System.Void Lean.Touch.LeanTouch::OnDisable()
extern void LeanTouch_OnDisable_m9972C916C2FAC663E3D4A684737478A33DFCC0EC ();
// 0x000009E3 System.Void Lean.Touch.LeanTouch::Update()
extern void LeanTouch_Update_m1FE1A464E540A936D36C85C56BF29DC3B6F29B25 ();
// 0x000009E4 System.Void Lean.Touch.LeanTouch::OnGUI()
extern void LeanTouch_OnGUI_m742EE76781D030673EC554D9767ADFB16CDE46ED ();
// 0x000009E5 System.Void Lean.Touch.LeanTouch::BeginFingers()
extern void LeanTouch_BeginFingers_mE20FD46D2675007F5AC1C645C9D6B2A17528E4A1 ();
// 0x000009E6 System.Void Lean.Touch.LeanTouch::EndFingers()
extern void LeanTouch_EndFingers_mEF762AC83B419B7EA32C1BFB3F69B6AA5AFA1A95 ();
// 0x000009E7 System.Void Lean.Touch.LeanTouch::PollFingers()
extern void LeanTouch_PollFingers_m41DC56D0459B675E18361E302C579AD4C19EB9D6 ();
// 0x000009E8 System.Void Lean.Touch.LeanTouch::UpdateEvents()
extern void LeanTouch_UpdateEvents_mD89F972D0CADFEDC7AC4B1A4BBAB6E06EEBC59A5 ();
// 0x000009E9 System.Void Lean.Touch.LeanTouch::AddFinger(System.Int32,UnityEngine.Vector2,System.Single)
extern void LeanTouch_AddFinger_m146B0A34AC43BADF2EBE98D5717F355BD4848C2B ();
// 0x000009EA Lean.Touch.LeanFinger Lean.Touch.LeanTouch::FindFinger(System.Int32)
extern void LeanTouch_FindFinger_mAB3FD0FA5A8BBC8F20719CCDD51BFE577C621F57 ();
// 0x000009EB System.Int32 Lean.Touch.LeanTouch::FindInactiveFingerIndex(System.Int32)
extern void LeanTouch_FindInactiveFingerIndex_m90C49C10B108B66296D1F02B2288AFF9E937C3F9 ();
// 0x000009EC System.Void Lean.Touch.LeanTouch::.ctor()
extern void LeanTouch__ctor_m888ABF46462DB451DCF00EA2D6749026F94D6233 ();
// 0x000009ED System.Void Lean.Touch.LeanTouch::.cctor()
extern void LeanTouch__cctor_m378C2A67FA99293BFB140591E7997DC42829784B ();
// 0x000009EE System.Single Lean.Common.LeanHelper::DampenFactor(System.Single,System.Single)
extern void LeanHelper_DampenFactor_m2C82A6D76F9D8C56E6FCD1A036CF688E2D2BE7B3 ();
// 0x000009EF T Lean.Common.LeanHelper::Destroy(T)
// 0x000009F0 System.Void Lean.Common.Examples.LeanCircuit::UpdateMesh()
extern void LeanCircuit_UpdateMesh_mED259F731E82703397C5691FB5AE0E1ADBE85425 ();
// 0x000009F1 System.Void Lean.Common.Examples.LeanCircuit::Populate()
extern void LeanCircuit_Populate_mCFB0E32A499294D0F7B30F97317786D0AA289935 ();
// 0x000009F2 System.Void Lean.Common.Examples.LeanCircuit::Start()
extern void LeanCircuit_Start_m570CB73BD1C6FF197BD14EC788CE454F0FCB585A ();
// 0x000009F3 System.Void Lean.Common.Examples.LeanCircuit::AddLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color)
extern void LeanCircuit_AddLine_mCD383374CCA0F52FCE1B6FFF42D29F504AD9C52D ();
// 0x000009F4 System.Void Lean.Common.Examples.LeanCircuit::AddPoint(UnityEngine.Vector3,System.Single,UnityEngine.Color)
extern void LeanCircuit_AddPoint_mFA3365B862B56BF30AF9E80044D4209955692A17 ();
// 0x000009F5 System.Void Lean.Common.Examples.LeanCircuit::AddNode(UnityEngine.Vector3)
extern void LeanCircuit_AddNode_m8A668166575F25039CAE8744EFCE55B86B3F53E9 ();
// 0x000009F6 System.Void Lean.Common.Examples.LeanCircuit::.ctor()
extern void LeanCircuit__ctor_m52655D141A32D9449A6FB7D85324FC786006CD28 ();
// 0x000009F7 System.Void Lean.Common.Examples.LeanCircuit::.cctor()
extern void LeanCircuit__cctor_m3026C1E28178AA5F151EE7D7735C8C386167DA9B ();
// 0x000009F8 UnityEngine.Texture2D Lean.Common.Examples.LeanDocumentation::get_Icon()
extern void LeanDocumentation_get_Icon_mE61BAAC7E851B65AC297FDAF57BD3BFE00BA6FAC ();
// 0x000009F9 System.Void Lean.Common.Examples.LeanDocumentation::.ctor()
extern void LeanDocumentation__ctor_mF9FFD184E8FA280D87E3786580F1708CF1D59144 ();
// 0x000009FA System.Void Lean.Common.Examples.LeanMarker::set_Target(UnityEngine.Object)
extern void LeanMarker_set_Target_m2CFF230587247B3FE2ED37FD7B9411263F79F3D7 ();
// 0x000009FB UnityEngine.Object Lean.Common.Examples.LeanMarker::get_Target()
extern void LeanMarker_get_Target_mDED9AE9AEF9DED1DD6CA6F5C9F954574960C58B7 ();
// 0x000009FC System.Void Lean.Common.Examples.LeanMarker::OnEnable()
extern void LeanMarker_OnEnable_m34D170CEA0AD6A4856526D424B3174AD5AF3EB21 ();
// 0x000009FD System.Void Lean.Common.Examples.LeanMarker::OnDisable()
extern void LeanMarker_OnDisable_m599F0BEC832B7998E70426C2A6604E8741C3209D ();
// 0x000009FE System.Void Lean.Common.Examples.LeanMarker::.ctor()
extern void LeanMarker__ctor_mF18C3FE4149EB17BDCC29163236B24E4EAB781F9 ();
// 0x000009FF System.Void Lean.Common.Examples.LeanMarker::.cctor()
extern void LeanMarker__cctor_mD6D6FDA3C37CF54CDE047EB2A21814D65D73AEFC ();
// 0x00000A00 System.Void Lean.Common.Examples.LeanOpenUrl::Open(System.String)
extern void LeanOpenUrl_Open_m8C9FE5446D8D6957713F3552CBED9DA0D39F9EE0 ();
// 0x00000A01 System.Void Lean.Common.Examples.LeanOpenUrl::.ctor()
extern void LeanOpenUrl__ctor_m5EFAB8E84C6F50EE13CC1A1F32793922163F024C ();
// 0x00000A02 SimpleJSON.JSONNodeType SimpleJSON.JSONNode::get_Tag()
// 0x00000A03 SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.Int32)
extern void JSONNode_get_Item_m15B56AF59515C383F438425F707DCD45A92F4865 ();
// 0x00000A04 System.Void SimpleJSON.JSONNode::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_mEB2B1436A55A4EA01826840CE454DE6139DBFD96 ();
// 0x00000A05 SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.String)
extern void JSONNode_get_Item_m60C1ABECBE0571F458998A9A8410EE8ED8D4BC9E ();
// 0x00000A06 System.Void SimpleJSON.JSONNode::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_m26390E552CD8420AFD0C634ED57597CC06625A26 ();
// 0x00000A07 System.String SimpleJSON.JSONNode::get_Value()
extern void JSONNode_get_Value_mA5FDEA6BB16F7B21AE6F41A85F5004120B552580 ();
// 0x00000A08 System.Void SimpleJSON.JSONNode::set_Value(System.String)
extern void JSONNode_set_Value_mAAD460AEE30562A5B2729DB9545D2984D6496E76 ();
// 0x00000A09 System.Int32 SimpleJSON.JSONNode::get_Count()
extern void JSONNode_get_Count_m37D2CF69CE110C3655E89366A3EE2262EA99933C ();
// 0x00000A0A System.Boolean SimpleJSON.JSONNode::get_IsNumber()
extern void JSONNode_get_IsNumber_mAEC1A3CE41B21C02317EB63FD8BD1366327A4D1E ();
// 0x00000A0B System.Boolean SimpleJSON.JSONNode::get_IsString()
extern void JSONNode_get_IsString_mBACE5A4D126E8011EE8D9D18510AAE31D8B51AE0 ();
// 0x00000A0C System.Boolean SimpleJSON.JSONNode::get_IsBoolean()
extern void JSONNode_get_IsBoolean_mB9C9F7A3C14C7250032AE78BC885CE87F15C3FFD ();
// 0x00000A0D System.Boolean SimpleJSON.JSONNode::get_IsNull()
extern void JSONNode_get_IsNull_m71B4615E695BF588CE5EAC79F52F6EC66B1C1461 ();
// 0x00000A0E System.Boolean SimpleJSON.JSONNode::get_IsArray()
extern void JSONNode_get_IsArray_m73A69DC1A6B8F910CEE872204A1FF2EA9CA65D31 ();
// 0x00000A0F System.Boolean SimpleJSON.JSONNode::get_IsObject()
extern void JSONNode_get_IsObject_m9210B0CDD4B70E42E3EA0B8EE6C2D510A640EA60 ();
// 0x00000A10 System.Boolean SimpleJSON.JSONNode::get_Inline()
extern void JSONNode_get_Inline_m72DF6AD0574D74648194D898901C0F61C2EF29E8 ();
// 0x00000A11 System.Void SimpleJSON.JSONNode::set_Inline(System.Boolean)
extern void JSONNode_set_Inline_mB44B0D5303F8644FB399963F8342F1915FED1EC2 ();
// 0x00000A12 System.Void SimpleJSON.JSONNode::Add(System.String,SimpleJSON.JSONNode)
extern void JSONNode_Add_mB007E02475524C684D2032F44344FC0727D0AED1 ();
// 0x00000A13 System.Void SimpleJSON.JSONNode::Add(SimpleJSON.JSONNode)
extern void JSONNode_Add_m72F8683C5AB8E6DDA52AAB63F5AB2CF5DE668AA4 ();
// 0x00000A14 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.String)
extern void JSONNode_Remove_m4C6ABDC53258E365E4BEDFFB471996D0595CE653 ();
// 0x00000A15 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.Int32)
extern void JSONNode_Remove_m40DC66D2BFA64F462C956249568569C640D65FBE ();
// 0x00000A16 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(SimpleJSON.JSONNode)
extern void JSONNode_Remove_mEC2EA47AADD914B3468F912E6EED1DA02821D118 ();
// 0x00000A17 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_Children()
extern void JSONNode_get_Children_m0BDB86A86A43943A02FBEA21A8FEDE6D91906395 ();
// 0x00000A18 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_DeepChildren()
extern void JSONNode_get_DeepChildren_m843513089FDD8B254DCC3232FD4DA85056923D9F ();
// 0x00000A19 System.String SimpleJSON.JSONNode::ToString()
extern void JSONNode_ToString_m8CFDF7832FB437B9D9285EA5913ACFA8BF529C75 ();
// 0x00000A1A System.String SimpleJSON.JSONNode::ToString(System.Int32)
extern void JSONNode_ToString_m959FD63EB02266172C30D5608E92E9B478EA1698 ();
// 0x00000A1B System.Void SimpleJSON.JSONNode::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
// 0x00000A1C SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONNode::GetEnumerator()
// 0x00000A1D System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>> SimpleJSON.JSONNode::get_Linq()
extern void JSONNode_get_Linq_m4CA50FA2E097B82ED352580539F7215F181AE1EA ();
// 0x00000A1E SimpleJSON.JSONNode_KeyEnumerator SimpleJSON.JSONNode::get_Keys()
extern void JSONNode_get_Keys_mAEC584E7C7F1CCC000C51E279CEA435552023ED3 ();
// 0x00000A1F SimpleJSON.JSONNode_ValueEnumerator SimpleJSON.JSONNode::get_Values()
extern void JSONNode_get_Values_m647ABB3BC12AFD94CE4C23531628EFC29A7981D5 ();
// 0x00000A20 System.Double SimpleJSON.JSONNode::get_AsDouble()
extern void JSONNode_get_AsDouble_mA9A87A9DDF3DB8A927705894B0A70369513743B8 ();
// 0x00000A21 System.Void SimpleJSON.JSONNode::set_AsDouble(System.Double)
extern void JSONNode_set_AsDouble_mA2D6CA445FBB3B93D4E135F91CF1CE9779375098 ();
// 0x00000A22 System.Int32 SimpleJSON.JSONNode::get_AsInt()
extern void JSONNode_get_AsInt_m35E5CF8D5FCA1E5D1697C6E666FF3CD5FC1B2BC1 ();
// 0x00000A23 System.Void SimpleJSON.JSONNode::set_AsInt(System.Int32)
extern void JSONNode_set_AsInt_m3D8AFBE4D49B29711CCECBDAD4C145BD72C47D5C ();
// 0x00000A24 System.Single SimpleJSON.JSONNode::get_AsFloat()
extern void JSONNode_get_AsFloat_m53D151773142FEECC3886C1ADEB2BEC22A0C3CAC ();
// 0x00000A25 System.Void SimpleJSON.JSONNode::set_AsFloat(System.Single)
extern void JSONNode_set_AsFloat_m6D669252ACE2A695075685624BDB94A60260FF63 ();
// 0x00000A26 System.Boolean SimpleJSON.JSONNode::get_AsBool()
extern void JSONNode_get_AsBool_mC1732D312696100E7F429542BB876EC949DA9947 ();
// 0x00000A27 System.Void SimpleJSON.JSONNode::set_AsBool(System.Boolean)
extern void JSONNode_set_AsBool_m91CAA7562009099B02BD538F37D94CEE8F8882B7 ();
// 0x00000A28 SimpleJSON.JSONArray SimpleJSON.JSONNode::get_AsArray()
extern void JSONNode_get_AsArray_m7DF6AB373218A86EFF6A9478A824D5BB8C01421A ();
// 0x00000A29 SimpleJSON.JSONObject SimpleJSON.JSONNode::get_AsObject()
extern void JSONNode_get_AsObject_m8BC40A325C24DE488E73E7DAFEA530B270BBD95B ();
// 0x00000A2A SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.String)
extern void JSONNode_op_Implicit_m94391C275D1BE4AC26A04A098F4B65BBB7D7AF84 ();
// 0x00000A2B System.String SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mB446B8B500123D3F4F3C5D66F095316FF663385D ();
// 0x00000A2C SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Double)
extern void JSONNode_op_Implicit_m112A87AB176887F93C5660CFD3C169A5BB1C3CB4 ();
// 0x00000A2D System.Double SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m9B6B63FDCCE9EC0F0CF543B13E3863936D283487 ();
// 0x00000A2E SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Single)
extern void JSONNode_op_Implicit_mC458C08484535F8FD3B586B5D5337B9AC093C837 ();
// 0x00000A2F System.Single SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m3DDE962F828FB4DB318C6D8C336FFD51AF19C552 ();
// 0x00000A30 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Int32)
extern void JSONNode_op_Implicit_m279DCD5A06BDC389C2AF42381AC41E3EC06700D5 ();
// 0x00000A31 System.Int32 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mE6EE79025736C59E41C99A0C9101BAF32BE77E18 ();
// 0x00000A32 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Boolean)
extern void JSONNode_op_Implicit_mBA5DC585F9071890732C4805B3FA7E1134E76477 ();
// 0x00000A33 System.Boolean SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m2E4691C3EE93FD2CB2570E30EBE1F84E25A53099 ();
// 0x00000A34 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern void JSONNode_op_Implicit_mFB610B47429EC263B56050700A6D74F38A93FCC1 ();
// 0x00000A35 System.Boolean SimpleJSON.JSONNode::op_Equality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Equality_mFA64BB19805777C603E6E1BD8D6628B52DEB4A1E ();
// 0x00000A36 System.Boolean SimpleJSON.JSONNode::op_Inequality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Inequality_m65F2F76C1716D266797A058589DF280A44E2CBA5 ();
// 0x00000A37 System.Boolean SimpleJSON.JSONNode::Equals(System.Object)
extern void JSONNode_Equals_mD1DBB741A272720F18B24437CD78B338B65900C0 ();
// 0x00000A38 System.Int32 SimpleJSON.JSONNode::GetHashCode()
extern void JSONNode_GetHashCode_mDDA191EC3E9FA81A238FCB2B1C8024FCA97677AC ();
// 0x00000A39 System.Text.StringBuilder SimpleJSON.JSONNode::get_EscapeBuilder()
extern void JSONNode_get_EscapeBuilder_mE5AC264BA62AA54B0DCDCB165564F5EE28B50653 ();
// 0x00000A3A System.String SimpleJSON.JSONNode::Escape(System.String)
extern void JSONNode_Escape_mABAE2F4F85D5F926A6B80D5181FD69D69669EFA6 ();
// 0x00000A3B System.Void SimpleJSON.JSONNode::ParseElement(SimpleJSON.JSONNode,System.String,System.String,System.Boolean)
extern void JSONNode_ParseElement_m7C0AE6936FF1C5678FF55A838971E8E0E24C8A69 ();
// 0x00000A3C SimpleJSON.JSONNode SimpleJSON.JSONNode::Parse(System.String)
extern void JSONNode_Parse_m1F5205640E23CB1423043FA1C5379D0BF309E4F9 ();
// 0x00000A3D System.Void SimpleJSON.JSONNode::.ctor()
extern void JSONNode__ctor_m5C8FC3D0D04154FFC73CDEB6D6D051422907D3CE ();
// 0x00000A3E System.Void SimpleJSON.JSONNode::.cctor()
extern void JSONNode__cctor_mA6EEB9601A65ECE220047266B8986E7B975909CD ();
// 0x00000A3F System.Boolean SimpleJSON.JSONArray::get_Inline()
extern void JSONArray_get_Inline_m701D46F6554CE60CD9967EA4598C51C1C941ED2B ();
// 0x00000A40 System.Void SimpleJSON.JSONArray::set_Inline(System.Boolean)
extern void JSONArray_set_Inline_m736B5C89F41DE7A7CCAC0FC6160CF4FD50B989DC ();
// 0x00000A41 SimpleJSON.JSONNodeType SimpleJSON.JSONArray::get_Tag()
extern void JSONArray_get_Tag_m51CD8C530354A4AEA428DC009FD95C27D4AB53CE ();
// 0x00000A42 System.Boolean SimpleJSON.JSONArray::get_IsArray()
extern void JSONArray_get_IsArray_m50C77F695B10048CA997D53BE3E3DC88E1689B03 ();
// 0x00000A43 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONArray::GetEnumerator()
extern void JSONArray_GetEnumerator_mB01C410D721F8B673A311AFE4913BD2F1C8E37F9 ();
// 0x00000A44 SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.Int32)
extern void JSONArray_get_Item_mFEBC0948C9244F2C604E80F5A4098BFB51255D10 ();
// 0x00000A45 System.Void SimpleJSON.JSONArray::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_mB2AFD08A9F2CE998903ABA685D88C9CF00894885 ();
// 0x00000A46 SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.String)
extern void JSONArray_get_Item_m1B541EE05E39CD8EB7A5541EF2ADC0031FC81215 ();
// 0x00000A47 System.Void SimpleJSON.JSONArray::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_m90E948A121CE1B37388237F92828AFA5C539B2CE ();
// 0x00000A48 System.Int32 SimpleJSON.JSONArray::get_Count()
extern void JSONArray_get_Count_m77CEECDA838C7D2B99D2A0ADBFD9F216BD50C647 ();
// 0x00000A49 System.Void SimpleJSON.JSONArray::Add(System.String,SimpleJSON.JSONNode)
extern void JSONArray_Add_m2F2793895033D6E8C9CA77D0440FEFD164C1F31D ();
// 0x00000A4A SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(System.Int32)
extern void JSONArray_Remove_mD5817778EF93C826B92D5E8CC86CA211D056879F ();
// 0x00000A4B SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(SimpleJSON.JSONNode)
extern void JSONArray_Remove_m36719C3D5868724800574C299C59A3398347D049 ();
// 0x00000A4C System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray::get_Children()
extern void JSONArray_get_Children_mB77EB8E1B351B82181B50334A57FE456AB8BBB85 ();
// 0x00000A4D System.Void SimpleJSON.JSONArray::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONArray_WriteToStringBuilder_m4FEE53C6F7BF56C22BC919A982F9A1047362819C ();
// 0x00000A4E System.Void SimpleJSON.JSONArray::.ctor()
extern void JSONArray__ctor_m80F3F8569F953F1B2448EF688ADBE2BE06B85EAA ();
// 0x00000A4F System.Boolean SimpleJSON.JSONObject::get_Inline()
extern void JSONObject_get_Inline_m68C30F81DBE34CC9512A1CF26E501A9042CEE6B2 ();
// 0x00000A50 System.Void SimpleJSON.JSONObject::set_Inline(System.Boolean)
extern void JSONObject_set_Inline_mE4E0D0ABDE60706A3BE440D751C80D45E565078E ();
// 0x00000A51 SimpleJSON.JSONNodeType SimpleJSON.JSONObject::get_Tag()
extern void JSONObject_get_Tag_m24A4D8474C6803E5B36B9C7B16CC35F48C00C486 ();
// 0x00000A52 System.Boolean SimpleJSON.JSONObject::get_IsObject()
extern void JSONObject_get_IsObject_mD17F33216C3AA982747307A9783FC34B4C249ACD ();
// 0x00000A53 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONObject::GetEnumerator()
extern void JSONObject_GetEnumerator_m89934764DE9D9DB8D007DE2FDB956B35A483CE2A ();
// 0x00000A54 SimpleJSON.JSONNode SimpleJSON.JSONObject::get_Item(System.String)
extern void JSONObject_get_Item_mC02C1B3017199E1B58E09012760D91B0236A79DF ();
// 0x00000A55 System.Void SimpleJSON.JSONObject::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONObject_set_Item_mAF05A0803D9648754295AD43922D8750E248AB5B ();
// 0x00000A56 SimpleJSON.JSONNode SimpleJSON.JSONObject::get_Item(System.Int32)
extern void JSONObject_get_Item_m7B5F21E465C8A06F0FD00EE62B59CFD229360976 ();
// 0x00000A57 System.Void SimpleJSON.JSONObject::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONObject_set_Item_mD3F2E832E77FDAEB95AF4C6113F0314E3C286D3E ();
// 0x00000A58 System.Int32 SimpleJSON.JSONObject::get_Count()
extern void JSONObject_get_Count_mDDB2E49A1D4DC4C26FAB083943E4F47B39C233AF ();
// 0x00000A59 System.Void SimpleJSON.JSONObject::Add(System.String,SimpleJSON.JSONNode)
extern void JSONObject_Add_m75EF35AE491E67AEDCAC14A8810985463681353D ();
// 0x00000A5A SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(System.String)
extern void JSONObject_Remove_m56A85EFAE9C4DDF43C755D4938665178D59186D7 ();
// 0x00000A5B SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(System.Int32)
extern void JSONObject_Remove_m56C844643C6B6E81D4E3467DA869A39FF93C79FE ();
// 0x00000A5C SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(SimpleJSON.JSONNode)
extern void JSONObject_Remove_m9943021B70272C24F3FE7D87421F3DD6E1EADB15 ();
// 0x00000A5D System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONObject::get_Children()
extern void JSONObject_get_Children_m553BB3AF1E134F5FB59F325E8D3E811C204E6AD6 ();
// 0x00000A5E System.Void SimpleJSON.JSONObject::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONObject_WriteToStringBuilder_mCC982C03B19077CC8BC60D61B1893FD5543A7B1E ();
// 0x00000A5F System.Void SimpleJSON.JSONObject::.ctor()
extern void JSONObject__ctor_m0F87A09A5AB3C1141E7E82D8C45D9C6B44F87E5F ();
// 0x00000A60 SimpleJSON.JSONNodeType SimpleJSON.JSONString::get_Tag()
extern void JSONString_get_Tag_m04F409B5247C8DCD38D24808764CF24D869E6185 ();
// 0x00000A61 System.Boolean SimpleJSON.JSONString::get_IsString()
extern void JSONString_get_IsString_mD933D266CC563B1D5E85CFA7C7480CB2E5ACA48E ();
// 0x00000A62 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONString::GetEnumerator()
extern void JSONString_GetEnumerator_mF8D0B322B157094DFDC176D6B8BD0E2C4BE626E4 ();
// 0x00000A63 System.String SimpleJSON.JSONString::get_Value()
extern void JSONString_get_Value_m29504F709941AD379852FDD0B1A08E8DC4B2E58A ();
// 0x00000A64 System.Void SimpleJSON.JSONString::set_Value(System.String)
extern void JSONString_set_Value_mECAA9F7BFEE67F189A1493F90CDC4475318ED6FF ();
// 0x00000A65 System.Void SimpleJSON.JSONString::.ctor(System.String)
extern void JSONString__ctor_m3DE989CC0DA12FE3E23174CD280D86971771ADAA ();
// 0x00000A66 System.Void SimpleJSON.JSONString::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONString_WriteToStringBuilder_m5297F304170133F94CB9E813928B07D57FFEB5C8 ();
// 0x00000A67 System.Boolean SimpleJSON.JSONString::Equals(System.Object)
extern void JSONString_Equals_mCE7D7CAD2604883600D10531380FECF4AFB92F09 ();
// 0x00000A68 System.Int32 SimpleJSON.JSONString::GetHashCode()
extern void JSONString_GetHashCode_mC05889B33A8AF366C4C8456D54F94F252C2300B1 ();
// 0x00000A69 SimpleJSON.JSONNodeType SimpleJSON.JSONNumber::get_Tag()
extern void JSONNumber_get_Tag_m5810C73995BD124FB40ADDFEB7172AE1295D3C3F ();
// 0x00000A6A System.Boolean SimpleJSON.JSONNumber::get_IsNumber()
extern void JSONNumber_get_IsNumber_mD941320E66AA55D3391E146E3E624AFFCCCC5912 ();
// 0x00000A6B SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONNumber::GetEnumerator()
extern void JSONNumber_GetEnumerator_m3DDB7A5F04D7C6C314D6969ECDB3314385E0F8C0 ();
// 0x00000A6C System.String SimpleJSON.JSONNumber::get_Value()
extern void JSONNumber_get_Value_m285D5CFDCF3259112D7128D9EC962DF7C1C638CB ();
// 0x00000A6D System.Void SimpleJSON.JSONNumber::set_Value(System.String)
extern void JSONNumber_set_Value_m7B6A055194CFD54CE0F8360809DF0516696D04B0 ();
// 0x00000A6E System.Double SimpleJSON.JSONNumber::get_AsDouble()
extern void JSONNumber_get_AsDouble_m837CA051FAF2D45E0B415B1CE91C6DE1A9F5C399 ();
// 0x00000A6F System.Void SimpleJSON.JSONNumber::set_AsDouble(System.Double)
extern void JSONNumber_set_AsDouble_mE9AE823BDDDD4CE0E3BD37ED70B0330A3D303E68 ();
// 0x00000A70 System.Void SimpleJSON.JSONNumber::.ctor(System.Double)
extern void JSONNumber__ctor_mA5B174BD1A163979DCDD304E4A679A1D9E8801B8 ();
// 0x00000A71 System.Void SimpleJSON.JSONNumber::.ctor(System.String)
extern void JSONNumber__ctor_mCA0A65AB3C617FC6F8066EB9C38E2B413971E28F ();
// 0x00000A72 System.Void SimpleJSON.JSONNumber::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONNumber_WriteToStringBuilder_mB754D2A3988C1E35BDEF7A2FB9E842ECBAFE1F4B ();
// 0x00000A73 System.Boolean SimpleJSON.JSONNumber::IsNumeric(System.Object)
extern void JSONNumber_IsNumeric_m88429E8156B5500398D25C0C5A0ED127BBABC887 ();
// 0x00000A74 System.Boolean SimpleJSON.JSONNumber::Equals(System.Object)
extern void JSONNumber_Equals_mF299CAFC541A41B0B4D9B12A6967B35B9F554931 ();
// 0x00000A75 System.Int32 SimpleJSON.JSONNumber::GetHashCode()
extern void JSONNumber_GetHashCode_m33B32859776F731322E20E05B527B92255F130F5 ();
// 0x00000A76 SimpleJSON.JSONNodeType SimpleJSON.JSONBool::get_Tag()
extern void JSONBool_get_Tag_mCBABB544B93C712D3F57CC8FBEC49260FEA9284A ();
// 0x00000A77 System.Boolean SimpleJSON.JSONBool::get_IsBoolean()
extern void JSONBool_get_IsBoolean_m98A1AEE6EC63912E54A15116CDB7C8418209FDBA ();
// 0x00000A78 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONBool::GetEnumerator()
extern void JSONBool_GetEnumerator_m4B8C0CC079A97EB3B0331ECFFB15F7DD0AB7A180 ();
// 0x00000A79 System.String SimpleJSON.JSONBool::get_Value()
extern void JSONBool_get_Value_mDDA4D24E9F16DED3152898F98F16B06327BEF9F6 ();
// 0x00000A7A System.Void SimpleJSON.JSONBool::set_Value(System.String)
extern void JSONBool_set_Value_m068B69773876CB81DF3C699ABC743FB1CE861F1A ();
// 0x00000A7B System.Boolean SimpleJSON.JSONBool::get_AsBool()
extern void JSONBool_get_AsBool_mCA60BF3904B572629003D2887EF27F89C1E764E9 ();
// 0x00000A7C System.Void SimpleJSON.JSONBool::set_AsBool(System.Boolean)
extern void JSONBool_set_AsBool_m4E1A515A31B59A3F2B683F4ECCEA388684320B91 ();
// 0x00000A7D System.Void SimpleJSON.JSONBool::.ctor(System.Boolean)
extern void JSONBool__ctor_mB729B68264E989BA9AEE3B86AF17E3906FF7C9AD ();
// 0x00000A7E System.Void SimpleJSON.JSONBool::.ctor(System.String)
extern void JSONBool__ctor_mFD5C92FB72B70A027D45ED45E42E6E7516858CEE ();
// 0x00000A7F System.Void SimpleJSON.JSONBool::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONBool_WriteToStringBuilder_mABA45EF0C30EE803D2FD1AC77FA7B2EA967BCFD9 ();
// 0x00000A80 System.Boolean SimpleJSON.JSONBool::Equals(System.Object)
extern void JSONBool_Equals_m853E2A5B2F12290669528B9E27BB2CBB2F8FDE09 ();
// 0x00000A81 System.Int32 SimpleJSON.JSONBool::GetHashCode()
extern void JSONBool_GetHashCode_m13AE52408BA8EFA79CD151D50EC9DCF4F7CAB73A ();
// 0x00000A82 SimpleJSON.JSONNull SimpleJSON.JSONNull::CreateOrGet()
extern void JSONNull_CreateOrGet_mFCEA43022679C084EA7BEB23AD203E1B9B33E1D7 ();
// 0x00000A83 System.Void SimpleJSON.JSONNull::.ctor()
extern void JSONNull__ctor_m49798992779B2B1E8D1BAFDF4498C2F8AEA76A4F ();
// 0x00000A84 SimpleJSON.JSONNodeType SimpleJSON.JSONNull::get_Tag()
extern void JSONNull_get_Tag_m498F7F5444421EDA491F023F0B76AC1D1D735936 ();
// 0x00000A85 System.Boolean SimpleJSON.JSONNull::get_IsNull()
extern void JSONNull_get_IsNull_m4ED1FF25799E79A71002A79A4AC27B2177635FAA ();
// 0x00000A86 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONNull::GetEnumerator()
extern void JSONNull_GetEnumerator_m390B90BACB37AC50B726CE28D07463748A202A79 ();
// 0x00000A87 System.String SimpleJSON.JSONNull::get_Value()
extern void JSONNull_get_Value_m07942D8E26A0D88514646E5603F55C4E3D16DD60 ();
// 0x00000A88 System.Void SimpleJSON.JSONNull::set_Value(System.String)
extern void JSONNull_set_Value_mA2582B26943415A7DE20E1DAD3C96D7A0E451922 ();
// 0x00000A89 System.Boolean SimpleJSON.JSONNull::get_AsBool()
extern void JSONNull_get_AsBool_m60DE1D508FB98AAF328AB1965DAC1BB43881CD98 ();
// 0x00000A8A System.Void SimpleJSON.JSONNull::set_AsBool(System.Boolean)
extern void JSONNull_set_AsBool_m5C230E9709559FD14099B0848A4D5C4C1A815000 ();
// 0x00000A8B System.Boolean SimpleJSON.JSONNull::Equals(System.Object)
extern void JSONNull_Equals_m522306B502C21C1E2EBE989556F3F16331848600 ();
// 0x00000A8C System.Int32 SimpleJSON.JSONNull::GetHashCode()
extern void JSONNull_GetHashCode_m950BC0E73B87DC3336908BEF882459BC20E5A55B ();
// 0x00000A8D System.Void SimpleJSON.JSONNull::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONNull_WriteToStringBuilder_m84A9A7C230BBEE86E8A548B8446FF19B5E469B6E ();
// 0x00000A8E System.Void SimpleJSON.JSONNull::.cctor()
extern void JSONNull__cctor_m0E80AEF0AD9DCA6A8018A55DAA8464FCA2DBCC16 ();
// 0x00000A8F SimpleJSON.JSONNodeType SimpleJSON.JSONLazyCreator::get_Tag()
extern void JSONLazyCreator_get_Tag_mC1AC02C25A03BF67F0D89FD9900CBC6EEB090A5D ();
// 0x00000A90 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONLazyCreator::GetEnumerator()
extern void JSONLazyCreator_GetEnumerator_mDAA175BC448F491BD873169D142E119DF6733ED6 ();
// 0x00000A91 System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode)
extern void JSONLazyCreator__ctor_m8FC6D598D0237C9350588BD29072C041A61F0798 ();
// 0x00000A92 System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode,System.String)
extern void JSONLazyCreator__ctor_mC6E81F011E8C8956780A3B334A91DA44147BF188 ();
// 0x00000A93 System.Void SimpleJSON.JSONLazyCreator::Set(SimpleJSON.JSONNode)
extern void JSONLazyCreator_Set_m746D69028C9A2C5E0B1FBBA1F8F008C2052A1779 ();
// 0x00000A94 SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.Int32)
extern void JSONLazyCreator_get_Item_m5EA50EE949F36710942A9FF8EF5777D233B3C047 ();
// 0x00000A95 System.Void SimpleJSON.JSONLazyCreator::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_m3B7837F849B2388008C45743CD3140CE828F2606 ();
// 0x00000A96 SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.String)
extern void JSONLazyCreator_get_Item_m95C7B93F385B7EF2D47052C39794763732D6FF0C ();
// 0x00000A97 System.Void SimpleJSON.JSONLazyCreator::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_m00DFAB6CD611C42D6B22D575ECDCFE265710FBCC ();
// 0x00000A98 System.Void SimpleJSON.JSONLazyCreator::Add(SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m4361ED9A8E16E28BAB9DCFA1FC1AFB27124FB670 ();
// 0x00000A99 System.Void SimpleJSON.JSONLazyCreator::Add(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m6A8B2BEC3941E48339A2AD67047FC6D0BADFA17F ();
// 0x00000A9A System.Boolean SimpleJSON.JSONLazyCreator::op_Equality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Equality_m68D0526912FAFBC3D3332DEBACCD45DEA750C2C7 ();
// 0x00000A9B System.Boolean SimpleJSON.JSONLazyCreator::op_Inequality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Inequality_mA05185BE9E99126A18F9EF368C747CE578FBAD95 ();
// 0x00000A9C System.Boolean SimpleJSON.JSONLazyCreator::Equals(System.Object)
extern void JSONLazyCreator_Equals_mB6087EC316E745F8D61D2068D2A98CA634113D4E ();
// 0x00000A9D System.Int32 SimpleJSON.JSONLazyCreator::GetHashCode()
extern void JSONLazyCreator_GetHashCode_m51335A4464EA4383300746254D92B163803C8C43 ();
// 0x00000A9E System.Int32 SimpleJSON.JSONLazyCreator::get_AsInt()
extern void JSONLazyCreator_get_AsInt_mD1A90C028EA0FEC6DB194136F7AD88FDE408F67A ();
// 0x00000A9F System.Void SimpleJSON.JSONLazyCreator::set_AsInt(System.Int32)
extern void JSONLazyCreator_set_AsInt_m8FE656CA5259D0B4320F3DBF60BD19A686A26857 ();
// 0x00000AA0 System.Single SimpleJSON.JSONLazyCreator::get_AsFloat()
extern void JSONLazyCreator_get_AsFloat_mC7748D1573024F4EC5A1E1EA1357D812FEDDF870 ();
// 0x00000AA1 System.Void SimpleJSON.JSONLazyCreator::set_AsFloat(System.Single)
extern void JSONLazyCreator_set_AsFloat_m5846F6BF2B58995E98DB57813A6C78CC1A7E1934 ();
// 0x00000AA2 System.Double SimpleJSON.JSONLazyCreator::get_AsDouble()
extern void JSONLazyCreator_get_AsDouble_m64481355784007008FE0CDEB3CC984B2B02FD465 ();
// 0x00000AA3 System.Void SimpleJSON.JSONLazyCreator::set_AsDouble(System.Double)
extern void JSONLazyCreator_set_AsDouble_m278B0693BF4190C6FACC267DB451E8E190577350 ();
// 0x00000AA4 System.Boolean SimpleJSON.JSONLazyCreator::get_AsBool()
extern void JSONLazyCreator_get_AsBool_m6675B2D4E0C4D06469EF0B66D3E0CF5B8C5EB7BE ();
// 0x00000AA5 System.Void SimpleJSON.JSONLazyCreator::set_AsBool(System.Boolean)
extern void JSONLazyCreator_set_AsBool_m3DA5CF7B6A12BE73753CDF5FD3F72D06932942B2 ();
// 0x00000AA6 SimpleJSON.JSONArray SimpleJSON.JSONLazyCreator::get_AsArray()
extern void JSONLazyCreator_get_AsArray_m7ED16496F0BC83E265C51066E586108D22850C5D ();
// 0x00000AA7 SimpleJSON.JSONObject SimpleJSON.JSONLazyCreator::get_AsObject()
extern void JSONLazyCreator_get_AsObject_m49378AF7ED532AAB2664C02C21E537630528B9C2 ();
// 0x00000AA8 System.Void SimpleJSON.JSONLazyCreator::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONLazyCreator_WriteToStringBuilder_m691033C947C63F4764EF87A8DC5A17CE3EF2D59F ();
// 0x00000AA9 SimpleJSON.JSONNode SimpleJSON.JSON::Parse(System.String)
extern void JSON_Parse_mAB3D7D96F2A4170914DA8D0A54787AAC1762BCE9 ();
// 0x00000AAA System.String Intelistyle.LatestProduct::ToString()
extern void LatestProduct_ToString_m59505EED68AA17DF6541A4B0753CDAA3C58FB0DC ();
// 0x00000AAB System.Void Intelistyle.LatestProduct::.ctor()
extern void LatestProduct__ctor_m8214D78AFA862E63DE858710D084DBC4B82ACC65 ();
// 0x00000AAC System.String Intelistyle.ProductDetail::ToString()
extern void ProductDetail_ToString_m8A0284DB58B82E8965EC6D409D28DD7847589394 ();
// 0x00000AAD System.Void Intelistyle.ProductDetail::.ctor()
extern void ProductDetail__ctor_m7D6C549F2782DE98960B7B5D2706B861B0BB487A ();
// 0x00000AAE System.Void Intelistyle.BASIC_DETAILS::.ctor()
extern void BASIC_DETAILS__ctor_m8C231A82EBCEE43AF4AAE8164D0E6410F7665C25 ();
// 0x00000AAF System.Void Intelistyle.TAG::.ctor()
extern void TAG__ctor_mA042C41C88534F5FC4079C5FDC9FA972BFA6F022 ();
// 0x00000AB0 System.Void Intelistyle.product_details::.ctor()
extern void product_details__ctor_mFEB3177FCAEC0EAE68C3FA437179063FEBE1DC35 ();
// 0x00000AB1 System.Void Intelistyle.DATALEVEL1::.ctor()
extern void DATALEVEL1__ctor_m44BC58218B11FEB97BF2613C3C69EC83DC321799 ();
// 0x00000AB2 System.Void Intelistyle.DATALEVEL2::.ctor()
extern void DATALEVEL2__ctor_m12BF53BCE9FC48C1D3A876035B4F9CF1813F104B ();
// 0x00000AB3 System.Void Intelistyle.COLORITEM::.ctor()
extern void COLORITEM__ctor_m194A6DCDE05DADA9F49B716DFE4EB1965FC576D7 ();
// 0x00000AB4 System.Void Intelistyle.INFO::.ctor()
extern void INFO__ctor_m63A4D5CF5CF151203477EB1CF5E2FB59B250DBF2 ();
// 0x00000AB5 System.Void ApiExample_<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m25149D6FEE53DCACE6BAAD705A6788C6EBF6C501 ();
// 0x00000AB6 System.Void ApiExample_<>c__DisplayClass12_0::<GetDetailProduct>b__0(Proyecto26.ResponseHelper)
extern void U3CU3Ec__DisplayClass12_0_U3CGetDetailProductU3Eb__0_m4E2AE071E9BAA94C3C094775A68B353433E34608 ();
// 0x00000AB7 System.Void ApiExample_<>c::.cctor()
extern void U3CU3Ec__cctor_mBB55E725B17FA4A1952CD335C17369A4FE7E8C53 ();
// 0x00000AB8 System.Void ApiExample_<>c::.ctor()
extern void U3CU3Ec__ctor_mB02544597371209AE1E834E809B3A1C0E7008A2E ();
// 0x00000AB9 System.Void ApiExample_<>c::<GetDetailProduct>b__12_1(System.Exception)
extern void U3CU3Ec_U3CGetDetailProductU3Eb__12_1_m0EBF9439D0CF7C2D5FC155102701EBB1D2509F63 ();
// 0x00000ABA System.Void ApiExample_<>c::<DownloadContinuouslyProductDetail>b__22_0(Proyecto26.ResponseHelper)
extern void U3CU3Ec_U3CDownloadContinuouslyProductDetailU3Eb__22_0_m59C97C81191665960A609CB88E4E18BB5D29BC2D ();
// 0x00000ABB System.Void ApiExample_<>c::<GetStyleIt>b__35_0(Proyecto26.ResponseHelper)
extern void U3CU3Ec_U3CGetStyleItU3Eb__35_0_mC1FAD67D0137E9509D492FC1334330526C32EE09 ();
// 0x00000ABC System.Void ApiExample_<>c::<GetSimilarItems>b__36_0(Proyecto26.ResponseHelper)
extern void U3CU3Ec_U3CGetSimilarItemsU3Eb__36_0_mA2A91F66E247338BB9E7053BFB588237D66839E5 ();
// 0x00000ABD System.Void ApiExample_<>c::<GetSimilarItems>b__36_1(System.Exception)
extern void U3CU3Ec_U3CGetSimilarItemsU3Eb__36_1_m5377A81D05489E6A0C484C20A7A8B9E6729F95A6 ();
// 0x00000ABE System.Void ApiExample_<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m5A31A0310AB9B899241FADEFC3B8EC1E02909B67 ();
// 0x00000ABF System.Void ApiExample_<>c__DisplayClass13_0::<TestCategories>b__0(Proyecto26.ResponseHelper)
extern void U3CU3Ec__DisplayClass13_0_U3CTestCategoriesU3Eb__0_m1DE86B2BD7D3C300615E48875239AABDF963145E ();
// 0x00000AC0 System.Void ApiExample_<>c__DisplayClass13_0::<TestCategories>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass13_0_U3CTestCategoriesU3Eb__1_mF724DC43EFFB768C6E62B1E87410365CE97296CB ();
// 0x00000AC1 System.Void ApiExample_<GETTexWithTimer>d__14::.ctor(System.Int32)
extern void U3CGETTexWithTimerU3Ed__14__ctor_mB3780C563A4473B475B6C7035E128C58C8F95658 ();
// 0x00000AC2 System.Void ApiExample_<GETTexWithTimer>d__14::System.IDisposable.Dispose()
extern void U3CGETTexWithTimerU3Ed__14_System_IDisposable_Dispose_m9EED32DF5F495B058A77E6E7232B668CA97A2B1A ();
// 0x00000AC3 System.Boolean ApiExample_<GETTexWithTimer>d__14::MoveNext()
extern void U3CGETTexWithTimerU3Ed__14_MoveNext_m9987674D352B1E1BFF0E2D331EFD801E34EBF5E9 ();
// 0x00000AC4 System.Object ApiExample_<GETTexWithTimer>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGETTexWithTimerU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC22A77CDDCC9A47DBC56F59B4CB6F24E7D2CAD13 ();
// 0x00000AC5 System.Void ApiExample_<GETTexWithTimer>d__14::System.Collections.IEnumerator.Reset()
extern void U3CGETTexWithTimerU3Ed__14_System_Collections_IEnumerator_Reset_mA9ABD1B38B238DBEDB9B6F1A9F1F752C8FCE9B8B ();
// 0x00000AC6 System.Object ApiExample_<GETTexWithTimer>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CGETTexWithTimerU3Ed__14_System_Collections_IEnumerator_get_Current_m34AF29C7AF067CCC8330BDFEF9192D077FAB078A ();
// 0x00000AC7 System.Void ApiExample_<GETWITHHEADER>d__15::.ctor(System.Int32)
extern void U3CGETWITHHEADERU3Ed__15__ctor_m5C4B8373E1C14CB6E044924AB87BB724DF8B460A ();
// 0x00000AC8 System.Void ApiExample_<GETWITHHEADER>d__15::System.IDisposable.Dispose()
extern void U3CGETWITHHEADERU3Ed__15_System_IDisposable_Dispose_m1EB4C7472904EEDD062E9D4F8F658A7782D27616 ();
// 0x00000AC9 System.Boolean ApiExample_<GETWITHHEADER>d__15::MoveNext()
extern void U3CGETWITHHEADERU3Ed__15_MoveNext_m3CDD40B09B8C80F17B900E1BCA56BC293CCFF32A ();
// 0x00000ACA System.Object ApiExample_<GETWITHHEADER>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGETWITHHEADERU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA99F0D8BC4A8D7F9B89B083A91E598BFBCDAABAA ();
// 0x00000ACB System.Void ApiExample_<GETWITHHEADER>d__15::System.Collections.IEnumerator.Reset()
extern void U3CGETWITHHEADERU3Ed__15_System_Collections_IEnumerator_Reset_mC88E3F6D8A214DB1949C295FB23E6B5E078C9C4A ();
// 0x00000ACC System.Object ApiExample_<GETWITHHEADER>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CGETWITHHEADERU3Ed__15_System_Collections_IEnumerator_get_Current_m16B2B872A92D309DC192F72B0064345FED6A5BF7 ();
// 0x00000ACD System.Void ApiExample_<DLExample>d__16::.ctor(System.Int32)
extern void U3CDLExampleU3Ed__16__ctor_m712F26C47330BD416C36EE802F5EAF4EF0EF5A67 ();
// 0x00000ACE System.Void ApiExample_<DLExample>d__16::System.IDisposable.Dispose()
extern void U3CDLExampleU3Ed__16_System_IDisposable_Dispose_m91306082B3BFC330EB9CA5176868FD766DAFD810 ();
// 0x00000ACF System.Boolean ApiExample_<DLExample>d__16::MoveNext()
extern void U3CDLExampleU3Ed__16_MoveNext_mDAFF2CAB5B8218627C841AAF67CED42853D62739 ();
// 0x00000AD0 System.Object ApiExample_<DLExample>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDLExampleU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m19FFEF9076B08E927AB34D4596ACC20A1550F1EC ();
// 0x00000AD1 System.Void ApiExample_<DLExample>d__16::System.Collections.IEnumerator.Reset()
extern void U3CDLExampleU3Ed__16_System_Collections_IEnumerator_Reset_m608F2170EDD9BABC87D49AE28CB087467FF1C56F ();
// 0x00000AD2 System.Object ApiExample_<DLExample>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CDLExampleU3Ed__16_System_Collections_IEnumerator_get_Current_m2D7978D7B4CBFE6AD8445BAFE901143335E49CE2 ();
// 0x00000AD3 System.Void ApiExample_<Downloading>d__24::.ctor(System.Int32)
extern void U3CDownloadingU3Ed__24__ctor_mD0E028408AAAF01B491F2D9A82D21D92F15CA6B5 ();
// 0x00000AD4 System.Void ApiExample_<Downloading>d__24::System.IDisposable.Dispose()
extern void U3CDownloadingU3Ed__24_System_IDisposable_Dispose_m5521A0798BBBB167DC3A123DC3CBFFFB4DA80D3F ();
// 0x00000AD5 System.Boolean ApiExample_<Downloading>d__24::MoveNext()
extern void U3CDownloadingU3Ed__24_MoveNext_m8ED3B2F0B061B5E71FE8217C7D42AF3A2EB11D88 ();
// 0x00000AD6 System.Object ApiExample_<Downloading>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadingU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB29C064BD5CFA476E95B432C6029071987F886D0 ();
// 0x00000AD7 System.Void ApiExample_<Downloading>d__24::System.Collections.IEnumerator.Reset()
extern void U3CDownloadingU3Ed__24_System_Collections_IEnumerator_Reset_m1B3B5A12CB15A13283CB4CCBA603278269559B4D ();
// 0x00000AD8 System.Object ApiExample_<Downloading>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadingU3Ed__24_System_Collections_IEnumerator_get_Current_mDEE0B0F7FFB1EFEFE485869B1B53776C763E2D36 ();
// 0x00000AD9 System.Void ApiExample_<GetRequest>d__27::.ctor(System.Int32)
extern void U3CGetRequestU3Ed__27__ctor_m8D6A61B14848DE84A7D407CA9722481203D8B297 ();
// 0x00000ADA System.Void ApiExample_<GetRequest>d__27::System.IDisposable.Dispose()
extern void U3CGetRequestU3Ed__27_System_IDisposable_Dispose_mFD8EA538E5AAEE49A0CAA0A008D5A1B68C6ADC5E ();
// 0x00000ADB System.Boolean ApiExample_<GetRequest>d__27::MoveNext()
extern void U3CGetRequestU3Ed__27_MoveNext_mDFE702445D15895DF79EA1088093F5CF66E0BBB6 ();
// 0x00000ADC System.Void ApiExample_<GetRequest>d__27::<>m__Finally1()
extern void U3CGetRequestU3Ed__27_U3CU3Em__Finally1_m3B1B1BC65F9288A6857976DF657B98CF01A15E92 ();
// 0x00000ADD System.Object ApiExample_<GetRequest>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetRequestU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9FBCEB84B833346D3DF484C0A41EBF50CAA74D85 ();
// 0x00000ADE System.Void ApiExample_<GetRequest>d__27::System.Collections.IEnumerator.Reset()
extern void U3CGetRequestU3Ed__27_System_Collections_IEnumerator_Reset_mDF485D496F6DF0778CAF9192A47959084E5333C5 ();
// 0x00000ADF System.Object ApiExample_<GetRequest>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CGetRequestU3Ed__27_System_Collections_IEnumerator_get_Current_mF665C2FAEC767D5B8C801F4B0A8B5244419607D9 ();
// 0x00000AE0 System.Void ApiExample_<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_mE40A2A9B3FBEC3C50D9719C8820CD7D9BB1C8C7A ();
// 0x00000AE1 System.Void ApiExample_<>c__DisplayClass28_0::<DownloadReadImageAsync>b__0(System.Object)
extern void U3CU3Ec__DisplayClass28_0_U3CDownloadReadImageAsyncU3Eb__0_m89E1B751F44B327A100ABC9DD753010DCE3B055B ();
// 0x00000AE2 System.Void ApiExample_<>c__DisplayClass28_1::.ctor()
extern void U3CU3Ec__DisplayClass28_1__ctor_m48CD99BD28EF67828488E45F62CFEA122EA8667C ();
// 0x00000AE3 System.Void ApiExample_<>c__DisplayClass28_1::<DownloadReadImageAsync>b__1()
extern void U3CU3Ec__DisplayClass28_1_U3CDownloadReadImageAsyncU3Eb__1_m4F72395C2BBB957072015181F865B103DCCC050C ();
// 0x00000AE4 System.Void ApiExample_<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_m9D2C89921018B34A88AE803E937DE83D210B2D40 ();
// 0x00000AE5 System.Void ApiExample_<>c__DisplayClass29_0::<ReadImageAsync>b__0(System.Object)
extern void U3CU3Ec__DisplayClass29_0_U3CReadImageAsyncU3Eb__0_m014B4A738C9AD452D2845E160C74D89CF3247342 ();
// 0x00000AE6 System.Void ApiExample_<>c__DisplayClass29_1::.ctor()
extern void U3CU3Ec__DisplayClass29_1__ctor_m2F2B89F5322AB6B16064F0AF99B2C334AE3F5D2C ();
// 0x00000AE7 System.Void ApiExample_<>c__DisplayClass29_1::<ReadImageAsync>b__1()
extern void U3CU3Ec__DisplayClass29_1_U3CReadImageAsyncU3Eb__1_mF486211944655093979DDBFFA9AD7FB16127E5C5 ();
// 0x00000AE8 System.Void ApiExample_<ReduceTexture2>d__31::.ctor(System.Int32)
extern void U3CReduceTexture2U3Ed__31__ctor_m150B04D458FAC3DA42ED0E44810FC14EA7A64BA6 ();
// 0x00000AE9 System.Void ApiExample_<ReduceTexture2>d__31::System.IDisposable.Dispose()
extern void U3CReduceTexture2U3Ed__31_System_IDisposable_Dispose_mC45A91DA3968CF5E77F216C891389E756CC819F1 ();
// 0x00000AEA System.Boolean ApiExample_<ReduceTexture2>d__31::MoveNext()
extern void U3CReduceTexture2U3Ed__31_MoveNext_m8B84731BE8DE732763463BC6C7020C0CA9B5D14F ();
// 0x00000AEB System.Object ApiExample_<ReduceTexture2>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReduceTexture2U3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m32AC526178659022F63336973BEE9678F11ED5B6 ();
// 0x00000AEC System.Void ApiExample_<ReduceTexture2>d__31::System.Collections.IEnumerator.Reset()
extern void U3CReduceTexture2U3Ed__31_System_Collections_IEnumerator_Reset_m7D1CE94E38909EEA031A2EFE5282112CA64CD690 ();
// 0x00000AED System.Object ApiExample_<ReduceTexture2>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CReduceTexture2U3Ed__31_System_Collections_IEnumerator_get_Current_m68C1284ECE5F20419C94AC4B2C65BE6CC0270382 ();
// 0x00000AEE System.Void ApiExample_<ReduceTexture>d__32::.ctor(System.Int32)
extern void U3CReduceTextureU3Ed__32__ctor_mC888817C06364BDDFC3171F3A80EDC76948F9476 ();
// 0x00000AEF System.Void ApiExample_<ReduceTexture>d__32::System.IDisposable.Dispose()
extern void U3CReduceTextureU3Ed__32_System_IDisposable_Dispose_m0FF19DA639792401ABF744997433FF18A78B03A2 ();
// 0x00000AF0 System.Boolean ApiExample_<ReduceTexture>d__32::MoveNext()
extern void U3CReduceTextureU3Ed__32_MoveNext_m4CCFEE17E383E87AEE49DAE2397208608D9A27C6 ();
// 0x00000AF1 System.Object ApiExample_<ReduceTexture>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReduceTextureU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D6B72538757AAAD9CDFA73FD31C79B6EB1C8DC4 ();
// 0x00000AF2 System.Void ApiExample_<ReduceTexture>d__32::System.Collections.IEnumerator.Reset()
extern void U3CReduceTextureU3Ed__32_System_Collections_IEnumerator_Reset_m52AA4767E714E8EB13C45310E34962F812F0AA39 ();
// 0x00000AF3 System.Object ApiExample_<ReduceTexture>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CReduceTextureU3Ed__32_System_Collections_IEnumerator_get_Current_m57D6C26F5C02333B7B4FBF8EDC3328CA6B81B8DD ();
// 0x00000AF4 System.Void ApiExample_<GetTexture>d__34::.ctor(System.Int32)
extern void U3CGetTextureU3Ed__34__ctor_m6BA0BC8BA5A3C036CA1EE35FA994FD8665145807 ();
// 0x00000AF5 System.Void ApiExample_<GetTexture>d__34::System.IDisposable.Dispose()
extern void U3CGetTextureU3Ed__34_System_IDisposable_Dispose_mA903B9F5C2CE107AA3B9D16797AA3696BEE7A5FD ();
// 0x00000AF6 System.Boolean ApiExample_<GetTexture>d__34::MoveNext()
extern void U3CGetTextureU3Ed__34_MoveNext_m931097F50FD0A2AD321B72091CCA7EBFB36B8314 ();
// 0x00000AF7 System.Object ApiExample_<GetTexture>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetTextureU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDB95224DD4B96927652568A0C97ABF77E838E0AF ();
// 0x00000AF8 System.Void ApiExample_<GetTexture>d__34::System.Collections.IEnumerator.Reset()
extern void U3CGetTextureU3Ed__34_System_Collections_IEnumerator_Reset_m15B6B0E81FB5318CA788ECA6C336B3B35E108B5D ();
// 0x00000AF9 System.Object ApiExample_<GetTexture>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CGetTextureU3Ed__34_System_Collections_IEnumerator_get_Current_m3C0340BE4A125645A4891DB7973D116504274DE7 ();
// 0x00000AFA System.Void ApiExample_<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_m6B5AFD189CAC65E3028F00264E404D010DC617D3 ();
// 0x00000AFB System.Void ApiExample_<>c__DisplayClass35_0::<GetStyleIt>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass35_0_U3CGetStyleItU3Eb__1_m7339AA27700E21D03980803965E41351DA12179D ();
// 0x00000AFC System.Void LoadingScene_<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_m85F8D5F0D5A46D2524F25D9B2E56908067D20B17 ();
// 0x00000AFD System.Void LoadingScene_<>c__DisplayClass35_0::<MultiDownloadAPI>b__0(Proyecto26.ResponseHelper)
extern void U3CU3Ec__DisplayClass35_0_U3CMultiDownloadAPIU3Eb__0_mAA6876F6FDF6AC66B4C8C849E58109EE09587CD7 ();
// 0x00000AFE System.Void LoadingScene_<>c__DisplayClass35_0::<MultiDownloadAPI>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass35_0_U3CMultiDownloadAPIU3Eb__1_m4BDCCD9EE2B5D9522D786676FE990BFA30D65C9B ();
// 0x00000AFF System.Void LoadingScene_<MultiDownloadNCache>d__41::.ctor(System.Int32)
extern void U3CMultiDownloadNCacheU3Ed__41__ctor_mDAE3F0944CEA399F02D06D604EF1666B1C4CF67A ();
// 0x00000B00 System.Void LoadingScene_<MultiDownloadNCache>d__41::System.IDisposable.Dispose()
extern void U3CMultiDownloadNCacheU3Ed__41_System_IDisposable_Dispose_m1C726AAC0BA51C7C3BB4805B5156219020BEAA92 ();
// 0x00000B01 System.Boolean LoadingScene_<MultiDownloadNCache>d__41::MoveNext()
extern void U3CMultiDownloadNCacheU3Ed__41_MoveNext_m076F88E2DB5343FE914879BDEA916DC190B3A41D ();
// 0x00000B02 System.Object LoadingScene_<MultiDownloadNCache>d__41::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMultiDownloadNCacheU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31F5B6BE969D1618EC197467E3510111A027F15B ();
// 0x00000B03 System.Void LoadingScene_<MultiDownloadNCache>d__41::System.Collections.IEnumerator.Reset()
extern void U3CMultiDownloadNCacheU3Ed__41_System_Collections_IEnumerator_Reset_m3E05CA0B91A81741A9CF212A676B6522BDDD1610 ();
// 0x00000B04 System.Object LoadingScene_<MultiDownloadNCache>d__41::System.Collections.IEnumerator.get_Current()
extern void U3CMultiDownloadNCacheU3Ed__41_System_Collections_IEnumerator_get_Current_m3D9736F78A426EC0C3F93C3107D0A9370E231E42 ();
// 0x00000B05 System.Void LoadingScene_<_MultiDownloadDetail>d__44::.ctor(System.Int32)
extern void U3C_MultiDownloadDetailU3Ed__44__ctor_m019DDA02DA16A437ECA1F6856E4F19643BD67030 ();
// 0x00000B06 System.Void LoadingScene_<_MultiDownloadDetail>d__44::System.IDisposable.Dispose()
extern void U3C_MultiDownloadDetailU3Ed__44_System_IDisposable_Dispose_mC5E91B4A310507055BD97469BDF6AA476CC68404 ();
// 0x00000B07 System.Boolean LoadingScene_<_MultiDownloadDetail>d__44::MoveNext()
extern void U3C_MultiDownloadDetailU3Ed__44_MoveNext_m4C8A374669A355C394C5CACB79C603AFA78FF474 ();
// 0x00000B08 System.Object LoadingScene_<_MultiDownloadDetail>d__44::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_MultiDownloadDetailU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m409764D8787998FAD8385E4203A30C0D4AB27E23 ();
// 0x00000B09 System.Void LoadingScene_<_MultiDownloadDetail>d__44::System.Collections.IEnumerator.Reset()
extern void U3C_MultiDownloadDetailU3Ed__44_System_Collections_IEnumerator_Reset_m05D5AE375F25209B2D4201A8C1AF746122FAC751 ();
// 0x00000B0A System.Object LoadingScene_<_MultiDownloadDetail>d__44::System.Collections.IEnumerator.get_Current()
extern void U3C_MultiDownloadDetailU3Ed__44_System_Collections_IEnumerator_get_Current_mF269C59C2BF101E9E3EB62C376607C7B20314C82 ();
// 0x00000B0B System.Void LoadingScene_<>c__DisplayClass45_0::.ctor()
extern void U3CU3Ec__DisplayClass45_0__ctor_mDECE20CCFA8FC23EFB796D1312C30AD6D0CC62A1 ();
// 0x00000B0C System.Void LoadingScene_<>c__DisplayClass45_0::<MultiDownloadDetail>b__0(Proyecto26.ResponseHelper)
extern void U3CU3Ec__DisplayClass45_0_U3CMultiDownloadDetailU3Eb__0_m5DF33F48D36BC6FC4E06242B61966213F622EFDC ();
// 0x00000B0D System.Void LoadingScene_<>c__DisplayClass45_0::<MultiDownloadDetail>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass45_0_U3CMultiDownloadDetailU3Eb__1_mE7EE7AAC90ABF5C70C09356859EB03DD5B6BDB1A ();
// 0x00000B0E System.Void LoadingScene_<MultiDLDetailNCache>d__46::.ctor(System.Int32)
extern void U3CMultiDLDetailNCacheU3Ed__46__ctor_m8A873237FA78145DEC72E51F86A5DBA1463CF407 ();
// 0x00000B0F System.Void LoadingScene_<MultiDLDetailNCache>d__46::System.IDisposable.Dispose()
extern void U3CMultiDLDetailNCacheU3Ed__46_System_IDisposable_Dispose_mEBBE6A05457C8FC310A7D9B579E14C6F0CF9A4D1 ();
// 0x00000B10 System.Boolean LoadingScene_<MultiDLDetailNCache>d__46::MoveNext()
extern void U3CMultiDLDetailNCacheU3Ed__46_MoveNext_m2FA6207CB8B4079F86AD9CB8F76A241AC9E0FD5C ();
// 0x00000B11 System.Object LoadingScene_<MultiDLDetailNCache>d__46::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMultiDLDetailNCacheU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m80F7A1FE96CB8D7ABA483DE13C9A2D87046CCF6E ();
// 0x00000B12 System.Void LoadingScene_<MultiDLDetailNCache>d__46::System.Collections.IEnumerator.Reset()
extern void U3CMultiDLDetailNCacheU3Ed__46_System_Collections_IEnumerator_Reset_m868AD24FF17DB9914CA7831B5FFAE5606D428574 ();
// 0x00000B13 System.Object LoadingScene_<MultiDLDetailNCache>d__46::System.Collections.IEnumerator.get_Current()
extern void U3CMultiDLDetailNCacheU3Ed__46_System_Collections_IEnumerator_get_Current_m35B8F94AF1F5F53C345C6FBD8EB730B0B19E59D4 ();
// 0x00000B14 System.Void LoadingScene_<>c__DisplayClass47_0::.ctor()
extern void U3CU3Ec__DisplayClass47_0__ctor_mB814EEF1081A8DB4F456D1DDABC8C4C5F3230FB1 ();
// 0x00000B15 System.Void LoadingScene_<>c__DisplayClass47_0::<GetStyleIt>b__0(Proyecto26.ResponseHelper)
extern void U3CU3Ec__DisplayClass47_0_U3CGetStyleItU3Eb__0_mBDF26F8F10EEA7A5E640D1446EB3AE6EAB06F038 ();
// 0x00000B16 System.Void LoadingScene_<>c__DisplayClass47_0::<GetStyleIt>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass47_0_U3CGetStyleItU3Eb__1_mA8569472FF2D21ED0646888A18D9C29B7179A547 ();
// 0x00000B17 System.Void LoadingSceneSlowVersion_<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m5294883FC7F25760809E1E617D6AE9546B38CE83 ();
// 0x00000B18 System.Void LoadingSceneSlowVersion_<>c__DisplayClass13_0::<DownloadAPI>b__0(Proyecto26.ResponseHelper)
extern void U3CU3Ec__DisplayClass13_0_U3CDownloadAPIU3Eb__0_mF614ACE2F9D4EFE80C0F811FFE73BA97B8BD45A8 ();
// 0x00000B19 System.Void LoadingSceneSlowVersion_<>c__DisplayClass13_0::<DownloadAPI>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass13_0_U3CDownloadAPIU3Eb__1_m7F69D98FB8AB1D811DE498E4644FC0FA33351484 ();
// 0x00000B1A System.Void TextureScale_ThreadData::.ctor(System.Int32,System.Int32)
extern void ThreadData__ctor_m56C244B7BC2B67E10F8536EB8A0C40F16297593F ();
// 0x00000B1B System.Void UnityMainThread_<SpeedRenderQueue>d__7::.ctor(System.Int32)
extern void U3CSpeedRenderQueueU3Ed__7__ctor_m8017F8214987809133142C325682D8C80A093AB9 ();
// 0x00000B1C System.Void UnityMainThread_<SpeedRenderQueue>d__7::System.IDisposable.Dispose()
extern void U3CSpeedRenderQueueU3Ed__7_System_IDisposable_Dispose_m8B0B86936FB9242960CF9D490486148CF67199E0 ();
// 0x00000B1D System.Boolean UnityMainThread_<SpeedRenderQueue>d__7::MoveNext()
extern void U3CSpeedRenderQueueU3Ed__7_MoveNext_m6A12AB052670DEECB82EEA059279D79B829A663E ();
// 0x00000B1E System.Object UnityMainThread_<SpeedRenderQueue>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpeedRenderQueueU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07C0A5D45C36424BA91325FA69ACA75BCE428997 ();
// 0x00000B1F System.Void UnityMainThread_<SpeedRenderQueue>d__7::System.Collections.IEnumerator.Reset()
extern void U3CSpeedRenderQueueU3Ed__7_System_Collections_IEnumerator_Reset_m5E0225E9BFF3C2F974932F5C5A79185AA96EA0F3 ();
// 0x00000B20 System.Object UnityMainThread_<SpeedRenderQueue>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CSpeedRenderQueueU3Ed__7_System_Collections_IEnumerator_get_Current_mB8DF0A34865649B50C6EA8B6FAD7B1D2682F417E ();
// 0x00000B21 System.Void UnityThread_<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m28CF8EB09D166F91B445B0796711E3FF84609C6A ();
// 0x00000B22 System.Void UnityThread_<>c__DisplayClass12_0::<executeCoroutine>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CexecuteCoroutineU3Eb__0_m45FAA3A843575F357EA999B5C09D52D96A7E479B ();
// 0x00000B23 System.Void TestingPunch_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m063B6F62D61CAA29B3AB1AC3A819D6D18240F9F6 ();
// 0x00000B24 System.Void TestingPunch_<>c__DisplayClass4_0::<Update>b__2(System.Single)
extern void U3CU3Ec__DisplayClass4_0_U3CUpdateU3Eb__2_mE84C0A7EAF064241DAA6E98CF768F59DF1AB4B9E ();
// 0x00000B25 System.Void TestingPunch_<>c__DisplayClass4_1::.ctor()
extern void U3CU3Ec__DisplayClass4_1__ctor_mBB8D9837F5EB6C46A04F399F601533FEDE46756B ();
// 0x00000B26 System.Void TestingPunch_<>c__DisplayClass4_1::<Update>b__6()
extern void U3CU3Ec__DisplayClass4_1_U3CUpdateU3Eb__6_m76EC6A2E3975E8A2E6A031E8B6DA4C4088F2A55E ();
// 0x00000B27 System.Void TestingPunch_<>c__DisplayClass4_2::.ctor()
extern void U3CU3Ec__DisplayClass4_2__ctor_mAD3D1D58AD09A66A3CFCD2A27D3FDA00EAB9A6AB ();
// 0x00000B28 System.Void TestingPunch_<>c__DisplayClass4_2::<Update>b__8(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass4_2_U3CUpdateU3Eb__8_mB2A8A874B685A024E5058BEED756182B1B13ACD9 ();
// 0x00000B29 System.Void TestingPunch_<>c::.cctor()
extern void U3CU3Ec__cctor_m962DC1D18E6A690D9DAFF83EC393FE4F5ADE18C8 ();
// 0x00000B2A System.Void TestingPunch_<>c::.ctor()
extern void U3CU3Ec__ctor_m63711F73205AC47759F3870E385391FC49C03864 ();
// 0x00000B2B System.Void TestingPunch_<>c::<Update>b__4_1()
extern void U3CU3Ec_U3CUpdateU3Eb__4_1_mD2E518290C8AB4EDAC4F79EEF665A4A3430655E1 ();
// 0x00000B2C System.Void TestingPunch_<>c::<Update>b__4_4(System.Single)
extern void U3CU3Ec_U3CUpdateU3Eb__4_4_m8FA6F3DC6A54EFB3028C4BDCC66F69FC1EFC9A01 ();
// 0x00000B2D System.Void TestingPunch_<>c::<Update>b__4_5()
extern void U3CU3Ec_U3CUpdateU3Eb__4_5_m3952D2A76F4153183CE5361AD763AC2F13EB00A1 ();
// 0x00000B2E System.Void TestingPunch_<>c::<tweenStatically>b__5_0(System.Single)
extern void U3CU3Ec_U3CtweenStaticallyU3Eb__5_0_m9F5117E4B185369A4121FC6E4972F55B802C4F5D ();
// 0x00000B2F System.Void GeneralBasic_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mEBB010A7E42F38BBA0F6526F4898B3793016754A ();
// 0x00000B30 System.Void GeneralBasic_<>c__DisplayClass2_0::<advancedExamples>b__1()
extern void U3CU3Ec__DisplayClass2_0_U3CadvancedExamplesU3Eb__1_m46CD88EA10100768CC8BC78BD206FB34418B38BE ();
// 0x00000B31 System.Void GeneralBasic_<>c__DisplayClass2_0::<advancedExamples>b__2()
extern void U3CU3Ec__DisplayClass2_0_U3CadvancedExamplesU3Eb__2_mD8310B529F6CC844EFAE6B47D4EE69A074102CAC ();
// 0x00000B32 System.Void GeneralBasics2d_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mAE212C3B3074A3A63D0FAFB45B319458DC469379 ();
// 0x00000B33 System.Void GeneralBasics2d_<>c__DisplayClass4_0::<advancedExamples>b__1()
extern void U3CU3Ec__DisplayClass4_0_U3CadvancedExamplesU3Eb__1_m5443364BDD6519C00E41B679475989EC7BFA2A74 ();
// 0x00000B34 System.Void GeneralBasics2d_<>c__DisplayClass4_0::<advancedExamples>b__2()
extern void U3CU3Ec__DisplayClass4_0_U3CadvancedExamplesU3Eb__2_mC7171C077A8800FAA156D255B27FF78E214EAB2D ();
// 0x00000B35 System.Void GeneralCameraShake_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m6E2FDBEF2F7D210950B106AA2AC99FB7C23365FD ();
// 0x00000B36 System.Void GeneralCameraShake_<>c__DisplayClass4_0::<bigGuyJump>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CbigGuyJumpU3Eb__0_m83B800F5E384DA675243ACF6BE368207914E6568 ();
// 0x00000B37 System.Void GeneralCameraShake_<>c__DisplayClass4_0::<bigGuyJump>b__1()
extern void U3CU3Ec__DisplayClass4_0_U3CbigGuyJumpU3Eb__1_mA58D74BCD32E0467DF4939BF6A9BBEBEB98B6966 ();
// 0x00000B38 System.Void GeneralCameraShake_<>c__DisplayClass4_1::.ctor()
extern void U3CU3Ec__DisplayClass4_1__ctor_m90198E5152DCA6BBCF86C52303D7FF53541123ED ();
// 0x00000B39 System.Void GeneralCameraShake_<>c__DisplayClass4_1::<bigGuyJump>b__2(System.Single)
extern void U3CU3Ec__DisplayClass4_1_U3CbigGuyJumpU3Eb__2_mA7236E71AC8335842B32DED8F5A9BD467BE5E68E ();
// 0x00000B3A System.Void GeneralEasingTypes_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m99076BF1EC2BA4183686FD305FE9257A1FC108D8 ();
// 0x00000B3B System.Void GeneralEasingTypes_<>c__DisplayClass4_0::<demoEaseTypes>b__0(System.Single)
extern void U3CU3Ec__DisplayClass4_0_U3CdemoEaseTypesU3Eb__0_m8895F6DF6D44640415A16CB3166821A05AE7FB13 ();
// 0x00000B3C System.Void GeneralSimpleUI_<>c::.cctor()
extern void U3CU3Ec__cctor_m7E87C117C81452C517C83B26D383ED1C5747A990 ();
// 0x00000B3D System.Void GeneralSimpleUI_<>c::.ctor()
extern void U3CU3Ec__ctor_mE3568F988560BC01C21F210610E8221EC2136358 ();
// 0x00000B3E System.Void GeneralSimpleUI_<>c::<Start>b__1_1(System.Single)
extern void U3CU3Ec_U3CStartU3Eb__1_1_mBDA4A05B6CF190E80BD4AD54FE5A7B843122E047 ();
// 0x00000B3F System.Void GeneralUISpace_<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m1545255D46C77C14BEC0F46C3D149CFF14287CBA ();
// 0x00000B40 System.Void GeneralUISpace_<>c__DisplayClass15_0::<Start>b__0(System.Single)
extern void U3CU3Ec__DisplayClass15_0_U3CStartU3Eb__0_m7360056B580EBA5508B4AAF9B8FE47F13869D198 ();
// 0x00000B41 System.Void TestingZLegacy_NextFunc::.ctor(System.Object,System.IntPtr)
extern void NextFunc__ctor_mE79736745346FAD184C90EC9FFB942EDC6273AA5 ();
// 0x00000B42 System.Void TestingZLegacy_NextFunc::Invoke()
extern void NextFunc_Invoke_m5F15E6FAEEE04B0364D5861105F46B8CA1D15392 ();
// 0x00000B43 System.IAsyncResult TestingZLegacy_NextFunc::BeginInvoke(System.AsyncCallback,System.Object)
extern void NextFunc_BeginInvoke_m8FCE73C9609127DA3C01D221A5D74FAD4CA3EB0B ();
// 0x00000B44 System.Void TestingZLegacy_NextFunc::EndInvoke(System.IAsyncResult)
extern void NextFunc_EndInvoke_m5901C99FC6387B6FB299C1C8426F59A415E9C0F9 ();
// 0x00000B45 System.Void TestingZLegacy_<>c::.cctor()
extern void U3CU3Ec__cctor_mF9DA54E17EBF14B22D6DB252952767BE7FD49C9A ();
// 0x00000B46 System.Void TestingZLegacy_<>c::.ctor()
extern void U3CU3Ec__ctor_mE708DC3155F07189D4E3399F4EF7B6C622475119 ();
// 0x00000B47 System.Void TestingZLegacy_<>c::<cycleThroughExamples>b__20_0(System.Single)
extern void U3CU3Ec_U3CcycleThroughExamplesU3Eb__20_0_m84C10D9978F636C29F9ED1E04BA0742A68D1C98A ();
// 0x00000B48 System.Void TestingZLegacyExt_NextFunc::.ctor(System.Object,System.IntPtr)
extern void NextFunc__ctor_mCCFCFD0AB879EBBB46104ADFF9A9024018DCAFD4 ();
// 0x00000B49 System.Void TestingZLegacyExt_NextFunc::Invoke()
extern void NextFunc_Invoke_m84CD7F169D5B749CFE2ED930F9CF0E7B9D41B75F ();
// 0x00000B4A System.IAsyncResult TestingZLegacyExt_NextFunc::BeginInvoke(System.AsyncCallback,System.Object)
extern void NextFunc_BeginInvoke_mCEE4B7EB7615D52E726E9DB1BF918E0ED3B46FBC ();
// 0x00000B4B System.Void TestingZLegacyExt_NextFunc::EndInvoke(System.IAsyncResult)
extern void NextFunc_EndInvoke_mE35B84DD8B0DD2D1124F28CB2B5729F070AE3FEC ();
// 0x00000B4C System.Void TestingZLegacyExt_<>c::.cctor()
extern void U3CU3Ec__cctor_m18DE319729D6EBE31B6DBEEF6AC68B727FEAFC58 ();
// 0x00000B4D System.Void TestingZLegacyExt_<>c::.ctor()
extern void U3CU3Ec__ctor_mF8076F513CB75419CD4AAF5D2CF96EA750C7C64D ();
// 0x00000B4E System.Void TestingZLegacyExt_<>c::<cycleThroughExamples>b__20_0(System.Single)
extern void U3CU3Ec_U3CcycleThroughExamplesU3Eb__20_0_m8A0627F6E7351B2DF8151D125E1E5045000F449D ();
// 0x00000B4F System.Void LTDescr_EaseTypeDelegate::.ctor(System.Object,System.IntPtr)
extern void EaseTypeDelegate__ctor_m479FD2E5E95095E811CD43C107F9F7BEDA5C1722 ();
// 0x00000B50 UnityEngine.Vector3 LTDescr_EaseTypeDelegate::Invoke()
extern void EaseTypeDelegate_Invoke_m6C4DD4B41DD183CD84736AB68488797939157559 ();
// 0x00000B51 System.IAsyncResult LTDescr_EaseTypeDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void EaseTypeDelegate_BeginInvoke_mC4A8F044C73F14833A8F0892A1B1D7EFA5719775 ();
// 0x00000B52 UnityEngine.Vector3 LTDescr_EaseTypeDelegate::EndInvoke(System.IAsyncResult)
extern void EaseTypeDelegate_EndInvoke_mBBC06324172B4D605212E2BF2D7568C91D55371D ();
// 0x00000B53 System.Void LTDescr_ActionMethodDelegate::.ctor(System.Object,System.IntPtr)
extern void ActionMethodDelegate__ctor_m2C8565D6C66397F9327D19C9D47C45587110BC5F ();
// 0x00000B54 System.Void LTDescr_ActionMethodDelegate::Invoke()
extern void ActionMethodDelegate_Invoke_mD731C08D9044E02DE9276EB7269BCA0F540950EA ();
// 0x00000B55 System.IAsyncResult LTDescr_ActionMethodDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void ActionMethodDelegate_BeginInvoke_mF15334FB75A22146CE827C20402393D61B7203C1 ();
// 0x00000B56 System.Void LTDescr_ActionMethodDelegate::EndInvoke(System.IAsyncResult)
extern void ActionMethodDelegate_EndInvoke_m164BF9BEF0C0CB55BC1AC688BECC75811982F9F6 ();
// 0x00000B57 System.Void LTDescr_<>c::.cctor()
extern void U3CU3Ec__cctor_m7B400CF0B15F5A6BB8EDD68761B868209D666561 ();
// 0x00000B58 System.Void LTDescr_<>c::.ctor()
extern void U3CU3Ec__ctor_m3E01BF844F661459DBA93A1A1A2052BB66D8278C ();
// 0x00000B59 System.Void LTDescr_<>c::<setCallback>b__113_0()
extern void U3CU3Ec_U3CsetCallbackU3Eb__113_0_mBAF1F42A0092D36D9EA8B302EAD7E4B64484A698 ();
// 0x00000B5A System.Void LTDescr_<>c::<setValue3>b__114_0()
extern void U3CU3Ec_U3CsetValue3U3Eb__114_0_m2BEB42E3CC61F73F63334E99081144AC817C3A6A ();
// 0x00000B5B System.Void LeanTester_<timeoutCheck>d__2::.ctor(System.Int32)
extern void U3CtimeoutCheckU3Ed__2__ctor_m86660954A201D3F4042917DED992CF67F2967E40 ();
// 0x00000B5C System.Void LeanTester_<timeoutCheck>d__2::System.IDisposable.Dispose()
extern void U3CtimeoutCheckU3Ed__2_System_IDisposable_Dispose_mE54F8B9D9C634E727ADEB82A507BCC5D2D7F6A58 ();
// 0x00000B5D System.Boolean LeanTester_<timeoutCheck>d__2::MoveNext()
extern void U3CtimeoutCheckU3Ed__2_MoveNext_m81A53EB7B0E977685E46FE5D64A808B557EC5EC3 ();
// 0x00000B5E System.Object LeanTester_<timeoutCheck>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtimeoutCheckU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7B28917D1D087CC4FFE00C923F13DE0E3C80DBFC ();
// 0x00000B5F System.Void LeanTester_<timeoutCheck>d__2::System.Collections.IEnumerator.Reset()
extern void U3CtimeoutCheckU3Ed__2_System_Collections_IEnumerator_Reset_m4A3583E3E830B3116E7DC2C562734E6153CAB4EF ();
// 0x00000B60 System.Object LeanTester_<timeoutCheck>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CtimeoutCheckU3Ed__2_System_Collections_IEnumerator_get_Current_m21261FBD3907579D1BC3B9C4B58CB01EA868DCB9 ();
// 0x00000B61 System.Void LeanTween_<>c__DisplayClass193_0::.ctor()
extern void U3CU3Ec__DisplayClass193_0__ctor_mC0B0125F1AA00B0D67E5471AC4723D7C4DDF745A ();
// 0x00000B62 System.Void LeanTween_<>c__DisplayClass193_0::<followDamp>b__0()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__0_m1C1C28A5106180AA479E783252142933B5B22CB3 ();
// 0x00000B63 System.Void LeanTween_<>c__DisplayClass193_0::<followDamp>b__1()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__1_mA9FBC068978683FE7F41AB0DE6764B37E8F12FCB ();
// 0x00000B64 System.Void LeanTween_<>c__DisplayClass193_0::<followDamp>b__2()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__2_m32EDEE7568C90E8D332BAF204B827BB696F8915F ();
// 0x00000B65 System.Void LeanTween_<>c__DisplayClass193_0::<followDamp>b__3()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__3_mAB9D969B192F8C7F5CD2D8CEFC40A0AB9A1E569C ();
// 0x00000B66 System.Void LeanTween_<>c__DisplayClass193_0::<followDamp>b__4()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__4_mD098FD9EE16741678387663B5E5A20AACDB3C461 ();
// 0x00000B67 System.Void LeanTween_<>c__DisplayClass193_0::<followDamp>b__5()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__5_m2D34FBF1D0050BC36587D4AC4AB768CBBAD16653 ();
// 0x00000B68 System.Void LeanTween_<>c__DisplayClass193_0::<followDamp>b__6()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__6_m4F2B350D8676AF4E40A9A522AB546E7D64CB5A4F ();
// 0x00000B69 System.Void LeanTween_<>c__DisplayClass193_0::<followDamp>b__7()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__7_m4794A3DFBD9F60DDB39A1D79B3261D224192C6EA ();
// 0x00000B6A System.Void LeanTween_<>c__DisplayClass193_0::<followDamp>b__8()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__8_m8F118FB039CC4F97D3B5099511C0ACF4A16794E1 ();
// 0x00000B6B System.Void LeanTween_<>c__DisplayClass193_0::<followDamp>b__9()
extern void U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__9_m1E35B42D88BA7A620623D40B97FE0A034E6D51DF ();
// 0x00000B6C System.Void LeanTween_<>c__DisplayClass194_0::.ctor()
extern void U3CU3Ec__DisplayClass194_0__ctor_mDCBB6E03EFED25D38C1645524A9CFB06906D64A0 ();
// 0x00000B6D System.Void LeanTween_<>c__DisplayClass194_0::<followSpring>b__0()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__0_m0DF3AC96834FE13B309965159F303CF6519EEF50 ();
// 0x00000B6E System.Void LeanTween_<>c__DisplayClass194_0::<followSpring>b__1()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__1_mC60E4D28F90F14EC7259E5FC1F36B0C7BA0587A4 ();
// 0x00000B6F System.Void LeanTween_<>c__DisplayClass194_0::<followSpring>b__2()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__2_mAC5BC0C40BD4FF4F7A321BB0E5C7F9759AC85F0C ();
// 0x00000B70 System.Void LeanTween_<>c__DisplayClass194_0::<followSpring>b__3()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__3_m7C23E5BA1FAF357B1D25B4DC5B0F587074A36440 ();
// 0x00000B71 System.Void LeanTween_<>c__DisplayClass194_0::<followSpring>b__4()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__4_m27AE2F3933DC68B0444E94E4C6C86ED6C1BAE149 ();
// 0x00000B72 System.Void LeanTween_<>c__DisplayClass194_0::<followSpring>b__5()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__5_m95EB2A430E734B7AA2726020BB238DF0374206B6 ();
// 0x00000B73 System.Void LeanTween_<>c__DisplayClass194_0::<followSpring>b__6()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__6_m15ED9AC2709C71E60DA3125DF3A16E7B8B94C3AD ();
// 0x00000B74 System.Void LeanTween_<>c__DisplayClass194_0::<followSpring>b__7()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__7_m4C0A7E27C26DB6DCFBEFE3C357B8A26CC65474D4 ();
// 0x00000B75 System.Void LeanTween_<>c__DisplayClass194_0::<followSpring>b__8()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__8_m763826D5C9CF0FF3D93E27C117BF47A4A2C57BD6 ();
// 0x00000B76 System.Void LeanTween_<>c__DisplayClass194_0::<followSpring>b__9()
extern void U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__9_mAB337657583472749A5443EF146F301CF2391AB0 ();
// 0x00000B77 System.Void LeanTween_<>c__DisplayClass195_0::.ctor()
extern void U3CU3Ec__DisplayClass195_0__ctor_mE00CB764397209766E590DF5EFEB52053DC0AE26 ();
// 0x00000B78 System.Void LeanTween_<>c__DisplayClass195_0::<followBounceOut>b__0()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__0_mAC6C7B8F8DF55A577CE8781796BE15239B566E89 ();
// 0x00000B79 System.Void LeanTween_<>c__DisplayClass195_0::<followBounceOut>b__1()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__1_m5184ABA1AB3627B2DDED0C74E2282344378AF3B8 ();
// 0x00000B7A System.Void LeanTween_<>c__DisplayClass195_0::<followBounceOut>b__2()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__2_m34A0E6745FC7434BB5A27B25A097C58784226F3C ();
// 0x00000B7B System.Void LeanTween_<>c__DisplayClass195_0::<followBounceOut>b__3()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__3_m2D76D667CE87B27FBCAECC55425C3207533F6A75 ();
// 0x00000B7C System.Void LeanTween_<>c__DisplayClass195_0::<followBounceOut>b__4()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__4_m205F4F7C5D9A2B137BCB29491B95C3828F378B07 ();
// 0x00000B7D System.Void LeanTween_<>c__DisplayClass195_0::<followBounceOut>b__5()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__5_mCDC95E138FF86491A853A74BF172BEEB38BECA09 ();
// 0x00000B7E System.Void LeanTween_<>c__DisplayClass195_0::<followBounceOut>b__6()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__6_mDD9833A53793BAE5B0A03C37BA4EF1F60E9930FF ();
// 0x00000B7F System.Void LeanTween_<>c__DisplayClass195_0::<followBounceOut>b__7()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__7_m74731F175AF6639677E96328854558399D9FE7CF ();
// 0x00000B80 System.Void LeanTween_<>c__DisplayClass195_0::<followBounceOut>b__8()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__8_m8B743DC6870594CE43D0325E2D884FB0CAF98E20 ();
// 0x00000B81 System.Void LeanTween_<>c__DisplayClass195_0::<followBounceOut>b__9()
extern void U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__9_m466A3211A248D8995FB3AE22D016F1EFD623A8E6 ();
// 0x00000B82 System.Void LeanTween_<>c__DisplayClass196_0::.ctor()
extern void U3CU3Ec__DisplayClass196_0__ctor_m929D534AE634FB58A7C0ECDFB23A1D78351F6F3F ();
// 0x00000B83 System.Void LeanTween_<>c__DisplayClass196_0::<followLinear>b__0()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__0_mA2E1AF83CA2338CA95BA84B64C912222C7A3D9E6 ();
// 0x00000B84 System.Void LeanTween_<>c__DisplayClass196_0::<followLinear>b__1()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__1_m23CB51E9EF35556F273C3C0B62C5C06D8A27273E ();
// 0x00000B85 System.Void LeanTween_<>c__DisplayClass196_0::<followLinear>b__2()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__2_m54430A940C2FEE40A54C3AE14482E1DD43CBF103 ();
// 0x00000B86 System.Void LeanTween_<>c__DisplayClass196_0::<followLinear>b__3()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__3_mE6D8BB8EFB0E336E59430CA8A2E029CEC51136C8 ();
// 0x00000B87 System.Void LeanTween_<>c__DisplayClass196_0::<followLinear>b__4()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__4_mD6D1CF2BB54CAF51FFF102ECDAE4BE98C25FE3E0 ();
// 0x00000B88 System.Void LeanTween_<>c__DisplayClass196_0::<followLinear>b__5()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__5_m4244AA15E1AFBF399BA4B03799FDD52F1893D5BA ();
// 0x00000B89 System.Void LeanTween_<>c__DisplayClass196_0::<followLinear>b__6()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__6_mB26E4D96497F6A2BBB615D69D4966E6B8FE8BFF7 ();
// 0x00000B8A System.Void LeanTween_<>c__DisplayClass196_0::<followLinear>b__7()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__7_mCC89DC9F63965ABBC1705F61D48B28D83959F20E ();
// 0x00000B8B System.Void LeanTween_<>c__DisplayClass196_0::<followLinear>b__8()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__8_mD29A47A9681480C45508BE22F8600B447930FA58 ();
// 0x00000B8C System.Void LeanTween_<>c__DisplayClass196_0::<followLinear>b__9()
extern void U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__9_m6F5ACA5A264C5CF1B4A20164725E5103856A9880 ();
// 0x00000B8D System.Void CFX2_Demo_<RandomSpawnsCoroutine>d__14::.ctor(System.Int32)
extern void U3CRandomSpawnsCoroutineU3Ed__14__ctor_m55FEE61BC4D871082A30F5B17961CAB2AB8F95E3 ();
// 0x00000B8E System.Void CFX2_Demo_<RandomSpawnsCoroutine>d__14::System.IDisposable.Dispose()
extern void U3CRandomSpawnsCoroutineU3Ed__14_System_IDisposable_Dispose_m0818C483E374908B7D1CE5F3E3853BBC0D029159 ();
// 0x00000B8F System.Boolean CFX2_Demo_<RandomSpawnsCoroutine>d__14::MoveNext()
extern void U3CRandomSpawnsCoroutineU3Ed__14_MoveNext_mBB5F29DB2B8D24DB134988AD74F02F1654FC90E0 ();
// 0x00000B90 System.Object CFX2_Demo_<RandomSpawnsCoroutine>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRandomSpawnsCoroutineU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED44A0393A99F4D3A3332636A8A61D4D2AC57D24 ();
// 0x00000B91 System.Void CFX2_Demo_<RandomSpawnsCoroutine>d__14::System.Collections.IEnumerator.Reset()
extern void U3CRandomSpawnsCoroutineU3Ed__14_System_Collections_IEnumerator_Reset_mB91900A7F499A5840DA1EC30D52644A9D0E97F2A ();
// 0x00000B92 System.Object CFX2_Demo_<RandomSpawnsCoroutine>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CRandomSpawnsCoroutineU3Ed__14_System_Collections_IEnumerator_get_Current_m9CB94ED98005F6D2CFC6F7030B18A15300819B13 ();
// 0x00000B93 System.Void CFX_AutoDestructShuriken_<CheckIfAlive>d__2::.ctor(System.Int32)
extern void U3CCheckIfAliveU3Ed__2__ctor_m59518A9AC72B3B2BEAAB552A8D9222A6956BE4B5 ();
// 0x00000B94 System.Void CFX_AutoDestructShuriken_<CheckIfAlive>d__2::System.IDisposable.Dispose()
extern void U3CCheckIfAliveU3Ed__2_System_IDisposable_Dispose_mE46F321812B5249C5965D8099CD74DF5757FC621 ();
// 0x00000B95 System.Boolean CFX_AutoDestructShuriken_<CheckIfAlive>d__2::MoveNext()
extern void U3CCheckIfAliveU3Ed__2_MoveNext_mBC913097EB4077ABF6B7E9C42FAE4486BC5F6B7D ();
// 0x00000B96 System.Object CFX_AutoDestructShuriken_<CheckIfAlive>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckIfAliveU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFAC4D31487A4AB3D719CB76D53AC10D7EF07DFE0 ();
// 0x00000B97 System.Void CFX_AutoDestructShuriken_<CheckIfAlive>d__2::System.Collections.IEnumerator.Reset()
extern void U3CCheckIfAliveU3Ed__2_System_Collections_IEnumerator_Reset_mFF789B70B62AB28C2A4B14C2882D0C28DC907DC6 ();
// 0x00000B98 System.Object CFX_AutoDestructShuriken_<CheckIfAlive>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CCheckIfAliveU3Ed__2_System_Collections_IEnumerator_get_Current_mD55424D90545BBD976F87E26A5E27EE9858AC4FC ();
// 0x00000B99 System.Void CFX_ShurikenThreadFix_<WaitFrame>d__2::.ctor(System.Int32)
extern void U3CWaitFrameU3Ed__2__ctor_m30C38499BA579212104535A61F69619BC2F70448 ();
// 0x00000B9A System.Void CFX_ShurikenThreadFix_<WaitFrame>d__2::System.IDisposable.Dispose()
extern void U3CWaitFrameU3Ed__2_System_IDisposable_Dispose_m076213034FB516C05B9A010F5EE8F36DA6BC2044 ();
// 0x00000B9B System.Boolean CFX_ShurikenThreadFix_<WaitFrame>d__2::MoveNext()
extern void U3CWaitFrameU3Ed__2_MoveNext_m6573E68F6898234B5F7BE5067C9D6795CA642309 ();
// 0x00000B9C System.Object CFX_ShurikenThreadFix_<WaitFrame>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitFrameU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDEA4815E620E29509EC3D76A6BF6912C11F834DA ();
// 0x00000B9D System.Void CFX_ShurikenThreadFix_<WaitFrame>d__2::System.Collections.IEnumerator.Reset()
extern void U3CWaitFrameU3Ed__2_System_Collections_IEnumerator_Reset_mDD7A1546AF81B04D68D1BF3D526A741F57A28D14 ();
// 0x00000B9E System.Object CFX_ShurikenThreadFix_<WaitFrame>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CWaitFrameU3Ed__2_System_Collections_IEnumerator_get_Current_m6184F70547D0221260D123CB3CACE6E321D51AD7 ();
// 0x00000B9F System.Void MainScript_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m0206C7B67398673472BB1EDEADFCDE7E7B83DC7E ();
// 0x00000BA0 RSG.IPromise`1<Models.Todo[]> MainScript_<>c__DisplayClass3_0::<Get>b__0(Models.Post[])
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__0_mE6A5E64E647BEA847980CA1A647D8F4DD7538717 ();
// 0x00000BA1 RSG.IPromise`1<Models.User[]> MainScript_<>c__DisplayClass3_0::<Get>b__1(Models.Todo[])
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__1_mF3A5A6BE10274FE2B6B92E2E3A37375350FB7C20 ();
// 0x00000BA2 RSG.IPromise`1<Models.Photo[]> MainScript_<>c__DisplayClass3_0::<Get>b__2(Models.User[])
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__2_mFFB1B636802C122D21246C6C519213869AC16F14 ();
// 0x00000BA3 System.Void MainScript_<>c__DisplayClass3_0::<Get>b__3(Models.Photo[])
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__3_m6674519BC0EE5BA68033C42DC0EDBCF007BC95D5 ();
// 0x00000BA4 System.Void MainScript_<>c__DisplayClass3_0::<Get>b__4(System.Exception)
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__4_m88B66BBD68D3B0C17E42D1A7783D4C70E8CF4B57 ();
// 0x00000BA5 System.Void MainScript_<>c::.cctor()
extern void U3CU3Ec__cctor_m5C0BC870EF559E2799B38AEBD2A18923F1C6495E ();
// 0x00000BA6 System.Void MainScript_<>c::.ctor()
extern void U3CU3Ec__ctor_m0232508BEA690347C7422E2637E635EAF983FBD0 ();
// 0x00000BA7 System.Void MainScript_<>c::<Put>b__5_1(Proyecto26.RequestException,System.Int32)
extern void U3CU3Ec_U3CPutU3Eb__5_1_m4E9DD6570C43418466C7A2D49DE1B614E8711511 ();
// 0x00000BA8 System.Void CanvasConfig_<StartCountDown>d__23::.ctor(System.Int32)
extern void U3CStartCountDownU3Ed__23__ctor_m53428FFF153B82D94C4EBFEC68BEC79EC85CA1D8 ();
// 0x00000BA9 System.Void CanvasConfig_<StartCountDown>d__23::System.IDisposable.Dispose()
extern void U3CStartCountDownU3Ed__23_System_IDisposable_Dispose_m80F80AA77C089EDFA3302256BE48BCEBFB758EB1 ();
// 0x00000BAA System.Boolean CanvasConfig_<StartCountDown>d__23::MoveNext()
extern void U3CStartCountDownU3Ed__23_MoveNext_m53760B6B2FEC89611C2057D3E1E08F352C480904 ();
// 0x00000BAB System.Object CanvasConfig_<StartCountDown>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartCountDownU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6FEBDC5780BE8743E59D57A44C3029A79A0F9EBC ();
// 0x00000BAC System.Void CanvasConfig_<StartCountDown>d__23::System.Collections.IEnumerator.Reset()
extern void U3CStartCountDownU3Ed__23_System_Collections_IEnumerator_Reset_mDACEDF419849A97D120B2A0D73E793F5738906C9 ();
// 0x00000BAD System.Object CanvasConfig_<StartCountDown>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CStartCountDownU3Ed__23_System_Collections_IEnumerator_get_Current_m24B48535C8C4063592B057EBEF6C028FB78AA7C7 ();
// 0x00000BAE System.Void CanvasConfig_<StartIdlingTimer>d__25::.ctor(System.Int32)
extern void U3CStartIdlingTimerU3Ed__25__ctor_mDBA1FDE4E03092947D4F1BE9784465E9BFFF1DB3 ();
// 0x00000BAF System.Void CanvasConfig_<StartIdlingTimer>d__25::System.IDisposable.Dispose()
extern void U3CStartIdlingTimerU3Ed__25_System_IDisposable_Dispose_m1206F13B74AF3EB667A7830E25A0776D31A44127 ();
// 0x00000BB0 System.Boolean CanvasConfig_<StartIdlingTimer>d__25::MoveNext()
extern void U3CStartIdlingTimerU3Ed__25_MoveNext_mC9240CA3827227739B8EAE1E967ACF10B89B42D9 ();
// 0x00000BB1 System.Object CanvasConfig_<StartIdlingTimer>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartIdlingTimerU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC6D4B2AC57D4533DA8B58827966483AA6372485E ();
// 0x00000BB2 System.Void CanvasConfig_<StartIdlingTimer>d__25::System.Collections.IEnumerator.Reset()
extern void U3CStartIdlingTimerU3Ed__25_System_Collections_IEnumerator_Reset_m64667A0F7C83F7479EA4762E5FF597BE184FF25B ();
// 0x00000BB3 System.Object CanvasConfig_<StartIdlingTimer>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CStartIdlingTimerU3Ed__25_System_Collections_IEnumerator_get_Current_mFA7BFD6947904A7AA24681D01CEDA1E48648083D ();
// 0x00000BB4 System.Void CanvasConfig_<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_mB8DA4BFD5500C7CFF7CF35996AC16A933319636A ();
// 0x00000BB5 System.Void CanvasConfig_<>c__DisplayClass29_0::<TapEffect>b__0()
extern void U3CU3Ec__DisplayClass29_0_U3CTapEffectU3Eb__0_m7513749B6158B0B9BF43A17C004E3CF606105752 ();
// 0x00000BB6 System.Void ImageConfig_<LoadTextures>d__22::.ctor(System.Int32)
extern void U3CLoadTexturesU3Ed__22__ctor_mB85A94C8B81FCD9DD459B439F2FEA25B6AD44824 ();
// 0x00000BB7 System.Void ImageConfig_<LoadTextures>d__22::System.IDisposable.Dispose()
extern void U3CLoadTexturesU3Ed__22_System_IDisposable_Dispose_m6F2C0169FC3ACE2B0107A8326E0BD21FC21B5740 ();
// 0x00000BB8 System.Boolean ImageConfig_<LoadTextures>d__22::MoveNext()
extern void U3CLoadTexturesU3Ed__22_MoveNext_m1AE68CAC44C621C9471D68415C3D08A9EB66BD72 ();
// 0x00000BB9 System.Object ImageConfig_<LoadTextures>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadTexturesU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m699C757DCEEFB6FBF7C001A821CA38C6D8FF0D50 ();
// 0x00000BBA System.Void ImageConfig_<LoadTextures>d__22::System.Collections.IEnumerator.Reset()
extern void U3CLoadTexturesU3Ed__22_System_Collections_IEnumerator_Reset_mD9EA48A421485076A823D3F48EB7FA91D7E7E812 ();
// 0x00000BBB System.Object ImageConfig_<LoadTextures>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CLoadTexturesU3Ed__22_System_Collections_IEnumerator_get_Current_mC146E0B530E5443C43E4539DFC48980F18F108FF ();
// 0x00000BBC System.Void ImageConfig_<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m878BEEEFBCEFF11C32120447FEA3DD0F0DCDD829 ();
// 0x00000BBD System.Void ImageConfig_<>c__DisplayClass26_0::<SpawnTexHorizontal>b__0()
extern void U3CU3Ec__DisplayClass26_0_U3CSpawnTexHorizontalU3Eb__0_m13E24D93312D053EFA2F904C443F0B5375C05619 ();
// 0x00000BBE System.Void ImageConfig_<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_mF04855A6C5F4B000108C4C3F0C6929955A0999F6 ();
// 0x00000BBF System.Void ImageConfig_<>c__DisplayClass28_0::<SmartDetect>b__0()
extern void U3CU3Ec__DisplayClass28_0_U3CSmartDetectU3Eb__0_m2183DBE8700B0E0A165A91ABE0264F2AEBEAC7C8 ();
// 0x00000BC0 System.Void ImageConfig_<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_m30EA17AC54E490863B15D1FB4CA4D203D2C64B9D ();
// 0x00000BC1 System.Void ImageConfig_<>c__DisplayClass30_1::.ctor()
extern void U3CU3Ec__DisplayClass30_1__ctor_m0B7DDBDB73163B573F2DD416BAA85931D3BD8111 ();
// 0x00000BC2 System.Void ImageConfig_<>c__DisplayClass30_1::<DownloadingJSONDetailProduct>b__0(Proyecto26.ResponseHelper)
extern void U3CU3Ec__DisplayClass30_1_U3CDownloadingJSONDetailProductU3Eb__0_m982CA715C610B2389B68FD8B6840924BB584F9CD ();
// 0x00000BC3 System.Void ImageConfig_<>c__DisplayClass30_1::<DownloadingJSONDetailProduct>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass30_1_U3CDownloadingJSONDetailProductU3Eb__1_m52015A6DFC789894FD3AC97EB55286ADA019B289 ();
// 0x00000BC4 System.Void ImageConfig_<SetingSPVLImage>d__32::.ctor(System.Int32)
extern void U3CSetingSPVLImageU3Ed__32__ctor_mE4E956152ED354968C090102675020E0CD629549 ();
// 0x00000BC5 System.Void ImageConfig_<SetingSPVLImage>d__32::System.IDisposable.Dispose()
extern void U3CSetingSPVLImageU3Ed__32_System_IDisposable_Dispose_mB3B15C31CA1C850848A26D56B54124CBC6D5B814 ();
// 0x00000BC6 System.Boolean ImageConfig_<SetingSPVLImage>d__32::MoveNext()
extern void U3CSetingSPVLImageU3Ed__32_MoveNext_mBCBB716B4383890183E91C4D8418ECD097DFE9BF ();
// 0x00000BC7 System.Object ImageConfig_<SetingSPVLImage>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetingSPVLImageU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCFE3DB767E4439A57F02B240F3BF1637B26D8963 ();
// 0x00000BC8 System.Void ImageConfig_<SetingSPVLImage>d__32::System.Collections.IEnumerator.Reset()
extern void U3CSetingSPVLImageU3Ed__32_System_Collections_IEnumerator_Reset_mB93EC4685714FF90B1C543811CC40A7CC6674C83 ();
// 0x00000BC9 System.Object ImageConfig_<SetingSPVLImage>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CSetingSPVLImageU3Ed__32_System_Collections_IEnumerator_get_Current_m3CD4F9F9E124CDBA00AABB2069E10DAB8FD94040 ();
// 0x00000BCA System.Void ImageConfig_<DownloadingSPVLimage>d__34::.ctor(System.Int32)
extern void U3CDownloadingSPVLimageU3Ed__34__ctor_mA729E0CDAE218B958F6BEABA645E60EB4A18033B ();
// 0x00000BCB System.Void ImageConfig_<DownloadingSPVLimage>d__34::System.IDisposable.Dispose()
extern void U3CDownloadingSPVLimageU3Ed__34_System_IDisposable_Dispose_m4DE11CA478C8DEB152A6A31019201FEC756EA377 ();
// 0x00000BCC System.Boolean ImageConfig_<DownloadingSPVLimage>d__34::MoveNext()
extern void U3CDownloadingSPVLimageU3Ed__34_MoveNext_mE2740FC5E35D351F93D6F5029543DDF005471F4C ();
// 0x00000BCD System.Object ImageConfig_<DownloadingSPVLimage>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadingSPVLimageU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB45BE80163A11C8F304A193C2062F4B848183C8B ();
// 0x00000BCE System.Void ImageConfig_<DownloadingSPVLimage>d__34::System.Collections.IEnumerator.Reset()
extern void U3CDownloadingSPVLimageU3Ed__34_System_Collections_IEnumerator_Reset_m5581652C40443A42BF1C862A891A17648F293BBC ();
// 0x00000BCF System.Object ImageConfig_<DownloadingSPVLimage>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadingSPVLimageU3Ed__34_System_Collections_IEnumerator_get_Current_m634EE28D6E3D129BC66F6B33D5CAF125FFE15D58 ();
// 0x00000BD0 System.Void ImageConfig_<DownloadingSPVSingle>d__39::.ctor(System.Int32)
extern void U3CDownloadingSPVSingleU3Ed__39__ctor_m9BA9EFCAC1B27CCEBE225021FB91751471E2BF58 ();
// 0x00000BD1 System.Void ImageConfig_<DownloadingSPVSingle>d__39::System.IDisposable.Dispose()
extern void U3CDownloadingSPVSingleU3Ed__39_System_IDisposable_Dispose_m3BFAB3DFC3AA9625A9C84FC01A8988FCB7883148 ();
// 0x00000BD2 System.Boolean ImageConfig_<DownloadingSPVSingle>d__39::MoveNext()
extern void U3CDownloadingSPVSingleU3Ed__39_MoveNext_mAA0D4EB7DEE65088F02EFCD9FBF6ECCFF6DC8802 ();
// 0x00000BD3 System.Object ImageConfig_<DownloadingSPVSingle>d__39::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadingSPVSingleU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCE3A9AA4835372259B50112E3018D36E4D16649F ();
// 0x00000BD4 System.Void ImageConfig_<DownloadingSPVSingle>d__39::System.Collections.IEnumerator.Reset()
extern void U3CDownloadingSPVSingleU3Ed__39_System_Collections_IEnumerator_Reset_m002B4851069A5194EE9286BCE81492DF5E8F5B59 ();
// 0x00000BD5 System.Object ImageConfig_<DownloadingSPVSingle>d__39::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadingSPVSingleU3Ed__39_System_Collections_IEnumerator_get_Current_m74BBB54C0ACA2C11C9ED325EBE884B50433D24E1 ();
// 0x00000BD6 System.Void ImageConfig_<>c__DisplayClass43_0::.ctor()
extern void U3CU3Ec__DisplayClass43_0__ctor_m60107EF7499E9675E293F8B34CCC48A8B5908CA0 ();
// 0x00000BD7 System.Void ImageConfig_<>c__DisplayClass43_0::<DownloadingJSON>b__0(Proyecto26.ResponseHelper)
extern void U3CU3Ec__DisplayClass43_0_U3CDownloadingJSONU3Eb__0_mF5480EA3A1DF76D32B5E0371DB6348938F8CCC12 ();
// 0x00000BD8 System.Void ImageConfig_<>c__DisplayClass43_0::<DownloadingJSON>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass43_0_U3CDownloadingJSONU3Eb__1_m8CF5A09EA5F8FA02A523B065B5193CF222F82BA0 ();
// 0x00000BD9 System.Void ImageConfig_<>c__DisplayClass45_0::.ctor()
extern void U3CU3Ec__DisplayClass45_0__ctor_mC8199F22672FECE4166348E993B9167553B1EBF5 ();
// 0x00000BDA System.Void ImageConfig_<>c__DisplayClass45_0::<WaitLoader>b__0()
extern void U3CU3Ec__DisplayClass45_0_U3CWaitLoaderU3Eb__0_mAC13FB4BAB11063394358A08199ED0E856E9CF3E ();
// 0x00000BDB System.Void ImageConfig_<>c__DisplayClass48_0::.ctor()
extern void U3CU3Ec__DisplayClass48_0__ctor_mED207FF44310C2071072DAA652ED7AECF5F64C68 ();
// 0x00000BDC System.Void ImageConfig_<>c__DisplayClass48_0::<SetPos>b__0()
extern void U3CU3Ec__DisplayClass48_0_U3CSetPosU3Eb__0_m5278C9D76DD645E6C0F30D4366E96A28AEC9ACF3 ();
// 0x00000BDD System.Void ImageConfig_<>c__DisplayClass48_0::<SetPos>b__1()
extern void U3CU3Ec__DisplayClass48_0_U3CSetPosU3Eb__1_m8752EDD1CD8D514F1A9AE18E5644FEDC9920A9BB ();
// 0x00000BDE System.Void ImageConfig_<>c__DisplayClass48_0::<SetPos>b__2()
extern void U3CU3Ec__DisplayClass48_0_U3CSetPosU3Eb__2_mF860FA140884D0583B95FB986EE337E70B252B5A ();
// 0x00000BDF System.Void ImageConfig_<>c__DisplayClass48_0::<SetPos>b__3()
extern void U3CU3Ec__DisplayClass48_0_U3CSetPosU3Eb__3_mA59965562045C52BEB4820312C4FF3B727DB94E3 ();
// 0x00000BE0 System.Void ImageConfig_<>c__DisplayClass52_0::.ctor()
extern void U3CU3Ec__DisplayClass52_0__ctor_mFE43B60BD5291A82589C8E6B8AFBA94BA5386FE9 ();
// 0x00000BE1 System.Void ImageConfig_<>c__DisplayClass52_0::<DownloadInBG>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass52_0_U3CDownloadInBGU3Eb__1_m1C936818248A4391B8D5904FDA99FC66D597EA55 ();
// 0x00000BE2 System.Void ImageConfig_<>c__DisplayClass52_1::.ctor()
extern void U3CU3Ec__DisplayClass52_1__ctor_m73782CCD3A6751FEEB5F9CA49B0780E60957F983 ();
// 0x00000BE3 System.Void ImageConfig_<>c__DisplayClass52_2::.ctor()
extern void U3CU3Ec__DisplayClass52_2__ctor_m08024914BDF5BECBA7D16FCBEC13D97CEDDDEDD3 ();
// 0x00000BE4 System.Void ImageConfig_<>c__DisplayClass52_2::<DownloadInBG>b__0(Proyecto26.ResponseHelper)
extern void U3CU3Ec__DisplayClass52_2_U3CDownloadInBGU3Eb__0_m227DC588C18FF872BE9530267303084E10C1EE3F ();
// 0x00000BE5 System.Void ImageConfig_<DownloadInBG>d__52::.ctor(System.Int32)
extern void U3CDownloadInBGU3Ed__52__ctor_m9C32150885A44693E430D666C71EDFDF32D82EB2 ();
// 0x00000BE6 System.Void ImageConfig_<DownloadInBG>d__52::System.IDisposable.Dispose()
extern void U3CDownloadInBGU3Ed__52_System_IDisposable_Dispose_m6C66D148375FFD5DB7DD84AF176EEE8951298C5D ();
// 0x00000BE7 System.Boolean ImageConfig_<DownloadInBG>d__52::MoveNext()
extern void U3CDownloadInBGU3Ed__52_MoveNext_m6F290A5F0DF5C7302F9D3A453605CE86CB5B3CC9 ();
// 0x00000BE8 System.Object ImageConfig_<DownloadInBG>d__52::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadInBGU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5228F8C506499D7F41FF732A16607AC6126AB8F3 ();
// 0x00000BE9 System.Void ImageConfig_<DownloadInBG>d__52::System.Collections.IEnumerator.Reset()
extern void U3CDownloadInBGU3Ed__52_System_Collections_IEnumerator_Reset_m659C0406047B5D8737886D663CF82DB29EDB604F ();
// 0x00000BEA System.Object ImageConfig_<DownloadInBG>d__52::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadInBGU3Ed__52_System_Collections_IEnumerator_get_Current_m038C5E50AA9B2C6684F25B4B383FF85BC8F155E0 ();
// 0x00000BEB System.Void InputManager_<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m9E2570663FA2462DA3B372A0BA504980D7A8D52E ();
// 0x00000BEC System.Void InputManager_<>c__DisplayClass20_0::<TouchManager>b__0()
extern void U3CU3Ec__DisplayClass20_0_U3CTouchManagerU3Eb__0_m60DFDF329FA13E3596A782CAA9940DCB505C3356 ();
// 0x00000BED System.Void InputManager_<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_m4F44AD9D2DF8CB6373FB5EE34615FDF9128F56DF ();
// 0x00000BEE System.Void InputManager_<>c__DisplayClass31_0::<EndResponsiveScrolling>b__0()
extern void U3CU3Ec__DisplayClass31_0_U3CEndResponsiveScrollingU3Eb__0_mAD754467736AE903ACEF06593115803131ADB9EC ();
// 0x00000BEF System.Void InputManager_<>c__DisplayClass31_0::<EndResponsiveScrolling>b__1()
extern void U3CU3Ec__DisplayClass31_0_U3CEndResponsiveScrollingU3Eb__1_mFB8D4AA041D6E0792BC5360A388215FDC4DA5E03 ();
// 0x00000BF0 System.Void ModalConfig_<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_m5E9344B9B4B3C8E0CCEE40261606B0D8E56C909C ();
// 0x00000BF1 System.Void ModalConfig_<>c__DisplayClass25_0::<ChangeModalFromCarasouls>b__0()
extern void U3CU3Ec__DisplayClass25_0_U3CChangeModalFromCarasoulsU3Eb__0_m129D8A854C288EBAA12A71B680D1372E7E87B4F1 ();
// 0x00000BF2 System.Void ModalConfig_<>c__DisplayClass27_0::.ctor()
extern void U3CU3Ec__DisplayClass27_0__ctor_m56A0AA23E068137A12D9FA692834EDA52B086B8C ();
// 0x00000BF3 System.Void ModalConfig_<>c__DisplayClass27_0::<ActivatePinchIcon>b__0()
extern void U3CU3Ec__DisplayClass27_0_U3CActivatePinchIconU3Eb__0_mF7DF63728C05CE7D1D2C1230798A8EA5F1C59BB3 ();
// 0x00000BF4 System.Void ModalConfig_<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_m69385B930CC9904209850F44044B251CF4464788 ();
// 0x00000BF5 System.Void ModalConfig_<>c__DisplayClass33_0::<HideSideTM>b__0()
extern void U3CU3Ec__DisplayClass33_0_U3CHideSideTMU3Eb__0_mC4530C8C1AAF7CB0A601114319CB6D617F3CEE3C ();
// 0x00000BF6 System.Void ModalConfig_<>c__DisplayClass33_0::<HideSideTM>b__1()
extern void U3CU3Ec__DisplayClass33_0_U3CHideSideTMU3Eb__1_m6AF4C0DB1A84682DB5B06F35AF09A5FB97D83041 ();
// 0x00000BF7 System.Void ModalConfig_<>c__DisplayClass33_1::.ctor()
extern void U3CU3Ec__DisplayClass33_1__ctor_m17582E556E4CC0C38F00352E707B383795BAE56E ();
// 0x00000BF8 System.Void ModalConfig_<>c__DisplayClass33_1::<HideSideTM>b__2()
extern void U3CU3Ec__DisplayClass33_1_U3CHideSideTMU3Eb__2_mC4ED50A61752EEB3E22A68037CEED034F7B988D0 ();
// 0x00000BF9 System.Void ModalConfig_<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_m6C6C98D4690E8793FCFE804359CC49340D5DC639 ();
// 0x00000BFA System.Void ModalConfig_<>c__DisplayClass34_0::<ActivateSPV>b__0()
extern void U3CU3Ec__DisplayClass34_0_U3CActivateSPVU3Eb__0_mECD3DA468ADBE7BCFC2C2A53432F9477606054EB ();
// 0x00000BFB System.Void ModalConfig_<>c__DisplayClass34_0::<ActivateSPV>b__1()
extern void U3CU3Ec__DisplayClass34_0_U3CActivateSPVU3Eb__1_mD2DE6826C1F44B7F7A46A2F805D1080A0C37129A ();
// 0x00000BFC System.Void ModalConfig_<>c__DisplayClass34_0::<ActivateSPV>b__2()
extern void U3CU3Ec__DisplayClass34_0_U3CActivateSPVU3Eb__2_m6372589F621DDD94729FBB8067B98BCF690D138E ();
// 0x00000BFD System.Void ModalConfig_<>c__DisplayClass41_0::.ctor()
extern void U3CU3Ec__DisplayClass41_0__ctor_mE3614AE5196C82DD067E066B035BAA43A1B1D85A ();
// 0x00000BFE System.Void ModalConfig_<>c__DisplayClass41_0::<DeactivateSPV>b__0()
extern void U3CU3Ec__DisplayClass41_0_U3CDeactivateSPVU3Eb__0_m2C1BDFE54F75C961B07B2CF2CE97A680879FACC7 ();
// 0x00000BFF System.Void ModalConfig_<>c::.cctor()
extern void U3CU3Ec__cctor_m7BA4783EFBB948C33D79BE72C37E27129E1E1FBF ();
// 0x00000C00 System.Void ModalConfig_<>c::.ctor()
extern void U3CU3Ec__ctor_m404AE10B9B24E03B9E9EE015795EABEBD0B5CC86 ();
// 0x00000C01 System.Void ModalConfig_<>c::<FlipingAnimation>b__52_0()
extern void U3CU3Ec_U3CFlipingAnimationU3Eb__52_0_mD368931020467939BF2AC978322AAD6D16AB17BB ();
// 0x00000C02 System.Void ModalConfig_<>c::<FlipingAnimation>b__52_1()
extern void U3CU3Ec_U3CFlipingAnimationU3Eb__52_1_m28C8B60D2FD7F2AC015E0317957AE4F41263440B ();
// 0x00000C03 System.Void ModalConfig_<>c__DisplayClass53_0::.ctor()
extern void U3CU3Ec__DisplayClass53_0__ctor_mE4D4BA9971901C385EF1A6441FC41AA4311B7AFE ();
// 0x00000C04 System.Void ModalConfig_<>c__DisplayClass53_0::<LeftModal>b__0()
extern void U3CU3Ec__DisplayClass53_0_U3CLeftModalU3Eb__0_mE57FB9B0FF58FC23E5E7C170B8FA6248B858B2C4 ();
// 0x00000C05 System.Void ModalConfig_<>c__DisplayClass53_0::<LeftModal>b__1()
extern void U3CU3Ec__DisplayClass53_0_U3CLeftModalU3Eb__1_m66B8A0D728D875FEFFE27B77B73A51696B4C3B2B ();
// 0x00000C06 System.Void ModalConfig_<>c__DisplayClass54_0::.ctor()
extern void U3CU3Ec__DisplayClass54_0__ctor_mA656950B789B2314722A00AB7F29AF683C7B24DD ();
// 0x00000C07 System.Void ModalConfig_<>c__DisplayClass54_0::<RightModal>b__0()
extern void U3CU3Ec__DisplayClass54_0_U3CRightModalU3Eb__0_mB149439EF6040259730B43FFF2AFA61BFD6131B7 ();
// 0x00000C08 System.Void ModalConfig_<>c__DisplayClass54_0::<RightModal>b__1()
extern void U3CU3Ec__DisplayClass54_0_U3CRightModalU3Eb__1_m9FD7E205B5823BD46C76F6065CBCF2479353CCDB ();
// 0x00000C09 System.Void ModalConfig_<>c__DisplayClass56_0::.ctor()
extern void U3CU3Ec__DisplayClass56_0__ctor_m47366D71A4AABCA505BA60E3920204ABEEC9F375 ();
// 0x00000C0A System.Void ModalConfig_<>c__DisplayClass56_0::<MidModal>b__0()
extern void U3CU3Ec__DisplayClass56_0_U3CMidModalU3Eb__0_mE438F47B739B13B91F9B24C32B5C1A25D2D7A8EE ();
// 0x00000C0B System.Void ModalConfig_<>c__DisplayClass56_0::<MidModal>b__1()
extern void U3CU3Ec__DisplayClass56_0_U3CMidModalU3Eb__1_m267385D5152C082FA6552EDF3EA8581B062A17D0 ();
// 0x00000C0C System.Void ModalConfig_<>c__DisplayClass56_1::.ctor()
extern void U3CU3Ec__DisplayClass56_1__ctor_mF20C175125229279CB5D074F6FB31F9EB6F7DD9E ();
// 0x00000C0D System.Void ModalConfig_<>c__DisplayClass56_1::<MidModal>b__2()
extern void U3CU3Ec__DisplayClass56_1_U3CMidModalU3Eb__2_m9E5FE6E22F612C7FECAF9288F0775473CE48390B ();
// 0x00000C0E System.Void ModalConfig_<>c__DisplayClass56_2::.ctor()
extern void U3CU3Ec__DisplayClass56_2__ctor_mD393F815092EFE30E8395AC6917CBBAFF669C31E ();
// 0x00000C0F System.Void ModalConfig_<>c__DisplayClass56_2::<MidModal>b__3()
extern void U3CU3Ec__DisplayClass56_2_U3CMidModalU3Eb__3_m1ACB773C8A5FE4C1455F34C7626B368142177FD1 ();
// 0x00000C10 System.Void MouseManager_<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m6ADF91B27CCB0B263DDEA4C038305E59340940A7 ();
// 0x00000C11 System.Void MouseManager_<>c__DisplayClass22_0::<_MouseManager>b__0()
extern void U3CU3Ec__DisplayClass22_0_U3C_MouseManagerU3Eb__0_m0420120849C5E2D860D88840BAB4E4664FF4FD85 ();
// 0x00000C12 System.Void MouseManager_<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_m4BA299C7F72FFBD509AB3BB3F8FA1BD5CE50422B ();
// 0x00000C13 System.Void MouseManager_<>c__DisplayClass24_0::<SwipeBotAlternative>b__0()
extern void U3CU3Ec__DisplayClass24_0_U3CSwipeBotAlternativeU3Eb__0_m560B7A58168E1786C3D14DF77A7256CA4B545965 ();
// 0x00000C14 System.Void MouseManager_<>c__DisplayClass24_0::<SwipeBotAlternative>b__1()
extern void U3CU3Ec__DisplayClass24_0_U3CSwipeBotAlternativeU3Eb__1_mEBFF0E8349097E6F174346C4007078CDF77A6F7F ();
// 0x00000C15 System.Void MouseManager_<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_m209295986C8F67915C0ED8BF651C058D46D18414 ();
// 0x00000C16 System.Void MouseManager_<>c__DisplayClass32_0::<EndResponsiveScrolling>b__0()
extern void U3CU3Ec__DisplayClass32_0_U3CEndResponsiveScrollingU3Eb__0_m1FDAA4A2D679255C2089CB593C426E2E83A35BD7 ();
// 0x00000C17 System.Void MouseManager_<>c__DisplayClass32_0::<EndResponsiveScrolling>b__1()
extern void U3CU3Ec__DisplayClass32_0_U3CEndResponsiveScrollingU3Eb__1_mD3D696090D2AA2FB6CD8865D2DF8E86A53A68DD8 ();
// 0x00000C18 System.Void SimilarItems_<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_m653CBE2AB467273DAC1ED4C55B91F208AE93A3E1 ();
// 0x00000C19 System.Void SimilarItems_<>c__DisplayClass29_0::<DownloadAPIStyleIT>b__0(Proyecto26.ResponseHelper)
extern void U3CU3Ec__DisplayClass29_0_U3CDownloadAPIStyleITU3Eb__0_m8F745C795B5098E305303EACBBE839DCA55F9F1C ();
// 0x00000C1A System.Void SimilarItems_<>c__DisplayClass29_0::<DownloadAPIStyleIT>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass29_0_U3CDownloadAPIStyleITU3Eb__1_m9B9BE861290996E529B0C80B95AD780AC4E341D9 ();
// 0x00000C1B System.Void SimilarItems_<DownloadingStyleIt2>d__32::.ctor(System.Int32)
extern void U3CDownloadingStyleIt2U3Ed__32__ctor_m42A04B908E7530E2284710A10146E1D89BB375F0 ();
// 0x00000C1C System.Void SimilarItems_<DownloadingStyleIt2>d__32::System.IDisposable.Dispose()
extern void U3CDownloadingStyleIt2U3Ed__32_System_IDisposable_Dispose_mF24B6AF677A6A42C72FE3E1738D36C0099A2FC88 ();
// 0x00000C1D System.Boolean SimilarItems_<DownloadingStyleIt2>d__32::MoveNext()
extern void U3CDownloadingStyleIt2U3Ed__32_MoveNext_m39B5D459342B1587E563FAC829BFB81A367C2AF8 ();
// 0x00000C1E System.Object SimilarItems_<DownloadingStyleIt2>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadingStyleIt2U3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF50A352DEC674B93255A4A2A0D9E06D2EE5BDA0D ();
// 0x00000C1F System.Void SimilarItems_<DownloadingStyleIt2>d__32::System.Collections.IEnumerator.Reset()
extern void U3CDownloadingStyleIt2U3Ed__32_System_Collections_IEnumerator_Reset_mE60BB186C5EEB2756996CB0B777E6328AC159823 ();
// 0x00000C20 System.Object SimilarItems_<DownloadingStyleIt2>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadingStyleIt2U3Ed__32_System_Collections_IEnumerator_get_Current_m16D17D52F81DBD046807F63367E9A29BD5BAFAC7 ();
// 0x00000C21 System.Void SimilarItems_<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_m297C9F3FCDC5DA350C0AD371B807F5E815CE3CA5 ();
// 0x00000C22 System.Void SimilarItems_<>c__DisplayClass36_1::.ctor()
extern void U3CU3Ec__DisplayClass36_1__ctor_m9BF2F59E7519123409A44C40E84D3C645FA08506 ();
// 0x00000C23 System.Void SimilarItems_<>c__DisplayClass36_1::<DownloadDetailCarasoul>b__0(Proyecto26.ResponseHelper)
extern void U3CU3Ec__DisplayClass36_1_U3CDownloadDetailCarasoulU3Eb__0_m795639C7D35455708FF0857CE54554DEDA9BDFC5 ();
// 0x00000C24 System.Void SimilarItems_<>c__DisplayClass36_1::<DownloadDetailCarasoul>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass36_1_U3CDownloadDetailCarasoulU3Eb__1_m893391E772D3A112AB018E569E275BAEFDAC8758 ();
// 0x00000C25 System.Void SimilarItems_<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_m8F675362040CF284D5F54E6CCEE52F8C0BB9D70C ();
// 0x00000C26 System.Void SimilarItems_<>c__DisplayClass38_0::<DownloadSubtly>b__0(Proyecto26.ResponseHelper)
extern void U3CU3Ec__DisplayClass38_0_U3CDownloadSubtlyU3Eb__0_mDD1EDAD7DA8B57B961FCE24D2DA4A64F1A13098A ();
// 0x00000C27 System.Void SimilarItems_<>c__DisplayClass38_0::<DownloadSubtly>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass38_0_U3CDownloadSubtlyU3Eb__1_mE39A8645FA34F2DDF1469B7E5C90CC7D84E24527 ();
// 0x00000C28 System.Void SimilarItems_<SetLPAndTex>d__41::.ctor(System.Int32)
extern void U3CSetLPAndTexU3Ed__41__ctor_mCD98FB48B22896C60B0EA29418D91B13F2634541 ();
// 0x00000C29 System.Void SimilarItems_<SetLPAndTex>d__41::System.IDisposable.Dispose()
extern void U3CSetLPAndTexU3Ed__41_System_IDisposable_Dispose_m136EA1369A6E3D47CACCEBEA74D87E78B269D992 ();
// 0x00000C2A System.Boolean SimilarItems_<SetLPAndTex>d__41::MoveNext()
extern void U3CSetLPAndTexU3Ed__41_MoveNext_m8F887FD229ED61A049E0E22D0EC52A660E5ED70F ();
// 0x00000C2B System.Object SimilarItems_<SetLPAndTex>d__41::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetLPAndTexU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2DE471B4BA1BD4BC339BFDF8094F3ED3BD178B72 ();
// 0x00000C2C System.Void SimilarItems_<SetLPAndTex>d__41::System.Collections.IEnumerator.Reset()
extern void U3CSetLPAndTexU3Ed__41_System_Collections_IEnumerator_Reset_m77CAF6D268B15483D2F1BCEEA1895C45A0FCC2CC ();
// 0x00000C2D System.Object SimilarItems_<SetLPAndTex>d__41::System.Collections.IEnumerator.get_Current()
extern void U3CSetLPAndTexU3Ed__41_System_Collections_IEnumerator_get_Current_mE06A2E328E043AEC6E6937B90A36F6D5F3214259 ();
// 0x00000C2E System.Void SimilarItems_<_GetDPSimilarAndContinue>d__43::.ctor(System.Int32)
extern void U3C_GetDPSimilarAndContinueU3Ed__43__ctor_m371CA9430B2B498B6A532AE6AD1B2AA6599F03D5 ();
// 0x00000C2F System.Void SimilarItems_<_GetDPSimilarAndContinue>d__43::System.IDisposable.Dispose()
extern void U3C_GetDPSimilarAndContinueU3Ed__43_System_IDisposable_Dispose_mDCE67EDF483D9DF1C819998E81C0214CA7303E38 ();
// 0x00000C30 System.Boolean SimilarItems_<_GetDPSimilarAndContinue>d__43::MoveNext()
extern void U3C_GetDPSimilarAndContinueU3Ed__43_MoveNext_mCB3A0BCF6D694A15986CD0BCB1F342EE62CEF211 ();
// 0x00000C31 System.Object SimilarItems_<_GetDPSimilarAndContinue>d__43::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3C_GetDPSimilarAndContinueU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43580EBB068989475A35F7467802D6B24281F89A ();
// 0x00000C32 System.Void SimilarItems_<_GetDPSimilarAndContinue>d__43::System.Collections.IEnumerator.Reset()
extern void U3C_GetDPSimilarAndContinueU3Ed__43_System_Collections_IEnumerator_Reset_mD418543FAE4EA398AB6506F2912D23CE61B18F24 ();
// 0x00000C33 System.Object SimilarItems_<_GetDPSimilarAndContinue>d__43::System.Collections.IEnumerator.get_Current()
extern void U3C_GetDPSimilarAndContinueU3Ed__43_System_Collections_IEnumerator_get_Current_m433C8F67741934EC376F302D0FDCE0A39BB5AA49 ();
// 0x00000C34 System.Void SimilarItems_<Download5FirstImg>d__46::.ctor(System.Int32)
extern void U3CDownload5FirstImgU3Ed__46__ctor_m7F8891F944669026E11C2FCA8A5E55BF2BA29D2C ();
// 0x00000C35 System.Void SimilarItems_<Download5FirstImg>d__46::System.IDisposable.Dispose()
extern void U3CDownload5FirstImgU3Ed__46_System_IDisposable_Dispose_mFA047EB7F2EB195452F3A561221CBF6D65032D49 ();
// 0x00000C36 System.Boolean SimilarItems_<Download5FirstImg>d__46::MoveNext()
extern void U3CDownload5FirstImgU3Ed__46_MoveNext_m235C87C476E49C456AD92F12A6522B391B22FD08 ();
// 0x00000C37 System.Object SimilarItems_<Download5FirstImg>d__46::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownload5FirstImgU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m539E78E67BD7D6369E4B2FBA5EE5E8808BEE1B35 ();
// 0x00000C38 System.Void SimilarItems_<Download5FirstImg>d__46::System.Collections.IEnumerator.Reset()
extern void U3CDownload5FirstImgU3Ed__46_System_Collections_IEnumerator_Reset_mC13FD9498A3539F8B8A4A8EA1332DDDAD65913E5 ();
// 0x00000C39 System.Object SimilarItems_<Download5FirstImg>d__46::System.Collections.IEnumerator.get_Current()
extern void U3CDownload5FirstImgU3Ed__46_System_Collections_IEnumerator_get_Current_m622928139B949871C8E74D33E51D01D65335ECD9 ();
// 0x00000C3A System.Void SimilarItems_<>c__DisplayClass48_0::.ctor()
extern void U3CU3Ec__DisplayClass48_0__ctor_mD515A9735C479501A642C30D17647C965B10E6EA ();
// 0x00000C3B System.Void SimilarItems_<>c__DisplayClass48_1::.ctor()
extern void U3CU3Ec__DisplayClass48_1__ctor_mC93A61149B481721A590B4B3F000CF29FE6BDE34 ();
// 0x00000C3C System.Void SimilarItems_<>c__DisplayClass48_1::<GetStyleITSimilar>b__0(Proyecto26.ResponseHelper)
extern void U3CU3Ec__DisplayClass48_1_U3CGetStyleITSimilarU3Eb__0_m426A216A0098224A5D2B9115484A9EF1C30D4C1F ();
// 0x00000C3D System.Void SimilarItems_<>c__DisplayClass48_1::<GetStyleITSimilar>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass48_1_U3CGetStyleITSimilarU3Eb__1_mE3A342AB55C35E283DFD0FC27024853A94AD8F78 ();
// 0x00000C3E System.Void SimilarItems_<DownloadingImgSubtly>d__49::.ctor(System.Int32)
extern void U3CDownloadingImgSubtlyU3Ed__49__ctor_m7384606ED7EB7913BA4BBED835231EB554E83028 ();
// 0x00000C3F System.Void SimilarItems_<DownloadingImgSubtly>d__49::System.IDisposable.Dispose()
extern void U3CDownloadingImgSubtlyU3Ed__49_System_IDisposable_Dispose_mE2EEF86407CFE50BA2A92231C671390918DC56BB ();
// 0x00000C40 System.Boolean SimilarItems_<DownloadingImgSubtly>d__49::MoveNext()
extern void U3CDownloadingImgSubtlyU3Ed__49_MoveNext_m9074F91FC41C773148821D58D38794C9824BDDC5 ();
// 0x00000C41 System.Object SimilarItems_<DownloadingImgSubtly>d__49::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadingImgSubtlyU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE252E3863DF34B6E2A6E8D9646F4719761DE15DE ();
// 0x00000C42 System.Void SimilarItems_<DownloadingImgSubtly>d__49::System.Collections.IEnumerator.Reset()
extern void U3CDownloadingImgSubtlyU3Ed__49_System_Collections_IEnumerator_Reset_mC779D62EFDF7070F7592BC75D2FFC63594387DDD ();
// 0x00000C43 System.Object SimilarItems_<DownloadingImgSubtly>d__49::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadingImgSubtlyU3Ed__49_System_Collections_IEnumerator_get_Current_mDA36E53BCBE228B802A57E258AA3DB731A4F0027 ();
// 0x00000C44 System.Void StartOver_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m5F83927542B0FB2F6C96E3546B9104095A023FF3 ();
// 0x00000C45 System.Void StartOver_<>c__DisplayClass9_0::<MaskCanvasTimer>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CMaskCanvasTimerU3Eb__0_mAB976CA3706030E058821359101D29056C860287 ();
// 0x00000C46 System.Void StartOver_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mAE572141468E81911A1A7CB2BF75A94D8C689264 ();
// 0x00000C47 System.Void StartOver_<>c__DisplayClass10_0::<StartCountingDownIdleSPV>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CStartCountingDownIdleSPVU3Eb__0_mC5566EF9A0FB2A018C0DAF4FDD755954AD7A42E6 ();
// 0x00000C48 System.Void StartOver_<>c__DisplayClass10_0::<StartCountingDownIdleSPV>b__1()
extern void U3CU3Ec__DisplayClass10_0_U3CStartCountingDownIdleSPVU3Eb__1_mB11B2FBC4D7E3F7809043D871AD151B86C0BCC1F ();
// 0x00000C49 System.Void StartOver_<>c__DisplayClass10_0::<StartCountingDownIdleSPV>b__2()
extern void U3CU3Ec__DisplayClass10_0_U3CStartCountingDownIdleSPVU3Eb__2_m41C01BEF91628259F4528F8AAAB965F96A9D0992 ();
// 0x00000C4A System.Void StartOver_<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_mAC3B8C8F0CBD0C4B5DF21497482D6ED690355145 ();
// 0x00000C4B System.Void StartOver_<>c__DisplayClass13_0::<CoolingDownAfterSPV>b__3()
extern void U3CU3Ec__DisplayClass13_0_U3CCoolingDownAfterSPVU3Eb__3_m6C3F15E6D7B20DB66DB157D6EDE9BF996174CEEF ();
// 0x00000C4C System.Void StartOver_<>c__DisplayClass13_0::<CoolingDownAfterSPV>b__4()
extern void U3CU3Ec__DisplayClass13_0_U3CCoolingDownAfterSPVU3Eb__4_mCA3D3BD1C470A4687C7CD0C38851BCD8D5328E59 ();
// 0x00000C4D System.Void StartOver_<>c__DisplayClass13_0::<CoolingDownAfterSPV>b__5()
extern void U3CU3Ec__DisplayClass13_0_U3CCoolingDownAfterSPVU3Eb__5_mE2526975C46DD404DC6F077E1C1EA44443229010 ();
// 0x00000C4E System.Void StartOver_<>c__DisplayClass13_1::.ctor()
extern void U3CU3Ec__DisplayClass13_1__ctor_m219CAAE7598F7528E8D296A76F02E5A19C7BE745 ();
// 0x00000C4F System.Void StartOver_<>c__DisplayClass13_1::<CoolingDownAfterSPV>b__0()
extern void U3CU3Ec__DisplayClass13_1_U3CCoolingDownAfterSPVU3Eb__0_m15502D0BDAE0958CFBA95F2DBEEC9690F062B73F ();
// 0x00000C50 System.Void StartOver_<>c__DisplayClass13_1::<CoolingDownAfterSPV>b__1()
extern void U3CU3Ec__DisplayClass13_1_U3CCoolingDownAfterSPVU3Eb__1_mCD9F12870F0D9ABD86EE333CC39EFC1BE73526F8 ();
// 0x00000C51 System.Void StartOver_<>c__DisplayClass13_1::<CoolingDownAfterSPV>b__2()
extern void U3CU3Ec__DisplayClass13_1_U3CCoolingDownAfterSPVU3Eb__2_m278F4215A5D0389D461FE36F7533656879DDEF4B ();
// 0x00000C52 System.Void StartOver_<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m60AD82FFD92AED2B7D313BA18FD0DA88382AEB4C ();
// 0x00000C53 System.Void StartOver_<>c__DisplayClass16_0::<Swiper>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CSwiperU3Eb__0_m4BA7300B4EA50880D343880EAC19E276DB7B4602 ();
// 0x00000C54 System.Void StartOver_<>c__DisplayClass16_0::<Swiper>b__1()
extern void U3CU3Ec__DisplayClass16_0_U3CSwiperU3Eb__1_m5E95332DAE95E12D4E94C24DE62C0149C9DDD179 ();
// 0x00000C55 System.Void StartOver_<>c__DisplayClass16_0::<Swiper>b__2()
extern void U3CU3Ec__DisplayClass16_0_U3CSwiperU3Eb__2_mEC77BFB810460B9042716715162C1716C0E1BCC4 ();
// 0x00000C56 System.Void StartOver_<>c__DisplayClass16_0::<Swiper>b__3()
extern void U3CU3Ec__DisplayClass16_0_U3CSwiperU3Eb__3_m341366EE6EF01E1F363B77C9E2B9516DC05BC542 ();
// 0x00000C57 System.Void StartOver_<>c__DisplayClass16_0::<Swiper>b__4()
extern void U3CU3Ec__DisplayClass16_0_U3CSwiperU3Eb__4_m97926705ED8D962A57F6E104C07B7481CA0E67EC ();
// 0x00000C58 System.Void StartOver_<>c__DisplayClass16_0::<Swiper>b__5()
extern void U3CU3Ec__DisplayClass16_0_U3CSwiperU3Eb__5_mC1506E8868F039BB999591EBF404BA3FE948A04D ();
// 0x00000C59 System.Void StartOver_<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_m5E081B9664F6D7BBC1BB3F3C43599995338490A1 ();
// 0x00000C5A System.Void StartOver_<>c__DisplayClass17_0::<GoToDefaultState>b__0()
extern void U3CU3Ec__DisplayClass17_0_U3CGoToDefaultStateU3Eb__0_m0988B7EC7E00340F8D4BC45B51F343BE7D8D9818 ();
// 0x00000C5B System.Void StartOver_<>c__DisplayClass17_0::<GoToDefaultState>b__1()
extern void U3CU3Ec__DisplayClass17_0_U3CGoToDefaultStateU3Eb__1_m769E53170157E7E7135EFB6E53B00FD6427C29D1 ();
// 0x00000C5C System.Void StartOver_<>c__DisplayClass17_0::<GoToDefaultState>b__6()
extern void U3CU3Ec__DisplayClass17_0_U3CGoToDefaultStateU3Eb__6_mFE2ACDBF06ACF9D6B323B80A2CD7B8B5D9760AD5 ();
// 0x00000C5D System.Void StartOver_<>c__DisplayClass17_0::<GoToDefaultState>b__7()
extern void U3CU3Ec__DisplayClass17_0_U3CGoToDefaultStateU3Eb__7_mA4BB07F5AEBE5720245C1B014CBA78DF36D9AD7A ();
// 0x00000C5E System.Void StartOver_<>c__DisplayClass17_1::.ctor()
extern void U3CU3Ec__DisplayClass17_1__ctor_mB5968D7C955844228115C9FB168B627BA4E135F7 ();
// 0x00000C5F System.Void StartOver_<>c__DisplayClass17_1::<GoToDefaultState>b__2()
extern void U3CU3Ec__DisplayClass17_1_U3CGoToDefaultStateU3Eb__2_mD853ECCB6C77707FE21934AAA56092D1B33A5EBF ();
// 0x00000C60 System.Void StartOver_<>c__DisplayClass17_1::<GoToDefaultState>b__3()
extern void U3CU3Ec__DisplayClass17_1_U3CGoToDefaultStateU3Eb__3_mB00290943876BAB62ABDEFBB15CDCC4FC2718B32 ();
// 0x00000C61 System.Void StartOver_<>c__DisplayClass17_1::<GoToDefaultState>b__4()
extern void U3CU3Ec__DisplayClass17_1_U3CGoToDefaultStateU3Eb__4_m9FBE7E0D9667C4EE6CEC5529CF248E5F5CFD8C91 ();
// 0x00000C62 System.Void StartOver_<>c__DisplayClass17_1::<GoToDefaultState>b__5()
extern void U3CU3Ec__DisplayClass17_1_U3CGoToDefaultStateU3Eb__5_mC0EF1D0B7E0B1BDCA73B70777C8CECC63C9F0BC6 ();
// 0x00000C63 System.Void StartOver_<DownloadImgGoBack>d__19::.ctor(System.Int32)
extern void U3CDownloadImgGoBackU3Ed__19__ctor_m5BD30B610950AAEACA0A79B9E5DEEE3DDCF6B56B ();
// 0x00000C64 System.Void StartOver_<DownloadImgGoBack>d__19::System.IDisposable.Dispose()
extern void U3CDownloadImgGoBackU3Ed__19_System_IDisposable_Dispose_mFB56F9775E85F8D173ED57ACA36B058CD5517CA8 ();
// 0x00000C65 System.Boolean StartOver_<DownloadImgGoBack>d__19::MoveNext()
extern void U3CDownloadImgGoBackU3Ed__19_MoveNext_m8BF70F76884BF0F745209DD88EB2BBC766189562 ();
// 0x00000C66 System.Object StartOver_<DownloadImgGoBack>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadImgGoBackU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD68B48E0E89FC3C2E0508E515FA0F59B26666E4 ();
// 0x00000C67 System.Void StartOver_<DownloadImgGoBack>d__19::System.Collections.IEnumerator.Reset()
extern void U3CDownloadImgGoBackU3Ed__19_System_Collections_IEnumerator_Reset_m45022B6700F6DC04B7DC92A16F79181B6C92D496 ();
// 0x00000C68 System.Object StartOver_<DownloadImgGoBack>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadImgGoBackU3Ed__19_System_Collections_IEnumerator_get_Current_m5E8EDCAE97978D4D6506ED4DB17F01DBF49E0F73 ();
// 0x00000C69 System.Void StartOver_<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m7024E676AE793FBCFCB1F2279B2FB97483F6FD05 ();
// 0x00000C6A System.Void StartOver_<>c__DisplayClass20_0::<DownloadDPGoBack>b__0(Proyecto26.ResponseHelper)
extern void U3CU3Ec__DisplayClass20_0_U3CDownloadDPGoBackU3Eb__0_m7606798CFDD68B82C75D4C10A811B29057B25750 ();
// 0x00000C6B System.Void StartOver_<>c__DisplayClass20_0::<DownloadDPGoBack>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass20_0_U3CDownloadDPGoBackU3Eb__1_mB38292A94C62C87BDA2640B29FA6F69CB7AC9C6E ();
// 0x00000C6C System.Void StartOver_<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m22DDB7D71EF49057B3EC0DB661DF13A5151B0734 ();
// 0x00000C6D System.Void StartOver_<>c__DisplayClass22_0::<CheckSpawnHorizontal>b__0()
extern void U3CU3Ec__DisplayClass22_0_U3CCheckSpawnHorizontalU3Eb__0_m57407159696242C5C835258C271A8610C70D54E6 ();
// 0x00000C6E System.Void StartOver_<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_m43F32AE26D666A48211F67C8C097B0915B6509CC ();
// 0x00000C6F System.Void StartOver_<>c__DisplayClass24_0::<ComebackFromFluid>b__0()
extern void U3CU3Ec__DisplayClass24_0_U3CComebackFromFluidU3Eb__0_m0F05C7E786A7C5D93F99807C18E6849779609E28 ();
// 0x00000C70 System.Void StartOver_<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m5FA854693358BBC76CB6886315C19486E4B4F639 ();
// 0x00000C71 System.Void StartOver_<>c__DisplayClass26_0::<ComebackRow>b__0()
extern void U3CU3Ec__DisplayClass26_0_U3CComebackRowU3Eb__0_m2070DF50DE23631BF11AD0B09599D939F1B986ED ();
// 0x00000C72 System.Void StaticStorage_<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_m435487F025C380806EFF874C06D9336FEC4A9592 ();
// 0x00000C73 System.Void StaticStorage_<>c__DisplayClass29_0::<DownloadStyleIt>b__0(Proyecto26.ResponseHelper)
extern void U3CU3Ec__DisplayClass29_0_U3CDownloadStyleItU3Eb__0_m645AF86A071A40D61FB1B984117560C0AF469BC2 ();
// 0x00000C74 System.Void StaticStorage_<>c::.cctor()
extern void U3CU3Ec__cctor_mC9CCE327DB010005D66AB15F205E14F048CCE34C ();
// 0x00000C75 System.Void StaticStorage_<>c::.ctor()
extern void U3CU3Ec__ctor_m684AAFC2817300FD3C32E31E8AF357CDA4AC1BA6 ();
// 0x00000C76 System.Void StaticStorage_<>c::<DownloadStyleIt>b__29_1(System.Exception)
extern void U3CU3Ec_U3CDownloadStyleItU3Eb__29_1_m6C0D4AA7AA946FB7CE5DC6D3A17B5D7CDAFDAA84 ();
// 0x00000C77 System.Void DragUI_<BackAnimated>d__38::.ctor(System.Int32)
extern void U3CBackAnimatedU3Ed__38__ctor_mB0162397CAF149D3D5CD3FFF8C661C39B3AD1B8E ();
// 0x00000C78 System.Void DragUI_<BackAnimated>d__38::System.IDisposable.Dispose()
extern void U3CBackAnimatedU3Ed__38_System_IDisposable_Dispose_m524FCA507BBD67AF77CECA85D59BA327CF15A2C1 ();
// 0x00000C79 System.Boolean DragUI_<BackAnimated>d__38::MoveNext()
extern void U3CBackAnimatedU3Ed__38_MoveNext_mB2B08A393C82A7B5E7F06339AF59CB0ACAD0E8D1 ();
// 0x00000C7A System.Object DragUI_<BackAnimated>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBackAnimatedU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m038F721CB12DFAB91B0D3C938970EB8D56C9A919 ();
// 0x00000C7B System.Void DragUI_<BackAnimated>d__38::System.Collections.IEnumerator.Reset()
extern void U3CBackAnimatedU3Ed__38_System_Collections_IEnumerator_Reset_m9C778128BF8C3E464B4165DB6207532D9940A2AA ();
// 0x00000C7C System.Object DragUI_<BackAnimated>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CBackAnimatedU3Ed__38_System_Collections_IEnumerator_get_Current_mABEA0C9148A56E0D4774C4A112C6D08E0EFD0CA0 ();
// 0x00000C7D System.Void LoadingScene2_<BranchDownloadAPI>d__21::.ctor(System.Int32)
extern void U3CBranchDownloadAPIU3Ed__21__ctor_m4BC4DCDCB90C1062A4D53EBF0F084D6DAD28E59C ();
// 0x00000C7E System.Void LoadingScene2_<BranchDownloadAPI>d__21::System.IDisposable.Dispose()
extern void U3CBranchDownloadAPIU3Ed__21_System_IDisposable_Dispose_m8DC64B33FFAB87F9BB28CB815CC7829359F62E32 ();
// 0x00000C7F System.Boolean LoadingScene2_<BranchDownloadAPI>d__21::MoveNext()
extern void U3CBranchDownloadAPIU3Ed__21_MoveNext_mA7AB985F625A238EACE4EB34CB8851BFB1F15CBF ();
// 0x00000C80 System.Void LoadingScene2_<BranchDownloadAPI>d__21::<>m__Finally1()
extern void U3CBranchDownloadAPIU3Ed__21_U3CU3Em__Finally1_m8F18EB19092764D8C74840CBA161B6E478DC10F9 ();
// 0x00000C81 System.Object LoadingScene2_<BranchDownloadAPI>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBranchDownloadAPIU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m20C2D7434CBD322990AEE1C1FA3E0B40425B88F6 ();
// 0x00000C82 System.Void LoadingScene2_<BranchDownloadAPI>d__21::System.Collections.IEnumerator.Reset()
extern void U3CBranchDownloadAPIU3Ed__21_System_Collections_IEnumerator_Reset_mDCB698FCD419166698AE4B5719976656C975A58B ();
// 0x00000C83 System.Object LoadingScene2_<BranchDownloadAPI>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CBranchDownloadAPIU3Ed__21_System_Collections_IEnumerator_get_Current_m54FEE335829B80C62DCD93F57798763502D89AAC ();
// 0x00000C84 System.Void LoadingScene2_<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m80AC54DC1527827E14CCAF4745BFB2486F90E640 ();
// 0x00000C85 System.Void LoadingScene2_<>c__DisplayClass22_0::<MultiDownloadAPI>b__0(Proyecto26.ResponseHelper)
extern void U3CU3Ec__DisplayClass22_0_U3CMultiDownloadAPIU3Eb__0_mB07ABD47A83079B531A7F22BEEF8B129AFEC206C ();
// 0x00000C86 System.Void LoadingScene2_<>c__DisplayClass22_0::<MultiDownloadAPI>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass22_0_U3CMultiDownloadAPIU3Eb__1_m38D7AFFD2388849E9435DBB5D5B96C40B2BBD5EB ();
// 0x00000C87 System.Void LoadingScene2_<MultiDownloadNCache>d__25::.ctor(System.Int32)
extern void U3CMultiDownloadNCacheU3Ed__25__ctor_mAF90793763EB80C4CFBF8A704C007A6A3DBB8032 ();
// 0x00000C88 System.Void LoadingScene2_<MultiDownloadNCache>d__25::System.IDisposable.Dispose()
extern void U3CMultiDownloadNCacheU3Ed__25_System_IDisposable_Dispose_m95ED45CD0F52C398E8DCF7E862B12D792D1D7587 ();
// 0x00000C89 System.Boolean LoadingScene2_<MultiDownloadNCache>d__25::MoveNext()
extern void U3CMultiDownloadNCacheU3Ed__25_MoveNext_m0E3ED18BB76C8BDFDA179D2B10CBCA850C37DEFE ();
// 0x00000C8A System.Object LoadingScene2_<MultiDownloadNCache>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMultiDownloadNCacheU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8EEA6E82F323AC9E6DFDD2F3D348EDCEA469CD78 ();
// 0x00000C8B System.Void LoadingScene2_<MultiDownloadNCache>d__25::System.Collections.IEnumerator.Reset()
extern void U3CMultiDownloadNCacheU3Ed__25_System_Collections_IEnumerator_Reset_m058BC975A1F821ED114BC09E82B539DB8724858E ();
// 0x00000C8C System.Object LoadingScene2_<MultiDownloadNCache>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CMultiDownloadNCacheU3Ed__25_System_Collections_IEnumerator_get_Current_m0AC4CCFC678D17C3DE764C6F7A902ECA7BFFFFE5 ();
// 0x00000C8D System.Void LoadingScene2_<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m40E3639426B60D365247CB0C1D9345D870C2BA92 ();
// 0x00000C8E System.Void LoadingScene2_<>c__DisplayClass26_0::<MultiDownloadDetail>b__0(Proyecto26.ResponseHelper)
extern void U3CU3Ec__DisplayClass26_0_U3CMultiDownloadDetailU3Eb__0_mA0D95980DEEB9976802FD31C228A4AC04D19FD85 ();
// 0x00000C8F System.Void LoadingScene2_<>c__DisplayClass26_0::<MultiDownloadDetail>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass26_0_U3CMultiDownloadDetailU3Eb__1_m6B11B2C0D8C3D7517A622FF07B7FB3A3766FB6F6 ();
// 0x00000C90 System.Void LoadingScene2_<MultiDLDetailNCache>d__27::.ctor(System.Int32)
extern void U3CMultiDLDetailNCacheU3Ed__27__ctor_m2CE1775E1FB2789814400CEECFCA8C24C025FA12 ();
// 0x00000C91 System.Void LoadingScene2_<MultiDLDetailNCache>d__27::System.IDisposable.Dispose()
extern void U3CMultiDLDetailNCacheU3Ed__27_System_IDisposable_Dispose_m58441CF17ED6F1FCD0C2D12485BDDDCDA406BA33 ();
// 0x00000C92 System.Boolean LoadingScene2_<MultiDLDetailNCache>d__27::MoveNext()
extern void U3CMultiDLDetailNCacheU3Ed__27_MoveNext_m5067196148B4AB062D7CF9DCAA0EA49137559868 ();
// 0x00000C93 System.Object LoadingScene2_<MultiDLDetailNCache>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMultiDLDetailNCacheU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1AE81F249631A57FCB0C47DE16CA89C660DE80C1 ();
// 0x00000C94 System.Void LoadingScene2_<MultiDLDetailNCache>d__27::System.Collections.IEnumerator.Reset()
extern void U3CMultiDLDetailNCacheU3Ed__27_System_Collections_IEnumerator_Reset_m1BA9D6028EA4CF10ED76EADA21C9DC92F01FA64F ();
// 0x00000C95 System.Object LoadingScene2_<MultiDLDetailNCache>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CMultiDLDetailNCacheU3Ed__27_System_Collections_IEnumerator_get_Current_m068312F7DD13BBFE441237D9DF944B698A43D48E ();
// 0x00000C96 System.Void EnvMapAnimator_<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m004DF17C34E6A9C76325BD4C65E0F328AD4B37E0 ();
// 0x00000C97 System.Void EnvMapAnimator_<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mE7E20C5789828A8FDE263BD5ACC2D02982334B46 ();
// 0x00000C98 System.Boolean EnvMapAnimator_<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mB3F55ABCABBA6717942BDC85935FDE439D09E226 ();
// 0x00000C99 System.Object EnvMapAnimator_<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF976D094846BDC403335C76F41CAC26D53C8F1D2 ();
// 0x00000C9A System.Void EnvMapAnimator_<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m9368B3951732B0D18993DA1B889346A775F252CE ();
// 0x00000C9B System.Object EnvMapAnimator_<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m495592283D84E6B446F7B9B7405F250D9687DEFB ();
// 0x00000C9C System.Void TMPro.TMP_TextEventHandler_CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m036DA7F340B0839696EB50045AB186BD1046BE85 ();
// 0x00000C9D System.Void TMPro.TMP_TextEventHandler_SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m0BC042938C4EBBB77FFAD68C1ACD74FC1C3C1052 ();
// 0x00000C9E System.Void TMPro.TMP_TextEventHandler_WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m1C01733FD9860337084DFE63607ECE0EF8A450EA ();
// 0x00000C9F System.Void TMPro.TMP_TextEventHandler_LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m1C3A0C84C5C0FEA6C33FA9ED99756A85363C9EF2 ();
// 0x00000CA0 System.Void TMPro.TMP_TextEventHandler_LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_mC7034F51586C51D1DE381F6222816DC1542AFF3A ();
// 0x00000CA1 System.Void TMPro.Examples.Benchmark01_<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m139DF863E59AD287A6C14228BB59D56E7FD2E578 ();
// 0x00000CA2 System.Void TMPro.Examples.Benchmark01_<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mBBFAE2F68813477259A0B665B4E81833C03C746B ();
// 0x00000CA3 System.Boolean TMPro.Examples.Benchmark01_<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_m8D27A150B4BC045DD5A1ACB2FABBB7F7F318A015 ();
// 0x00000CA4 System.Object TMPro.Examples.Benchmark01_<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C55687F407BA372889D6A533FB817E1EA81C165 ();
// 0x00000CA5 System.Void TMPro.Examples.Benchmark01_<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mE725DFFE744FAEDABF70579D04BBEB17A6CFF692 ();
// 0x00000CA6 System.Object TMPro.Examples.Benchmark01_<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD8EEDE3D2E1E9838472D20AE93DD750D1EE39AF8 ();
// 0x00000CA7 System.Void TMPro.Examples.Benchmark01_UGUI_<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mECD4DB9695B4D04CEF08DF193DAFA21412DA40EF ();
// 0x00000CA8 System.Void TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m6773C67C5C6E7E52F8C8545181173A58A6B7D939 ();
// 0x00000CA9 System.Boolean TMPro.Examples.Benchmark01_UGUI_<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mB823F98B1B0907B6959EAEE4D1EBE0AA35795EA3 ();
// 0x00000CAA System.Object TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF100CFC13DED969B3BBE2007B3F863DCE919D978 ();
// 0x00000CAB System.Void TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mDE1D3A30ECA00E3CA2218A582F2C87EEE082E2DB ();
// 0x00000CAC System.Object TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m13710692FCBB7BB736B1F6446778124064802C83 ();
// 0x00000CAD System.Void TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_mB4DA3EEEFC5ECB8376EF29EAC034162B575961B8 ();
// 0x00000CAE System.Void TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_mC6A68A27A902E234429E3706D6DB432BEA04A384 ();
// 0x00000CAF System.Boolean TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_mDE142F6B3AAB5916834A6820A5E7B13A7B71E822 ();
// 0x00000CB0 System.Object TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9F4D59DDC372B977CD7297AA5F0C83D4FFBBA830 ();
// 0x00000CB1 System.Void TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mAC1E960F7FFCF032EE668635835954377D1469F6 ();
// 0x00000CB2 System.Object TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m96B6B8A0D715268DB789F3D4C60FC6DEC9E1F6E6 ();
// 0x00000CB3 System.Void TMPro.Examples.SkewTextExample_<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_m07871FDF578BC130082658B43FB4322C15F0909E ();
// 0x00000CB4 System.Void TMPro.Examples.SkewTextExample_<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m3C8BC1501397E256464ADD27A32486CDE63C2BE2 ();
// 0x00000CB5 System.Boolean TMPro.Examples.SkewTextExample_<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_m11D4701B069FCFC106319CBDCA56244EFA4C795F ();
// 0x00000CB6 System.Object TMPro.Examples.SkewTextExample_<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m995FCE4A3B9B270605168D01EF7439A437864C06 ();
// 0x00000CB7 System.Void TMPro.Examples.SkewTextExample_<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_m9970CA113E1A293472987C004B42771993CAA05C ();
// 0x00000CB8 System.Object TMPro.Examples.SkewTextExample_<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m398E9D0D09F26D5C3268EB41936ED92240474910 ();
// 0x00000CB9 System.Void TMPro.Examples.TeleType_<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mFB2D6F55665AAA83D38C58F58FA9DD0F5CE51351 ();
// 0x00000CBA System.Void TMPro.Examples.TeleType_<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m72D91E3700B8E1F2053A7620793F97E01C1A2E3A ();
// 0x00000CBB System.Boolean TMPro.Examples.TeleType_<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m2839A135AFB4518430283897981AF4C91ABDBC6A ();
// 0x00000CBC System.Object TMPro.Examples.TeleType_<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB3E80ED07333F946FC221C16A77997DF51A80F87 ();
// 0x00000CBD System.Void TMPro.Examples.TeleType_<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mAC8E620F4FCE24A409840017FB003AA1048497EF ();
// 0x00000CBE System.Object TMPro.Examples.TeleType_<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mD6DFD0886CCA250CB95B2828DEEEEE5EA6DC303A ();
// 0x00000CBF System.Void TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_mAF579198F26F3FD002CB7F4919CCB513E2B770E1 ();
// 0x00000CC0 System.Void TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m2B3AB12689498DE28F90CF5DA3D40AA6C31B928E ();
// 0x00000CC1 System.Boolean TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_m16C2594A51B9D102B30646C47111F3476FFBF311 ();
// 0x00000CC2 System.Object TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8B5325041D4E4A3F3D5575373F25A93752D9E514 ();
// 0x00000CC3 System.Void TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_m321E1A1FD7DE7840F8433BF739D3E889FB3E9C7C ();
// 0x00000CC4 System.Object TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9CF6271A74900FFA5BD4027E84D63C164C1650BC ();
// 0x00000CC5 System.Void TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_m5D2D48675C51D6CBD649C1AAD44A80CCA291F310 ();
// 0x00000CC6 System.Void TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m9F97B9D63E30AF41B31E44C66F31FE6B22DDFD4C ();
// 0x00000CC7 System.Boolean TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_m19EB0E15617A1893EEF1916629334CED1D8E9EA1 ();
// 0x00000CC8 System.Object TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m98E45A0AFB76FA46A5DA4E92E3FE114E310D8643 ();
// 0x00000CC9 System.Void TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_m09CF6739322B1DB072CB8CFE591DBBC046008E63 ();
// 0x00000CCA System.Object TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m2083C1001867012CC47AFA8158F00ED15527C603 ();
// 0x00000CCB System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12__ctor_m7E5E121E510EFDCCF0D565EBBF607F80836EBB66 ();
// 0x00000CCC System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_IDisposable_Dispose_mD0C99DF97552E843D1E86CF689372B3759134BB7 ();
// 0x00000CCD System.Boolean TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_MoveNext_mA0FD89A2C5D0867977DAB0896DAB041EDFA64200 ();
// 0x00000CCE System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m735F43BD7983BCC8E417573886ADC91CDED8F29B ();
// 0x00000CCF System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_Reset_mBE24EA2EDAFC5FD22D633595174B122D5F84BB02 ();
// 0x00000CD0 System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_get_Current_m4199EEF69BC21DC6DDC2A0D0323AB70508CB1194 ();
// 0x00000CD1 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__13__ctor_m664F9327A457FB4D53F20172479BB74A4B50675A ();
// 0x00000CD2 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_IDisposable_Dispose_mA301D3B10770F41C50E25B8785CE78B373BBCE7C ();
// 0x00000CD3 System.Boolean TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_MoveNext_m96E4CEDBAD5AA7AC7C96A481502B002BC711725F ();
// 0x00000CD4 System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7609D75AFDAE758DAAE60202465AB3733475352B ();
// 0x00000CD5 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_Reset_m6F0C933685FE89DF2DECC34EDD2FE5AD10542579 ();
// 0x00000CD6 System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_get_Current_m29F3BB8ADAADB8FD750636C535D2B50292E2DE3B ();
// 0x00000CD7 System.Void TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_mD05EA47C6D3F9DC7BE5B4F687A62244E48BC3808 ();
// 0x00000CD8 System.Void TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5E0C78AE94BC2C9BD7818EF0DD43D46ABF8C0172 ();
// 0x00000CD9 System.Boolean TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m8ABBF64D39EB5C5AC87AE0D833B6CBE570444347 ();
// 0x00000CDA System.Object TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m328E88E5192D85CDD2157E26CD15CC0B21149AB6 ();
// 0x00000CDB System.Void TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_mB49B7C6E42E5B1F488ECA85B1B79AC1D6BBC3022 ();
// 0x00000CDC System.Object TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m6966F60B8B8F4540B175D0C145D8A73C89CE5429 ();
// 0x00000CDD System.Void TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m14F85BFAE5EFFAF0BBD6519F6A65B1EE36FC5F0F ();
// 0x00000CDE System.Void TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m36A05E300F1E06AE13E2BFF19C18A59F3E73424A ();
// 0x00000CDF System.Boolean TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m95629D41B32EBFFE935AA88081C00EB958D17F53 ();
// 0x00000CE0 System.Object TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m994AE653E10219CEB601B88CC22E332FF8AD1F36 ();
// 0x00000CE1 System.Void TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mE8D9B5795F095798688B43CF40C04C3FE3F01841 ();
// 0x00000CE2 System.Object TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m12F2FCDBABCD9F41374064CAD53515D2D039EFD0 ();
// 0x00000CE3 System.Void TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m93C153A3293F3E7F9AB47C712F5D8BC9CB0B179D ();
// 0x00000CE4 System.Void TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m6E2E964131F208551826E353B282FBB9477CB002 ();
// 0x00000CE5 System.Boolean TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mD5F89478A4FDEA52E1261CCCF30CC88B0D693407 ();
// 0x00000CE6 System.Object TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE6A7BC75B026CFA96FA1C123E6F7E5A5AAFC46E9 ();
// 0x00000CE7 System.Void TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m28679C8DDFAAD809DD115DB68829543159F47FBD ();
// 0x00000CE8 System.Object TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m85E44906BDBB53959D8A47AE74B74420179AD3EC ();
// 0x00000CE9 System.Void TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m9B592C7897687114428DF0983D5CE52A21051818 ();
// 0x00000CEA System.Void TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m24A98FB24DAE578F96B90B3A6D61D18400A731B1 ();
// 0x00000CEB System.Boolean TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m52F01BB7305705B9DE4309E1A05E2725BA06672E ();
// 0x00000CEC System.Object TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB05E3BDEFD461F6516F45D404DBDEC452C646D37 ();
// 0x00000CED System.Void TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m72086FED04B8AC40B92836B894D6807AFB51BB38 ();
// 0x00000CEE System.Object TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mA1E44018FA1464D953241F5FA414715C9ADE98CF ();
// 0x00000CEF System.Void TMPro.Examples.VertexZoom_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mF154C9990B4AF8DA353F6A8C115C96FB7CB76410 ();
// 0x00000CF0 System.Int32 TMPro.Examples.VertexZoom_<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_mDEE1E28BD53D02CB2E40D0C263BBA65C0B5AC66C ();
// 0x00000CF1 System.Void TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m92C755B17AC00199A759541B62E354765068E7F1 ();
// 0x00000CF2 System.Void TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mBA95580B1808E4EB047CD7F082595E1C5EE930C2 ();
// 0x00000CF3 System.Boolean TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_mA307F1943028D7555032189B48F6289E09D2E220 ();
// 0x00000CF4 System.Object TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5288CCD0BF91027A0CE3AE12D301F3E5189F0DCE ();
// 0x00000CF5 System.Void TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m7C214643A52D954B2755D3477C3D0BFC85DAFF8E ();
// 0x00000CF6 System.Object TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m666CA82A5C974B5AC09F277AFC97A82D1D1C365E ();
// 0x00000CF7 System.Void TMPro.Examples.WarpTextExample_<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m63F411BEA2E513D84AAA701A1EDF0D0322FDE9C4 ();
// 0x00000CF8 System.Void TMPro.Examples.WarpTextExample_<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m3D9631AF92186316351AFF095E5176423B84E25F ();
// 0x00000CF9 System.Boolean TMPro.Examples.WarpTextExample_<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_m24ED53830B643858F6068C6908E7C0E044C671A4 ();
// 0x00000CFA System.Object TMPro.Examples.WarpTextExample_<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07B1ABC445F7EADD1DB5F513857F1607C97AAC12 ();
// 0x00000CFB System.Void TMPro.Examples.WarpTextExample_<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4C7C46CC80C502CC1AD61D2F9DAF34F09226AF06 ();
// 0x00000CFC System.Object TMPro.Examples.WarpTextExample_<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m86693688EEF5E754D44B448E82D87FAB40D6C8B8 ();
// 0x00000CFD System.Void RSG.Promise`1_<>c__DisplayClass24_0::.ctor()
// 0x00000CFE System.Void RSG.Promise`1_<>c__DisplayClass24_0::<InvokeRejectHandlers>b__0(RSG.RejectHandler)
// 0x00000CFF System.Void RSG.Promise`1_<>c__DisplayClass26_0::.ctor()
// 0x00000D00 System.Void RSG.Promise`1_<>c__DisplayClass26_0::<InvokeProgressHandlers>b__0(RSG.ProgressHandler)
// 0x00000D01 System.Void RSG.Promise`1_<>c__DisplayClass34_0::.ctor()
// 0x00000D02 System.Void RSG.Promise`1_<>c__DisplayClass34_0::<Catch>b__0(PromisedT)
// 0x00000D03 System.Void RSG.Promise`1_<>c__DisplayClass34_0::<Catch>b__1(System.Exception)
// 0x00000D04 System.Void RSG.Promise`1_<>c__DisplayClass34_0::<Catch>b__2(System.Single)
// 0x00000D05 System.Void RSG.Promise`1_<>c__DisplayClass35_0::.ctor()
// 0x00000D06 System.Void RSG.Promise`1_<>c__DisplayClass35_0::<Catch>b__0(PromisedT)
// 0x00000D07 System.Void RSG.Promise`1_<>c__DisplayClass35_0::<Catch>b__1(System.Exception)
// 0x00000D08 System.Void RSG.Promise`1_<>c__DisplayClass35_0::<Catch>b__2(System.Single)
// 0x00000D09 System.Void RSG.Promise`1_<>c__DisplayClass42_0`1::.ctor()
// 0x00000D0A System.Void RSG.Promise`1_<>c__DisplayClass42_0`1::<Then>b__0(PromisedT)
// 0x00000D0B System.Void RSG.Promise`1_<>c__DisplayClass42_0`1::<Then>b__2(System.Single)
// 0x00000D0C System.Void RSG.Promise`1_<>c__DisplayClass42_0`1::<Then>b__3(ConvertedT)
// 0x00000D0D System.Void RSG.Promise`1_<>c__DisplayClass42_0`1::<Then>b__4(System.Exception)
// 0x00000D0E System.Void RSG.Promise`1_<>c__DisplayClass42_0`1::<Then>b__1(System.Exception)
// 0x00000D0F System.Void RSG.Promise`1_<>c__DisplayClass42_0`1::<Then>b__5(ConvertedT)
// 0x00000D10 System.Void RSG.Promise`1_<>c__DisplayClass42_0`1::<Then>b__6(System.Exception)
// 0x00000D11 System.Void RSG.Promise`1_<>c__DisplayClass43_0::.ctor()
// 0x00000D12 System.Void RSG.Promise`1_<>c__DisplayClass43_0::<Then>b__0(PromisedT)
// 0x00000D13 System.Void RSG.Promise`1_<>c__DisplayClass43_0::<Then>b__2(System.Single)
// 0x00000D14 System.Void RSG.Promise`1_<>c__DisplayClass43_0::<Then>b__3()
// 0x00000D15 System.Void RSG.Promise`1_<>c__DisplayClass43_0::<Then>b__4(System.Exception)
// 0x00000D16 System.Void RSG.Promise`1_<>c__DisplayClass43_0::<Then>b__1(System.Exception)
// 0x00000D17 System.Void RSG.Promise`1_<>c__DisplayClass44_0::.ctor()
// 0x00000D18 System.Void RSG.Promise`1_<>c__DisplayClass44_0::<Then>b__0(PromisedT)
// 0x00000D19 System.Void RSG.Promise`1_<>c__DisplayClass44_0::<Then>b__1(System.Exception)
// 0x00000D1A System.Void RSG.Promise`1_<>c__DisplayClass45_0`1::.ctor()
// 0x00000D1B RSG.IPromise`1<ConvertedT> RSG.Promise`1_<>c__DisplayClass45_0`1::<Then>b__0(PromisedT)
// 0x00000D1C System.Void RSG.Promise`1_<>c__DisplayClass48_0`1::.ctor()
// 0x00000D1D RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise`1_<>c__DisplayClass48_0`1::<ThenAll>b__0(PromisedT)
// 0x00000D1E System.Void RSG.Promise`1_<>c__DisplayClass49_0::.ctor()
// 0x00000D1F RSG.IPromise RSG.Promise`1_<>c__DisplayClass49_0::<ThenAll>b__0(PromisedT)
// 0x00000D20 System.Void RSG.Promise`1_<>c__DisplayClass51_0::.ctor()
// 0x00000D21 System.Void RSG.Promise`1_<>c__DisplayClass51_0::<All>b__0(RSG.IPromise`1<PromisedT>,System.Int32)
// 0x00000D22 System.Void RSG.Promise`1_<>c__DisplayClass51_0::<All>b__3(System.Exception)
// 0x00000D23 System.Void RSG.Promise`1_<>c__DisplayClass51_1::.ctor()
// 0x00000D24 System.Void RSG.Promise`1_<>c__DisplayClass51_1::<All>b__1(System.Single)
// 0x00000D25 System.Void RSG.Promise`1_<>c__DisplayClass51_1::<All>b__2(PromisedT)
// 0x00000D26 System.Void RSG.Promise`1_<>c__DisplayClass52_0`1::.ctor()
// 0x00000D27 RSG.IPromise`1<ConvertedT> RSG.Promise`1_<>c__DisplayClass52_0`1::<ThenRace>b__0(PromisedT)
// 0x00000D28 System.Void RSG.Promise`1_<>c__DisplayClass53_0::.ctor()
// 0x00000D29 RSG.IPromise RSG.Promise`1_<>c__DisplayClass53_0::<ThenRace>b__0(PromisedT)
// 0x00000D2A System.Void RSG.Promise`1_<>c__DisplayClass55_0::.ctor()
// 0x00000D2B System.Void RSG.Promise`1_<>c__DisplayClass55_0::<Race>b__0(RSG.IPromise`1<PromisedT>,System.Int32)
// 0x00000D2C System.Void RSG.Promise`1_<>c__DisplayClass55_0::<Race>b__2(PromisedT)
// 0x00000D2D System.Void RSG.Promise`1_<>c__DisplayClass55_0::<Race>b__3(System.Exception)
// 0x00000D2E System.Void RSG.Promise`1_<>c__DisplayClass55_1::.ctor()
// 0x00000D2F System.Void RSG.Promise`1_<>c__DisplayClass55_1::<Race>b__1(System.Single)
// 0x00000D30 System.Void RSG.Promise`1_<>c__DisplayClass58_0::.ctor()
// 0x00000D31 System.Void RSG.Promise`1_<>c__DisplayClass58_0::<Finally>b__0(PromisedT)
// 0x00000D32 System.Void RSG.Promise`1_<>c__DisplayClass58_0::<Finally>b__1(System.Exception)
// 0x00000D33 PromisedT RSG.Promise`1_<>c__DisplayClass58_0::<Finally>b__2(PromisedT)
// 0x00000D34 System.Void RSG.Promise`1_<>c__DisplayClass59_0::.ctor()
// 0x00000D35 System.Void RSG.Promise`1_<>c__DisplayClass59_0::<ContinueWith>b__0(PromisedT)
// 0x00000D36 System.Void RSG.Promise`1_<>c__DisplayClass59_0::<ContinueWith>b__1(System.Exception)
// 0x00000D37 System.Void RSG.Promise`1_<>c__DisplayClass60_0`1::.ctor()
// 0x00000D38 System.Void RSG.Promise`1_<>c__DisplayClass60_0`1::<ContinueWith>b__0(PromisedT)
// 0x00000D39 System.Void RSG.Promise`1_<>c__DisplayClass60_0`1::<ContinueWith>b__1(System.Exception)
// 0x00000D3A System.Void RSG.PromiseHelpers_<>c__DisplayClass0_0`2::.ctor()
// 0x00000D3B System.Void RSG.PromiseHelpers_<>c__DisplayClass0_0`2::<All>b__0(T1)
// 0x00000D3C System.Void RSG.PromiseHelpers_<>c__DisplayClass0_0`2::<All>b__1(System.Exception)
// 0x00000D3D System.Void RSG.PromiseHelpers_<>c__DisplayClass0_0`2::<All>b__2(T2)
// 0x00000D3E System.Void RSG.PromiseHelpers_<>c__DisplayClass0_0`2::<All>b__3(System.Exception)
// 0x00000D3F System.Void RSG.PromiseHelpers_<>c__1`3::.cctor()
// 0x00000D40 System.Void RSG.PromiseHelpers_<>c__1`3::.ctor()
// 0x00000D41 RSG.Tuple`3<T1,T2,T3> RSG.PromiseHelpers_<>c__1`3::<All>b__1_0(RSG.Tuple`2<RSG.Tuple`2<T1,T2>,T3>)
// 0x00000D42 System.Void RSG.PromiseHelpers_<>c__2`4::.cctor()
// 0x00000D43 System.Void RSG.PromiseHelpers_<>c__2`4::.ctor()
// 0x00000D44 RSG.Tuple`4<T1,T2,T3,T4> RSG.PromiseHelpers_<>c__2`4::<All>b__2_0(RSG.Tuple`2<RSG.Tuple`2<T1,T2>,RSG.Tuple`2<T3,T4>>)
// 0x00000D45 System.Void RSG.PromiseTimer_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mDD9B2D764A7F06CE14C9AA9268BCB17ED0813404 ();
// 0x00000D46 System.Boolean RSG.PromiseTimer_<>c__DisplayClass3_0::<WaitFor>b__0(RSG.TimeData)
extern void U3CU3Ec__DisplayClass3_0_U3CWaitForU3Eb__0_mA9BB5AA0EEA3D6D9641C8925AD5A9C40581359D6 ();
// 0x00000D47 System.Void RSG.PromiseTimer_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mF7332E74D6031A699FB3E362BCA5449D842E0C77 ();
// 0x00000D48 System.Boolean RSG.PromiseTimer_<>c__DisplayClass4_0::<WaitWhile>b__0(RSG.TimeData)
extern void U3CU3Ec__DisplayClass4_0_U3CWaitWhileU3Eb__0_m0F7701136468BAB79030BC43C81E50638797F1E9 ();
// 0x00000D49 System.Void RSG.Promise_<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_m259D4EC3DAFFD7558DD10749A133A0910C081090 ();
// 0x00000D4A System.Void RSG.Promise_<>c__DisplayClass34_0::<InvokeRejectHandlers>b__0(RSG.RejectHandler)
extern void U3CU3Ec__DisplayClass34_0_U3CInvokeRejectHandlersU3Eb__0_m335E63FDF51ACDBEDAB0629720B1D1B55F1E58EC ();
// 0x00000D4B System.Void RSG.Promise_<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_m45686FC6F8CB9BE85B51D393013062AB66C9D5B7 ();
// 0x00000D4C System.Void RSG.Promise_<>c__DisplayClass36_0::<InvokeProgressHandlers>b__0(RSG.ProgressHandler)
extern void U3CU3Ec__DisplayClass36_0_U3CInvokeProgressHandlersU3Eb__0_m88EAEB7C25F24DEEC94BB7D8BDBA66DD83D13B6F ();
// 0x00000D4D System.Void RSG.Promise_<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_m35729FAE3B44B8B748AEB1980A6AC0B983C749BD ();
// 0x00000D4E System.Void RSG.Promise_<>c__DisplayClass44_0::<Catch>b__0()
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__0_mE53DE47A4C06B730655C86F31EE9ED9D860C4479 ();
// 0x00000D4F System.Void RSG.Promise_<>c__DisplayClass44_0::<Catch>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__1_m4D6EFED1B9CCA059F0ACCF7E6708081A3AB5F086 ();
// 0x00000D50 System.Void RSG.Promise_<>c__DisplayClass44_0::<Catch>b__2(System.Single)
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__2_m98B65D5E97F22A93A8217F6EB085F675377332B0 ();
// 0x00000D51 System.Void RSG.Promise_<>c__DisplayClass51_0`1::.ctor()
// 0x00000D52 System.Void RSG.Promise_<>c__DisplayClass51_0`1::<Then>b__0()
// 0x00000D53 System.Void RSG.Promise_<>c__DisplayClass51_0`1::<Then>b__2(System.Single)
// 0x00000D54 System.Void RSG.Promise_<>c__DisplayClass51_0`1::<Then>b__3(ConvertedT)
// 0x00000D55 System.Void RSG.Promise_<>c__DisplayClass51_0`1::<Then>b__4(System.Exception)
// 0x00000D56 System.Void RSG.Promise_<>c__DisplayClass51_0`1::<Then>b__1(System.Exception)
// 0x00000D57 System.Void RSG.Promise_<>c__DisplayClass51_0`1::<Then>b__5(ConvertedT)
// 0x00000D58 System.Void RSG.Promise_<>c__DisplayClass51_0`1::<Then>b__6(System.Exception)
// 0x00000D59 System.Void RSG.Promise_<>c__DisplayClass52_0::.ctor()
extern void U3CU3Ec__DisplayClass52_0__ctor_m69716E3F6A735C4B4C28087FDA50F5F09DB30E32 ();
// 0x00000D5A System.Void RSG.Promise_<>c__DisplayClass52_0::<Then>b__0()
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__0_m233B389EBFC4D7286B55ED390D159850790E2FDB ();
// 0x00000D5B System.Void RSG.Promise_<>c__DisplayClass52_0::<Then>b__2(System.Single)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__2_mB46295DCAFE8FE7C54F482CA0C86E54680992E15 ();
// 0x00000D5C System.Void RSG.Promise_<>c__DisplayClass52_0::<Then>b__3()
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__3_mAA3309C3626EBF3E8F76009CA4F427C83F953F40 ();
// 0x00000D5D System.Void RSG.Promise_<>c__DisplayClass52_0::<Then>b__4(System.Exception)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__4_mEACDABDAA6FFF67C581B08BD62CD9BC56A0949F9 ();
// 0x00000D5E System.Void RSG.Promise_<>c__DisplayClass52_0::<Then>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__1_mB5C72805A51D0983C5208F1989AA4E63EFD7DDB1 ();
// 0x00000D5F System.Void RSG.Promise_<>c__DisplayClass53_0::.ctor()
extern void U3CU3Ec__DisplayClass53_0__ctor_m222556F76031E532583B13710D1D44D59203F88B ();
// 0x00000D60 System.Void RSG.Promise_<>c__DisplayClass53_0::<Then>b__0()
extern void U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__0_mE9CBBBAD23B511665E4DAF51182BEFF19717D66A ();
// 0x00000D61 System.Void RSG.Promise_<>c__DisplayClass53_0::<Then>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__1_m9FFED99D29BDE440B63A4211688A0E73AC063656 ();
// 0x00000D62 System.Void RSG.Promise_<>c__DisplayClass56_0::.ctor()
extern void U3CU3Ec__DisplayClass56_0__ctor_mE5ABE39A011297619BBB9A417EAFF67C90B3ECAC ();
// 0x00000D63 RSG.IPromise RSG.Promise_<>c__DisplayClass56_0::<ThenAll>b__0()
extern void U3CU3Ec__DisplayClass56_0_U3CThenAllU3Eb__0_mA21AFB4C21A37853627FF773D4D1404AD6786740 ();
// 0x00000D64 System.Void RSG.Promise_<>c__DisplayClass57_0`1::.ctor()
// 0x00000D65 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise_<>c__DisplayClass57_0`1::<ThenAll>b__0()
// 0x00000D66 System.Void RSG.Promise_<>c__DisplayClass59_0::.ctor()
extern void U3CU3Ec__DisplayClass59_0__ctor_m21AA08F1CCD5964BA35DF716C53261025C460112 ();
// 0x00000D67 System.Void RSG.Promise_<>c__DisplayClass59_0::<All>b__0(RSG.IPromise,System.Int32)
extern void U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__0_mFE4AFFCEE3F4C03203991C50CE715F72C79934FA ();
// 0x00000D68 System.Void RSG.Promise_<>c__DisplayClass59_0::<All>b__3(System.Exception)
extern void U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__3_m534C390DE221AAB5B12A7F18178B50982F183196 ();
// 0x00000D69 System.Void RSG.Promise_<>c__DisplayClass59_1::.ctor()
extern void U3CU3Ec__DisplayClass59_1__ctor_m8CADFC14A776F00B5CD88C833CB4AA7F9B18C17B ();
// 0x00000D6A System.Void RSG.Promise_<>c__DisplayClass59_1::<All>b__1(System.Single)
extern void U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__1_mB40874546F22A3FA6FFC04A2386F73B57802B4FD ();
// 0x00000D6B System.Void RSG.Promise_<>c__DisplayClass59_1::<All>b__2()
extern void U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__2_mEB0DC6CE06D97E8415B3AE8BE18D33CB885C127B ();
// 0x00000D6C System.Void RSG.Promise_<>c__DisplayClass60_0::.ctor()
extern void U3CU3Ec__DisplayClass60_0__ctor_mFF337F3758214F04A90EBCCE909A7455EAF86978 ();
// 0x00000D6D RSG.IPromise RSG.Promise_<>c__DisplayClass60_0::<ThenSequence>b__0()
extern void U3CU3Ec__DisplayClass60_0_U3CThenSequenceU3Eb__0_mDADCE479295647D84A792C1F706EA461171D991A ();
// 0x00000D6E System.Void RSG.Promise_<>c__DisplayClass62_0::.ctor()
extern void U3CU3Ec__DisplayClass62_0__ctor_m53A064E5AB7603599616344F2B28ACA37D698011 ();
// 0x00000D6F RSG.IPromise RSG.Promise_<>c__DisplayClass62_0::<Sequence>b__0(RSG.IPromise,System.Func`1<RSG.IPromise>)
extern void U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__0_mE0CCBEC473F4F6DCCA161248B39B9B782B295163 ();
// 0x00000D70 System.Void RSG.Promise_<>c__DisplayClass62_0::<Sequence>b__1()
extern void U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__1_mACDAAF3228380745A6842299964731D368B7FCDF ();
// 0x00000D71 System.Void RSG.Promise_<>c__DisplayClass62_1::.ctor()
extern void U3CU3Ec__DisplayClass62_1__ctor_mA9304C3D108EC6058B368D9DD6E948B06AFE9FDB ();
// 0x00000D72 RSG.IPromise RSG.Promise_<>c__DisplayClass62_1::<Sequence>b__2()
extern void U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__2_m5360F807690CF5E74F146D584D34DE677EE93B33 ();
// 0x00000D73 System.Void RSG.Promise_<>c__DisplayClass62_1::<Sequence>b__3(System.Single)
extern void U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__3_mC8ECE43EBCA660A2853FFB58DB0724268F2E8CDB ();
// 0x00000D74 System.Void RSG.Promise_<>c__DisplayClass63_0::.ctor()
extern void U3CU3Ec__DisplayClass63_0__ctor_m8F351021BA6EECAB7541825324F716AC68B1D9DA ();
// 0x00000D75 RSG.IPromise RSG.Promise_<>c__DisplayClass63_0::<ThenRace>b__0()
extern void U3CU3Ec__DisplayClass63_0_U3CThenRaceU3Eb__0_mC8980C10FD965277E8B6C4B1039DD4F6E26E3219 ();
// 0x00000D76 System.Void RSG.Promise_<>c__DisplayClass64_0`1::.ctor()
// 0x00000D77 RSG.IPromise`1<ConvertedT> RSG.Promise_<>c__DisplayClass64_0`1::<ThenRace>b__0()
// 0x00000D78 System.Void RSG.Promise_<>c__DisplayClass66_0::.ctor()
extern void U3CU3Ec__DisplayClass66_0__ctor_m7EB9C2B14C29EFA69EB45597C94535431002058A ();
// 0x00000D79 System.Void RSG.Promise_<>c__DisplayClass66_0::<Race>b__0(RSG.IPromise,System.Int32)
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__0_m2104C52784C77E1D04926D34D96A74512C4520FB ();
// 0x00000D7A System.Void RSG.Promise_<>c__DisplayClass66_0::<Race>b__2(System.Exception)
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__2_m77A50A6EF9965FB41F5633217FD998B0EF6B863A ();
// 0x00000D7B System.Void RSG.Promise_<>c__DisplayClass66_0::<Race>b__3()
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__3_mA0DDD75707B1382532981C92A7B3531185896EF6 ();
// 0x00000D7C System.Void RSG.Promise_<>c__DisplayClass66_1::.ctor()
extern void U3CU3Ec__DisplayClass66_1__ctor_m888B2508224216BC793FA7DA5E5A044DD8EB4DF5 ();
// 0x00000D7D System.Void RSG.Promise_<>c__DisplayClass66_1::<Race>b__1(System.Single)
extern void U3CU3Ec__DisplayClass66_1_U3CRaceU3Eb__1_mECFDEB9A5F2502DDC4F37A62293E5B2C5532035D ();
// 0x00000D7E System.Void RSG.Promise_<>c__DisplayClass69_0::.ctor()
extern void U3CU3Ec__DisplayClass69_0__ctor_mD4C27332BF7FB89D1D3CD681EFD76D4F5B0E2AA9 ();
// 0x00000D7F System.Void RSG.Promise_<>c__DisplayClass69_0::<Finally>b__0()
extern void U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__0_m298F4EC934D7AC355229E5BF0DB6BFF0CAD58F18 ();
// 0x00000D80 System.Void RSG.Promise_<>c__DisplayClass69_0::<Finally>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__1_mB0F765C7C3721CC58C3F321427114632EF064052 ();
// 0x00000D81 System.Void RSG.Promise_<>c__DisplayClass70_0::.ctor()
extern void U3CU3Ec__DisplayClass70_0__ctor_mB88A3F890AB8B9A3ABE655F0974CDF1CDA5AEF7E ();
// 0x00000D82 System.Void RSG.Promise_<>c__DisplayClass70_0::<ContinueWith>b__0()
extern void U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__0_mE689907E2D5FAA8A7130A21B22CD727F68159F54 ();
// 0x00000D83 System.Void RSG.Promise_<>c__DisplayClass70_0::<ContinueWith>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__1_m593BDA8724A11D73C8391A31328031138C6B2455 ();
// 0x00000D84 System.Void RSG.Promise_<>c__DisplayClass71_0`1::.ctor()
// 0x00000D85 System.Void RSG.Promise_<>c__DisplayClass71_0`1::<ContinueWith>b__0()
// 0x00000D86 System.Void RSG.Promise_<>c__DisplayClass71_0`1::<ContinueWith>b__1(System.Exception)
// 0x00000D87 System.Void RSG.Promises.EnumerableExt_<FromItems>d__2`1::.ctor(System.Int32)
// 0x00000D88 System.Void RSG.Promises.EnumerableExt_<FromItems>d__2`1::System.IDisposable.Dispose()
// 0x00000D89 System.Boolean RSG.Promises.EnumerableExt_<FromItems>d__2`1::MoveNext()
// 0x00000D8A T RSG.Promises.EnumerableExt_<FromItems>d__2`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000D8B System.Void RSG.Promises.EnumerableExt_<FromItems>d__2`1::System.Collections.IEnumerator.Reset()
// 0x00000D8C System.Object RSG.Promises.EnumerableExt_<FromItems>d__2`1::System.Collections.IEnumerator.get_Current()
// 0x00000D8D System.Collections.Generic.IEnumerator`1<T> RSG.Promises.EnumerableExt_<FromItems>d__2`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000D8E System.Collections.IEnumerator RSG.Promises.EnumerableExt_<FromItems>d__2`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000D8F System.Void Proyecto26.HttpBase_<CreateRequestAndRetry>d__0::.ctor(System.Int32)
extern void U3CCreateRequestAndRetryU3Ed__0__ctor_mD37DD8CA8B40E520CF56145DC05E68555FF76E77 ();
// 0x00000D90 System.Void Proyecto26.HttpBase_<CreateRequestAndRetry>d__0::System.IDisposable.Dispose()
extern void U3CCreateRequestAndRetryU3Ed__0_System_IDisposable_Dispose_m080EB28F49C65EC51AC4750E83100207A95D3B86 ();
// 0x00000D91 System.Boolean Proyecto26.HttpBase_<CreateRequestAndRetry>d__0::MoveNext()
extern void U3CCreateRequestAndRetryU3Ed__0_MoveNext_m235030D2A161E2C5C8049CC0174BD9F7B289BF8F ();
// 0x00000D92 System.Void Proyecto26.HttpBase_<CreateRequestAndRetry>d__0::<>m__Finally1()
extern void U3CCreateRequestAndRetryU3Ed__0_U3CU3Em__Finally1_m61B12EF61BE5FB037D21C2224F87B0E4AFF8071B ();
// 0x00000D93 System.Object Proyecto26.HttpBase_<CreateRequestAndRetry>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateRequestAndRetryU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2E8715A83B734FD17A9E241D9A60C9C56B6E9360 ();
// 0x00000D94 System.Void Proyecto26.HttpBase_<CreateRequestAndRetry>d__0::System.Collections.IEnumerator.Reset()
extern void U3CCreateRequestAndRetryU3Ed__0_System_Collections_IEnumerator_Reset_m9E714EF70D4BAD2C62D9074581A6882AF357803D ();
// 0x00000D95 System.Object Proyecto26.HttpBase_<CreateRequestAndRetry>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CCreateRequestAndRetryU3Ed__0_System_Collections_IEnumerator_get_Current_m9BB4AB12471CEF850D24DC9B5B0B4BBDCF973DBE ();
// 0x00000D96 System.Void Proyecto26.HttpBase_<>c__DisplayClass5_0`1::.ctor()
// 0x00000D97 System.Void Proyecto26.HttpBase_<>c__DisplayClass5_0`1::<DefaultUnityWebRequest>b__0(Proyecto26.RequestException,Proyecto26.ResponseHelper)
// 0x00000D98 System.Void Proyecto26.HttpBase_<>c__DisplayClass6_0`1::.ctor()
// 0x00000D99 System.Void Proyecto26.HttpBase_<>c__DisplayClass6_0`1::<DefaultUnityWebRequest>b__0(Proyecto26.RequestException,Proyecto26.ResponseHelper)
// 0x00000D9A System.Void Proyecto26.JsonHelper_Wrapper`1::.ctor()
// 0x00000D9B System.Void Proyecto26.StaticCoroutine_CoroutineHolder::.ctor()
extern void CoroutineHolder__ctor_m368583023672118E43018D9FF54F80B9AFD49656 ();
// 0x00000D9C System.Void Proyecto26.Common.Common_<SendWebRequestWithOptions>d__4::.ctor(System.Int32)
extern void U3CSendWebRequestWithOptionsU3Ed__4__ctor_m0759F9AB8CE86B695FB6D3E582FA7B30A0814CCE ();
// 0x00000D9D System.Void Proyecto26.Common.Common_<SendWebRequestWithOptions>d__4::System.IDisposable.Dispose()
extern void U3CSendWebRequestWithOptionsU3Ed__4_System_IDisposable_Dispose_mDB3D942B1FA63A72AEDCBFBF05E2147161225E0D ();
// 0x00000D9E System.Boolean Proyecto26.Common.Common_<SendWebRequestWithOptions>d__4::MoveNext()
extern void U3CSendWebRequestWithOptionsU3Ed__4_MoveNext_mABDBEBA35842F4574B69061243E67283F120DFB2 ();
// 0x00000D9F System.Object Proyecto26.Common.Common_<SendWebRequestWithOptions>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9CD6D592C9CFF9F6548C8058596A8BB698CD9D9 ();
// 0x00000DA0 System.Void Proyecto26.Common.Common_<SendWebRequestWithOptions>d__4::System.Collections.IEnumerator.Reset()
extern void U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_IEnumerator_Reset_mA14639758B270F07294F673BD202A6DC447F1EE2 ();
// 0x00000DA1 System.Object Proyecto26.Common.Common_<SendWebRequestWithOptions>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_IEnumerator_get_Current_mC8EEB7C6BDB27BB7877DDBFC1196D69BE3B1474E ();
// 0x00000DA2 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m09737A2030CD017312EC64C12F90D25E7F6D889C ();
// 0x00000DA3 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_0::<Start>b__0()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__0_mD9304EE0F587C5B18D9DA6809F5D81BCAB92D43B ();
// 0x00000DA4 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_0::<Start>b__1()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__1_mF26C6F23B1DE3EA3FD39340C393170DBA0B09740 ();
// 0x00000DA5 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_0::<Start>b__21()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__21_m7A53268725D5D9AD30AFBA3B94C22E4FE40BE8ED ();
// 0x00000DA6 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_0::<Start>b__2()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__2_mD6EF48C094516FE0626C9DA7EFAB3AEBBB6A2FE7 ();
// 0x00000DA7 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_0::<Start>b__4()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__4_m2122D73717CF12BDD39D8D0D7C19611B1F21A2C6 ();
// 0x00000DA8 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_0::<Start>b__5()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__5_m749A2A34E2D95A13A8D8E483357FA5D731550CB2 ();
// 0x00000DA9 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_0::<Start>b__6()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__6_m0B499A19BE3F839A9EFC34F6077C38D5D275AF32 ();
// 0x00000DAA System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_0::<Start>b__8()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__8_m4FA25C019E9365E7E37985DC1E7F7907F379A4F2 ();
// 0x00000DAB System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_0::<Start>b__9()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__9_m2455B8C83E029A34C3DBF08ADD33FFFEC7F0D7CC ();
// 0x00000DAC System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_0::<Start>b__10()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__10_m1A7C51B0CC5CE0D268DBB4FDBABB3B3C09B76E8B ();
// 0x00000DAD System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_0::<Start>b__11()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__11_m0C33237140A600D0C563633E550551710271F061 ();
// 0x00000DAE System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_0::<Start>b__13(System.Object)
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__13_m705EEBD4D5DBDE19B8FDFC3A819610EE1D2509CF ();
// 0x00000DAF System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_0::<Start>b__14()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__14_m3A7DB9A92B2D6751BD123943544D8C0DFCB7EBA8 ();
// 0x00000DB0 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_0::<Start>b__15()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__15_m136FDB1FB84D17F0E6052AC8D1482C41E983C7B7 ();
// 0x00000DB1 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_0::<Start>b__16()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__16_m505B89E3C2306856A34C770F72B07D2F36459437 ();
// 0x00000DB2 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_0::<Start>b__17()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__17_m6C5F087D0C92313A411D887D78A23FC35674C5AE ();
// 0x00000DB3 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_0::<Start>b__19(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__19_mBF88042ADFBF15D57513B1F7691F9EBB1AD743E4 ();
// 0x00000DB4 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_0::<Start>b__20()
extern void U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__20_m53E8A86C0D0731F935717A27DA325C1CF5BA06A9 ();
// 0x00000DB5 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_1::.ctor()
extern void U3CU3Ec__DisplayClass22_1__ctor_mF03C28B73060772B45422D83C2CCD891461E7F5A ();
// 0x00000DB6 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_1::<Start>b__23()
extern void U3CU3Ec__DisplayClass22_1_U3CStartU3Eb__23_m0A92AF27DA09C241937F80B5E52AC8A467F592C7 ();
// 0x00000DB7 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_2::.ctor()
extern void U3CU3Ec__DisplayClass22_2__ctor_m0D149B5FF3BAD974378B32F32122517AFE1008F7 ();
// 0x00000DB8 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass22_2::<Start>b__24(System.Object)
extern void U3CU3Ec__DisplayClass22_2_U3CStartU3Eb__24_m244CFBB7D843FDE489A08F6438F480F77705B733 ();
// 0x00000DB9 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c::.cctor()
extern void U3CU3Ec__cctor_mBB9151D0770DA4B6C0D5DE0DF4505B0B1473C792 ();
// 0x00000DBA System.Void DentedPixel.LTExamples.TestingUnitTests_<>c::.ctor()
extern void U3CU3Ec__ctor_m7292CCD570F1D648747270AA2E9366DC8BEA76AE ();
// 0x00000DBB System.Void DentedPixel.LTExamples.TestingUnitTests_<>c::<Start>b__22_3()
extern void U3CU3Ec_U3CStartU3Eb__22_3_mBE510D88050EF0EAE74485B1380103286F1E2ACF ();
// 0x00000DBC System.Void DentedPixel.LTExamples.TestingUnitTests_<>c::<Start>b__22_22()
extern void U3CU3Ec_U3CStartU3Eb__22_22_mC2B2676DF6376C960A2168BB616C30FAF9572F46 ();
// 0x00000DBD System.Void DentedPixel.LTExamples.TestingUnitTests_<>c::<Start>b__22_7()
extern void U3CU3Ec_U3CStartU3Eb__22_7_m333F34EE7AB7B6BA9AFB42616E6F31AD16648DFA ();
// 0x00000DBE System.Void DentedPixel.LTExamples.TestingUnitTests_<>c::<Start>b__22_12(System.Single)
extern void U3CU3Ec_U3CStartU3Eb__22_12_mE9BCA088D81630DCA6B4A29493F717166DE4BDC2 ();
// 0x00000DBF System.Void DentedPixel.LTExamples.TestingUnitTests_<>c::<Start>b__22_18()
extern void U3CU3Ec_U3CStartU3Eb__22_18_m0E1A87851B8F186E1D037D6019BF2B5F03BE4AE7 ();
// 0x00000DC0 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c::<pauseTimeNow>b__26_0()
extern void U3CU3Ec_U3CpauseTimeNowU3Eb__26_0_m0067B26B1415DF3C65658EF775E43DE444C9F89E ();
// 0x00000DC1 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_m277D86C124C510C86F124C314062F6CA37F04780 ();
// 0x00000DC2 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass24_0::<timeBasedTesting>b__0()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__0_m1C7842AC0922C8535A9D996B445C45BD68ABC7CB ();
// 0x00000DC3 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass24_0::<timeBasedTesting>b__1()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__1_m3B2F75CA971620227F026AC96214BE0E9F33EE3B ();
// 0x00000DC4 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass24_0::<timeBasedTesting>b__2(System.Single)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__2_mA4939E85F8BBD95C7A69CCA149A6F37BC65799FB ();
// 0x00000DC5 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass24_0::<timeBasedTesting>b__3()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__3_mA4873B5E9E38B22E691207FB286333F85B549ACB ();
// 0x00000DC6 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass24_0::<timeBasedTesting>b__4()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__4_m14C5D280141ADAF244B2213E9CE61686FC470854 ();
// 0x00000DC7 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass24_0::<timeBasedTesting>b__5()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__5_m25B454BD3DB25DF33E9961063468DC070C809146 ();
// 0x00000DC8 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass24_0::<timeBasedTesting>b__6(System.Single)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__6_m33BFB7BC2272AE16E35A6AF6271EAAD019DE7D5B ();
// 0x00000DC9 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass24_0::<timeBasedTesting>b__7()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__7_m9DE96F4CFC41638644635C5699FDA81811802151 ();
// 0x00000DCA System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass24_0::<timeBasedTesting>b__13()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__13_mC7412BB7516D3AAAA92794BF489DD6224FDFA38A ();
// 0x00000DCB System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass24_0::<timeBasedTesting>b__14(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__14_mE19037EF79A1618B0ABFA5EBB8B7C7167BF59D81 ();
// 0x00000DCC System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass24_0::<timeBasedTesting>b__15(System.Object)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__15_m5B4E117FD86BFF9E7C043AC7AF54886AFE964EC2 ();
// 0x00000DCD System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass24_0::<timeBasedTesting>b__16()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__16_mD4F03BFC2B675FB03F1475391F156ACFF2D9AEA9 ();
// 0x00000DCE System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass24_0::<timeBasedTesting>b__8()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__8_mABF6F7F053A21322BA9C371F9FFF930148E42C63 ();
// 0x00000DCF System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass24_0::<timeBasedTesting>b__9(System.Single)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__9_m0832DF1094C24C2B4E50D843130FD5C09B2F33C5 ();
// 0x00000DD0 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass24_0::<timeBasedTesting>b__10()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__10_mDA5269BA5F2FB4EB59DFCCF9E6C806EF781EF838 ();
// 0x00000DD1 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass24_0::<timeBasedTesting>b__11(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__11_m451E7955EC34F408DFAC4365893C10BCCD1F0FFE ();
// 0x00000DD2 System.Void DentedPixel.LTExamples.TestingUnitTests_<>c__DisplayClass24_0::<timeBasedTesting>b__12()
extern void U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__12_m58715E24DE0A08CD597D94E0B4DABAA49D93F71C ();
// 0x00000DD3 System.Void DentedPixel.LTExamples.TestingUnitTests_<timeBasedTesting>d__24::.ctor(System.Int32)
extern void U3CtimeBasedTestingU3Ed__24__ctor_mCE839055D4DA089ADAB24FBCCA8FCCB9307C948C ();
// 0x00000DD4 System.Void DentedPixel.LTExamples.TestingUnitTests_<timeBasedTesting>d__24::System.IDisposable.Dispose()
extern void U3CtimeBasedTestingU3Ed__24_System_IDisposable_Dispose_m57943CFF04FA4711B6B63A93258509F56768483A ();
// 0x00000DD5 System.Boolean DentedPixel.LTExamples.TestingUnitTests_<timeBasedTesting>d__24::MoveNext()
extern void U3CtimeBasedTestingU3Ed__24_MoveNext_m40360D1C97BFDAB90C0E02A76CBAE4E3C2EC99B5 ();
// 0x00000DD6 System.Object DentedPixel.LTExamples.TestingUnitTests_<timeBasedTesting>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtimeBasedTestingU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB206B6F0564F23D404E3E0F14B01E7359C2EAA8B ();
// 0x00000DD7 System.Void DentedPixel.LTExamples.TestingUnitTests_<timeBasedTesting>d__24::System.Collections.IEnumerator.Reset()
extern void U3CtimeBasedTestingU3Ed__24_System_Collections_IEnumerator_Reset_m90D3F19EAF2AE82FB852651B1A13B8EC21FA77D0 ();
// 0x00000DD8 System.Object DentedPixel.LTExamples.TestingUnitTests_<timeBasedTesting>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CtimeBasedTestingU3Ed__24_System_Collections_IEnumerator_get_Current_mBD52D1EB4D613ADB298D9E9A684232F65AC7468F ();
// 0x00000DD9 System.Void DentedPixel.LTExamples.TestingUnitTests_<lotsOfCancels>d__25::.ctor(System.Int32)
extern void U3ClotsOfCancelsU3Ed__25__ctor_mB107D0BCF384F8F0CD3DF49E8E91829D271AB34D ();
// 0x00000DDA System.Void DentedPixel.LTExamples.TestingUnitTests_<lotsOfCancels>d__25::System.IDisposable.Dispose()
extern void U3ClotsOfCancelsU3Ed__25_System_IDisposable_Dispose_mC7CD787978EEAB84A938E833CE818B59FC967926 ();
// 0x00000DDB System.Boolean DentedPixel.LTExamples.TestingUnitTests_<lotsOfCancels>d__25::MoveNext()
extern void U3ClotsOfCancelsU3Ed__25_MoveNext_m6FB026E807483B7AA2A7CF23F6940E0E9DD8B1FE ();
// 0x00000DDC System.Object DentedPixel.LTExamples.TestingUnitTests_<lotsOfCancels>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3ClotsOfCancelsU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D4392AC16098A343CB609FBF9FA41618CC01B3E ();
// 0x00000DDD System.Void DentedPixel.LTExamples.TestingUnitTests_<lotsOfCancels>d__25::System.Collections.IEnumerator.Reset()
extern void U3ClotsOfCancelsU3Ed__25_System_Collections_IEnumerator_Reset_mFF39885CA3EDBEF256FBE637E5C9E4230518A07A ();
// 0x00000DDE System.Object DentedPixel.LTExamples.TestingUnitTests_<lotsOfCancels>d__25::System.Collections.IEnumerator.get_Current()
extern void U3ClotsOfCancelsU3Ed__25_System_Collections_IEnumerator_get_Current_m94A9DE71BD10C268704724FC2B1E588F75D3C78D ();
// 0x00000DDF System.Void DentedPixel.LTExamples.TestingUnitTests_<pauseTimeNow>d__26::.ctor(System.Int32)
extern void U3CpauseTimeNowU3Ed__26__ctor_m5F319CD4B560891783427EB803E77B4A8ACE0FB6 ();
// 0x00000DE0 System.Void DentedPixel.LTExamples.TestingUnitTests_<pauseTimeNow>d__26::System.IDisposable.Dispose()
extern void U3CpauseTimeNowU3Ed__26_System_IDisposable_Dispose_mAB85DDD605A77343E6464DA39BF37EDEFB254086 ();
// 0x00000DE1 System.Boolean DentedPixel.LTExamples.TestingUnitTests_<pauseTimeNow>d__26::MoveNext()
extern void U3CpauseTimeNowU3Ed__26_MoveNext_m49B3D9FE98B48DE72D1FA96F2C22FFED13EEB9E5 ();
// 0x00000DE2 System.Object DentedPixel.LTExamples.TestingUnitTests_<pauseTimeNow>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CpauseTimeNowU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m372428EA5DCAD59137F4E5AFC1C5566DDB6E1625 ();
// 0x00000DE3 System.Void DentedPixel.LTExamples.TestingUnitTests_<pauseTimeNow>d__26::System.Collections.IEnumerator.Reset()
extern void U3CpauseTimeNowU3Ed__26_System_Collections_IEnumerator_Reset_m3DE5854E0C5EAB304CF501FD2D42B92798B6233D ();
// 0x00000DE4 System.Object DentedPixel.LTExamples.TestingUnitTests_<pauseTimeNow>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CpauseTimeNowU3Ed__26_System_Collections_IEnumerator_get_Current_m1B325E4F8BE919D78719C70EFC817D07842F3C7C ();
// 0x00000DE5 System.Void Lean.Touch.LeanFingerDown_LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mB87E6F24934BDC577CFF4AA5773BE5C4F9B32648 ();
// 0x00000DE6 System.Void Lean.Touch.LeanFingerHeld_LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mAAD46C660F8B9EBAF6B2BED711CA41A22348E49F ();
// 0x00000DE7 System.Void Lean.Touch.LeanFingerHeld_Link::.ctor()
extern void Link__ctor_m1A8DC5DFE60F1BE6C77691E4247B58771D6566BE ();
// 0x00000DE8 System.Void Lean.Touch.LeanFingerLine_Vector3Vector3Event::.ctor()
extern void Vector3Vector3Event__ctor_m0312381CC23B5EF1DC054A58A590AA22F98562FF ();
// 0x00000DE9 System.Void Lean.Touch.LeanFingerLine_Vector3Event::.ctor()
extern void Vector3Event__ctor_m85527E38274A76342FE4415FFE0117F51BC9049E ();
// 0x00000DEA System.Void Lean.Touch.LeanFingerSet_LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mF30FFEAE76482E248DB2F1D4E6F6FEBC0E6A8D2E ();
// 0x00000DEB System.Void Lean.Touch.LeanFingerSet_Vector2Event::.ctor()
extern void Vector2Event__ctor_m83F82A779D5F0CAA8D422563F15715E3D9160099 ();
// 0x00000DEC System.Void Lean.Touch.LeanFingerSwipe_LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m28D3CC40458A74DC26E67B1E33F9BE46622E1B6D ();
// 0x00000DED System.Void Lean.Touch.LeanFingerSwipe_Vector2Event::.ctor()
extern void Vector2Event__ctor_mF448C7E550E06DFCFE770E1DDA821BDBE65E555C ();
// 0x00000DEE System.Void Lean.Touch.LeanFingerTap_LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mF6789F45618C6F2324906DB871A31C2BC5296638 ();
// 0x00000DEF System.Void Lean.Touch.LeanFingerTrail_FingerData::.ctor()
extern void FingerData__ctor_m9BB8C75E525D6102ECC893E1DF8E5CC837D27910 ();
// 0x00000DF0 System.Void Lean.Touch.LeanFingerUp_LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m5127F698F385886B67EB8033736C1EBAC9B5DF28 ();
// 0x00000DF1 System.Void Lean.Touch.LeanSelectable_LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m400AFD1A3CB8DA7C354345AC297F0466278164FF ();
// 0x00000DF2 System.Void Lean.Common.Examples.LeanCircuit_Path::.ctor()
extern void Path__ctor_m45C894ABA0FC3B1BE649EDDA63AC852A3F111965 ();
// 0x00000DF3 System.Boolean Lean.Common.Examples.LeanCircuit_Node::Increment(UnityEngine.Vector3)
extern void Node_Increment_m9E9A92D1E02E7B5AC2175B0B2FB79D23ED458222 ();
// 0x00000DF4 System.Void Lean.Common.Examples.LeanCircuit_Node::.ctor()
extern void Node__ctor_m0B2B80358AC583E82186000D0915FBB1DAE123A8 ();
// 0x00000DF5 System.Void Lean.Common.Examples.LeanMarker_Reference`1::.ctor(System.String)
// 0x00000DF6 T Lean.Common.Examples.LeanMarker_Reference`1::get_Instance()
// 0x00000DF7 System.Void Lean.Common.Examples.LeanMarker_Reference`1::Build(Lean.Common.Examples.LeanMarker)
// 0x00000DF8 System.Void Lean.Common.Examples.LeanMarker_Reference`1::Find()
// 0x00000DF9 System.Boolean SimpleJSON.JSONNode_Enumerator::get_IsValid()
extern void Enumerator_get_IsValid_mE03DD42D0DEDE3493D9405D9EE535AEA5FE446CC_AdjustorThunk ();
// 0x00000DFA System.Void SimpleJSON.JSONNode_Enumerator::.ctor(System.Collections.Generic.List`1_Enumerator<SimpleJSON.JSONNode>)
extern void Enumerator__ctor_m05355A819BFE7107717A69957C3A255273449385_AdjustorThunk ();
// 0x00000DFB System.Void SimpleJSON.JSONNode_Enumerator::.ctor(System.Collections.Generic.Dictionary`2_Enumerator<System.String,SimpleJSON.JSONNode>)
extern void Enumerator__ctor_m4E912D002FEBB7AD53C59AFAF90DF4917BD85B02_AdjustorThunk ();
// 0x00000DFC System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONNode_Enumerator::get_Current()
extern void Enumerator_get_Current_mE01930C040D8565C5DF7682E6415FC48BD388B94_AdjustorThunk ();
// 0x00000DFD System.Boolean SimpleJSON.JSONNode_Enumerator::MoveNext()
extern void Enumerator_MoveNext_m5F7677A228DDBA16F1D18078933A17ABAA823C75_AdjustorThunk ();
// 0x00000DFE System.Void SimpleJSON.JSONNode_ValueEnumerator::.ctor(System.Collections.Generic.List`1_Enumerator<SimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_m7A7A4CA011E1ECFB43F7C7DAB985DF2B3DB921EE_AdjustorThunk ();
// 0x00000DFF System.Void SimpleJSON.JSONNode_ValueEnumerator::.ctor(System.Collections.Generic.Dictionary`2_Enumerator<System.String,SimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_m1CDBED8B76E22050F7FCED0CEDB8A7B26C94B515_AdjustorThunk ();
// 0x00000E00 System.Void SimpleJSON.JSONNode_ValueEnumerator::.ctor(SimpleJSON.JSONNode_Enumerator)
extern void ValueEnumerator__ctor_m4D8F3303471408B2B130036835DA3AE22F3EA430_AdjustorThunk ();
// 0x00000E01 SimpleJSON.JSONNode SimpleJSON.JSONNode_ValueEnumerator::get_Current()
extern void ValueEnumerator_get_Current_m151356ECDEFD55E1FFF524FE25A0F489878A10AB_AdjustorThunk ();
// 0x00000E02 System.Boolean SimpleJSON.JSONNode_ValueEnumerator::MoveNext()
extern void ValueEnumerator_MoveNext_m80DB8324975D9A8428322BB7B34D1D45ACD5176B_AdjustorThunk ();
// 0x00000E03 SimpleJSON.JSONNode_ValueEnumerator SimpleJSON.JSONNode_ValueEnumerator::GetEnumerator()
extern void ValueEnumerator_GetEnumerator_m3C2328208D593CFC614F7D42AC30DDB1047A9E1A_AdjustorThunk ();
// 0x00000E04 System.Void SimpleJSON.JSONNode_KeyEnumerator::.ctor(System.Collections.Generic.List`1_Enumerator<SimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_m7562FD1BB37B81B6EC866F2D4B91D449D4A2617B_AdjustorThunk ();
// 0x00000E05 System.Void SimpleJSON.JSONNode_KeyEnumerator::.ctor(System.Collections.Generic.Dictionary`2_Enumerator<System.String,SimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_m4C89FED3394614DF05CA0BB2E5348A502F508191_AdjustorThunk ();
// 0x00000E06 System.Void SimpleJSON.JSONNode_KeyEnumerator::.ctor(SimpleJSON.JSONNode_Enumerator)
extern void KeyEnumerator__ctor_mC9EAD48CFE6FE0ABE897E6EA4BC1EE0F240758EE_AdjustorThunk ();
// 0x00000E07 SimpleJSON.JSONNode SimpleJSON.JSONNode_KeyEnumerator::get_Current()
extern void KeyEnumerator_get_Current_m5CFAB7F891D0DDC9B3A5DF1DE2038F6DAF292AEB_AdjustorThunk ();
// 0x00000E08 System.Boolean SimpleJSON.JSONNode_KeyEnumerator::MoveNext()
extern void KeyEnumerator_MoveNext_m7DE4E6044266B4C47EDE88397169D03DF60BF244_AdjustorThunk ();
// 0x00000E09 SimpleJSON.JSONNode_KeyEnumerator SimpleJSON.JSONNode_KeyEnumerator::GetEnumerator()
extern void KeyEnumerator_GetEnumerator_mF33634E45FC6CCD3ACDE273AEB14CAAA35009A91_AdjustorThunk ();
// 0x00000E0A System.Void SimpleJSON.JSONNode_LinqEnumerator::.ctor(SimpleJSON.JSONNode)
extern void LinqEnumerator__ctor_m1A1EE79F7821C0ED795C46EEDB7783176040EBA3 ();
// 0x00000E0B System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONNode_LinqEnumerator::get_Current()
extern void LinqEnumerator_get_Current_m394350654CC60ACA7DBE5CE90AD892927BFC33B2 ();
// 0x00000E0C System.Object SimpleJSON.JSONNode_LinqEnumerator::System.Collections.IEnumerator.get_Current()
extern void LinqEnumerator_System_Collections_IEnumerator_get_Current_m31927FCDF96A4B90105E25B324E2CAE85134A8ED ();
// 0x00000E0D System.Boolean SimpleJSON.JSONNode_LinqEnumerator::MoveNext()
extern void LinqEnumerator_MoveNext_mFC180A5717730454E62E0286F5D8D9AE72BDE393 ();
// 0x00000E0E System.Void SimpleJSON.JSONNode_LinqEnumerator::Dispose()
extern void LinqEnumerator_Dispose_mBF5FB73384994A555175F46E352478281AB91C63 ();
// 0x00000E0F System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>> SimpleJSON.JSONNode_LinqEnumerator::GetEnumerator()
extern void LinqEnumerator_GetEnumerator_mFF6F7F4A9E1900EEDDCF167EB48EFC325A4B0066 ();
// 0x00000E10 System.Void SimpleJSON.JSONNode_LinqEnumerator::Reset()
extern void LinqEnumerator_Reset_m06A57EF3840D4EE2704E0CB4DFB8EB9B7251EFA4 ();
// 0x00000E11 System.Collections.IEnumerator SimpleJSON.JSONNode_LinqEnumerator::System.Collections.IEnumerable.GetEnumerator()
extern void LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_m0F3F335982F9A8EA3C287B0D15DBBF65BD5E7D7E ();
// 0x00000E12 System.Void SimpleJSON.JSONNode_<get_Children>d__39::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__39__ctor_m08A6302695FA8FFED3961CEC013FAE54A844A596 ();
// 0x00000E13 System.Void SimpleJSON.JSONNode_<get_Children>d__39::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__39_System_IDisposable_Dispose_mE5C32E35EC7D527C9C11C20F309E38FF92B59D5E ();
// 0x00000E14 System.Boolean SimpleJSON.JSONNode_<get_Children>d__39::MoveNext()
extern void U3Cget_ChildrenU3Ed__39_MoveNext_m631A849E9C817F907E77C1194938FA563F554D55 ();
// 0x00000E15 SimpleJSON.JSONNode SimpleJSON.JSONNode_<get_Children>d__39::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mA653A80D0CCEF661004013DF85936DDE4D4F803A ();
// 0x00000E16 System.Void SimpleJSON.JSONNode_<get_Children>d__39::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_Reset_mE69EE6153C71479E77F6CDF38ABDCF6984C5ABA6 ();
// 0x00000E17 System.Object SimpleJSON.JSONNode_<get_Children>d__39::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_get_Current_m5E565021D4E7CF1B6D7902ED534C33442460D63C ();
// 0x00000E18 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode_<get_Children>d__39::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m31645C0A0DDD485DFF26CF141E02DFB3A8D20DAE ();
// 0x00000E19 System.Collections.IEnumerator SimpleJSON.JSONNode_<get_Children>d__39::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerable_GetEnumerator_mD0A55464C40549A568E050A922E19E5969E161A7 ();
// 0x00000E1A System.Void SimpleJSON.JSONNode_<get_DeepChildren>d__41::.ctor(System.Int32)
extern void U3Cget_DeepChildrenU3Ed__41__ctor_m0712F6AA6A7F37973B3F66EB37691B9F4783814D ();
// 0x00000E1B System.Void SimpleJSON.JSONNode_<get_DeepChildren>d__41::System.IDisposable.Dispose()
extern void U3Cget_DeepChildrenU3Ed__41_System_IDisposable_Dispose_mBBC1E7AEB8F9B7AC04D165ECAE9D183F19F8296A ();
// 0x00000E1C System.Boolean SimpleJSON.JSONNode_<get_DeepChildren>d__41::MoveNext()
extern void U3Cget_DeepChildrenU3Ed__41_MoveNext_m0DF534784D92B51FE386064197554F9491459867 ();
// 0x00000E1D System.Void SimpleJSON.JSONNode_<get_DeepChildren>d__41::<>m__Finally1()
extern void U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally1_m87F47E29A82391F9EABDC2960423809A63D3DA64 ();
// 0x00000E1E System.Void SimpleJSON.JSONNode_<get_DeepChildren>d__41::<>m__Finally2()
extern void U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally2_m948556C997EAEE00AC4050FA6DA476FF8AF2D65A ();
// 0x00000E1F SimpleJSON.JSONNode SimpleJSON.JSONNode_<get_DeepChildren>d__41::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m436DE502A96515949F5C5027B1F2810165801B8A ();
// 0x00000E20 System.Void SimpleJSON.JSONNode_<get_DeepChildren>d__41::System.Collections.IEnumerator.Reset()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_Reset_mD10B0B2BBC87D7BFDC59F237563C3515A19F612F ();
// 0x00000E21 System.Object SimpleJSON.JSONNode_<get_DeepChildren>d__41::System.Collections.IEnumerator.get_Current()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_get_Current_m5D325EA5E1D268FA593EB324238DB117FA6CBC34 ();
// 0x00000E22 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode_<get_DeepChildren>d__41::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m057AEFA5ACBFD6128FBAD7D76EFE7830B59B55BE ();
// 0x00000E23 System.Collections.IEnumerator SimpleJSON.JSONNode_<get_DeepChildren>d__41::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerable_GetEnumerator_m981A7B811CFD4CAD91222704760D1EF0F3423FA7 ();
// 0x00000E24 System.Void SimpleJSON.JSONArray_<get_Children>d__22::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__22__ctor_m06E8E8B2FC52FE3018AE3269AAC642BCFA34C707 ();
// 0x00000E25 System.Void SimpleJSON.JSONArray_<get_Children>d__22::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_m80DF5B4FDDA2D5154F34AFDF6AA28B727EF7D8B2 ();
// 0x00000E26 System.Boolean SimpleJSON.JSONArray_<get_Children>d__22::MoveNext()
extern void U3Cget_ChildrenU3Ed__22_MoveNext_m57D914F733B5D614D5CAFCDD73DDFA157A03EAA2 ();
// 0x00000E27 System.Void SimpleJSON.JSONArray_<get_Children>d__22::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__22_U3CU3Em__Finally1_m85D02220871BC7AD5F7FB9F627A25273631A999D ();
// 0x00000E28 SimpleJSON.JSONNode SimpleJSON.JSONArray_<get_Children>d__22::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m5D1768596B7404BC7B060F244F5D88C3528E52BB ();
// 0x00000E29 System.Void SimpleJSON.JSONArray_<get_Children>d__22::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_mF86B3D72C5015C52CB57E5FF8C90CD58BC75192D ();
// 0x00000E2A System.Object SimpleJSON.JSONArray_<get_Children>d__22::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_m527FCAE88CA8E2F6E28FBDC52B4C4884832DDD29 ();
// 0x00000E2B System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray_<get_Children>d__22::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mA802939A320EA3BD49E49507CF21D4056A78518A ();
// 0x00000E2C System.Collections.IEnumerator SimpleJSON.JSONArray_<get_Children>d__22::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_m4A7EE4525D2D51F084E7489AA33ABEC1DCEA6479 ();
// 0x00000E2D System.Void SimpleJSON.JSONObject_<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mF8A6932A8B490316F55E33EC122C544BB332B12A ();
// 0x00000E2E System.Boolean SimpleJSON.JSONObject_<>c__DisplayClass21_0::<Remove>b__0(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern void U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_m78AEA81A69084D7B4B4D6A1DE8558D50B065B7DB ();
// 0x00000E2F System.Void SimpleJSON.JSONObject_<get_Children>d__23::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__23__ctor_m41067DDF3D08F0BE8CED3D9E7872A7FBA8316CDC ();
// 0x00000E30 System.Void SimpleJSON.JSONObject_<get_Children>d__23::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_mE9BEDEC6724C22575197031050734765574F3973 ();
// 0x00000E31 System.Boolean SimpleJSON.JSONObject_<get_Children>d__23::MoveNext()
extern void U3Cget_ChildrenU3Ed__23_MoveNext_m92ED45D5D77C97089D74BC1B35568C8ED33176DC ();
// 0x00000E32 System.Void SimpleJSON.JSONObject_<get_Children>d__23::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__23_U3CU3Em__Finally1_m4A46673EE413AF8FAD22D64152BEBE5EE76A5E65 ();
// 0x00000E33 SimpleJSON.JSONNode SimpleJSON.JSONObject_<get_Children>d__23::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mD50C7203AF0D12B0EDFB7F1AABB9B5BBBD327243 ();
// 0x00000E34 System.Void SimpleJSON.JSONObject_<get_Children>d__23::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_mF4C27D1B6113C761A688634D1A4C9C5058280CEC ();
// 0x00000E35 System.Object SimpleJSON.JSONObject_<get_Children>d__23::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_m2EE583F6F365FBAA04E82B8963501E8A3DA58C70 ();
// 0x00000E36 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONObject_<get_Children>d__23::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m948470928F9C800DBE8DB201F697EE2F36254AA3 ();
// 0x00000E37 System.Collections.IEnumerator SimpleJSON.JSONObject_<get_Children>d__23::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_m5D486E26CEBA6C8C62417BFB778CAD59F2306BCE ();
static Il2CppMethodPointer s_methodPointers[3639] = 
{
	ApiExample_Update_m3B2E77CA197799B03F83F1B29D466EC4976AC1CC,
	ApiExample_FixedUpdate_m4F49759DE3C2BB60CD28807C5852FF3BF7558DD7,
	ApiExample_Start_mAF6A04D0D0B7297C5C5DD6A11D7B4A722C880F0C,
	ApiExample_ReturnXL_Link_mA8A7868FFC871033F108C61F916FC1E06F5A8603,
	ApiExample_CropTex_m89926E40021BEDD468EBE559B3B7AC937EED86ED,
	ApiExample_GetDetailProduct_mD16F1D07CFFF3926B6AC955BBE701D6113E38453,
	ApiExample_TestCategories_m87E82886B283BC4EA03008D76C45E68445D09F2F,
	ApiExample_GETTexWithTimer_m44FB46DD924C3B349AF7FE12BB1E18661690A0EB,
	ApiExample_GETWITHHEADER_mC0914C9ABECDAAD63A334FEF83ECC80B645F36DA,
	ApiExample_DLExample_m0408D673C3592FBC07409053874594CED0146DE1,
	ApiExample_Encode_mB19386CEFF0ED408DD6F07A4BE39C7B41DE94349,
	ApiExample_generateQR_m5E16397D3E071C8843B89C191197020A18EC8404,
	ApiExample_DownloadLP_mE166EC15058DE3BA2CF698458CD78FB485FD281B,
	ApiExample_DownloadContinuouslyProductDetail_m1CB409FC22BF1C36049C3BDE5F1DF5AE68B5C960,
	ApiExample_DownloadProductNDetail_mAD4411CD0F714EE7E0BB20E922DCE7F98AFA887F,
	ApiExample_Downloading_mCE39887CF1662BF30445FB50DE802B801463D677,
	ApiExample_GetRequest_m81264FB9877B70E2CD2BD7143A14813196BEF953,
	ApiExample_DownloadReadImageAsync_m2F02B230074A1477C5031C4A95BF383717149B2B,
	ApiExample_ReadImageAsync_mD92832EB192D20F20E5F7BB92247A63BFF8382B8,
	ApiExample_ReduceTexture2_m53B91140DC9847BE0B127F6CD6301442E8FB82F5,
	ApiExample_ReduceTexture_m941EC2E650758C1A74F425397366B9A4EE7067F2,
	ApiExample_PreparingDatas_m133775AEF6D54A48A0A1B0F972C8F3653FBAFD45,
	ApiExample_GetTexture_mD7BDBC2232403E5FC626A4EE8A66E748D523255A,
	ApiExample_GetStyleIt_mAB5B4159F9F72CF7316FD2C3779B97B582420B9A,
	ApiExample_GetSimilarItems_m0EF41B26486AF562AA94C970DB358419A5DC5AFB,
	ApiExample__ctor_m061AF5CFC6883F332A265A8A733691941BA0496C,
	ApiManager_Awake_mA377DC3B82D5260B87106E695C44F82012521158,
	ApiManager_Start_m7D54DEE32C56D90294D4229CF34C0DE021A6C3C6,
	ApiManager_ROLatestProduct_m9501321C4F69267EA0E00CAB3870830F7B565E51,
	ApiManager_ROProductDetail_mACC478C78FE2428F4ABBBBF19FEB62927BE605D9,
	ApiManager_ROSimilarProduct_m596D333C2F8BB21AEDCBE2A6814F9FF2444B831F,
	ApiManager_ROStyleWithIt_m7C0FE8761C87113507101503AF05BDAF297CC19A,
	ApiManager_StyleWithItBody_m06B8F0E5988EE1DF1FA237F6C7686D9F455E937C,
	ApiManager_SimilarProductBody_m8B3A843418E08D397770DCCB30D9BB2F67DDDAB0,
	ApiManager_LatestProductBody_m423F2CF62B4BF8F14589556940D1B4748D3BF0C7,
	ApiManager_ProductDetailBody_m8C2A4DEA8BEDF781E0AEDA7D4A86FF214A40490C,
	ApiManager_SaveLatestProduct_m177AB9859FE53640A0833ECE326D30494130116C,
	ApiManager_SaveProductDetail_m9206DDE74A252D3DA1B9545641AB046C24F5012B,
	ApiManager_FindWeirdChar_m2E28BC3D56C53A7ECFB60562DEC07AD67D1D393E,
	ApiManager_Encode_mB486F36E89E55962836C8F347C384FE2AC55C8C0,
	ApiManager_generateQR_m07C4623AC03EDB3F55FB9637F7BFFCB6D84C4B9E,
	NULL,
	ApiManager__ctor_m191ED4BD1DAA72F9C42E142FFFE1103F0665E786,
	DetailProduct__ctor_m86280B0407EDB756B0E7045F562BD06AC185D0E0,
	TAG__ctor_mF4A24B9D1E4D5CCA8D402975E943754F3768DF18,
	DATA__ctor_m49281891E81412C15A73B01152D6B47E942ACBC0,
	LoadingScene_Awake_mCDEEB637850BBAE4FBE307841D0EE50705B3C331,
	LoadingScene_Start_m7341A0A7817AEF05816C2920042FBECA67DE28FC,
	LoadingScene_ErrMessage_m30A479F970DAEB8F7D74BD9A3B3B3A8D20F3B589,
	LoadingScene_Init_m7F714B8605A97C76444C8DD9E66D94887F346213,
	LoadingScene_StartDownloading_m41547C24257C3BE77D2BCBEB04FC81ABDF3A8344,
	LoadingScene_Update_mEB55EF55BBB68FF4363980BDF27D5CB85D77B417,
	LoadingScene_ChangeColumn_m2B7B626540DED9F3AE11154145240AB32DDD4B3D,
	LoadingScene_ChangingMode_m6D15BCEBB56901798BC45AFC93F7058EB991C310,
	LoadingScene_InitAct_m9F656A07A56FEC2CBEA6CCFADA78D737F508E2E2,
	LoadingScene_MultiAct_m40CA43F99D1700B00869096840FBF9108B9A6135,
	LoadingScene_MultiDownloadAPI_m5BD8E8CF054D47CAEEDE98160E114A242B86720B,
	LoadingScene_CheckAPIBool_mFB5E5C864BAA0E557F7063B56D24928D2EE92FDC,
	LoadingScene_MultiDownload_mB1C963051D5546C01408D1D676815F67973B2C63,
	LoadingScene_MultiDownloadDivier_mD22099F28F919B31E45A3C083003B83EFDB31D75,
	LoadingScene_MultiDownloadNCache_mB637FEC88AFEC768CF8F5BFD2C0A43EE9556BBCA,
	LoadingScene_CropTex_m70F8E4536A8662A8AD0293E61E1E8D146C4F4054,
	LoadingScene_MultiCheck_m988668F4C75B95F6601CC4F670BF4D97848EA06D,
	LoadingScene__MultiDownloadDetail_mB2BD4F4E01DC0083E1E9C50A5237CE2A87B29FB5,
	LoadingScene_MultiDownloadDetail_m92296284AB4A86AD7CB4C90CEB63904CB873B1C6,
	LoadingScene_MultiDLDetailNCache_m0F2BADE4A363EA2C026D186EF2D570916251F6DB,
	LoadingScene_GetStyleIt_m8F00F766E315DCCE1DEE7430B98C70D7D7C1E268,
	LoadingScene_GOBtn_mDDFE20936BDAD5E465ADF9867132B6828D113F78,
	LoadingScene_RestartScene_mFF0B708A8A850CA8DDE0B1292D4EC91240BA38EF,
	LoadingScene_GetDownloadList_mC770B4D8D2A2AF13CC1C6D41DE886623B5E2E33A,
	LoadingScene_ResetDownloadList_m38F60929ADDD0DA0DCA08CA0861296EEAB22106E,
	LoadingScene__ctor_m04314096CD284A7CD2CDDE1996B412F59B12D4FA,
	LoadingScene_U3CStartDownloadingU3Eb__15_0_m0B5F9D7D2E22F78A60E2798B2E1B59ACE31228E2,
	LoadingSceneSlowVersion_Awake_m0F6414C50FFCD3156683F23D6F73414452FC70D9,
	LoadingSceneSlowVersion_Start_m8E9CD19BDDA4266614129D5BC3AA13248A48AFF2,
	LoadingSceneSlowVersion_Init_m9A43E95005F52D2BFE97A1A6AF0681A449DC5C55,
	LoadingSceneSlowVersion_Update_m22C7F1D52A7284E3835240D0F6C99856FC0CB5CE,
	LoadingSceneSlowVersion_ChangingMode_mCFBDE2950D15E7FF7E1C82E9B6A49368011689D9,
	LoadingSceneSlowVersion_InitAct_m2A6AF00DADBB38ED1882BBAA93E519F1E7A825BC,
	LoadingSceneSlowVersion_MultiAct_mB14B0541F3133AB399DDC72BD2F1808BBF2BD804,
	LoadingSceneSlowVersion_DownloadAPI_m52C284A20BA453E0595B0337B7AE6FA75656B544,
	LoadingSceneSlowVersion_MultiDownload_m4D9F9105860CCE540998352E4A5849FB9AA86EA4,
	LoadingSceneSlowVersion_RestartScene_m98C68E14BE2D1F35B5AA7A194A789782915DEB90,
	LoadingSceneSlowVersion__ctor_m4B33F6A32D2FB6776455AB213E7C81E4930C30A0,
	StyleIt__ctor_mE5ED8C2885AF921DD246CCBF8F80BE6CFE0D5C3D,
	TextureScale_Point_m7D79D2581343446711D6ECEE7D1366B870FD2D21,
	TextureScale_Bilinear_m19634C708F31692207126257D6E3045AEAD163EB,
	TextureScale_ThreadedScale_m7A51B5F57C7908ACD9698BB27C43CD1C8A4C9527,
	TextureScale_BilinearScale_mCE3B99635F9CD5653B0802466250A722FEA4799B,
	TextureScale_PointScale_m0D7A658B33A3BDD5A6E386BB79285B621F672F84,
	TextureScale_ColorLerpUnclamped_mB7923465A92B145C734DBCA973508005124AEF63,
	TextureScale__ctor_m6EBAB8CDD2CC174FE3C4E30C5A8CE1B8136BF5A9,
	UnityMainThread_Awake_m1D1ADC061302A2D509A7FD88AEC0F670A7A13C1D,
	UnityMainThread_Update_mED329E025414CF950307678274E0D9B5D0771DA5,
	UnityMainThread_AddJob_mD0D37FBE2D3C9E63D93A9E85A866AC349AE12187,
	UnityMainThread_SpeedRenderQueue_m0C671D395104C530710236C7973E4301C6EF906C,
	UnityMainThread__ctor_mAB5B2022B463D0810D4D6675E9C435351F820009,
	UnityThread_initUnityThread_m04D8C8A616CA2CFE59936ACD285CA0E6CE97F1CA,
	UnityThread_Awake_m042E7A2495E1F4F4956EE606B19D6CF2239F1CC4,
	UnityThread_executeCoroutine_m229FB4AE98BF2EF4516041F446593F44559D1A5A,
	UnityThread_executeInUpdate_m207EBF73F71FC33F4E336719369A9ECE2412C0E3,
	UnityThread_Update_m6FB70D79F2D1E69BEE2D313C6EAD1DB00A0C8AD2,
	UnityThread_executeInLateUpdate_m7D81850D6544AB6B572E7C63005AE70E061A1FA4,
	UnityThread_LateUpdate_mF1D62411D4EC9F3BEA9E73C46FFB4ADFB974FE7A,
	UnityThread_executeInFixedUpdate_mB10742C263E9C724A08103CD24B62630E81F03F6,
	UnityThread_FixedUpdate_m3A7FF39632283C0DF329614B01DF6686D65F8B55,
	UnityThread_OnDisable_m02FD692EA4251FCA9FE54695E638A7555EDE5CBF,
	UnityThread__ctor_m0FEF37FE1A8C04C56348B3B14196833512BE67F8,
	UnityThread__cctor_m86A9FB85C688B5F869227516214135999CA14938,
	OldGUIExamplesCS_Start_m5C679AD607A16C85DDFAE78B996120E58E8989E9,
	OldGUIExamplesCS_catMoved_m844C01EC2655FB6AEA6261F74BE3AFE380B195A7,
	OldGUIExamplesCS_OnGUI_mE08A1CA5C406551B5D66A4A6BB86E00285F97C30,
	OldGUIExamplesCS__ctor_m626C9BA19A539E78953626625960526EDB180F4A,
	TestingPunch_Start_mB88A524025639F5F9A18D9B82E98E30EB3356D06,
	TestingPunch_Update_m9B5977F7EA9B1FC0F7D69A7E202DC2FA38BBEAA8,
	TestingPunch_tweenStatically_m8E1F7A46E2EAAB6AEDD62E0C2428EC4EBAD0C7F3,
	TestingPunch_enterMiniGameStart_m2FAE7161DDC3665930335EABF53A611D1DDA65A1,
	TestingPunch_updateColor_mFA69FF82ECDF6CD2EC12354370879F019E15D477,
	TestingPunch_delayedMethod_mF8E7A6CE3054ACE65057D43047ECAB6E458C4E84,
	TestingPunch_destroyOnComp_m0D7E0E3DA9113B3B5F265EBC69AEB3CC70D63E13,
	TestingPunch_curveToString_mDE53974600DB58372C612CB078B663510C667F61,
	TestingPunch__ctor_m9A8492FDD64928449DB0DB92CD16061D642E4A6A,
	TestingPunch_U3CUpdateU3Eb__4_0_m8FB81D468DBC883BE134CF8314A05D3482BE566C,
	TestingPunch_U3CUpdateU3Eb__4_3_m542BB1341B4AECC188C74DCBE4F49CA2AB8AC4BE,
	TestingPunch_U3CUpdateU3Eb__4_7_m0D94A53B95AF073898CBDEAD58DC696E6E060066,
	TestingRigidbodyCS_Start_m936906C87BBDA6BF06D51D430EF08BB8D14E0376,
	TestingRigidbodyCS_Update_mFA7EB03B37C1B2DC3C7ABA84488484149155828D,
	TestingRigidbodyCS__ctor_mA1DCB32BECE6491D4E8B9F8E06979C6F2B775516,
	Following_Start_mFEEA46D15CD9B280B5E7F418BAB4456C730EF556,
	Following_Update_m0909311BA8C83F7F2448084BAFC093C62C012B76,
	Following_moveArrow_m6B308968FAA0842DE5E36D17DFF4CFADC87BDE64,
	Following__ctor_m47A2E77D28030BA448B3705FBD95D3EF1C0EF5DE,
	GeneralAdvancedTechniques_Start_mA1FF0FADF625671F926CC2E4D0CB77B979301564,
	GeneralAdvancedTechniques__ctor_m54D6A82A08BC2E749DA9363763E710312B79E899,
	GeneralAdvancedTechniques_U3CStartU3Eb__10_0_mBEBB18A1DC341C34F99BEF02138E30A577DC63C0,
	GeneralBasic_Start_m722ABC7DD46AD33613ED9A99550009B1388A2478,
	GeneralBasic_advancedExamples_mC9210C18842768B7D2E99936EC766E18D85C8773,
	GeneralBasic__ctor_mA0A6D418EBB951CF5EF97DCDE5E62CCC6D85D31F,
	GeneralBasic_U3CadvancedExamplesU3Eb__2_0_m6A1F674D4183516100E64513ACC50585EBA731E8,
	GeneralBasics2d_Start_mE3215316598A87E2CF0D9833F686D13FDC86DF2C,
	GeneralBasics2d_createSpriteDude_mBB652175A3D6AE7952BBE8C0E1226864D79928EA,
	GeneralBasics2d_advancedExamples_mAC58CC5F094D9E634ACE4F04656C6A484C6D52C3,
	GeneralBasics2d__ctor_mFEB91E190B0738BC50845B33FAE252A2B41D55C7,
	GeneralBasics2d_U3CadvancedExamplesU3Eb__4_0_m05957C9477E9A908C2EAF3BD0AD001EE3DC0D881,
	GeneralCameraShake_Start_mB1EE5CBBF521F4D697F8EAF2F53600826F54DABA,
	GeneralCameraShake_bigGuyJump_mE82096DC9C47AFAB293E2B8BAD0B7F2153063681,
	GeneralCameraShake__ctor_mA22AEAD53E34A074E7DBC77BC3164C64814270D8,
	GeneralEasingTypes_Start_mBDA1C8799607E8C3D6052D0B88E5E96ED8892184,
	GeneralEasingTypes_demoEaseTypes_mAD63F64427B735F85C75265777B1EA2C6038B422,
	GeneralEasingTypes_resetLines_m46F8B9B5ADFB7348E099993918DB8EAA1108B746,
	GeneralEasingTypes__ctor_mB2B0932931E070FA4BA5C24CFCFBBBF894E534BB,
	GeneralEventsListeners_Awake_mBAFA4AE2A5056FBFED34D12A4776985220D66AF7,
	GeneralEventsListeners_Start_m6B12B5EE52523BBA2008F17CA57A49B0ECCB619A,
	GeneralEventsListeners_jumpUp_mC8BCDFB7169C9F7EBC42A5860DBB0BAF05612E70,
	GeneralEventsListeners_changeColor_m6FBDF19D2457EA40303B510C9B9C4206943A8D09,
	GeneralEventsListeners_OnCollisionEnter_m43F8FDFAC97EC09E53E98849F68469FCA7881F5F,
	GeneralEventsListeners_OnCollisionStay_mC15D1654A132C57A85C90F0A9D1A0489481B20D2,
	GeneralEventsListeners_FixedUpdate_mF30FB01A225EFB50EB4714A0110DD6668F9715D0,
	GeneralEventsListeners_OnMouseDown_m7961A64655D9C0BC7401ACCD7A54741F5440C659,
	GeneralEventsListeners__ctor_mFE5DA13A6BE436986ADB09018AC3C641A1DD5A3A,
	GeneralEventsListeners_U3CchangeColorU3Eb__8_0_mF7603717CE44B39A97A3F80DC96F80E43A2DD33B,
	GeneralSequencer_Start_mE22252603584E9595014C8CEAC0006FD9B55DBE7,
	GeneralSequencer__ctor_mCF1E29ADF0F70414A63D0F950D0FAB09AB1F0C8E,
	GeneralSequencer_U3CStartU3Eb__4_0_mCC4F15FF69FEEA0BFB73E8E2542C8910D2FAA220,
	GeneralSimpleUI_Start_mEEF7BDC999733A706FE7C03F54C8B4B83243C221,
	GeneralSimpleUI__ctor_m9A7A8639A7F2EE5FEAF39E2B6232FA533F58E1CC,
	GeneralSimpleUI_U3CStartU3Eb__1_0_mD28AC7A7223ECEB398951A2AAA561A4B9DC6FB66,
	GeneralSimpleUI_U3CStartU3Eb__1_2_mA798C3D21C7EDE1BF5F133F6946478FDCE1B5CA8,
	GeneralSimpleUI_U3CStartU3Eb__1_3_m759D56CF0CDCA8E27EDEB8BF968FD00AD9932824,
	GeneralUISpace_Start_mA7F9726366918F04A038C11E04A5230F0C4AA41D,
	GeneralUISpace__ctor_mD9DD0C8F2377084F9DC2EA074C24E93DF1A3518B,
	LogoCinematic_Awake_m72CA9A35497E16D50121250F9BCF15F3BDF65138,
	LogoCinematic_Start_m5DB42CBB808A9C41D32B440B65FEE9F35E1FF86A,
	LogoCinematic_playBoom_m1359C13981E4254A26839968D26F0F96E6033E4E,
	LogoCinematic__ctor_m87A3D83E1047FD2818C5319C7462F2475468743E,
	PathBezier2d_Start_m6101865E419D574B55D90AFC87D59C61E2FE6A5A,
	PathBezier2d_OnDrawGizmos_m5AF15DD02CC758B94C6988CA1084C7D5C03FC721,
	PathBezier2d__ctor_m57F2349CACC84E24CFC8D242C81E3DE948CC357E,
	ExampleSpline_Start_mAD9D6848447333E450D9991F68BDD989B6AE6BFC,
	ExampleSpline_Update_m0C06D2C4C359A2AA1E1EEE9C4A7CE76B10A774F7,
	ExampleSpline_OnDrawGizmos_m1EC31ED5019C6CAAF409DE171F1D46631C4FEB4B,
	ExampleSpline__ctor_mA777C76A3F034C55804819D09FAD803C22B6945A,
	PathSpline2d_Start_m322D034A9E1AE896E48FD9C3A83D81E298B101F2,
	PathSpline2d_OnDrawGizmos_m45503986AD7ABD0B14F69C276092F5E99F3508E0,
	PathSpline2d__ctor_m0C4B7813D0D578B5CAAAF5D2963ADF0E699583B8,
	PathSplineEndless_Start_m0D63B5D9F0A5A8953829DBFF1AB80AD1699E5858,
	PathSplineEndless_Update_m2168C1E36EB175C86685B1D6438244B9905B0D55,
	PathSplineEndless_objectQueue_mDF89CDC0D51CA6B9A84F700BFD011EA5D53D0B3C,
	PathSplineEndless_addRandomTrackPoint_m2A6DF9D7DC969FC9E3E269C8F6815BDB1622552E,
	PathSplineEndless_refreshSpline_mE7C260A36EBDE07ED250D752CC2AE13F9640BF84,
	PathSplineEndless_playSwish_m8DF90B3209DEC3201AD432B013B8CE3D665E70F3,
	PathSplineEndless__ctor_m936283AAD1B2ACB8A975A6C5F44E5C49F235D889,
	PathSplineEndless_U3CStartU3Eb__17_0_m6457B9A5A1966F3A5EB50724D4F867D6AB05BDD5,
	PathSplinePerformance_Start_m1DA9D6816079EA25341277C76A317D94B4168EF3,
	PathSplinePerformance_Update_m440225D51242DD97344316E466389BB633099B3F,
	PathSplinePerformance_OnDrawGizmos_m963F417D81A92D1D1260EB172050FEB2C95B9E06,
	PathSplinePerformance_playSwish_mCDD5986D88F473C4C789EBB08013D5C4371B8934,
	PathSplinePerformance__ctor_mF91B2042C6950B108ACFC3CDB4FA0F2F6545377E,
	PathSplineTrack_Start_m281BC1BE6C44BADFA80DA3D5C0679D34FEBADE1B,
	PathSplineTrack_Update_m789CE9A3C91D635B04AE45C77ADC552CEEB0BE3A,
	PathSplineTrack_OnDrawGizmos_mF8A6C592C8E177043453FAFA09F3917D133E82FC,
	PathSplineTrack_playSwish_m166E8F61FFC8D4DE30A55839EE95413669489D99,
	PathSplineTrack__ctor_m3D4ECADCEDAC59010FC4B6576F49BCC3083E8DBA,
	PathSplines_OnEnable_m9A31A2680F7A3A5A786B007758F6B68DEFD47CBF,
	PathSplines_Start_mBB7ABDB218B114C164C2E61B9BD469CB8D5E76D6,
	PathSplines_Update_mFAFAB73B81EA5981B4EEC5C507B72A52D49E593C,
	PathSplines_OnDrawGizmos_mD3B3EE01513F4BBF82A00F87DBA40C283384C5EB,
	PathSplines__ctor_mFFF59F5DECB94D08ECA1765F4AF70F15F1D0F807,
	PathSplines_U3CStartU3Eb__4_0_mCDFF93FF0B96E66451EDB0D037E46B7F6A0B0D04,
	TestingZLegacy_Awake_mBCE69D28EF4B5C8DD9623C93150A62690FF6F560,
	TestingZLegacy_Start_mF7B3C1D0359654FACF1A636ED276DA2D7AC2F238,
	TestingZLegacy_pauseNow_m655EADF071BB3053B444C84C0BE35675AD971405,
	TestingZLegacy_OnGUI_m8736E68E13F34D38E820CBDF166DB67989878F22,
	TestingZLegacy_endlessCallback_m9AEBB3556FEF851B1F6DB08DB8050E8D89D45D17,
	TestingZLegacy_cycleThroughExamples_mC9AF8FB1FED800DF92B00E6C482380B1ED2EF4A3,
	TestingZLegacy_updateValue3Example_mCA94016385AF00299FD43C7778501DB3F51E8786,
	TestingZLegacy_updateValue3ExampleUpdate_mC0566B31227122FC6140F7704C48065E4AC046C3,
	TestingZLegacy_updateValue3ExampleCallback_mEA31AFEE5C74647804BDE02889A38307D8096B4B,
	TestingZLegacy_loopTestClamp_mEC3D64101ACA122E2257ED726DA36D5E7E2EE6DD,
	TestingZLegacy_loopTestPingPong_mC9FC3BFBB6EB66050EB24EA3623777201C131F91,
	TestingZLegacy_colorExample_mDCABB46692619BF22DD41425D03EA835F6024AC2,
	TestingZLegacy_moveOnACurveExample_m643B475C72457BFC0580809D25206A474786A1AA,
	TestingZLegacy_customTweenExample_mE863F3298241D782EBCB333A93B882C572C8F9E6,
	TestingZLegacy_moveExample_m8CB97B6FAD6C66EC740EB31D539F78BC5930F02A,
	TestingZLegacy_rotateExample_m09C24A6D46978C652A6D8F321F9A071247462DD3,
	TestingZLegacy_rotateOnUpdate_mDB551FEC7A3BC50E5BFCB3A4D86EDBD71343FE97,
	TestingZLegacy_rotateFinished_mEE1B541D31B1E9388A575AEF8FECC8D8FDD3605F,
	TestingZLegacy_scaleExample_m8683640627D1DF303D1A55A59C476198A6D5D7D8,
	TestingZLegacy_updateValueExample_mE0A9F76605CEA59FA59357CE4C5313A32801AAF4,
	TestingZLegacy_updateValueExampleCallback_m4CB6A988D0383234B578C9465B40085A962D92EC,
	TestingZLegacy_delayedCallExample_mB11B32B256519D1ECFBB54216CEB46E7719BE5E9,
	TestingZLegacy_delayedCallExampleCallback_mE8E45FAD4B3C8A46BA856C0250AF6A4F6606F20C,
	TestingZLegacy_alphaExample_m7DEE863B73235D62D7FACC183D9B69A9C22C68F8,
	TestingZLegacy_moveLocalExample_mD7173FDBB36229E28B108F6AD5195F253326F3B2,
	TestingZLegacy_rotateAroundExample_m2D08EC29B26084F4B7D22AAD9D8A4453FFB9659B,
	TestingZLegacy_loopPause_m55A880DF0CB7F8737EAB6CB0202121B258759EE7,
	TestingZLegacy_loopResume_mE7636F1996095F5B17B155C5BA0350DDEF222F8D,
	TestingZLegacy_punchTest_mE2E20734BDEE2A30E5F643FDF84FE56BD99ACB49,
	TestingZLegacy__ctor_m54DC8DE5EF69E156FDDF1C34916397E68B0B9F2E,
	TestingZLegacyExt_Awake_m97F85D3DD812AE6BFA4E59E5BDB074A11DEFB74D,
	TestingZLegacyExt_Start_m3FC22E42B4D1C512B8F76260CF90F7584CDE78C9,
	TestingZLegacyExt_pauseNow_m6FA2ACC29A279AFFEAC9C1117C615E692109E47E,
	TestingZLegacyExt_OnGUI_mE335B55E2688BCF03B84575D64CE6905E9EB390C,
	TestingZLegacyExt_endlessCallback_mC91F80F402003198552BB5A47773AE91C1CDF3E4,
	TestingZLegacyExt_cycleThroughExamples_m7ADA1FA87F2373C6CA2F710EE09E07C6E6B48DA7,
	TestingZLegacyExt_updateValue3Example_m56A718E3C7178DB0C59E3E030EF2007D60168950,
	TestingZLegacyExt_updateValue3ExampleUpdate_mBA7E2C03C01B5CDA6C4DF5EE4E5521FF72644235,
	TestingZLegacyExt_updateValue3ExampleCallback_m83AD25E3BEB33C7F57C68FCD9C1EA9E27F762651,
	TestingZLegacyExt_loopTestClamp_m402BD2B45CDF06FA5F47E43EE139E3D593D3837E,
	TestingZLegacyExt_loopTestPingPong_m88A111C6F9F4C97A89F6E141862BD22F1723F1A8,
	TestingZLegacyExt_colorExample_mD487DAAD856F9B4E044869A56A9E83F0E5CE686D,
	TestingZLegacyExt_moveOnACurveExample_m76FBF8744FDD1EDFA144DF0B05F91F341DDE7C86,
	TestingZLegacyExt_customTweenExample_m7FD53EA42C543F5CDE51BE17B7B1714B078A3671,
	TestingZLegacyExt_moveExample_m877A1E1840D36A34C7655F995471BA952B64FC2D,
	TestingZLegacyExt_rotateExample_mB882491A5D47DE36B743560863B366C88CF69A2D,
	TestingZLegacyExt_rotateOnUpdate_m60EEE824C1EBA3FA12C60B3B23B6144739697A25,
	TestingZLegacyExt_rotateFinished_m6885AB18A26592B3844455D756D70BDC20F257E8,
	TestingZLegacyExt_scaleExample_m3D54AA1EEFBA37C30889E34AFBB350316CEAB41B,
	TestingZLegacyExt_updateValueExample_m0A08F0B235AD41CC0C4153B431862904C81799C6,
	TestingZLegacyExt_updateValueExampleCallback_mD07695C332FF263FCFF89F4B305C96A02A786729,
	TestingZLegacyExt_delayedCallExample_m530FFA75198C880599CE65E105D061B133E172F9,
	TestingZLegacyExt_delayedCallExampleCallback_m0F8E4A9EC78F99065D1B519DA641FEB9B4A686A5,
	TestingZLegacyExt_alphaExample_m6FEFC52F975614F51524FB7B91547C791FC1A982,
	TestingZLegacyExt_moveLocalExample_mD509C90ADEA3546144652A770965A5C98F88F3C2,
	TestingZLegacyExt_rotateAroundExample_m4507C9CD41E678846AE918C6D796A4BFC43EF056,
	TestingZLegacyExt_loopPause_mC23D0D95589F74EA510A9E79D35F28C7418F514F,
	TestingZLegacyExt_loopResume_mA3FBE26AE1243FE2178C55BEDB45AC6763C42FF9,
	TestingZLegacyExt_punchTest_mBC5D8A5FFF359CFAFF16BFD89387B56B571DA001,
	TestingZLegacyExt__ctor_m918A02678AF6D9E398E17E98FC792A73E8205542,
	LTDescr_get_from_m8E7FF7FB5FE65C313FE2CD26C8B1BBAC00BFBDC2,
	LTDescr_set_from_m0C7AA03B3869C1A3B6EEB8ECD16FF0311A19B5FD,
	LTDescr_get_to_m842DE271937A3E21367D57D4C1A963E98E0AF5E9,
	LTDescr_set_to_m720DB590EE8641E7091375EE626ED38CCA36C63D,
	LTDescr_get_easeInternal_mEDF89A6F240F4770536F1FBC0FBB79A6A55C7BBD,
	LTDescr_set_easeInternal_m16C7B8635E5D2FB3E31EBE721E954967EFBF30B7,
	LTDescr_get_initInternal_mE336562DA52E756130ADBB07B3CEF2D7E7EB01D4,
	LTDescr_set_initInternal_m0753D24B8455857620589DBBE1EF095A2AFE489B,
	LTDescr_get_toTrans_m2B06D495FA4BC4F44D1EAD32F163D62BCC457B5F,
	LTDescr_ToString_mA312106BDDBB7E356B88C05BF8C767C63964CCC6,
	LTDescr__ctor_m975E0C19B34E8078C70AFAE95A581777B8D6F4D8,
	LTDescr_cancel_m944E66783EA1A1BD7781AFE7C7EF7A6117B56930,
	LTDescr_get_uniqueId_m11CA85564A0B0C9BE7FC551ECA3A05111888C7B2,
	LTDescr_get_id_m76232B8A21BCE44149F9014942BD1C63F5C6B4E7,
	LTDescr_get_optional_mEA6CD7BE31135EB1F2310490F9FAC346767B3020,
	LTDescr_set_optional_mF78587FE46E7E26D9742F3F156DC92DA51AEA2EE,
	LTDescr_reset_m16BED571328E24D5385BDA9E4E478508EEB74664,
	LTDescr_setFollow_mD35310187FD79F81851CD3635C0376BAAC9EF9FB,
	LTDescr_setMoveX_m3B1BB06BE81F8DF47CE145148ADE5D2E63B7D630,
	LTDescr_setMoveY_m62E79AA722E091918379FDA5255DB3F517D447F3,
	LTDescr_setMoveZ_m5846FCA035E0BC391028178FEAEF74B0C31C998B,
	LTDescr_setMoveLocalX_m1DAB1F3CFCF599184B907273B7EEDB58C319177D,
	LTDescr_setMoveLocalY_m3EFB302B1C5A842539CCC7D972836D4AA5BEF9C1,
	LTDescr_setMoveLocalZ_mBCD523EEE6FDE11FBDF9B291FA827EFCAADC055F,
	LTDescr_initFromInternal_mA4604276862B5C16B4D0A1F323C5031D37CBE125,
	LTDescr_setOffset_m29ED0A18B591990BBCC94E36A22426FEFF0B6F5F,
	LTDescr_setMoveCurved_mD2FC79FD2FBC09320F29D253187B0FD5E1E3F242,
	LTDescr_setMoveCurvedLocal_m5B19FE68C93FBE105E3D02DA4AABB28183218DA4,
	LTDescr_setMoveSpline_m483C9060295F816DDB9F363B2B7D3C7EB2BC709A,
	LTDescr_setMoveSplineLocal_mE23B6AEEBD002D6D811758CB415DBB8559505DB2,
	LTDescr_setScaleX_mC50D44B615017A9DEC47B21746DA49E6907C2106,
	LTDescr_setScaleY_m3980093C790F98A85A697A6CB76A08CE258A1831,
	LTDescr_setScaleZ_m1E3D4F892EFB3F884EB006D6314CEAAE2E86B788,
	LTDescr_setRotateX_m9710079249D050C4709F38FF212A8C5A064141A6,
	LTDescr_setRotateY_m0A6206F1BAC1EB1E74373C866BB993A17AC474C7,
	LTDescr_setRotateZ_m88EB19296F3A5A255B0DDAD63F5556854A3AA6D5,
	LTDescr_setRotateAround_m5C916E21674683F454CF66A50F1B52688EC5A72F,
	LTDescr_setRotateAroundLocal_m26621B8F8199CF50CFD97CF32E6DF271543CE142,
	LTDescr_setAlpha_mA82AF444875CEBDC11AF76A6DA33F8832070A7C9,
	LTDescr_setTextAlpha_m140059336BABDBE97F7B9EB26E7B07A6A2FECCCF,
	LTDescr_setAlphaVertex_m4BE3949864A804651A8E884317090AC0BE9BA543,
	LTDescr_setColor_m677D553334BCD65A976646538D2E3FB901FEB2F8,
	LTDescr_setCallbackColor_m8ED344DF8DA4A35A9323090A9705F338CCEFA6BF,
	LTDescr_setTextColor_mB0E1342E1C5604929D621039E74EB5C968E63CA5,
	LTDescr_setCanvasAlpha_m883942EF674A4FCF4C0621D8BB36F1EB78D9465E,
	LTDescr_setCanvasGroupAlpha_mA08B850710279DF7259A9EA457614BABD29EF28C,
	LTDescr_setCanvasColor_m8D0A659A765604171C1ECE12ACB617319E179015,
	LTDescr_setCanvasMoveX_m012C7901BCEC98126783ACDB01B088297D30EB69,
	LTDescr_setCanvasMoveY_m8124C7761145134D07245BB3031857695501B9EB,
	LTDescr_setCanvasMoveZ_mFDFC0D71E22EE1097FDA8A2A6F0A9990C5403D6C,
	LTDescr_initCanvasRotateAround_m1205028161B594D223E69013F8C92B766CE75959,
	LTDescr_setCanvasRotateAround_m7E04711CE56F7BB9D43E27A0102E0C3C7FDCDDF9,
	LTDescr_setCanvasRotateAroundLocal_m7BA851F76908BF978A682D4E49CD2DB770F27F55,
	LTDescr_setCanvasPlaySprite_m3CDD391E0DC1AB5B60FC4A01F1527184062E0202,
	LTDescr_setCanvasMove_m84559A096BC7A140C8CA25DE9DB766D537C9A148,
	LTDescr_setCanvasScale_mF06739A6DC21077E3EB7704B3997CB495302F134,
	LTDescr_setCanvasSizeDelta_m6C41930BF6F585DCDDF31EB4B540F3C3163DF0E3,
	LTDescr_callback_m9B45B1DCD83B9880A61C16C6C21F0D05C7C6AFA7,
	LTDescr_setCallback_m7BD2A7CFAC15C8D9D992E0521B819B41FD9A6CB3,
	LTDescr_setValue3_m553FBFB157A9765053F9FB7CF08B59A1E3D976D7,
	LTDescr_setMove_mDEC044497DD6398395A17A83EDC32ABEB3F8DA88,
	LTDescr_setMoveLocal_m4B75F54B72038AF2971A73A8A6E905DE94AA6346,
	LTDescr_setMoveToTransform_m9C07DB063E64D2F9A91054DFA08B1C2594D22F88,
	LTDescr_setRotate_m37FB3B6088196BB2A0DD48105125989468400A5B,
	LTDescr_setRotateLocal_m94A88F95DB9D3BD078305AB3A25039966CF4147C,
	LTDescr_setScale_m003B5242EE6667C4F5AC57C5C06A9A32007F0FBD,
	LTDescr_setGUIMove_mC25E34C2B27930D956DB92F12FF79F25F7D78E9E,
	LTDescr_setGUIMoveMargin_m96C670A2423CC1E4A2D02021CF0F5CFA51A8E14B,
	LTDescr_setGUIScale_mE8BA06B69FFA9FF14F0C7CD3FB76AE2F12283B0D,
	LTDescr_setGUIAlpha_m964F89CFFC7D20A97EE78E6340C9F8236AC2DE6A,
	LTDescr_setGUIRotate_m9B46A83C43BAFD1DB0C5629D850F1D3424B3AA7B,
	LTDescr_setDelayedSound_m6223261128689E6768A694E089DE42B67A2BC026,
	LTDescr_setTarget_m96F2398CBF82B65C2BD373807C75C0E04A39DD29,
	LTDescr_init_mB964AC9BD3406D19362D8675474F4DC99B33347B,
	LTDescr_initSpeed_m97D3A943AA4B2F25ABC91FC5C5C055611799AE16,
	LTDescr_updateNow_mB7468B5F37D19C21184A2A885C1DF0A55E1F1109,
	LTDescr_updateInternal_m2295EAEC09FDEF9AD71EB618734DE0DF5570D2A5,
	LTDescr_callOnCompletes_mE047FE764250E4071E38197BC1B29221B87B8B4B,
	LTDescr_setFromColor_m84DCC218D18266CA0D5167AE546CF54EBF2D38B0,
	LTDescr_alphaRecursive_mA15152C74BCA8D8CC498BF7A84A0481A35AA29EC,
	LTDescr_colorRecursive_mA38B1CB2A51FECCCF49F1D1F50D98312A2296E0E,
	LTDescr_alphaRecursive_m6BAD882CBD0900B2ED3ACD96B49E1AE50DEA791D,
	LTDescr_alphaRecursiveSprite_m64AA415027FB1434EFB1E006721B206F47CB1ECA,
	LTDescr_colorRecursiveSprite_mFE805A4F826EC3CFF84CB6E86E43E857B934897D,
	LTDescr_colorRecursive_mE714E3CB2CB79B4F6C40A0C0A5CF648A5C1E3F94,
	LTDescr_textAlphaChildrenRecursive_m99FA1EC22F4CF7A7743038CF8369298B1F3ADAF2,
	LTDescr_textAlphaRecursive_m8EC23765F7904CBA0C35734A7D63F58C879C577C,
	LTDescr_textColorRecursive_mE39D07578A5FCAEE8336AE9D1DE9E841B98FD90C,
	LTDescr_tweenColor_mD7F8129343D2677EC17D5740E160A88540685140,
	LTDescr_pause_m516CADEFB418399C0A796883DB35D183F8FBD9D3,
	LTDescr_resume_m1D572322CCB20396E715EED46A1692A6D4F81E80,
	LTDescr_setAxis_m6785FFA919508809C7A0A5FB7A998E46E1CB22D1,
	LTDescr_setDelay_m78E34545BB3EEA43ABF9CD5D29C8FB1CC3D4F464,
	LTDescr_setEase_mCF284E5CA17BB1BECBB5B1CC1013571B108580A3,
	LTDescr_setEaseLinear_mB1944DC5601584DF8F68F2FD1E844526B4B57D0A,
	LTDescr_setEaseSpring_mCD7F60BDFCE5BFF617264B8775C4E80ABED4A465,
	LTDescr_setEaseInQuad_mC03167FBB2CD3CB171D87440DC20604F51248C6E,
	LTDescr_setEaseOutQuad_mC87008E2DA94BFA521BB12A74F4FC0F160ED1291,
	LTDescr_setEaseInOutQuad_m22C85D4E11EA5B77869864113758310F097CFDDD,
	LTDescr_setEaseInCubic_m0D93D024F3F116E6A7887F86F0F5203D74FAF5FF,
	LTDescr_setEaseOutCubic_mEEAA2F960FB30E6B84B3D52BEB173E406A0A8A6F,
	LTDescr_setEaseInOutCubic_mACAC52E920C0602EC0D90E6019B3EAE9034D25F9,
	LTDescr_setEaseInQuart_mEFE37EB2D9BB775BAC45A1D55CABB0D41FA679F0,
	LTDescr_setEaseOutQuart_mEAD18F5546A6BE77FE48294B5BB316769338A461,
	LTDescr_setEaseInOutQuart_mAFC9F8155E7348004D99107EE10C10AD51C526B4,
	LTDescr_setEaseInQuint_m8C116A004899207382401BCADC6F39F4EE3B75B7,
	LTDescr_setEaseOutQuint_mA59F24802CD0E1007E659D34F533F8F1CA7FAD13,
	LTDescr_setEaseInOutQuint_mF815B5687F9162E45C9D1AD031C317840712FE19,
	LTDescr_setEaseInSine_mF481D539F3DA2FF9AE13F373BB79B2CC93AE82B6,
	LTDescr_setEaseOutSine_mD8439D9AD162893667913F82632A087BB7BF66D0,
	LTDescr_setEaseInOutSine_m1647757426F728F0A076E73DCB939C469B77E1A9,
	LTDescr_setEaseInExpo_mE27C2F90670FA473FC8D27DD70D3E4F47A4935C2,
	LTDescr_setEaseOutExpo_m9220EBCED76070AE7B72E997FD5711A68D35371F,
	LTDescr_setEaseInOutExpo_mA4B0D2B2F98FDD2C5864ECA9000CADFBCE38C8CC,
	LTDescr_setEaseInCirc_m75ABE78F120AC863C1BFCE3426ED6940CA9B30BD,
	LTDescr_setEaseOutCirc_m2728C6194974BE6C02932FB34B991EE2110FB14D,
	LTDescr_setEaseInOutCirc_m75B555F1A463C9A3B59D2DA7642B9A7AD36770F2,
	LTDescr_setEaseInBounce_mFC166FCC2D1359368A01D60AD95F87880E89E22B,
	LTDescr_setEaseOutBounce_mF118AA6B5601E83ED52CCDE9A0EA4C92F30A5626,
	LTDescr_setEaseInOutBounce_m1268040122DB6FA1269D298171FADB03DDE8BA9E,
	LTDescr_setEaseInBack_m3220F0CC60207776341D8612292803E58A8D2175,
	LTDescr_setEaseOutBack_mF300DC1C059B5BB6CCD5BFC2CC71B665334AD717,
	LTDescr_setEaseInOutBack_mCEA5FB6CF2A0A6335CF1F9F06177A4B663CE1F47,
	LTDescr_setEaseInElastic_m1FE80F0CD5ABAA68172CE15DB1280788BB53B440,
	LTDescr_setEaseOutElastic_m0AED6CBCC243E3EE10DBCE0973F32BF5DD16A873,
	LTDescr_setEaseInOutElastic_m494EE77E1C98A7356847D0B44E4D088A5ACC1F6A,
	LTDescr_setEasePunch_m7B72691172462D818F06010F316087DBF5EDEB49,
	LTDescr_setEaseShake_m6AE6595FBDF8FFB68838D6D20B29C64EBAC941E9,
	LTDescr_tweenOnCurve_m84B46DFEB19FDB6D772DE2D47B4366E0A706C4DB,
	LTDescr_easeInOutQuad_m36065001F18BC0B29C29EBA965B08989B028234F,
	LTDescr_easeInQuad_m802EB621FB9F8EF34C4DD060972E506C7D6CD1F0,
	LTDescr_easeOutQuad_mA2A1E36372772F90ED90F59E97062FE128B6794D,
	LTDescr_easeLinear_mAD6CECCB459D27BE9B3ACEBDC2F7473F6A688A17,
	LTDescr_easeSpring_m9D0A0D6942FF7A2D9DE855C19487D7E9ED6F26E7,
	LTDescr_easeInCubic_mEF13536017E3D96BA908FA0C37028D3BB7FD7304,
	LTDescr_easeOutCubic_mD5A8610D69B8116750EA64A02DFAB3314EC11C78,
	LTDescr_easeInOutCubic_m2F441E0BDA04412B6746865E4D810E30E830C0E3,
	LTDescr_easeInQuart_m865902CA1856CE060535F26E68FC3E48A1B9EC0C,
	LTDescr_easeOutQuart_mE87CA7D4DF0135B45C8536A8FC74D4CDE8EE1357,
	LTDescr_easeInOutQuart_m6D290AB4B5EDF285E1A4AD1945B87864B810E672,
	LTDescr_easeInQuint_m3A8922E83FB7FD5921895D0C7C180B251E0D8D87,
	LTDescr_easeOutQuint_mA84CCBDB25A1E58D82C7A446850ADDA258592A0D,
	LTDescr_easeInOutQuint_m73C03ABA976E0D1D40E8835BE9A1FE5DBF7F640B,
	LTDescr_easeInSine_m0608A3B0E9C83EC723B9FB55149DB9837F51AB41,
	LTDescr_easeOutSine_mD364D0BBA67752AE94B5752213507D91A41C3CF2,
	LTDescr_easeInOutSine_m5817005F731481A8FB1B8B88E63319B0C08E27AA,
	LTDescr_easeInExpo_m8975809F06D63CBAC26760C7243779393D7FCCC5,
	LTDescr_easeOutExpo_m1793556A051CCF34AD78DCC5FEE153CFEDC857F2,
	LTDescr_easeInOutExpo_mF6009DCEAA7D84D5E219F93448BF38340917DF47,
	LTDescr_easeInCirc_m7F525DEA40803C0025FAC510774C26F86AB100DC,
	LTDescr_easeOutCirc_m78859138DEAF3005D845A924841F0EA8BBEAD585,
	LTDescr_easeInOutCirc_m16338C8496BAE07CDE37BDA8FCF813BFBA7F19FE,
	LTDescr_easeInBounce_m16D1EE3AB1D5E67FEBE2EA6DBE9C95A92F084C3F,
	LTDescr_easeOutBounce_mC4DFF3C3FD156884B92838BBABB56570EFDBADFD,
	LTDescr_easeInOutBounce_mFD18B378E248EB26725EEE7267C20BD3FBED946C,
	LTDescr_easeInBack_mD2D30FEF3F06538124A2AB1775BCA2CED07256A8,
	LTDescr_easeOutBack_m744F4CCCD32F8F556B68C9577E67B0468BEA3DE8,
	LTDescr_easeInOutBack_m57DB03AEDB85C66A5925B8FF6F15B7C97E2DDFC2,
	LTDescr_easeInElastic_mDF8A4B5CDAAA652D432B99F536FDE5F12D29CC13,
	LTDescr_easeOutElastic_m8D73182BD8A48500C5FB89BF4AA84094CCB62F85,
	LTDescr_easeInOutElastic_m3BFEB7C3B52B2C061544C8C2849BB7AD000E341C,
	LTDescr_setOvershoot_m0E935F8EBE21ED62F29A62DD2FC4E7C1378E0544,
	LTDescr_setPeriod_m6D8399405BD7E4C3CE73E90B1DF55991A6EEF6B6,
	LTDescr_setScale_m75F4440E58BAAACBE7205441A2FDB96E61AFE2AB,
	LTDescr_setEase_mBF09981854FEEF635C7CD97C0CBAB2A941193DC6,
	LTDescr_setTo_m829E53D1E4FA8D3C11D35CC8E736AAAC151A8234,
	LTDescr_setTo_mB3C80993607B61B638F5EC5D773CA42AE33CDB3A,
	LTDescr_setFrom_m869544CF9DE9E93780E6ADAED4B99CD3580058AC,
	LTDescr_setFrom_m916CEBF6049D5ACC8C00337FF34DF141461951A2,
	LTDescr_setDiff_m4E9CA4C17BEAC2C76720E33912216FE25D816E5D,
	LTDescr_setHasInitialized_mDCBCCB7087C97B20C58181593EB1C7A8587C84C6,
	LTDescr_setId_m4F5E561450C9DD9297F9C0A4920E7F56A57BA8F6,
	LTDescr_setPassed_m629829107456CBBEA75E743A244D5A8156872ED4,
	LTDescr_setTime_m9DB98DF42D1DE55F45346D9F4F0C0B836EF5BE82,
	LTDescr_setSpeed_mD51B284F6FEC68EACC2216BCC1CAE20588B88596,
	LTDescr_setRepeat_m39C1FFE0F3BE7AEA0AB2D3D09B8C5E93471FD528,
	LTDescr_setLoopType_m432119442AFE9EC9DCA026A196ACB390CB6E490A,
	LTDescr_setUseEstimatedTime_m1DCE69547A9FDA123D41FC468334AAB464A9E27A,
	LTDescr_setIgnoreTimeScale_m74876A4244E7C476990FD9F49A32F2077733D10E,
	LTDescr_setUseFrames_mC5C200981C6D66E22A194C46E4E68F4DEE93A583,
	LTDescr_setUseManualTime_mA583DBD31325B81D677BFDE10848D522F3B13917,
	LTDescr_setLoopCount_m5F4204D6F059EAB72ECF645AFFF1909CE328E9A7,
	LTDescr_setLoopOnce_mC7EF69D620AE0723DBF4F7786DDE722A9CC46914,
	LTDescr_setLoopClamp_m26C0A8ECA2F70ADA1C48A2C92B13B47B58E2B1B0,
	LTDescr_setLoopClamp_m45FC70613BB57B5B72A397242071368CCC539E35,
	LTDescr_setLoopPingPong_m53E03879D46B899429F463427A5EACEE747F9636,
	LTDescr_setLoopPingPong_m21667D1F3685ED55D706C025D48849E61D655B0E,
	LTDescr_setOnComplete_mDCB57CB9AFEC6C5FFD63A9B89962DEE80BD1E72C,
	LTDescr_setOnComplete_m63F5635BC23F15B3B64960290B0D723D36BC1030,
	LTDescr_setOnComplete_m2D493BA5F77149765D519E685C05ADEC2F9EE5E6,
	LTDescr_setOnCompleteParam_m4172EF1D7B63D5B2BA5B4358BB8282FF55A5D9C2,
	LTDescr_setOnUpdate_m437DAC29C60F0FFBE61C926313DF48FBB1BBDDE7,
	LTDescr_setOnUpdateRatio_m004D6332C968E0F0059FF807E34770989AA009F6,
	LTDescr_setOnUpdateObject_m770A74E823CD5DB5B6448F7A66DED37B968893D1,
	LTDescr_setOnUpdateVector2_m7FD189610C25124F725477B4DC948F11A69455B1,
	LTDescr_setOnUpdateVector3_m0DDC2E6AEDC0E0EACE3947E4E67A6A6803EEFABA,
	LTDescr_setOnUpdateColor_mDAB53EF324EB204A7038AC70A88978C5E93D8009,
	LTDescr_setOnUpdateColor_mF0B148249135750B5B772E6FA0EFC59A9925E69D,
	LTDescr_setOnUpdate_mDFE9026BDBF9479028A6D2DF03C3F9E1C391E354,
	LTDescr_setOnUpdate_mA5CB1382CE68571CE5DB619406AC5C6EB22729B9,
	LTDescr_setOnUpdate_mEFD57B3855E8C131BA76DADD3FD13C41E867F58A,
	LTDescr_setOnUpdate_mEFD26172DE9342C6C0FB34248E3EA7F6267A1968,
	LTDescr_setOnUpdate_m73910A7E30781DFDE8149BF06376A3CB98DDF74B,
	LTDescr_setOnUpdate_mAFDE1872327547292A82B3D393F4B7007913EFDB,
	LTDescr_setOnUpdateParam_mB449256AC309484AE7FF7E1E685A8B6B5727D4D0,
	LTDescr_setOrientToPath_m6458FAA1A9AACC5BA13A1C83AEB87A63883CFBE4,
	LTDescr_setOrientToPath2d_m135B485E55660A23D7FE3A51AA1C5D3E38804E27,
	LTDescr_setRect_mDE3387640B652E3851AA62B4483FE0150067BCF6,
	LTDescr_setRect_mFF955D0F4C0518F219B3DF1063035FF8AD8B5A4B,
	LTDescr_setPath_mD75B1338F2117AEDC0E243C925038014EEC86974,
	LTDescr_setPoint_mE041B263E9365DE9D2C8A12537A69E59DE02D311,
	LTDescr_setDestroyOnComplete_mD7468721AACBFC786485DC873912EF6DC442D831,
	LTDescr_setAudio_m5AA3E45C784E79CC21DB55C4789C4C3B8DB5FE9B,
	LTDescr_setOnCompleteOnRepeat_m6BB648BEEBC71B54A8EA1CD68E4C09C1BD0ED414,
	LTDescr_setOnCompleteOnStart_mEF3D29199EA20722C91FD0FA1663A279D46036F0,
	LTDescr_setRect_m24CBF3848B9211ED00193D668308C5849294191D,
	LTDescr_setSprites_mCA611B87878CCED8217DD98E25AC53FB8874B5BA,
	LTDescr_setFrameRate_mA3F0847615F3E5A97C192C548D40BDC1B6BC48F2,
	LTDescr_setOnStart_mAA21647EAE7FBD188227754F8F8E175985CD9379,
	LTDescr_setDirection_m1C23D5D973D7CB4391403DFEFBDF12F15A1E75B4,
	LTDescr_setRecursive_mB6A214753796545145520D40C7AF9C7B85B8DC97,
	LTDescr_U3CsetMoveXU3Eb__73_0_mC1B3A6D25B73B3945BA6BAE2A0B8048B2E7706F4,
	LTDescr_U3CsetMoveXU3Eb__73_1_mB298DE15C613EDB42361E42D3935981846884621,
	LTDescr_U3CsetMoveYU3Eb__74_0_mB45AE75CE0B616D21D397C4600C3FA7E19608E21,
	LTDescr_U3CsetMoveYU3Eb__74_1_mC7C72E8D96740B0F4F476545FF9A9CE6579D773C,
	LTDescr_U3CsetMoveZU3Eb__75_0_m2F90BBD09164CCA80404F98AEBE39E0C7C49E140,
	LTDescr_U3CsetMoveZU3Eb__75_1_mB0FCBC7EDEE189C9FC90E38F51C5F36FFB1C6E75,
	LTDescr_U3CsetMoveLocalXU3Eb__76_0_m2E2D10CEC52049D66A18E61B7A6820F5693D1000,
	LTDescr_U3CsetMoveLocalXU3Eb__76_1_m2B451B55022DFA5A2149080DCF5EFEA51DD1EF15,
	LTDescr_U3CsetMoveLocalYU3Eb__77_0_mD773679AD1B3F289E97B8A97FA3C9816B43B0F97,
	LTDescr_U3CsetMoveLocalYU3Eb__77_1_mD806F3C4BD68BD6A88B7E47DD52B9E8EF67516DA,
	LTDescr_U3CsetMoveLocalZU3Eb__78_0_mDFB2967D5068D7DA7D1FBD20D8F621C8A7E4A5C7,
	LTDescr_U3CsetMoveLocalZU3Eb__78_1_m9FE503FE722E8A8877BF3559FFA4EFF806841A0E,
	LTDescr_U3CsetMoveCurvedU3Eb__81_0_m93E47D28F6BC0CAEF79EC1B39091B66D2ABFF67F,
	LTDescr_U3CsetMoveCurvedLocalU3Eb__82_0_m7AB4E3AF580B52EE389B4264B1C3DC08035631EF,
	LTDescr_U3CsetMoveSplineU3Eb__83_0_mB704A557E71BAE0EEADA2DBDC7C85EBDE842174D,
	LTDescr_U3CsetMoveSplineLocalU3Eb__84_0_m71CA556831D55D2294212AF2E49D1C38ACB38C73,
	LTDescr_U3CsetScaleXU3Eb__85_0_mFF1273FC3FC6E6955FDF2804FB5894109E1E6877,
	LTDescr_U3CsetScaleXU3Eb__85_1_mB8660436928C1ABFE1AAA2988DC2AC0BD6CEDFAB,
	LTDescr_U3CsetScaleYU3Eb__86_0_m86846124370FA864FD9E0A6F84F3ACFFB73946DB,
	LTDescr_U3CsetScaleYU3Eb__86_1_m2A79BFC6964E50B4602F3C4D17A372BA1712F4E2,
	LTDescr_U3CsetScaleZU3Eb__87_0_m5BB4E20E122944640691C42C328283ACB0E779AA,
	LTDescr_U3CsetScaleZU3Eb__87_1_mDF138908ABE364C4CA46DA854F9C1A01F3A115F8,
	LTDescr_U3CsetRotateXU3Eb__88_0_m493F46830D9AE2D265D85AECD9CC07542CB2D164,
	LTDescr_U3CsetRotateXU3Eb__88_1_mC1D2B816B4365D56F639D50DDA16AE00A3E6FF5F,
	LTDescr_U3CsetRotateYU3Eb__89_0_m0C043D7BDF837A3C3FDA107395C85D181B66F13F,
	LTDescr_U3CsetRotateYU3Eb__89_1_m4E38375358C4BC49BD7BF69476F9B0FA5525BACF,
	LTDescr_U3CsetRotateZU3Eb__90_0_mA1C23C492D18C89ED39A5E0B2B24F14930DAB66A,
	LTDescr_U3CsetRotateZU3Eb__90_1_mD77CC83BA388F7F3A8E101D31AC5662901F347B3,
	LTDescr_U3CsetRotateAroundU3Eb__91_0_mC30EB5DCA1B236C2D2BFB1BC8C2F620729399547,
	LTDescr_U3CsetRotateAroundU3Eb__91_1_m54A375FE4EE6BB07CB382CDA4B91D655E30D8177,
	LTDescr_U3CsetRotateAroundLocalU3Eb__92_0_m71580817297D4C28EF66A991FE629314AD3FE249,
	LTDescr_U3CsetRotateAroundLocalU3Eb__92_1_mEA423F4B08C78D3F9319DF0CF2C2EA95CFA40595,
	LTDescr_U3CsetAlphaU3Eb__93_0_m62840E0A1F518EEED15E89CA1B8FE51800CB6ACA,
	LTDescr_U3CsetAlphaU3Eb__93_2_m74F5E55F3656D02C098B8D5F51444822BCBF619E,
	LTDescr_U3CsetAlphaU3Eb__93_1_m5E1833D9D31C62DF5234D7AD9CB657BE909F10C0,
	LTDescr_U3CsetTextAlphaU3Eb__94_0_m8DFC387E69EA4F5D71017CA03EBBB3B865041251,
	LTDescr_U3CsetTextAlphaU3Eb__94_1_mF29634A706AB1DFF06D7ED9D5314C9CC10998676,
	LTDescr_U3CsetAlphaVertexU3Eb__95_0_m38DB9282A06B18A985B4FA2A3051210DB0796FA3,
	LTDescr_U3CsetAlphaVertexU3Eb__95_1_m9B16A2B73E285ABB14E7AECE55CACB8C04BE2ECB,
	LTDescr_U3CsetColorU3Eb__96_0_mC4C335EE7D3765075D4287E9C5C4E185A010B463,
	LTDescr_U3CsetColorU3Eb__96_1_mF45002C64332B080B32EE1056139487352147496,
	LTDescr_U3CsetCallbackColorU3Eb__97_0_m0C6A0800CAAB148669FB87B0E686A60E9E0E1E50,
	LTDescr_U3CsetCallbackColorU3Eb__97_1_mD30A488146A9F8DE2876C79404BF0A69361CFFC8,
	LTDescr_U3CsetTextColorU3Eb__98_0_m3369FE91DC4472C45068C9B348E308991CDB0D92,
	LTDescr_U3CsetTextColorU3Eb__98_1_mAD33FF9EE92E06980CBEB4C7759E32DA69F625EB,
	LTDescr_U3CsetCanvasAlphaU3Eb__99_0_m4AC161231D4D1B57CA69B7BA0323F4E6CF0F4CA2,
	LTDescr_U3CsetCanvasAlphaU3Eb__99_1_m2B6D509A77B2396A304997DC39DCB3455E4D198F,
	LTDescr_U3CsetCanvasGroupAlphaU3Eb__100_0_mF33DE2956D5D901DC51B5D9924EFD4FD3E38ED57,
	LTDescr_U3CsetCanvasGroupAlphaU3Eb__100_1_mBA6DC3BCE7F96E4D726A0AC809397EE35FFFF222,
	LTDescr_U3CsetCanvasColorU3Eb__101_0_m7054A2F4C59932B279ACCE46880C9832B8A9D889,
	LTDescr_U3CsetCanvasColorU3Eb__101_1_m6460741C9E869E7B10CB86D933E4BDCA18D737ED,
	LTDescr_U3CsetCanvasMoveXU3Eb__102_0_mFE234BD5C94E07BCF7414CB6D52994226A53EF0C,
	LTDescr_U3CsetCanvasMoveXU3Eb__102_1_m2DA6E431C160C39E876CD86BFF777B6D8D0C0894,
	LTDescr_U3CsetCanvasMoveYU3Eb__103_0_m54D0560CC7DACB6DF5B711C9DFF85C1A08524104,
	LTDescr_U3CsetCanvasMoveYU3Eb__103_1_m25A736629C0A77D4B89641F916444588968FA12B,
	LTDescr_U3CsetCanvasMoveZU3Eb__104_0_m334C0650BE3F004D3246A37F9703EE58F310AFEF,
	LTDescr_U3CsetCanvasMoveZU3Eb__104_1_m093E0527B343F64B8D424AFFF2FA73EFF4CD23AC,
	LTDescr_U3CsetCanvasRotateAroundU3Eb__106_0_m77AE4A4821E0D9DA232D32B3F85AA6D17B102CE1,
	LTDescr_U3CsetCanvasRotateAroundLocalU3Eb__107_0_m5B22A810186C6A02EEA6C3D1F9E7823A58F48A8A,
	LTDescr_U3CsetCanvasPlaySpriteU3Eb__108_0_mCCC3A883C06689CE1AF4E56F599E17E6A08063D9,
	LTDescr_U3CsetCanvasPlaySpriteU3Eb__108_1_mEEA14A276656406AA550A39A45E0EF0DBCFCAD00,
	LTDescr_U3CsetCanvasMoveU3Eb__109_0_m9B67812E976E616B40413119359E8A358B779DD9,
	LTDescr_U3CsetCanvasMoveU3Eb__109_1_m515B75E2D9C1099D35D29062A747D6881F3FD22A,
	LTDescr_U3CsetCanvasScaleU3Eb__110_0_m66C78DAA27E664E37107383D3F72D9EAE85E93B5,
	LTDescr_U3CsetCanvasScaleU3Eb__110_1_m7DED18C9319531415228CB9DAD9E40A0578BA7D6,
	LTDescr_U3CsetCanvasSizeDeltaU3Eb__111_0_m622448B8EA313708FCB496103613BE9368191CFA,
	LTDescr_U3CsetCanvasSizeDeltaU3Eb__111_1_mEB78ECCD2C73EDA02A10E61A43F44B0522D99FA8,
	LTDescr_U3CsetMoveU3Eb__115_0_mA9FC21145770DFE3CD83595E33E2A3F9CBA088A5,
	LTDescr_U3CsetMoveU3Eb__115_1_m268228A84C3365C7DFD41C66717C908DD0408766,
	LTDescr_U3CsetMoveLocalU3Eb__116_0_m81C2B5508318FB8C79015D00599694265975859F,
	LTDescr_U3CsetMoveLocalU3Eb__116_1_m4FC9E996AA295A46BEAF965E85DC9372A8A1C081,
	LTDescr_U3CsetMoveToTransformU3Eb__117_0_m4A1FC4A154E19A07F743509A9044C9447DB2A24E,
	LTDescr_U3CsetMoveToTransformU3Eb__117_1_m1CED29CFFC5D0BE430F5DCC24367B64CEE4E43F3,
	LTDescr_U3CsetRotateU3Eb__118_0_m23A4A1AD7B915AC5EA95B9AE5231809F4E7A7D97,
	LTDescr_U3CsetRotateU3Eb__118_1_m575493C2DA420D01FEA96EE0AA7E4871430EBD55,
	LTDescr_U3CsetRotateLocalU3Eb__119_0_m8A85181B21112A32CE5CCE169692DEDB501B4E23,
	LTDescr_U3CsetRotateLocalU3Eb__119_1_m41757196871546467230FABFFB37075B85251B1F,
	LTDescr_U3CsetScaleU3Eb__120_0_mC7315F796459D639557D6B0F54514AEED391BA35,
	LTDescr_U3CsetScaleU3Eb__120_1_mE6F7F8EAEC18A9A19D4D698E0FD8034E91F763F6,
	LTDescr_U3CsetGUIMoveU3Eb__121_0_m1224EB2264D882E8A0055B5AE36CEEE740B3F21C,
	LTDescr_U3CsetGUIMoveU3Eb__121_1_m1A995AAE2505372C605DC64A13E5F8D9282307FE,
	LTDescr_U3CsetGUIMoveMarginU3Eb__122_0_mA08F3D978D14B8FE43A13FD6C51C6EA287D70276,
	LTDescr_U3CsetGUIMoveMarginU3Eb__122_1_m6C40E08EF41DE0B32C19DF4483E1D153D1CA04AF,
	LTDescr_U3CsetGUIScaleU3Eb__123_0_m713651C4163C0D725D46C2CE175BEA1E8A541770,
	LTDescr_U3CsetGUIScaleU3Eb__123_1_mD45B431D66ED1E7AA392547A69C2F14698FC31DD,
	LTDescr_U3CsetGUIAlphaU3Eb__124_0_mB1E87936B50CEA5B8066511633A64DC06C437B7D,
	LTDescr_U3CsetGUIAlphaU3Eb__124_1_m8E111699E7BB33552DF1284E5B6E793D53A0E32A,
	LTDescr_U3CsetGUIRotateU3Eb__125_0_m608C2F49CC8E321754F2F2C67186893EC1BCF121,
	LTDescr_U3CsetGUIRotateU3Eb__125_1_m3CF50F3F6A1980ECB3712F1A437DF9A3EE9A7A17,
	LTDescr_U3CsetDelayedSoundU3Eb__126_0_mC6AFAF26799C7A75AE029A2F1B6ACCFC0EC9C714,
	LTDescrOptional_get_toTrans_mB530A713C24C3F06F8B42AF31CA3FF977AB7EE0C,
	LTDescrOptional_set_toTrans_mD1A169A39C828FA5536708E3A19F1A1D24370ADA,
	LTDescrOptional_get_point_m684081D83B6A9895107F41A78E6FB2EF4F871A62,
	LTDescrOptional_set_point_m699F8960E9420CC80E624E3A914294836144D84B,
	LTDescrOptional_get_axis_mB136A693C94957550FB4A493D8055AF2A8498950,
	LTDescrOptional_set_axis_m3B316ED04F0DD39AA46549611DD2A87CB09563A3,
	LTDescrOptional_get_lastVal_mF32405DF5E4967E0526173E12A1D49ED459DEE96,
	LTDescrOptional_set_lastVal_m10DF605FAB025AE13E603C2272486F8A4CB90A92,
	LTDescrOptional_get_origRotation_m07382A5C09A8A47B7D9C9DE129BD7CF56E59FDBF,
	LTDescrOptional_set_origRotation_m72912AB74F93857BB64706F91607E176B7C5B084,
	LTDescrOptional_get_path_m70C0472D3F2618618EA9A0EA152799AD814CBF56,
	LTDescrOptional_set_path_mBA7489CB0E86A582174D0A5E183CC0BE9FF859A0,
	LTDescrOptional_get_spline_m8DF792E3FA917F95827962B53715396B14B58CFE,
	LTDescrOptional_set_spline_mE5EEFABD4DE99672B93F6A46B09B90C272EEE01F,
	LTDescrOptional_get_ltRect_m6906D85E922AD1856AEEDE0E2736AA2CAD13401C,
	LTDescrOptional_set_ltRect_m16894F8919730156E369DB2273FDA7EFF3736C1E,
	LTDescrOptional_get_onUpdateFloat_m22C332D592FA821F4A74034B3F77061500B9728E,
	LTDescrOptional_set_onUpdateFloat_m2396684827436DC9B10053C9AC2C10A53DA8AECD,
	LTDescrOptional_get_onUpdateFloatRatio_m6B22F53C31109E96E7E48E43A9B44651838F7945,
	LTDescrOptional_set_onUpdateFloatRatio_m94CE022ED233DA9D79AA3F18C8E6054C2D8E6573,
	LTDescrOptional_get_onUpdateFloatObject_mBCB3D7865F5F9ABAC4C5A03DD91241BF2B1AB601,
	LTDescrOptional_set_onUpdateFloatObject_m81041094C42F5FF531D4C26135A74444B4EAC4EC,
	LTDescrOptional_get_onUpdateVector2_m310F0582519C2897287E219E5AE5EB4A034F3FD1,
	LTDescrOptional_set_onUpdateVector2_m843CED0BD5F49E0B9CADEE1E0D5C1F418A77F5BB,
	LTDescrOptional_get_onUpdateVector3_m1EAF8967B52F428EF0BB00792489D5F9DBC2C959,
	LTDescrOptional_set_onUpdateVector3_m8AB5DC8AFCF8FBA6132342A8E662D0BAD10B22E0,
	LTDescrOptional_get_onUpdateVector3Object_m1BC1054A2FBE8FABCE6F6DDB453A60685C8B86B8,
	LTDescrOptional_set_onUpdateVector3Object_m98BFC4AA1096210EC4D9374E8AD0DC61E927FBEB,
	LTDescrOptional_get_onUpdateColor_m2ED4389F5DCBCE5D9157F6DB12EEFE229D279006,
	LTDescrOptional_set_onUpdateColor_m71FBD24255ADA5C0CBA8E473AFB71F18D4A958CE,
	LTDescrOptional_get_onUpdateColorObject_m8942669D7FE94243BCF809D11BAF52E289CF3065,
	LTDescrOptional_set_onUpdateColorObject_m9556E2C730376300992A2388E38161C59B7B87DD,
	LTDescrOptional_get_onComplete_mFF2D0179EF228E49F57BE663851C6BFDB45044B6,
	LTDescrOptional_set_onComplete_m5337F3EF337F442B0CBE09C1721EAC212FE306BC,
	LTDescrOptional_get_onCompleteObject_m6CC2CEEEEE1FA541AEC4D71580593EB4A603CDF0,
	LTDescrOptional_set_onCompleteObject_mB19BF5653BE9F370A1AE846055F9B546C4D74D7B,
	LTDescrOptional_get_onCompleteParam_m428D156539D89C6007E0BA22E368A7B8252FD512,
	LTDescrOptional_set_onCompleteParam_mEDC4259A922981661AEAF55E9B845B4514BF0FBD,
	LTDescrOptional_get_onUpdateParam_mBFC0491E0705696A41C4DCB044F7ED0D70AB1BEC,
	LTDescrOptional_set_onUpdateParam_m795BC6412556889BC4503D1E571DC2FF722643E0,
	LTDescrOptional_get_onStart_mC70E09328F589CFB5965B061923AD4FEB6D4B2C5,
	LTDescrOptional_set_onStart_m529CC6B3EE612FF8C1F736BC185A090C1F04C2FA,
	LTDescrOptional_reset_mAE8172FF1FFB9054153167751ACB4A3756853159,
	LTDescrOptional_callOnUpdate_m10CF5AF6AE9BDA730C97538C3E164DF2961274AB,
	LTDescrOptional__ctor_m50C4F69D364DD967ACF09DE82F78E20D3B15AB0C,
	LTSeq_get_id_mF7BDB8790E2BC1B542C0F5A4260AADDF48795D3F,
	LTSeq_reset_m342DA923D9419644BF3A4793A0606AF0A5C133B1,
	LTSeq_init_m5315CCEC0CACF7895E5B1D5F617F589D41741A1B,
	LTSeq_addOn_mD55953C7985C4A08B02742D62A7E010367F68C2A,
	LTSeq_addPreviousDelays_mB6198AF3CABBB0D35D0AC220991530A5DD2155EB,
	LTSeq_append_m0B12C0E6C61C2780F758FD94D903C716E72F68C0,
	LTSeq_append_mC0F7F9A6570CBE8BAA7112CF7217D6E164A413F4,
	LTSeq_append_mB7C3B091C37DBD0042777E6F099687007BAF5D17,
	LTSeq_append_mDF7F824A764228060EFF8BBA69F903D1795D499F,
	LTSeq_append_mD32A66D2490D4B839B6AAA8B0B577895941E61EE,
	LTSeq_append_m76F5F63AB647BB18EF33454A98F105D75E9E9B80,
	LTSeq_insert_m2B024BE54107A8CAF3AE6B271FFB3CE86441DD9A,
	LTSeq_setScale_m896BA18CF4C685EF34A26175CED8C6906043568E,
	LTSeq_setScaleRecursive_m78C901915A35EB641BDBD9FB639199463C49639B,
	LTSeq_reverse_m777F21FCED971A010923D62CD068626ECAAD9DC5,
	LTSeq__ctor_mB6D4797A9F32BE64240BC1D4FB9A0A8B6D824E2C,
	LeanAudioStream__ctor_m531CEA7A157EA4A83F540265CA7160894005CFC1,
	LeanAudioStream_OnAudioRead_m97338215EAA44081B43443314379D323FA30C308,
	LeanAudioStream_OnAudioSetPosition_m5B845BECDEBA740CB23D0725D335BBAE1B2FA4B4,
	LeanAudio_options_mB74094BFE3791F34AD4614CCB425AEE5228AAC83,
	LeanAudio_createAudioStream_m86F3E14A3FF84DC88892A8B7F16D92224BD15D8A,
	LeanAudio_createAudio_mB693792EC007530C3DCA798A57ECCD7BFE67D426,
	LeanAudio_createAudioWave_m3D9DFFC3319332471839B2D76C5266629AAEED04,
	LeanAudio_createAudioFromWave_m8FFF705E1832195B4E0EAA977C9F4CF61FA33058,
	LeanAudio_OnAudioSetPosition_mFF60A18D3230037BCEB08585DDDA7B27F8B71C4F,
	LeanAudio_generateAudioFromCurve_m7E8EFFD1CC78766F82416FD21A47889DC8D31923,
	LeanAudio_play_mB81F9043651130080D3F0CE28033BED5EB39E0CB,
	LeanAudio_play_m32A1254BE2452ED5367119EFFF686D9595E4D57B,
	LeanAudio_play_m3A30E48BBA88B9E8881968633686928B854544F9,
	LeanAudio_play_mCD89719473F04DF4A0CBDE0FACBBD831A8DD2ACF,
	LeanAudio_playClipAt_mC5C4CD778BDCF8D897740C2B3EA8415C631BCF3F,
	LeanAudio_printOutAudioClip_m2783A6C26F9A85D837099B9A4010A3523C9BDA90,
	LeanAudio__ctor_m88410F3D66A6EDDF53D657CF52DBBCBD40E7E8D8,
	LeanAudio__cctor_m5647818341FC7B5272CFCDD6966CA0407EC9D131,
	LeanAudioOptions__ctor_mC4A118B38AA350E5502615E56400F51A43B3B60B,
	LeanAudioOptions_setFrequency_m2534EC3E99ECCB13667B5703931133E97CCB6459,
	LeanAudioOptions_setVibrato_m493C38946EC65F6B1C2E811A1C232CF8C40E8876,
	LeanAudioOptions_setWaveSine_mDADF8446B8A6BB14326F15C6FD010E43A6B5A84B,
	LeanAudioOptions_setWaveSquare_m694508FC09511CF1BF371B7FA456BE92D503322F,
	LeanAudioOptions_setWaveSawtooth_mC60BB12911D96EA79544EBF3780588CCB0858EF7,
	LeanAudioOptions_setWaveNoise_m92B623A5316643EACFC1CCEF761D463EA7F53C15,
	LeanAudioOptions_setWaveStyle_mD446529C32C1D2809CE0BD19558B9B9A1CCC7381,
	LeanAudioOptions_setWaveNoiseScale_mFE6B538697857F28C0077DC7C7F3C34CC00DD5B3,
	LeanAudioOptions_setWaveNoiseInfluence_m321FC18AC3DB87F3F22658CB134088F6AE8B0A5D,
	LeanSmooth_damp_mC64F76E7BEB749AE0C6620500B8F7A124C8B2281,
	LeanSmooth_damp_m2AE19609C34606AAC949DD5737F7C905D6D1FAEC,
	LeanSmooth_damp_m7155F328C1DE25635DFE88A26F308E83D559C084,
	LeanSmooth_spring_m13E4D3125D99326FA237E391E6AFC87321D643A7,
	LeanSmooth_spring_mB9D231C3690876EE5299B0261A5B101C0299E3C6,
	LeanSmooth_spring_m93FC4C89467835E4367BE781881F47A7E7416B36,
	LeanSmooth_linear_mE0319D93C18D8C6230B69BB6914BC2171D9991FC,
	LeanSmooth_linear_mE1E0D6A877174839F0D805E0E23AB7B9869F99BE,
	LeanSmooth_linear_m5F142DE9C4019A6163ED38F547F8E26D6FF4C1DF,
	LeanSmooth_bounceOut_mAF91D979E058FDC80D6E7BB35FDBDF5D1C584930,
	LeanSmooth_bounceOut_m213E8992275BA3F7E5DF6DCB584C2E4EAE055163,
	LeanSmooth_bounceOut_mC0BF320EBCFCF7EAD5CF449073C707968FC27F1C,
	LeanSmooth__ctor_m14E227157D0426A2F47C06DE30CB17982B72BF66,
	LeanTester_Start_m9A9E920B5F046E3372B0366E0BC9DFC84A919F58,
	LeanTester_timeoutCheck_mC313EB95692CB44EBD8BA1A87AA815BD5416B150,
	LeanTester__ctor_mC69E2DD7669788DEA00909ED2351EAAA6CF317C3,
	LeanTest_debug_m6C2E9AD4476177558EF713F252CB2FCEDB14DEE3,
	LeanTest_expect_mD39E3D849F6BB08F68658A3EA543DABC2ECC9B77,
	LeanTest_padRight_mD8E69C06CFE450F91AE8B44FD304BAECDCB25F91,
	LeanTest_printOutLength_m340E6C2B8CE237DD562D42857AA79E56724407C6,
	LeanTest_formatBC_m61FCBB26428987CBC8EAD380847104E662D747CE,
	LeanTest_formatB_m3A16FF8B95ABDFCFEAF5C5B3591F5D84867A7954,
	LeanTest_formatC_mF76064C060FB981E2535F8A487393A849D5DD292,
	LeanTest_overview_m54D7E1E1324A5D63C8834BCBDCD9627490C9FC37,
	LeanTest__ctor_m08E8F412E4EBF00D54D3AB5D8EE7E0015C3A04EF,
	LeanTest__cctor_m589610FAD222BE5A0012463E6AF9D88514317EBF,
	LeanTween_init_m742C83A1FC52BA7DC1A45D77418E2A20C6EA3FA4,
	LeanTween_get_maxSearch_mCBB6EBACEB7810A13B6BFE5E46FF93D9B576F8DB,
	LeanTween_get_maxSimulataneousTweens_m29E755842BECB453B666A30118106AC8FA3ECDA9,
	LeanTween_get_tweensRunning_m7DAE5C7327AF47F20CEAE9B5D7BE7460C53CDC46,
	LeanTween_init_mF9EBB839A07AFBCAEBB72C052B6F8EC4CD48D7BA,
	LeanTween_init_m876B8AA3523AB02C0246A87E9E37F5388FFFB35B,
	LeanTween_reset_m239840212FEA6441ED109FC1E312A2BBE2534AF9,
	LeanTween_Update_mF15DAA93C9C32D4BA1645DB4B03AC67F6C3D8F8D,
	LeanTween_onLevelWasLoaded54_m30E5953BBD22DF6BFAD61B4B62081D26ED1E0046,
	LeanTween_internalOnLevelWasLoaded_mF01B04BD6D4CCB7D5C896B0C9381BACE2A6996F1,
	LeanTween_update_mE2EEEF001DE108CFA101BB549033B6022655C0E8,
	LeanTween_removeTween_m47036DA44D4E0AF9443A2A459B10F51C039BC835,
	LeanTween_removeTween_m5183FCF6228B6149D948D3175B1906EDE9EB5A2F,
	LeanTween_add_m94B30FC92F9702D1C000E53F0481E2547BCEA152,
	LeanTween_closestRot_m425E4814D09E1E33641F79E253706B42A8677DCA,
	LeanTween_cancelAll_m57047A3005FF66C0137640192B7BDB3DA6CD2F27,
	LeanTween_cancelAll_mDFE02D9737D5CFFBBBAB9B7ADEFF4EC8A2EC6BA7,
	LeanTween_cancel_m5FAAE217AED5E47A774964AD4B49CBF06BB4CFAE,
	LeanTween_cancel_m8C912896B48485DEF6C233BDE9515DD9BCB37F4D,
	LeanTween_cancel_m83465350000117665934A8D6556E5759C60E4BA3,
	LeanTween_cancel_mC1789BEE750E0F49BBB9A5427837EAAED0F6B362,
	LeanTween_cancel_mF06045394FD8DB00302C26CBB06F791FC20BE002,
	LeanTween_cancel_mBDB0EE47F9FEA5CDC99DAE5AB071A317D1646E73,
	LeanTween_cancel_mFBB7196E53B68C2337C13F0AA1F6F96A9E652995,
	LeanTween_descr_m673151C22BCD383E070208DC886A48EA5682354E,
	LeanTween_description_m614B276C8B85841618BC11CC53EC3F9F998B12CD,
	LeanTween_descriptions_m15F1EE30DA28ED509BED8B1208D21C6A305D0F7C,
	LeanTween_pause_m544CC2BEF9E44BF1FA9C2B02FC8C8D8D840303F5,
	LeanTween_pause_mD334E7016A1280D8612923E19FE0EAC3A99213C7,
	LeanTween_pause_m34D1984074FEAD898CB5A732E153230C05BC0F8A,
	LeanTween_pauseAll_m9A08262908FA3B89F629A53310A615960E25681F,
	LeanTween_resumeAll_m0CD4B24182F95AB48670CAA51B31356DA0298F02,
	LeanTween_resume_mA7A5417B25090825B935A5A53BBCBC0DD2CA60FA,
	LeanTween_resume_m9A7648F170B3EF8AD8CF448578A1423E5AC57DC0,
	LeanTween_resume_mED2ED2E734874013D07AB1CA160FA30098BB93CA,
	LeanTween_isPaused_mEA3B9A77082F25532A58E01A3B16F0EB6AD3C9D1,
	LeanTween_isPaused_mF0B834CE3C9278DF00216045E11ACE5EE9D66354,
	LeanTween_isPaused_mB1A22DBCB84A1FAD2A2D5E0B7F2307A77D1C630A,
	LeanTween_isTweening_m8565F710EC2593893864BDC02B0F286E58D77928,
	LeanTween_isTweening_mD5E04C094ED73605C74804ADCF9B13FADDAC9470,
	LeanTween_isTweening_m7300B17390DAF00D71AF9837EFAE524A0F067607,
	LeanTween_isTweening_mBEB3331D582A3CF1DFC989B9538934CBC8B29F39,
	LeanTween_drawBezierPath_mFC7D2D0AF1632D71D25BF9B390285DAFB57A1FBA,
	LeanTween_logError_m37F7E5A9E9028A282D3FB22858AE9E751B5B7E17,
	LeanTween_options_mA6EBBE759321A9ED63E09FCA3E19257A5732399C,
	LeanTween_options_m96D5FD6CD10BC38781B78A7ABBAFD40FD6896C10,
	LeanTween_get_tweenEmpty_mAD2C44A742A9DBD681D325C9EC5708FB4B2DDAD9,
	LeanTween_pushNewTween_m5B8A1F2059CF7A5AA5A994254FB6FF2325FDB5E8,
	LeanTween_play_mCEA864283E853E9E7086662D30B2B35229601255,
	LeanTween_sequence_mF0E50A4160E6E03ED0B1B6A57B25552E195F1E60,
	LeanTween_alpha_m03563AAF9FE673DAEAD06EC87B702F6633009459,
	LeanTween_alpha_m8BD4F21C3DA8E6057BA72EB4500336A8AD2D23D0,
	LeanTween_textAlpha_m56966F76D08E1D2F771F68FAC336CD26A3789C4B,
	LeanTween_alphaText_mDF874DA494D7B483C1AD62D2459795583C87EA50,
	LeanTween_alphaCanvas_mD7760E3B438F098F8E52C0E19BE66AB042700482,
	LeanTween_alphaVertex_m9C46F3D3D5156962E5E6DAFD6D3B91D87A86CD77,
	LeanTween_color_m37AE0DBE5DBE82AB6CCF14D76DF1EFC01D95B69F,
	LeanTween_textColor_mEF9FEF414B74FE47A423CF17CA79712A0D1FB658,
	LeanTween_colorText_m15B3D867D6A42D0D7770B97CB2E7E200F0D661B2,
	LeanTween_delayedCall_m04427AEA4FBB41C4A3CCB898D64BB4AC740BF03C,
	LeanTween_delayedCall_m899A2AE8C3C19487CFA5AB41977F67D01F9DD4E6,
	LeanTween_delayedCall_mA05A41E42F1921AB3867C1AA9B43100FC5F57F2E,
	LeanTween_delayedCall_m7A1A97BE2037CF29C9C68D5DC41476904215AAA0,
	LeanTween_destroyAfter_m7641087D50C872EB2A5BEC69287673C0AA63A6C1,
	LeanTween_move_m1191EC5B42C368CBBAA12E7EF8EFC8378980F2A5,
	LeanTween_move_m0AFE5CA5D827F49074E9ACA6C73E2E6D6E78E79E,
	LeanTween_move_m69E063649877CD64A4755003DB27E1608A41C08B,
	LeanTween_move_mEDCFBFB4014A966F52A3DC08FE2E952DCDC53E95,
	LeanTween_move_m8058A34EDCE00BFEE3336DE5698A3927B796A796,
	LeanTween_moveSpline_mA1A07399D0522C220E7C062CCA41CA1A5F07781B,
	LeanTween_moveSpline_mBC72A4210E5EBE1678828E39AA926A53D72C2EEC,
	LeanTween_moveSplineLocal_mC1BF1287479F66BEADB1E325D9DED97A6E69E67A,
	LeanTween_move_m3ABE07EDBD7AD98100B8545E5AAA864F03A4F6A4,
	LeanTween_moveMargin_mCFF341DFEC796404C5DC2979392988C15AA31620,
	LeanTween_moveX_mC528E208B5A163AA4C5400294A2D03C8E5448DED,
	LeanTween_moveY_m9E9F3F2DD9FC9791FB42E952BC71A32858E9D894,
	LeanTween_moveZ_mA8D3635AAD59B698E00B6AD6981BE2B8E2A381D4,
	LeanTween_moveLocal_m4628772CED24062B5B96C041E6C3FE3B64DE8252,
	LeanTween_moveLocal_m6B50D6459C312416C631F3C391C76022ED19C5D8,
	LeanTween_moveLocalX_mA9353C4CDB6A49FC6F7B9568E70D4D3C210033A1,
	LeanTween_moveLocalY_m605F33473A6318A1BC2719D5032D048B2206ABA9,
	LeanTween_moveLocalZ_m0B7FEA9EDE5CDEB065F4187E11F174465D7E8EF6,
	LeanTween_moveLocal_m52BCB9FDEEFBB74988883648CA96FBA390F991A1,
	LeanTween_moveLocal_mE6FF69270695A740FF6950ABB1C389BD87336445,
	LeanTween_move_mB930433417E02C8708DA33ABFDCB478397AA070B,
	LeanTween_rotate_mA9190F8C72DBB9FF8A0998C936208C97D18236AE,
	LeanTween_rotate_m3D39319C8CA729B58884DED8AAB29903BAD76541,
	LeanTween_rotateLocal_mB981D5E76B2EA75987333079E8EEEDE7F78740A7,
	LeanTween_rotateX_m39C9891518C637C4DFEC9C6E455DF5562AAB1050,
	LeanTween_rotateY_mF66A36114A32D24BFE92D94B7944838A19847B90,
	LeanTween_rotateZ_mDBBBFF6BFBE5B724FCE59D3A6EF40F377431C1C8,
	LeanTween_rotateAround_m061DF6B0D1F4FB9C9A635C2B5C8D5F0D0486DCF9,
	LeanTween_rotateAroundLocal_m8498C0D937E0D12B6EF6C43F5F1AA7F4FD721726,
	LeanTween_scale_m4CE3CCD970CF75664CF7FCA1E6286F418FDE2A0F,
	LeanTween_scale_m209552C2E41F5D6B0B1525A25540FE40A33F4ADE,
	LeanTween_scaleX_mAA4702337267BB96770EA128A25EB894E4C7E2AB,
	LeanTween_scaleY_m664993B4D0F2296E8BE7BF8168D17A90EEEBB3D5,
	LeanTween_scaleZ_m4F475E490D515FAF13E36488615B3C9972EAC2F7,
	LeanTween_value_m915028F98333F18A1326931DFAC8106B6301D23F,
	LeanTween_value_m589DDED951D44371EB0589C01EBB679977F47556,
	LeanTween_value_m4887473E05B0379914BD3795CA6697D274C54606,
	LeanTween_value_m3EF8D28045541C4387176018135B82C81F1FD38E,
	LeanTween_value_m3F8D4D5F95CA382EA0D770B07EC74C270B7EC526,
	LeanTween_value_m115667D1977F3E7BF517B9EA3EA3EE161A8D9D39,
	LeanTween_value_m169AE41FDE8360E78E0B637CEA5A114E112BA45A,
	LeanTween_value_m1709E165F0E2491A98561574CE72092EF202C1F5,
	LeanTween_value_m931E3256745BE0E7AF60C79DEE155EC421C55EC6,
	LeanTween_value_m4FEF278D52CEFC694E29730BC229D3C52F7070C5,
	LeanTween_value_m0846E512D96A497507FAA08EC3747DA89CD6FB7D,
	LeanTween_value_m0E16AF4A36148D3831CBEF54C0DFD01CE08475DD,
	LeanTween_delayedSound_mD225DC785D5C479E00DF03A9406C5EF1A145446A,
	LeanTween_delayedSound_m17C87E524E93E54A7AC33CDF95F5560831E0A6CA,
	LeanTween_move_m9EDD3EF2EDA02B6524829C56199400A302CC7BD3,
	LeanTween_moveX_mAE925994CB0FAE76F0768A0595CCEEB5C440031B,
	LeanTween_moveY_m7FC0800EAC2943A5DF5F0652CD254D40EBA15BD1,
	LeanTween_moveZ_m3AF920F4CDB4AE4E359A577F223341EF73CA9F04,
	LeanTween_rotate_m15EB5A2DF0974570B758B596AF032A42A1EB5F6A,
	LeanTween_rotate_m362D5FBBA0847D5E7A4C8D0037355D7192F10ABB,
	LeanTween_rotateAround_m6149AA8E86FFC604EB792197923D69BA3BF46823,
	LeanTween_rotateAroundLocal_m20B862CE01E52B5C4533D2512D718EDA1437B57A,
	LeanTween_scale_m47628FC09C70C5B77089F8AA97B7682F849EC0A9,
	LeanTween_size_mEC9D2E7E59F8EBFC0CA941CADD467CD1A5491B43,
	LeanTween_alpha_m640B7551150854D49EC0BA41B7CA866B7A6CBF6D,
	LeanTween_color_mD14B3C4D1453119045FC7D74FB6746287E9BA037,
	LeanTween_tweenOnCurve_mF41225AC36913842DA74788977F04689BB139920,
	LeanTween_tweenOnCurveVector_m517A4B35EE61BAE6ADB0DF2D84863D280540DA59,
	LeanTween_easeOutQuadOpt_m3C04BFEED580755A923F99E89B4F023F62450868,
	LeanTween_easeInQuadOpt_mCBC13EA35ADC87B69182C6DDAB43904CE2649BC6,
	LeanTween_easeInOutQuadOpt_m430D10EAD4D0A1FEC9AAD1E33F67B477EE72FD31,
	LeanTween_easeInOutQuadOpt_m6496C1F3A68C113F8798961D6F0E357BAA178A34,
	LeanTween_linear_mC1ED9F0ADBDA8939C75B3BC0914E50DC34CB4F6C,
	LeanTween_clerp_mE62F3DDE55042FAFC1484ECAFC2243E82B86AE62,
	LeanTween_spring_mF690BE574938A3A8F8EC8248642F31ACD7785F32,
	LeanTween_easeInQuad_m4D2FD34AC16BDD31B2838E3A5D80916469833DF7,
	LeanTween_easeOutQuad_mB929599F71CEDB7C7C8579B9C8BA97500166584F,
	LeanTween_easeInOutQuad_m584E55A5AF9BF428B143857479D7D790CA38745C,
	LeanTween_easeInOutQuadOpt2_m6F3DE20B7BFF5E0F5B34C1246C40F9AF115F7799,
	LeanTween_easeInCubic_mE4683158D3ECF4DB9FBBDF6187759886DB83F853,
	LeanTween_easeOutCubic_m42408FA585BA84351F5CD74AD1A3ECED10FD8B91,
	LeanTween_easeInOutCubic_mFB2A765901D685FA00D1C16E6133E53047B7FE71,
	LeanTween_easeInQuart_m1C37EFBC471397640865AF91771EA21DA0ED492F,
	LeanTween_easeOutQuart_m1A24432DC39B7ADD6D87960129D2FDE0C6D03C12,
	LeanTween_easeInOutQuart_mBB2A5F122AC70BF79993F1180AC9A0D77D9C139F,
	LeanTween_easeInQuint_m1104B28EFA33850E5CB9BDC867D4C67B4D90457B,
	LeanTween_easeOutQuint_m9900891B73DD3AC15F28155E1EF340B260C8C67F,
	LeanTween_easeInOutQuint_mC67EDFA9A9953B9AC7C073D7211D115B37BC7B41,
	LeanTween_easeInSine_m81F6144AC64B5E5A4ED990C48A36F5A3D275525C,
	LeanTween_easeOutSine_mD6871A62B4F306845FD33F26B9B82E5CC43DDECC,
	LeanTween_easeInOutSine_m74089B5D05EA9EA3FB89A14DC48F1D310939320F,
	LeanTween_easeInExpo_m3B47C6876FA3DDCDBA8E27516A9D5A92AB8A0F96,
	LeanTween_easeOutExpo_m90B7C8EFAC973AFD5B0FBC48AC5277F3ED95BA4D,
	LeanTween_easeInOutExpo_m1818C9E2CA4DB499749B6E701C523FE50C57CEB7,
	LeanTween_easeInCirc_m32983AB18E1A8F9632D5D3350FE72AAC235F9DF9,
	LeanTween_easeOutCirc_m9C4FF7DD334929D7993925BBCBB8FD8D2D198134,
	LeanTween_easeInOutCirc_m4AF54D1824BB627E59856E1EB3FEEE9801A24926,
	LeanTween_easeInBounce_m4D853643882AA66C8BA82904BF06C8FD89172577,
	LeanTween_easeOutBounce_m1790E9DEA28BAE58ED7FD4606E999056A1074136,
	LeanTween_easeInOutBounce_mDDC1155A0C82DE93397F30C80DF6059685229413,
	LeanTween_easeInBack_mF3612B1794D308B659FBF9FF3DFB1BE11C4C5C3E,
	LeanTween_easeOutBack_m7B845AD4DA44D5C0B79DFFB66BFE8D1EA78BB57D,
	LeanTween_easeInOutBack_m1A651107C2734CD9259AE6F3422D1544D9C445E2,
	LeanTween_easeInElastic_mC0C2B9BD7D1ED6B6624F52DC5D8F6D2E1C1C575D,
	LeanTween_easeOutElastic_mDD7A06B6C97A24B25C34A4191312D6F8E64DED7A,
	LeanTween_easeInOutElastic_mC2E44F37BF3623A004AD1B60EE3AF7968DB21D2B,
	LeanTween_followDamp_mA9C52B1FBD62D1491E62714A18E092E939948A12,
	LeanTween_followSpring_m6A4518BC770FBA7041F96B2D93910A874D8DE95D,
	LeanTween_followBounceOut_m30E47CECD44D0928C51E4868BD229F8C3DE5939C,
	LeanTween_followLinear_mEF3E3F81DB95C8FC7512D9EB79B1A29818537333,
	LeanTween_addListener_mEC31B40DB9ED344E2F2BAE152DA564537AB16754,
	LeanTween_addListener_m34C97CB48F8B906351A79F49A6CECC6334BBD3C8,
	LeanTween_removeListener_m982E421FADD5A0991EBDFBD169F5BE77740ECE3A,
	LeanTween_removeListener_m39242CA2654CC5FC27C4D08B45F0F71888164F41,
	LeanTween_removeListener_mE054AB523D46CA58B0C59101C79AB1C9E680072A,
	LeanTween_dispatchEvent_m2996C15CD7A7C91A70A896DACBD85AD50596EE74,
	LeanTween_dispatchEvent_m746D9A2E728E4018D7156A8C05A2B2113151E475,
	LeanTween__ctor_m29640AFA36A452E41D7D7FCE837930805FE524D4,
	LeanTween__cctor_m4A2CF2E2F8D1A20278F8B86DCF870EBE903F9AD5,
	LTUtility_reverse_mFA541E145D5DB2CAD1B20BA78057C7A3495556C0,
	LTUtility__ctor_m04573B783EECEFDD4E2AF1FFD4D7B62E914D0F4D,
	LTBezier__ctor_m97D899A3D06174612D033E42F804FB14684814D9,
	LTBezier_map_m3DD6B1382C93E0204A339F62ACCFAB5C3A29DD2A,
	LTBezier_bezierPoint_mEB0E0EF802E14A7FD9AB49F23936260D8B0AFC45,
	LTBezier_point_m017F4A7A1923793B57C7A8A76682609CCFD0E8D4,
	LTBezierPath__ctor_m3180C1A051F04229804D0BB0F4ECCAE916AA6845,
	LTBezierPath__ctor_m431529CBFCA2BEBB9158B0E80E9AFDBB2F9EF7C4,
	LTBezierPath_setPoints_m6717D57148D9DD5B0B3B9F2C67F5060423CC62BF,
	LTBezierPath_get_distance_mE03E0C6CAC8CB0D3DC5CD4F21CCE6AF6A2B5615E,
	LTBezierPath_point_m7DD32C57F1078C21BFF31EF88109EF188FBBEA89,
	LTBezierPath_place2d_mA08CEFF1862BCD64FBF0D6DD7821822BED4F56A7,
	LTBezierPath_placeLocal2d_mBDE2E3BB035F85D3A500AC0B0E36E5038CF3B27A,
	LTBezierPath_place_m080C91568428A12F6CAA5EA5787D0A1EE78569E3,
	LTBezierPath_place_mA0099B55C6D850148EF3F139A3AAA21115FF6395,
	LTBezierPath_placeLocal_m0D2D81B70292CE5B7D595E8E1FECB865E2617F88,
	LTBezierPath_placeLocal_mE7093E9870F33F6B93BB03F804B14011A7C0542A,
	LTBezierPath_gizmoDraw_m32B996E9870AA9A63DD800B3E406C54F697B23ED,
	LTBezierPath_ratioAtPoint_mDBAB488E78E8993C759918B979CCCCD9DFDAED19,
	LTSpline__ctor_m89DAF8F9A02B744F14DC951BF26998ABE636079E,
	LTSpline__ctor_mB9F6B9D5E8250215A98C318B775D0862716E2F62,
	LTSpline_init_m5F8F0ED8EE3B00DFB2020C9B8E7F0589D764296E,
	LTSpline_map_m8586FE88C55DFE6D7CD897651AEBB8E161D4AAD3,
	LTSpline_interp_m07E5A5D423EC7092EF3CCF7FD65FFD65DEF7704D,
	LTSpline_ratioAtPoint_m6BE44439D18029959983FA5D960C319BAA0F5EC9,
	LTSpline_point_mDCF2C08D8F3F338D5749D4478D652DDE9B2949F9,
	LTSpline_place2d_m7715D476897D521AC679B2007254773606199FAF,
	LTSpline_placeLocal2d_m1264CC1B0A061E5B1D399B46F1E6BEE91BE49926,
	LTSpline_place_mDBCF580928976DB68CFA586B31B88C23C6D9FD52,
	LTSpline_place_m4F75114ECFDE9711D5BC6AA012180139DB21136F,
	LTSpline_placeLocal_m47E0DF17D96F80E0EF9E7B2809828BECF1063660,
	LTSpline_placeLocal_mA60E9587B94103C8BD655ADD566F2218D4D2B1B5,
	LTSpline_gizmoDraw_mA538B509F29ED2A1583D44FDC86935B98364FA88,
	LTSpline_drawGizmo_mD374863782B673CE885A2855910958A2DF60A95E,
	LTSpline_drawGizmo_m3C4516917996D8F6D1B40A5017937C2B2BADB1F1,
	LTSpline_drawLine_mB44BF9E463BE732E1FC8A178E92FE3230FA79588,
	LTSpline_drawLinesGLLines_m7E7A763E4DEF72CFF387EAE1B80A1A6D26905590,
	LTSpline_generateVectors_m85FEDD714CEEAD04670F854D11CED89775F05AB3,
	LTSpline__cctor_m6739314E6052E4C78709F02047E8DC2DC9096AAF,
	LTRect__ctor_mD2DD65C62E8EDAC7B218018C87391F7E2F36FEF9,
	LTRect__ctor_m47F4B741D47E9809C1B03A566F06863E246A2696,
	LTRect__ctor_mBE81F7E8AAD5FB8A7E8AFCEAECCD66DA9A30F345,
	LTRect__ctor_m108E96CEDBA3D65FEFBE787F4B17F844CF3638AE,
	LTRect__ctor_m6A1374116E6DC9E18821247FAEDDD60F74FA3758,
	LTRect_get_hasInitiliazed_m2D51CC6C1FCF712275E919DFA2E44F1E860DF216,
	LTRect_get_id_m6F74EEF30562B278C561253B7591297CD389C30A,
	LTRect_setId_mF1BD1C3E2DB5CA883A47FB9E8F644EB16A5B294F,
	LTRect_reset_m53A78C5EA3E9E78CC06DB2F25D1C71A588B06F2D,
	LTRect_resetForRotation_mA8B3D468DCD205AE911118E6EED6C8A479A674A0,
	LTRect_get_x_mD70B7C3B314D4FCDF6AF3BE29F6397C47426BFC6,
	LTRect_set_x_m640BF975EE3C8DFC498BEAFC41742A094684ACAB,
	LTRect_get_y_m168437019BB9D70389C282B93AF269634C80EB5A,
	LTRect_set_y_mAD963E12C76DC233063AFACED81EAF3D03F06D27,
	LTRect_get_width_m622A9922E21390846F78DA4E37A7EE0675D9F5CE,
	LTRect_set_width_m03AEDBAC9CA96B53E086CDD5DAB57A394B1417B5,
	LTRect_get_height_m1C9CB0CBAEC6888479CDF92240484145730F0E30,
	LTRect_set_height_m34DC6F5C00E4A2C16382539CE21524603F6EAB89,
	LTRect_get_rect_m6C78FB7176F49F9F8FBB80160FDF37C57EEEEA79,
	LTRect_set_rect_m7FBDEB9CFFC6BE529A76996FCA83F69517F0280D,
	LTRect_setStyle_m73FC3A26735BB8712D8B2D92357035D9FB8FA1C5,
	LTRect_setFontScaleToFit_m39043C9072BB81E3DFF6C7697404EFB2C3090984,
	LTRect_setColor_m260F2239C224794A8C49B970E5F5DBF5CA594779,
	LTRect_setAlpha_m2B25F81A596DFA252D4C2D0090FAF8ED224AD2CD,
	LTRect_setLabel_m1FFCB5B75A7AA46B894883AD569AF4ECE8662D5F,
	LTRect_setUseSimpleScale_mFD395CF7FAF8EF1E65A4697817CA41A1E4DF22D5,
	LTRect_setUseSimpleScale_m319F90C955999F948E57D8FEFFB3706E9446EDE4,
	LTRect_setSizeByHeight_m17196A565268B33273FE50DE6A9D89D71B2C7AB3,
	LTRect_ToString_m30F891F0B46B40656CD5BC9265D6D2F0AAADEA33,
	LTEvent__ctor_mF1824A2419396C340A300E02A11BD3C710931494,
	LTGUI_init_m56E76FF99D2B3BA59DC04B0CD796B8672BAD16CB,
	LTGUI_initRectCheck_mD1D857FC17862BBF0171D8B30C2629B9FD84D4D8,
	LTGUI_reset_m7CE773801CF53E0AFCA45D4C470DA38930F518F0,
	LTGUI_update_m00A3E17E130357C2B8F88B29572E767536C3C097,
	LTGUI_checkOnScreen_m0B39FD2B120D5FB49801098E46F8705837445E6A,
	LTGUI_destroy_m59B5A127738FD610340C642E74C5FC5ED8CDF2C0,
	LTGUI_destroyAll_mEE7BC1026BAC0D8132D6AD8CAAE8CD4B5249807F,
	LTGUI_label_mA4BFF45B5BC97C4227CC0E47BC2183470B42C5F4,
	LTGUI_label_m9016D43E1833A398F9DE235F106DB4B83E4A3EF9,
	LTGUI_texture_m4EAA8E004AB27F16AF3F70E001F32086C34DF6A6,
	LTGUI_texture_m6D73FEC3838D61DB4B92333120AF15139EEB2615,
	LTGUI_element_m5933C737FE6DB8B7F4AC87B9EBEBF338BE4DD456,
	LTGUI_hasNoOverlap_m1174D1999AEEC77DAB75CC5456C4A28268D72663,
	LTGUI_pressedWithinRect_m585FA74045ACAFFF9EDAEC8469B6C61F4191C6BF,
	LTGUI_checkWithinRect_mE13CE64B1EA92D1287B6B090A75E99832C45E4E8,
	LTGUI_firstTouch_mF464F4A8A19F65CBC9EA2BF2111236BEBB9E618C,
	LTGUI__ctor_m914864F7BFF6D41D79693EED54116EA572A41429,
	LTGUI__cctor_m01C5B6F47B1C4B45CA1D6B77B5720BDBAFB049AA,
	LeanTweenExt_LeanAlpha_m461C89B7ADA172C00449BE049ACF50F3CE55D766,
	LeanTweenExt_LeanAlphaVertex_m78E0D1E1E6D229BB56EAC9F208119463298D79D7,
	LeanTweenExt_LeanAlpha_m62EE99D970FEDB18E72E1CECC8BF3C3C806B8897,
	LeanTweenExt_LeanAlpha_mDC6296AC255B03B668D291C60FCC3F49788DF8EB,
	LeanTweenExt_LeanAlphaText_m8289E2F78CE20F51FD72D83399F535A6ECE4DCD0,
	LeanTweenExt_LeanCancel_m258065657CE313E3C76F55578F793097140D9F19,
	LeanTweenExt_LeanCancel_m3EAAA4F94BA9D26442BF5503CF1C13F56B136E41,
	LeanTweenExt_LeanCancel_m0EBACDA55B64F6124F9863947D3EDFF41FE5BC91,
	LeanTweenExt_LeanCancel_m16FFCC4C3A923A8261181F5D40AF48369B766112,
	LeanTweenExt_LeanColor_mA1A81C489F98EB1520DE95136AB29B46E5EB5D1F,
	LeanTweenExt_LeanColorText_m23602453BAB11CF15AA4ED5E4F1B3962CF6836C5,
	LeanTweenExt_LeanDelayedCall_m67EE573EEA0027172A59DC8E6810F2643050D72F,
	LeanTweenExt_LeanDelayedCall_mB1FF6EE6E33AEAFE689676F8674FDF2F8BC8D395,
	LeanTweenExt_LeanIsPaused_mE920863734EE45312ECA09EA8B998C524E26BFE1,
	LeanTweenExt_LeanIsPaused_m40DD7C17AFF3F2B488C94B529A62F0EC3773B532,
	LeanTweenExt_LeanIsTweening_m11D59C75569AD0E04DA5CAA351D2F68F38C8E1D6,
	LeanTweenExt_LeanMove_mBCF028720C25916836835CBFDDEFBBAE0F747730,
	LeanTweenExt_LeanMove_m090CF72208FA48F140219041C8062A46EF3F3ADE,
	LeanTweenExt_LeanMove_m3D682F2EABA9624478F95DC984962451F4C72335,
	LeanTweenExt_LeanMove_mDE73F1D58AAC128EBF331B493A9B836851AC1589,
	LeanTweenExt_LeanMove_m38E623C91DB8CBB6FDAB138979719D2D1837EA1E,
	LeanTweenExt_LeanMove_mCB11792AADB6F2D82EF43C90709B1CBD79FDDB83,
	LeanTweenExt_LeanMove_m3CC6348A9E1270D6703F193F8CC9B1C2CE12384B,
	LeanTweenExt_LeanMove_mDD7DB276339BE449389D687CCF4793E275393331,
	LeanTweenExt_LeanMove_m1D02677FF9D2509C2FA77A3BE7C3C69718AA191B,
	LeanTweenExt_LeanMove_m1FC68173632416D0BBB72EFC74D82348B0523D80,
	LeanTweenExt_LeanMove_m87B72A7F9F763586108B9F5B648D48428B1F8FFD,
	LeanTweenExt_LeanMoveLocal_mBD123664B63D8DE32BF36F0ED100E9B87996267E,
	LeanTweenExt_LeanMoveLocal_m3149C4DF54C63DB2384A197A0AC8831EC0845703,
	LeanTweenExt_LeanMoveLocal_m97955CB5D282428BC2D2CBE129F6A401B482A8F8,
	LeanTweenExt_LeanMoveLocal_m616AE133C599BAF61CE3159CF579C3DB0EFE7B66,
	LeanTweenExt_LeanMoveLocal_mFDF99A832F9B857E1B16A2E744F9F383C1F90375,
	LeanTweenExt_LeanMoveLocal_mA80CD3FC03494A1744FB551747A732908D9B2D05,
	LeanTweenExt_LeanMoveLocalX_m5C894E1B3E98732358670D3837F1F8D3C4867AB6,
	LeanTweenExt_LeanMoveLocalY_mC4D2C4DFD2A41D0988F10D78AB782834A2D7AEA3,
	LeanTweenExt_LeanMoveLocalZ_m2523332A42D3C35D0EB0983B50290B260BE971D0,
	LeanTweenExt_LeanMoveLocalX_m8AF36480056D23742446C3D011AB3462FF6E4DE7,
	LeanTweenExt_LeanMoveLocalY_m402602941FDB4A2F4E4F52723678149A63D28742,
	LeanTweenExt_LeanMoveLocalZ_m277E9019194B31C9D2F5FACD5CA5BDB78D14031F,
	LeanTweenExt_LeanMoveSpline_m1146A15795C039B9B2B03ED4C80AD8D232A3C4F9,
	LeanTweenExt_LeanMoveSpline_m0C9AE2BAD998E3BC3CD0C3F1671F47B2F769D955,
	LeanTweenExt_LeanMoveSpline_m65F76F58B8003E56FCB6C2EDF7044D467CE61D4C,
	LeanTweenExt_LeanMoveSpline_m54CFB8DA58EB1DC4557E6456579F9D3DA7F28C8A,
	LeanTweenExt_LeanMoveSplineLocal_mA26C4FC628C8DBE7393FCEFAEABDDE769F3BA36A,
	LeanTweenExt_LeanMoveSplineLocal_m1E899C48EA516A34C9929D7FDF44EF2632AA0D1E,
	LeanTweenExt_LeanMoveX_m82773527741CA6AE6E241C12A1D8634788BEEB4A,
	LeanTweenExt_LeanMoveX_m96FB87B93DDACF291202E912E981B3760B44215D,
	LeanTweenExt_LeanMoveX_m3E49D1A15D798CF0DEE8FB9A1D7F2932D636371D,
	LeanTweenExt_LeanMoveY_m0372D8F81E13E6B32BCBA2DBE800598DCA71866B,
	LeanTweenExt_LeanMoveY_m821E5633762A108AF0D80528ED060EB0E62DA9E5,
	LeanTweenExt_LeanMoveY_m1AA13E442CA147D6B062BE8424D669317537F6F5,
	LeanTweenExt_LeanMoveZ_m9939145DDF1A83F4D8547D53A4538912466C008B,
	LeanTweenExt_LeanMoveZ_m79A272207A6FBEA47E355DE22A106896A0941ECC,
	LeanTweenExt_LeanMoveZ_m94F3AB61DB4EBFE4F39B405E5EFE8CD3EC9F2A36,
	LeanTweenExt_LeanPause_m46A64B42348CB04917830211A8C40548EED616E6,
	LeanTweenExt_LeanPlay_m635E84DBAF3658181BD11102F39B17126695612C,
	LeanTweenExt_LeanResume_m3499A30358E3318BB0F73B0B1A07AC9C00E463A4,
	LeanTweenExt_LeanRotate_m3663A0A9B43101BE83E0113F198EDE1DC7A61A4E,
	LeanTweenExt_LeanRotate_m0050CFBB76AE4B345C300EE00F8F13584DAF39A9,
	LeanTweenExt_LeanRotate_m8B46191A435295E93DB9D3F3F982A468D020E54C,
	LeanTweenExt_LeanRotateAround_m94E627C88BD4C138F9FD7ADE41FA86F614D4F26B,
	LeanTweenExt_LeanRotateAround_m8D2DDA35FF67D85798119A863ED498C97980575A,
	LeanTweenExt_LeanRotateAround_m1C2F12BDAD3FB54F4DB76AD02D759ECBCD15289E,
	LeanTweenExt_LeanRotateAroundLocal_m6939D0BB618449C6DB007BD42F9A4C898BE97618,
	LeanTweenExt_LeanRotateAroundLocal_m3977508F1548419CF11A64412C95D747F1C5BE71,
	LeanTweenExt_LeanRotateAroundLocal_m83B8A190A95128CD31886544515865EE7553C0E0,
	LeanTweenExt_LeanRotateX_m1945A6153A434154D6ADB55FDDECAC9CCC46BDB7,
	LeanTweenExt_LeanRotateX_mD17F3CCE10C947CB82E9EC222B0810EEA727FCFA,
	LeanTweenExt_LeanRotateY_mA27D8888248D81C3B8EB606E0E5DBD47716C3A7A,
	LeanTweenExt_LeanRotateY_m9984A206D938B5D53BAC015F467C919DA595BEDA,
	LeanTweenExt_LeanRotateZ_mA27C015E2FEA84D212B0C3D03D6E511035E133CB,
	LeanTweenExt_LeanRotateZ_m8CAA7A563407C9E73FA9B514AC08A9B2C0225A97,
	LeanTweenExt_LeanScale_mB0084B9F5FC69561F247954CBED4BA70511D1F96,
	LeanTweenExt_LeanScale_mAE75E6B1234A072C02D7354461EAA9EB3D485E76,
	LeanTweenExt_LeanScale_m4ED118613DFC434764355605FD977578EC755910,
	LeanTweenExt_LeanScaleX_mB4F38759A8A24E581E837A8A696D9AB275172103,
	LeanTweenExt_LeanScaleX_mB89FF0BBCE3BE85F916A6A7B1D8E6AE9E9E88A7A,
	LeanTweenExt_LeanScaleY_m64BF74A2E7E6B81DA3DA69F7F29AD5EB6DC894C4,
	LeanTweenExt_LeanScaleY_mBCBD993A50FC95AF46ADD7992EB634D05875AF55,
	LeanTweenExt_LeanScaleZ_m42B1B4A160D482743F667A0E398885BB3C976787,
	LeanTweenExt_LeanScaleZ_m821101BF3F56706E65FE11C856D03C1F4C02393E,
	LeanTweenExt_LeanSize_m99C3564D6FB42626360A9BB25BD965F40F61B376,
	LeanTweenExt_LeanValue_m0ABC90AD97B6278EBAE75887B295D8EB1A4CA6E1,
	LeanTweenExt_LeanValue_m6192DF3CB7E8ABD9827718DF1C26F4892DCCA3FA,
	LeanTweenExt_LeanValue_mC3261ED9FF392F50DD1713982ADD612BBAD5266E,
	LeanTweenExt_LeanValue_m5757F21E978B62AF23CC5E142AC222BB4A081784,
	LeanTweenExt_LeanValue_m49CCF82C65BDB92A503F5C61B0EA0121BEEA646B,
	LeanTweenExt_LeanValue_mD59F8CFB5C81AF1061A856CDB0F42244708BC1D6,
	LeanTweenExt_LeanValue_mB5C28F3E39E0871176731D189EF33CE24E7A6DD3,
	LeanTweenExt_LeanValue_m6062D935C55251AC83C7D7D71706FBB350CD375B,
	LeanTweenExt_LeanValue_m7F3A44B64D4EB8824AF830E9BCEDA6B5463099AB,
	LeanTweenExt_LeanValue_m0663B7A32820E3685F4447EFC55951C507170CF5,
	LeanTweenExt_LeanSetPosX_mB614BC76AA5B0934B2F156A9A781DE7DB2A9AEB2,
	LeanTweenExt_LeanSetPosY_m8935780D650702F87FB47C62CB3BA667F4153BAE,
	LeanTweenExt_LeanSetPosZ_m2D663CFB259143ECA22EEF0957396362A0648A7F,
	LeanTweenExt_LeanSetLocalPosX_mC5B1B50F60706CF6FF542A63287C1B4DB534691E,
	LeanTweenExt_LeanSetLocalPosY_m9D85F225670E5804C0E9E944161F520536DD81C8,
	LeanTweenExt_LeanSetLocalPosZ_mDEF497EF11DC45905E787A89A835EDC0BB24CE40,
	LeanTweenExt_LeanColor_m3C101DA3D605DD4228FD3E6BD67DA7B2C6D61438,
	CFX2_Demo_OnMouseDown_mD6B1074EECF2FCF1D56D48B5686D16D579F081DB,
	CFX2_Demo_spawnParticle_m454C46458ED3CAD9F0766C01CD4374B09158C1CF,
	CFX2_Demo_OnGUI_m198082F45AB35B29FFC863367860B4FC1236EC2B,
	CFX2_Demo_RandomSpawnsCoroutine_mCEC8290770FB2A6AF7C2AE9A971516C4F2B4F38E,
	CFX2_Demo_Update_mFD7432FC01D62E3CBBB7B9CF2C43CD43D74C1B12,
	CFX2_Demo_prevParticle_m8954469C9EBCE0E55269709D5C555F46933EB57C,
	CFX2_Demo_nextParticle_m79B7E31DF6A58936EEA308DB28F799E9915B8ADA,
	CFX2_Demo__ctor_m934DD748B71A67C218215894636FF8810C04FDFE,
	CFX_Demo_RandomDir_Awake_m28D19806B87ACE5DAA109ACFC0BE30A7C29D32AF,
	CFX_Demo_RandomDir__ctor_m68DE80058501E3F4557491BA0511288EA4365931,
	CFX_Demo_RotateCamera_Update_m63467A79E286EFE052F430CBC2C92D97C49B5EA4,
	CFX_Demo_RotateCamera__ctor_m31569C8D4A062669CAC67506ABB7128ED09D03D2,
	CFX_Demo_RotateCamera__cctor_m141862E78433EA4544705007E8C1329550E8982B,
	CFX2_AutoRotate_Start_mDD7E24968E8CCF6D1B3F4AAAD677A1A022BF3F14,
	CFX2_AutoRotate_Update_m9FAD02A93B133E508FAFA70109F789E9CF9D5A52,
	CFX2_AutoRotate__ctor_mCE858ED05692AF1622B501C5DC9D0AD3D3CD58DF,
	CFX_AutoDestructShuriken_OnEnable_m820E068B952492BE214932B6E6981DC9F4179607,
	CFX_AutoDestructShuriken_CheckIfAlive_m73EC45CBDF4C2556242F3C5D6E34FA7ACDC32885,
	CFX_AutoDestructShuriken__ctor_mAE78A3C4A17595164EFAE7BB23A5B0D7B12A4DDA,
	CFX_AutodestructWhenNoChildren_Update_mB48205AD814F8EB7CA47BE78C274EB7EF1ABED9C,
	CFX_AutodestructWhenNoChildren__ctor_m9B85A5FB0FE2C28EF290D11D7ECD3608153F28A7,
	CFX_LightIntensityFade_Start_m24228E934E3D33407ECAAE11D847B8D039EDB8AE,
	CFX_LightIntensityFade_OnEnable_m0790466897F96BE7FE251C42266202EF212BB5FB,
	CFX_LightIntensityFade_Update_m7A8519E7F13A95015FFDF3286E4D36187439F4FE,
	CFX_LightIntensityFade__ctor_m21702BB3DE6D56EA026FAF1EB52C373CCFCF23A8,
	CFX_ShurikenThreadFix_Awake_mF7F575D5E2C6A9F52F3AC2F692B28CF9659CFEC5,
	CFX_ShurikenThreadFix_WaitFrame_mA41CF60521F45DFF968C6C932823B9D45579E015,
	CFX_ShurikenThreadFix__ctor_mF832E2EFBFFB98EED172E417BCADC6D7E1C38BA1,
	CFX_SpawnSystem_GetNextObject_m7E6CD3C70DE44AF7BFC8CC5A66500C55C27F9CB7,
	CFX_SpawnSystem_PreloadObject_m428725AFB10A106AE042C732963EDB9EB63F6222,
	CFX_SpawnSystem_UnloadObjects_m252B7E272A76A61F7EA0BA567681061B0F77ECC6,
	CFX_SpawnSystem_get_AllObjectsLoaded_mBC6802FE5B19C419CEE20B3B5B68FC55D975EB0B,
	CFX_SpawnSystem_addObjectToPool_mC6F9F7883E30FEFF6429322325117BDF4353DF45,
	CFX_SpawnSystem_removeObjectsFromPool_mB40EFBC56E00F16B44A3760BFDCC017807478299,
	CFX_SpawnSystem_Awake_m1440873E859D18C88AC10A1342B5F152A6D8913C,
	CFX_SpawnSystem_Start_m3F2BBCD24AEAD78957401BF7E7A89D3AFC80563A,
	CFX_SpawnSystem__ctor_mC9A1988CE77CC7860268B84CF1F5B62A09A613F5,
	MainScript_LogMessage_mE71A5E26022CB5670420942BB736618CF85C3F0B,
	MainScript_Get_m40F54A362B3BD2AF9D9D4C11FC793BFFCBC0D829,
	MainScript_Post_m5CBD6BB2A8AD379C6E839DC31AB4BC02696833AF,
	MainScript_Put_m81EEF338104C4BF925D074BEFA99AED7169D1E92,
	MainScript_Delete_m61C09220BC359F4A722F86E1861CF43B09AFE389,
	MainScript_AbortRequest_mFD9F086798CC3C77B99B7E539F42555587358445,
	MainScript_DownloadFile_mD1AC16E006883F0859EEA1191E5441C194257181,
	MainScript__ctor_m0CE0E3FC499A77C04A65041CF01C7FF3B077EB1F,
	MainScript_U3CPostU3Eb__4_0_m18C7C32083A6F5DF7D5C0662330CCD6E30FB2F41,
	MainScript_U3CPostU3Eb__4_1_mB08CD54A3F07695236A44128B16030A4D601AD2E,
	MainScript_U3CPutU3Eb__5_0_mAA12574E51D788B962126A2E3985E2C0E30E418F,
	MainScript_U3CDeleteU3Eb__6_0_m7184949EA26F4C9A4F50E9D9ECBA3F6E1ECD92F5,
	MainScript_U3CDownloadFileU3Eb__8_0_m5C29038407FD94F4368ABE37CE65EC3338913300,
	MainScript_U3CDownloadFileU3Eb__8_1_m5A1A1F5375A3DE53A686B7F4240D07DB5EC2E81F,
	AppReg_ChangeBaseURLAPI_mFBC2C33065F8228F189CABD02FDE2AC64070DA7E,
	AppReg_MixCategories_mA238CA4A8159A1C94BD0DAFFDF7F8125A3349B62,
	AppReg_GetBU_mE3223374432E1D99C5E3C31EA6AA3BCF889EE566,
	AppReg_GetCat_m7524AD82BFC2B054F34E25AAA640AB953F9680F6,
	AppReg__ctor_mA2EAEB436A78BCB729C34BD4723730C2B1EF81F2,
	AppReg__cctor_m3C8ECFAFC08C6652A01CAB185C6EEAE583C04083,
	CanvasConfig_Awake_m518A2EDB4E9B128176B7833EFD34415532C030E6,
	CanvasConfig_ChineseSetting_m544C33A7E68EDF4E28B1331F3C4FF30C354F7868,
	CanvasConfig_ObjTapOnCanvas_m2E093D60BA786014047E6774CBEF939A5ACFBE61,
	CanvasConfig_DetectHowManyTouchOnCanvas_mF2D1DE869C35437C68282212F531B4EA146BE68B,
	CanvasConfig_SetSizePerCanvas_mE5543AC225B917E0456544E2B202CC50929636AC,
	CanvasConfig_SetDistance_m4216BF0760941D9EA7895EF37E2923445D7A2DE4,
	CanvasConfig_DownPoint_m41458F65287CFE57CF03F4B6DB9F7EDC860FD478,
	CanvasConfig_CheckDownPoint_m21805350BD31467074CF18307D7C2FBD2992085A,
	CanvasConfig_CheckUpPoint_m0967FBDF75CC654F7D8E87AA9D71E407B4AE612E,
	CanvasConfig_GetParentItem_mB4DB5ADFF95AA6140F398569D642A341EC19BCCB,
	CanvasConfig_TeaserOpenUp_mFA2139579332FB9D781F8D010116EBA284C42082,
	CanvasConfig_TeaserCloseDown_mE6AAE2F349F676CD9FA502A1A59209B2471ADAD6,
	CanvasConfig_SetTouchToDiscover_m0FA62590AA6512DDE61D84F1CCE60F1528F86E5F,
	CanvasConfig_CountDown_m698E971A80DB00291215897804F2C3F58FB18B7F,
	CanvasConfig_StartCountDown_mE795ECE7B990D91CAE263E48BD93785A42456A72,
	CanvasConfig_IdleTimer_m1BE0C46E819E57D213D91C3874997EF3BC7DC446,
	CanvasConfig_StartIdlingTimer_mFB5A4E9FE2F901B4307DD42722E03B1E3C827DD2,
	CanvasConfig_SetCanvasSize_m08F400E9D37D9075EED9382F5D26146A322D3EC3,
	CanvasConfig_MidPoint_mCD2AF0C6398F8BBFB81C8F23F6CFB4399390DDFC,
	CanvasConfig_SetTapEffect_m999990D08E6F7AC9FF8D7114916985029C958853,
	CanvasConfig_TapEffect_m30EDF52DC0760413A84A5377D22EC8743604C98E,
	CanvasConfig_CheckFreezeImages_m5D4E655E024842C42DE27BFCC74EC164667C2A16,
	CanvasConfig_CancelCheckFreeze_m3E1C8CF54D02DDD0D34E04F63C04FBF233D63023,
	CanvasConfig_ReturnCanvasPos_mB86C00A243846704AD8ECF47E730F8B23AD9A495,
	CanvasConfig_ReturnCanvasPos_M_m40ECECBDC474F1FFA2468E0DA12B7810D4897C1F,
	CanvasConfig_DeactivateColliderWhileSPV_m07CC0C189A02964441382573EE440EB5F1621F2E,
	CanvasConfig__ctor_m09F97D333BF67726723D4DE6FBBEC4D348C83443,
	CodeTexture__ctor_m2B7B0E61AAA2B356C4B6680DC74C29E73592E650,
	ImageConfig_Awake_m362FAF1147F20671CDD95640EAD1C5B3C39E4D5A,
	ImageConfig_Init_m37607F48D6090A485CD7C4C3B28CE3332CA5BA3D,
	ImageConfig_SpawnAtCol_m6EAD39B7E92CDC8A503BEB5D8555C947399A6F40,
	ImageConfig_FirstSpawn_mA3FD0AFA86A5A29E44FBD17B1D1760F0FD28F226,
	ImageConfig_ReadImageAsync_mAC8269D3D1E9862F4FDEFC1185CE5220F896AE62,
	ImageConfig_FilePath_m5E209C92DF489163A1144F8D1374FA597F56A806,
	ImageConfig_StreamRouter_m34FA4543CCAA0C871AE5551176073A9E42750B1E,
	ImageConfig_LoadTextures_m1407043ECC4B34FC98438757E4DC45DA39590204,
	ImageConfig_AddTexture_m0BA5672931AA5EA738A09E27FD3559F8CCB5D542,
	ImageConfig_SpawnImg_m09D0E48D67B4BCFA01E7B1F4F514423FB72945BD,
	ImageConfig_CheckTexture_mE0F068384BEECB350EE9A5B601370B28FE0AA9A1,
	ImageConfig_SpawnTexHorizontal_m8A14796B140B2BA817AED6EFFC916F132D2E8A3B,
	ImageConfig_SpawnTex_mF66039360B1B8DDBB5E544081AADC3B04EA20ECD,
	ImageConfig_SmartDetect_m756E1E4BB325171598166257879B00B1CAD7BBCC,
	ImageConfig_DownloadNextJson_mFEDE9D52646DF353342B3721CADD704E5CFBA82E,
	ImageConfig_DownloadingJSONDetailProduct_m63F249B140008F9BF48CF00010263650EB21AC29,
	ImageConfig_SetSPVLImage_mEEE82BE5D097A69D3B26B6A1084CFC42827E7ABC,
	ImageConfig_SetingSPVLImage_m5E018814133510EF24A217CEE5DAEF1838758D3B,
	ImageConfig_DownloadSPVLImage_mF707F333E5C609AF435411D68527986CB10B43FD,
	ImageConfig_DownloadingSPVLimage_m1432B819FE8ED1B653EC4AADE5456F011D325D89,
	ImageConfig_ReturnXL_Link_m755799ABC61E0E529BECF9C15B9F8237FC90849F,
	ImageConfig_DownloadSPVSingle_mC9C9CA33C253F29B58A1D4389A38594AF09F8151,
	ImageConfig_DownloadingSPVSingle_mA5C019FB2C58CE816CA786183D8F6A360AC09A32,
	ImageConfig_ChangeSPVImage_mA272A63CEBA876C3E28A79FF6FA798D72D6EDA80,
	ImageConfig_DeChangeSPVImage_mF8F9EF8D14357D4F9DA0DE7D951FE7FA4618644C,
	ImageConfig_ResetSPVImage_mC45BA6695CE543186BB8D34C0EC269A0B0126822,
	ImageConfig_DownloadingJSON_m34188416FD94BC90E1596576BEB4A1C451602870,
	ImageConfig_SetSize_m1ED3DC909622830703E4681ADC9D93629E6B9BF1,
	ImageConfig_WaitLoader_mE90BD0F058DEEA11513950873282809813765016,
	ImageConfig_GetLineSize_mFAE45B88BF681D071D4A9FECC805F780120F20F1,
	ImageConfig_GetMidPoint_mA4F9CA7F7441FB05939FF48CE5269075260E876A,
	ImageConfig_SetPos_m832C8E0D5CF727D39C04E9098B1CFF19D62D363D,
	ImageConfig_SetPosSimilar_mCC1BE89F2FF3D0E9F9867CF1E0E34A7AA0912A95,
	ImageConfig_Finish_m9D41DC2F8F91470219723990DAAD0E22D7498447,
	ImageConfig_CheckActiveRow_m292594F2DAB33DC0874E9C02CE97AFA4FF5657A8,
	ImageConfig_DownloadInBG_mD3E9B2A4F266F0439353A7C2BCF5E0987721FE79,
	ImageConfig_CheckToMove_m0BB58B408B7848539BF532389942E8D9A1EF218F,
	ImageConfig_CheckDetailProduct_mF81E7E1C7E3D7B78741ABAF48CA14B071974E42F,
	ImageConfig_GetTex_m45F9D8DB111D8EC2FD9D2510B8307BA5D94CE1C1,
	ImageConfig_MakeTrilinear_m28D3678A248104090EC84964E5098443AFA21ED5,
	ImageConfig__ctor_m909582D25EAB870E4E80D2530267DD47E72FBF6E,
	ImageMover_Start_m386EC86442B43C5AC0E273B153E27DB9C1658C9D,
	ImageMover_Update_mEFCB9EECF1B31AD1DDD41F744211C154C19AD746,
	ImageMover_MoveDirection_m0F60E4FD7FFA13A9DEA522594DBEF0846A3636B0,
	ImageMover_CekPos_m44D60B7CFD6FB29A4DFCA03F5498E9FD3EA0682D,
	ImageMover__ctor_m63461C877EFA6D15F1D61CAD0B8D9C9CD367377D,
	InputHandler_OnPointerDown_m73A421C5F4898DEB87CE0B7D648C6701870EE0B0,
	InputHandler_OnPointerUp_mEB2ACFD04FC731CC92DFCEDB9AE6FDA4356C16EA,
	InputHandler_OnEndDrag_mCA09C2C53F68323B5C7726C9D2D87067059AC063,
	InputHandler_OnDrag_mEEF399021E12B48F8D90BBCB2044B181805359B0,
	InputHandler__ctor_mC0430AF0D0189A36436A3CA392BBCE44D470AA6A,
	InputManager_Awake_m622BD99CCA0EDF4523438C59FD499A06C0B05F21,
	InputManager_TouchManager_mC0C244B8F117CD60723B806B0371AFFBBFA368EA,
	InputManager_ClickCarasoul_mFC7E29A76B1E5FBD05132F83A328B03F16484D18,
	InputManager_SetMoveDown_m11CC3536C1FFDBDD83CEF63D8BDEA7E5FD01DF41,
	InputManager_SetUserActive_mCC502F375515EEA0E3BC64A02ABBD98C48F200AD,
	InputManager_ScrollingThrough_mC0D2F96AF1EB40D2E77889F287EAE892763116B4,
	InputManager_DetectFingerSwipeHorizontal_m0A81F6002B9885A78E1D83D5A5D1AF960DBF62CD,
	InputManager_DetectFingerOnColumn_m7F95772E03F3E2F690DEBFFE04E6EF0D2DD88FAE,
	InputManager_DetectFingerOnRow_mF1CC2C065D5E9EBEF10673F1DA374139A46B5752,
	InputManager_GetDragDownStatus_mB2BED5FEDFE308B8A5B0D7905DBEED14899C028F,
	InputManager_GetIndex_mA57531DFB4007B694B5D3989F8A5E24628217C42,
	InputManager_InitResponsiveScrolling_m6AC7015F27D86F8FF56667E42C5281CF9E1DCEB2,
	InputManager_EndResponsiveScrolling_m81C6DE9A946B8C723FD5BFEA47E6CD4C6FE76DE4,
	InputManager_ResponsiveScrolling_m81F1BE1B5621CC323D05F47D94816D41840C7634,
	InputManager_CheckSwipeResponsive_m354759FEE70B330FA6A8B5B0E07F49F19E0BE735,
	InputManager_SwipeUpRes_m6C66BB036D76FA3478A27E514C79A96372776677,
	InputManager_SwipeDownRes_m92887E85A0EAE3B9E973A54C3DBD9FDB27B9677A,
	InputManager_SwipeLeftRes_m9CD08E1A3B1A15818914FEF049ADA0C176198AF4,
	InputManager_SwipeRightRes_m23A9D82F458AC23A42476EA3C1FC7AF633D1F536,
	InputManager_checkSwipe_m870FEFB1C9FE48F6A17BB1EAD7A1D8B22A145AB9,
	InputManager_verticalMove_mBD4ED32305C4F87443600DA49EDF9061B602A1C7,
	InputManager_horizontalValMove_mED45F02C9EE2F8ADCCD931B4B2D41A49F9347F11,
	InputManager_OnSwipeUp_m19E8C99969D7E8E5408F1209DCA4125931F8E863,
	InputManager_OnSwipeDown_m563F2D6BB7296D11966D64BFA4AADE69F40D572E,
	InputManager_OnSwipeLeft_m5A307541C07CB2B32E3F573BAB343FA3DD8B261D,
	InputManager_OnSwipeRight_m462C016E2D46BDED0438EDF1C1E2CA0E40C87E69,
	InputManager__ctor_m92E656DB5CA317F9006F3CD8CE443B7D15EE2A47,
	Main_Awake_mF1A75A989CFFED83051CD144FF853DDD08C422C9,
	Main_Start_mF306944EE9F427229AA0BB76EAE0D3CBA61C026C,
	Main_FixedUpdate_m05FEE8510ED94BB359F543D57B04DF161856DFDF,
	Main_SetOrientation_m39E8A202680C07DFF3091CA9407405D7CD3A3A89,
	Main_Update_m43D4349D9BDBB3F441E461282AC09404734F904F,
	Main_ResetSettings_m756F2EBD4CCB8880EB41D70714F7BE6AA83C1EB0,
	Main_InitMain_m3B4EFF7B0F091F065E1BC61EF7808300D700E79E,
	Main_CheckHomeAppliancesLP_mC73A4AA2F3D8B8BEFD01D4C09B79CB3DC14E7632,
	Main_AllCollider_m48ABB727C59E408280AE78DADA4AB047E5F4B41E,
	Main_GetItemFirstPos_mC44AFA137B87716A10AC33565948903FFB0FFAC8,
	Main_SetFirstPos_mEA5A7B3B773DC916431DC333B2281F29CC874EC6,
	Main_ZeroSpeedAtCol_m2DB265EF5A14CCE068FDF11B089FBB41D42E97E8,
	Main_NormalSpeedAtRow_mA0351FCD6E7D091162F10ABBC03AC24227F2383E,
	Main_NormalSpeed_mA4FDBA20A07E4EB41A56DFBFF292F46ABD279A3F,
	Main_SetAllSpeed_m19A3E02AC5D12CCB519B02E4DA00A9B5663EB825,
	Main_GetArrTaps_m9D01873DC80412319BA84C70EE38ECB2C20E3D45,
	Main_SetFirstPos_m3DC89DBF5DD787681A78C2D7ACE7B96ACF4D5FEA,
	Main_GetFirstPos_m50BD0B4B9CA72854A40BFB6B0854FE0C1B9E33B9,
	Main_SetRays_m94AB5CBB0C23339F94726E1F80F9C963AED7D538,
	Main_GetRays_mD945E757D2276ADA78AA66D2624063EC63CD47AA,
	Main_GetTaps_mB83E48E70DF647F0915A082070DE9044AAB28D98,
	Main_SetTaps_mA1CB8CAB381432111C419AA8CFE4980774D9FB9D,
	Main_GetSpeed_mEE9BE437FC4D01F6D1EFB5EC155C429D8936F2C2,
	Main_SetSpeed_m4A45141A3D45FAA4A07EDD9DBCC88047D128E0B3,
	Main_GetFactorialScrolling_mF2F32EA1D77F51EC17B00D9BE9209981236EB467,
	Main_CheckCloseToDestroy_mB5D8D164EA9CE303ED8C60926DD192B81C5FA692,
	Main__ctor_mCCF4399D4778FA3A061E6367C22EF40014C36EC8,
	SPVImages__ctor_mB899A6349B45AFCDDB82D9402629FADA2CE5F782,
	ModalConfig_Awake_m029DA2C6A18E34F371C7ECF0AC8A6FFB873E2F20,
	ModalConfig_ResetIndexTM_m93DF5B981613936A5C416E3EA74A0091CA003BAE,
	ModalConfig_ResetTMPos_m76783FB00146A51AD88CAEA9B395CE76DA5A8E1C,
	ModalConfig_ChangeModalFromCarasouls_m016624A470ABD6E7C37BDA9C5775F29B4AFCA256,
	ModalConfig_SetSideModalImg_m15669B7BFF1F329B7761E136FEAA0BB578387B76,
	ModalConfig_ActivatePinchIcon_mAF7B6DA149091FA262525ECAD31BE7372479D6BD,
	ModalConfig_SetPriceBox_mA596FDDD0ACE63E11DE1071DFD0ACC8BEB77ADF4,
	ModalConfig_SetBrandBoxPosition_mE333106F90E846556C4CF450366C6ADFC3CAC461,
	ModalConfig_SetSPVBG_m1E4EE9F3A376B3E0EB27062C9D7CF0EDA3C061CF,
	ModalConfig_SetBotPagination_mFFCFA5E1DEAB671DCFFE0B4AB8F52E0789DD90BF,
	ModalConfig_SetTopPagination_m2892F518808F17EB18EDCC6E0723A25B14E32606,
	ModalConfig_HideSideTM_m15CE44875FB3B0713758E93FABE6711C45E73D27,
	ModalConfig_ActivateSPV_m5CFFE9654DC58CCE3CBAAA98CC3DD6DA61E104BD,
	ModalConfig_BackModal_mE517F7F36BB62CE7A38A17A73714B4AF633C8FE1,
	ModalConfig_SetBottomModal_m56DC57C700A1AA2B412264E245F83D61D529CC8C,
	ModalConfig_SetBottomText_mBCFF036A788D275F5A825E239FF22F742B0ADEC1,
	ModalConfig_FillTopModalInfo_mBDD5AE241B39ED4C0290FB261C43E899A0F0B5F3,
	ModalConfig_BottomModalSpecialCase_m9E056D0F80133FFA51BAEE8E938DFC76732CF395,
	ModalConfig_AddCollider_m46165DA4B12E431B9CC00D60BFA0FD7FCB985653,
	ModalConfig_DeactivateSPV_mE90A4F79F7B3F6E006A5807AAC79E8AEED8329AD,
	ModalConfig_FlippingStatus_m34A1147EC45BAC9D00A55FFB23587A76C0F1B8CD,
	ModalConfig_MovingModal_m2A38A035C0DAC8580CCF14A99DEEAD88CC0B1EFD,
	ModalConfig_DetectModalOutside_mB396EDD98CD542BC6BBAE6216EDC1EBEE7663308,
	ModalConfig_CheckSPVLimit_m70CDB41405C1C5F8C31C86D1AD0270B17B278548,
	ModalConfig_CheckSPVOutside_mDE1CF5340F6E675AF1436F9A22F3E5E846FA7E0A,
	ModalConfig_SetSPVsImage_mE0A62095C2EDA27A3F25F793A386628BFD76CBF3,
	ModalConfig_SetALLBotAlternative_mCECA586ADD0B8DF389DB932BD6A8D09BD40D92CB,
	ModalConfig_SetALLSPVImage_m52AEE77B76A3FEC0DD26008D5B92B69721959683,
	ModalConfig_SetBoldPagination_m04D3722D50C68B3921E257524FFAF53EE4704B2D,
	ModalConfig_FlipingAnimation_m9AFF285A4C69EB5A177E4C6715C3CC7070C64422,
	ModalConfig_LeftModal_mBDA836C4AAFDD9BFEE1D344E2EDC2AB8559166AD,
	ModalConfig_RightModal_mE3911E0DCDB19879055F01D1E88F4F84FCE96552,
	ModalConfig_CheckExitBtnOnTopBox_mA8124600BE4A9B71F6F22BE00F5CD6E58F3F06F9,
	ModalConfig_MidModal_m69D680968A4AC791DD34F09A59211125BC103550,
	ModalConfig_InstantiateBack_mDCEE85C6C42152E29397A0A1AC33C7F8779B4F3D,
	ModalConfig_InstantiateClose_m2955CA4F3F43CB81E6D994D8397DBCCFD739262C,
	ModalConfig_GetTexture2D_mBC8B9195F009705615F91C0E714485EB1646F24A,
	ModalConfig_AddTexture2dSPV_m1CE8BB496F4D9A94EBBA64F542A694171D28FDB1,
	ModalConfig_AddBigSPV_mC3A1D2B52DC056D18E7E4203ABEC96DA2A7788F9,
	ModalConfig_GetBigSize_m6D6B4ECF4D767EA9CEC235EC849B0D084FE08941,
	ModalConfig_GetBigSizeSPV_m7486D02AD01799576244985506ADC5DF25654CD1,
	ModalConfig_GetTexture2DSPV_m2FBCB1BFCB9D0DB202F6BE0BC5B78DC83CD8F3C8,
	ModalConfig_GetBotModal_m3FE90D15FB08D60134E0060A49A14BA1BC22A53D,
	ModalConfig_ResetScollBarPosition_m527D23CA5DE6E6AF26F1DAFD9590238103BA4AFF,
	ModalConfig_GetTopModal_m6D82D8F3FFC9B723B78CCCB59B1080D98BC08AF4,
	ModalConfig_SetImageSizeTM_mBD4B7D7D30C2DB38C6F30674979987C8C543DC28,
	ModalConfig_SetImageSizeBig_mFD571B30110A894D49B8FB63741FBDB3CEACB06E,
	ModalConfig__ctor_m3EF58744BB846652CBCCA690F449A950FA49B686,
	ModalConfig__cctor_m551684EE6D1D8247F9AF65E9AF2B860E87918F1C,
	MouseManager_Awake_mD369B4BF2D650F29418D181F951A94121D93657E,
	MouseManager_Start_m99014BB61EFA2CACB12CA5545C1C5769BFA1F277,
	MouseManager__MouseManager_mD63E6C8E3316C4C39335D02BBEDD6CB87986890F,
	MouseManager_ClickCarasoul_m6C1CD2130E0B8341609140369B9AFEEB0992B1C9,
	MouseManager_SwipeBotAlternative_m88340B722DB13A3D9D3730627EE0A0EEA55A53C4,
	MouseManager_SwipeTopAlternative_mC2458054E15FDF756CCB53759BD29409B86BE458,
	MouseManager_ScrollBarStatus_m48BC0F6144391CE693E79651309C9D62456B6255,
	MouseManager_GetLastChild_m75902EF8B006C4E24975C6C33766001E67A22D1F,
	MouseManager_ScrollingDescText_m56D390C79491AAC678E4613B22E6318E80341ADD,
	MouseManager_ScrollingThroughMouse_m8C9D38E0910EA56A6AB4A595E1B71FA1529DAA86,
	MouseManager_ButtonClose_m759EAF36C6E6105F986A3E7DB327DDF2888B6FF5,
	MouseManager_GetIndex_mA68470DCB350C2DD71E599F0660FBCB9CE746149,
	MouseManager_EndResponsiveScrolling_m4C70A8DEFEF0181E1A54326C8BDB9CEDA27EBB85,
	MouseManager_InitResponsiveScrolling_m536D3CCAE6B28EC7AD2D3C24A2E0014E22678A19,
	MouseManager_ResponsiveScrolling_m7B05D7EF52E71F4979A18814FDC954E512A04A6E,
	MouseManager_CheckSwipeResponsive_m46F795C54AB1C5305F57F768BBA60F1B0D34E5F2,
	MouseManager_SwipeUpRes_m198F9EF5195AB900BF42CB9CAC275EAF0C046313,
	MouseManager_SwipeDownRes_m8B849E0188946011BEFE9486BD32B211B5853E9D,
	MouseManager_SwipeLeftRes_mDAA31E4973910860A6484556D13F2200A8FC5866,
	MouseManager_SwipeRightRes_m273F096944E51088675512D2378629A1C095E165,
	MouseManager_checkSwipe_m47973D210553A22241A8A8AEE30566DABDEBF7B1,
	MouseManager_verticalMove_m87C1FFA56173D5E757BCBC5D68C5C411E1090DB9,
	MouseManager_horizontalValMove_m2AEB31DF7A62D19DE709389666A827DA39FD67B5,
	MouseManager_OnSwipeUp_m2F681729CBD59DBF657CCADB146631A29FADE129,
	MouseManager_OnSwipeDown_m0B1D400E8B0D1124E253E72604E38137306DFCB1,
	MouseManager_OnSwipeLeft_mDAB724D596310948E868B054DDF013D130D6FBEE,
	MouseManager_OnSwipeRight_m0B0F54D10186030BF5A07E15F2F7A53BA05AD304,
	MouseManager__ctor_mF030DEE44FED499E3F86AE86DDCC0D1729084F28,
	MouseManager__cctor_m5B805DE586BFCA851E46F1CB5896702E96846158,
	Product_Registry__ctor_m94B94BDFDFAD40552DF6E178DB14A045FE43C905,
	ProductClass_get_Index_m2901BF7277598F2E7019070B169454918F9BF980,
	ProductClass_get_Link_m25DC63D18C6E6FBE4722AF4F85A4A49A68D4DA18,
	ProductClass__ctor_m7D8D4B89873EED9188357090CF39D85820EB3073,
	SPVDetailProduct_OnEnable_m7ECE051EDE61C92CCF40B870A560F08EDA1CEF06,
	SPVDetailProduct_RetryDP_m4A4562DF063B5D36138324A0117BB02F1829F799,
	SPVDetailProduct_OnDisable_m442F60A9818A0B5A13473DA90C41CCA0EE478652,
	SPVDetailProduct__ctor_m0BC08056B5B0C0AE48A62447F98F4C395444D9C9,
	Scroll_Update_Awake_m9EB9A06BDC2B7A27D79507E048AB18B259E9892B,
	Scroll_Update_Update_m2B8378E682FEE6C37BCDE15389A6CE3E3FA1A100,
	Scroll_Update_SetDelayNSpeed_m52B8346CC28F66EA0A4311A98F2611864D9488BF,
	Scroll_Update_ScrollHubber_m5250BEBA7DC2C86943F932B0DA0B93308EB0CA15,
	Scroll_Update_ScrollRow1Col1_m08ED0EC932E873728EC35BA4C6DDE48F228731AF,
	Scroll_Update_ScrollRow2Col1_m2D4C7697F164AA74A22542FA0F2107A5DBA63A88,
	Scroll_Update_ScrollRow3Col1_m3BDD4BDC8415F9EE7811C6D0453A6B3D9CB561F8,
	Scroll_Update_ScrollRow4Col1_m748897E1C8E472E3A8950C7F526019C74634C8E2,
	Scroll_Update_ScrollRow5Col1_m00B654B87CE11831A2BD6AB115F609F927E4867D,
	Scroll_Update_ScrollRow6Col1_mF794BEA144324DA4747C596561228C68FC5581C9,
	Scroll_Update_ScrollRow7Col1_m3003355E4535E6B718DA852DB863F0006ABE1836,
	Scroll_Update_ScrollRow1Col2_mA0D91BBEE1B241D2171B981914CFB7D8539548A6,
	Scroll_Update_ScrollRow2Col2_m78D61AD64ACF1A7076CA26B03EBD300F7A430A16,
	Scroll_Update_ScrollRow3Col2_m50F796928F7871611C14C07E9077A3064F4C6BD0,
	Scroll_Update_ScrollRow4Col2_mDCECF2B64D353B36DF14F3600B3018F638219DF5,
	Scroll_Update_ScrollRow5Col2_mA42B2FCA92F4E43BDF526DDB4F072A1D785F9FAF,
	Scroll_Update_ScrollRow6Col2_mAFB23F505763D76E153832A83DFC912364DDC847,
	Scroll_Update_ScrollRow7Col2_m38B51E814628E90C01BE3E38C88437EBE43055BF,
	Scroll_Update_ScrollRow1Col3_mF7CECC18007361C04BCD99D3DE85350E694E9BCD,
	Scroll_Update_ScrollRow2Col3_mDFB0C16530BA8E55CFE1DA770D8629DA6756F5D6,
	Scroll_Update_ScrollRow3Col3_mCFC6879F603EDC518940498E2B43C3984F2A9C6E,
	Scroll_Update_ScrollRow4Col3_mD4FE634FA0A69DFAAAC3E0E0A8557B2C707181F8,
	Scroll_Update_ScrollRow5Col3_mC09EB6CE228CBCD3333E432A34658CC8ADA5AA83,
	Scroll_Update_ScrollRow6Col3_m7A7FFE8FA642F8E226255EF4428B64A03EDE3DF4,
	Scroll_Update_ScrollRow7Col3_mF8CB04A184F07233D0F188886124CAFB6304FED0,
	Scroll_Update__ctor_mEE53421C7561104C94AC86379B8C6BC1199FE99C,
	ErrorItems__ctor_mE2C03654FCC4F21BBD89BC776341BFD19356C7AD,
	SimilarItems_Awake_mDEB4D3F8C2F37F959EBB504AA7B9E6A531C7D86F,
	SimilarItems_DownloadAPIStyleIT_mED8B878E31CD172F613A5363CA5B552C323A39B4,
	SimilarItems_GetAPIStyleIt_m8B0FD86780866A301F0395D5A99971C7A2D6D68E,
	SimilarItems_DownloadStyleIt_mD6C3FB558985ED5F12B9195404FF945E249BB4D1,
	SimilarItems_DownloadingStyleIt2_mDC3B8536F4F55DB1DCB4051313A59CBEB6942A0D,
	SimilarItems_SetStyleWithItLength_m2A95CB0198330C91112B8BBA170EC663C8D0627D,
	SimilarItems_SetStyleToProduct_m0EF8DBE1C51FD594D0C2A3FF9AC6B073D801A096,
	SimilarItems_RePositionCarasoul_m865595E2766EB58059467A10DA9A3D03B7386ED5,
	SimilarItems_DownloadDetailCarasoul_m10B59A7A5A27703D7F1EF544F471201169194BFA,
	SimilarItems_SubtlyRain_m153D4ED3FA81F3C95CDBB58FB511279FA6F69DBF,
	SimilarItems_DownloadSubtly_mD7DB25BE800A386878C5FFC71B3058511350D644,
	SimilarItems_GetTexForGOFwd_m776D1D8443009F55089861F4F2C5AD5688F2183A,
	SimilarItems_GetTexForGOBACK_m3244C0DAEFDCA7ED645049EC894DF75BA26C89FB,
	SimilarItems_SetLPAndTex_mDA2B79B374839CE33EBCBA3D9A613E54A7241210,
	SimilarItems_GetDPSimilarAndContinue_mA0FA76F0563B601C765589221979216844D3C37A,
	SimilarItems__GetDPSimilarAndContinue_m644F27F810A6CA9BB7E15252748E01344E7C12E8,
	SimilarItems_DPFailed_mE910CB1DD5961E6E1621027D4E4CA33BF16FCCC6,
	SimilarItems_Downloading5Img_m40BE7F13279E89768D5274A4C1922A3267003ABD,
	SimilarItems_Download5FirstImg_m4D21C294228C217EE3282E844569C7CC537AB7D3,
	SimilarItems_DownloadImgSubtly_m4B717DF8D578CABD2DEDEE4D0F9EE540B9C3690A,
	SimilarItems_GetStyleITSimilar_mB6F9EAA80769261BA2FB537EC20DDD6D361BF93B,
	SimilarItems_DownloadingImgSubtly_m54FB937E24BD69E1840AFAC871934A0B1F3A59A8,
	SimilarItems_SetSimilarProductsArr_m366A8505D019F52EB6AA3911928AB09395956DA9,
	SimilarItems_SetSimilarWithOutGoBack_m3ED4EAEFB54FE7663A41617D6538CC19D5EA52A9,
	SimilarItems__GetLPGOBack_mD748407FCA2CFF8E2725AEDFA2D298B6B7664F48,
	SimilarItems_GetLPGoback_mAC62743925F3F87E9BC80CD601B75A650F09205F,
	SimilarItems_ClearScrollBack_m9441EBB15ABE2C14620A7D8843D08D8DF41FB7E0,
	SimilarItems_ClearLPSimilar_mA55445263C65D99C58639E640FB3902CC94EC5E9,
	SimilarItems_GetSimilarProducts_m466F87B10FA5CF91F45E4E65076C8485A6D03385,
	SimilarItems_GetLPScrollBack_m1444F3E3C095BE342B01E143CB902F84CFD7A6B9,
	SimilarItems_GetLPScrollIndex_mCBDC1E1B5B9EB1ECFC7DB52D0DD1444414EA4408,
	SimilarItems_AddET_m8AE7ACC21B1602B077F8FA6D1A7A0DE911401112,
	SimilarItems_AddErrorItems_m90003561AC865E97DF577B6EFCF9932A542716B5,
	SimilarItems_GetErrorItem_m44C1DD59E1D6AB696C7B0C0F654DB33AFB0CADB7,
	SimilarItems_AddTextureSimilar_m145CEDB83C33DEE6FD2C1B5151AAA5E8FB9B4C3D,
	SimilarItems_AddTextureUnSimilar_m735FDADC0D662CA1FFD28A5C32DDD41F8581673E,
	SimilarItems_AddSimilarCT_m7BE7A44EF01FD092CA787993B6660F34AA744ED8,
	SimilarItems_AddUnsimilarCT_m2C36CBA3E804C8975A56682EF5D82DC1DAA36161,
	SimilarItems_GetUnSimilarCT_m60FCDB7E96B087FB8D92A77EA60EE8D928DE9590,
	SimilarItems_GetSimilarCT_m4FAEC55474747A0240C9591CE3AD11A008A081DE,
	SimilarItems_ResetSimilarCT_mA58BDD28344BF14DAD01403AE1202C8219242578,
	SimilarItems_GetSimilarCTs_mBDEDF69EBCB47E75D61239D384DA8B9BA0981EEC,
	SimilarItems_GetUnSimilarCTs_mA2103A5DE8184E837471443366065423F09207A9,
	SimilarItems__ctor_mD464FD848F21046A20A37EE79E084AC4C53FF795,
	DownloadGOBackKeperluan__ctor_mD9922444CD25729D36F22B7C6E09C8BAA2A66449,
	StartOver_Awake_mD22540DBEDEEC58C9286DA8670A94C610F464C33,
	StartOver_CancelMaskTimer_mB79FD78D70BF4483C094875081BBCBC70D1C29FE,
	StartOver_MaskCanvasTimer_m2ED222196AD29522274D3D50AD46F04F6EB176DF,
	StartOver_StartCountingDownIdleSPV_m5C5688CD93E4BA8CD4898DD4C7D232D0187BC162,
	StartOver_MidPoint_mC3F810B6A1B978879A84AD3AD51AC8F7673A0DDB,
	StartOver_CancelngCDSPV_mCA8A90344376DAFFDC65610B9F4E5F121B6ADD82,
	StartOver_CoolingDownAfterSPV_m1EEBA8276382F7C00D7CF901B5CB9A27CBD2387D,
	StartOver_GoBackObj_m488AD3425CB61BE15FAB3620505D3CA86DD23E4E,
	StartOver_SetSPVPosIM_mB86B1D5FBB379681190B7DAF84139CE4A589F679,
	StartOver_Swiper_mC7FAE7BAFDE8CF02C19DE72F113880EEDC193FE5,
	StartOver_GoToDefaultState_mA041F9633FA88C3E498EEE74BD486DC68790CEDB,
	StartOver_SpawnGoBackItems_m62BC373D30779AFCEC9371C49BE40639C9CF44D5,
	StartOver_DownloadImgGoBack_mDDE3FC3AFDAC65B86742459FFB95E4CDA3C7EE60,
	StartOver_DownloadDPGoBack_m16E5F9EEAF08EECA61CACA53387D40D931DA7CA4,
	StartOver_SpawnTex_mA76331A2DB25654E07DF255CF59AF3F82B049032,
	StartOver_CheckSpawnHorizontal_m596B906B33EB4A43E0612294655EA6AB89784D5A,
	StartOver_FluidAwayRow_m8FEF81B7668EF7B6234E61174B94763C796A2367,
	StartOver_ComebackFromFluid_m497282C86C7074B14BBBB3044EAD1DCFB6E19B7F,
	StartOver_MoveAwayRow_mA93F9D9C4B83233320EE136564DDF62C11989F73,
	StartOver_ComebackRow_m2FDED8E08B6D9420D65CFBCF10BAB8AA7D2C86FE,
	StartOver_CollingDownOnCol01_m41857C3145500B0FB0C5B0B61FAD360157FB8143,
	StartOver_CollingDownOnCol11_m7F374115BA28D87FD2EE7C64115B22CDEE32CB58,
	StartOver_CollingDownOnCol21_mCC04531946A3810253C803325566B3E7403354EA,
	StartOver_CollingDownOnCol31_m983BFA9BAD23B851CFED235F179F6815D963D00E,
	StartOver_CollingDownOnCol41_mFA7BF63DCA4890CD8E7B3C812085748EAF98C140,
	StartOver_CollingDownOnCol02_mAA707305328A77E3EA0E6719063B2FF5C9B667BF,
	StartOver_CollingDownOnCol12_m713ED93F2738C444B022BE9D589D3E018C2CCFBF,
	StartOver_CollingDownOnCol22_m9689A47781A31074AB52BEE84A195012C7449EF6,
	StartOver_CollingDownOnCol32_m3D04B89E1092486E8723471EA543C739CC0C2F84,
	StartOver_CollingDownOnCol42_mE2D9D66FF580CD098F1539653DCBBFEE2494C6C6,
	StartOver_CollingDownOnCol03_mFF072B1CC85947FCE9689EC762DA547C60D51D00,
	StartOver_CollingDownOnCol13_m28A5F1F852FAC1AE6B95F7B6AB58F60BF4852C75,
	StartOver_CollingDownOnCol23_m77B8030F429DDC5802718D252374A66FB28A30F8,
	StartOver_CollingDownOnCol33_mC3808A1D099D01225E42AE9B3EFCC4DF56EC2496,
	StartOver_CollingDownOnCol43_m3C3D9D367E6CC9156999969F9496612649463709,
	StartOver__ctor_m6E7E8728BE662BF3B97B10A918AE8B2229F15F87,
	QueueRenderTexture__ctor_m1A92F453D94863226640588129902BCC795E3DED,
	QueueRenderTexture__cctor_m1775B3861B7FBDDF7608F64B5E322FF138AF1B5A,
	StaticStorage_GetDP_m344791E9D9B120D46F73FF96097810233CAF1EAB,
	StaticStorage_GetLPs_m421F3B0061D5580C7E04FAB51BC9122C0A6AE0AA,
	StaticStorage_GetLP_mC83A1BA9762979C0D8AD252A1F2BDBD37BEF10E4,
	StaticStorage_GetPosString_mD96C829C4C2258A6B43BEE3B10934C0A81A3CEB3,
	StaticStorage_GetLPosString_m8B2E834577A3D58AD92E326066625882552ACAC1,
	StaticStorage_ShuffleLPArr_m2DF0F49B95CD122DB93937E442A9A7866F7F2893,
	StaticStorage_SortingLPArr_mAC511FBA78A1C037263325EAC5FAB5AA73F19A1D,
	StaticStorage_GetDetailProduct_mB1603A9756BF0128D4303AFF08277F003657EF9F,
	StaticStorage_GetStyleIt_mF63887CB49B7CAB400D4FC31427460B49B87C66A,
	StaticStorage_SetFPS_m58826B2F44BD0A51C59939B4121A6900EFFE8EE3,
	StaticStorage_ResetStaticStorage_m3CBB7105F1952826ED5B9523BB22F2112965F686,
	StaticStorage_DownloadStyleIt_m777CD838AA4597E9A2A86DDDFCE862FBDCF0BE75,
	StaticStorage_AddCodeTextures_mD7A22C424EA0EA3E801A760289D3B84FF82518B7,
	StaticStorage_GetCodeTextures_m6FAE6602091BAC13CBCE792894EF95DA37DBF547,
	StaticStorage_SaveSmallTex_m624691660B086A23B9527686CD0967AB186B8512,
	StaticStorage_GetSmallCodeTex_mD4A80B277D442E10E6556B5939D196E2898510CD,
	StaticStorage_CT_m82C539A74DD3114877C196933FB6A70E18F41CBC,
	StaticStorage_CropTex_m1CD18B6BA44258FF6255E049EBCEF8C52704E6EC,
	StaticStorage__ctor_m43D48E8DC321836A5CD51D2C242B5D441ADF91AC,
	StaticStorage__cctor_m22F85484DFD238692F57228F04E06166C0AFC831,
	TopModal__ctor_m48BA2C9F31D56555A4E9E75EDC79502A777AEBF6,
	Tracker_ClickOnProduct_mFCBCA2C031710B9D01BEB1A022C54CA77E8683FE,
	Tracker_CatchError_m84078CF53F5093251E012B5665BAB26BDCD7C271,
	Tracker_SwipeToHome_m51F01F5E1F5996A9E0902CF8AFD29DE1C637E612,
	Tracker_SwipeToBack_m50D66FD8627DE9116EADBA3A44FC2FBB710F9498,
	Tracker_SwipeLargeImage_m145F8D5C62B5F1188B935AF5E3BF576BE5FDCBB9,
	Tracker_BotViewed_mC944A210369318B2819360D15C85033242229376,
	Tracker_BotProductDesc_m2D5F5CF04DA31422F49049911BD01233B4124001,
	Tracker_BotQR_m725231CBD4FC4BD49B20A63A00930845DE7A56B4,
	Tracker_BotStyleIt_m0807B4B78B848251AF7F4B63953255A3B36CC1E2,
	Tracker_PopupIdle_m8A68C56D9B93983776CA4899A5B9F6884B6B590B,
	Tracker__ctor_mCB1738FF4A8747740E90BB2D4604CF7EDB8C8158,
	CheckScene_Start_m0742F789CD58A7BB043F7DF5A27EF8C840B0040D,
	CheckScene_Update_mDA1218E2B9270CB6BEEAF78A45868413A0AAE29C,
	CheckScene_ChangeAPIStatus_m78D3917772289763892610ADBBEB4818B40D47DE,
	CheckScene_ChangeAPIServerStatus_m37DA4AE0A2FEEA7A32D63F02E6A9369EC20564CF,
	CheckScene_LOADAPI_m3D87202122347AE9557D0D8C9A90453CBAB60D98,
	CheckScene__ctor_m5DFFBD6998D29281C55AF718086E2D5855D060B8,
	ColliderTools_Start_mA8613F37B9F5BE47B527E16612A168DF2A33A833,
	ColliderTools_OnCollisionEnter2D_m78F7FABF5B56F5729C6F6A2D1D2258D4DF913BAA,
	ColliderTools_OnCollisionStay2D_m2B49E7A46A4A07A4FC1B7D61C0D91075B2C4B079,
	ColliderTools_OnCollisionExit2D_m27BFEDDDB24FAD02D09E86D1648B61F14222ED5D,
	ColliderTools__ctor_m0E4BDD0969C98629843795FE8F6B1B8688BBCAFD,
	CsvReadWrite_Awake_m872B69E0430289A4EC4AF0ACF2357CE9B10B4691,
	CsvReadWrite_SaveTxt_m3236282BB50991E8B7CB7C814A5D31633F0BC911,
	CsvReadWrite_AddLoadingDataTxt_mFA5160BDA26E7C2B1F15B4FF51274B345EDB527F,
	CsvReadWrite_AddDataCSV_m5C61BFC4763E402E2C3B6CFDA7DBBB12A726E2D4,
	CsvReadWrite_Init_mD5E76E4A28FF2673801740533A896451A18958AC,
	CsvReadWrite_Save_m12D7EFF412E125D816709AFF871DFBDAA8E9EC57,
	CsvReadWrite_GetFilePath_m67D4DDFA70BDE787D214A09D3304AA7D2B5239DA,
	CsvReadWrite_getPath_m9E71202C86B4321BB3929DDD4A52A53B6B52A7C5,
	CsvReadWrite__ctor_m6A795679813F744C663778CAEADD87D3525CE042,
	DetectSFX_Start_m5E961CB91E6DC0E61C561D34EF0A58C0684034D3,
	DetectSFX_Update_m1C11F1CD31EA4B11110DD791B31674F6F93845D9,
	DetectSFX__ctor_m66841804139887B9D6C7F6E6D4F2E6EA0ED5E03B,
	DetectTouchMovement_Calculate_m959665980E52112D4215C53D39EBA6D10AFCA984,
	DetectTouchMovement_Angle_m92E0D73E6D983DD03541403A4242A4235501F0E8,
	DetectTouchMovement_ApplyTo_mE3D11AAFB852BF54221CE7DEADFEC1CE6984A165,
	DetectTouchMovement__ctor_m71C55ED87D74AF305CD7ACBB4D41D7DC6C089E17,
	DragTools_Start_m706E26A5D0B308D6BBB75A969DA4D34F3CE685CD,
	DragTools_OnMouseDown_mC054A130FB4CF57B39DD44ED5BE7D14AC6D03E03,
	DragTools_OnMouseDrag_mA873A4CC83B41703E36F7F26C50BEB0D9DB12884,
	DragTools_OnMouseUp_mA1761DF1890D0DF7E535A62A8A563CDC5BA18BAA,
	DragTools__ctor_m0AC0D55FEC838B1EA0CD6D84FF7D04D676D62DEF,
	DragUI_get_Current_m6D53D9D69B102F96EA5FBA9CCDB8581E9EE547DE,
	DragUI_get_TopBound_m282534A181D96F60C9735D060A14FDF245769CAA,
	DragUI_get_BottomBound_m8EA2CEB1915668195AB6B6549933FED83679DD8C,
	DragUI_get_RightBound_mB23CFA8A024086A9638B3B925425D5E767C96ED0,
	DragUI_get_LeftBound_m98B2586E6B165AF56DA9B2D22F16B0C5CC66F633,
	DragUI_get_ParentCanvas_mFCBD06BEC946F664074147135DC01A575247EDAF,
	DragUI_get_IsOverlayMode_mD77BBFF67093EDF3A8D3EFCFB6744EDDAA56E98C,
	DragUI_Reset_mAB16AA6CFAB5228F039E1F03FD33117CB4AE7E83,
	DragUI_AttachBoxCollider_m911CF4714DAC0683D2E72CC478A09875ECD1BBF3,
	DragUI_Start_m9874AAE51FFDFF3299C79A39D0F6A9FB3FD4FC29,
	DragUI_Update_m199289A2C2730A00D796FACDEC68AA7A1753050D,
	DragUI_OnPointerDown_m977E5D65EF8A705570C3ADDF2CE7A78016CC1CFE,
	DragUI_UpdateDefaultPosition_m7437364959B8C21B516D21B0E9A573C009DB442F,
	DragUI_OnDrag_m0BD970B833BD7603F1CF61069C623970E84BC33D,
	DragUI_OnPointerUp_mA93DDABCA43BC93CF130786B01D0370262DCE7EC,
	DragUI_BackToDefaultPosition_m2DBDFAE6592BE1A8926D1D3615BB8B82E15BF3BF,
	DragUI_BackAnimated_mD28BF8926495CF4384093886AC1F2C6E98667FB9,
	DragUI_BackToDefaultSiblingIndex_mCE1F3EDE9446588795E6580AE6F5D2DBEB97A3E1,
	DragUI_OnDisable_m63A353686AA2FE8135FDF09F63C6D485C6CF951D,
	DragUI_OnDestroy_m3A610FAD2886BA057354DDEC9AE6BB1296AC6874,
	DragUI_LockMovements_m66C63D7F9A1515687327B2E1F5BD376B82A743C0,
	DragUI__ctor_mE72E9308ED94411A1F14BE528593765FDCE37E6F,
	FPSDisplay_Start_m9DE167885FC272684751D270809F5CD7CBE56661,
	FPSDisplay_Update_mE45C5E9F8AA0815DBDBC36FFE099E4E174985930,
	FPSDisplay_OnGUI_mBD59BE8E75936F4CEEA5D97FC32AD8BE1F1FEFEF,
	FPSDisplay__ctor_mE5C50AF5EDE70F699D1BDC3B56BF31972E2215CE,
	ImageResources_Awake_m962A6D29E957F012BABB5FB4ECF540A237142040,
	ImageResources_TshirtCounter_mEAA7737D70491917EF97E8856DF65BE6B9024B7B,
	ImageResources__ctor_mE3723A8BE67EEB3E7DE71BED67DD8BE2A5CED1C9,
	LoadingScene2_Awake_m1C9592348C66012A53681CC1230B3A976129ABF2,
	LoadingScene2_Start_m50934A31EBFC6FDA1A51919930F9C660A9BC90B1,
	LoadingScene2_Update_m8CE78A89BABF32282279D71383F5E9C7ECED796B,
	LoadingScene2_GOBtn_mBCE2090928CD92A39EA9843483D2D36979EFC1CD,
	LoadingScene2_ChangeColumn_mE40D9E211D0916F76D5EA41D6F078230A4255B3B,
	LoadingScene2_BranchDownloadAPI_m9E3D346366D4147415EF066243B96404A5550C00,
	LoadingScene2_MultiDownloadAPI_m0C3BB5CE22711E71B81D09EA2B8FC36BB08F7FB5,
	LoadingScene2_CheckAPIBool_m6E59F3055224CE5ECC144078FF70F05DDD618463,
	LoadingScene2_MultiDownload_mDB8C7E6992A667762A5F6AEC63E327BB369D0173,
	LoadingScene2_MultiDownloadNCache_m250FFDE279EF8AE50CC7FAC73778A2D11476CC2C,
	LoadingScene2_MultiDownloadDetail_m539528AE838A65B6A667D8DABE549ACBEF0355A4,
	LoadingScene2_MultiDLDetailNCache_m9F15C8D7C5FC0B64585DA8677881D0420DFA685B,
	LoadingScene2_CheckStep1_mC3A64D25C0068407CFA84D2CB17AF0101697ADFE,
	LoadingScene2_CheckStep2_mF70FAD1EC76D06879D87C1474FBE347104384427,
	LoadingScene2_CheckStep2_Failed_mBF1A493E588B70FF98E3962AE24D3778E46AECD2,
	LoadingScene2_FinalCheck_m36828570AE9085F486B351F3E693D73141C9897A,
	LoadingScene2__ctor_m24844DB1692771491E27ABD3E194EC82BB337306,
	MobileInputHandler_Start_m6AB3FE268655F136C6BF653785E17A3988F71AC2,
	MobileInputHandler_Update_m6127D687BD849FC6C0C96C953C7A7615086AEF5F,
	MobileInputHandler_OnPressBackKey_mD068FD19E121CE693A062E2725653AD2365108B3,
	MobileInputHandler__ctor_m5874BB0CA421ADC81FC1F40E43B2994D4FB1FF9B,
	PlayerPrefsX_SetBool_m65A3486B9543DC5EB90ABC94AECA79917B99782C,
	PlayerPrefsX_GetBool_m79C50F07E41D786E7AEC4A2F9F12699B8653C0AA,
	PlayerPrefsX_GetBool_mD8D779122D1FE8E5D2194E0F5907E16BA53C28C9,
	PlayerPrefsX_GetLong_mF623D746F8E99ECC43AF74853D434751AAC52251,
	PlayerPrefsX_GetLong_m12B4F39533853341DCD561621437264EE7A0E224,
	PlayerPrefsX_SplitLong_mEE5B8C5473668332C1F35150FA58A296E4600836,
	PlayerPrefsX_SetLong_m3F01DD141F8854F17D37DDD03338A81918ED5789,
	PlayerPrefsX_SetVector2_m55F95C8CB796DF7C8CB9DC56AA7593EBCB0810EA,
	PlayerPrefsX_GetVector2_mA1134460C558BF1DB8AA21847F4CE259D756A3E9,
	PlayerPrefsX_GetVector2_m54BCC7208903ED101AB18E1F365661738E8D7C33,
	PlayerPrefsX_SetVector3_m19D8C5F14F50E1043CD2A65BDF248E7E92246520,
	PlayerPrefsX_GetVector3_m3864F748022CB6918D18A830FCA406D6F5A6407D,
	PlayerPrefsX_GetVector3_m067939EBC9967B8FCD4583394D7AFC5C659BBAF2,
	PlayerPrefsX_SetQuaternion_mD20099D11CC4FEE41D23D5B33CD7CDCA681FCCA4,
	PlayerPrefsX_GetQuaternion_m0DEBA1DC49DD8B10F8319BE51D536A79D037946E,
	PlayerPrefsX_GetQuaternion_m2B57F04CBFBC2A93C668AB99D69DF2CA94709ADD,
	PlayerPrefsX_SetColor_m1DF6AEB4C0471EDB7DE54950DED558BECE54FD85,
	PlayerPrefsX_GetColor_mFEDCBD770BA05E6EDB8417BC80DDA0994B2215DD,
	PlayerPrefsX_GetColor_mD6C55716F9715ECBA5F58EBCF1BFAC9AE8C3317C,
	PlayerPrefsX_SetBoolArray_m0053952131A28788A57EA17A35AD317776B94BEB,
	PlayerPrefsX_GetBoolArray_mCA6912469F93CE770BCA202140668908831E687F,
	PlayerPrefsX_GetBoolArray_mB27C61D2742FCCC22125D5C9A26AB329235040F4,
	PlayerPrefsX_SetStringArray_mE7F9544383E91AEBED66859ED957371EA9897171,
	PlayerPrefsX_GetStringArray_m56A0F4FBAA6AA47D35CFDFD09D46898D42FFB89C,
	PlayerPrefsX_GetStringArray_m51D364FBA310FD2EA3BAF2456878CBEA0569D5D2,
	PlayerPrefsX_SetIntArray_mA76A96BDC8F678C853B9470B5D013881794ABEFD,
	PlayerPrefsX_SetFloatArray_m206F2B51F9C4D4BB8461A2AAAC45ED2C5284920B,
	PlayerPrefsX_SetVector2Array_m3DBFB4A55E125962B52A50598B0A1B15F59AE5D8,
	PlayerPrefsX_SetVector3Array_m5CDF8EBBE07D2769764E96457BAE346726F55CE1,
	PlayerPrefsX_SetQuaternionArray_mCAD0DB1DC55D9FDA0C94F71BE01D48BB65DEAE8E,
	PlayerPrefsX_SetColorArray_mAF3268DC430B09CAE7C9EB572485252FFB66D92A,
	NULL,
	PlayerPrefsX_ConvertFromInt_mA214B13EDB658E59C6D76383F0FB334506A4792E,
	PlayerPrefsX_ConvertFromFloat_mCD49916CDB39ED7A1B035F03046B04BDE0610952,
	PlayerPrefsX_ConvertFromVector2_m2F6011CBC58DEA02436B612E426953C3F236B6AF,
	PlayerPrefsX_ConvertFromVector3_m9509552575513A02DB9DA0364B8B294F8E195364,
	PlayerPrefsX_ConvertFromQuaternion_m2AB8ECFEC0C300AD267EF8FB381D199E780F5F2E,
	PlayerPrefsX_ConvertFromColor_m5DAA4AA3700BD4E4F653784C17E80B597A65E27B,
	PlayerPrefsX_GetIntArray_m349AB786A0F916179B9E3E44AC0E022988FE21AB,
	PlayerPrefsX_GetIntArray_m2BE03C4CCE0B4F5F4405CF30069590834FFFB18F,
	PlayerPrefsX_GetFloatArray_m65613451D241A80297A79FA77BC505EF9CC187F8,
	PlayerPrefsX_GetFloatArray_mCA6266CF0B77F7A536D6A5BB5AFAA80859256D0F,
	PlayerPrefsX_GetVector2Array_mF690D2104BF67B5058A19494D9A78D37F20032C0,
	PlayerPrefsX_GetVector2Array_m3C2181DC09CF31EC4D8E639381E796E52116FEC0,
	PlayerPrefsX_GetVector3Array_mE1BC5E69B68E3D6F9C04AC1CFBFBF171132FEBE5,
	PlayerPrefsX_GetVector3Array_m01A7E338CDC2F7B57D827598B7374DCC2C87586B,
	PlayerPrefsX_GetQuaternionArray_mC1935A7AE43DC4335515B649F14F57C1E4AB583B,
	PlayerPrefsX_GetQuaternionArray_m8B17BDD7ABD71430182AA9EEB98B8BEEEF505460,
	PlayerPrefsX_GetColorArray_m5A3F14CD6E3FDA3E6C8BA8708DE563B187A1F695,
	PlayerPrefsX_GetColorArray_m488B0F0D9175D92C065E0EAFFA843CE107FE36A0,
	NULL,
	PlayerPrefsX_ConvertToInt_mD3E07E6E9D9101165A89EA99245448A3473C2EAA,
	PlayerPrefsX_ConvertToFloat_mEA9482480CBF847B60571B5ED6191B79D51D1073,
	PlayerPrefsX_ConvertToVector2_m27012AE8FD4BD9FC1E4C908C931D212893B8EC58,
	PlayerPrefsX_ConvertToVector3_m1514F7034540CF554D1A9C45E3FDFDDF660B5F51,
	PlayerPrefsX_ConvertToQuaternion_mF46BD45044218548093B772806EAFFCD8D03CA99,
	PlayerPrefsX_ConvertToColor_m4B068B45621719E49D069515D317C7B27735F4EE,
	PlayerPrefsX_ShowArrayType_mD21DAF3E943D67D359CDC5E66548A8F1439A68C4,
	PlayerPrefsX_Initialize_mA42B73D4CEB5E787232081316795EAE682FDF968,
	PlayerPrefsX_SaveBytes_m3E5803A2E2510AC5CA8AE227BF311BB5FE420F60,
	PlayerPrefsX_ConvertFloatToBytes_m40CA01A5ABE435B40949FA52C15216157F08583B,
	PlayerPrefsX_ConvertBytesToFloat_m5E30741F2EFAD90022D4CF082D8A65FFE4A70E6F,
	PlayerPrefsX_ConvertInt32ToBytes_m7E0964FFC5EB3EDB4D8C861CD2273BFEB436413C,
	PlayerPrefsX_ConvertBytesToInt32_m12C722E4494600F4792ACFF8996EF582CF235680,
	PlayerPrefsX_ConvertTo4Bytes_m0E98C08BFDA3B7CBBBBD6ABDB5E7BCB54DBF4E5A,
	PlayerPrefsX_ConvertFrom4Bytes_mB7B5773E1AE2ACB18A1CB29A3B3FBBAC403E23AD,
	PlayerPrefsX__ctor_m986F8E45BDB9448BFA635CBC3F9898AE8646CD9E,
	StarFX_Start_m3BD70D045D73EDA70F2AAA59076BC1462A2D2130,
	StarFX_Update_m5D991ACD005AB9882B7A31330001369DF5C63144,
	StarFX_Star_m23E114883BD322EAEA93E0F1338326D3CEDA72BE,
	StarFX__ctor_m63D49EBEFFE4FB745D0A77ADAE27D769DA035ED6,
	SwipeDetector_Update_mA095F65D05FE0B2FC84E88D00DCE273D74A13BEC,
	SwipeDetector_checkSwipe_m2A607CE45F221869AED7243D928F344BBC05CCC4,
	SwipeDetector_verticalMove_mE585D1F8858FE0459C8472EF578A61EF1C6844C0,
	SwipeDetector_horizontalValMove_mD02D67A2CE78F9F9B3EE2979617ED7C1177919F2,
	SwipeDetector_OnSwipeUp_m091A03F545820C1B69AC974BC293F91C38E24C68,
	SwipeDetector_OnSwipeDown_m376B53DECF679B05FD81ED72B8D9F6F4D3996EDF,
	SwipeDetector_OnSwipeLeft_m0FF32BE2CEB5FC86F17F1CD83728E3AC33E0AE93,
	SwipeDetector_OnSwipeRight_mB47E4809A73F15744B763903180B5189F2305652,
	SwipeDetector__ctor_m2411490382B3EAD042053E6843814D951CE9D535,
	TouchDownInput_Start_m88037C0A05D9F7358576F23F793DC83A40C14596,
	TouchDownInput_OnMouseDown_mBB2927D4E7BC8042A1BB73BD30B1B971BEE9A5BE,
	TouchDownInput__ctor_m142C28FCBE9D80F4A6EF7CB85953C5FFD994C9B6,
	TouchUpInput_Start_mCC82E84E3690092935B6CF7328CEC93958AE1EFF,
	TouchUpInput_OnMouseUp_mE76D5806545096D42D48022FBB7BC940D4485706,
	TouchUpInput__ctor_m014132CC5987A9D586A420B93C566787E6EE0DAF,
	ChatController_OnEnable_mD13A02B63932BDA275E1A788FD72D86D69B9E440,
	ChatController_OnDisable_mD31D1ED1B2C3C82986BFBD18FA584D45167431F3,
	ChatController_AddToChatOutput_m01CC3E959ACECC222DFD8541231EC1E00C024194,
	ChatController__ctor_mF4343BA56301C6825EB0A71EBF9600525B437BCD,
	EnvMapAnimator_Awake_mAB3C67FA11192EFB31545F42D3D8AAB2A662FEB2,
	EnvMapAnimator_Start_m21098EFD0904DFD43A3CB2FF536E83F1593C2412,
	EnvMapAnimator__ctor_mBA9215F94AB29FBA4D3335AEA6FAB50567509E74,
	TMP_DigitValidator_Validate_mD139A23C440A026E47FA8E3AD01AD6FEF7713C3D,
	TMP_DigitValidator__ctor_mF6477F5EB75EC15CD6B81ACD85271F854BABC5D6,
	TMP_PhoneNumberValidator_Validate_mCA5EA200223A9F224F2F4DBD306DAE038C71A35F,
	TMP_PhoneNumberValidator__ctor_mBB38130850945A40631821275F07C19720E0C55E,
	TMP_TextEventHandler_get_onCharacterSelection_mF70DBE3FF43B3D6E64053D37A2FADF802533E1FF,
	TMP_TextEventHandler_set_onCharacterSelection_m237C99FE66E4E16518DAE68FF9CBF1A52E816AD2,
	TMP_TextEventHandler_get_onSpriteSelection_m395603314F8CD073897DCAB5513270C6ADD94BF4,
	TMP_TextEventHandler_set_onSpriteSelection_mAAE4B440E34EE5736D43D6A8A7D3A7CEE0503D69,
	TMP_TextEventHandler_get_onWordSelection_m415F4479934B1739658356B47DF4C2E90496AE2E,
	TMP_TextEventHandler_set_onWordSelection_m6062C0AF2FDD8752DC4A75663EE8E5C128504698,
	TMP_TextEventHandler_get_onLineSelection_m8E724700CC5DF1197B103F87156576A52F62AB2B,
	TMP_TextEventHandler_set_onLineSelection_m1A8E37D2069EF684EF930D4F1ABE764AE17D9A62,
	TMP_TextEventHandler_get_onLinkSelection_m221527467F0606DD3561E0FB0D7678AA8329AD5D,
	TMP_TextEventHandler_set_onLinkSelection_m1376CC9B70177B0C25ACEDF91D5B94BC4B8CF71D,
	TMP_TextEventHandler_Awake_m9A353CC9705A9E824A60C3D2D026A7FD96B41D74,
	TMP_TextEventHandler_LateUpdate_m2F3241223A91F9C50E11B27F67BA2B6D19328B72,
	TMP_TextEventHandler_OnPointerEnter_m1827A9D3F08839023DE71352202FE5F744E150EF,
	TMP_TextEventHandler_OnPointerExit_m788B93D2C3B54BCF09475675B274BCB047D449FB,
	TMP_TextEventHandler_SendOnCharacterSelection_mBC44C107A6FB8C43F7C6629D4A15CA85471A28B2,
	TMP_TextEventHandler_SendOnSpriteSelection_mEF24BCE06B0CE4450B6AE9561EC4B5052DAF00F6,
	TMP_TextEventHandler_SendOnWordSelection_m7C4D266070EE2ADC66BCCFD50EB74FEB4923B77E,
	TMP_TextEventHandler_SendOnLineSelection_mAAF4AF44929D0C9FD73C89E5266028908074AEB1,
	TMP_TextEventHandler_SendOnLinkSelection_m082D12F7D044456D8514E4D31944C6900F8262C0,
	TMP_TextEventHandler__ctor_mEA56AE9489B50CF5E5FC682AA18D1CE9AF8E1F8B,
	Benchmark01_Start_mC0055F208B85783F0B3DB942137439C897552571,
	Benchmark01__ctor_m2FA501D2C572C46A61635E0A3E2FF45EC3A3749C,
	Benchmark01_UGUI_Start_m976ED5172DEAFC628DE7C4C51DF25B1373C7846A,
	Benchmark01_UGUI__ctor_mD92CA5A254960EB149966C2A3243514596C96EAD,
	Benchmark02_Start_m2D028BFC6EFB4C84C1A7A98B87A509B27E75BA06,
	Benchmark02__ctor_m7CA524C53D9E88510EE7987E680F49E8353E4B64,
	Benchmark03_Awake_m8D1A987C39FD4756642011D01F35BDC3B1F99403,
	Benchmark03_Start_m73F65BA012D86A6BE17E82012AE8E2339CA5D550,
	Benchmark03__ctor_m9A5E67EA64AAC56D56C7D269CC9685E78276360A,
	Benchmark04_Start_m22D98FCFC356D5CD7F401DE7EDCDAF7AE0219402,
	Benchmark04__ctor_mDAC3E3BE80C9236562EFB6E74DEBE67D8713101D,
	CameraController_Awake_m581B79998DB6946746CBF7380AFCC7F2B75D99F7,
	CameraController_Start_mA9D72DB0BB6E4F72192DA91BC9F8918A9C61B676,
	CameraController_LateUpdate_mDC862C8119AB0B4807245CC3482F027842EAB425,
	CameraController_GetPlayerInput_m198C209AC84EF7A2437ADB2B67F6B78D12AB9216,
	CameraController__ctor_m2108A608ABD8EA7FD2B47EE40C07F4117BB7607E,
	ObjectSpin_Awake_mDA26D26457D277CC2D9042F3BD623D48849440C4,
	ObjectSpin_Update_mC50AAC1AF75B07CD6753EA3224C369E43001791B,
	ObjectSpin__ctor_m2832B9D713355ECF861642D115F86AA64A6F119E,
	ShaderPropAnimator_Awake_m8E01638EBFE80CC0B9E4A97AB809B91E3C6956BE,
	ShaderPropAnimator_Start_m24F4FADC328B0C76264DE24663CFA914EA94D1FD,
	ShaderPropAnimator_AnimateProperties_mC45F318132D23804CBF73EA2445EF7589C2333E9,
	ShaderPropAnimator__ctor_mC3894CE97A12F50FB225CA8F6F05A7B3CA4B0623,
	SimpleScript_Start_m22A3AE8E48128DF849EE2957F4EF881A433CA8CB,
	SimpleScript_Update_m742F828A2245E8CC29BC045A999C5E527931DFF1,
	SimpleScript__ctor_mA2284F621031B4D494AC06B687AF43D2D1D89BD7,
	SkewTextExample_Awake_m51C217E0CB26C2E627BA01599147F69B893EF189,
	SkewTextExample_Start_m6A9CEFA12DB252E297E41E256698DD4E90809F6A,
	SkewTextExample_CopyAnimationCurve_m3CE7B666BEF4CFFE9EB110C8D57D9A5F6385720B,
	SkewTextExample_WarpText_m4A69C47EA665D49482B930F924E49C8E70FAC225,
	SkewTextExample__ctor_m11DC90EB1A059F4201457E33C4422A7BDA90F099,
	TMP_ExampleScript_01_Awake_m9CE8A9F929B99B2318A6F8598EE20E1D4E842ECD,
	TMP_ExampleScript_01_Update_m8F48CBCC48D4CD26F731BA82ECBAC9DC0392AE0D,
	TMP_ExampleScript_01__ctor_m9F5CE74EDA110F7539B4081CF3EE6B9FCF40D4A7,
	TMP_FrameRateCounter_Awake_m906CC32CE5FE551DF29928581FFF7DE589C501F2,
	TMP_FrameRateCounter_Start_m614FA9DE53ECB1CF4C6AF6BBC58CE35CA904EB32,
	TMP_FrameRateCounter_Update_m16AB65EF6AB38F237F5A6D2D412AB7E5BF7B1349,
	TMP_FrameRateCounter_Set_FrameCounter_Position_m537A709F25C3AA752437A025BEE741BD2F71320E,
	TMP_FrameRateCounter__ctor_mD86AC3A8D918D14200BF80A354E0E43DC5A565A2,
	TMP_TextEventCheck_OnEnable_m22D9B03F3E1269B8B104E76DA083ED105029258A,
	TMP_TextEventCheck_OnDisable_m42813B343A1FDD155C6BFBFCB514E084FB528DA0,
	TMP_TextEventCheck_OnCharacterSelection_mC6992B7B1B6A441DEC5315185E3CE022BB567D61,
	TMP_TextEventCheck_OnSpriteSelection_mEC541297C2228C26AB54F825705F0476D45F877A,
	TMP_TextEventCheck_OnWordSelection_mA1170F805C77CC89B818D8FBEE533846AF66509C,
	TMP_TextEventCheck_OnLineSelection_mB871339347DCB016E019F509A00BDE9A58105822,
	TMP_TextEventCheck_OnLinkSelection_m44A79DDBDF03F254BAFB97BE3E42845B769136C5,
	TMP_TextEventCheck__ctor_mA67343988C9E9B71C981A9FFAD620C4A9A6AA267,
	TMP_TextInfoDebugTool__ctor_m1EA6A5E31F88A1C7E20167A3BCCE427E9E828116,
	TMP_TextSelector_A_Awake_m82972EF3AF67EAAFD94A5EE3EA852CE15BE37FC1,
	TMP_TextSelector_A_LateUpdate_m40594D716F53E6E5BC0ECD2FFE8ECA44FAA5C8E4,
	TMP_TextSelector_A_OnPointerEnter_m8462C2DC4F71BDE295BE446B213B73F78442E264,
	TMP_TextSelector_A_OnPointerExit_m3AC7467ECE689A58590DA325F8B300B08C1E1B5D,
	TMP_TextSelector_A__ctor_m081D44F31AA16E345F914869A07BD47D118707DF,
	TMP_TextSelector_B_Awake_m217B6E2FC4029A304908EE9DC1E4AA2885CBF8A3,
	TMP_TextSelector_B_OnEnable_m24A7CCA0D93F17AC1A12A340277C706B5C2F9BAB,
	TMP_TextSelector_B_OnDisable_m6088452529A70A6684BD8936872B71451779A2F4,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m79EEE4DF7792F553F5DEDCF0094DAC6F2A58137A,
	TMP_TextSelector_B_LateUpdate_m745577078D86EF6C23B914BD03EA1A1D169B9B7B,
	TMP_TextSelector_B_OnPointerEnter_mF6C09A2C64F5D2619014ADD50039358FAD24DB3E,
	TMP_TextSelector_B_OnPointerExit_mB0AAA8D034FC575EB3BCF7B0D4514BD110178AD3,
	TMP_TextSelector_B_OnPointerClick_m13B20506F762769F099DE10B3CCA2DF194192B42,
	TMP_TextSelector_B_OnPointerUp_mD53FD60E0C5930231FB16BDEA37165FF46D85F6E,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m1D03D0E14D6054D292334C19030256B666ACDA0E,
	TMP_TextSelector_B__ctor_m494C501CF565B1ED7C8CB2951EB6FB4F8505637F,
	TMP_UiFrameRateCounter_Awake_m255A7821E5BA4A7A75B9276E07BC9EA7331B5AA6,
	TMP_UiFrameRateCounter_Start_mA4A02EB5C853A44F251F43F0AD5967AE914E2B0F,
	TMP_UiFrameRateCounter_Update_m04555F8DF147C553FA2D59E33E744901D811B615,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_mDF0FDFBCD11955E0E1D1C9E961B6AD0690C669ED,
	TMP_UiFrameRateCounter__ctor_m99ACA1D2410917C5837321FC5AC84EAED676D4CC,
	TMPro_InstructionOverlay_Awake_m639E300B56757BDB94766447365E1C94B5B83ACD,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m74B8F0AE15DA6C9968C482981EBCF5CC9DB1F43D,
	TMPro_InstructionOverlay__ctor_m570C4B4CB3126622D6DFF71158336313C45C717A,
	TeleType_Awake_m5F758974DA88ED8187E71A5100D2D9E47985E359,
	TeleType_Start_mC32B726B6202883E12E1A62A52E50092E7E9D9F0,
	TeleType__ctor_m6CDFDC88D47FE66021C133974C8CB0E16B08A00E,
	TextConsoleSimulator_Awake_m4F61F06DFE11CFAF9B064CCA5B2D6423D5CFC302,
	TextConsoleSimulator_Start_m572903C9070A8AD276D2CB14DF7659AE551C75B3,
	TextConsoleSimulator_OnEnable_m1A36B043E4EDD945C93DFC49F7FAFB7034728593,
	TextConsoleSimulator_OnDisable_m6F9BE1975CB15EE559D3B617E8972C8812B41325,
	TextConsoleSimulator_ON_TEXT_CHANGED_m73C6B3DAA27778B666B9B3B75C9D4641FC1BEC8A,
	TextConsoleSimulator_RevealCharacters_mC24F1D67B99F0AFE7535143BB60C530C8E8735F0,
	TextConsoleSimulator_RevealWords_m68CAA9BDC1DF3454ECB7C5496A5A2020F84027B8,
	TextConsoleSimulator__ctor_m4DB9B9E3836D192AA7F42B7EBDC31883E39610E9,
	TextMeshProFloatingText_Awake_mD99CC6A70945373DA699327F5A0D0C4516A7D02A,
	TextMeshProFloatingText_Start_m3286BB7639F6042AD4437D41BF4633326C4290BE,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_m61ABFBAA95ED83A248FBCC3F5823907824434D34,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_m65A61DF0BA640F2787DD87E93B8FD9DEC14AACB4,
	TextMeshProFloatingText__ctor_m11DBC535BA8001B48A06F61515C0787C3A86611F,
	TextMeshSpawner_Awake_mB3C405B4856E9B437E13E4BD85DAE73FFF1F6561,
	TextMeshSpawner_Start_m436D4CD2A82C0F95B8D942DF4CCFCE09792EDF0F,
	TextMeshSpawner__ctor_mDB693DABF1C3BA92B7A3A4E1460F28E3FAFB444D,
	VertexColorCycler_Awake_mAEEAA831C084B447DFD0C91A5FA606CB26D3E22A,
	VertexColorCycler_Start_mF01B64B3E5FE5B648DE2EED031962D9183C2D238,
	VertexColorCycler_AnimateVertexColors_m9CDB87631C324FB89FA7D52F5BC910146F3DDEB7,
	VertexColorCycler__ctor_m9B68D69B87E07DC0154344E5276EFC5B11205718,
	VertexJitter_Awake_m9699A62E72D3A262EDFDF7AC63ABD33E53F179B6,
	VertexJitter_OnEnable_m1B592E7AC81C7F17D0A59325EBDE71E067178E8A,
	VertexJitter_OnDisable_m5567B541D1602AD54B0F437D4710BCD1951FE2C5,
	VertexJitter_Start_mB698E212B8C3B7C66C71C88F4761A4F33FCE4683,
	VertexJitter_ON_TEXT_CHANGED_m09CFD4E872042E7377BEC8B95D34F22F62ABC45B,
	VertexJitter_AnimateVertexColors_m1CCF60CA14B2FF530950D366FF078281ADC48FF4,
	VertexJitter__ctor_mC19C148659C8C97357DB56F36E14914133DA93CF,
	VertexShakeA_Awake_mD2ABEB338822E0DBCFDEC1DB46EC20BB2C44A8C2,
	VertexShakeA_OnEnable_m60947EACA10408B6EAD2EC7AC77B4E54E2666DD8,
	VertexShakeA_OnDisable_mF5AF9069E523C0DF610EF3BE2017E73A4609E3CC,
	VertexShakeA_Start_m96D17C13A279F9B442588F6448672BCF07E4A409,
	VertexShakeA_ON_TEXT_CHANGED_mE761EE13D2F9573841EFA5DEE82E545545280BFA,
	VertexShakeA_AnimateVertexColors_mC97FED540BDE59254AB30C5E6BCF1F53B766945F,
	VertexShakeA__ctor_mD84B9A0167705D5D3C8CA024D479DC7B5362E67B,
	VertexShakeB_Awake_mA59F3CA197B3A474F4D795E2B3F182179FF633CF,
	VertexShakeB_OnEnable_m8D4DD5CA81E5B0C02937D43C1782533D54F34B3F,
	VertexShakeB_OnDisable_m4473873008D4A843A26F0D2353FC36ABE74CEED2,
	VertexShakeB_Start_m24BEF670ABD1CCC864FAFE04750FEB83D916D0DF,
	VertexShakeB_ON_TEXT_CHANGED_mCC864EE9569AF68F53F6EAEB2CE8077CFEAF9E53,
	VertexShakeB_AnimateVertexColors_mAF3B23F4DD98AC3E641700B994B2994C14F0E12C,
	VertexShakeB__ctor_mDCAEC737A20F161914DD12A2FCBAB6AB7C64FEF2,
	VertexZoom_Awake_m5F98434E568929859E250743BA214C0782D17F2B,
	VertexZoom_OnEnable_m01E35B4259BA0B13EC5DA18A11535C2EC6344123,
	VertexZoom_OnDisable_mACD450919605863EC4C36DA360898BAEBBF3DDDB,
	VertexZoom_Start_m6910FF4AC88466E3B7DB867AD553429F1745320D,
	VertexZoom_ON_TEXT_CHANGED_mBFB58FE53145750AD747B38D9D1E7E00F8DBF9A0,
	VertexZoom_AnimateVertexColors_m328088EC0F893B3E60BB072F77ADF939B8566E4D,
	VertexZoom__ctor_m67833553C2739616BF059C661F92A032885510B2,
	WarpTextExample_Awake_m9643904751E2DF0A7DF536847AEA1A2B0774DD20,
	WarpTextExample_Start_mDF931C271901519BF21FD356F706FA8CDE236406,
	WarpTextExample_CopyAnimationCurve_m2C738EA265E2B35868110EE1D8FCBD4F1D61C038,
	WarpTextExample_WarpText_mAAAB1687869E0121C858C65BDB2D294D55CFB67A,
	WarpTextExample__ctor_mC3EAA1AE81FB3DA4B25F20E557EC6627A06DCB31,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PromiseCancelledException__ctor_m030697F2BC9A3E445CCA7C2C8C2A929815AF9F78,
	PromiseCancelledException__ctor_m323888449BD9E45FAC65ED965B9A2BB2FC95542A,
	PredicateWait__ctor_mC08AF6709F2CBE2195CA76D97541173CB67F29D7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PromiseTimer_WaitFor_m0529FE7F8390A993817A2A2BBB3681E4A3CB8884,
	PromiseTimer_WaitWhile_m4D17DB0C6BAF26BDDB218B85E39ED46187C59942,
	PromiseTimer_WaitUntil_m566F62EAF61CC80FEB9D55C4E280A983F7B433DE,
	PromiseTimer_Cancel_mB9D64EE0A89E91089E2F9CA5767E4608EDD33436,
	PromiseTimer_FindInWaiting_mEFABFD6D1047E169ED375A258A9D2EAA8E0DFD86,
	PromiseTimer_Update_m65584A695C723DDE11E807CAB9DD87AE0DB717E9,
	PromiseTimer_RemoveNode_mA5D117461EAD340EC4BE54013BE1D5D47A50B96F,
	PromiseTimer__ctor_m5E4CD41451C70CB626BD9855456A3D364C35A188,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ExceptionEventArgs__ctor_m93824CCD3489EB59D01C6CFFE7B43A53B7F30C90,
	ExceptionEventArgs_get_Exception_m6C8716CA4FB7C4BC8A73ECFC575F105AB95E435E,
	ExceptionEventArgs_set_Exception_m877F36F858B5D027D965CDFC78F3368BB2D7B0D4,
	Promise_add_UnhandledException_m941463FE821F0D72B1BFB10931A8BA9334796AF2,
	Promise_remove_UnhandledException_m300D2B53F6534C0B3814F86B4DAC54E6AF8B26DD,
	Promise_GetPendingPromises_m95C0BBD9DBB94BC438E394562C1CF780AF58F2F5,
	Promise_get_Id_m1B96CB48D753887FA47004BDF844BEC9D7C07D27,
	Promise_get_Name_m6C739D83EBE47A29C224B6D429A68DF7679D152E,
	Promise_set_Name_m6D430D307BEFB602FFACC551FBD0A1D807C5FF1A,
	Promise_get_CurState_m6BBB58E657D13BF0CD04C9655F6F21B9ED43C41D,
	Promise_set_CurState_m32B7787EC17864659AFDD172596A29D83F6B5276,
	Promise__ctor_m74479ED63EA3F49B8359E3EE719F5908EBF09827,
	Promise__ctor_mF031BEF18EAD28846907566FE0C342BCB2F25582,
	Promise_NextId_mAD3EC5C27AC9D6D63BC742AEF2AC10457B6D1264,
	Promise_AddRejectHandler_m727EC90B67554F44C4ACC0C9EC271B35528CC964,
	Promise_AddResolveHandler_m9A302DBCBEE45AF916835B674404517018732093,
	Promise_AddProgressHandler_mE0304DED3ED13E1F9C097EEF03A22054B34245B2,
	Promise_InvokeRejectHandler_mEAC453FA262BC879419E0133D02BA4CE571265C8,
	Promise_InvokeResolveHandler_m07E8086C139F2B59867859A4F0C441B633F5BCA2,
	Promise_InvokeProgressHandler_m5F9131DA45C48F195CDEDDB3AEA8BF9B4AA4221C,
	Promise_ClearHandlers_mC9D48501996B8DE89E2482A2B9D5FBC131408E0E,
	Promise_InvokeRejectHandlers_m633D8D6BD825251FE3B80773BA7CCD3891763B09,
	Promise_InvokeResolveHandlers_m960BB0612F17CF8C11E51AEB39B8D992075AECA8,
	Promise_InvokeProgressHandlers_m3DCA143530F7A34478B6857C6BBE317335158B07,
	Promise_Reject_mBD86521F8A2146603C73294A82CFDCEFAA16CE37,
	Promise_Resolve_m472421BD74A2F4B26381B2647E41D9E9213C8DC4,
	Promise_ReportProgress_mBFE7ADC3DD1288940A3FD8C22D47721A63E4095A,
	Promise_Done_m4993F845C81D3E879B4F0A5FD7707AAB7CF3EDCA,
	Promise_Done_m62C247DA7CD5916E319B453E928F9CF10677F1E0,
	Promise_Done_m6214125B2C3D5E1E4C605E7E63027D9A5B9EEF9E,
	Promise_WithName_m056817CB1DFC0AECA1E6F89B7F33B8751E2BC641,
	Promise_Catch_mD44B946DA04440FB389D4F3D824B3F6D0ECC6436,
	NULL,
	Promise_Then_mAE61A3C357AFD1DA2DFC968DC97D62E5D2485354,
	Promise_Then_m882CBA16CDC16CCB804EE7A04A845CF124890F8A,
	NULL,
	Promise_Then_m52095C7766399A71B875EFACF38B1A796F4F8C67,
	Promise_Then_m775060BCFE154B25F9CEDDBD48F7D414C3CF8286,
	NULL,
	Promise_Then_m60CADBEDBB3AAFC26BD4EC6D538400058DD4CC1B,
	Promise_Then_m2C3A022FCE5E18FB1076017E4F60F8E7DDBE7409,
	Promise_ActionHandlers_m0F8C97FC81D290E1CEB3B90EFC49881B17CCDD0C,
	Promise_ProgressHandlers_m606A9EEA6147508575B64DA430B847DF5DC0BDF8,
	Promise_ThenAll_m3015B5D9AC9E6993B8DFD4D9B55DE6F3DD3C032C,
	NULL,
	Promise_All_m82839CDADB53DD91569988C65EC287EED839ECF4,
	Promise_All_m7981EBD01C3DD56996F970D3B8EF61FEAD7FDC23,
	Promise_ThenSequence_m5A5323085CD776CB5E8056598851FC5F13B614E1,
	Promise_Sequence_mCDCB22689B059501F850EF021209D37956F933B5,
	Promise_Sequence_mE48B325D21BE25E5BED812EF4B0F85445F03CD67,
	Promise_ThenRace_m5F2C5506CB757272201882A203B0A891105D316D,
	NULL,
	Promise_Race_m78DEF2E794FF3713EB41095CB056B7DE90F61329,
	Promise_Race_m044CC5D4C0B92A1E6F5B3F664238B8F149E4C787,
	Promise_Resolved_m68DB50577F8EEF1CA2536EDDB26928A5A85DE6A3,
	Promise_Rejected_mB2B4B779181CFB7A0E1990FFA140A233514AF2B8,
	Promise_Finally_mC960B8E819FF3975B3C58B88BA8C886E5AE8361B,
	Promise_ContinueWith_mD3A2C762DEF611012BBA39BDFB076ED25CC81AF9,
	NULL,
	Promise_Progress_m66861F86E45D527A1D4371EB5A3B26C41FCF1DE7,
	Promise_PropagateUnhandledException_mDAF1E6FD53CD7F1E470BD091CEABC3FC6F819A8B,
	Promise__cctor_mDF7E11D37225ACD6A42CC2FE1E4AD32671A16050,
	Promise_U3CInvokeResolveHandlersU3Eb__35_0_m5EE7EC2D8D0D87277E2B6D0615FDA4E88BA55E93,
	Promise_U3CDoneU3Eb__40_0_m56A2395BBD3D2807B926F86AEA92D0B4867CD8F0,
	Promise_U3CDoneU3Eb__41_0_m4CD266F731687F1277C63E96B638B9E83EEE27EB,
	Promise_U3CDoneU3Eb__42_0_m87C455CEA250E7327645A69DF866C4EEC4531ADB,
	NULL,
	NULL,
	NULL,
	Tuple__ctor_m93D270330C9264FC3AB9AC7994C49DE719240BA1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PromiseException__ctor_m0B6078A58B2840086737C7E6431C15EC9546E73E,
	PromiseException__ctor_mD633D198CE0C78BAA84BD2BB9D03F09EFA4F8931,
	PromiseException__ctor_m3B3A828C1DBEA521CE7FADC387480191C26B211D,
	PromiseStateException__ctor_m3F47AB95336E2494D42CA7D7F82ED5931C699FA8,
	PromiseStateException__ctor_mE5A772777FDF0B4CED17F8DEC8B33078302D5C43,
	PromiseStateException__ctor_m60FCCFAA1B0C7DB848B551706DF46B0580F24B04,
	NULL,
	NULL,
	NULL,
	HttpBase_CreateRequestAndRetry_mC2DC93BB1F8E3DBDAB19DBAAE80BF0323E48D4C6,
	HttpBase_CreateRequest_m275DB6EB32A338391A7422933B486595D82DB9D2,
	HttpBase_CreateException_m194CF2E26B1376590E2E20D91A012AF5556DE4F3,
	HttpBase_DebugLog_m280EFB4540C3CCB190B890A1631862150E991A4B,
	HttpBase_DefaultUnityWebRequest_m2CF8E2BFE0119EA9B76B4FF7896C5D47C29292AA,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RequestException_get_IsHttpError_mFF87E7CAB853D22BD0345719750444487D1736B5,
	RequestException_set_IsHttpError_mF5A40E18BE001DDEE41D4B5FBA9E0DED104F44EB,
	RequestException_get_IsNetworkError_m017D90CE96C6FA0165454BCD9C1E0BDC33044941,
	RequestException_set_IsNetworkError_m347A0E2913201C77B6D2CE644694F1BACFB9B43A,
	RequestException_get_StatusCode_m814AA8E99FD8EE2052D6224666D5092966E7AF05,
	RequestException_set_StatusCode_mADD8E870A5939F0BA68B47BA77BEEF4EE5556BF2,
	RequestException_get_ServerMessage_mE37BBA653D256A6D0F758463ACAD369939FE9623,
	RequestException_set_ServerMessage_m60F4D169E2007D87AFDD8D221B6555DC452DB8AD,
	RequestException_get_Response_mECD4188FFA6183AF6524C1A9D7427A61F498E0C3,
	RequestException_set_Response_m72C53532E5A721556BE396D4454127B2B43BA8BA,
	RequestException__ctor_mD06A4E9B88C6CF90EF35A84EAA108C119BE0CAD7,
	RequestException__ctor_m30814F6D6E2C849E3DD4B9F80EA517986AC34AB8,
	RequestException__ctor_m0D1B1BD0F895C3D46BE0044FD92DA7C583308EB1,
	RequestException__ctor_mEB51E7ADCFD3D9DD7742F99201CBE3FCB98DFC41,
	RequestHelper_get_Uri_mA0F26E3CEFC06E4296FD778AD134D28B6C9F2F0E,
	RequestHelper_set_Uri_m5084F40F17E97D15B9DBAFF2C5842F9C23F37125,
	RequestHelper_get_Method_mE7D65CD2E38840BAA7152E6C479505AB5785C239,
	RequestHelper_set_Method_m0271B5138A9F42F647934636114EF52554A2E135,
	RequestHelper_get_Body_mF61A9E742CC917DBE6D34AA01215FE275C538696,
	RequestHelper_set_Body_m2DA99D4521DDC5E9AFCC56A7CA17B1AB4B04F772,
	RequestHelper_get_BodyString_m653C16D59BCD18AA6FAF96D95E2BBDA61FD7A6D2,
	RequestHelper_set_BodyString_mE89CA3FA1126508706C73FAD358B0288775A2CED,
	RequestHelper_get_BodyRaw_m266FF4343D3F90EE294C25F9D6905B863E7E9BC9,
	RequestHelper_set_BodyRaw_m94F63910CCB1D202251DFB86C70634700280434A,
	RequestHelper_get_Timeout_m0217319BD8221533E25AF67D92BC37AE5D181FCD,
	RequestHelper_set_Timeout_m2CF3BE56E39C366C09A273CC723F758EA5D2432A,
	RequestHelper_get_ContentType_m86014A7728C445B47A8E1A48A58B1661FABEA809,
	RequestHelper_set_ContentType_mD3FDDEDC31A8590CEF738B93068413D24CAF3927,
	RequestHelper_get_Retries_mE6A90798726222C0C7852A068A3DE5EDC6ED3ADC,
	RequestHelper_set_Retries_m45A0714E13CE97BA7AD480FEB5A9A1075BC47425,
	RequestHelper_get_RetrySecondsDelay_m78A4C284E0282F6C3090BE1ED74D631EC4C850C5,
	RequestHelper_set_RetrySecondsDelay_m984D36BA1BF60CF8C800FA48BF2DBE44EDEF7A19,
	RequestHelper_get_RetryCallback_m4D106F45CFA6C9FCF7A348A02A61C5D8BB92220B,
	RequestHelper_set_RetryCallback_m2C497361D572027AB76B25C3639EFFB9BD923AE8,
	RequestHelper_get_EnableDebug_mB32C7653514C62DF73CCB037B8FDA79246B1DBDE,
	RequestHelper_set_EnableDebug_m806F36DEF5921A1AFE2A33BDD0A7D222CA0F5B13,
	RequestHelper_get_ChunkedTransfer_m622B618F6755E8A9D4CE9BDDC2150AA2B9F6FBE2,
	RequestHelper_set_ChunkedTransfer_mBCCD30B3D6DC5547F0DCA5A22059D01B73BF1DD3,
	RequestHelper_get_UseHttpContinue_m7645253CE5AC91E02D6C3D812621A8A3DD1A1289,
	RequestHelper_set_UseHttpContinue_m87D10B37D75B5A2A5AE462E217D9CA1675E02B0D,
	RequestHelper_get_RedirectLimit_m488E83B4F9D57B43FE5490BF244F3ACD9E604480,
	RequestHelper_set_RedirectLimit_mDE3645EC1C530E6F37EE6464A29F2D98DC6286F2,
	RequestHelper_get_IgnoreHttpException_m1482402406DA715D76A3B25CBE750F5F202915A5,
	RequestHelper_set_IgnoreHttpException_m92B08A788BB48F93F0D4392D4F62D4346889EE4E,
	RequestHelper_get_FormData_m0207D1A53E755E7031AC4C4B5B8D511071F4F478,
	RequestHelper_set_FormData_m2A30257174CF3BF3CCD3B1CAC82FBA1084D38AC1,
	RequestHelper_get_SimpleForm_m7238AB9746C2CC01E8983C21BC186BB7F5A4774E,
	RequestHelper_set_SimpleForm_m212A29BA698C9AB66D4D960A704F73D9EBAEE8D6,
	RequestHelper_get_FormSections_mA3311E750CEE3519DFB762FB959EFB316F12714B,
	RequestHelper_set_FormSections_mDBBF28D05F7B52B8AD569F7CDFAD521012E3151F,
	RequestHelper_get_CertificateHandler_m7D5D164ACD676DC4B29297D75A4EC3E191F9A5E5,
	RequestHelper_set_CertificateHandler_m384C483B84E223958C99282C06F6E27AD9EEC9A8,
	RequestHelper_get_UploadHandler_mC39A192F8F8205937A2E3429A3585B23C6179815,
	RequestHelper_set_UploadHandler_mB7B0E07D289B2A7B2D2BDF0F541915906640CE91,
	RequestHelper_get_DownloadHandler_m16A51AA814F360D0B86C9AD5AB1487931049603B,
	RequestHelper_set_DownloadHandler_m8A008CEFD7D4CBA555D1248AD1A5DB75A58EA5E6,
	RequestHelper_get_Headers_m000261B804D7400A61C788013FC41EA0CFEFB8CA,
	RequestHelper_set_Headers_m28FA528C86977AADCBAB150BB0E4C5097CAB3A08,
	RequestHelper_get_Request_mC869E47DC8EEEFF9894D23786D2BF266FAC6E254,
	RequestHelper_set_Request_m85340DEFF640624B8BE3C17264140C7A5E3A1847,
	RequestHelper_get_UploadProgress_m58689E2655FC874DD9F425F8A867941FCB57D954,
	RequestHelper_get_UploadedBytes_m8025EF075FE7C9EDC130533C24202577F7C6E208,
	RequestHelper_get_DownloadProgress_mF13E879706FDABA0E0D1877BD23FB23BDCD8C144,
	RequestHelper_get_DownloadedBytes_mD168B4F5340F315E0CBD35649A8E7FF4F9B6672D,
	RequestHelper_GetHeader_mFA543B7D28A98472859DC8AB7F78DDE6C63C2703,
	RequestHelper_get_IsAborted_m4270E2F4B4A1A53CA564C80C107BE4FA1EF5F381,
	RequestHelper_set_IsAborted_m7E6EFCC0F4F6D8E0ECBC2645F38EF5E2F3965C37,
	RequestHelper_get_DefaultContentType_m11ACAF07CCCC9C0F8E9A3DE25BA5AA2A63633190,
	RequestHelper_set_DefaultContentType_mAB4BE4E5E5777C5E8DC1EF0A09A99EC22499E8EF,
	RequestHelper_Abort_m272B799532ECD8A4C921504A053B16103F1B0735,
	RequestHelper__ctor_m7D4C8C4EB6C100C23A9F5A494B9FF5B7D287FAB9,
	ResponseHelper_get_Request_m7D2000B276199E7012E28DF03E784C7862CFA55B,
	ResponseHelper_set_Request_mB82C191ED94F1BCE8E5C2D65CC9F1045676362A8,
	ResponseHelper__ctor_m2045EE543D8AAABC9480B8B39A64BC1946F635EA,
	ResponseHelper_get_StatusCode_m130E1F760BE225F2CC4FEF905D601EFD60CB75E7,
	ResponseHelper_get_Data_m68A42948C0B93963C8344E7B978066347D38196E,
	ResponseHelper_get_Text_mA285A72FBA91A37ED55E48088DFE8E53EA3B733D,
	ResponseHelper_get_Error_mA0B2D9B4B18E045BAB68FCC26FF28ADF8BA69960,
	ResponseHelper_get_Headers_m6E5EF8D44E4EBBF6D1E378B9812C79FCE8D64503,
	ResponseHelper_ToString_mCC1393BA950333F550C38F397F49E08A32967DFA,
	StaticCoroutine_get_runner_mCDEDC96828F412E908516930AA0A07628AD45941,
	StaticCoroutine_StartCoroutine_mFAC8FFD7E197092660CF9CD8C31064F96B0CF0C8,
	RestClient_get_DefaultRequestHeaders_m2F0D8796DC7CA980429E020C9AA4EA6255C7E387,
	RestClient_set_DefaultRequestHeaders_mE3F99AE2A63B087395ED82D30E2BBDC986376B0C,
	RestClient_CleanDefaultHeaders_m7AC65C774FD9AA128697A8EDA24D6588A472CC82,
	RestClient_Request_m72295846A2779920152B206CE3B3CC94D124B154,
	NULL,
	RestClient_Get_mDD2DAC2B3E217CCFF9ED88DAA9DE1FB6A5A755C2,
	RestClient_Get_m704A16C01784F9E871CA46DFC0524D49BE0D799A,
	NULL,
	NULL,
	NULL,
	NULL,
	RestClient_Post_m61D63B57FBF0B4553D01AFE74CC20EEE83B9B66C,
	RestClient_Post_mA52CAE06D96B1BD156639CF4A5CEF34E8047EBC9,
	RestClient_Post_m26AF6D3E2CE5DBCB3F1E96B0F72390212038BD82,
	NULL,
	NULL,
	NULL,
	RestClient_Put_m39AC49D20CFB40AFD2539092D36836F7EE59B539,
	RestClient_Put_m49CA1B70D92D456B5B49453774638D2FA6BD593B,
	RestClient_Put_mA25CA5AAD568FBE40F0A502A7ECE03B5EDE78861,
	NULL,
	NULL,
	NULL,
	RestClient_Delete_m283D55CA7A7929275D1EAC7C030CD2B14620CEFE,
	RestClient_Delete_mD10497E9FC59164F3B0CFEBFBFB6B18D181CB869,
	RestClient_Head_m95150CFFFFC7CCF920E514E6F36619A9F2F31084,
	RestClient_Head_m9D3F8FCF9EEA02E9FF5EEB33800D8CB3B5607FF6,
	RestClient_Request_m0BA554BC9023E4726CE1777F868B0FD55F623206,
	NULL,
	RestClient_Get_mB7C87B45B0AC6B96336314E2C6F9BF16446565A7,
	RestClient_Get_m0D9E2C1EB049974F78BE7BA8F92063BC842AAB88,
	NULL,
	NULL,
	NULL,
	NULL,
	RestClient_Post_mFA0B3D897E0659CD912BDD688EE11EBDF486EBA6,
	RestClient_Post_mB2294872D87C70BE710630EAF8ACB38D97EBC045,
	RestClient_Post_mC632B22BFA78D0B3C346CC608EAC730173AB69B3,
	NULL,
	NULL,
	NULL,
	RestClient_Put_mDA697668DBD06112E4E5B05C4741DD46C76387A4,
	RestClient_Put_m0AFED5E547B0CD8E5093BF2A31FD015C7C63B890,
	RestClient_Put_mF3DFA649A055F6C9B3ADB56226F63089F279DACE,
	NULL,
	NULL,
	NULL,
	RestClient_Delete_m0D116A07266EE3536822E2635109915650AC471D,
	RestClient_Delete_m6AA933C3F377A10BBB0E7E7A848B2A53C7AEFD8D,
	RestClient_Head_mAAFC13D98D744BB34E2E062C1D2F0254B7F12C07,
	RestClient_Head_m559D8243A556974E9D847DB3E3128F051A666564,
	NULL,
	NULL,
	Common_GetFormSectionsContentType_mE6E8DB71813E34A714D9EB6DF09F82CA9923566E,
	Common_ConfigureWebRequestWithOptions_m0B52F2B99763C3F647A5905EB223C4D2C95108E9,
	Common_SendWebRequestWithOptions_m3B484321BD8529D5C5663E99619D00F542AE0D74,
	Extensions_CreateWebResponse_mCBE6A32B2E275A121212E0BC04B33455D3252BF9,
	Extensions_IsValidRequest_mF5A8D8632EF8A666305D1CE76DA0951D86ADA759,
	Photo_ToString_mF42702D13A2A3D85EB60023A1B9E8588290B8C56,
	Photo__ctor_mDEE2043FEDE45230489EC99DB57D76B42F97BDA1,
	Post_ToString_mA7FAD7DD3CE9A69561D6314A19190F103DB0B3ED,
	Post__ctor_m94FB3E59A55B159FA9B3C5E3A6592813AA0660A2,
	Todo_ToString_m9AD1D2F35055C98E42FC04A6696AB0C2455B1972,
	Todo__ctor_m2D95DAA38846943DC4F5D7D5B6954C9055487C49,
	User_ToString_m1B4F9151670216FFDFC95734956CF9B33BA6093D,
	User__ctor_mD17E5AF774F035FE0A0F26BE4C757AF27668760C,
	LeanDummy__ctor_mAD9437EB3765999702C790EAC2DE9FEB4442E092,
	PathBezier_OnEnable_m56E18FAFF5DFB0ED61856F90EAFEF6BDD116E2DD,
	PathBezier_Start_mDAF11682189F6675D4FDD93D652A898A3347B50F,
	PathBezier_Update_m3CC2AC4CACBB72CE7640DC38DFC4F19689EAB564,
	PathBezier_OnDrawGizmos_m345FFC89DBF05FCF81AA56654D2764EB6199F256,
	PathBezier__ctor_mB9E6AE4BEEE953C926C93944334E5F099FC262CA,
	TestingUnitTests_Awake_m3084D275E0537EAC8B54FE41937CF217BB332504,
	TestingUnitTests_Start_mE8442EF073840E58B91FD7047EC0D8708D70D928,
	TestingUnitTests_cubeNamed_m46762B53A8B1B8EC91E95B1B886E0BEDA7F59270,
	TestingUnitTests_timeBasedTesting_m5510CA65F550F87AAD701A457ADAF778B46B5758,
	TestingUnitTests_lotsOfCancels_mF5CDB314DC950F82FF777A507914DAA972E57D08,
	TestingUnitTests_pauseTimeNow_m038EABEB7FE1D5E01BAD36F26EDAD62253864249,
	TestingUnitTests_rotateRepeatFinished_m75177B46DFB956A9AC0870AC6E1E767A09E41A65,
	TestingUnitTests_rotateRepeatAllFinished_mE2C3707783D21F0E24E390907EC41AAD46537D53,
	TestingUnitTests_eventGameObjectCalled_m696EE506C9313CDC2955B76C394C7D8DBAFA10D6,
	TestingUnitTests_eventGeneralCalled_m2ACEC83AC54AF7D79C1BFB26F9D63F2201886540,
	TestingUnitTests__ctor_m1551B3D3BCD78DA068595A4F9CA1934061711DA2,
	TestingUnitTests_U3ClotsOfCancelsU3Eb__25_0_m22BEE01EA5BDFFF8A1DBF6DB397911D91B74FABA,
	TestingUnitTests_U3CpauseTimeNowU3Eb__26_1_m4F3F229D6D22317DC30FF68B332B22A3CB6CFE68,
	LeanCameraMove_SnapToSelection_mCF45F65928E94714CCC18F2B99A11C94ABCEAADD,
	LeanCameraMove_LateUpdate_m8199A04A20DD363CEBF44F42AE447273255B17F2,
	LeanCameraMove__ctor_m9D8AB4604EADE8E50CE3189CE0F523456C478E52,
	LeanCameraZoom_ContinuouslyZoom_mFCDB903A5873AF92EFB4EEEBA092D390ADF00DC3,
	LeanCameraZoom_Start_m1D63AD2606F070894C08C5A5B93C57263620B9B1,
	LeanCameraZoom_LateUpdate_m9A35BCE30EA1C6668BFD25605C378762E41E5268,
	LeanCameraZoom_Translate_m503BC4885B7298971844A7C93ED6CEC2EA752773,
	LeanCameraZoom_SetZoom_m4D4A8D5C5518F93B51007EC5F6312A2C1695F11B,
	LeanCameraZoom__ctor_m218380820F05FE77DF33F3CBA47808275641BAAF,
	LeanCanvasArrow_RotateToDelta_mA75E4316B72DA3E69CDA2D18226788AF2F3242AF,
	LeanCanvasArrow_Update_m6E5E83648EB353C309E72A2BCD1884767210F056,
	LeanCanvasArrow__ctor_m86D6586CFE6221C16D3AE58A5374D3E27145F877,
	LeanDestroy_Update_m96BE8B43BD546EB19CCDBE8DF833945E42E49DB8,
	LeanDestroy_DestroyNow_m05F2B50717B2FFB345324A847F44E7585AF13DDD,
	LeanDestroy__ctor_mAF42A9F0629FE42B0C0B2890E3368419ADC876B2,
	NULL,
	NULL,
	LeanFingerData__ctor_mD98951F55492E2F926528BCB4FA0C4AC4A616D4F,
	LeanFingerDown_get_OnDown_m01F417F5AD2ED8DF51C64410A8F4E9469C77FC2A,
	LeanFingerDown_Start_m79C6D331F0908B3C444D05687B9F9039681E6596,
	LeanFingerDown_OnEnable_m7534989DC473AE33B0D633EB0D8F813EB06871CC,
	LeanFingerDown_OnDisable_m2C8843299E5D451A0453F2DA3CA4B98BAC849E5D,
	LeanFingerDown_FingerDown_m9A872EE6C7A89E5B2E361BD7668BF390C7CDB809,
	LeanFingerDown__ctor_m71B424F8281F46219ABA7D3DD9684D6BC375074F,
	LeanFingerHeld_get_OnHeldDown_mDCBD079B5AD9F887A793638962503E4C930E6ACB,
	LeanFingerHeld_get_OnHeldSet_mF422114A9A1F28F8566C4BD76428D9965D755FF5,
	LeanFingerHeld_get_OnSelect_m864311026D38721DB0AC470CCA486C756DB0576E,
	LeanFingerHeld_Start_m2A5D07E7E0C66AB5CA95BBA038E3226667F4E292,
	LeanFingerHeld_OnEnable_mEF1F4AA2EDFF4CB1AE72D06B4799F89B8A91DC5A,
	LeanFingerHeld_OnDisable_m8DBB4B94F213878ED999DE062A9E241F8C84B8E7,
	LeanFingerHeld_OnFingerDown_mC9774E22CEA56124942EABBEDC9D7DB12B64362B,
	LeanFingerHeld_OnFingerSet_m0EEA11DC109D56A57EDBE8036A6D0405CDDB64D7,
	LeanFingerHeld_OnFingerUp_m02CF2279B2AFF7227D774D69CD806BA647C205F6,
	LeanFingerHeld_FindLink_m86986ED6CBAF01EC93D4ACB36C8EB465DDA631EF,
	LeanFingerHeld__ctor_m371FAB44F0431B9A5682CE7556375825A513497A,
	LeanFingerLine_get_OnReleasedFromTo_m5534DEE35985E3E7D16989063071696B2276675D,
	LeanFingerLine_get_OnReleasedTo_m94D7EA4A1748ACCCD93E4FBDD126CD27229D809D,
	LeanFingerLine_LinkFingerUp_m15040ACA7A6EA5618BE4165AC34F0AABFF1A471E,
	LeanFingerLine_WritePositions_m7E3BCC076B9366D51DE2B8C5F2B69B3AF3D39EF2,
	LeanFingerLine_GetStartPoint_m6F9BA6663B2AB003DDC6128E81ABDD2D4B9635C5,
	LeanFingerLine_GetEndPoint_mC2BA5AAA44BF50468839BA5D48CCA55E14E5E321,
	LeanFingerLine__ctor_m90A3A891A197E42FC707797E53584EBCA9C43AB6,
	LeanFingerSet_get_OnSet_mB08E734D9EF0666360880A2AD833189F42BBF5EA,
	LeanFingerSet_get_OnSetDelta_mCC630467A7745C06FCDE46088F2EA3485E17CEEE,
	LeanFingerSet_Start_mC5A683576EC2E7A6B797469265D481CE4455E9EA,
	LeanFingerSet_OnEnable_mEE8D62914B914A5C364735057E16672F91BEDCAF,
	LeanFingerSet_OnDisable_mF115EABA7CD0D95DB2FD232079B4368C9FDA7E9E,
	LeanFingerSet_FingerSet_m9613C2BFAA7ADFEBF3117288C9A639497D6BDFF9,
	LeanFingerSet__ctor_m8E66E7826E3B8905B3A9BEA6676CAC4A5F6033FF,
	LeanFingerSwipe_get_OnSwipe_m1CBD2D067DC31447D8FE3273D0A3CBA215D62864,
	LeanFingerSwipe_get_OnSwipeDelta_m5263E0F14C0AFBF56B27AC836CFAFB2D9A2B4E34,
	LeanFingerSwipe_CheckSwipe_m57B659175B45E5789F4AE4A18DC800A4BECCE185,
	LeanFingerSwipe_OnEnable_m505603566376B408AA7117659729C15B262501E7,
	LeanFingerSwipe_Start_mEB69138725B360D34EE6378A445000227C5EB606,
	LeanFingerSwipe_FingerSwipe_m947DCA328E5B8695AF8D826574C7124EADBBE2D8,
	LeanFingerSwipe__ctor_m6A48ED5E0A22AE4E4E7196A25670CCB747A6A512,
	LeanFingerTap_get_OnTap_m7D397EDB21F2C2AF855702D02EFF4ECC620BE4A2,
	LeanFingerTap_Start_m3A4EDF8069308719FEAB9AB1097CB81E49BC4DDB,
	LeanFingerTap_OnEnable_m683BDD1CFFE951877D19858670DA30CFBD49BE6F,
	LeanFingerTap_OnDisable_m6F68FA245FDC28B8C650F37C647867E334A4C3AF,
	LeanFingerTap_FingerTap_m9D40017293211FE06102396B8AF78F4FB80FDDE7,
	LeanFingerTap__ctor_mED989C7A98A2581454D6D5A9CCD506DD8C182195,
	LeanFingerTrail_Start_mA1E93A0C76592EB205D7D497CDE2A7176310D2FF,
	LeanFingerTrail_OnEnable_m4564F591C913E426F842E99119BAB012053B0BCA,
	LeanFingerTrail_OnDisable_m0D041A6FCEFAC46478BD114166167E332669217E,
	LeanFingerTrail_WritePositions_m24F8F3789AED66AAF453BC1EE2C170B6C1C47C50,
	LeanFingerTrail_FingerSet_m8E016D553A70E984F092254E3A14AE5621FA5E65,
	LeanFingerTrail_FingerUp_m79F07D80D5E57681A5B82D93C8D8AE69790A0E8A,
	LeanFingerTrail_LinkFingerUp_m508B6D2383E5E2AB1BD9F6EBE2703C60DCD19103,
	LeanFingerTrail__ctor_m858BECF14573DE145CE58137DDEFB7EC60D6179B,
	LeanFingerUp_get_OnUp_m72D51BFBD27576DD1BB89E6EEF8FD343F0D0B57C,
	LeanFingerUp_Start_m270CAA8585511FF787C38EE206F884B48293E11A,
	LeanFingerUp_OnEnable_mD0B304DB1C2BDF052FB8BF3A11CD9E7AF7B217C1,
	LeanFingerUp_OnDisable_mAB9285F0A342A4C57F6576057127D47811A4725C,
	LeanFingerUp_FingerUp_m5BD8E7AF8F3C42A010B3EE0101F6F59FA760E89E,
	LeanFingerUp__ctor_mA53E151EF5555BBEFB6306882E46E2ABC459C15B,
	LeanOpenUrl_Open_m41B624E57AA32174314ED36111FE95EC2D0E2D07,
	LeanOpenUrl_Update_mA1F50D91BB67CDC4DCE1E556FEBA58F4BBB1718D,
	LeanOpenUrl__ctor_mF8D82EA66ED45F7AE0F46951139B338693F90512,
	LeanPath_get_PointCount_m9045E672BD2FF353999596E6E261270513F746D2,
	LeanPath_GetPointCount_m31A0FB489FA54C2630C7A8310CFE22143A57E8D9,
	LeanPath_GetSmoothedPoint_mC90B314DB35373DD33ABFA20E4674BDCA6B75486,
	LeanPath_GetPoint_m20E43752CA69EE2701E20265DD15C17478915F97,
	LeanPath_GetPointRaw_m2740E3BCBCC28875BFC13AADB9CD569F9A49357E,
	LeanPath_SetLine_mCA222133037FBD6E37D167DE7468C283B3656716,
	LeanPath_TryGetClosest_m603575984CA3AF91D809EDDDCE7720BC3DDC2695,
	LeanPath_TryGetClosest_mCBAD5EF228A7EA7B179312CA0AA6384DECCE854A,
	LeanPath_GetClosestPoint_m1EA337BBAD860D0647F1AE0025125CB3484A9B56,
	LeanPath_GetClosestPoint_mCFCCBDC61362E4092AE6C9DBF91608F8D12F7B5A,
	LeanPath_GetClosestDistance_mFFED298C41C661C1301C8F7A6A8EDE1B0B94F611,
	LeanPath_Mod_mF6965EBB3AE58BBD2086ED85352B9BADDA9ED8E6,
	LeanPath_CubicInterpolate_mAA20DA9E3C90657AF2AACCA1359BD6CC294CB5CD,
	LeanPath__ctor_mCF6DCDFBB71DFF5C0EE11C9DEFBB7F64DA6ADD2F,
	LeanPath__cctor_m000BACFE90EEBE48EED93CC532C21F00072513EF,
	LeanPlane_GetClosest_m7683693B13E0F8118E395660C6BF8B6A55F09BE1,
	LeanPlane_TryRaycast_mFE7C5228953B5803B2E2AC56AE723AE15C93F986,
	LeanPlane_GetClosest_m7CC90569CD40DCAC36CB4E236DBD9D35134BBC69,
	LeanPlane_Raycast_m6DC4A90A167D648C34B7C8B2A0FFA625F5E2771D,
	LeanPlane__ctor_m193015630E686140C5E7426BB91742AFD82FD881,
	LeanRotate_Start_mDC240D0B3C7D54B1A891BF3A9A3C1C884AB13152,
	LeanRotate_Update_m3DD0A6274590A5E2B94819E61D2E37F648FD38AB,
	LeanRotate_TranslateUI_mC4004FF7236DCE86BA564B899706468AE850CE49,
	LeanRotate_Translate_m4DFEE2D4E93F934F9436404BE570EA53C49977A5,
	LeanRotate_RotateUI_mF97E20F6A11F4F424443B77BDE05D1A65BAAF891,
	LeanRotate_Rotate_m69CA06783A10450AB613081EA2289EAF0AF249F1,
	LeanRotate__ctor_m9CBCD0467270F8099F3B9435242A19252F1212F3,
	LeanRotateCustomAxis_Start_m6CB431470B1165A30548BFD7C0969862E822C52C,
	LeanRotateCustomAxis_Update_mBB7052DCA8F2ECCF03654C53C81DFD358F0E1816,
	LeanRotateCustomAxis__ctor_m14D55B95AB5EF12807C40C6FBD0F2138913BFC33,
	LeanScale_Start_m09D3E4BE4BA85EC37B2056BD577354FECB69F742,
	LeanScale_Update_m7416C2BEAA82D5A9C9913F85D07ED6BCD586B11F,
	LeanScale_TranslateUI_mAB9C06AE8C10C9CEDE09119FEB054F857CA38E42,
	LeanScale_Translate_m85A244A9143F0C3CC19CBCB5CDCC162035F85E89,
	LeanScale_Scale_mF1E6B056A9049302D6D31CA2410F3161C3206532,
	LeanScale__ctor_m0C60988D6FC39475549BD3BE8F7943D2540C6EA2,
	LeanScreenDepth_Convert_m17FA6C55BC020C621949587EE638A6119BFDF5BB_AdjustorThunk,
	LeanScreenDepth_ConvertDelta_m7B2487D484E8A5F5A72A1E105B804EF1C59D1517_AdjustorThunk,
	LeanScreenDepth_TryConvert_m2578623A359392A87C58035883C3D7E2933BF9E5_AdjustorThunk,
	LeanScreenDepth_IsChildOf_m034EC0E242D644C8332165A4DD8E72F44533B229,
	LeanScreenDepth__cctor_mBA240A88458C9D0F80FF060BDDD4EB4A5861FC03,
	LeanSelect_SelectStartScreenPosition_m1EF260E5A794918BB3F6CB0533FCB7E5EA6B251E,
	LeanSelect_SelectScreenPosition_mAAFDAE9F146DE1E568F753EC4D080BEB639C145F,
	LeanSelect_SelectScreenPosition_m9879B433CDA1B04E751186E1FAD3B338D781B311,
	LeanSelect_TryGetComponent_mDC20B49902DEDE331C20CCD5764F460C975AB4D1,
	LeanSelect_Select_m60F8F5F4530958F5F66F16F15A1480A65BC8A143,
	LeanSelect_Select_mC1830C9BE70AB16D1E0723C0C41EF81308B54439,
	LeanSelect_DeselectAll_m1B290CA1890634909F924CC8DC79131C0866DE9A,
	LeanSelect_OnEnable_m167EA7A97777591DB6E9E96F891523E127122DBB,
	LeanSelect_OnDisable_m248827E8DC9C4826013BFB3B054E381373C9AD2B,
	LeanSelect__ctor_m2805372C7A136AC720229354F6EFF9B5164F9EAB,
	LeanSelect__cctor_mF8DAF1D094796D8083F49ABEF1DA8887C5AD94CF,
	LeanSelectable_get_OnSelect_m5C9024D82B653F01DE73B8B0A090CC3929D41F30,
	LeanSelectable_get_OnSelectSet_m27616098FCCF6DFFC7A9B0E0089D0CBD00D43261,
	LeanSelectable_get_OnSelectUp_m817AD57D6B429B409CC79190907F575E9432FFDB,
	LeanSelectable_get_OnDeselect_m7C7EDEA2304E185949B9C074136D136215947EEF,
	LeanSelectable_set_IsSelected_m517C73DEC280EA11BF65BCC493640771DCA27826,
	LeanSelectable_get_IsSelected_m0E95E231CEEEA2368306D5D05AFE9537D67FD99D,
	LeanSelectable_get_IsSelectedRaw_mB36FF9B2F6526FF3DED4E82A9BBB226AE71206FA,
	LeanSelectable_get_IsSelectedCount_m9BD8C3ED56E289D504F871B04B9D3AAFCFF40376,
	LeanSelectable_get_SelectingFinger_m5D91B7F92F5CEC495CC9892458F40C0A9314C9A9,
	LeanSelectable_get_SelectingFingers_m48C74C7A3EB4F4F52423B546EABE3913FD196ED0,
	LeanSelectable_GetFingers_m0D64345B7E411E38DCD5BC02B7547F49D04C8DAC,
	LeanSelectable_Cull_m8EFC431440561FA762B70BB11FF8731580B3EC02,
	LeanSelectable_FindSelectable_m5C62AC00DB6109196486CBB558B92DC0A8C81149,
	LeanSelectable_ReplaceSelection_m50AA185BDC77E90F11DEF0DECEFE1340AC746D67,
	LeanSelectable_IsSelectedBy_m2BC0DEF99EA824C4CE6FC8907CDAFA6246342087,
	LeanSelectable_GetIsSelected_m1410B16332E61C7EDB9D065F36ECBAC9AE8A2C83,
	LeanSelectable_Select_mAC1825426C13E9054BC04C5D46165E9257DCDD17,
	LeanSelectable_Select_mD9F5699A7FC88903C3EA3AEA7A1F1BAA795EA1C5,
	LeanSelectable_Deselect_m44C95DD63A74584A8A8E63EC9AE23927EEE63019,
	LeanSelectable_DeselectAll_m3D6B37079735D596BF3EF9AE9A3669939D23C8BF,
	LeanSelectable_OnEnable_m663499A5FCF163550A4BAF9260DCA3C45947649F,
	LeanSelectable_OnDisable_m5613E5067F8A2BADCD01BAC8D0F4481CE6A07AB0,
	LeanSelectable_LateUpdate_mB01A4277760B945B6E90969535B02EFEAECC3DC8,
	LeanSelectable_FingerSet_m5D922C3E1AA78E4269BEC4ACCC3B90C83F50FAB1,
	LeanSelectable_FingerUp_m25AFEE6D4A6B95A7D3A0FCE605AFFF2C4AED3F6A,
	LeanSelectable__ctor_m3097F83B158F3D4D1456604CEFC2E13A070202FB,
	LeanSelectable__cctor_mAA297FEAD5E6134B121C5CFB51FF00A9898C169A,
	LeanSelectableBehaviour_get_Selectable_mBDF53AAD90E9A4FB7BE1D0EE9B84447D7E6450FF,
	LeanSelectableBehaviour_OnEnable_mB2BE80ABC4CBAB9C399E554CE712F5D4C224B105,
	LeanSelectableBehaviour_OnDisable_m2C1F94BA33165DF721FBACBB5D3FA57B342AEC73,
	LeanSelectableBehaviour_OnSelect_m9BA3A1147A76D5172648ECFA90449B86B83F7145,
	LeanSelectableBehaviour_OnSelectUp_mE691446CBD739570F9A45B99CFFD2201C62EF2F5,
	LeanSelectableBehaviour_OnDeselect_m0D5370300D2FE51038F8EAEFCAA68C29C011BD40,
	LeanSelectableBehaviour_UpdateSelectable_mF9E3FA1C93F2DF075EEAA0CE9F859EDFAAE107D2,
	LeanSelectableBehaviour__ctor_m995A2C7ADD6DD197410310E270424723A8FD6F77,
	LeanSelectableGraphicColor_Start_m98DB08FC0228058861063846BE895292415FC3A7,
	LeanSelectableGraphicColor_OnSelect_m82AED3F8866350F39A61FEE6AF4D0DDAFDBCC127,
	LeanSelectableGraphicColor_OnDeselect_mFC78EDD601DA200C17B7F51CA692FB47A3054F83,
	LeanSelectableGraphicColor_ChangeColor_mC7D163C985960AD1C3FE21C2475BEE2FED025BC8,
	LeanSelectableGraphicColor__ctor_m0316DB094111DF284B397AB57BF984023EA0E52E,
	LeanSelectableRendererColor_Awake_mBBCF64F8E2A781456CC31A7B78B448702BFEBDC2,
	LeanSelectableRendererColor_OnSelect_mA7E55C46C0E916C3F36E244C29B16B37F6555D3B,
	LeanSelectableRendererColor_OnDeselect_mC7F5806A8311EF2DF68386DFC9689E38DEF4A5EA,
	LeanSelectableRendererColor_ChangeColor_mEE38535524FFA325D44EA70BED83E7DE9CA3F4F3,
	LeanSelectableRendererColor__ctor_m90261910773A2F8AC48269B9804FF18BDB231C2E,
	LeanSelectableSpriteRendererColor_Start_m1F115B54BC5C928D5BF7A57A3B504CA8693168FD,
	LeanSelectableSpriteRendererColor_OnSelect_mA90FDB561F0522CC065A2204FDBF7DAACEDF4490,
	LeanSelectableSpriteRendererColor_OnDeselect_m88752CD3DD604BEB223FC10B9CA8062D52667672,
	LeanSelectableSpriteRendererColor_ChangeColor_mD2A6F9007453DE9295508B98271D05EB0A9C3F2C,
	LeanSelectableSpriteRendererColor__ctor_m3EAD21A8B36BC85E518BB41128E8F952A900F038,
	LeanSpawn_Spawn_m7E653322FDA83F6D2B7264B238D952C65A1B86FB,
	LeanSpawn_TrySpawn_m72FE5AE72230C5915D61F1FA5DFB41955CF8CB4F,
	LeanSpawn_UpdateSpawnedTransform_m75E8A403823654A949383440E4636C78F18BD223,
	LeanSpawn__ctor_m7D5D14262983BCC8E46A2C2F6C3682AE64A21CFB,
	LeanTouchEvents_OnEnable_mFE78D00228E54D211EE9B533D5C23B83F528A0F2,
	LeanTouchEvents_OnDisable_m4D6EED9A5EA60C3D4A556581C9A339FE05C9150F,
	LeanTouchEvents_OnFingerDown_mA543BE8CCA72241C2200977CB2A50AB848E9E4A5,
	LeanTouchEvents_OnFingerSet_mE21B49D33910ABAEEE9B6F5E6CC9FF13B027E8F7,
	LeanTouchEvents_OnFingerUp_mD90E32290811E523013D8D275938BA349148490D,
	LeanTouchEvents_OnFingerTap_mCE1D9C7A44602C5F854E119D77D8C2B10956EFAA,
	LeanTouchEvents_OnFingerSwipe_m2516A40BB5C4DB78CA09DCDB43A020EE407C4A4A,
	LeanTouchEvents_OnGesture_m0E568D97D5A178870644E691AE53E30A24B700D8,
	LeanTouchEvents__ctor_m982C62FE308EA74978D38336B2B5C3D1B8625598,
	LeanTranslate_Start_mBFB6F3AD46E747C9F2EB77D21282844E262C87C1,
	LeanTranslate_Update_m49730E2C618A017F541B65FE7922A0C8290EF92D,
	LeanTranslate_TranslateUI_m46365A1760FE0399C813EB35AC3A7BC21DE85F89,
	LeanTranslate_Translate_mB2A5748D8874FD8B43E29208B819DE4106105629,
	LeanTranslate__ctor_m3565AFF03C61DC548137F6D676719D1D55F9296D,
	LeanFinger_get_IsActive_mED08E1A0FE9E5BC0F4E9414FE8123CE4E933EA1A,
	LeanFinger_get_SnapshotDuration_mDED148AEBE2DE6A6572B8D803D054C2DD2F78476,
	LeanFinger_get_IsOverGui_mFAF99A6D9105F1B786CCEF8F2B49482203EAC860,
	LeanFinger_get_Down_m1805E57A7EA45C1614F0F48D094F6CD8BFE473A0,
	LeanFinger_get_Up_m25EAFB017A1CDBD559DA0A04A8CED5DAFF9F00FF,
	LeanFinger_get_LastSnapshotScreenDelta_m6BABA22BA9D64BB18B275DE986B70226ACD35A78,
	LeanFinger_get_LastSnapshotScaledDelta_m779031A0C31B6679ABAA9728473F75753D02CB32,
	LeanFinger_get_ScreenDelta_m425001057EEA27D3A688D4F7A6DB9A0F0C5F9AAF,
	LeanFinger_get_ScaledDelta_mFA2EE55C42279487140F746561DCCF65B4E71556,
	LeanFinger_get_SwipeScreenDelta_mE93F503A6938BDBA414E4FDD966164348395A55E,
	LeanFinger_get_SwipeScaledDelta_m309305C36E8E716951EC2B69455A0D7A14F7F540,
	LeanFinger_GetRay_mF6E2731A4B05E6F4F4F046047AB1E4E6AE8C5622,
	LeanFinger_GetStartRay_m5C7D7FB8817B5758D0E407F8B7841AF4E3A296FC,
	LeanFinger_GetSnapshotScreenDelta_m0C9165D27D71D373CA93233235F108D097125BC1,
	LeanFinger_GetSnapshotScaledDelta_m132FCF360F231F0CEB40EF6F784C31A75291D902,
	LeanFinger_GetSnapshotScreenPosition_m92274CF51619130B789E9C79B63D815280FA67F2,
	LeanFinger_GetSnapshotWorldPosition_mBFB7D50E8575FCF1A55EB8E1742671CBF360C66C,
	LeanFinger_GetRadians_m0A483C7927A7509EA561667A82588D782E8F9751,
	LeanFinger_GetDegrees_m06D7309A14F837C7C0FD0B10F4901F57FBDD0FF8,
	LeanFinger_GetLastRadians_m3996A159E8B08111844E7F3CA60847B7042725C6,
	LeanFinger_GetLastDegrees_m5E8A26889D1908FEE6DAFD5A67547882A658D137,
	LeanFinger_GetDeltaRadians_m6C3C6021D48DB3E237672D0171BF9EBF44036CBA,
	LeanFinger_GetDeltaRadians_m876E8B4F503EA84BCDF7021F30BD4A4DEE045AFB,
	LeanFinger_GetDeltaDegrees_m495E1B8C20C0DB6E3D189896336419DFD6BD43E8,
	LeanFinger_GetDeltaDegrees_m2CE7770F72FDC57C6F3EB5B0E5CF5CBF3B6D7BC8,
	LeanFinger_GetScreenDistance_mD33CA7C4AD147F33A3D339C246EDB9DAC22E1DD5,
	LeanFinger_GetScaledDistance_m311DABF41EEA19DE3225F103E97E2D4DC4348C63,
	LeanFinger_GetLastScreenDistance_m209656E8E8BF4CFFD4DC1F4DCBFD1FF7675291A1,
	LeanFinger_GetLastScaledDistance_mE6C9C22AF38BB1D9C973CBB3796C34D704EB9965,
	LeanFinger_GetStartScreenDistance_mE210C3037748F2AA73BFE54C130C7D14479D0E59,
	LeanFinger_GetStartScaledDistance_mF9031456E65F7025048CFC6346AF4C890EDAEB3D,
	LeanFinger_GetStartWorldPosition_m3A354623C2E4CCAAD745808E709DF99F6D2E441B,
	LeanFinger_GetLastWorldPosition_m517937BFACA47CA557AA262853EEEAC890B96F21,
	LeanFinger_GetWorldPosition_m0783C1FB5C3F6ABAC425A3580782749F0F6102C9,
	LeanFinger_GetWorldDelta_mA2D0BCA07D501C0BADC34644119A5428C1C38B38,
	LeanFinger_GetWorldDelta_m781C6A434D62E719DB3B45A043EC59A472D5504C,
	LeanFinger_ClearSnapshots_mD96270D2187996E5567071AE7AD0C6F5B2A0CF0D,
	LeanFinger_RecordSnapshot_mE7ADF663EAE2C265E7FBAC5475F22D711BE48F3F,
	LeanFinger__ctor_mF28F6A57F6CF80EAB8647D2983EBD0109DE5D559,
	LeanGesture_GetScreenCenter_m6F583702C5D811B23523ED40C53B3710690022D7,
	LeanGesture_GetScreenCenter_mBEC4DE64D37ED43EB82B6E85F52A96148A42F080,
	LeanGesture_TryGetScreenCenter_m0660DC297D23E080D1B8621A80A233F2F931B377,
	LeanGesture_GetLastScreenCenter_m66FDBECCF89954AC020CC60B0352C19B9906D524,
	LeanGesture_GetLastScreenCenter_m987FFE8EEDB32AAA69A57B5CBCED54276B212198,
	LeanGesture_TryGetLastScreenCenter_m5856882DA0C327EEEDD3698639A5F684FDBFC806,
	LeanGesture_GetStartScreenCenter_m58784E301770003075BD42704E38650B76EE4674,
	LeanGesture_GetStartScreenCenter_m49D20E336A531719B7139567EDD26431438E318F,
	LeanGesture_TryGetStartScreenCenter_mB5219AB78347B05980039C19069F88196A791CE0,
	LeanGesture_GetScreenDelta_mFE2D30E45982C09F1B8033C0B162F1C2534E0202,
	LeanGesture_GetScreenDelta_mDEF2602190CCE528E2B82D389003D776E903FB50,
	LeanGesture_TryGetScreenDelta_m99C52693AD179253B538F307C6D5424C349E486E,
	LeanGesture_GetScaledDelta_mFBD949167DDAF3EEC3794841906949261DB303F9,
	LeanGesture_GetScaledDelta_mEBCF12934A62E30D4CCF7C1E105CCD7CDDBD8722,
	LeanGesture_TryGetScaledDelta_mA82ADE140404DF591535F14ECC57AC48C8F116EC,
	LeanGesture_GetWorldDelta_m803890CA93315D14D4B42ED528B76376616B93BA,
	LeanGesture_GetWorldDelta_m18A2D76FD00C156F50FFEB13D58F8CA77FA360EF,
	LeanGesture_TryGetWorldDelta_m00C6ADBB133D5E2013B73CE1E7EC4CDB64DC53DF,
	LeanGesture_GetScreenDistance_m4491038BC481923F9C1189192B94570B628173ED,
	LeanGesture_GetScreenDistance_m49234CF02121AC2E0B590C613AF2984FBC9227BA,
	LeanGesture_GetScreenDistance_mA3F575986D4FBD51409E97F0906322E971C45C8E,
	LeanGesture_TryGetScreenDistance_m05405D9D6EB3F296E9A32F1B0F6DEB572B5D56F5,
	LeanGesture_GetScaledDistance_m5CF1C4ED100296BD5827E7E7E808969E54594BD5,
	LeanGesture_GetScaledDistance_m25BBEB85F0B2E9C61A28A432F82F26D4092A9EA2,
	LeanGesture_GetScaledDistance_m5AC1FD1B6E0F751D1F184B0D5CEBC67A3B290F16,
	LeanGesture_TryGetScaledDistance_m7241C7137635CC6D4A1A08AF24995568D55A6089,
	LeanGesture_GetLastScreenDistance_mB16AE83227A86ED36B24896BC000AC0EBB8E7BFE,
	LeanGesture_GetLastScreenDistance_m73169594EF4D8CF707159010C725A1890D25B42D,
	LeanGesture_GetLastScreenDistance_m7F14A08E28E683BBD7AB9DC36BDFFD66432EFEF4,
	LeanGesture_TryGetLastScreenDistance_m515DEE90923C17670475A4DCB5E907B83041CA1F,
	LeanGesture_GetLastScaledDistance_mA9B68D5E00645335555ACF88DB15CF1F47E7536C,
	LeanGesture_GetLastScaledDistance_mF21CA58E54E66DBB7141365A62F18B3A86F84B54,
	LeanGesture_GetLastScaledDistance_m903CBB366E704F9A92D215246A3DB101B95F3DA7,
	LeanGesture_TryGetLastScaledDistance_mCDE66D39C5A0330B5B52ECE33ED7A1ED97CE099B,
	LeanGesture_GetStartScreenDistance_m1B1B7D6A85518D33125EB7980F5D27B1B082C2A5,
	LeanGesture_GetStartScreenDistance_m6F6C0358ED91F801E159A2A4E13E231C4B17C2D5,
	LeanGesture_GetStartScreenDistance_mB138A7DB14B67AB5448BC24DE913ACBBD3F4A9CA,
	LeanGesture_TryGetStartScreenDistance_m2C9895EAA8769EBDB40B44AB383EE8F8F66B429B,
	LeanGesture_GetStartScaledDistance_m2844DA256DAF2456FC7ABD37677F289E085484BD,
	LeanGesture_GetStartScaledDistance_m2705202B5F71D2C1AD996D9B9C8239F007205455,
	LeanGesture_GetStartScaledDistance_m7DF4516FDD55B94E8A62A85A074FD2AA674AC2E9,
	LeanGesture_TryGetStartScaledDistance_m319D5142563436FD67BBD10CFE646ED8DDC916E5,
	LeanGesture_GetPinchScale_m7371A033DC946C7666BB9FEA5F2B90B01FAA9514,
	LeanGesture_GetPinchScale_m8FACA26AA33EF9F3C740A7675961CFD1B52AD218,
	LeanGesture_TryGetPinchScale_m184B99758343C54491B349599CA86E4F6E78B13D,
	LeanGesture_GetPinchRatio_m831DF88BF1BCCB563596402688221921BB4C856E,
	LeanGesture_GetPinchRatio_m67B78766805397BE267C10FE7CC35DD469CBE563,
	LeanGesture_TryGetPinchRatio_m263528E7637E8B8A71F33A1220AAAA8694E16E5E,
	LeanGesture_GetTwistDegrees_m84E94A9ACEECCC0E6438BD51919869B4FB59FA76,
	LeanGesture_GetTwistDegrees_mEF59A73DC08474FFD965A451DF1CC11E64549EEA,
	LeanGesture_GetTwistDegrees_m36260508D01D334AD41479C59D17CBA206CD16CF,
	LeanGesture_TryGetTwistDegrees_m73EB6ADA284F4BEB85971D7D45E720BD23F65E00,
	LeanGesture_GetTwistRadians_m6DEC4CB7A335B6A21635877DDF3BE4EEED93D466,
	LeanGesture_GetTwistRadians_m08BB2C3EBFC89B00A79299F81136A3413EB4156F,
	LeanGesture_GetTwistRadians_m2CD5C1EC8140ED6930025FCBB0DE11964DA77DE5,
	LeanGesture_TryGetTwistRadians_mA24D7D75153F45E04AD8889AB7403C822BD79AE3,
	LeanSnapshot_GetWorldPosition_m1C53DCA33B47EE047EAE80128377E06F484C9858,
	LeanSnapshot_Pop_m38C4A797FAA9486660642A6AD100344869757AEF,
	LeanSnapshot_TryGetScreenPosition_m4801A9FBDF9604E13E3E133BB4DD63A40A422869,
	LeanSnapshot_TryGetSnapshot_mDCF40DD73B05287AFE77AD595EAF7E89067A6B2D,
	LeanSnapshot_GetLowerIndex_m66494922A5DCB5E4DCB9DF7A1AE6608ABF1A6D8F,
	LeanSnapshot__ctor_m03099624F50615D694FB6349E8939BC26021A1CD,
	LeanSnapshot__cctor_m33057BEE87A9F720AE236AE9FEC88C39C4C7CD93,
	LeanTouch_get_CurrentTapThreshold_m30269461E945AD3E2A5FAB6ECFEEA21F3E6D4B0E,
	LeanTouch_get_CurrentSwipeThreshold_mA5D4266D0DAB77E81733078185ADAC2E1789DCA5,
	LeanTouch_get_CurrentReferenceDpi_m7801C2A66E53F2A7CA5B573DEE1095BD7BD9B6E7,
	LeanTouch_get_CurrentGuiLayers_mE40055092CF899FE5554763D311F3363CA180A6E,
	LeanTouch_get_Instance_m931EA1A57E10617F1381C2191AD1C675C0E7A9BC,
	LeanTouch_get_ScalingFactor_m83C3989FF8B07240B2B77D8DF86775045B050635,
	LeanTouch_get_AnyMouseButtonSet_m74CA0055BB7A8AF81009762C28555160FDA0B8AA,
	LeanTouch_get_GuiInUse_mD832B00C159C7D800F6535FDE3501A34CC9FC1C2,
	LeanTouch_GetCamera_mC9C5B0EC3E9D5C79724BE049DA38D90B329CF389,
	LeanTouch_GetDampenFactor_mCEE40A0D99DD2A50B886C7F7898A962F64B63BF9,
	LeanTouch_PointOverGui_m6094AD4CE04424942661B2AC1BB7AB2D1E42A18E,
	LeanTouch_RaycastGui_mC1F9A41639819F278217FAACE2D1F4911CB1ADEB,
	LeanTouch_RaycastGui_mC963F9772C011AE8AF0127DB33BAA43F1A309D51,
	LeanTouch_GetFingers_m9E6D5C938A65135B101FE7B0AC0727CA3AE73D3B,
	LeanTouch_Awake_m8A631DE9493E60D9F822440FC12BC67257F89C2C,
	LeanTouch_OnEnable_mEE0A9053210AF5FD0E188942BF1371CB332E7B10,
	LeanTouch_OnDisable_m9972C916C2FAC663E3D4A684737478A33DFCC0EC,
	LeanTouch_Update_m1FE1A464E540A936D36C85C56BF29DC3B6F29B25,
	LeanTouch_OnGUI_m742EE76781D030673EC554D9767ADFB16CDE46ED,
	LeanTouch_BeginFingers_mE20FD46D2675007F5AC1C645C9D6B2A17528E4A1,
	LeanTouch_EndFingers_mEF762AC83B419B7EA32C1BFB3F69B6AA5AFA1A95,
	LeanTouch_PollFingers_m41DC56D0459B675E18361E302C579AD4C19EB9D6,
	LeanTouch_UpdateEvents_mD89F972D0CADFEDC7AC4B1A4BBAB6E06EEBC59A5,
	LeanTouch_AddFinger_m146B0A34AC43BADF2EBE98D5717F355BD4848C2B,
	LeanTouch_FindFinger_mAB3FD0FA5A8BBC8F20719CCDD51BFE577C621F57,
	LeanTouch_FindInactiveFingerIndex_m90C49C10B108B66296D1F02B2288AFF9E937C3F9,
	LeanTouch__ctor_m888ABF46462DB451DCF00EA2D6749026F94D6233,
	LeanTouch__cctor_m378C2A67FA99293BFB140591E7997DC42829784B,
	LeanHelper_DampenFactor_m2C82A6D76F9D8C56E6FCD1A036CF688E2D2BE7B3,
	NULL,
	LeanCircuit_UpdateMesh_mED259F731E82703397C5691FB5AE0E1ADBE85425,
	LeanCircuit_Populate_mCFB0E32A499294D0F7B30F97317786D0AA289935,
	LeanCircuit_Start_m570CB73BD1C6FF197BD14EC788CE454F0FCB585A,
	LeanCircuit_AddLine_mCD383374CCA0F52FCE1B6FFF42D29F504AD9C52D,
	LeanCircuit_AddPoint_mFA3365B862B56BF30AF9E80044D4209955692A17,
	LeanCircuit_AddNode_m8A668166575F25039CAE8744EFCE55B86B3F53E9,
	LeanCircuit__ctor_m52655D141A32D9449A6FB7D85324FC786006CD28,
	LeanCircuit__cctor_m3026C1E28178AA5F151EE7D7735C8C386167DA9B,
	LeanDocumentation_get_Icon_mE61BAAC7E851B65AC297FDAF57BD3BFE00BA6FAC,
	LeanDocumentation__ctor_mF9FFD184E8FA280D87E3786580F1708CF1D59144,
	LeanMarker_set_Target_m2CFF230587247B3FE2ED37FD7B9411263F79F3D7,
	LeanMarker_get_Target_mDED9AE9AEF9DED1DD6CA6F5C9F954574960C58B7,
	LeanMarker_OnEnable_m34D170CEA0AD6A4856526D424B3174AD5AF3EB21,
	LeanMarker_OnDisable_m599F0BEC832B7998E70426C2A6604E8741C3209D,
	LeanMarker__ctor_mF18C3FE4149EB17BDCC29163236B24E4EAB781F9,
	LeanMarker__cctor_mD6D6FDA3C37CF54CDE047EB2A21814D65D73AEFC,
	LeanOpenUrl_Open_m8C9FE5446D8D6957713F3552CBED9DA0D39F9EE0,
	LeanOpenUrl__ctor_m5EFAB8E84C6F50EE13CC1A1F32793922163F024C,
	NULL,
	JSONNode_get_Item_m15B56AF59515C383F438425F707DCD45A92F4865,
	JSONNode_set_Item_mEB2B1436A55A4EA01826840CE454DE6139DBFD96,
	JSONNode_get_Item_m60C1ABECBE0571F458998A9A8410EE8ED8D4BC9E,
	JSONNode_set_Item_m26390E552CD8420AFD0C634ED57597CC06625A26,
	JSONNode_get_Value_mA5FDEA6BB16F7B21AE6F41A85F5004120B552580,
	JSONNode_set_Value_mAAD460AEE30562A5B2729DB9545D2984D6496E76,
	JSONNode_get_Count_m37D2CF69CE110C3655E89366A3EE2262EA99933C,
	JSONNode_get_IsNumber_mAEC1A3CE41B21C02317EB63FD8BD1366327A4D1E,
	JSONNode_get_IsString_mBACE5A4D126E8011EE8D9D18510AAE31D8B51AE0,
	JSONNode_get_IsBoolean_mB9C9F7A3C14C7250032AE78BC885CE87F15C3FFD,
	JSONNode_get_IsNull_m71B4615E695BF588CE5EAC79F52F6EC66B1C1461,
	JSONNode_get_IsArray_m73A69DC1A6B8F910CEE872204A1FF2EA9CA65D31,
	JSONNode_get_IsObject_m9210B0CDD4B70E42E3EA0B8EE6C2D510A640EA60,
	JSONNode_get_Inline_m72DF6AD0574D74648194D898901C0F61C2EF29E8,
	JSONNode_set_Inline_mB44B0D5303F8644FB399963F8342F1915FED1EC2,
	JSONNode_Add_mB007E02475524C684D2032F44344FC0727D0AED1,
	JSONNode_Add_m72F8683C5AB8E6DDA52AAB63F5AB2CF5DE668AA4,
	JSONNode_Remove_m4C6ABDC53258E365E4BEDFFB471996D0595CE653,
	JSONNode_Remove_m40DC66D2BFA64F462C956249568569C640D65FBE,
	JSONNode_Remove_mEC2EA47AADD914B3468F912E6EED1DA02821D118,
	JSONNode_get_Children_m0BDB86A86A43943A02FBEA21A8FEDE6D91906395,
	JSONNode_get_DeepChildren_m843513089FDD8B254DCC3232FD4DA85056923D9F,
	JSONNode_ToString_m8CFDF7832FB437B9D9285EA5913ACFA8BF529C75,
	JSONNode_ToString_m959FD63EB02266172C30D5608E92E9B478EA1698,
	NULL,
	NULL,
	JSONNode_get_Linq_m4CA50FA2E097B82ED352580539F7215F181AE1EA,
	JSONNode_get_Keys_mAEC584E7C7F1CCC000C51E279CEA435552023ED3,
	JSONNode_get_Values_m647ABB3BC12AFD94CE4C23531628EFC29A7981D5,
	JSONNode_get_AsDouble_mA9A87A9DDF3DB8A927705894B0A70369513743B8,
	JSONNode_set_AsDouble_mA2D6CA445FBB3B93D4E135F91CF1CE9779375098,
	JSONNode_get_AsInt_m35E5CF8D5FCA1E5D1697C6E666FF3CD5FC1B2BC1,
	JSONNode_set_AsInt_m3D8AFBE4D49B29711CCECBDAD4C145BD72C47D5C,
	JSONNode_get_AsFloat_m53D151773142FEECC3886C1ADEB2BEC22A0C3CAC,
	JSONNode_set_AsFloat_m6D669252ACE2A695075685624BDB94A60260FF63,
	JSONNode_get_AsBool_mC1732D312696100E7F429542BB876EC949DA9947,
	JSONNode_set_AsBool_m91CAA7562009099B02BD538F37D94CEE8F8882B7,
	JSONNode_get_AsArray_m7DF6AB373218A86EFF6A9478A824D5BB8C01421A,
	JSONNode_get_AsObject_m8BC40A325C24DE488E73E7DAFEA530B270BBD95B,
	JSONNode_op_Implicit_m94391C275D1BE4AC26A04A098F4B65BBB7D7AF84,
	JSONNode_op_Implicit_mB446B8B500123D3F4F3C5D66F095316FF663385D,
	JSONNode_op_Implicit_m112A87AB176887F93C5660CFD3C169A5BB1C3CB4,
	JSONNode_op_Implicit_m9B6B63FDCCE9EC0F0CF543B13E3863936D283487,
	JSONNode_op_Implicit_mC458C08484535F8FD3B586B5D5337B9AC093C837,
	JSONNode_op_Implicit_m3DDE962F828FB4DB318C6D8C336FFD51AF19C552,
	JSONNode_op_Implicit_m279DCD5A06BDC389C2AF42381AC41E3EC06700D5,
	JSONNode_op_Implicit_mE6EE79025736C59E41C99A0C9101BAF32BE77E18,
	JSONNode_op_Implicit_mBA5DC585F9071890732C4805B3FA7E1134E76477,
	JSONNode_op_Implicit_m2E4691C3EE93FD2CB2570E30EBE1F84E25A53099,
	JSONNode_op_Implicit_mFB610B47429EC263B56050700A6D74F38A93FCC1,
	JSONNode_op_Equality_mFA64BB19805777C603E6E1BD8D6628B52DEB4A1E,
	JSONNode_op_Inequality_m65F2F76C1716D266797A058589DF280A44E2CBA5,
	JSONNode_Equals_mD1DBB741A272720F18B24437CD78B338B65900C0,
	JSONNode_GetHashCode_mDDA191EC3E9FA81A238FCB2B1C8024FCA97677AC,
	JSONNode_get_EscapeBuilder_mE5AC264BA62AA54B0DCDCB165564F5EE28B50653,
	JSONNode_Escape_mABAE2F4F85D5F926A6B80D5181FD69D69669EFA6,
	JSONNode_ParseElement_m7C0AE6936FF1C5678FF55A838971E8E0E24C8A69,
	JSONNode_Parse_m1F5205640E23CB1423043FA1C5379D0BF309E4F9,
	JSONNode__ctor_m5C8FC3D0D04154FFC73CDEB6D6D051422907D3CE,
	JSONNode__cctor_mA6EEB9601A65ECE220047266B8986E7B975909CD,
	JSONArray_get_Inline_m701D46F6554CE60CD9967EA4598C51C1C941ED2B,
	JSONArray_set_Inline_m736B5C89F41DE7A7CCAC0FC6160CF4FD50B989DC,
	JSONArray_get_Tag_m51CD8C530354A4AEA428DC009FD95C27D4AB53CE,
	JSONArray_get_IsArray_m50C77F695B10048CA997D53BE3E3DC88E1689B03,
	JSONArray_GetEnumerator_mB01C410D721F8B673A311AFE4913BD2F1C8E37F9,
	JSONArray_get_Item_mFEBC0948C9244F2C604E80F5A4098BFB51255D10,
	JSONArray_set_Item_mB2AFD08A9F2CE998903ABA685D88C9CF00894885,
	JSONArray_get_Item_m1B541EE05E39CD8EB7A5541EF2ADC0031FC81215,
	JSONArray_set_Item_m90E948A121CE1B37388237F92828AFA5C539B2CE,
	JSONArray_get_Count_m77CEECDA838C7D2B99D2A0ADBFD9F216BD50C647,
	JSONArray_Add_m2F2793895033D6E8C9CA77D0440FEFD164C1F31D,
	JSONArray_Remove_mD5817778EF93C826B92D5E8CC86CA211D056879F,
	JSONArray_Remove_m36719C3D5868724800574C299C59A3398347D049,
	JSONArray_get_Children_mB77EB8E1B351B82181B50334A57FE456AB8BBB85,
	JSONArray_WriteToStringBuilder_m4FEE53C6F7BF56C22BC919A982F9A1047362819C,
	JSONArray__ctor_m80F3F8569F953F1B2448EF688ADBE2BE06B85EAA,
	JSONObject_get_Inline_m68C30F81DBE34CC9512A1CF26E501A9042CEE6B2,
	JSONObject_set_Inline_mE4E0D0ABDE60706A3BE440D751C80D45E565078E,
	JSONObject_get_Tag_m24A4D8474C6803E5B36B9C7B16CC35F48C00C486,
	JSONObject_get_IsObject_mD17F33216C3AA982747307A9783FC34B4C249ACD,
	JSONObject_GetEnumerator_m89934764DE9D9DB8D007DE2FDB956B35A483CE2A,
	JSONObject_get_Item_mC02C1B3017199E1B58E09012760D91B0236A79DF,
	JSONObject_set_Item_mAF05A0803D9648754295AD43922D8750E248AB5B,
	JSONObject_get_Item_m7B5F21E465C8A06F0FD00EE62B59CFD229360976,
	JSONObject_set_Item_mD3F2E832E77FDAEB95AF4C6113F0314E3C286D3E,
	JSONObject_get_Count_mDDB2E49A1D4DC4C26FAB083943E4F47B39C233AF,
	JSONObject_Add_m75EF35AE491E67AEDCAC14A8810985463681353D,
	JSONObject_Remove_m56A85EFAE9C4DDF43C755D4938665178D59186D7,
	JSONObject_Remove_m56C844643C6B6E81D4E3467DA869A39FF93C79FE,
	JSONObject_Remove_m9943021B70272C24F3FE7D87421F3DD6E1EADB15,
	JSONObject_get_Children_m553BB3AF1E134F5FB59F325E8D3E811C204E6AD6,
	JSONObject_WriteToStringBuilder_mCC982C03B19077CC8BC60D61B1893FD5543A7B1E,
	JSONObject__ctor_m0F87A09A5AB3C1141E7E82D8C45D9C6B44F87E5F,
	JSONString_get_Tag_m04F409B5247C8DCD38D24808764CF24D869E6185,
	JSONString_get_IsString_mD933D266CC563B1D5E85CFA7C7480CB2E5ACA48E,
	JSONString_GetEnumerator_mF8D0B322B157094DFDC176D6B8BD0E2C4BE626E4,
	JSONString_get_Value_m29504F709941AD379852FDD0B1A08E8DC4B2E58A,
	JSONString_set_Value_mECAA9F7BFEE67F189A1493F90CDC4475318ED6FF,
	JSONString__ctor_m3DE989CC0DA12FE3E23174CD280D86971771ADAA,
	JSONString_WriteToStringBuilder_m5297F304170133F94CB9E813928B07D57FFEB5C8,
	JSONString_Equals_mCE7D7CAD2604883600D10531380FECF4AFB92F09,
	JSONString_GetHashCode_mC05889B33A8AF366C4C8456D54F94F252C2300B1,
	JSONNumber_get_Tag_m5810C73995BD124FB40ADDFEB7172AE1295D3C3F,
	JSONNumber_get_IsNumber_mD941320E66AA55D3391E146E3E624AFFCCCC5912,
	JSONNumber_GetEnumerator_m3DDB7A5F04D7C6C314D6969ECDB3314385E0F8C0,
	JSONNumber_get_Value_m285D5CFDCF3259112D7128D9EC962DF7C1C638CB,
	JSONNumber_set_Value_m7B6A055194CFD54CE0F8360809DF0516696D04B0,
	JSONNumber_get_AsDouble_m837CA051FAF2D45E0B415B1CE91C6DE1A9F5C399,
	JSONNumber_set_AsDouble_mE9AE823BDDDD4CE0E3BD37ED70B0330A3D303E68,
	JSONNumber__ctor_mA5B174BD1A163979DCDD304E4A679A1D9E8801B8,
	JSONNumber__ctor_mCA0A65AB3C617FC6F8066EB9C38E2B413971E28F,
	JSONNumber_WriteToStringBuilder_mB754D2A3988C1E35BDEF7A2FB9E842ECBAFE1F4B,
	JSONNumber_IsNumeric_m88429E8156B5500398D25C0C5A0ED127BBABC887,
	JSONNumber_Equals_mF299CAFC541A41B0B4D9B12A6967B35B9F554931,
	JSONNumber_GetHashCode_m33B32859776F731322E20E05B527B92255F130F5,
	JSONBool_get_Tag_mCBABB544B93C712D3F57CC8FBEC49260FEA9284A,
	JSONBool_get_IsBoolean_m98A1AEE6EC63912E54A15116CDB7C8418209FDBA,
	JSONBool_GetEnumerator_m4B8C0CC079A97EB3B0331ECFFB15F7DD0AB7A180,
	JSONBool_get_Value_mDDA4D24E9F16DED3152898F98F16B06327BEF9F6,
	JSONBool_set_Value_m068B69773876CB81DF3C699ABC743FB1CE861F1A,
	JSONBool_get_AsBool_mCA60BF3904B572629003D2887EF27F89C1E764E9,
	JSONBool_set_AsBool_m4E1A515A31B59A3F2B683F4ECCEA388684320B91,
	JSONBool__ctor_mB729B68264E989BA9AEE3B86AF17E3906FF7C9AD,
	JSONBool__ctor_mFD5C92FB72B70A027D45ED45E42E6E7516858CEE,
	JSONBool_WriteToStringBuilder_mABA45EF0C30EE803D2FD1AC77FA7B2EA967BCFD9,
	JSONBool_Equals_m853E2A5B2F12290669528B9E27BB2CBB2F8FDE09,
	JSONBool_GetHashCode_m13AE52408BA8EFA79CD151D50EC9DCF4F7CAB73A,
	JSONNull_CreateOrGet_mFCEA43022679C084EA7BEB23AD203E1B9B33E1D7,
	JSONNull__ctor_m49798992779B2B1E8D1BAFDF4498C2F8AEA76A4F,
	JSONNull_get_Tag_m498F7F5444421EDA491F023F0B76AC1D1D735936,
	JSONNull_get_IsNull_m4ED1FF25799E79A71002A79A4AC27B2177635FAA,
	JSONNull_GetEnumerator_m390B90BACB37AC50B726CE28D07463748A202A79,
	JSONNull_get_Value_m07942D8E26A0D88514646E5603F55C4E3D16DD60,
	JSONNull_set_Value_mA2582B26943415A7DE20E1DAD3C96D7A0E451922,
	JSONNull_get_AsBool_m60DE1D508FB98AAF328AB1965DAC1BB43881CD98,
	JSONNull_set_AsBool_m5C230E9709559FD14099B0848A4D5C4C1A815000,
	JSONNull_Equals_m522306B502C21C1E2EBE989556F3F16331848600,
	JSONNull_GetHashCode_m950BC0E73B87DC3336908BEF882459BC20E5A55B,
	JSONNull_WriteToStringBuilder_m84A9A7C230BBEE86E8A548B8446FF19B5E469B6E,
	JSONNull__cctor_m0E80AEF0AD9DCA6A8018A55DAA8464FCA2DBCC16,
	JSONLazyCreator_get_Tag_mC1AC02C25A03BF67F0D89FD9900CBC6EEB090A5D,
	JSONLazyCreator_GetEnumerator_mDAA175BC448F491BD873169D142E119DF6733ED6,
	JSONLazyCreator__ctor_m8FC6D598D0237C9350588BD29072C041A61F0798,
	JSONLazyCreator__ctor_mC6E81F011E8C8956780A3B334A91DA44147BF188,
	JSONLazyCreator_Set_m746D69028C9A2C5E0B1FBBA1F8F008C2052A1779,
	JSONLazyCreator_get_Item_m5EA50EE949F36710942A9FF8EF5777D233B3C047,
	JSONLazyCreator_set_Item_m3B7837F849B2388008C45743CD3140CE828F2606,
	JSONLazyCreator_get_Item_m95C7B93F385B7EF2D47052C39794763732D6FF0C,
	JSONLazyCreator_set_Item_m00DFAB6CD611C42D6B22D575ECDCFE265710FBCC,
	JSONLazyCreator_Add_m4361ED9A8E16E28BAB9DCFA1FC1AFB27124FB670,
	JSONLazyCreator_Add_m6A8B2BEC3941E48339A2AD67047FC6D0BADFA17F,
	JSONLazyCreator_op_Equality_m68D0526912FAFBC3D3332DEBACCD45DEA750C2C7,
	JSONLazyCreator_op_Inequality_mA05185BE9E99126A18F9EF368C747CE578FBAD95,
	JSONLazyCreator_Equals_mB6087EC316E745F8D61D2068D2A98CA634113D4E,
	JSONLazyCreator_GetHashCode_m51335A4464EA4383300746254D92B163803C8C43,
	JSONLazyCreator_get_AsInt_mD1A90C028EA0FEC6DB194136F7AD88FDE408F67A,
	JSONLazyCreator_set_AsInt_m8FE656CA5259D0B4320F3DBF60BD19A686A26857,
	JSONLazyCreator_get_AsFloat_mC7748D1573024F4EC5A1E1EA1357D812FEDDF870,
	JSONLazyCreator_set_AsFloat_m5846F6BF2B58995E98DB57813A6C78CC1A7E1934,
	JSONLazyCreator_get_AsDouble_m64481355784007008FE0CDEB3CC984B2B02FD465,
	JSONLazyCreator_set_AsDouble_m278B0693BF4190C6FACC267DB451E8E190577350,
	JSONLazyCreator_get_AsBool_m6675B2D4E0C4D06469EF0B66D3E0CF5B8C5EB7BE,
	JSONLazyCreator_set_AsBool_m3DA5CF7B6A12BE73753CDF5FD3F72D06932942B2,
	JSONLazyCreator_get_AsArray_m7ED16496F0BC83E265C51066E586108D22850C5D,
	JSONLazyCreator_get_AsObject_m49378AF7ED532AAB2664C02C21E537630528B9C2,
	JSONLazyCreator_WriteToStringBuilder_m691033C947C63F4764EF87A8DC5A17CE3EF2D59F,
	JSON_Parse_mAB3D7D96F2A4170914DA8D0A54787AAC1762BCE9,
	LatestProduct_ToString_m59505EED68AA17DF6541A4B0753CDAA3C58FB0DC,
	LatestProduct__ctor_m8214D78AFA862E63DE858710D084DBC4B82ACC65,
	ProductDetail_ToString_m8A0284DB58B82E8965EC6D409D28DD7847589394,
	ProductDetail__ctor_m7D6C549F2782DE98960B7B5D2706B861B0BB487A,
	BASIC_DETAILS__ctor_m8C231A82EBCEE43AF4AAE8164D0E6410F7665C25,
	TAG__ctor_mA042C41C88534F5FC4079C5FDC9FA972BFA6F022,
	product_details__ctor_mFEB3177FCAEC0EAE68C3FA437179063FEBE1DC35,
	DATALEVEL1__ctor_m44BC58218B11FEB97BF2613C3C69EC83DC321799,
	DATALEVEL2__ctor_m12BF53BCE9FC48C1D3A876035B4F9CF1813F104B,
	COLORITEM__ctor_m194A6DCDE05DADA9F49B716DFE4EB1965FC576D7,
	INFO__ctor_m63A4D5CF5CF151203477EB1CF5E2FB59B250DBF2,
	U3CU3Ec__DisplayClass12_0__ctor_m25149D6FEE53DCACE6BAAD705A6788C6EBF6C501,
	U3CU3Ec__DisplayClass12_0_U3CGetDetailProductU3Eb__0_m4E2AE071E9BAA94C3C094775A68B353433E34608,
	U3CU3Ec__cctor_mBB55E725B17FA4A1952CD335C17369A4FE7E8C53,
	U3CU3Ec__ctor_mB02544597371209AE1E834E809B3A1C0E7008A2E,
	U3CU3Ec_U3CGetDetailProductU3Eb__12_1_m0EBF9439D0CF7C2D5FC155102701EBB1D2509F63,
	U3CU3Ec_U3CDownloadContinuouslyProductDetailU3Eb__22_0_m59C97C81191665960A609CB88E4E18BB5D29BC2D,
	U3CU3Ec_U3CGetStyleItU3Eb__35_0_mC1FAD67D0137E9509D492FC1334330526C32EE09,
	U3CU3Ec_U3CGetSimilarItemsU3Eb__36_0_mA2A91F66E247338BB9E7053BFB588237D66839E5,
	U3CU3Ec_U3CGetSimilarItemsU3Eb__36_1_m5377A81D05489E6A0C484C20A7A8B9E6729F95A6,
	U3CU3Ec__DisplayClass13_0__ctor_m5A31A0310AB9B899241FADEFC3B8EC1E02909B67,
	U3CU3Ec__DisplayClass13_0_U3CTestCategoriesU3Eb__0_m1DE86B2BD7D3C300615E48875239AABDF963145E,
	U3CU3Ec__DisplayClass13_0_U3CTestCategoriesU3Eb__1_mF724DC43EFFB768C6E62B1E87410365CE97296CB,
	U3CGETTexWithTimerU3Ed__14__ctor_mB3780C563A4473B475B6C7035E128C58C8F95658,
	U3CGETTexWithTimerU3Ed__14_System_IDisposable_Dispose_m9EED32DF5F495B058A77E6E7232B668CA97A2B1A,
	U3CGETTexWithTimerU3Ed__14_MoveNext_m9987674D352B1E1BFF0E2D331EFD801E34EBF5E9,
	U3CGETTexWithTimerU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC22A77CDDCC9A47DBC56F59B4CB6F24E7D2CAD13,
	U3CGETTexWithTimerU3Ed__14_System_Collections_IEnumerator_Reset_mA9ABD1B38B238DBEDB9B6F1A9F1F752C8FCE9B8B,
	U3CGETTexWithTimerU3Ed__14_System_Collections_IEnumerator_get_Current_m34AF29C7AF067CCC8330BDFEF9192D077FAB078A,
	U3CGETWITHHEADERU3Ed__15__ctor_m5C4B8373E1C14CB6E044924AB87BB724DF8B460A,
	U3CGETWITHHEADERU3Ed__15_System_IDisposable_Dispose_m1EB4C7472904EEDD062E9D4F8F658A7782D27616,
	U3CGETWITHHEADERU3Ed__15_MoveNext_m3CDD40B09B8C80F17B900E1BCA56BC293CCFF32A,
	U3CGETWITHHEADERU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA99F0D8BC4A8D7F9B89B083A91E598BFBCDAABAA,
	U3CGETWITHHEADERU3Ed__15_System_Collections_IEnumerator_Reset_mC88E3F6D8A214DB1949C295FB23E6B5E078C9C4A,
	U3CGETWITHHEADERU3Ed__15_System_Collections_IEnumerator_get_Current_m16B2B872A92D309DC192F72B0064345FED6A5BF7,
	U3CDLExampleU3Ed__16__ctor_m712F26C47330BD416C36EE802F5EAF4EF0EF5A67,
	U3CDLExampleU3Ed__16_System_IDisposable_Dispose_m91306082B3BFC330EB9CA5176868FD766DAFD810,
	U3CDLExampleU3Ed__16_MoveNext_mDAFF2CAB5B8218627C841AAF67CED42853D62739,
	U3CDLExampleU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m19FFEF9076B08E927AB34D4596ACC20A1550F1EC,
	U3CDLExampleU3Ed__16_System_Collections_IEnumerator_Reset_m608F2170EDD9BABC87D49AE28CB087467FF1C56F,
	U3CDLExampleU3Ed__16_System_Collections_IEnumerator_get_Current_m2D7978D7B4CBFE6AD8445BAFE901143335E49CE2,
	U3CDownloadingU3Ed__24__ctor_mD0E028408AAAF01B491F2D9A82D21D92F15CA6B5,
	U3CDownloadingU3Ed__24_System_IDisposable_Dispose_m5521A0798BBBB167DC3A123DC3CBFFFB4DA80D3F,
	U3CDownloadingU3Ed__24_MoveNext_m8ED3B2F0B061B5E71FE8217C7D42AF3A2EB11D88,
	U3CDownloadingU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB29C064BD5CFA476E95B432C6029071987F886D0,
	U3CDownloadingU3Ed__24_System_Collections_IEnumerator_Reset_m1B3B5A12CB15A13283CB4CCBA603278269559B4D,
	U3CDownloadingU3Ed__24_System_Collections_IEnumerator_get_Current_mDEE0B0F7FFB1EFEFE485869B1B53776C763E2D36,
	U3CGetRequestU3Ed__27__ctor_m8D6A61B14848DE84A7D407CA9722481203D8B297,
	U3CGetRequestU3Ed__27_System_IDisposable_Dispose_mFD8EA538E5AAEE49A0CAA0A008D5A1B68C6ADC5E,
	U3CGetRequestU3Ed__27_MoveNext_mDFE702445D15895DF79EA1088093F5CF66E0BBB6,
	U3CGetRequestU3Ed__27_U3CU3Em__Finally1_m3B1B1BC65F9288A6857976DF657B98CF01A15E92,
	U3CGetRequestU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9FBCEB84B833346D3DF484C0A41EBF50CAA74D85,
	U3CGetRequestU3Ed__27_System_Collections_IEnumerator_Reset_mDF485D496F6DF0778CAF9192A47959084E5333C5,
	U3CGetRequestU3Ed__27_System_Collections_IEnumerator_get_Current_mF665C2FAEC767D5B8C801F4B0A8B5244419607D9,
	U3CU3Ec__DisplayClass28_0__ctor_mE40A2A9B3FBEC3C50D9719C8820CD7D9BB1C8C7A,
	U3CU3Ec__DisplayClass28_0_U3CDownloadReadImageAsyncU3Eb__0_m89E1B751F44B327A100ABC9DD753010DCE3B055B,
	U3CU3Ec__DisplayClass28_1__ctor_m48CD99BD28EF67828488E45F62CFEA122EA8667C,
	U3CU3Ec__DisplayClass28_1_U3CDownloadReadImageAsyncU3Eb__1_m4F72395C2BBB957072015181F865B103DCCC050C,
	U3CU3Ec__DisplayClass29_0__ctor_m9D2C89921018B34A88AE803E937DE83D210B2D40,
	U3CU3Ec__DisplayClass29_0_U3CReadImageAsyncU3Eb__0_m014B4A738C9AD452D2845E160C74D89CF3247342,
	U3CU3Ec__DisplayClass29_1__ctor_m2F2B89F5322AB6B16064F0AF99B2C334AE3F5D2C,
	U3CU3Ec__DisplayClass29_1_U3CReadImageAsyncU3Eb__1_mF486211944655093979DDBFFA9AD7FB16127E5C5,
	U3CReduceTexture2U3Ed__31__ctor_m150B04D458FAC3DA42ED0E44810FC14EA7A64BA6,
	U3CReduceTexture2U3Ed__31_System_IDisposable_Dispose_mC45A91DA3968CF5E77F216C891389E756CC819F1,
	U3CReduceTexture2U3Ed__31_MoveNext_m8B84731BE8DE732763463BC6C7020C0CA9B5D14F,
	U3CReduceTexture2U3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m32AC526178659022F63336973BEE9678F11ED5B6,
	U3CReduceTexture2U3Ed__31_System_Collections_IEnumerator_Reset_m7D1CE94E38909EEA031A2EFE5282112CA64CD690,
	U3CReduceTexture2U3Ed__31_System_Collections_IEnumerator_get_Current_m68C1284ECE5F20419C94AC4B2C65BE6CC0270382,
	U3CReduceTextureU3Ed__32__ctor_mC888817C06364BDDFC3171F3A80EDC76948F9476,
	U3CReduceTextureU3Ed__32_System_IDisposable_Dispose_m0FF19DA639792401ABF744997433FF18A78B03A2,
	U3CReduceTextureU3Ed__32_MoveNext_m4CCFEE17E383E87AEE49DAE2397208608D9A27C6,
	U3CReduceTextureU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D6B72538757AAAD9CDFA73FD31C79B6EB1C8DC4,
	U3CReduceTextureU3Ed__32_System_Collections_IEnumerator_Reset_m52AA4767E714E8EB13C45310E34962F812F0AA39,
	U3CReduceTextureU3Ed__32_System_Collections_IEnumerator_get_Current_m57D6C26F5C02333B7B4FBF8EDC3328CA6B81B8DD,
	U3CGetTextureU3Ed__34__ctor_m6BA0BC8BA5A3C036CA1EE35FA994FD8665145807,
	U3CGetTextureU3Ed__34_System_IDisposable_Dispose_mA903B9F5C2CE107AA3B9D16797AA3696BEE7A5FD,
	U3CGetTextureU3Ed__34_MoveNext_m931097F50FD0A2AD321B72091CCA7EBFB36B8314,
	U3CGetTextureU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDB95224DD4B96927652568A0C97ABF77E838E0AF,
	U3CGetTextureU3Ed__34_System_Collections_IEnumerator_Reset_m15B6B0E81FB5318CA788ECA6C336B3B35E108B5D,
	U3CGetTextureU3Ed__34_System_Collections_IEnumerator_get_Current_m3C0340BE4A125645A4891DB7973D116504274DE7,
	U3CU3Ec__DisplayClass35_0__ctor_m6B5AFD189CAC65E3028F00264E404D010DC617D3,
	U3CU3Ec__DisplayClass35_0_U3CGetStyleItU3Eb__1_m7339AA27700E21D03980803965E41351DA12179D,
	U3CU3Ec__DisplayClass35_0__ctor_m85F8D5F0D5A46D2524F25D9B2E56908067D20B17,
	U3CU3Ec__DisplayClass35_0_U3CMultiDownloadAPIU3Eb__0_mAA6876F6FDF6AC66B4C8C849E58109EE09587CD7,
	U3CU3Ec__DisplayClass35_0_U3CMultiDownloadAPIU3Eb__1_m4BDCCD9EE2B5D9522D786676FE990BFA30D65C9B,
	U3CMultiDownloadNCacheU3Ed__41__ctor_mDAE3F0944CEA399F02D06D604EF1666B1C4CF67A,
	U3CMultiDownloadNCacheU3Ed__41_System_IDisposable_Dispose_m1C726AAC0BA51C7C3BB4805B5156219020BEAA92,
	U3CMultiDownloadNCacheU3Ed__41_MoveNext_m076F88E2DB5343FE914879BDEA916DC190B3A41D,
	U3CMultiDownloadNCacheU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m31F5B6BE969D1618EC197467E3510111A027F15B,
	U3CMultiDownloadNCacheU3Ed__41_System_Collections_IEnumerator_Reset_m3E05CA0B91A81741A9CF212A676B6522BDDD1610,
	U3CMultiDownloadNCacheU3Ed__41_System_Collections_IEnumerator_get_Current_m3D9736F78A426EC0C3F93C3107D0A9370E231E42,
	U3C_MultiDownloadDetailU3Ed__44__ctor_m019DDA02DA16A437ECA1F6856E4F19643BD67030,
	U3C_MultiDownloadDetailU3Ed__44_System_IDisposable_Dispose_mC5E91B4A310507055BD97469BDF6AA476CC68404,
	U3C_MultiDownloadDetailU3Ed__44_MoveNext_m4C8A374669A355C394C5CACB79C603AFA78FF474,
	U3C_MultiDownloadDetailU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m409764D8787998FAD8385E4203A30C0D4AB27E23,
	U3C_MultiDownloadDetailU3Ed__44_System_Collections_IEnumerator_Reset_m05D5AE375F25209B2D4201A8C1AF746122FAC751,
	U3C_MultiDownloadDetailU3Ed__44_System_Collections_IEnumerator_get_Current_mF269C59C2BF101E9E3EB62C376607C7B20314C82,
	U3CU3Ec__DisplayClass45_0__ctor_mDECE20CCFA8FC23EFB796D1312C30AD6D0CC62A1,
	U3CU3Ec__DisplayClass45_0_U3CMultiDownloadDetailU3Eb__0_m5DF33F48D36BC6FC4E06242B61966213F622EFDC,
	U3CU3Ec__DisplayClass45_0_U3CMultiDownloadDetailU3Eb__1_mE7EE7AAC90ABF5C70C09356859EB03DD5B6BDB1A,
	U3CMultiDLDetailNCacheU3Ed__46__ctor_m8A873237FA78145DEC72E51F86A5DBA1463CF407,
	U3CMultiDLDetailNCacheU3Ed__46_System_IDisposable_Dispose_mEBBE6A05457C8FC310A7D9B579E14C6F0CF9A4D1,
	U3CMultiDLDetailNCacheU3Ed__46_MoveNext_m2FA6207CB8B4079F86AD9CB8F76A241AC9E0FD5C,
	U3CMultiDLDetailNCacheU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m80F7A1FE96CB8D7ABA483DE13C9A2D87046CCF6E,
	U3CMultiDLDetailNCacheU3Ed__46_System_Collections_IEnumerator_Reset_m868AD24FF17DB9914CA7831B5FFAE5606D428574,
	U3CMultiDLDetailNCacheU3Ed__46_System_Collections_IEnumerator_get_Current_m35B8F94AF1F5F53C345C6FBD8EB730B0B19E59D4,
	U3CU3Ec__DisplayClass47_0__ctor_mB814EEF1081A8DB4F456D1DDABC8C4C5F3230FB1,
	U3CU3Ec__DisplayClass47_0_U3CGetStyleItU3Eb__0_mBDF26F8F10EEA7A5E640D1446EB3AE6EAB06F038,
	U3CU3Ec__DisplayClass47_0_U3CGetStyleItU3Eb__1_mA8569472FF2D21ED0646888A18D9C29B7179A547,
	U3CU3Ec__DisplayClass13_0__ctor_m5294883FC7F25760809E1E617D6AE9546B38CE83,
	U3CU3Ec__DisplayClass13_0_U3CDownloadAPIU3Eb__0_mF614ACE2F9D4EFE80C0F811FFE73BA97B8BD45A8,
	U3CU3Ec__DisplayClass13_0_U3CDownloadAPIU3Eb__1_m7F69D98FB8AB1D811DE498E4644FC0FA33351484,
	ThreadData__ctor_m56C244B7BC2B67E10F8536EB8A0C40F16297593F,
	U3CSpeedRenderQueueU3Ed__7__ctor_m8017F8214987809133142C325682D8C80A093AB9,
	U3CSpeedRenderQueueU3Ed__7_System_IDisposable_Dispose_m8B0B86936FB9242960CF9D490486148CF67199E0,
	U3CSpeedRenderQueueU3Ed__7_MoveNext_m6A12AB052670DEECB82EEA059279D79B829A663E,
	U3CSpeedRenderQueueU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07C0A5D45C36424BA91325FA69ACA75BCE428997,
	U3CSpeedRenderQueueU3Ed__7_System_Collections_IEnumerator_Reset_m5E0225E9BFF3C2F974932F5C5A79185AA96EA0F3,
	U3CSpeedRenderQueueU3Ed__7_System_Collections_IEnumerator_get_Current_mB8DF0A34865649B50C6EA8B6FAD7B1D2682F417E,
	U3CU3Ec__DisplayClass12_0__ctor_m28CF8EB09D166F91B445B0796711E3FF84609C6A,
	U3CU3Ec__DisplayClass12_0_U3CexecuteCoroutineU3Eb__0_m45FAA3A843575F357EA999B5C09D52D96A7E479B,
	U3CU3Ec__DisplayClass4_0__ctor_m063B6F62D61CAA29B3AB1AC3A819D6D18240F9F6,
	U3CU3Ec__DisplayClass4_0_U3CUpdateU3Eb__2_mE84C0A7EAF064241DAA6E98CF768F59DF1AB4B9E,
	U3CU3Ec__DisplayClass4_1__ctor_mBB8D9837F5EB6C46A04F399F601533FEDE46756B,
	U3CU3Ec__DisplayClass4_1_U3CUpdateU3Eb__6_m76EC6A2E3975E8A2E6A031E8B6DA4C4088F2A55E,
	U3CU3Ec__DisplayClass4_2__ctor_mAD3D1D58AD09A66A3CFCD2A27D3FDA00EAB9A6AB,
	U3CU3Ec__DisplayClass4_2_U3CUpdateU3Eb__8_mB2A8A874B685A024E5058BEED756182B1B13ACD9,
	U3CU3Ec__cctor_m962DC1D18E6A690D9DAFF83EC393FE4F5ADE18C8,
	U3CU3Ec__ctor_m63711F73205AC47759F3870E385391FC49C03864,
	U3CU3Ec_U3CUpdateU3Eb__4_1_mD2E518290C8AB4EDAC4F79EEF665A4A3430655E1,
	U3CU3Ec_U3CUpdateU3Eb__4_4_m8FA6F3DC6A54EFB3028C4BDCC66F69FC1EFC9A01,
	U3CU3Ec_U3CUpdateU3Eb__4_5_m3952D2A76F4153183CE5361AD763AC2F13EB00A1,
	U3CU3Ec_U3CtweenStaticallyU3Eb__5_0_m9F5117E4B185369A4121FC6E4972F55B802C4F5D,
	U3CU3Ec__DisplayClass2_0__ctor_mEBB010A7E42F38BBA0F6526F4898B3793016754A,
	U3CU3Ec__DisplayClass2_0_U3CadvancedExamplesU3Eb__1_m46CD88EA10100768CC8BC78BD206FB34418B38BE,
	U3CU3Ec__DisplayClass2_0_U3CadvancedExamplesU3Eb__2_mD8310B529F6CC844EFAE6B47D4EE69A074102CAC,
	U3CU3Ec__DisplayClass4_0__ctor_mAE212C3B3074A3A63D0FAFB45B319458DC469379,
	U3CU3Ec__DisplayClass4_0_U3CadvancedExamplesU3Eb__1_m5443364BDD6519C00E41B679475989EC7BFA2A74,
	U3CU3Ec__DisplayClass4_0_U3CadvancedExamplesU3Eb__2_mC7171C077A8800FAA156D255B27FF78E214EAB2D,
	U3CU3Ec__DisplayClass4_0__ctor_m6E2FDBEF2F7D210950B106AA2AC99FB7C23365FD,
	U3CU3Ec__DisplayClass4_0_U3CbigGuyJumpU3Eb__0_m83B800F5E384DA675243ACF6BE368207914E6568,
	U3CU3Ec__DisplayClass4_0_U3CbigGuyJumpU3Eb__1_mA58D74BCD32E0467DF4939BF6A9BBEBEB98B6966,
	U3CU3Ec__DisplayClass4_1__ctor_m90198E5152DCA6BBCF86C52303D7FF53541123ED,
	U3CU3Ec__DisplayClass4_1_U3CbigGuyJumpU3Eb__2_mA7236E71AC8335842B32DED8F5A9BD467BE5E68E,
	U3CU3Ec__DisplayClass4_0__ctor_m99076BF1EC2BA4183686FD305FE9257A1FC108D8,
	U3CU3Ec__DisplayClass4_0_U3CdemoEaseTypesU3Eb__0_m8895F6DF6D44640415A16CB3166821A05AE7FB13,
	U3CU3Ec__cctor_m7E87C117C81452C517C83B26D383ED1C5747A990,
	U3CU3Ec__ctor_mE3568F988560BC01C21F210610E8221EC2136358,
	U3CU3Ec_U3CStartU3Eb__1_1_mBDA4A05B6CF190E80BD4AD54FE5A7B843122E047,
	U3CU3Ec__DisplayClass15_0__ctor_m1545255D46C77C14BEC0F46C3D149CFF14287CBA,
	U3CU3Ec__DisplayClass15_0_U3CStartU3Eb__0_m7360056B580EBA5508B4AAF9B8FE47F13869D198,
	NextFunc__ctor_mE79736745346FAD184C90EC9FFB942EDC6273AA5,
	NextFunc_Invoke_m5F15E6FAEEE04B0364D5861105F46B8CA1D15392,
	NextFunc_BeginInvoke_m8FCE73C9609127DA3C01D221A5D74FAD4CA3EB0B,
	NextFunc_EndInvoke_m5901C99FC6387B6FB299C1C8426F59A415E9C0F9,
	U3CU3Ec__cctor_mF9DA54E17EBF14B22D6DB252952767BE7FD49C9A,
	U3CU3Ec__ctor_mE708DC3155F07189D4E3399F4EF7B6C622475119,
	U3CU3Ec_U3CcycleThroughExamplesU3Eb__20_0_m84C10D9978F636C29F9ED1E04BA0742A68D1C98A,
	NextFunc__ctor_mCCFCFD0AB879EBBB46104ADFF9A9024018DCAFD4,
	NextFunc_Invoke_m84CD7F169D5B749CFE2ED930F9CF0E7B9D41B75F,
	NextFunc_BeginInvoke_mCEE4B7EB7615D52E726E9DB1BF918E0ED3B46FBC,
	NextFunc_EndInvoke_mE35B84DD8B0DD2D1124F28CB2B5729F070AE3FEC,
	U3CU3Ec__cctor_m18DE319729D6EBE31B6DBEEF6AC68B727FEAFC58,
	U3CU3Ec__ctor_mF8076F513CB75419CD4AAF5D2CF96EA750C7C64D,
	U3CU3Ec_U3CcycleThroughExamplesU3Eb__20_0_m8A0627F6E7351B2DF8151D125E1E5045000F449D,
	EaseTypeDelegate__ctor_m479FD2E5E95095E811CD43C107F9F7BEDA5C1722,
	EaseTypeDelegate_Invoke_m6C4DD4B41DD183CD84736AB68488797939157559,
	EaseTypeDelegate_BeginInvoke_mC4A8F044C73F14833A8F0892A1B1D7EFA5719775,
	EaseTypeDelegate_EndInvoke_mBBC06324172B4D605212E2BF2D7568C91D55371D,
	ActionMethodDelegate__ctor_m2C8565D6C66397F9327D19C9D47C45587110BC5F,
	ActionMethodDelegate_Invoke_mD731C08D9044E02DE9276EB7269BCA0F540950EA,
	ActionMethodDelegate_BeginInvoke_mF15334FB75A22146CE827C20402393D61B7203C1,
	ActionMethodDelegate_EndInvoke_m164BF9BEF0C0CB55BC1AC688BECC75811982F9F6,
	U3CU3Ec__cctor_m7B400CF0B15F5A6BB8EDD68761B868209D666561,
	U3CU3Ec__ctor_m3E01BF844F661459DBA93A1A1A2052BB66D8278C,
	U3CU3Ec_U3CsetCallbackU3Eb__113_0_mBAF1F42A0092D36D9EA8B302EAD7E4B64484A698,
	U3CU3Ec_U3CsetValue3U3Eb__114_0_m2BEB42E3CC61F73F63334E99081144AC817C3A6A,
	U3CtimeoutCheckU3Ed__2__ctor_m86660954A201D3F4042917DED992CF67F2967E40,
	U3CtimeoutCheckU3Ed__2_System_IDisposable_Dispose_mE54F8B9D9C634E727ADEB82A507BCC5D2D7F6A58,
	U3CtimeoutCheckU3Ed__2_MoveNext_m81A53EB7B0E977685E46FE5D64A808B557EC5EC3,
	U3CtimeoutCheckU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7B28917D1D087CC4FFE00C923F13DE0E3C80DBFC,
	U3CtimeoutCheckU3Ed__2_System_Collections_IEnumerator_Reset_m4A3583E3E830B3116E7DC2C562734E6153CAB4EF,
	U3CtimeoutCheckU3Ed__2_System_Collections_IEnumerator_get_Current_m21261FBD3907579D1BC3B9C4B58CB01EA868DCB9,
	U3CU3Ec__DisplayClass193_0__ctor_mC0B0125F1AA00B0D67E5471AC4723D7C4DDF745A,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__0_m1C1C28A5106180AA479E783252142933B5B22CB3,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__1_mA9FBC068978683FE7F41AB0DE6764B37E8F12FCB,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__2_m32EDEE7568C90E8D332BAF204B827BB696F8915F,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__3_mAB9D969B192F8C7F5CD2D8CEFC40A0AB9A1E569C,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__4_mD098FD9EE16741678387663B5E5A20AACDB3C461,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__5_m2D34FBF1D0050BC36587D4AC4AB768CBBAD16653,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__6_m4F2B350D8676AF4E40A9A522AB546E7D64CB5A4F,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__7_m4794A3DFBD9F60DDB39A1D79B3261D224192C6EA,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__8_m8F118FB039CC4F97D3B5099511C0ACF4A16794E1,
	U3CU3Ec__DisplayClass193_0_U3CfollowDampU3Eb__9_m1E35B42D88BA7A620623D40B97FE0A034E6D51DF,
	U3CU3Ec__DisplayClass194_0__ctor_mDCBB6E03EFED25D38C1645524A9CFB06906D64A0,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__0_m0DF3AC96834FE13B309965159F303CF6519EEF50,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__1_mC60E4D28F90F14EC7259E5FC1F36B0C7BA0587A4,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__2_mAC5BC0C40BD4FF4F7A321BB0E5C7F9759AC85F0C,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__3_m7C23E5BA1FAF357B1D25B4DC5B0F587074A36440,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__4_m27AE2F3933DC68B0444E94E4C6C86ED6C1BAE149,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__5_m95EB2A430E734B7AA2726020BB238DF0374206B6,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__6_m15ED9AC2709C71E60DA3125DF3A16E7B8B94C3AD,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__7_m4C0A7E27C26DB6DCFBEFE3C357B8A26CC65474D4,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__8_m763826D5C9CF0FF3D93E27C117BF47A4A2C57BD6,
	U3CU3Ec__DisplayClass194_0_U3CfollowSpringU3Eb__9_mAB337657583472749A5443EF146F301CF2391AB0,
	U3CU3Ec__DisplayClass195_0__ctor_mE00CB764397209766E590DF5EFEB52053DC0AE26,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__0_mAC6C7B8F8DF55A577CE8781796BE15239B566E89,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__1_m5184ABA1AB3627B2DDED0C74E2282344378AF3B8,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__2_m34A0E6745FC7434BB5A27B25A097C58784226F3C,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__3_m2D76D667CE87B27FBCAECC55425C3207533F6A75,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__4_m205F4F7C5D9A2B137BCB29491B95C3828F378B07,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__5_mCDC95E138FF86491A853A74BF172BEEB38BECA09,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__6_mDD9833A53793BAE5B0A03C37BA4EF1F60E9930FF,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__7_m74731F175AF6639677E96328854558399D9FE7CF,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__8_m8B743DC6870594CE43D0325E2D884FB0CAF98E20,
	U3CU3Ec__DisplayClass195_0_U3CfollowBounceOutU3Eb__9_m466A3211A248D8995FB3AE22D016F1EFD623A8E6,
	U3CU3Ec__DisplayClass196_0__ctor_m929D534AE634FB58A7C0ECDFB23A1D78351F6F3F,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__0_mA2E1AF83CA2338CA95BA84B64C912222C7A3D9E6,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__1_m23CB51E9EF35556F273C3C0B62C5C06D8A27273E,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__2_m54430A940C2FEE40A54C3AE14482E1DD43CBF103,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__3_mE6D8BB8EFB0E336E59430CA8A2E029CEC51136C8,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__4_mD6D1CF2BB54CAF51FFF102ECDAE4BE98C25FE3E0,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__5_m4244AA15E1AFBF399BA4B03799FDD52F1893D5BA,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__6_mB26E4D96497F6A2BBB615D69D4966E6B8FE8BFF7,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__7_mCC89DC9F63965ABBC1705F61D48B28D83959F20E,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__8_mD29A47A9681480C45508BE22F8600B447930FA58,
	U3CU3Ec__DisplayClass196_0_U3CfollowLinearU3Eb__9_m6F5ACA5A264C5CF1B4A20164725E5103856A9880,
	U3CRandomSpawnsCoroutineU3Ed__14__ctor_m55FEE61BC4D871082A30F5B17961CAB2AB8F95E3,
	U3CRandomSpawnsCoroutineU3Ed__14_System_IDisposable_Dispose_m0818C483E374908B7D1CE5F3E3853BBC0D029159,
	U3CRandomSpawnsCoroutineU3Ed__14_MoveNext_mBB5F29DB2B8D24DB134988AD74F02F1654FC90E0,
	U3CRandomSpawnsCoroutineU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED44A0393A99F4D3A3332636A8A61D4D2AC57D24,
	U3CRandomSpawnsCoroutineU3Ed__14_System_Collections_IEnumerator_Reset_mB91900A7F499A5840DA1EC30D52644A9D0E97F2A,
	U3CRandomSpawnsCoroutineU3Ed__14_System_Collections_IEnumerator_get_Current_m9CB94ED98005F6D2CFC6F7030B18A15300819B13,
	U3CCheckIfAliveU3Ed__2__ctor_m59518A9AC72B3B2BEAAB552A8D9222A6956BE4B5,
	U3CCheckIfAliveU3Ed__2_System_IDisposable_Dispose_mE46F321812B5249C5965D8099CD74DF5757FC621,
	U3CCheckIfAliveU3Ed__2_MoveNext_mBC913097EB4077ABF6B7E9C42FAE4486BC5F6B7D,
	U3CCheckIfAliveU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFAC4D31487A4AB3D719CB76D53AC10D7EF07DFE0,
	U3CCheckIfAliveU3Ed__2_System_Collections_IEnumerator_Reset_mFF789B70B62AB28C2A4B14C2882D0C28DC907DC6,
	U3CCheckIfAliveU3Ed__2_System_Collections_IEnumerator_get_Current_mD55424D90545BBD976F87E26A5E27EE9858AC4FC,
	U3CWaitFrameU3Ed__2__ctor_m30C38499BA579212104535A61F69619BC2F70448,
	U3CWaitFrameU3Ed__2_System_IDisposable_Dispose_m076213034FB516C05B9A010F5EE8F36DA6BC2044,
	U3CWaitFrameU3Ed__2_MoveNext_m6573E68F6898234B5F7BE5067C9D6795CA642309,
	U3CWaitFrameU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDEA4815E620E29509EC3D76A6BF6912C11F834DA,
	U3CWaitFrameU3Ed__2_System_Collections_IEnumerator_Reset_mDD7A1546AF81B04D68D1BF3D526A741F57A28D14,
	U3CWaitFrameU3Ed__2_System_Collections_IEnumerator_get_Current_m6184F70547D0221260D123CB3CACE6E321D51AD7,
	U3CU3Ec__DisplayClass3_0__ctor_m0206C7B67398673472BB1EDEADFCDE7E7B83DC7E,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__0_mE6A5E64E647BEA847980CA1A647D8F4DD7538717,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__1_mF3A5A6BE10274FE2B6B92E2E3A37375350FB7C20,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__2_mFFB1B636802C122D21246C6C519213869AC16F14,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__3_m6674519BC0EE5BA68033C42DC0EDBCF007BC95D5,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__4_m88B66BBD68D3B0C17E42D1A7783D4C70E8CF4B57,
	U3CU3Ec__cctor_m5C0BC870EF559E2799B38AEBD2A18923F1C6495E,
	U3CU3Ec__ctor_m0232508BEA690347C7422E2637E635EAF983FBD0,
	U3CU3Ec_U3CPutU3Eb__5_1_m4E9DD6570C43418466C7A2D49DE1B614E8711511,
	U3CStartCountDownU3Ed__23__ctor_m53428FFF153B82D94C4EBFEC68BEC79EC85CA1D8,
	U3CStartCountDownU3Ed__23_System_IDisposable_Dispose_m80F80AA77C089EDFA3302256BE48BCEBFB758EB1,
	U3CStartCountDownU3Ed__23_MoveNext_m53760B6B2FEC89611C2057D3E1E08F352C480904,
	U3CStartCountDownU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6FEBDC5780BE8743E59D57A44C3029A79A0F9EBC,
	U3CStartCountDownU3Ed__23_System_Collections_IEnumerator_Reset_mDACEDF419849A97D120B2A0D73E793F5738906C9,
	U3CStartCountDownU3Ed__23_System_Collections_IEnumerator_get_Current_m24B48535C8C4063592B057EBEF6C028FB78AA7C7,
	U3CStartIdlingTimerU3Ed__25__ctor_mDBA1FDE4E03092947D4F1BE9784465E9BFFF1DB3,
	U3CStartIdlingTimerU3Ed__25_System_IDisposable_Dispose_m1206F13B74AF3EB667A7830E25A0776D31A44127,
	U3CStartIdlingTimerU3Ed__25_MoveNext_mC9240CA3827227739B8EAE1E967ACF10B89B42D9,
	U3CStartIdlingTimerU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC6D4B2AC57D4533DA8B58827966483AA6372485E,
	U3CStartIdlingTimerU3Ed__25_System_Collections_IEnumerator_Reset_m64667A0F7C83F7479EA4762E5FF597BE184FF25B,
	U3CStartIdlingTimerU3Ed__25_System_Collections_IEnumerator_get_Current_mFA7BFD6947904A7AA24681D01CEDA1E48648083D,
	U3CU3Ec__DisplayClass29_0__ctor_mB8DA4BFD5500C7CFF7CF35996AC16A933319636A,
	U3CU3Ec__DisplayClass29_0_U3CTapEffectU3Eb__0_m7513749B6158B0B9BF43A17C004E3CF606105752,
	U3CLoadTexturesU3Ed__22__ctor_mB85A94C8B81FCD9DD459B439F2FEA25B6AD44824,
	U3CLoadTexturesU3Ed__22_System_IDisposable_Dispose_m6F2C0169FC3ACE2B0107A8326E0BD21FC21B5740,
	U3CLoadTexturesU3Ed__22_MoveNext_m1AE68CAC44C621C9471D68415C3D08A9EB66BD72,
	U3CLoadTexturesU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m699C757DCEEFB6FBF7C001A821CA38C6D8FF0D50,
	U3CLoadTexturesU3Ed__22_System_Collections_IEnumerator_Reset_mD9EA48A421485076A823D3F48EB7FA91D7E7E812,
	U3CLoadTexturesU3Ed__22_System_Collections_IEnumerator_get_Current_mC146E0B530E5443C43E4539DFC48980F18F108FF,
	U3CU3Ec__DisplayClass26_0__ctor_m878BEEEFBCEFF11C32120447FEA3DD0F0DCDD829,
	U3CU3Ec__DisplayClass26_0_U3CSpawnTexHorizontalU3Eb__0_m13E24D93312D053EFA2F904C443F0B5375C05619,
	U3CU3Ec__DisplayClass28_0__ctor_mF04855A6C5F4B000108C4C3F0C6929955A0999F6,
	U3CU3Ec__DisplayClass28_0_U3CSmartDetectU3Eb__0_m2183DBE8700B0E0A165A91ABE0264F2AEBEAC7C8,
	U3CU3Ec__DisplayClass30_0__ctor_m30EA17AC54E490863B15D1FB4CA4D203D2C64B9D,
	U3CU3Ec__DisplayClass30_1__ctor_m0B7DDBDB73163B573F2DD416BAA85931D3BD8111,
	U3CU3Ec__DisplayClass30_1_U3CDownloadingJSONDetailProductU3Eb__0_m982CA715C610B2389B68FD8B6840924BB584F9CD,
	U3CU3Ec__DisplayClass30_1_U3CDownloadingJSONDetailProductU3Eb__1_m52015A6DFC789894FD3AC97EB55286ADA019B289,
	U3CSetingSPVLImageU3Ed__32__ctor_mE4E956152ED354968C090102675020E0CD629549,
	U3CSetingSPVLImageU3Ed__32_System_IDisposable_Dispose_mB3B15C31CA1C850848A26D56B54124CBC6D5B814,
	U3CSetingSPVLImageU3Ed__32_MoveNext_mBCBB716B4383890183E91C4D8418ECD097DFE9BF,
	U3CSetingSPVLImageU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCFE3DB767E4439A57F02B240F3BF1637B26D8963,
	U3CSetingSPVLImageU3Ed__32_System_Collections_IEnumerator_Reset_mB93EC4685714FF90B1C543811CC40A7CC6674C83,
	U3CSetingSPVLImageU3Ed__32_System_Collections_IEnumerator_get_Current_m3CD4F9F9E124CDBA00AABB2069E10DAB8FD94040,
	U3CDownloadingSPVLimageU3Ed__34__ctor_mA729E0CDAE218B958F6BEABA645E60EB4A18033B,
	U3CDownloadingSPVLimageU3Ed__34_System_IDisposable_Dispose_m4DE11CA478C8DEB152A6A31019201FEC756EA377,
	U3CDownloadingSPVLimageU3Ed__34_MoveNext_mE2740FC5E35D351F93D6F5029543DDF005471F4C,
	U3CDownloadingSPVLimageU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB45BE80163A11C8F304A193C2062F4B848183C8B,
	U3CDownloadingSPVLimageU3Ed__34_System_Collections_IEnumerator_Reset_m5581652C40443A42BF1C862A891A17648F293BBC,
	U3CDownloadingSPVLimageU3Ed__34_System_Collections_IEnumerator_get_Current_m634EE28D6E3D129BC66F6B33D5CAF125FFE15D58,
	U3CDownloadingSPVSingleU3Ed__39__ctor_m9BA9EFCAC1B27CCEBE225021FB91751471E2BF58,
	U3CDownloadingSPVSingleU3Ed__39_System_IDisposable_Dispose_m3BFAB3DFC3AA9625A9C84FC01A8988FCB7883148,
	U3CDownloadingSPVSingleU3Ed__39_MoveNext_mAA0D4EB7DEE65088F02EFCD9FBF6ECCFF6DC8802,
	U3CDownloadingSPVSingleU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCE3A9AA4835372259B50112E3018D36E4D16649F,
	U3CDownloadingSPVSingleU3Ed__39_System_Collections_IEnumerator_Reset_m002B4851069A5194EE9286BCE81492DF5E8F5B59,
	U3CDownloadingSPVSingleU3Ed__39_System_Collections_IEnumerator_get_Current_m74BBB54C0ACA2C11C9ED325EBE884B50433D24E1,
	U3CU3Ec__DisplayClass43_0__ctor_m60107EF7499E9675E293F8B34CCC48A8B5908CA0,
	U3CU3Ec__DisplayClass43_0_U3CDownloadingJSONU3Eb__0_mF5480EA3A1DF76D32B5E0371DB6348938F8CCC12,
	U3CU3Ec__DisplayClass43_0_U3CDownloadingJSONU3Eb__1_m8CF5A09EA5F8FA02A523B065B5193CF222F82BA0,
	U3CU3Ec__DisplayClass45_0__ctor_mC8199F22672FECE4166348E993B9167553B1EBF5,
	U3CU3Ec__DisplayClass45_0_U3CWaitLoaderU3Eb__0_mAC13FB4BAB11063394358A08199ED0E856E9CF3E,
	U3CU3Ec__DisplayClass48_0__ctor_mED207FF44310C2071072DAA652ED7AECF5F64C68,
	U3CU3Ec__DisplayClass48_0_U3CSetPosU3Eb__0_m5278C9D76DD645E6C0F30D4366E96A28AEC9ACF3,
	U3CU3Ec__DisplayClass48_0_U3CSetPosU3Eb__1_m8752EDD1CD8D514F1A9AE18E5644FEDC9920A9BB,
	U3CU3Ec__DisplayClass48_0_U3CSetPosU3Eb__2_mF860FA140884D0583B95FB986EE337E70B252B5A,
	U3CU3Ec__DisplayClass48_0_U3CSetPosU3Eb__3_mA59965562045C52BEB4820312C4FF3B727DB94E3,
	U3CU3Ec__DisplayClass52_0__ctor_mFE43B60BD5291A82589C8E6B8AFBA94BA5386FE9,
	U3CU3Ec__DisplayClass52_0_U3CDownloadInBGU3Eb__1_m1C936818248A4391B8D5904FDA99FC66D597EA55,
	U3CU3Ec__DisplayClass52_1__ctor_m73782CCD3A6751FEEB5F9CA49B0780E60957F983,
	U3CU3Ec__DisplayClass52_2__ctor_m08024914BDF5BECBA7D16FCBEC13D97CEDDDEDD3,
	U3CU3Ec__DisplayClass52_2_U3CDownloadInBGU3Eb__0_m227DC588C18FF872BE9530267303084E10C1EE3F,
	U3CDownloadInBGU3Ed__52__ctor_m9C32150885A44693E430D666C71EDFDF32D82EB2,
	U3CDownloadInBGU3Ed__52_System_IDisposable_Dispose_m6C66D148375FFD5DB7DD84AF176EEE8951298C5D,
	U3CDownloadInBGU3Ed__52_MoveNext_m6F290A5F0DF5C7302F9D3A453605CE86CB5B3CC9,
	U3CDownloadInBGU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5228F8C506499D7F41FF732A16607AC6126AB8F3,
	U3CDownloadInBGU3Ed__52_System_Collections_IEnumerator_Reset_m659C0406047B5D8737886D663CF82DB29EDB604F,
	U3CDownloadInBGU3Ed__52_System_Collections_IEnumerator_get_Current_m038C5E50AA9B2C6684F25B4B383FF85BC8F155E0,
	U3CU3Ec__DisplayClass20_0__ctor_m9E2570663FA2462DA3B372A0BA504980D7A8D52E,
	U3CU3Ec__DisplayClass20_0_U3CTouchManagerU3Eb__0_m60DFDF329FA13E3596A782CAA9940DCB505C3356,
	U3CU3Ec__DisplayClass31_0__ctor_m4F44AD9D2DF8CB6373FB5EE34615FDF9128F56DF,
	U3CU3Ec__DisplayClass31_0_U3CEndResponsiveScrollingU3Eb__0_mAD754467736AE903ACEF06593115803131ADB9EC,
	U3CU3Ec__DisplayClass31_0_U3CEndResponsiveScrollingU3Eb__1_mFB8D4AA041D6E0792BC5360A388215FDC4DA5E03,
	U3CU3Ec__DisplayClass25_0__ctor_m5E9344B9B4B3C8E0CCEE40261606B0D8E56C909C,
	U3CU3Ec__DisplayClass25_0_U3CChangeModalFromCarasoulsU3Eb__0_m129D8A854C288EBAA12A71B680D1372E7E87B4F1,
	U3CU3Ec__DisplayClass27_0__ctor_m56A0AA23E068137A12D9FA692834EDA52B086B8C,
	U3CU3Ec__DisplayClass27_0_U3CActivatePinchIconU3Eb__0_mF7DF63728C05CE7D1D2C1230798A8EA5F1C59BB3,
	U3CU3Ec__DisplayClass33_0__ctor_m69385B930CC9904209850F44044B251CF4464788,
	U3CU3Ec__DisplayClass33_0_U3CHideSideTMU3Eb__0_mC4530C8C1AAF7CB0A601114319CB6D617F3CEE3C,
	U3CU3Ec__DisplayClass33_0_U3CHideSideTMU3Eb__1_m6AF4C0DB1A84682DB5B06F35AF09A5FB97D83041,
	U3CU3Ec__DisplayClass33_1__ctor_m17582E556E4CC0C38F00352E707B383795BAE56E,
	U3CU3Ec__DisplayClass33_1_U3CHideSideTMU3Eb__2_mC4ED50A61752EEB3E22A68037CEED034F7B988D0,
	U3CU3Ec__DisplayClass34_0__ctor_m6C6C98D4690E8793FCFE804359CC49340D5DC639,
	U3CU3Ec__DisplayClass34_0_U3CActivateSPVU3Eb__0_mECD3DA468ADBE7BCFC2C2A53432F9477606054EB,
	U3CU3Ec__DisplayClass34_0_U3CActivateSPVU3Eb__1_mD2DE6826C1F44B7F7A46A2F805D1080A0C37129A,
	U3CU3Ec__DisplayClass34_0_U3CActivateSPVU3Eb__2_m6372589F621DDD94729FBB8067B98BCF690D138E,
	U3CU3Ec__DisplayClass41_0__ctor_mE3614AE5196C82DD067E066B035BAA43A1B1D85A,
	U3CU3Ec__DisplayClass41_0_U3CDeactivateSPVU3Eb__0_m2C1BDFE54F75C961B07B2CF2CE97A680879FACC7,
	U3CU3Ec__cctor_m7BA4783EFBB948C33D79BE72C37E27129E1E1FBF,
	U3CU3Ec__ctor_m404AE10B9B24E03B9E9EE015795EABEBD0B5CC86,
	U3CU3Ec_U3CFlipingAnimationU3Eb__52_0_mD368931020467939BF2AC978322AAD6D16AB17BB,
	U3CU3Ec_U3CFlipingAnimationU3Eb__52_1_m28C8B60D2FD7F2AC015E0317957AE4F41263440B,
	U3CU3Ec__DisplayClass53_0__ctor_mE4D4BA9971901C385EF1A6441FC41AA4311B7AFE,
	U3CU3Ec__DisplayClass53_0_U3CLeftModalU3Eb__0_mE57FB9B0FF58FC23E5E7C170B8FA6248B858B2C4,
	U3CU3Ec__DisplayClass53_0_U3CLeftModalU3Eb__1_m66B8A0D728D875FEFFE27B77B73A51696B4C3B2B,
	U3CU3Ec__DisplayClass54_0__ctor_mA656950B789B2314722A00AB7F29AF683C7B24DD,
	U3CU3Ec__DisplayClass54_0_U3CRightModalU3Eb__0_mB149439EF6040259730B43FFF2AFA61BFD6131B7,
	U3CU3Ec__DisplayClass54_0_U3CRightModalU3Eb__1_m9FD7E205B5823BD46C76F6065CBCF2479353CCDB,
	U3CU3Ec__DisplayClass56_0__ctor_m47366D71A4AABCA505BA60E3920204ABEEC9F375,
	U3CU3Ec__DisplayClass56_0_U3CMidModalU3Eb__0_mE438F47B739B13B91F9B24C32B5C1A25D2D7A8EE,
	U3CU3Ec__DisplayClass56_0_U3CMidModalU3Eb__1_m267385D5152C082FA6552EDF3EA8581B062A17D0,
	U3CU3Ec__DisplayClass56_1__ctor_mF20C175125229279CB5D074F6FB31F9EB6F7DD9E,
	U3CU3Ec__DisplayClass56_1_U3CMidModalU3Eb__2_m9E5FE6E22F612C7FECAF9288F0775473CE48390B,
	U3CU3Ec__DisplayClass56_2__ctor_mD393F815092EFE30E8395AC6917CBBAFF669C31E,
	U3CU3Ec__DisplayClass56_2_U3CMidModalU3Eb__3_m1ACB773C8A5FE4C1455F34C7626B368142177FD1,
	U3CU3Ec__DisplayClass22_0__ctor_m6ADF91B27CCB0B263DDEA4C038305E59340940A7,
	U3CU3Ec__DisplayClass22_0_U3C_MouseManagerU3Eb__0_m0420120849C5E2D860D88840BAB4E4664FF4FD85,
	U3CU3Ec__DisplayClass24_0__ctor_m4BA299C7F72FFBD509AB3BB3F8FA1BD5CE50422B,
	U3CU3Ec__DisplayClass24_0_U3CSwipeBotAlternativeU3Eb__0_m560B7A58168E1786C3D14DF77A7256CA4B545965,
	U3CU3Ec__DisplayClass24_0_U3CSwipeBotAlternativeU3Eb__1_mEBFF0E8349097E6F174346C4007078CDF77A6F7F,
	U3CU3Ec__DisplayClass32_0__ctor_m209295986C8F67915C0ED8BF651C058D46D18414,
	U3CU3Ec__DisplayClass32_0_U3CEndResponsiveScrollingU3Eb__0_m1FDAA4A2D679255C2089CB593C426E2E83A35BD7,
	U3CU3Ec__DisplayClass32_0_U3CEndResponsiveScrollingU3Eb__1_mD3D696090D2AA2FB6CD8865D2DF8E86A53A68DD8,
	U3CU3Ec__DisplayClass29_0__ctor_m653CBE2AB467273DAC1ED4C55B91F208AE93A3E1,
	U3CU3Ec__DisplayClass29_0_U3CDownloadAPIStyleITU3Eb__0_m8F745C795B5098E305303EACBBE839DCA55F9F1C,
	U3CU3Ec__DisplayClass29_0_U3CDownloadAPIStyleITU3Eb__1_m9B9BE861290996E529B0C80B95AD780AC4E341D9,
	U3CDownloadingStyleIt2U3Ed__32__ctor_m42A04B908E7530E2284710A10146E1D89BB375F0,
	U3CDownloadingStyleIt2U3Ed__32_System_IDisposable_Dispose_mF24B6AF677A6A42C72FE3E1738D36C0099A2FC88,
	U3CDownloadingStyleIt2U3Ed__32_MoveNext_m39B5D459342B1587E563FAC829BFB81A367C2AF8,
	U3CDownloadingStyleIt2U3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF50A352DEC674B93255A4A2A0D9E06D2EE5BDA0D,
	U3CDownloadingStyleIt2U3Ed__32_System_Collections_IEnumerator_Reset_mE60BB186C5EEB2756996CB0B777E6328AC159823,
	U3CDownloadingStyleIt2U3Ed__32_System_Collections_IEnumerator_get_Current_m16D17D52F81DBD046807F63367E9A29BD5BAFAC7,
	U3CU3Ec__DisplayClass36_0__ctor_m297C9F3FCDC5DA350C0AD371B807F5E815CE3CA5,
	U3CU3Ec__DisplayClass36_1__ctor_m9BF2F59E7519123409A44C40E84D3C645FA08506,
	U3CU3Ec__DisplayClass36_1_U3CDownloadDetailCarasoulU3Eb__0_m795639C7D35455708FF0857CE54554DEDA9BDFC5,
	U3CU3Ec__DisplayClass36_1_U3CDownloadDetailCarasoulU3Eb__1_m893391E772D3A112AB018E569E275BAEFDAC8758,
	U3CU3Ec__DisplayClass38_0__ctor_m8F675362040CF284D5F54E6CCEE52F8C0BB9D70C,
	U3CU3Ec__DisplayClass38_0_U3CDownloadSubtlyU3Eb__0_mDD1EDAD7DA8B57B961FCE24D2DA4A64F1A13098A,
	U3CU3Ec__DisplayClass38_0_U3CDownloadSubtlyU3Eb__1_mE39A8645FA34F2DDF1469B7E5C90CC7D84E24527,
	U3CSetLPAndTexU3Ed__41__ctor_mCD98FB48B22896C60B0EA29418D91B13F2634541,
	U3CSetLPAndTexU3Ed__41_System_IDisposable_Dispose_m136EA1369A6E3D47CACCEBEA74D87E78B269D992,
	U3CSetLPAndTexU3Ed__41_MoveNext_m8F887FD229ED61A049E0E22D0EC52A660E5ED70F,
	U3CSetLPAndTexU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2DE471B4BA1BD4BC339BFDF8094F3ED3BD178B72,
	U3CSetLPAndTexU3Ed__41_System_Collections_IEnumerator_Reset_m77CAF6D268B15483D2F1BCEEA1895C45A0FCC2CC,
	U3CSetLPAndTexU3Ed__41_System_Collections_IEnumerator_get_Current_mE06A2E328E043AEC6E6937B90A36F6D5F3214259,
	U3C_GetDPSimilarAndContinueU3Ed__43__ctor_m371CA9430B2B498B6A532AE6AD1B2AA6599F03D5,
	U3C_GetDPSimilarAndContinueU3Ed__43_System_IDisposable_Dispose_mDCE67EDF483D9DF1C819998E81C0214CA7303E38,
	U3C_GetDPSimilarAndContinueU3Ed__43_MoveNext_mCB3A0BCF6D694A15986CD0BCB1F342EE62CEF211,
	U3C_GetDPSimilarAndContinueU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43580EBB068989475A35F7467802D6B24281F89A,
	U3C_GetDPSimilarAndContinueU3Ed__43_System_Collections_IEnumerator_Reset_mD418543FAE4EA398AB6506F2912D23CE61B18F24,
	U3C_GetDPSimilarAndContinueU3Ed__43_System_Collections_IEnumerator_get_Current_m433C8F67741934EC376F302D0FDCE0A39BB5AA49,
	U3CDownload5FirstImgU3Ed__46__ctor_m7F8891F944669026E11C2FCA8A5E55BF2BA29D2C,
	U3CDownload5FirstImgU3Ed__46_System_IDisposable_Dispose_mFA047EB7F2EB195452F3A561221CBF6D65032D49,
	U3CDownload5FirstImgU3Ed__46_MoveNext_m235C87C476E49C456AD92F12A6522B391B22FD08,
	U3CDownload5FirstImgU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m539E78E67BD7D6369E4B2FBA5EE5E8808BEE1B35,
	U3CDownload5FirstImgU3Ed__46_System_Collections_IEnumerator_Reset_mC13FD9498A3539F8B8A4A8EA1332DDDAD65913E5,
	U3CDownload5FirstImgU3Ed__46_System_Collections_IEnumerator_get_Current_m622928139B949871C8E74D33E51D01D65335ECD9,
	U3CU3Ec__DisplayClass48_0__ctor_mD515A9735C479501A642C30D17647C965B10E6EA,
	U3CU3Ec__DisplayClass48_1__ctor_mC93A61149B481721A590B4B3F000CF29FE6BDE34,
	U3CU3Ec__DisplayClass48_1_U3CGetStyleITSimilarU3Eb__0_m426A216A0098224A5D2B9115484A9EF1C30D4C1F,
	U3CU3Ec__DisplayClass48_1_U3CGetStyleITSimilarU3Eb__1_mE3A342AB55C35E283DFD0FC27024853A94AD8F78,
	U3CDownloadingImgSubtlyU3Ed__49__ctor_m7384606ED7EB7913BA4BBED835231EB554E83028,
	U3CDownloadingImgSubtlyU3Ed__49_System_IDisposable_Dispose_mE2EEF86407CFE50BA2A92231C671390918DC56BB,
	U3CDownloadingImgSubtlyU3Ed__49_MoveNext_m9074F91FC41C773148821D58D38794C9824BDDC5,
	U3CDownloadingImgSubtlyU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE252E3863DF34B6E2A6E8D9646F4719761DE15DE,
	U3CDownloadingImgSubtlyU3Ed__49_System_Collections_IEnumerator_Reset_mC779D62EFDF7070F7592BC75D2FFC63594387DDD,
	U3CDownloadingImgSubtlyU3Ed__49_System_Collections_IEnumerator_get_Current_mDA36E53BCBE228B802A57E258AA3DB731A4F0027,
	U3CU3Ec__DisplayClass9_0__ctor_m5F83927542B0FB2F6C96E3546B9104095A023FF3,
	U3CU3Ec__DisplayClass9_0_U3CMaskCanvasTimerU3Eb__0_mAB976CA3706030E058821359101D29056C860287,
	U3CU3Ec__DisplayClass10_0__ctor_mAE572141468E81911A1A7CB2BF75A94D8C689264,
	U3CU3Ec__DisplayClass10_0_U3CStartCountingDownIdleSPVU3Eb__0_mC5566EF9A0FB2A018C0DAF4FDD755954AD7A42E6,
	U3CU3Ec__DisplayClass10_0_U3CStartCountingDownIdleSPVU3Eb__1_mB11B2FBC4D7E3F7809043D871AD151B86C0BCC1F,
	U3CU3Ec__DisplayClass10_0_U3CStartCountingDownIdleSPVU3Eb__2_m41C01BEF91628259F4528F8AAAB965F96A9D0992,
	U3CU3Ec__DisplayClass13_0__ctor_mAC3B8C8F0CBD0C4B5DF21497482D6ED690355145,
	U3CU3Ec__DisplayClass13_0_U3CCoolingDownAfterSPVU3Eb__3_m6C3F15E6D7B20DB66DB157D6EDE9BF996174CEEF,
	U3CU3Ec__DisplayClass13_0_U3CCoolingDownAfterSPVU3Eb__4_mCA3D3BD1C470A4687C7CD0C38851BCD8D5328E59,
	U3CU3Ec__DisplayClass13_0_U3CCoolingDownAfterSPVU3Eb__5_mE2526975C46DD404DC6F077E1C1EA44443229010,
	U3CU3Ec__DisplayClass13_1__ctor_m219CAAE7598F7528E8D296A76F02E5A19C7BE745,
	U3CU3Ec__DisplayClass13_1_U3CCoolingDownAfterSPVU3Eb__0_m15502D0BDAE0958CFBA95F2DBEEC9690F062B73F,
	U3CU3Ec__DisplayClass13_1_U3CCoolingDownAfterSPVU3Eb__1_mCD9F12870F0D9ABD86EE333CC39EFC1BE73526F8,
	U3CU3Ec__DisplayClass13_1_U3CCoolingDownAfterSPVU3Eb__2_m278F4215A5D0389D461FE36F7533656879DDEF4B,
	U3CU3Ec__DisplayClass16_0__ctor_m60AD82FFD92AED2B7D313BA18FD0DA88382AEB4C,
	U3CU3Ec__DisplayClass16_0_U3CSwiperU3Eb__0_m4BA7300B4EA50880D343880EAC19E276DB7B4602,
	U3CU3Ec__DisplayClass16_0_U3CSwiperU3Eb__1_m5E95332DAE95E12D4E94C24DE62C0149C9DDD179,
	U3CU3Ec__DisplayClass16_0_U3CSwiperU3Eb__2_mEC77BFB810460B9042716715162C1716C0E1BCC4,
	U3CU3Ec__DisplayClass16_0_U3CSwiperU3Eb__3_m341366EE6EF01E1F363B77C9E2B9516DC05BC542,
	U3CU3Ec__DisplayClass16_0_U3CSwiperU3Eb__4_m97926705ED8D962A57F6E104C07B7481CA0E67EC,
	U3CU3Ec__DisplayClass16_0_U3CSwiperU3Eb__5_mC1506E8868F039BB999591EBF404BA3FE948A04D,
	U3CU3Ec__DisplayClass17_0__ctor_m5E081B9664F6D7BBC1BB3F3C43599995338490A1,
	U3CU3Ec__DisplayClass17_0_U3CGoToDefaultStateU3Eb__0_m0988B7EC7E00340F8D4BC45B51F343BE7D8D9818,
	U3CU3Ec__DisplayClass17_0_U3CGoToDefaultStateU3Eb__1_m769E53170157E7E7135EFB6E53B00FD6427C29D1,
	U3CU3Ec__DisplayClass17_0_U3CGoToDefaultStateU3Eb__6_mFE2ACDBF06ACF9D6B323B80A2CD7B8B5D9760AD5,
	U3CU3Ec__DisplayClass17_0_U3CGoToDefaultStateU3Eb__7_mA4BB07F5AEBE5720245C1B014CBA78DF36D9AD7A,
	U3CU3Ec__DisplayClass17_1__ctor_mB5968D7C955844228115C9FB168B627BA4E135F7,
	U3CU3Ec__DisplayClass17_1_U3CGoToDefaultStateU3Eb__2_mD853ECCB6C77707FE21934AAA56092D1B33A5EBF,
	U3CU3Ec__DisplayClass17_1_U3CGoToDefaultStateU3Eb__3_mB00290943876BAB62ABDEFBB15CDCC4FC2718B32,
	U3CU3Ec__DisplayClass17_1_U3CGoToDefaultStateU3Eb__4_m9FBE7E0D9667C4EE6CEC5529CF248E5F5CFD8C91,
	U3CU3Ec__DisplayClass17_1_U3CGoToDefaultStateU3Eb__5_mC0EF1D0B7E0B1BDCA73B70777C8CECC63C9F0BC6,
	U3CDownloadImgGoBackU3Ed__19__ctor_m5BD30B610950AAEACA0A79B9E5DEEE3DDCF6B56B,
	U3CDownloadImgGoBackU3Ed__19_System_IDisposable_Dispose_mFB56F9775E85F8D173ED57ACA36B058CD5517CA8,
	U3CDownloadImgGoBackU3Ed__19_MoveNext_m8BF70F76884BF0F745209DD88EB2BBC766189562,
	U3CDownloadImgGoBackU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD68B48E0E89FC3C2E0508E515FA0F59B26666E4,
	U3CDownloadImgGoBackU3Ed__19_System_Collections_IEnumerator_Reset_m45022B6700F6DC04B7DC92A16F79181B6C92D496,
	U3CDownloadImgGoBackU3Ed__19_System_Collections_IEnumerator_get_Current_m5E8EDCAE97978D4D6506ED4DB17F01DBF49E0F73,
	U3CU3Ec__DisplayClass20_0__ctor_m7024E676AE793FBCFCB1F2279B2FB97483F6FD05,
	U3CU3Ec__DisplayClass20_0_U3CDownloadDPGoBackU3Eb__0_m7606798CFDD68B82C75D4C10A811B29057B25750,
	U3CU3Ec__DisplayClass20_0_U3CDownloadDPGoBackU3Eb__1_mB38292A94C62C87BDA2640B29FA6F69CB7AC9C6E,
	U3CU3Ec__DisplayClass22_0__ctor_m22DDB7D71EF49057B3EC0DB661DF13A5151B0734,
	U3CU3Ec__DisplayClass22_0_U3CCheckSpawnHorizontalU3Eb__0_m57407159696242C5C835258C271A8610C70D54E6,
	U3CU3Ec__DisplayClass24_0__ctor_m43F32AE26D666A48211F67C8C097B0915B6509CC,
	U3CU3Ec__DisplayClass24_0_U3CComebackFromFluidU3Eb__0_m0F05C7E786A7C5D93F99807C18E6849779609E28,
	U3CU3Ec__DisplayClass26_0__ctor_m5FA854693358BBC76CB6886315C19486E4B4F639,
	U3CU3Ec__DisplayClass26_0_U3CComebackRowU3Eb__0_m2070DF50DE23631BF11AD0B09599D939F1B986ED,
	U3CU3Ec__DisplayClass29_0__ctor_m435487F025C380806EFF874C06D9336FEC4A9592,
	U3CU3Ec__DisplayClass29_0_U3CDownloadStyleItU3Eb__0_m645AF86A071A40D61FB1B984117560C0AF469BC2,
	U3CU3Ec__cctor_mC9CCE327DB010005D66AB15F205E14F048CCE34C,
	U3CU3Ec__ctor_m684AAFC2817300FD3C32E31E8AF357CDA4AC1BA6,
	U3CU3Ec_U3CDownloadStyleItU3Eb__29_1_m6C0D4AA7AA946FB7CE5DC6D3A17B5D7CDAFDAA84,
	U3CBackAnimatedU3Ed__38__ctor_mB0162397CAF149D3D5CD3FFF8C661C39B3AD1B8E,
	U3CBackAnimatedU3Ed__38_System_IDisposable_Dispose_m524FCA507BBD67AF77CECA85D59BA327CF15A2C1,
	U3CBackAnimatedU3Ed__38_MoveNext_mB2B08A393C82A7B5E7F06339AF59CB0ACAD0E8D1,
	U3CBackAnimatedU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m038F721CB12DFAB91B0D3C938970EB8D56C9A919,
	U3CBackAnimatedU3Ed__38_System_Collections_IEnumerator_Reset_m9C778128BF8C3E464B4165DB6207532D9940A2AA,
	U3CBackAnimatedU3Ed__38_System_Collections_IEnumerator_get_Current_mABEA0C9148A56E0D4774C4A112C6D08E0EFD0CA0,
	U3CBranchDownloadAPIU3Ed__21__ctor_m4BC4DCDCB90C1062A4D53EBF0F084D6DAD28E59C,
	U3CBranchDownloadAPIU3Ed__21_System_IDisposable_Dispose_m8DC64B33FFAB87F9BB28CB815CC7829359F62E32,
	U3CBranchDownloadAPIU3Ed__21_MoveNext_mA7AB985F625A238EACE4EB34CB8851BFB1F15CBF,
	U3CBranchDownloadAPIU3Ed__21_U3CU3Em__Finally1_m8F18EB19092764D8C74840CBA161B6E478DC10F9,
	U3CBranchDownloadAPIU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m20C2D7434CBD322990AEE1C1FA3E0B40425B88F6,
	U3CBranchDownloadAPIU3Ed__21_System_Collections_IEnumerator_Reset_mDCB698FCD419166698AE4B5719976656C975A58B,
	U3CBranchDownloadAPIU3Ed__21_System_Collections_IEnumerator_get_Current_m54FEE335829B80C62DCD93F57798763502D89AAC,
	U3CU3Ec__DisplayClass22_0__ctor_m80AC54DC1527827E14CCAF4745BFB2486F90E640,
	U3CU3Ec__DisplayClass22_0_U3CMultiDownloadAPIU3Eb__0_mB07ABD47A83079B531A7F22BEEF8B129AFEC206C,
	U3CU3Ec__DisplayClass22_0_U3CMultiDownloadAPIU3Eb__1_m38D7AFFD2388849E9435DBB5D5B96C40B2BBD5EB,
	U3CMultiDownloadNCacheU3Ed__25__ctor_mAF90793763EB80C4CFBF8A704C007A6A3DBB8032,
	U3CMultiDownloadNCacheU3Ed__25_System_IDisposable_Dispose_m95ED45CD0F52C398E8DCF7E862B12D792D1D7587,
	U3CMultiDownloadNCacheU3Ed__25_MoveNext_m0E3ED18BB76C8BDFDA179D2B10CBCA850C37DEFE,
	U3CMultiDownloadNCacheU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8EEA6E82F323AC9E6DFDD2F3D348EDCEA469CD78,
	U3CMultiDownloadNCacheU3Ed__25_System_Collections_IEnumerator_Reset_m058BC975A1F821ED114BC09E82B539DB8724858E,
	U3CMultiDownloadNCacheU3Ed__25_System_Collections_IEnumerator_get_Current_m0AC4CCFC678D17C3DE764C6F7A902ECA7BFFFFE5,
	U3CU3Ec__DisplayClass26_0__ctor_m40E3639426B60D365247CB0C1D9345D870C2BA92,
	U3CU3Ec__DisplayClass26_0_U3CMultiDownloadDetailU3Eb__0_mA0D95980DEEB9976802FD31C228A4AC04D19FD85,
	U3CU3Ec__DisplayClass26_0_U3CMultiDownloadDetailU3Eb__1_m6B11B2C0D8C3D7517A622FF07B7FB3A3766FB6F6,
	U3CMultiDLDetailNCacheU3Ed__27__ctor_m2CE1775E1FB2789814400CEECFCA8C24C025FA12,
	U3CMultiDLDetailNCacheU3Ed__27_System_IDisposable_Dispose_m58441CF17ED6F1FCD0C2D12485BDDDCDA406BA33,
	U3CMultiDLDetailNCacheU3Ed__27_MoveNext_m5067196148B4AB062D7CF9DCAA0EA49137559868,
	U3CMultiDLDetailNCacheU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1AE81F249631A57FCB0C47DE16CA89C660DE80C1,
	U3CMultiDLDetailNCacheU3Ed__27_System_Collections_IEnumerator_Reset_m1BA9D6028EA4CF10ED76EADA21C9DC92F01FA64F,
	U3CMultiDLDetailNCacheU3Ed__27_System_Collections_IEnumerator_get_Current_m068312F7DD13BBFE441237D9DF944B698A43D48E,
	U3CStartU3Ed__4__ctor_m004DF17C34E6A9C76325BD4C65E0F328AD4B37E0,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mE7E20C5789828A8FDE263BD5ACC2D02982334B46,
	U3CStartU3Ed__4_MoveNext_mB3F55ABCABBA6717942BDC85935FDE439D09E226,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF976D094846BDC403335C76F41CAC26D53C8F1D2,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m9368B3951732B0D18993DA1B889346A775F252CE,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m495592283D84E6B446F7B9B7405F250D9687DEFB,
	CharacterSelectionEvent__ctor_m036DA7F340B0839696EB50045AB186BD1046BE85,
	SpriteSelectionEvent__ctor_m0BC042938C4EBBB77FFAD68C1ACD74FC1C3C1052,
	WordSelectionEvent__ctor_m1C01733FD9860337084DFE63607ECE0EF8A450EA,
	LineSelectionEvent__ctor_m1C3A0C84C5C0FEA6C33FA9ED99756A85363C9EF2,
	LinkSelectionEvent__ctor_mC7034F51586C51D1DE381F6222816DC1542AFF3A,
	U3CStartU3Ed__10__ctor_m139DF863E59AD287A6C14228BB59D56E7FD2E578,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mBBFAE2F68813477259A0B665B4E81833C03C746B,
	U3CStartU3Ed__10_MoveNext_m8D27A150B4BC045DD5A1ACB2FABBB7F7F318A015,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C55687F407BA372889D6A533FB817E1EA81C165,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mE725DFFE744FAEDABF70579D04BBEB17A6CFF692,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD8EEDE3D2E1E9838472D20AE93DD750D1EE39AF8,
	U3CStartU3Ed__10__ctor_mECD4DB9695B4D04CEF08DF193DAFA21412DA40EF,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m6773C67C5C6E7E52F8C8545181173A58A6B7D939,
	U3CStartU3Ed__10_MoveNext_mB823F98B1B0907B6959EAEE4D1EBE0AA35795EA3,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF100CFC13DED969B3BBE2007B3F863DCE919D978,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mDE1D3A30ECA00E3CA2218A582F2C87EEE082E2DB,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m13710692FCBB7BB736B1F6446778124064802C83,
	U3CAnimatePropertiesU3Ed__6__ctor_mB4DA3EEEFC5ECB8376EF29EAC034162B575961B8,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_mC6A68A27A902E234429E3706D6DB432BEA04A384,
	U3CAnimatePropertiesU3Ed__6_MoveNext_mDE142F6B3AAB5916834A6820A5E7B13A7B71E822,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9F4D59DDC372B977CD7297AA5F0C83D4FFBBA830,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mAC1E960F7FFCF032EE668635835954377D1469F6,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m96B6B8A0D715268DB789F3D4C60FC6DEC9E1F6E6,
	U3CWarpTextU3Ed__7__ctor_m07871FDF578BC130082658B43FB4322C15F0909E,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m3C8BC1501397E256464ADD27A32486CDE63C2BE2,
	U3CWarpTextU3Ed__7_MoveNext_m11D4701B069FCFC106319CBDCA56244EFA4C795F,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m995FCE4A3B9B270605168D01EF7439A437864C06,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_m9970CA113E1A293472987C004B42771993CAA05C,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m398E9D0D09F26D5C3268EB41936ED92240474910,
	U3CStartU3Ed__4__ctor_mFB2D6F55665AAA83D38C58F58FA9DD0F5CE51351,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m72D91E3700B8E1F2053A7620793F97E01C1A2E3A,
	U3CStartU3Ed__4_MoveNext_m2839A135AFB4518430283897981AF4C91ABDBC6A,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB3E80ED07333F946FC221C16A77997DF51A80F87,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mAC8E620F4FCE24A409840017FB003AA1048497EF,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mD6DFD0886CCA250CB95B2828DEEEEE5EA6DC303A,
	U3CRevealCharactersU3Ed__7__ctor_mAF579198F26F3FD002CB7F4919CCB513E2B770E1,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m2B3AB12689498DE28F90CF5DA3D40AA6C31B928E,
	U3CRevealCharactersU3Ed__7_MoveNext_m16C2594A51B9D102B30646C47111F3476FFBF311,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8B5325041D4E4A3F3D5575373F25A93752D9E514,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_m321E1A1FD7DE7840F8433BF739D3E889FB3E9C7C,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9CF6271A74900FFA5BD4027E84D63C164C1650BC,
	U3CRevealWordsU3Ed__8__ctor_m5D2D48675C51D6CBD649C1AAD44A80CCA291F310,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m9F97B9D63E30AF41B31E44C66F31FE6B22DDFD4C,
	U3CRevealWordsU3Ed__8_MoveNext_m19EB0E15617A1893EEF1916629334CED1D8E9EA1,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m98E45A0AFB76FA46A5DA4E92E3FE114E310D8643,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_m09CF6739322B1DB072CB8CFE591DBBC046008E63,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m2083C1001867012CC47AFA8158F00ED15527C603,
	U3CDisplayTextMeshProFloatingTextU3Ed__12__ctor_m7E5E121E510EFDCCF0D565EBBF607F80836EBB66,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_IDisposable_Dispose_mD0C99DF97552E843D1E86CF689372B3759134BB7,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_MoveNext_mA0FD89A2C5D0867977DAB0896DAB041EDFA64200,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m735F43BD7983BCC8E417573886ADC91CDED8F29B,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_Reset_mBE24EA2EDAFC5FD22D633595174B122D5F84BB02,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_get_Current_m4199EEF69BC21DC6DDC2A0D0323AB70508CB1194,
	U3CDisplayTextMeshFloatingTextU3Ed__13__ctor_m664F9327A457FB4D53F20172479BB74A4B50675A,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_IDisposable_Dispose_mA301D3B10770F41C50E25B8785CE78B373BBCE7C,
	U3CDisplayTextMeshFloatingTextU3Ed__13_MoveNext_m96E4CEDBAD5AA7AC7C96A481502B002BC711725F,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7609D75AFDAE758DAAE60202465AB3733475352B,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_Reset_m6F0C933685FE89DF2DECC34EDD2FE5AD10542579,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_get_Current_m29F3BB8ADAADB8FD750636C535D2B50292E2DE3B,
	U3CAnimateVertexColorsU3Ed__3__ctor_mD05EA47C6D3F9DC7BE5B4F687A62244E48BC3808,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5E0C78AE94BC2C9BD7818EF0DD43D46ABF8C0172,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m8ABBF64D39EB5C5AC87AE0D833B6CBE570444347,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m328E88E5192D85CDD2157E26CD15CC0B21149AB6,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_mB49B7C6E42E5B1F488ECA85B1B79AC1D6BBC3022,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m6966F60B8B8F4540B175D0C145D8A73C89CE5429,
	U3CAnimateVertexColorsU3Ed__11__ctor_m14F85BFAE5EFFAF0BBD6519F6A65B1EE36FC5F0F,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m36A05E300F1E06AE13E2BFF19C18A59F3E73424A,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m95629D41B32EBFFE935AA88081C00EB958D17F53,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m994AE653E10219CEB601B88CC22E332FF8AD1F36,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mE8D9B5795F095798688B43CF40C04C3FE3F01841,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m12F2FCDBABCD9F41374064CAD53515D2D039EFD0,
	U3CAnimateVertexColorsU3Ed__11__ctor_m93C153A3293F3E7F9AB47C712F5D8BC9CB0B179D,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m6E2E964131F208551826E353B282FBB9477CB002,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mD5F89478A4FDEA52E1261CCCF30CC88B0D693407,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE6A7BC75B026CFA96FA1C123E6F7E5A5AAFC46E9,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m28679C8DDFAAD809DD115DB68829543159F47FBD,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m85E44906BDBB53959D8A47AE74B74420179AD3EC,
	U3CAnimateVertexColorsU3Ed__10__ctor_m9B592C7897687114428DF0983D5CE52A21051818,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m24A98FB24DAE578F96B90B3A6D61D18400A731B1,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m52F01BB7305705B9DE4309E1A05E2725BA06672E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB05E3BDEFD461F6516F45D404DBDEC452C646D37,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m72086FED04B8AC40B92836B894D6807AFB51BB38,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mA1E44018FA1464D953241F5FA414715C9ADE98CF,
	U3CU3Ec__DisplayClass10_0__ctor_mF154C9990B4AF8DA353F6A8C115C96FB7CB76410,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_mDEE1E28BD53D02CB2E40D0C263BBA65C0B5AC66C,
	U3CAnimateVertexColorsU3Ed__10__ctor_m92C755B17AC00199A759541B62E354765068E7F1,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mBA95580B1808E4EB047CD7F082595E1C5EE930C2,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_mA307F1943028D7555032189B48F6289E09D2E220,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5288CCD0BF91027A0CE3AE12D301F3E5189F0DCE,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m7C214643A52D954B2755D3477C3D0BFC85DAFF8E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m666CA82A5C974B5AC09F277AFC97A82D1D1C365E,
	U3CWarpTextU3Ed__8__ctor_m63F411BEA2E513D84AAA701A1EDF0D0322FDE9C4,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m3D9631AF92186316351AFF095E5176423B84E25F,
	U3CWarpTextU3Ed__8_MoveNext_m24ED53830B643858F6068C6908E7C0E044C671A4,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07B1ABC445F7EADD1DB5F513857F1607C97AAC12,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4C7C46CC80C502CC1AD61D2F9DAF34F09226AF06,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m86693688EEF5E754D44B448E82D87FAB40D6C8B8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass3_0__ctor_mDD9B2D764A7F06CE14C9AA9268BCB17ED0813404,
	U3CU3Ec__DisplayClass3_0_U3CWaitForU3Eb__0_mA9BB5AA0EEA3D6D9641C8925AD5A9C40581359D6,
	U3CU3Ec__DisplayClass4_0__ctor_mF7332E74D6031A699FB3E362BCA5449D842E0C77,
	U3CU3Ec__DisplayClass4_0_U3CWaitWhileU3Eb__0_m0F7701136468BAB79030BC43C81E50638797F1E9,
	U3CU3Ec__DisplayClass34_0__ctor_m259D4EC3DAFFD7558DD10749A133A0910C081090,
	U3CU3Ec__DisplayClass34_0_U3CInvokeRejectHandlersU3Eb__0_m335E63FDF51ACDBEDAB0629720B1D1B55F1E58EC,
	U3CU3Ec__DisplayClass36_0__ctor_m45686FC6F8CB9BE85B51D393013062AB66C9D5B7,
	U3CU3Ec__DisplayClass36_0_U3CInvokeProgressHandlersU3Eb__0_m88EAEB7C25F24DEEC94BB7D8BDBA66DD83D13B6F,
	U3CU3Ec__DisplayClass44_0__ctor_m35729FAE3B44B8B748AEB1980A6AC0B983C749BD,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__0_mE53DE47A4C06B730655C86F31EE9ED9D860C4479,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__1_m4D6EFED1B9CCA059F0ACCF7E6708081A3AB5F086,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__2_m98B65D5E97F22A93A8217F6EB085F675377332B0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass52_0__ctor_m69716E3F6A735C4B4C28087FDA50F5F09DB30E32,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__0_m233B389EBFC4D7286B55ED390D159850790E2FDB,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__2_mB46295DCAFE8FE7C54F482CA0C86E54680992E15,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__3_mAA3309C3626EBF3E8F76009CA4F427C83F953F40,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__4_mEACDABDAA6FFF67C581B08BD62CD9BC56A0949F9,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__1_mB5C72805A51D0983C5208F1989AA4E63EFD7DDB1,
	U3CU3Ec__DisplayClass53_0__ctor_m222556F76031E532583B13710D1D44D59203F88B,
	U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__0_mE9CBBBAD23B511665E4DAF51182BEFF19717D66A,
	U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__1_m9FFED99D29BDE440B63A4211688A0E73AC063656,
	U3CU3Ec__DisplayClass56_0__ctor_mE5ABE39A011297619BBB9A417EAFF67C90B3ECAC,
	U3CU3Ec__DisplayClass56_0_U3CThenAllU3Eb__0_mA21AFB4C21A37853627FF773D4D1404AD6786740,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass59_0__ctor_m21AA08F1CCD5964BA35DF716C53261025C460112,
	U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__0_mFE4AFFCEE3F4C03203991C50CE715F72C79934FA,
	U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__3_m534C390DE221AAB5B12A7F18178B50982F183196,
	U3CU3Ec__DisplayClass59_1__ctor_m8CADFC14A776F00B5CD88C833CB4AA7F9B18C17B,
	U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__1_mB40874546F22A3FA6FFC04A2386F73B57802B4FD,
	U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__2_mEB0DC6CE06D97E8415B3AE8BE18D33CB885C127B,
	U3CU3Ec__DisplayClass60_0__ctor_mFF337F3758214F04A90EBCCE909A7455EAF86978,
	U3CU3Ec__DisplayClass60_0_U3CThenSequenceU3Eb__0_mDADCE479295647D84A792C1F706EA461171D991A,
	U3CU3Ec__DisplayClass62_0__ctor_m53A064E5AB7603599616344F2B28ACA37D698011,
	U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__0_mE0CCBEC473F4F6DCCA161248B39B9B782B295163,
	U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__1_mACDAAF3228380745A6842299964731D368B7FCDF,
	U3CU3Ec__DisplayClass62_1__ctor_mA9304C3D108EC6058B368D9DD6E948B06AFE9FDB,
	U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__2_m5360F807690CF5E74F146D584D34DE677EE93B33,
	U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__3_mC8ECE43EBCA660A2853FFB58DB0724268F2E8CDB,
	U3CU3Ec__DisplayClass63_0__ctor_m8F351021BA6EECAB7541825324F716AC68B1D9DA,
	U3CU3Ec__DisplayClass63_0_U3CThenRaceU3Eb__0_mC8980C10FD965277E8B6C4B1039DD4F6E26E3219,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass66_0__ctor_m7EB9C2B14C29EFA69EB45597C94535431002058A,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__0_m2104C52784C77E1D04926D34D96A74512C4520FB,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__2_m77A50A6EF9965FB41F5633217FD998B0EF6B863A,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__3_mA0DDD75707B1382532981C92A7B3531185896EF6,
	U3CU3Ec__DisplayClass66_1__ctor_m888B2508224216BC793FA7DA5E5A044DD8EB4DF5,
	U3CU3Ec__DisplayClass66_1_U3CRaceU3Eb__1_mECFDEB9A5F2502DDC4F37A62293E5B2C5532035D,
	U3CU3Ec__DisplayClass69_0__ctor_mD4C27332BF7FB89D1D3CD681EFD76D4F5B0E2AA9,
	U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__0_m298F4EC934D7AC355229E5BF0DB6BFF0CAD58F18,
	U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__1_mB0F765C7C3721CC58C3F321427114632EF064052,
	U3CU3Ec__DisplayClass70_0__ctor_mB88A3F890AB8B9A3ABE655F0974CDF1CDA5AEF7E,
	U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__0_mE689907E2D5FAA8A7130A21B22CD727F68159F54,
	U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__1_m593BDA8724A11D73C8391A31328031138C6B2455,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CCreateRequestAndRetryU3Ed__0__ctor_mD37DD8CA8B40E520CF56145DC05E68555FF76E77,
	U3CCreateRequestAndRetryU3Ed__0_System_IDisposable_Dispose_m080EB28F49C65EC51AC4750E83100207A95D3B86,
	U3CCreateRequestAndRetryU3Ed__0_MoveNext_m235030D2A161E2C5C8049CC0174BD9F7B289BF8F,
	U3CCreateRequestAndRetryU3Ed__0_U3CU3Em__Finally1_m61B12EF61BE5FB037D21C2224F87B0E4AFF8071B,
	U3CCreateRequestAndRetryU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2E8715A83B734FD17A9E241D9A60C9C56B6E9360,
	U3CCreateRequestAndRetryU3Ed__0_System_Collections_IEnumerator_Reset_m9E714EF70D4BAD2C62D9074581A6882AF357803D,
	U3CCreateRequestAndRetryU3Ed__0_System_Collections_IEnumerator_get_Current_m9BB4AB12471CEF850D24DC9B5B0B4BBDCF973DBE,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CoroutineHolder__ctor_m368583023672118E43018D9FF54F80B9AFD49656,
	U3CSendWebRequestWithOptionsU3Ed__4__ctor_m0759F9AB8CE86B695FB6D3E582FA7B30A0814CCE,
	U3CSendWebRequestWithOptionsU3Ed__4_System_IDisposable_Dispose_mDB3D942B1FA63A72AEDCBFBF05E2147161225E0D,
	U3CSendWebRequestWithOptionsU3Ed__4_MoveNext_mABDBEBA35842F4574B69061243E67283F120DFB2,
	U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF9CD6D592C9CFF9F6548C8058596A8BB698CD9D9,
	U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_IEnumerator_Reset_mA14639758B270F07294F673BD202A6DC447F1EE2,
	U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_IEnumerator_get_Current_mC8EEB7C6BDB27BB7877DDBFC1196D69BE3B1474E,
	U3CU3Ec__DisplayClass22_0__ctor_m09737A2030CD017312EC64C12F90D25E7F6D889C,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__0_mD9304EE0F587C5B18D9DA6809F5D81BCAB92D43B,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__1_mF26C6F23B1DE3EA3FD39340C393170DBA0B09740,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__21_m7A53268725D5D9AD30AFBA3B94C22E4FE40BE8ED,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__2_mD6EF48C094516FE0626C9DA7EFAB3AEBBB6A2FE7,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__4_m2122D73717CF12BDD39D8D0D7C19611B1F21A2C6,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__5_m749A2A34E2D95A13A8D8E483357FA5D731550CB2,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__6_m0B499A19BE3F839A9EFC34F6077C38D5D275AF32,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__8_m4FA25C019E9365E7E37985DC1E7F7907F379A4F2,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__9_m2455B8C83E029A34C3DBF08ADD33FFFEC7F0D7CC,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__10_m1A7C51B0CC5CE0D268DBB4FDBABB3B3C09B76E8B,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__11_m0C33237140A600D0C563633E550551710271F061,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__13_m705EEBD4D5DBDE19B8FDFC3A819610EE1D2509CF,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__14_m3A7DB9A92B2D6751BD123943544D8C0DFCB7EBA8,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__15_m136FDB1FB84D17F0E6052AC8D1482C41E983C7B7,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__16_m505B89E3C2306856A34C770F72B07D2F36459437,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__17_m6C5F087D0C92313A411D887D78A23FC35674C5AE,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__19_mBF88042ADFBF15D57513B1F7691F9EBB1AD743E4,
	U3CU3Ec__DisplayClass22_0_U3CStartU3Eb__20_m53E8A86C0D0731F935717A27DA325C1CF5BA06A9,
	U3CU3Ec__DisplayClass22_1__ctor_mF03C28B73060772B45422D83C2CCD891461E7F5A,
	U3CU3Ec__DisplayClass22_1_U3CStartU3Eb__23_m0A92AF27DA09C241937F80B5E52AC8A467F592C7,
	U3CU3Ec__DisplayClass22_2__ctor_m0D149B5FF3BAD974378B32F32122517AFE1008F7,
	U3CU3Ec__DisplayClass22_2_U3CStartU3Eb__24_m244CFBB7D843FDE489A08F6438F480F77705B733,
	U3CU3Ec__cctor_mBB9151D0770DA4B6C0D5DE0DF4505B0B1473C792,
	U3CU3Ec__ctor_m7292CCD570F1D648747270AA2E9366DC8BEA76AE,
	U3CU3Ec_U3CStartU3Eb__22_3_mBE510D88050EF0EAE74485B1380103286F1E2ACF,
	U3CU3Ec_U3CStartU3Eb__22_22_mC2B2676DF6376C960A2168BB616C30FAF9572F46,
	U3CU3Ec_U3CStartU3Eb__22_7_m333F34EE7AB7B6BA9AFB42616E6F31AD16648DFA,
	U3CU3Ec_U3CStartU3Eb__22_12_mE9BCA088D81630DCA6B4A29493F717166DE4BDC2,
	U3CU3Ec_U3CStartU3Eb__22_18_m0E1A87851B8F186E1D037D6019BF2B5F03BE4AE7,
	U3CU3Ec_U3CpauseTimeNowU3Eb__26_0_m0067B26B1415DF3C65658EF775E43DE444C9F89E,
	U3CU3Ec__DisplayClass24_0__ctor_m277D86C124C510C86F124C314062F6CA37F04780,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__0_m1C7842AC0922C8535A9D996B445C45BD68ABC7CB,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__1_m3B2F75CA971620227F026AC96214BE0E9F33EE3B,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__2_mA4939E85F8BBD95C7A69CCA149A6F37BC65799FB,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__3_mA4873B5E9E38B22E691207FB286333F85B549ACB,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__4_m14C5D280141ADAF244B2213E9CE61686FC470854,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__5_m25B454BD3DB25DF33E9961063468DC070C809146,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__6_m33BFB7BC2272AE16E35A6AF6271EAAD019DE7D5B,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__7_m9DE96F4CFC41638644635C5699FDA81811802151,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__13_mC7412BB7516D3AAAA92794BF489DD6224FDFA38A,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__14_mE19037EF79A1618B0ABFA5EBB8B7C7167BF59D81,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__15_m5B4E117FD86BFF9E7C043AC7AF54886AFE964EC2,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__16_mD4F03BFC2B675FB03F1475391F156ACFF2D9AEA9,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__8_mABF6F7F053A21322BA9C371F9FFF930148E42C63,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__9_m0832DF1094C24C2B4E50D843130FD5C09B2F33C5,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__10_mDA5269BA5F2FB4EB59DFCCF9E6C806EF781EF838,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__11_m451E7955EC34F408DFAC4365893C10BCCD1F0FFE,
	U3CU3Ec__DisplayClass24_0_U3CtimeBasedTestingU3Eb__12_m58715E24DE0A08CD597D94E0B4DABAA49D93F71C,
	U3CtimeBasedTestingU3Ed__24__ctor_mCE839055D4DA089ADAB24FBCCA8FCCB9307C948C,
	U3CtimeBasedTestingU3Ed__24_System_IDisposable_Dispose_m57943CFF04FA4711B6B63A93258509F56768483A,
	U3CtimeBasedTestingU3Ed__24_MoveNext_m40360D1C97BFDAB90C0E02A76CBAE4E3C2EC99B5,
	U3CtimeBasedTestingU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB206B6F0564F23D404E3E0F14B01E7359C2EAA8B,
	U3CtimeBasedTestingU3Ed__24_System_Collections_IEnumerator_Reset_m90D3F19EAF2AE82FB852651B1A13B8EC21FA77D0,
	U3CtimeBasedTestingU3Ed__24_System_Collections_IEnumerator_get_Current_mBD52D1EB4D613ADB298D9E9A684232F65AC7468F,
	U3ClotsOfCancelsU3Ed__25__ctor_mB107D0BCF384F8F0CD3DF49E8E91829D271AB34D,
	U3ClotsOfCancelsU3Ed__25_System_IDisposable_Dispose_mC7CD787978EEAB84A938E833CE818B59FC967926,
	U3ClotsOfCancelsU3Ed__25_MoveNext_m6FB026E807483B7AA2A7CF23F6940E0E9DD8B1FE,
	U3ClotsOfCancelsU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D4392AC16098A343CB609FBF9FA41618CC01B3E,
	U3ClotsOfCancelsU3Ed__25_System_Collections_IEnumerator_Reset_mFF39885CA3EDBEF256FBE637E5C9E4230518A07A,
	U3ClotsOfCancelsU3Ed__25_System_Collections_IEnumerator_get_Current_m94A9DE71BD10C268704724FC2B1E588F75D3C78D,
	U3CpauseTimeNowU3Ed__26__ctor_m5F319CD4B560891783427EB803E77B4A8ACE0FB6,
	U3CpauseTimeNowU3Ed__26_System_IDisposable_Dispose_mAB85DDD605A77343E6464DA39BF37EDEFB254086,
	U3CpauseTimeNowU3Ed__26_MoveNext_m49B3D9FE98B48DE72D1FA96F2C22FFED13EEB9E5,
	U3CpauseTimeNowU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m372428EA5DCAD59137F4E5AFC1C5566DDB6E1625,
	U3CpauseTimeNowU3Ed__26_System_Collections_IEnumerator_Reset_m3DE5854E0C5EAB304CF501FD2D42B92798B6233D,
	U3CpauseTimeNowU3Ed__26_System_Collections_IEnumerator_get_Current_m1B325E4F8BE919D78719C70EFC817D07842F3C7C,
	LeanFingerEvent__ctor_mB87E6F24934BDC577CFF4AA5773BE5C4F9B32648,
	LeanFingerEvent__ctor_mAAD46C660F8B9EBAF6B2BED711CA41A22348E49F,
	Link__ctor_m1A8DC5DFE60F1BE6C77691E4247B58771D6566BE,
	Vector3Vector3Event__ctor_m0312381CC23B5EF1DC054A58A590AA22F98562FF,
	Vector3Event__ctor_m85527E38274A76342FE4415FFE0117F51BC9049E,
	LeanFingerEvent__ctor_mF30FFEAE76482E248DB2F1D4E6F6FEBC0E6A8D2E,
	Vector2Event__ctor_m83F82A779D5F0CAA8D422563F15715E3D9160099,
	LeanFingerEvent__ctor_m28D3CC40458A74DC26E67B1E33F9BE46622E1B6D,
	Vector2Event__ctor_mF448C7E550E06DFCFE770E1DDA821BDBE65E555C,
	LeanFingerEvent__ctor_mF6789F45618C6F2324906DB871A31C2BC5296638,
	FingerData__ctor_m9BB8C75E525D6102ECC893E1DF8E5CC837D27910,
	LeanFingerEvent__ctor_m5127F698F385886B67EB8033736C1EBAC9B5DF28,
	LeanFingerEvent__ctor_m400AFD1A3CB8DA7C354345AC297F0466278164FF,
	Path__ctor_m45C894ABA0FC3B1BE649EDDA63AC852A3F111965,
	Node_Increment_m9E9A92D1E02E7B5AC2175B0B2FB79D23ED458222,
	Node__ctor_m0B2B80358AC583E82186000D0915FBB1DAE123A8,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerator_get_IsValid_mE03DD42D0DEDE3493D9405D9EE535AEA5FE446CC_AdjustorThunk,
	Enumerator__ctor_m05355A819BFE7107717A69957C3A255273449385_AdjustorThunk,
	Enumerator__ctor_m4E912D002FEBB7AD53C59AFAF90DF4917BD85B02_AdjustorThunk,
	Enumerator_get_Current_mE01930C040D8565C5DF7682E6415FC48BD388B94_AdjustorThunk,
	Enumerator_MoveNext_m5F7677A228DDBA16F1D18078933A17ABAA823C75_AdjustorThunk,
	ValueEnumerator__ctor_m7A7A4CA011E1ECFB43F7C7DAB985DF2B3DB921EE_AdjustorThunk,
	ValueEnumerator__ctor_m1CDBED8B76E22050F7FCED0CEDB8A7B26C94B515_AdjustorThunk,
	ValueEnumerator__ctor_m4D8F3303471408B2B130036835DA3AE22F3EA430_AdjustorThunk,
	ValueEnumerator_get_Current_m151356ECDEFD55E1FFF524FE25A0F489878A10AB_AdjustorThunk,
	ValueEnumerator_MoveNext_m80DB8324975D9A8428322BB7B34D1D45ACD5176B_AdjustorThunk,
	ValueEnumerator_GetEnumerator_m3C2328208D593CFC614F7D42AC30DDB1047A9E1A_AdjustorThunk,
	KeyEnumerator__ctor_m7562FD1BB37B81B6EC866F2D4B91D449D4A2617B_AdjustorThunk,
	KeyEnumerator__ctor_m4C89FED3394614DF05CA0BB2E5348A502F508191_AdjustorThunk,
	KeyEnumerator__ctor_mC9EAD48CFE6FE0ABE897E6EA4BC1EE0F240758EE_AdjustorThunk,
	KeyEnumerator_get_Current_m5CFAB7F891D0DDC9B3A5DF1DE2038F6DAF292AEB_AdjustorThunk,
	KeyEnumerator_MoveNext_m7DE4E6044266B4C47EDE88397169D03DF60BF244_AdjustorThunk,
	KeyEnumerator_GetEnumerator_mF33634E45FC6CCD3ACDE273AEB14CAAA35009A91_AdjustorThunk,
	LinqEnumerator__ctor_m1A1EE79F7821C0ED795C46EEDB7783176040EBA3,
	LinqEnumerator_get_Current_m394350654CC60ACA7DBE5CE90AD892927BFC33B2,
	LinqEnumerator_System_Collections_IEnumerator_get_Current_m31927FCDF96A4B90105E25B324E2CAE85134A8ED,
	LinqEnumerator_MoveNext_mFC180A5717730454E62E0286F5D8D9AE72BDE393,
	LinqEnumerator_Dispose_mBF5FB73384994A555175F46E352478281AB91C63,
	LinqEnumerator_GetEnumerator_mFF6F7F4A9E1900EEDDCF167EB48EFC325A4B0066,
	LinqEnumerator_Reset_m06A57EF3840D4EE2704E0CB4DFB8EB9B7251EFA4,
	LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_m0F3F335982F9A8EA3C287B0D15DBBF65BD5E7D7E,
	U3Cget_ChildrenU3Ed__39__ctor_m08A6302695FA8FFED3961CEC013FAE54A844A596,
	U3Cget_ChildrenU3Ed__39_System_IDisposable_Dispose_mE5C32E35EC7D527C9C11C20F309E38FF92B59D5E,
	U3Cget_ChildrenU3Ed__39_MoveNext_m631A849E9C817F907E77C1194938FA563F554D55,
	U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mA653A80D0CCEF661004013DF85936DDE4D4F803A,
	U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_Reset_mE69EE6153C71479E77F6CDF38ABDCF6984C5ABA6,
	U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_get_Current_m5E565021D4E7CF1B6D7902ED534C33442460D63C,
	U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m31645C0A0DDD485DFF26CF141E02DFB3A8D20DAE,
	U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerable_GetEnumerator_mD0A55464C40549A568E050A922E19E5969E161A7,
	U3Cget_DeepChildrenU3Ed__41__ctor_m0712F6AA6A7F37973B3F66EB37691B9F4783814D,
	U3Cget_DeepChildrenU3Ed__41_System_IDisposable_Dispose_mBBC1E7AEB8F9B7AC04D165ECAE9D183F19F8296A,
	U3Cget_DeepChildrenU3Ed__41_MoveNext_m0DF534784D92B51FE386064197554F9491459867,
	U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally1_m87F47E29A82391F9EABDC2960423809A63D3DA64,
	U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally2_m948556C997EAEE00AC4050FA6DA476FF8AF2D65A,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m436DE502A96515949F5C5027B1F2810165801B8A,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_Reset_mD10B0B2BBC87D7BFDC59F237563C3515A19F612F,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_get_Current_m5D325EA5E1D268FA593EB324238DB117FA6CBC34,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m057AEFA5ACBFD6128FBAD7D76EFE7830B59B55BE,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerable_GetEnumerator_m981A7B811CFD4CAD91222704760D1EF0F3423FA7,
	U3Cget_ChildrenU3Ed__22__ctor_m06E8E8B2FC52FE3018AE3269AAC642BCFA34C707,
	U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_m80DF5B4FDDA2D5154F34AFDF6AA28B727EF7D8B2,
	U3Cget_ChildrenU3Ed__22_MoveNext_m57D914F733B5D614D5CAFCDD73DDFA157A03EAA2,
	U3Cget_ChildrenU3Ed__22_U3CU3Em__Finally1_m85D02220871BC7AD5F7FB9F627A25273631A999D,
	U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m5D1768596B7404BC7B060F244F5D88C3528E52BB,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_mF86B3D72C5015C52CB57E5FF8C90CD58BC75192D,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_m527FCAE88CA8E2F6E28FBDC52B4C4884832DDD29,
	U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mA802939A320EA3BD49E49507CF21D4056A78518A,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_m4A7EE4525D2D51F084E7489AA33ABEC1DCEA6479,
	U3CU3Ec__DisplayClass21_0__ctor_mF8A6932A8B490316F55E33EC122C544BB332B12A,
	U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_m78AEA81A69084D7B4B4D6A1DE8558D50B065B7DB,
	U3Cget_ChildrenU3Ed__23__ctor_m41067DDF3D08F0BE8CED3D9E7872A7FBA8316CDC,
	U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_mE9BEDEC6724C22575197031050734765574F3973,
	U3Cget_ChildrenU3Ed__23_MoveNext_m92ED45D5D77C97089D74BC1B35568C8ED33176DC,
	U3Cget_ChildrenU3Ed__23_U3CU3Em__Finally1_m4A46673EE413AF8FAD22D64152BEBE5EE76A5E65,
	U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mD50C7203AF0D12B0EDFB7F1AABB9B5BBBD327243,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_mF4C27D1B6113C761A688634D1A4C9C5058280CEC,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_m2EE583F6F365FBAA04E82B8963501E8A3DA58C70,
	U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m948470928F9C800DBE8DB201F697EE2F36254AA3,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_m5D486E26CEBA6C8C62417BFB778CAD59F2306BCE,
};
static const int32_t s_InvokerIndices[3639] = 
{
	23,
	23,
	23,
	28,
	28,
	26,
	27,
	14,
	14,
	28,
	202,
	28,
	23,
	26,
	32,
	28,
	28,
	897,
	23,
	28,
	28,
	26,
	28,
	26,
	26,
	23,
	23,
	23,
	125,
	105,
	105,
	105,
	105,
	105,
	125,
	105,
	28,
	102,
	28,
	202,
	2115,
	-1,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	31,
	23,
	23,
	23,
	31,
	23,
	130,
	23,
	23,
	129,
	58,
	28,
	23,
	58,
	130,
	2116,
	130,
	23,
	23,
	34,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	130,
	23,
	23,
	23,
	23,
	191,
	191,
	362,
	163,
	163,
	1433,
	23,
	23,
	23,
	26,
	14,
	23,
	841,
	23,
	163,
	163,
	23,
	163,
	23,
	163,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	163,
	26,
	1383,
	26,
	26,
	28,
	23,
	23,
	23,
	1363,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	332,
	23,
	23,
	23,
	23,
	23,
	2117,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	1383,
	23,
	23,
	23,
	23,
	23,
	1363,
	1355,
	1383,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	473,
	23,
	23,
	23,
	23,
	332,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1355,
	1355,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	332,
	26,
	23,
	23,
	2118,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1355,
	1355,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	332,
	26,
	23,
	23,
	2118,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1354,
	1355,
	1354,
	1355,
	14,
	26,
	14,
	26,
	14,
	14,
	23,
	28,
	10,
	10,
	14,
	26,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	1921,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	28,
	23,
	23,
	14,
	89,
	23,
	1985,
	2119,
	2120,
	2121,
	1500,
	2122,
	2122,
	2119,
	2119,
	2122,
	2123,
	14,
	14,
	1921,
	1878,
	34,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1354,
	1878,
	1878,
	1878,
	28,
	1921,
	28,
	1921,
	1878,
	1921,
	123,
	199,
	1878,
	1878,
	1878,
	34,
	34,
	123,
	123,
	123,
	123,
	34,
	14,
	14,
	34,
	14,
	34,
	28,
	28,
	105,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	105,
	105,
	105,
	105,
	28,
	123,
	123,
	28,
	1991,
	28,
	1921,
	123,
	28,
	123,
	123,
	28,
	28,
	1878,
	28,
	1878,
	123,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	1354,
	1355,
	1354,
	1355,
	720,
	332,
	1509,
	1510,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	1335,
	23,
	10,
	23,
	129,
	14,
	720,
	1878,
	28,
	105,
	105,
	213,
	28,
	28,
	1878,
	2124,
	14,
	23,
	26,
	26,
	32,
	4,
	2,
	2,
	187,
	132,
	173,
	119,
	2125,
	0,
	2126,
	2127,
	2126,
	2128,
	23,
	3,
	23,
	34,
	28,
	14,
	14,
	14,
	14,
	34,
	1878,
	1878,
	1474,
	1449,
	2129,
	2130,
	2131,
	2132,
	2133,
	2134,
	1433,
	2135,
	2136,
	2137,
	23,
	23,
	14,
	23,
	2138,
	2139,
	43,
	480,
	1,
	0,
	1,
	3,
	23,
	3,
	3,
	106,
	106,
	106,
	173,
	174,
	3,
	23,
	1533,
	173,
	3,
	174,
	173,
	2126,
	433,
	3,
	841,
	163,
	605,
	163,
	2140,
	363,
	173,
	892,
	43,
	43,
	0,
	363,
	173,
	163,
	3,
	3,
	363,
	173,
	163,
	114,
	114,
	46,
	114,
	114,
	46,
	114,
	2141,
	0,
	0,
	4,
	4,
	2142,
	1,
	808,
	2143,
	2143,
	2143,
	2143,
	2143,
	2143,
	2144,
	2144,
	2144,
	2145,
	2145,
	599,
	599,
	2125,
	2127,
	2146,
	2147,
	2147,
	2147,
	2147,
	2147,
	2147,
	2146,
	2146,
	2143,
	2143,
	2143,
	2127,
	2147,
	2143,
	2143,
	2143,
	2147,
	2147,
	2147,
	2127,
	2143,
	2127,
	2143,
	2143,
	2143,
	2148,
	2148,
	2127,
	2146,
	2143,
	2143,
	2143,
	2149,
	2150,
	2151,
	2152,
	2153,
	2154,
	2154,
	2155,
	2155,
	2156,
	2157,
	2154,
	2127,
	2158,
	2127,
	2143,
	2143,
	2143,
	2143,
	2127,
	2148,
	2148,
	2127,
	2146,
	2143,
	2144,
	2159,
	2160,
	1472,
	1472,
	1472,
	1447,
	1472,
	1472,
	1472,
	1472,
	1472,
	1472,
	2133,
	1472,
	1472,
	1472,
	1472,
	1472,
	1472,
	1472,
	1472,
	1472,
	1472,
	1472,
	1472,
	1472,
	1472,
	1472,
	1472,
	1472,
	1472,
	1472,
	1472,
	1472,
	2133,
	2133,
	2133,
	2161,
	2161,
	2161,
	2162,
	2163,
	2164,
	2165,
	571,
	488,
	1295,
	46,
	237,
	173,
	571,
	23,
	3,
	0,
	23,
	2166,
	1338,
	1358,
	1358,
	23,
	26,
	26,
	720,
	1358,
	930,
	930,
	930,
	2167,
	930,
	2167,
	332,
	2168,
	26,
	448,
	448,
	1358,
	1358,
	2169,
	1358,
	930,
	930,
	930,
	2167,
	930,
	2167,
	332,
	1383,
	2122,
	2170,
	2171,
	14,
	3,
	23,
	1718,
	1336,
	1808,
	2172,
	89,
	10,
	129,
	23,
	23,
	720,
	332,
	720,
	332,
	720,
	332,
	720,
	332,
	1341,
	1718,
	28,
	123,
	1985,
	1878,
	28,
	2173,
	123,
	123,
	14,
	62,
	3,
	3,
	3,
	173,
	2174,
	173,
	173,
	2175,
	546,
	2175,
	546,
	119,
	2176,
	2174,
	2177,
	1483,
	23,
	3,
	2143,
	2143,
	2143,
	2143,
	2143,
	163,
	605,
	2140,
	163,
	2144,
	2144,
	599,
	599,
	114,
	114,
	114,
	2127,
	2127,
	2127,
	2146,
	2146,
	2147,
	2147,
	2147,
	2147,
	2147,
	2147,
	2127,
	2147,
	2147,
	2127,
	2147,
	2147,
	2143,
	2143,
	2143,
	2143,
	2143,
	2143,
	2147,
	2147,
	2147,
	2147,
	2147,
	2147,
	2143,
	2143,
	2143,
	2143,
	2143,
	2143,
	2143,
	2143,
	2143,
	163,
	1,
	163,
	2127,
	2127,
	2127,
	2148,
	2148,
	2148,
	2148,
	2148,
	2148,
	2143,
	2143,
	2143,
	2143,
	2143,
	2143,
	2127,
	2127,
	2127,
	2143,
	2143,
	2143,
	2143,
	2143,
	2143,
	2146,
	2153,
	2149,
	2151,
	2152,
	2154,
	2154,
	2154,
	2155,
	2156,
	2157,
	1500,
	1500,
	1500,
	1500,
	1500,
	1500,
	1976,
	23,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	162,
	363,
	163,
	49,
	130,
	26,
	23,
	23,
	23,
	27,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	206,
	27,
	26,
	26,
	841,
	0,
	43,
	43,
	23,
	3,
	23,
	23,
	34,
	37,
	23,
	23,
	112,
	9,
	9,
	199,
	130,
	130,
	32,
	32,
	34,
	32,
	34,
	32,
	1386,
	32,
	26,
	32,
	32,
	2178,
	2178,
	130,
	23,
	23,
	23,
	26,
	32,
	23,
	38,
	199,
	38,
	59,
	152,
	38,
	199,
	1067,
	2179,
	38,
	129,
	130,
	2180,
	903,
	2180,
	903,
	28,
	2181,
	2116,
	32,
	32,
	32,
	130,
	26,
	26,
	720,
	1442,
	2182,
	35,
	26,
	30,
	199,
	32,
	26,
	28,
	28,
	23,
	23,
	23,
	26,
	1363,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	2183,
	1257,
	813,
	2184,
	1589,
	1894,
	1894,
	30,
	2185,
	2186,
	2187,
	2188,
	32,
	32,
	32,
	23,
	23,
	32,
	1386,
	1386,
	32,
	32,
	32,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	133,
	34,
	32,
	32,
	129,
	32,
	23,
	14,
	2189,
	2085,
	2190,
	2191,
	34,
	62,
	1442,
	2192,
	1338,
	23,
	23,
	62,
	23,
	32,
	32,
	130,
	118,
	32,
	2193,
	2194,
	32,
	32,
	2195,
	107,
	26,
	360,
	62,
	27,
	2196,
	130,
	448,
	32,
	2197,
	2198,
	23,
	32,
	32,
	133,
	129,
	129,
	26,
	2199,
	2199,
	2199,
	133,
	2199,
	130,
	26,
	199,
	606,
	606,
	199,
	34,
	34,
	199,
	32,
	199,
	27,
	27,
	23,
	3,
	23,
	23,
	23,
	30,
	2200,
	2200,
	627,
	34,
	62,
	1257,
	32,
	2185,
	2201,
	2202,
	2189,
	23,
	23,
	23,
	23,
	23,
	23,
	720,
	720,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	10,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1257,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	35,
	35,
	55,
	606,
	2203,
	32,
	130,
	35,
	130,
	2204,
	2204,
	2205,
	2206,
	2207,
	2208,
	2209,
	2210,
	38,
	129,
	199,
	35,
	130,
	199,
	34,
	32,
	32,
	34,
	34,
	34,
	130,
	26,
	28,
	2211,
	2211,
	360,
	360,
	136,
	136,
	32,
	34,
	34,
	23,
	23,
	23,
	32,
	32,
	32,
	1386,
	32,
	32,
	32,
	32,
	133,
	2212,
	38,
	200,
	1304,
	2213,
	129,
	1363,
	129,
	133,
	133,
	1335,
	1335,
	1335,
	1335,
	1335,
	1335,
	1335,
	1335,
	1335,
	1335,
	1335,
	1335,
	1335,
	1335,
	1335,
	23,
	27,
	3,
	43,
	43,
	295,
	43,
	43,
	0,
	119,
	119,
	0,
	3,
	3,
	163,
	137,
	0,
	137,
	0,
	4,
	0,
	23,
	3,
	23,
	2214,
	163,
	3,
	3,
	841,
	163,
	3,
	3,
	3,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	23,
	23,
	206,
	206,
	206,
	23,
	26,
	14,
	14,
	23,
	23,
	23,
	23,
	3,
	1477,
	163,
	23,
	23,
	23,
	23,
	23,
	23,
	4,
	720,
	720,
	720,
	720,
	14,
	89,
	23,
	23,
	23,
	23,
	26,
	23,
	26,
	26,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	10,
	23,
	23,
	23,
	23,
	23,
	23,
	58,
	35,
	23,
	23,
	54,
	35,
	2215,
	129,
	1257,
	129,
	23,
	23,
	23,
	23,
	23,
	23,
	612,
	114,
	612,
	2216,
	160,
	2217,
	161,
	2218,
	1526,
	2219,
	2220,
	1978,
	2221,
	2222,
	1979,
	2223,
	2224,
	1976,
	2225,
	135,
	0,
	2226,
	135,
	0,
	546,
	135,
	135,
	135,
	135,
	135,
	135,
	-1,
	204,
	204,
	204,
	204,
	204,
	204,
	0,
	202,
	0,
	2227,
	0,
	2228,
	0,
	2229,
	0,
	2230,
	0,
	2231,
	-1,
	137,
	137,
	137,
	137,
	137,
	137,
	163,
	3,
	135,
	2232,
	480,
	571,
	94,
	163,
	163,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	720,
	720,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	14,
	23,
	2036,
	23,
	2036,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	23,
	26,
	26,
	492,
	492,
	35,
	35,
	118,
	23,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	492,
	492,
	35,
	35,
	118,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	26,
	23,
	26,
	26,
	26,
	26,
	32,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	32,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	28,
	28,
	23,
	23,
	23,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	28,
	14,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	26,
	23,
	1878,
	28,
	28,
	332,
	9,
	1878,
	28,
	28,
	9,
	28,
	332,
	28,
	23,
	10,
	28,
	27,
	26,
	23,
	28,
	-1,
	28,
	28,
	-1,
	105,
	105,
	-1,
	213,
	213,
	28,
	-1,
	28,
	28,
	-1,
	28,
	28,
	-1,
	28,
	10,
	23,
	332,
	10,
	14,
	26,
	14,
	26,
	163,
	163,
	4,
	10,
	14,
	26,
	10,
	32,
	23,
	26,
	106,
	27,
	27,
	27,
	206,
	27,
	2234,
	23,
	26,
	23,
	332,
	26,
	23,
	332,
	27,
	26,
	23,
	28,
	28,
	-1,
	28,
	28,
	-1,
	105,
	105,
	-1,
	213,
	213,
	206,
	27,
	28,
	-1,
	0,
	0,
	28,
	0,
	0,
	28,
	-1,
	0,
	0,
	4,
	0,
	28,
	28,
	-1,
	28,
	137,
	3,
	2235,
	26,
	26,
	26,
	-1,
	-1,
	-1,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	26,
	27,
	23,
	26,
	27,
	-1,
	-1,
	-1,
	1,
	0,
	0,
	2238,
	1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	89,
	31,
	89,
	31,
	181,
	208,
	14,
	26,
	14,
	26,
	23,
	26,
	27,
	2239,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	770,
	2240,
	14,
	26,
	10,
	32,
	720,
	332,
	14,
	26,
	89,
	31,
	1089,
	1090,
	1089,
	1090,
	770,
	2240,
	89,
	31,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	720,
	181,
	720,
	181,
	28,
	89,
	31,
	89,
	31,
	23,
	23,
	14,
	26,
	26,
	181,
	14,
	14,
	14,
	14,
	14,
	4,
	0,
	4,
	163,
	3,
	137,
	-1,
	137,
	137,
	-1,
	-1,
	-1,
	-1,
	195,
	195,
	137,
	-1,
	-1,
	-1,
	195,
	195,
	137,
	-1,
	-1,
	-1,
	137,
	137,
	137,
	137,
	0,
	-1,
	0,
	0,
	-1,
	-1,
	-1,
	-1,
	1,
	1,
	0,
	-1,
	-1,
	-1,
	1,
	1,
	0,
	-1,
	-1,
	-1,
	0,
	0,
	0,
	0,
	-1,
	-1,
	692,
	2112,
	1,
	0,
	135,
	14,
	23,
	14,
	23,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	14,
	14,
	14,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	332,
	23,
	23,
	2241,
	332,
	23,
	1363,
	23,
	23,
	23,
	23,
	23,
	-1,
	-1,
	23,
	14,
	23,
	23,
	23,
	26,
	23,
	14,
	14,
	14,
	23,
	23,
	23,
	26,
	26,
	26,
	157,
	23,
	14,
	14,
	26,
	27,
	1995,
	2242,
	23,
	14,
	14,
	23,
	23,
	23,
	26,
	23,
	14,
	14,
	2243,
	23,
	23,
	26,
	23,
	14,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	27,
	26,
	26,
	26,
	23,
	14,
	23,
	23,
	23,
	26,
	23,
	26,
	23,
	23,
	10,
	37,
	1358,
	2244,
	2244,
	1352,
	2245,
	2246,
	2247,
	2248,
	2249,
	56,
	2250,
	23,
	3,
	2251,
	1766,
	2252,
	2253,
	23,
	23,
	23,
	2241,
	2241,
	332,
	332,
	23,
	23,
	23,
	23,
	23,
	23,
	2241,
	2241,
	1355,
	23,
	2254,
	2255,
	2256,
	135,
	3,
	26,
	26,
	1895,
	2257,
	27,
	27,
	23,
	23,
	23,
	23,
	3,
	14,
	14,
	14,
	14,
	31,
	89,
	89,
	106,
	14,
	14,
	2258,
	173,
	0,
	137,
	9,
	223,
	23,
	26,
	23,
	3,
	23,
	23,
	23,
	163,
	163,
	23,
	3,
	14,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	26,
	23,
	1383,
	23,
	23,
	26,
	23,
	1383,
	23,
	23,
	26,
	23,
	1383,
	23,
	26,
	915,
	27,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	1363,
	1363,
	23,
	89,
	720,
	89,
	89,
	89,
	1362,
	1362,
	1362,
	1362,
	1362,
	1362,
	2259,
	2259,
	2260,
	2260,
	2260,
	2261,
	2053,
	2053,
	2053,
	2053,
	2053,
	2262,
	2053,
	2262,
	2053,
	2053,
	2053,
	2053,
	2053,
	2053,
	2263,
	2263,
	2263,
	2263,
	2261,
	32,
	23,
	23,
	1483,
	1526,
	225,
	1483,
	1526,
	225,
	1483,
	1526,
	225,
	1483,
	1526,
	225,
	1483,
	1526,
	225,
	2264,
	2265,
	2266,
	1377,
	480,
	2267,
	2268,
	1377,
	480,
	2267,
	2268,
	1377,
	480,
	2267,
	2268,
	1377,
	480,
	2267,
	2268,
	1377,
	480,
	2267,
	2268,
	1377,
	480,
	2267,
	2268,
	432,
	2159,
	2269,
	432,
	2159,
	2269,
	1377,
	480,
	2270,
	2271,
	1377,
	480,
	2270,
	2271,
	2263,
	4,
	2272,
	2273,
	2274,
	23,
	3,
	1377,
	1377,
	106,
	2275,
	4,
	1377,
	49,
	49,
	1,
	433,
	2276,
	1969,
	2277,
	2278,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	2279,
	34,
	37,
	23,
	3,
	433,
	-1,
	23,
	23,
	23,
	2280,
	2281,
	1355,
	23,
	3,
	14,
	23,
	26,
	14,
	23,
	23,
	23,
	3,
	26,
	23,
	10,
	34,
	62,
	28,
	27,
	14,
	26,
	10,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	31,
	27,
	26,
	28,
	34,
	28,
	14,
	14,
	14,
	34,
	201,
	2282,
	14,
	2283,
	2284,
	452,
	333,
	10,
	32,
	720,
	332,
	89,
	31,
	14,
	14,
	0,
	0,
	98,
	279,
	97,
	480,
	43,
	94,
	808,
	114,
	2285,
	135,
	135,
	9,
	10,
	4,
	0,
	820,
	0,
	23,
	3,
	89,
	31,
	10,
	89,
	2282,
	34,
	62,
	28,
	27,
	10,
	27,
	34,
	28,
	14,
	201,
	23,
	89,
	31,
	10,
	89,
	2282,
	28,
	27,
	34,
	62,
	10,
	27,
	28,
	34,
	28,
	14,
	201,
	23,
	10,
	89,
	2282,
	14,
	26,
	26,
	201,
	9,
	10,
	10,
	89,
	2282,
	14,
	26,
	452,
	333,
	333,
	26,
	201,
	114,
	9,
	10,
	10,
	89,
	2282,
	14,
	26,
	89,
	31,
	31,
	26,
	201,
	9,
	10,
	4,
	23,
	10,
	89,
	2282,
	14,
	26,
	89,
	31,
	9,
	10,
	201,
	3,
	10,
	2282,
	26,
	27,
	26,
	34,
	62,
	28,
	27,
	26,
	27,
	135,
	135,
	9,
	10,
	10,
	32,
	720,
	332,
	452,
	333,
	89,
	31,
	14,
	14,
	201,
	0,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	3,
	23,
	26,
	26,
	26,
	26,
	26,
	23,
	26,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	23,
	26,
	23,
	23,
	23,
	26,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	26,
	23,
	26,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	26,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	26,
	26,
	23,
	26,
	26,
	129,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	332,
	23,
	23,
	23,
	1363,
	3,
	23,
	23,
	332,
	23,
	332,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	332,
	23,
	332,
	3,
	23,
	332,
	23,
	332,
	124,
	23,
	105,
	26,
	3,
	23,
	332,
	124,
	23,
	105,
	26,
	3,
	23,
	332,
	124,
	1354,
	105,
	1995,
	124,
	23,
	105,
	26,
	3,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	28,
	28,
	28,
	26,
	26,
	3,
	23,
	130,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	26,
	26,
	23,
	26,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	26,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	3,
	23,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	23,
	26,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	26,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	56,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	2233,
	23,
	2233,
	23,
	2236,
	23,
	2237,
	23,
	23,
	26,
	332,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	332,
	23,
	26,
	26,
	23,
	23,
	26,
	23,
	14,
	-1,
	-1,
	23,
	130,
	26,
	23,
	332,
	23,
	23,
	14,
	23,
	105,
	23,
	23,
	14,
	332,
	23,
	14,
	-1,
	-1,
	23,
	130,
	26,
	23,
	23,
	332,
	23,
	23,
	26,
	23,
	23,
	26,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	1363,
	23,
	23,
	23,
	23,
	26,
	3,
	23,
	23,
	23,
	23,
	332,
	23,
	23,
	23,
	23,
	23,
	332,
	23,
	23,
	23,
	332,
	23,
	23,
	1355,
	26,
	23,
	23,
	332,
	23,
	1355,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1365,
	23,
	-1,
	-1,
	-1,
	-1,
	89,
	2286,
	2287,
	2288,
	89,
	2286,
	2287,
	2289,
	14,
	89,
	2284,
	2286,
	2287,
	2289,
	14,
	89,
	2283,
	26,
	2288,
	14,
	89,
	23,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	14,
	32,
	23,
	89,
	23,
	23,
	14,
	23,
	14,
	14,
	14,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	14,
	14,
	23,
	2290,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	14,
	14,
};
static const Il2CppTokenRangePair s_rgctxIndices[89] = 
{
	{ 0x0200008F, { 6, 101 } },
	{ 0x0200009E, { 304, 2 } },
	{ 0x0200009F, { 306, 3 } },
	{ 0x020000A0, { 309, 4 } },
	{ 0x02000191, { 140, 1 } },
	{ 0x02000192, { 141, 1 } },
	{ 0x02000194, { 142, 4 } },
	{ 0x02000195, { 146, 13 } },
	{ 0x02000196, { 159, 4 } },
	{ 0x02000197, { 163, 1 } },
	{ 0x02000198, { 164, 3 } },
	{ 0x02000199, { 167, 3 } },
	{ 0x0200019A, { 170, 1 } },
	{ 0x0200019B, { 171, 10 } },
	{ 0x0200019C, { 181, 3 } },
	{ 0x0200019D, { 184, 3 } },
	{ 0x0200019E, { 187, 1 } },
	{ 0x0200019F, { 188, 11 } },
	{ 0x020001A0, { 199, 2 } },
	{ 0x020001A1, { 201, 2 } },
	{ 0x020001A4, { 232, 3 } },
	{ 0x020001A5, { 235, 8 } },
	{ 0x020001A6, { 243, 10 } },
	{ 0x020001AD, { 279, 13 } },
	{ 0x020001B1, { 292, 3 } },
	{ 0x020001B8, { 295, 3 } },
	{ 0x020001BE, { 321, 4 } },
	{ 0x020001C0, { 331, 2 } },
	{ 0x020001C1, { 333, 2 } },
	{ 0x020001E3, { 404, 6 } },
	{ 0x0600002A, { 0, 3 } },
	{ 0x06000637, { 3, 2 } },
	{ 0x0600064A, { 5, 1 } },
	{ 0x06000734, { 107, 1 } },
	{ 0x06000742, { 108, 1 } },
	{ 0x06000745, { 109, 1 } },
	{ 0x06000748, { 110, 7 } },
	{ 0x0600074B, { 117, 6 } },
	{ 0x0600074E, { 123, 6 } },
	{ 0x06000752, { 129, 6 } },
	{ 0x0600075A, { 135, 5 } },
	{ 0x0600075F, { 203, 14 } },
	{ 0x06000760, { 217, 7 } },
	{ 0x06000761, { 224, 8 } },
	{ 0x060007AF, { 253, 1 } },
	{ 0x060007B2, { 254, 1 } },
	{ 0x060007B5, { 255, 7 } },
	{ 0x060007BB, { 262, 6 } },
	{ 0x060007C2, { 268, 6 } },
	{ 0x060007C9, { 274, 5 } },
	{ 0x060007D1, { 298, 2 } },
	{ 0x060007D2, { 300, 2 } },
	{ 0x060007D3, { 302, 2 } },
	{ 0x060007F0, { 313, 3 } },
	{ 0x060007F1, { 316, 3 } },
	{ 0x060007F2, { 319, 2 } },
	{ 0x060007F8, { 325, 3 } },
	{ 0x060007F9, { 328, 3 } },
	{ 0x060007FA, { 335, 1 } },
	{ 0x060007FB, { 336, 1 } },
	{ 0x060007FC, { 337, 2 } },
	{ 0x060007FD, { 339, 2 } },
	{ 0x06000854, { 341, 1 } },
	{ 0x06000857, { 342, 1 } },
	{ 0x06000858, { 343, 1 } },
	{ 0x06000859, { 344, 1 } },
	{ 0x0600085A, { 345, 1 } },
	{ 0x0600085E, { 346, 1 } },
	{ 0x0600085F, { 347, 1 } },
	{ 0x06000860, { 348, 1 } },
	{ 0x06000864, { 349, 1 } },
	{ 0x06000865, { 350, 1 } },
	{ 0x06000866, { 351, 1 } },
	{ 0x0600086C, { 352, 6 } },
	{ 0x0600086F, { 358, 1 } },
	{ 0x06000870, { 359, 6 } },
	{ 0x06000871, { 365, 1 } },
	{ 0x06000872, { 366, 6 } },
	{ 0x06000876, { 372, 1 } },
	{ 0x06000877, { 373, 1 } },
	{ 0x06000878, { 374, 6 } },
	{ 0x0600087C, { 380, 1 } },
	{ 0x0600087D, { 381, 1 } },
	{ 0x0600087E, { 382, 6 } },
	{ 0x06000883, { 388, 2 } },
	{ 0x06000884, { 390, 1 } },
	{ 0x060008B4, { 391, 5 } },
	{ 0x060008B5, { 396, 7 } },
	{ 0x060009EF, { 403, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[410] = 
{
	{ (Il2CppRGCTXDataType)3, 18362 },
	{ (Il2CppRGCTXDataType)3, 18363 },
	{ (Il2CppRGCTXDataType)3, 18364 },
	{ (Il2CppRGCTXDataType)2, 24179 },
	{ (Il2CppRGCTXDataType)3, 18365 },
	{ (Il2CppRGCTXDataType)3, 18366 },
	{ (Il2CppRGCTXDataType)3, 18367 },
	{ (Il2CppRGCTXDataType)3, 18368 },
	{ (Il2CppRGCTXDataType)2, 24408 },
	{ (Il2CppRGCTXDataType)3, 18369 },
	{ (Il2CppRGCTXDataType)3, 18370 },
	{ (Il2CppRGCTXDataType)3, 18371 },
	{ (Il2CppRGCTXDataType)2, 25576 },
	{ (Il2CppRGCTXDataType)3, 18372 },
	{ (Il2CppRGCTXDataType)3, 18373 },
	{ (Il2CppRGCTXDataType)2, 25577 },
	{ (Il2CppRGCTXDataType)3, 18374 },
	{ (Il2CppRGCTXDataType)3, 18375 },
	{ (Il2CppRGCTXDataType)3, 18376 },
	{ (Il2CppRGCTXDataType)3, 18377 },
	{ (Il2CppRGCTXDataType)3, 18378 },
	{ (Il2CppRGCTXDataType)3, 18379 },
	{ (Il2CppRGCTXDataType)2, 25578 },
	{ (Il2CppRGCTXDataType)3, 18380 },
	{ (Il2CppRGCTXDataType)3, 18381 },
	{ (Il2CppRGCTXDataType)3, 18382 },
	{ (Il2CppRGCTXDataType)3, 18383 },
	{ (Il2CppRGCTXDataType)3, 18384 },
	{ (Il2CppRGCTXDataType)3, 18385 },
	{ (Il2CppRGCTXDataType)3, 18386 },
	{ (Il2CppRGCTXDataType)3, 18387 },
	{ (Il2CppRGCTXDataType)3, 18388 },
	{ (Il2CppRGCTXDataType)3, 18389 },
	{ (Il2CppRGCTXDataType)3, 18390 },
	{ (Il2CppRGCTXDataType)3, 18391 },
	{ (Il2CppRGCTXDataType)3, 18392 },
	{ (Il2CppRGCTXDataType)2, 25579 },
	{ (Il2CppRGCTXDataType)3, 18393 },
	{ (Il2CppRGCTXDataType)3, 18394 },
	{ (Il2CppRGCTXDataType)3, 18395 },
	{ (Il2CppRGCTXDataType)3, 18396 },
	{ (Il2CppRGCTXDataType)3, 18397 },
	{ (Il2CppRGCTXDataType)3, 18398 },
	{ (Il2CppRGCTXDataType)3, 18399 },
	{ (Il2CppRGCTXDataType)2, 25580 },
	{ (Il2CppRGCTXDataType)3, 18400 },
	{ (Il2CppRGCTXDataType)2, 25581 },
	{ (Il2CppRGCTXDataType)3, 18401 },
	{ (Il2CppRGCTXDataType)3, 18402 },
	{ (Il2CppRGCTXDataType)3, 18403 },
	{ (Il2CppRGCTXDataType)3, 18404 },
	{ (Il2CppRGCTXDataType)3, 18405 },
	{ (Il2CppRGCTXDataType)3, 18406 },
	{ (Il2CppRGCTXDataType)3, 18407 },
	{ (Il2CppRGCTXDataType)2, 25582 },
	{ (Il2CppRGCTXDataType)3, 18408 },
	{ (Il2CppRGCTXDataType)3, 18409 },
	{ (Il2CppRGCTXDataType)3, 18410 },
	{ (Il2CppRGCTXDataType)2, 25583 },
	{ (Il2CppRGCTXDataType)3, 18411 },
	{ (Il2CppRGCTXDataType)3, 18412 },
	{ (Il2CppRGCTXDataType)3, 18413 },
	{ (Il2CppRGCTXDataType)3, 18414 },
	{ (Il2CppRGCTXDataType)3, 18415 },
	{ (Il2CppRGCTXDataType)3, 18416 },
	{ (Il2CppRGCTXDataType)3, 18417 },
	{ (Il2CppRGCTXDataType)2, 25584 },
	{ (Il2CppRGCTXDataType)3, 18418 },
	{ (Il2CppRGCTXDataType)3, 18419 },
	{ (Il2CppRGCTXDataType)2, 24417 },
	{ (Il2CppRGCTXDataType)3, 18420 },
	{ (Il2CppRGCTXDataType)3, 18421 },
	{ (Il2CppRGCTXDataType)3, 18422 },
	{ (Il2CppRGCTXDataType)2, 25581 },
	{ (Il2CppRGCTXDataType)2, 25585 },
	{ (Il2CppRGCTXDataType)3, 18423 },
	{ (Il2CppRGCTXDataType)3, 18424 },
	{ (Il2CppRGCTXDataType)3, 18425 },
	{ (Il2CppRGCTXDataType)3, 18426 },
	{ (Il2CppRGCTXDataType)2, 25586 },
	{ (Il2CppRGCTXDataType)2, 25587 },
	{ (Il2CppRGCTXDataType)2, 25586 },
	{ (Il2CppRGCTXDataType)3, 18427 },
	{ (Il2CppRGCTXDataType)3, 18428 },
	{ (Il2CppRGCTXDataType)3, 18429 },
	{ (Il2CppRGCTXDataType)2, 25588 },
	{ (Il2CppRGCTXDataType)3, 18430 },
	{ (Il2CppRGCTXDataType)3, 18431 },
	{ (Il2CppRGCTXDataType)2, 25589 },
	{ (Il2CppRGCTXDataType)3, 18432 },
	{ (Il2CppRGCTXDataType)3, 18433 },
	{ (Il2CppRGCTXDataType)3, 18434 },
	{ (Il2CppRGCTXDataType)2, 25590 },
	{ (Il2CppRGCTXDataType)3, 18435 },
	{ (Il2CppRGCTXDataType)3, 18436 },
	{ (Il2CppRGCTXDataType)2, 25591 },
	{ (Il2CppRGCTXDataType)3, 18437 },
	{ (Il2CppRGCTXDataType)3, 18438 },
	{ (Il2CppRGCTXDataType)3, 18439 },
	{ (Il2CppRGCTXDataType)3, 18440 },
	{ (Il2CppRGCTXDataType)2, 25592 },
	{ (Il2CppRGCTXDataType)3, 18441 },
	{ (Il2CppRGCTXDataType)3, 18442 },
	{ (Il2CppRGCTXDataType)2, 25593 },
	{ (Il2CppRGCTXDataType)3, 18443 },
	{ (Il2CppRGCTXDataType)3, 18444 },
	{ (Il2CppRGCTXDataType)3, 18445 },
	{ (Il2CppRGCTXDataType)3, 18446 },
	{ (Il2CppRGCTXDataType)3, 18447 },
	{ (Il2CppRGCTXDataType)3, 18448 },
	{ (Il2CppRGCTXDataType)2, 25594 },
	{ (Il2CppRGCTXDataType)3, 18449 },
	{ (Il2CppRGCTXDataType)2, 25595 },
	{ (Il2CppRGCTXDataType)3, 18450 },
	{ (Il2CppRGCTXDataType)3, 18451 },
	{ (Il2CppRGCTXDataType)3, 18452 },
	{ (Il2CppRGCTXDataType)3, 18453 },
	{ (Il2CppRGCTXDataType)2, 25596 },
	{ (Il2CppRGCTXDataType)3, 18454 },
	{ (Il2CppRGCTXDataType)3, 18455 },
	{ (Il2CppRGCTXDataType)2, 25597 },
	{ (Il2CppRGCTXDataType)3, 18456 },
	{ (Il2CppRGCTXDataType)3, 18457 },
	{ (Il2CppRGCTXDataType)2, 25598 },
	{ (Il2CppRGCTXDataType)3, 18458 },
	{ (Il2CppRGCTXDataType)3, 18459 },
	{ (Il2CppRGCTXDataType)2, 25599 },
	{ (Il2CppRGCTXDataType)3, 18460 },
	{ (Il2CppRGCTXDataType)3, 18461 },
	{ (Il2CppRGCTXDataType)2, 25600 },
	{ (Il2CppRGCTXDataType)3, 18462 },
	{ (Il2CppRGCTXDataType)3, 18463 },
	{ (Il2CppRGCTXDataType)2, 25601 },
	{ (Il2CppRGCTXDataType)3, 18464 },
	{ (Il2CppRGCTXDataType)3, 18465 },
	{ (Il2CppRGCTXDataType)2, 25602 },
	{ (Il2CppRGCTXDataType)3, 18466 },
	{ (Il2CppRGCTXDataType)3, 18467 },
	{ (Il2CppRGCTXDataType)3, 18468 },
	{ (Il2CppRGCTXDataType)3, 18469 },
	{ (Il2CppRGCTXDataType)3, 18470 },
	{ (Il2CppRGCTXDataType)3, 18471 },
	{ (Il2CppRGCTXDataType)3, 18472 },
	{ (Il2CppRGCTXDataType)3, 18473 },
	{ (Il2CppRGCTXDataType)3, 18474 },
	{ (Il2CppRGCTXDataType)3, 18475 },
	{ (Il2CppRGCTXDataType)3, 18476 },
	{ (Il2CppRGCTXDataType)3, 18477 },
	{ (Il2CppRGCTXDataType)2, 24476 },
	{ (Il2CppRGCTXDataType)3, 18478 },
	{ (Il2CppRGCTXDataType)2, 25603 },
	{ (Il2CppRGCTXDataType)3, 18479 },
	{ (Il2CppRGCTXDataType)3, 18480 },
	{ (Il2CppRGCTXDataType)3, 18481 },
	{ (Il2CppRGCTXDataType)3, 18482 },
	{ (Il2CppRGCTXDataType)3, 18483 },
	{ (Il2CppRGCTXDataType)3, 18484 },
	{ (Il2CppRGCTXDataType)3, 18485 },
	{ (Il2CppRGCTXDataType)3, 18486 },
	{ (Il2CppRGCTXDataType)3, 18487 },
	{ (Il2CppRGCTXDataType)3, 18488 },
	{ (Il2CppRGCTXDataType)3, 18489 },
	{ (Il2CppRGCTXDataType)3, 18490 },
	{ (Il2CppRGCTXDataType)3, 18491 },
	{ (Il2CppRGCTXDataType)3, 18492 },
	{ (Il2CppRGCTXDataType)3, 18493 },
	{ (Il2CppRGCTXDataType)2, 25604 },
	{ (Il2CppRGCTXDataType)3, 18494 },
	{ (Il2CppRGCTXDataType)3, 18495 },
	{ (Il2CppRGCTXDataType)2, 25605 },
	{ (Il2CppRGCTXDataType)3, 18496 },
	{ (Il2CppRGCTXDataType)2, 25606 },
	{ (Il2CppRGCTXDataType)3, 18497 },
	{ (Il2CppRGCTXDataType)3, 18498 },
	{ (Il2CppRGCTXDataType)2, 24510 },
	{ (Il2CppRGCTXDataType)3, 18499 },
	{ (Il2CppRGCTXDataType)2, 25607 },
	{ (Il2CppRGCTXDataType)3, 18500 },
	{ (Il2CppRGCTXDataType)3, 18501 },
	{ (Il2CppRGCTXDataType)3, 18502 },
	{ (Il2CppRGCTXDataType)3, 18503 },
	{ (Il2CppRGCTXDataType)3, 18504 },
	{ (Il2CppRGCTXDataType)3, 18505 },
	{ (Il2CppRGCTXDataType)3, 18506 },
	{ (Il2CppRGCTXDataType)3, 18507 },
	{ (Il2CppRGCTXDataType)3, 18508 },
	{ (Il2CppRGCTXDataType)2, 25609 },
	{ (Il2CppRGCTXDataType)3, 18509 },
	{ (Il2CppRGCTXDataType)2, 25610 },
	{ (Il2CppRGCTXDataType)3, 18510 },
	{ (Il2CppRGCTXDataType)3, 18511 },
	{ (Il2CppRGCTXDataType)2, 24532 },
	{ (Il2CppRGCTXDataType)3, 18512 },
	{ (Il2CppRGCTXDataType)2, 25611 },
	{ (Il2CppRGCTXDataType)3, 18513 },
	{ (Il2CppRGCTXDataType)3, 18514 },
	{ (Il2CppRGCTXDataType)3, 18515 },
	{ (Il2CppRGCTXDataType)3, 18516 },
	{ (Il2CppRGCTXDataType)3, 18517 },
	{ (Il2CppRGCTXDataType)3, 18518 },
	{ (Il2CppRGCTXDataType)3, 18519 },
	{ (Il2CppRGCTXDataType)3, 18520 },
	{ (Il2CppRGCTXDataType)3, 18521 },
	{ (Il2CppRGCTXDataType)2, 25612 },
	{ (Il2CppRGCTXDataType)3, 18522 },
	{ (Il2CppRGCTXDataType)2, 25613 },
	{ (Il2CppRGCTXDataType)3, 18523 },
	{ (Il2CppRGCTXDataType)3, 18524 },
	{ (Il2CppRGCTXDataType)2, 25614 },
	{ (Il2CppRGCTXDataType)3, 18525 },
	{ (Il2CppRGCTXDataType)2, 24555 },
	{ (Il2CppRGCTXDataType)3, 18526 },
	{ (Il2CppRGCTXDataType)3, 18527 },
	{ (Il2CppRGCTXDataType)2, 25615 },
	{ (Il2CppRGCTXDataType)3, 18528 },
	{ (Il2CppRGCTXDataType)2, 24557 },
	{ (Il2CppRGCTXDataType)3, 18529 },
	{ (Il2CppRGCTXDataType)3, 18530 },
	{ (Il2CppRGCTXDataType)3, 18531 },
	{ (Il2CppRGCTXDataType)2, 25617 },
	{ (Il2CppRGCTXDataType)3, 18532 },
	{ (Il2CppRGCTXDataType)2, 25618 },
	{ (Il2CppRGCTXDataType)3, 18533 },
	{ (Il2CppRGCTXDataType)3, 18534 },
	{ (Il2CppRGCTXDataType)3, 18535 },
	{ (Il2CppRGCTXDataType)3, 18536 },
	{ (Il2CppRGCTXDataType)3, 18537 },
	{ (Il2CppRGCTXDataType)2, 25622 },
	{ (Il2CppRGCTXDataType)3, 18538 },
	{ (Il2CppRGCTXDataType)2, 25623 },
	{ (Il2CppRGCTXDataType)3, 18539 },
	{ (Il2CppRGCTXDataType)3, 18540 },
	{ (Il2CppRGCTXDataType)3, 18541 },
	{ (Il2CppRGCTXDataType)3, 18542 },
	{ (Il2CppRGCTXDataType)3, 18543 },
	{ (Il2CppRGCTXDataType)2, 25625 },
	{ (Il2CppRGCTXDataType)3, 18544 },
	{ (Il2CppRGCTXDataType)2, 25625 },
	{ (Il2CppRGCTXDataType)3, 18545 },
	{ (Il2CppRGCTXDataType)3, 18546 },
	{ (Il2CppRGCTXDataType)3, 18547 },
	{ (Il2CppRGCTXDataType)3, 18548 },
	{ (Il2CppRGCTXDataType)3, 18549 },
	{ (Il2CppRGCTXDataType)2, 25626 },
	{ (Il2CppRGCTXDataType)3, 18550 },
	{ (Il2CppRGCTXDataType)2, 25626 },
	{ (Il2CppRGCTXDataType)3, 18551 },
	{ (Il2CppRGCTXDataType)3, 18552 },
	{ (Il2CppRGCTXDataType)3, 18553 },
	{ (Il2CppRGCTXDataType)3, 18554 },
	{ (Il2CppRGCTXDataType)3, 18555 },
	{ (Il2CppRGCTXDataType)3, 18556 },
	{ (Il2CppRGCTXDataType)3, 18557 },
	{ (Il2CppRGCTXDataType)3, 18558 },
	{ (Il2CppRGCTXDataType)3, 18559 },
	{ (Il2CppRGCTXDataType)2, 25627 },
	{ (Il2CppRGCTXDataType)3, 18560 },
	{ (Il2CppRGCTXDataType)2, 25628 },
	{ (Il2CppRGCTXDataType)3, 18561 },
	{ (Il2CppRGCTXDataType)3, 18562 },
	{ (Il2CppRGCTXDataType)3, 18563 },
	{ (Il2CppRGCTXDataType)3, 18564 },
	{ (Il2CppRGCTXDataType)2, 25629 },
	{ (Il2CppRGCTXDataType)3, 18565 },
	{ (Il2CppRGCTXDataType)3, 18566 },
	{ (Il2CppRGCTXDataType)2, 25630 },
	{ (Il2CppRGCTXDataType)3, 18567 },
	{ (Il2CppRGCTXDataType)3, 18568 },
	{ (Il2CppRGCTXDataType)2, 25631 },
	{ (Il2CppRGCTXDataType)3, 18569 },
	{ (Il2CppRGCTXDataType)3, 18570 },
	{ (Il2CppRGCTXDataType)2, 25632 },
	{ (Il2CppRGCTXDataType)3, 18571 },
	{ (Il2CppRGCTXDataType)3, 18572 },
	{ (Il2CppRGCTXDataType)2, 25633 },
	{ (Il2CppRGCTXDataType)3, 18573 },
	{ (Il2CppRGCTXDataType)3, 18574 },
	{ (Il2CppRGCTXDataType)3, 18575 },
	{ (Il2CppRGCTXDataType)3, 18576 },
	{ (Il2CppRGCTXDataType)3, 18577 },
	{ (Il2CppRGCTXDataType)3, 18578 },
	{ (Il2CppRGCTXDataType)2, 24699 },
	{ (Il2CppRGCTXDataType)3, 18579 },
	{ (Il2CppRGCTXDataType)2, 25634 },
	{ (Il2CppRGCTXDataType)3, 18580 },
	{ (Il2CppRGCTXDataType)3, 18581 },
	{ (Il2CppRGCTXDataType)3, 18582 },
	{ (Il2CppRGCTXDataType)3, 18583 },
	{ (Il2CppRGCTXDataType)3, 18584 },
	{ (Il2CppRGCTXDataType)3, 18585 },
	{ (Il2CppRGCTXDataType)3, 18586 },
	{ (Il2CppRGCTXDataType)3, 18587 },
	{ (Il2CppRGCTXDataType)3, 18588 },
	{ (Il2CppRGCTXDataType)3, 18589 },
	{ (Il2CppRGCTXDataType)2, 25635 },
	{ (Il2CppRGCTXDataType)3, 18590 },
	{ (Il2CppRGCTXDataType)3, 18591 },
	{ (Il2CppRGCTXDataType)2, 25636 },
	{ (Il2CppRGCTXDataType)2, 24756 },
	{ (Il2CppRGCTXDataType)3, 18592 },
	{ (Il2CppRGCTXDataType)2, 24760 },
	{ (Il2CppRGCTXDataType)3, 18593 },
	{ (Il2CppRGCTXDataType)2, 24765 },
	{ (Il2CppRGCTXDataType)3, 18594 },
	{ (Il2CppRGCTXDataType)3, 18595 },
	{ (Il2CppRGCTXDataType)3, 18596 },
	{ (Il2CppRGCTXDataType)3, 18597 },
	{ (Il2CppRGCTXDataType)3, 18598 },
	{ (Il2CppRGCTXDataType)3, 18599 },
	{ (Il2CppRGCTXDataType)3, 18600 },
	{ (Il2CppRGCTXDataType)3, 18601 },
	{ (Il2CppRGCTXDataType)3, 18602 },
	{ (Il2CppRGCTXDataType)3, 18603 },
	{ (Il2CppRGCTXDataType)2, 24796 },
	{ (Il2CppRGCTXDataType)2, 25637 },
	{ (Il2CppRGCTXDataType)3, 18604 },
	{ (Il2CppRGCTXDataType)2, 24799 },
	{ (Il2CppRGCTXDataType)2, 25638 },
	{ (Il2CppRGCTXDataType)3, 18605 },
	{ (Il2CppRGCTXDataType)2, 25639 },
	{ (Il2CppRGCTXDataType)3, 18606 },
	{ (Il2CppRGCTXDataType)2, 24807 },
	{ (Il2CppRGCTXDataType)2, 25640 },
	{ (Il2CppRGCTXDataType)3, 18607 },
	{ (Il2CppRGCTXDataType)3, 18608 },
	{ (Il2CppRGCTXDataType)2, 25641 },
	{ (Il2CppRGCTXDataType)3, 18609 },
	{ (Il2CppRGCTXDataType)3, 18610 },
	{ (Il2CppRGCTXDataType)2, 25642 },
	{ (Il2CppRGCTXDataType)3, 18611 },
	{ (Il2CppRGCTXDataType)3, 18612 },
	{ (Il2CppRGCTXDataType)3, 18613 },
	{ (Il2CppRGCTXDataType)3, 18614 },
	{ (Il2CppRGCTXDataType)3, 18615 },
	{ (Il2CppRGCTXDataType)3, 18616 },
	{ (Il2CppRGCTXDataType)3, 18617 },
	{ (Il2CppRGCTXDataType)3, 18618 },
	{ (Il2CppRGCTXDataType)2, 25645 },
	{ (Il2CppRGCTXDataType)3, 18619 },
	{ (Il2CppRGCTXDataType)2, 25646 },
	{ (Il2CppRGCTXDataType)3, 18620 },
	{ (Il2CppRGCTXDataType)3, 18621 },
	{ (Il2CppRGCTXDataType)3, 18622 },
	{ (Il2CppRGCTXDataType)3, 18623 },
	{ (Il2CppRGCTXDataType)3, 18624 },
	{ (Il2CppRGCTXDataType)3, 18625 },
	{ (Il2CppRGCTXDataType)3, 18626 },
	{ (Il2CppRGCTXDataType)3, 18627 },
	{ (Il2CppRGCTXDataType)3, 18628 },
	{ (Il2CppRGCTXDataType)3, 18629 },
	{ (Il2CppRGCTXDataType)3, 18630 },
	{ (Il2CppRGCTXDataType)3, 18631 },
	{ (Il2CppRGCTXDataType)2, 25647 },
	{ (Il2CppRGCTXDataType)3, 18632 },
	{ (Il2CppRGCTXDataType)3, 18633 },
	{ (Il2CppRGCTXDataType)2, 25648 },
	{ (Il2CppRGCTXDataType)3, 18634 },
	{ (Il2CppRGCTXDataType)3, 18635 },
	{ (Il2CppRGCTXDataType)3, 18636 },
	{ (Il2CppRGCTXDataType)2, 25649 },
	{ (Il2CppRGCTXDataType)3, 18637 },
	{ (Il2CppRGCTXDataType)3, 18638 },
	{ (Il2CppRGCTXDataType)2, 25650 },
	{ (Il2CppRGCTXDataType)3, 18639 },
	{ (Il2CppRGCTXDataType)3, 18640 },
	{ (Il2CppRGCTXDataType)3, 18641 },
	{ (Il2CppRGCTXDataType)2, 25651 },
	{ (Il2CppRGCTXDataType)3, 18642 },
	{ (Il2CppRGCTXDataType)3, 18643 },
	{ (Il2CppRGCTXDataType)2, 25652 },
	{ (Il2CppRGCTXDataType)3, 18644 },
	{ (Il2CppRGCTXDataType)3, 18645 },
	{ (Il2CppRGCTXDataType)3, 18646 },
	{ (Il2CppRGCTXDataType)3, 18647 },
	{ (Il2CppRGCTXDataType)2, 25653 },
	{ (Il2CppRGCTXDataType)3, 18648 },
	{ (Il2CppRGCTXDataType)3, 18649 },
	{ (Il2CppRGCTXDataType)2, 25654 },
	{ (Il2CppRGCTXDataType)3, 18650 },
	{ (Il2CppRGCTXDataType)3, 18651 },
	{ (Il2CppRGCTXDataType)3, 18652 },
	{ (Il2CppRGCTXDataType)3, 18653 },
	{ (Il2CppRGCTXDataType)2, 25655 },
	{ (Il2CppRGCTXDataType)3, 18654 },
	{ (Il2CppRGCTXDataType)3, 18655 },
	{ (Il2CppRGCTXDataType)2, 25656 },
	{ (Il2CppRGCTXDataType)3, 18656 },
	{ (Il2CppRGCTXDataType)3, 18657 },
	{ (Il2CppRGCTXDataType)3, 18658 },
	{ (Il2CppRGCTXDataType)3, 18659 },
	{ (Il2CppRGCTXDataType)3, 18660 },
	{ (Il2CppRGCTXDataType)2, 24966 },
	{ (Il2CppRGCTXDataType)3, 18661 },
	{ (Il2CppRGCTXDataType)3, 18662 },
	{ (Il2CppRGCTXDataType)3, 18663 },
	{ (Il2CppRGCTXDataType)2, 24967 },
	{ (Il2CppRGCTXDataType)2, 24969 },
	{ (Il2CppRGCTXDataType)3, 18664 },
	{ (Il2CppRGCTXDataType)3, 18665 },
	{ (Il2CppRGCTXDataType)3, 18666 },
	{ (Il2CppRGCTXDataType)2, 24970 },
	{ (Il2CppRGCTXDataType)3, 18667 },
	{ (Il2CppRGCTXDataType)3, 18668 },
	{ (Il2CppRGCTXDataType)2, 25116 },
	{ (Il2CppRGCTXDataType)3, 18669 },
	{ (Il2CppRGCTXDataType)1, 25135 },
	{ (Il2CppRGCTXDataType)2, 25135 },
	{ (Il2CppRGCTXDataType)3, 18670 },
	{ (Il2CppRGCTXDataType)3, 18671 },
	{ (Il2CppRGCTXDataType)3, 18672 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	3639,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	89,
	s_rgctxIndices,
	410,
	s_rgctxValues,
	NULL,
};
