﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.ArgumentException
struct ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Byte[][]
struct ByteU5BU5DU5BU5D_t1DE3927D87FD236507BFE9CA7E3EEA348C53E0E1;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>
struct IDictionary_2_tF73B268C1272A960FC7C95CBC924725A88430258;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Exception
struct Exception_t;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.InvalidOperationException
struct InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// ZXing.Common.BitMatrix
struct BitMatrix_tB23698CCD2261D1983E1603CCAF19684444A1E32;
// ZXing.Common.EncodingOptions
struct EncodingOptions_tEC02060C7836454C4DB2D76154506A007E47C5FE;
// ZXing.QrCode.Internal.ByteMatrix
struct ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074;
// ZXing.QrCode.Internal.ErrorCorrectionLevel
struct ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6;
// ZXing.QrCode.Internal.ErrorCorrectionLevel[]
struct ErrorCorrectionLevelU5BU5D_tFCF20C826BCBF04DFC797C09F208BCC911954814;
// ZXing.QrCode.Internal.Mode
struct Mode_t0B2186932F702F8B697CD35A95550BBAB4899091;
// ZXing.QrCode.Internal.QRCode
struct QRCode_t3B3E8ADDB511765BD2A812755F54D666EBAEC199;
// ZXing.QrCode.Internal.Version
struct Version_t916EFACAD794FFB471EB5B40EBC67937AB123573;
// ZXing.QrCode.Internal.Version/ECB
struct ECB_t007E646D4E54F927A82C543F7EB9C57EF311F63E;
// ZXing.QrCode.Internal.Version/ECB[]
struct ECBU5BU5D_t75F6E60192DB1B0CC09F3384C8E35DCA0CB44784;
// ZXing.QrCode.Internal.Version/ECBlocks
struct ECBlocks_t98A23F9B6EE275E133181D28EB0FB4BB378253B9;
// ZXing.QrCode.QRCodeWriter
struct QRCodeWriter_t65A43B121E258F5C8B18F77A406F231C2D86E7DF;
// ZXing.QrCode.QrCodeEncodingOptions
struct QrCodeEncodingOptions_t8B2BF4DDE5125CDE1E4C768A41EEF650E68AE300;
// ZXing.WriterException
struct WriterException_t75BB88103D8CEF3A0F0DCCA4C44B05BCC724137D;

IL2CPP_EXTERN_C RuntimeClass* ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BarcodeFormat_t590C5FC7F33F6F279F97CF1071AB576A9B1D6734_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BitMatrix_tB23698CCD2261D1983E1603CCAF19684444A1E32_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Encoder_tDB4D921C79CC3B37B103C18E794373662C11204E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDictionary_2_tF73B268C1272A960FC7C95CBC924725A88430258_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tFB388E53C7FDC6FCCF9A19ABF5A4E521FBD52E19_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral6AA687B810E5DC0D39756EE0C221E7B274C32588;
IL2CPP_EXTERN_C String_t* _stringLiteral6FE3CBB8F24B4BABEDC09BB0C1E5E185A07BCB46;
IL2CPP_EXTERN_C String_t* _stringLiteral7ABCE570D68EE8A7E3D50B8FA5E7AAD157BF08B9;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1__ctor_m11F9C228CFDF836DDFCD7880C09CB4098AB9D7F2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_get_HasValue_mB664E2C41CADA8413EF8842E6601B8C696A7CE15_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_get_Value_mA8BB683CA6A8C5BF448A737FB5A2AF63C730B3E5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* QRCodeWriter_encode_m2BF5BEE9968D266156FA1F71BB5167D1CA3F76FA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* QRCodeWriter_renderResult_mD16F8C4BA46AEFAF7593E56CFC80CADEF7EC3371_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t QRCodeWriter_encode_m2BF5BEE9968D266156FA1F71BB5167D1CA3F76FA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t QRCodeWriter_renderResult_mD16F8C4BA46AEFAF7593E56CFC80CADEF7EC3371_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SupportClass_ToBinaryString_mF93C2C2E4649147F9B8DBD4CDBBFBA9A2E697171_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WriterException__ctor_m4AD7D8048A426E901C4322D3A55233C831B5C6F2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WriterException__ctor_m984B83060D67D633A9496118D6CA5E07CCF055E3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WriterException__ctor_mA8B31A4537F9BBE1047525E91F2E9691C033E243_MetadataUsageId;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct ECBU5BU5D_t75F6E60192DB1B0CC09F3384C8E35DCA0CB44784;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// ZXing.Common.BitMatrix
struct  BitMatrix_tB23698CCD2261D1983E1603CCAF19684444A1E32  : public RuntimeObject
{
public:
	// System.Int32 ZXing.Common.BitMatrix::width
	int32_t ___width_0;
	// System.Int32 ZXing.Common.BitMatrix::height
	int32_t ___height_1;
	// System.Int32 ZXing.Common.BitMatrix::rowSize
	int32_t ___rowSize_2;
	// System.Int32[] ZXing.Common.BitMatrix::bits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___bits_3;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(BitMatrix_tB23698CCD2261D1983E1603CCAF19684444A1E32, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(BitMatrix_tB23698CCD2261D1983E1603CCAF19684444A1E32, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_rowSize_2() { return static_cast<int32_t>(offsetof(BitMatrix_tB23698CCD2261D1983E1603CCAF19684444A1E32, ___rowSize_2)); }
	inline int32_t get_rowSize_2() const { return ___rowSize_2; }
	inline int32_t* get_address_of_rowSize_2() { return &___rowSize_2; }
	inline void set_rowSize_2(int32_t value)
	{
		___rowSize_2 = value;
	}

	inline static int32_t get_offset_of_bits_3() { return static_cast<int32_t>(offsetof(BitMatrix_tB23698CCD2261D1983E1603CCAF19684444A1E32, ___bits_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_bits_3() const { return ___bits_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_bits_3() { return &___bits_3; }
	inline void set_bits_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___bits_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bits_3), (void*)value);
	}
};


// ZXing.Common.EncodingOptions
struct  EncodingOptions_tEC02060C7836454C4DB2D76154506A007E47C5FE  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object> ZXing.Common.EncodingOptions::<Hints>k__BackingField
	RuntimeObject* ___U3CHintsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CHintsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EncodingOptions_tEC02060C7836454C4DB2D76154506A007E47C5FE, ___U3CHintsU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CHintsU3Ek__BackingField_0() const { return ___U3CHintsU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CHintsU3Ek__BackingField_0() { return &___U3CHintsU3Ek__BackingField_0; }
	inline void set_U3CHintsU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CHintsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CHintsU3Ek__BackingField_0), (void*)value);
	}
};


// ZXing.QrCode.Internal.ByteMatrix
struct  ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074  : public RuntimeObject
{
public:
	// System.Byte[][] ZXing.QrCode.Internal.ByteMatrix::bytes
	ByteU5BU5DU5BU5D_t1DE3927D87FD236507BFE9CA7E3EEA348C53E0E1* ___bytes_0;
	// System.Int32 ZXing.QrCode.Internal.ByteMatrix::width
	int32_t ___width_1;
	// System.Int32 ZXing.QrCode.Internal.ByteMatrix::height
	int32_t ___height_2;

public:
	inline static int32_t get_offset_of_bytes_0() { return static_cast<int32_t>(offsetof(ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074, ___bytes_0)); }
	inline ByteU5BU5DU5BU5D_t1DE3927D87FD236507BFE9CA7E3EEA348C53E0E1* get_bytes_0() const { return ___bytes_0; }
	inline ByteU5BU5DU5BU5D_t1DE3927D87FD236507BFE9CA7E3EEA348C53E0E1** get_address_of_bytes_0() { return &___bytes_0; }
	inline void set_bytes_0(ByteU5BU5DU5BU5D_t1DE3927D87FD236507BFE9CA7E3EEA348C53E0E1* value)
	{
		___bytes_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bytes_0), (void*)value);
	}

	inline static int32_t get_offset_of_width_1() { return static_cast<int32_t>(offsetof(ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074, ___width_1)); }
	inline int32_t get_width_1() const { return ___width_1; }
	inline int32_t* get_address_of_width_1() { return &___width_1; }
	inline void set_width_1(int32_t value)
	{
		___width_1 = value;
	}

	inline static int32_t get_offset_of_height_2() { return static_cast<int32_t>(offsetof(ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074, ___height_2)); }
	inline int32_t get_height_2() const { return ___height_2; }
	inline int32_t* get_address_of_height_2() { return &___height_2; }
	inline void set_height_2(int32_t value)
	{
		___height_2 = value;
	}
};


// ZXing.QrCode.Internal.ErrorCorrectionLevel
struct  ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6  : public RuntimeObject
{
public:
	// System.Int32 ZXing.QrCode.Internal.ErrorCorrectionLevel::bits
	int32_t ___bits_5;
	// System.Int32 ZXing.QrCode.Internal.ErrorCorrectionLevel::ordinal_Renamed_Field
	int32_t ___ordinal_Renamed_Field_6;
	// System.String ZXing.QrCode.Internal.ErrorCorrectionLevel::name
	String_t* ___name_7;

public:
	inline static int32_t get_offset_of_bits_5() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6, ___bits_5)); }
	inline int32_t get_bits_5() const { return ___bits_5; }
	inline int32_t* get_address_of_bits_5() { return &___bits_5; }
	inline void set_bits_5(int32_t value)
	{
		___bits_5 = value;
	}

	inline static int32_t get_offset_of_ordinal_Renamed_Field_6() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6, ___ordinal_Renamed_Field_6)); }
	inline int32_t get_ordinal_Renamed_Field_6() const { return ___ordinal_Renamed_Field_6; }
	inline int32_t* get_address_of_ordinal_Renamed_Field_6() { return &___ordinal_Renamed_Field_6; }
	inline void set_ordinal_Renamed_Field_6(int32_t value)
	{
		___ordinal_Renamed_Field_6 = value;
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_7), (void*)value);
	}
};

struct ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6_StaticFields
{
public:
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.ErrorCorrectionLevel::L
	ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * ___L_0;
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.ErrorCorrectionLevel::M
	ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * ___M_1;
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.ErrorCorrectionLevel::Q
	ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * ___Q_2;
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.ErrorCorrectionLevel::H
	ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * ___H_3;
	// ZXing.QrCode.Internal.ErrorCorrectionLevel[] ZXing.QrCode.Internal.ErrorCorrectionLevel::FOR_BITS
	ErrorCorrectionLevelU5BU5D_tFCF20C826BCBF04DFC797C09F208BCC911954814* ___FOR_BITS_4;

public:
	inline static int32_t get_offset_of_L_0() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6_StaticFields, ___L_0)); }
	inline ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * get_L_0() const { return ___L_0; }
	inline ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 ** get_address_of_L_0() { return &___L_0; }
	inline void set_L_0(ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * value)
	{
		___L_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___L_0), (void*)value);
	}

	inline static int32_t get_offset_of_M_1() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6_StaticFields, ___M_1)); }
	inline ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * get_M_1() const { return ___M_1; }
	inline ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 ** get_address_of_M_1() { return &___M_1; }
	inline void set_M_1(ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * value)
	{
		___M_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___M_1), (void*)value);
	}

	inline static int32_t get_offset_of_Q_2() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6_StaticFields, ___Q_2)); }
	inline ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * get_Q_2() const { return ___Q_2; }
	inline ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 ** get_address_of_Q_2() { return &___Q_2; }
	inline void set_Q_2(ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * value)
	{
		___Q_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Q_2), (void*)value);
	}

	inline static int32_t get_offset_of_H_3() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6_StaticFields, ___H_3)); }
	inline ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * get_H_3() const { return ___H_3; }
	inline ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 ** get_address_of_H_3() { return &___H_3; }
	inline void set_H_3(ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * value)
	{
		___H_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___H_3), (void*)value);
	}

	inline static int32_t get_offset_of_FOR_BITS_4() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6_StaticFields, ___FOR_BITS_4)); }
	inline ErrorCorrectionLevelU5BU5D_tFCF20C826BCBF04DFC797C09F208BCC911954814* get_FOR_BITS_4() const { return ___FOR_BITS_4; }
	inline ErrorCorrectionLevelU5BU5D_tFCF20C826BCBF04DFC797C09F208BCC911954814** get_address_of_FOR_BITS_4() { return &___FOR_BITS_4; }
	inline void set_FOR_BITS_4(ErrorCorrectionLevelU5BU5D_tFCF20C826BCBF04DFC797C09F208BCC911954814* value)
	{
		___FOR_BITS_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FOR_BITS_4), (void*)value);
	}
};


// ZXing.QrCode.Internal.QRCode
struct  QRCode_t3B3E8ADDB511765BD2A812755F54D666EBAEC199  : public RuntimeObject
{
public:
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.QRCode::<Mode>k__BackingField
	Mode_t0B2186932F702F8B697CD35A95550BBAB4899091 * ___U3CModeU3Ek__BackingField_1;
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.QRCode::<ECLevel>k__BackingField
	ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * ___U3CECLevelU3Ek__BackingField_2;
	// ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.QRCode::<Version>k__BackingField
	Version_t916EFACAD794FFB471EB5B40EBC67937AB123573 * ___U3CVersionU3Ek__BackingField_3;
	// System.Int32 ZXing.QrCode.Internal.QRCode::<MaskPattern>k__BackingField
	int32_t ___U3CMaskPatternU3Ek__BackingField_4;
	// ZXing.QrCode.Internal.ByteMatrix ZXing.QrCode.Internal.QRCode::<Matrix>k__BackingField
	ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074 * ___U3CMatrixU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CModeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(QRCode_t3B3E8ADDB511765BD2A812755F54D666EBAEC199, ___U3CModeU3Ek__BackingField_1)); }
	inline Mode_t0B2186932F702F8B697CD35A95550BBAB4899091 * get_U3CModeU3Ek__BackingField_1() const { return ___U3CModeU3Ek__BackingField_1; }
	inline Mode_t0B2186932F702F8B697CD35A95550BBAB4899091 ** get_address_of_U3CModeU3Ek__BackingField_1() { return &___U3CModeU3Ek__BackingField_1; }
	inline void set_U3CModeU3Ek__BackingField_1(Mode_t0B2186932F702F8B697CD35A95550BBAB4899091 * value)
	{
		___U3CModeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CModeU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CECLevelU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(QRCode_t3B3E8ADDB511765BD2A812755F54D666EBAEC199, ___U3CECLevelU3Ek__BackingField_2)); }
	inline ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * get_U3CECLevelU3Ek__BackingField_2() const { return ___U3CECLevelU3Ek__BackingField_2; }
	inline ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 ** get_address_of_U3CECLevelU3Ek__BackingField_2() { return &___U3CECLevelU3Ek__BackingField_2; }
	inline void set_U3CECLevelU3Ek__BackingField_2(ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * value)
	{
		___U3CECLevelU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CECLevelU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CVersionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(QRCode_t3B3E8ADDB511765BD2A812755F54D666EBAEC199, ___U3CVersionU3Ek__BackingField_3)); }
	inline Version_t916EFACAD794FFB471EB5B40EBC67937AB123573 * get_U3CVersionU3Ek__BackingField_3() const { return ___U3CVersionU3Ek__BackingField_3; }
	inline Version_t916EFACAD794FFB471EB5B40EBC67937AB123573 ** get_address_of_U3CVersionU3Ek__BackingField_3() { return &___U3CVersionU3Ek__BackingField_3; }
	inline void set_U3CVersionU3Ek__BackingField_3(Version_t916EFACAD794FFB471EB5B40EBC67937AB123573 * value)
	{
		___U3CVersionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CVersionU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CMaskPatternU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(QRCode_t3B3E8ADDB511765BD2A812755F54D666EBAEC199, ___U3CMaskPatternU3Ek__BackingField_4)); }
	inline int32_t get_U3CMaskPatternU3Ek__BackingField_4() const { return ___U3CMaskPatternU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CMaskPatternU3Ek__BackingField_4() { return &___U3CMaskPatternU3Ek__BackingField_4; }
	inline void set_U3CMaskPatternU3Ek__BackingField_4(int32_t value)
	{
		___U3CMaskPatternU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CMatrixU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(QRCode_t3B3E8ADDB511765BD2A812755F54D666EBAEC199, ___U3CMatrixU3Ek__BackingField_5)); }
	inline ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074 * get_U3CMatrixU3Ek__BackingField_5() const { return ___U3CMatrixU3Ek__BackingField_5; }
	inline ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074 ** get_address_of_U3CMatrixU3Ek__BackingField_5() { return &___U3CMatrixU3Ek__BackingField_5; }
	inline void set_U3CMatrixU3Ek__BackingField_5(ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074 * value)
	{
		___U3CMatrixU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CMatrixU3Ek__BackingField_5), (void*)value);
	}
};

struct QRCode_t3B3E8ADDB511765BD2A812755F54D666EBAEC199_StaticFields
{
public:
	// System.Int32 ZXing.QrCode.Internal.QRCode::NUM_MASK_PATTERNS
	int32_t ___NUM_MASK_PATTERNS_0;

public:
	inline static int32_t get_offset_of_NUM_MASK_PATTERNS_0() { return static_cast<int32_t>(offsetof(QRCode_t3B3E8ADDB511765BD2A812755F54D666EBAEC199_StaticFields, ___NUM_MASK_PATTERNS_0)); }
	inline int32_t get_NUM_MASK_PATTERNS_0() const { return ___NUM_MASK_PATTERNS_0; }
	inline int32_t* get_address_of_NUM_MASK_PATTERNS_0() { return &___NUM_MASK_PATTERNS_0; }
	inline void set_NUM_MASK_PATTERNS_0(int32_t value)
	{
		___NUM_MASK_PATTERNS_0 = value;
	}
};


// ZXing.QrCode.Internal.Version_ECB
struct  ECB_t007E646D4E54F927A82C543F7EB9C57EF311F63E  : public RuntimeObject
{
public:
	// System.Int32 ZXing.QrCode.Internal.Version_ECB::count
	int32_t ___count_0;
	// System.Int32 ZXing.QrCode.Internal.Version_ECB::dataCodewords
	int32_t ___dataCodewords_1;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(ECB_t007E646D4E54F927A82C543F7EB9C57EF311F63E, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_dataCodewords_1() { return static_cast<int32_t>(offsetof(ECB_t007E646D4E54F927A82C543F7EB9C57EF311F63E, ___dataCodewords_1)); }
	inline int32_t get_dataCodewords_1() const { return ___dataCodewords_1; }
	inline int32_t* get_address_of_dataCodewords_1() { return &___dataCodewords_1; }
	inline void set_dataCodewords_1(int32_t value)
	{
		___dataCodewords_1 = value;
	}
};


// ZXing.QrCode.Internal.Version_ECBlocks
struct  ECBlocks_t98A23F9B6EE275E133181D28EB0FB4BB378253B9  : public RuntimeObject
{
public:
	// System.Int32 ZXing.QrCode.Internal.Version_ECBlocks::ecCodewordsPerBlock
	int32_t ___ecCodewordsPerBlock_0;
	// ZXing.QrCode.Internal.Version_ECB[] ZXing.QrCode.Internal.Version_ECBlocks::ecBlocks
	ECBU5BU5D_t75F6E60192DB1B0CC09F3384C8E35DCA0CB44784* ___ecBlocks_1;

public:
	inline static int32_t get_offset_of_ecCodewordsPerBlock_0() { return static_cast<int32_t>(offsetof(ECBlocks_t98A23F9B6EE275E133181D28EB0FB4BB378253B9, ___ecCodewordsPerBlock_0)); }
	inline int32_t get_ecCodewordsPerBlock_0() const { return ___ecCodewordsPerBlock_0; }
	inline int32_t* get_address_of_ecCodewordsPerBlock_0() { return &___ecCodewordsPerBlock_0; }
	inline void set_ecCodewordsPerBlock_0(int32_t value)
	{
		___ecCodewordsPerBlock_0 = value;
	}

	inline static int32_t get_offset_of_ecBlocks_1() { return static_cast<int32_t>(offsetof(ECBlocks_t98A23F9B6EE275E133181D28EB0FB4BB378253B9, ___ecBlocks_1)); }
	inline ECBU5BU5D_t75F6E60192DB1B0CC09F3384C8E35DCA0CB44784* get_ecBlocks_1() const { return ___ecBlocks_1; }
	inline ECBU5BU5D_t75F6E60192DB1B0CC09F3384C8E35DCA0CB44784** get_address_of_ecBlocks_1() { return &___ecBlocks_1; }
	inline void set_ecBlocks_1(ECBU5BU5D_t75F6E60192DB1B0CC09F3384C8E35DCA0CB44784* value)
	{
		___ecBlocks_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ecBlocks_1), (void*)value);
	}
};


// ZXing.QrCode.QRCodeWriter
struct  QRCodeWriter_t65A43B121E258F5C8B18F77A406F231C2D86E7DF  : public RuntimeObject
{
public:

public:
};


// ZXing.SupportClass
struct  SupportClass_t3FD3063BF08611D2219F5D8B889439388BA5B360  : public RuntimeObject
{
public:

public:
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Char
struct  Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Nullable`1<System.Int32>
struct  Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// ZXing.QrCode.QrCodeEncodingOptions
struct  QrCodeEncodingOptions_t8B2BF4DDE5125CDE1E4C768A41EEF650E68AE300  : public EncodingOptions_tEC02060C7836454C4DB2D76154506A007E47C5FE
{
public:

public:
};


// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// ZXing.BarcodeFormat
struct  BarcodeFormat_t590C5FC7F33F6F279F97CF1071AB576A9B1D6734 
{
public:
	// System.Int32 ZXing.BarcodeFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BarcodeFormat_t590C5FC7F33F6F279F97CF1071AB576A9B1D6734, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// ZXing.EncodeHintType
struct  EncodeHintType_t995B6CF49CE934D0ED7CF44929DDD7257348B9E2 
{
public:
	// System.Int32 ZXing.EncodeHintType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EncodeHintType_t995B6CF49CE934D0ED7CF44929DDD7257348B9E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};


// ZXing.WriterException
struct  WriterException_t75BB88103D8CEF3A0F0DCCA4C44B05BCC724137D  : public Exception_t
{
public:

public:
};


// System.ArgumentException
struct  ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};


// System.InvalidOperationException
struct  InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// ZXing.QrCode.Internal.Version_ECB[]
struct ECBU5BU5D_t75F6E60192DB1B0CC09F3384C8E35DCA0CB44784  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ECB_t007E646D4E54F927A82C543F7EB9C57EF311F63E * m_Items[1];

public:
	inline ECB_t007E646D4E54F927A82C543F7EB9C57EF311F63E * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ECB_t007E646D4E54F927A82C543F7EB9C57EF311F63E ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ECB_t007E646D4E54F927A82C543F7EB9C57EF311F63E * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline ECB_t007E646D4E54F927A82C543F7EB9C57EF311F63E * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ECB_t007E646D4E54F927A82C543F7EB9C57EF311F63E ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ECB_t007E646D4E54F927A82C543F7EB9C57EF311F63E * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Nullable`1<System.Int32>::.ctor(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Nullable_1__ctor_m11F9C228CFDF836DDFCD7880C09CB4098AB9D7F2_gshared (Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<System.Int32>::get_HasValue()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_mB664E2C41CADA8413EF8842E6601B8C696A7CE15_gshared_inline (Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * __this, const RuntimeMethod* method);
// !0 System.Nullable`1<System.Int32>::get_Value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Nullable_1_get_Value_mA8BB683CA6A8C5BF448A737FB5A2AF63C730B3E5_gshared (Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * __this, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Int32 ZXing.QrCode.Internal.Version/ECB::get_Count()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t ECB_get_Count_m4FBB8FD4E11F2B8CE59D5EE756DF988D43C37D47_inline (ECB_t007E646D4E54F927A82C543F7EB9C57EF311F63E * __this, const RuntimeMethod* method);
// System.Int32 ZXing.QrCode.Internal.Version/ECBlocks::get_NumBlocks()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ECBlocks_get_NumBlocks_mB6635BF406786F37EE84719EF4F4EB5545EDF2E1 (ECBlocks_t98A23F9B6EE275E133181D28EB0FB4BB378253B9 * __this, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229 (String_t* ___value0, const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m9A85EF7FEFEC21DDD525A67E831D77278E5165B7 (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495 (RuntimeObject * ___arg00, RuntimeObject * ___arg11, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07 (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args0, const RuntimeMethod* method);
// System.Void System.Nullable`1<System.Int32>::.ctor(!0)
inline void Nullable_1__ctor_m11F9C228CFDF836DDFCD7880C09CB4098AB9D7F2 (Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * __this, int32_t ___value0, const RuntimeMethod* method)
{
	((  void (*) (Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB *, int32_t, const RuntimeMethod*))Nullable_1__ctor_m11F9C228CFDF836DDFCD7880C09CB4098AB9D7F2_gshared)(__this, ___value0, method);
}
// System.Boolean System.Nullable`1<System.Int32>::get_HasValue()
inline bool Nullable_1_get_HasValue_mB664E2C41CADA8413EF8842E6601B8C696A7CE15_inline (Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB *, const RuntimeMethod*))Nullable_1_get_HasValue_mB664E2C41CADA8413EF8842E6601B8C696A7CE15_gshared_inline)(__this, method);
}
// !0 System.Nullable`1<System.Int32>::get_Value()
inline int32_t Nullable_1_get_Value_mA8BB683CA6A8C5BF448A737FB5A2AF63C730B3E5 (Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB *, const RuntimeMethod*))Nullable_1_get_Value_mA8BB683CA6A8C5BF448A737FB5A2AF63C730B3E5_gshared)(__this, method);
}
// ZXing.QrCode.Internal.QRCode ZXing.QrCode.Internal.Encoder::encode(System.String,ZXing.QrCode.Internal.ErrorCorrectionLevel,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR QRCode_t3B3E8ADDB511765BD2A812755F54D666EBAEC199 * Encoder_encode_m7A8C877527E7D3F4365C54DE1B51438A67A616AF (String_t* ___content0, ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * ___ecLevel1, RuntimeObject* ___hints2, const RuntimeMethod* method);
// ZXing.Common.BitMatrix ZXing.QrCode.QRCodeWriter::renderResult(ZXing.QrCode.Internal.QRCode,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BitMatrix_tB23698CCD2261D1983E1603CCAF19684444A1E32 * QRCodeWriter_renderResult_mD16F8C4BA46AEFAF7593E56CFC80CADEF7EC3371 (QRCode_t3B3E8ADDB511765BD2A812755F54D666EBAEC199 * ___code0, int32_t ___width1, int32_t ___height2, int32_t ___quietZone3, const RuntimeMethod* method);
// ZXing.QrCode.Internal.ByteMatrix ZXing.QrCode.Internal.QRCode::get_Matrix()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074 * QRCode_get_Matrix_m1D78FE3793DCF8D820A92DB86653DA5C8068DB17_inline (QRCode_t3B3E8ADDB511765BD2A812755F54D666EBAEC199 * __this, const RuntimeMethod* method);
// System.Void System.InvalidOperationException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidOperationException__ctor_m1F94EA1226068BD1B7EAA1B836A59C99979F579E (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * __this, const RuntimeMethod* method);
// System.Int32 ZXing.QrCode.Internal.ByteMatrix::get_Width()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t ByteMatrix_get_Width_m930710557D0607CFFF125AC86B8E536684CA7B75_inline (ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074 * __this, const RuntimeMethod* method);
// System.Int32 ZXing.QrCode.Internal.ByteMatrix::get_Height()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t ByteMatrix_get_Height_m6FEEA80C7E4835A939F495C28FB903D37E7A1F27_inline (ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074 * __this, const RuntimeMethod* method);
// System.Int32 System.Math::Max(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Math_Max_mA99E48BB021F2E4B62D4EA9F52EA6928EED618A2 (int32_t ___val10, int32_t ___val21, const RuntimeMethod* method);
// System.Int32 System.Math::Min(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Math_Min_mC950438198519FB2B0260FCB91220847EE4BB525 (int32_t ___val10, int32_t ___val21, const RuntimeMethod* method);
// System.Void ZXing.Common.BitMatrix::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BitMatrix__ctor_m37A95608226171AF5ABB00C563ACB5C163680D7C (BitMatrix_tB23698CCD2261D1983E1603CCAF19684444A1E32 * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method);
// System.Int32 ZXing.QrCode.Internal.ByteMatrix::get_Item(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ByteMatrix_get_Item_m4F36BABC12EBEF956138502F8BD031279CA27D88 (ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method);
// System.Void ZXing.Common.BitMatrix::setRegion(System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BitMatrix_setRegion_m81FFADFC79822D450F24FAF1583E89FA90C6A162 (BitMatrix_tB23698CCD2261D1983E1603CCAF19684444A1E32 * __this, int32_t ___left0, int32_t ___top1, int32_t ___width2, int32_t ___height3, const RuntimeMethod* method);
// System.Void ZXing.Common.EncodingOptions::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EncodingOptions__ctor_m363D9FD3CD59DC41EE7C0C38D1B8012041DCC3F1 (EncodingOptions_tEC02060C7836454C4DB2D76154506A007E47C5FE * __this, const RuntimeMethod* method);
// System.Void System.Array::Reverse(System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Reverse_m2257A7D1D672441107CA66836FF6C136EA5E3C7B (RuntimeArray * ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method);
// System.String System.String::CreateString(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_CreateString_m394C06654854ADD4C51FF957BE0CC72EF52BAA96 (String_t* __this, CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___val0, const RuntimeMethod* method);
// System.Void System.Exception::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m5FEC89FBFACEEDCEE29CCFD44A85D72FC28EB0D1 (Exception_t * __this, const RuntimeMethod* method);
// System.Void System.Exception::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m89BADFF36C3B170013878726E07729D51AA9FBE0 (Exception_t * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void System.Exception::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m62590BC1925B7B354EBFD852E162CD170FEB861D (Exception_t * __this, String_t* ___message0, Exception_t * ___innerException1, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZXing.QrCode.Internal.Version_ECB::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ECB__ctor_mEEE7BB0A54D58BFAE0ED2EC3DBA48430602DD669 (ECB_t007E646D4E54F927A82C543F7EB9C57EF311F63E * __this, int32_t ___count0, int32_t ___dataCodewords1, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___count0;
		__this->set_count_0(L_0);
		int32_t L_1 = ___dataCodewords1;
		__this->set_dataCodewords_1(L_1);
		return;
	}
}
// System.Int32 ZXing.QrCode.Internal.Version_ECB::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ECB_get_Count_m4FBB8FD4E11F2B8CE59D5EE756DF988D43C37D47 (ECB_t007E646D4E54F927A82C543F7EB9C57EF311F63E * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_count_0();
		return L_0;
	}
}
// System.Int32 ZXing.QrCode.Internal.Version_ECB::get_DataCodewords()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ECB_get_DataCodewords_m694477F0AD79E182212DECF38DCD53C259019B21 (ECB_t007E646D4E54F927A82C543F7EB9C57EF311F63E * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_dataCodewords_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZXing.QrCode.Internal.Version_ECBlocks::.ctor(System.Int32,ZXing.QrCode.Internal.Version_ECB[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ECBlocks__ctor_m273BD850A3379D1335D32BC7EE1421CA6F60F6DA (ECBlocks_t98A23F9B6EE275E133181D28EB0FB4BB378253B9 * __this, int32_t ___ecCodewordsPerBlock0, ECBU5BU5D_t75F6E60192DB1B0CC09F3384C8E35DCA0CB44784* ___ecBlocks1, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___ecCodewordsPerBlock0;
		__this->set_ecCodewordsPerBlock_0(L_0);
		ECBU5BU5D_t75F6E60192DB1B0CC09F3384C8E35DCA0CB44784* L_1 = ___ecBlocks1;
		__this->set_ecBlocks_1(L_1);
		return;
	}
}
// System.Int32 ZXing.QrCode.Internal.Version_ECBlocks::get_ECCodewordsPerBlock()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ECBlocks_get_ECCodewordsPerBlock_m038DC27C12D5D308E469DB99571FD02B307C0B1B (ECBlocks_t98A23F9B6EE275E133181D28EB0FB4BB378253B9 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_ecCodewordsPerBlock_0();
		return L_0;
	}
}
// System.Int32 ZXing.QrCode.Internal.Version_ECBlocks::get_NumBlocks()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ECBlocks_get_NumBlocks_mB6635BF406786F37EE84719EF4F4EB5545EDF2E1 (ECBlocks_t98A23F9B6EE275E133181D28EB0FB4BB378253B9 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	ECB_t007E646D4E54F927A82C543F7EB9C57EF311F63E * V_1 = NULL;
	ECBU5BU5D_t75F6E60192DB1B0CC09F3384C8E35DCA0CB44784* V_2 = NULL;
	int32_t V_3 = 0;
	{
		V_0 = 0;
		ECBU5BU5D_t75F6E60192DB1B0CC09F3384C8E35DCA0CB44784* L_0 = __this->get_ecBlocks_1();
		V_2 = L_0;
		V_3 = 0;
		goto IL_0021;
	}

IL_0010:
	{
		ECBU5BU5D_t75F6E60192DB1B0CC09F3384C8E35DCA0CB44784* L_1 = V_2;
		int32_t L_2 = V_3;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		ECB_t007E646D4E54F927A82C543F7EB9C57EF311F63E * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_1 = L_4;
		int32_t L_5 = V_0;
		ECB_t007E646D4E54F927A82C543F7EB9C57EF311F63E * L_6 = V_1;
		NullCheck(L_6);
		int32_t L_7 = ECB_get_Count_m4FBB8FD4E11F2B8CE59D5EE756DF988D43C37D47_inline(L_6, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)L_7));
		int32_t L_8 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0021:
	{
		int32_t L_9 = V_3;
		ECBU5BU5D_t75F6E60192DB1B0CC09F3384C8E35DCA0CB44784* L_10 = V_2;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_10)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_11 = V_0;
		return L_11;
	}
}
// System.Int32 ZXing.QrCode.Internal.Version_ECBlocks::get_TotalECCodewords()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ECBlocks_get_TotalECCodewords_m7822705F7441B8531FFD48D55455AA9B80048F8C (ECBlocks_t98A23F9B6EE275E133181D28EB0FB4BB378253B9 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_ecCodewordsPerBlock_0();
		int32_t L_1 = ECBlocks_get_NumBlocks_mB6635BF406786F37EE84719EF4F4EB5545EDF2E1(__this, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_multiply((int32_t)L_0, (int32_t)L_1));
	}
}
// ZXing.QrCode.Internal.Version_ECB[] ZXing.QrCode.Internal.Version_ECBlocks::getECBlocks()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ECBU5BU5D_t75F6E60192DB1B0CC09F3384C8E35DCA0CB44784* ECBlocks_getECBlocks_mE3BCB1A131F4B969CA507F40B4BC943CB96A7C7C (ECBlocks_t98A23F9B6EE275E133181D28EB0FB4BB378253B9 * __this, const RuntimeMethod* method)
{
	{
		ECBU5BU5D_t75F6E60192DB1B0CC09F3384C8E35DCA0CB44784* L_0 = __this->get_ecBlocks_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZXing.QrCode.QRCodeWriter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void QRCodeWriter__ctor_mE79E7DD767D5CC5936047CEBD617FAB88B5F3DEA (QRCodeWriter_t65A43B121E258F5C8B18F77A406F231C2D86E7DF * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// ZXing.Common.BitMatrix ZXing.QrCode.QRCodeWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BitMatrix_tB23698CCD2261D1983E1603CCAF19684444A1E32 * QRCodeWriter_encode_m2BF5BEE9968D266156FA1F71BB5167D1CA3F76FA (QRCodeWriter_t65A43B121E258F5C8B18F77A406F231C2D86E7DF * __this, String_t* ___contents0, int32_t ___format1, int32_t ___width2, int32_t ___height3, RuntimeObject* ___hints4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QRCodeWriter_encode_m2BF5BEE9968D266156FA1F71BB5167D1CA3F76FA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * V_0 = NULL;
	int32_t V_1 = 0;
	ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * V_2 = NULL;
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  V_3;
	memset((&V_3), 0, sizeof(V_3));
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  V_4;
	memset((&V_4), 0, sizeof(V_4));
	QRCode_t3B3E8ADDB511765BD2A812755F54D666EBAEC199 * V_5 = NULL;
	ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * G_B11_0 = NULL;
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  G_B16_0;
	memset((&G_B16_0), 0, sizeof(G_B16_0));
	{
		String_t* L_0 = ___contents0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * L_2 = (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 *)il2cpp_codegen_object_new(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m9A85EF7FEFEC21DDD525A67E831D77278E5165B7(L_2, _stringLiteral6FE3CBB8F24B4BABEDC09BB0C1E5E185A07BCB46, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, QRCodeWriter_encode_m2BF5BEE9968D266156FA1F71BB5167D1CA3F76FA_RuntimeMethod_var);
	}

IL_0016:
	{
		int32_t L_3 = ___format1;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)2048))))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_4 = ___format1;
		int32_t L_5 = L_4;
		RuntimeObject * L_6 = Box(BarcodeFormat_t590C5FC7F33F6F279F97CF1071AB576A9B1D6734_il2cpp_TypeInfo_var, &L_5);
		String_t* L_7 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral6AA687B810E5DC0D39756EE0C221E7B274C32588, L_6, /*hidden argument*/NULL);
		ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * L_8 = (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 *)il2cpp_codegen_object_new(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m9A85EF7FEFEC21DDD525A67E831D77278E5165B7(L_8, L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, QRCodeWriter_encode_m2BF5BEE9968D266156FA1F71BB5167D1CA3F76FA_RuntimeMethod_var);
	}

IL_0037:
	{
		int32_t L_9 = ___width2;
		if ((((int32_t)L_9) < ((int32_t)0)))
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_10 = ___height3;
		if ((((int32_t)L_10) >= ((int32_t)0)))
		{
			goto IL_007c;
		}
	}

IL_0046:
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_12 = L_11;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral7ABCE570D68EE8A7E3D50B8FA5E7AAD157BF08B9);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral7ABCE570D68EE8A7E3D50B8FA5E7AAD157BF08B9);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_13 = L_12;
		int32_t L_14 = ___width2;
		int32_t L_15 = L_14;
		RuntimeObject * L_16 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_16);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_17 = L_13;
		Il2CppChar L_18 = ((Il2CppChar)((int32_t)120));
		RuntimeObject * L_19 = Box(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_19);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_20 = L_17;
		int32_t L_21 = ___height3;
		int32_t L_22 = L_21;
		RuntimeObject * L_23 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_23);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_23);
		String_t* L_24 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_20, /*hidden argument*/NULL);
		ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * L_25 = (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 *)il2cpp_codegen_object_new(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m9A85EF7FEFEC21DDD525A67E831D77278E5165B7(L_25, L_24, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_25, QRCodeWriter_encode_m2BF5BEE9968D266156FA1F71BB5167D1CA3F76FA_RuntimeMethod_var);
	}

IL_007c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6_il2cpp_TypeInfo_var);
		ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * L_26 = ((ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6_StaticFields*)il2cpp_codegen_static_fields_for(ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6_il2cpp_TypeInfo_var))->get_L_0();
		V_0 = L_26;
		V_1 = 4;
		RuntimeObject* L_27 = ___hints4;
		if (!L_27)
		{
			goto IL_00f7;
		}
	}
	{
		RuntimeObject* L_28 = ___hints4;
		NullCheck(L_28);
		bool L_29 = InterfaceFuncInvoker1< bool, int32_t >::Invoke(2 /* System.Boolean System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>::ContainsKey(!0) */, IDictionary_2_tF73B268C1272A960FC7C95CBC924725A88430258_il2cpp_TypeInfo_var, L_28, 3);
		if (!L_29)
		{
			goto IL_00aa;
		}
	}
	{
		RuntimeObject* L_30 = ___hints4;
		NullCheck(L_30);
		RuntimeObject * L_31 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(0 /* !1 System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>::get_Item(!0) */, IDictionary_2_tF73B268C1272A960FC7C95CBC924725A88430258_il2cpp_TypeInfo_var, L_30, 3);
		G_B11_0 = ((ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 *)CastclassSealed((RuntimeObject*)L_31, ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6_il2cpp_TypeInfo_var));
		goto IL_00ab;
	}

IL_00aa:
	{
		G_B11_0 = ((ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 *)(NULL));
	}

IL_00ab:
	{
		V_2 = G_B11_0;
		ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * L_32 = V_2;
		if (!L_32)
		{
			goto IL_00b4;
		}
	}
	{
		ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * L_33 = V_2;
		V_0 = L_33;
	}

IL_00b4:
	{
		RuntimeObject* L_34 = ___hints4;
		NullCheck(L_34);
		bool L_35 = InterfaceFuncInvoker1< bool, int32_t >::Invoke(2 /* System.Boolean System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>::ContainsKey(!0) */, IDictionary_2_tF73B268C1272A960FC7C95CBC924725A88430258_il2cpp_TypeInfo_var, L_34, 5);
		if (!L_35)
		{
			goto IL_00d8;
		}
	}
	{
		RuntimeObject* L_36 = ___hints4;
		NullCheck(L_36);
		RuntimeObject * L_37 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(0 /* !1 System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>::get_Item(!0) */, IDictionary_2_tF73B268C1272A960FC7C95CBC924725A88430258_il2cpp_TypeInfo_var, L_36, 5);
		Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  L_38;
		memset((&L_38), 0, sizeof(L_38));
		Nullable_1__ctor_m11F9C228CFDF836DDFCD7880C09CB4098AB9D7F2((&L_38), ((*(int32_t*)((int32_t*)UnBox(L_37, Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var)))), /*hidden argument*/Nullable_1__ctor_m11F9C228CFDF836DDFCD7880C09CB4098AB9D7F2_RuntimeMethod_var);
		G_B16_0 = L_38;
		goto IL_00e2;
	}

IL_00d8:
	{
		il2cpp_codegen_initobj((&V_4), sizeof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB ));
		Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  L_39 = V_4;
		G_B16_0 = L_39;
	}

IL_00e2:
	{
		V_3 = G_B16_0;
		bool L_40 = Nullable_1_get_HasValue_mB664E2C41CADA8413EF8842E6601B8C696A7CE15_inline((Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB *)(&V_3), /*hidden argument*/Nullable_1_get_HasValue_mB664E2C41CADA8413EF8842E6601B8C696A7CE15_RuntimeMethod_var);
		if (!L_40)
		{
			goto IL_00f7;
		}
	}
	{
		int32_t L_41 = Nullable_1_get_Value_mA8BB683CA6A8C5BF448A737FB5A2AF63C730B3E5((Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB *)(&V_3), /*hidden argument*/Nullable_1_get_Value_mA8BB683CA6A8C5BF448A737FB5A2AF63C730B3E5_RuntimeMethod_var);
		V_1 = L_41;
	}

IL_00f7:
	{
		String_t* L_42 = ___contents0;
		ErrorCorrectionLevel_t1061A652154174374D8F2ECE8BC9BB3FE00CF3B6 * L_43 = V_0;
		RuntimeObject* L_44 = ___hints4;
		IL2CPP_RUNTIME_CLASS_INIT(Encoder_tDB4D921C79CC3B37B103C18E794373662C11204E_il2cpp_TypeInfo_var);
		QRCode_t3B3E8ADDB511765BD2A812755F54D666EBAEC199 * L_45 = Encoder_encode_m7A8C877527E7D3F4365C54DE1B51438A67A616AF(L_42, L_43, L_44, /*hidden argument*/NULL);
		V_5 = L_45;
		QRCode_t3B3E8ADDB511765BD2A812755F54D666EBAEC199 * L_46 = V_5;
		int32_t L_47 = ___width2;
		int32_t L_48 = ___height3;
		int32_t L_49 = V_1;
		BitMatrix_tB23698CCD2261D1983E1603CCAF19684444A1E32 * L_50 = QRCodeWriter_renderResult_mD16F8C4BA46AEFAF7593E56CFC80CADEF7EC3371(L_46, L_47, L_48, L_49, /*hidden argument*/NULL);
		return L_50;
	}
}
// ZXing.Common.BitMatrix ZXing.QrCode.QRCodeWriter::renderResult(ZXing.QrCode.Internal.QRCode,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BitMatrix_tB23698CCD2261D1983E1603CCAF19684444A1E32 * QRCodeWriter_renderResult_mD16F8C4BA46AEFAF7593E56CFC80CADEF7EC3371 (QRCode_t3B3E8ADDB511765BD2A812755F54D666EBAEC199 * ___code0, int32_t ___width1, int32_t ___height2, int32_t ___quietZone3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (QRCodeWriter_renderResult_mD16F8C4BA46AEFAF7593E56CFC80CADEF7EC3371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	BitMatrix_tB23698CCD2261D1983E1603CCAF19684444A1E32 * V_10 = NULL;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	{
		QRCode_t3B3E8ADDB511765BD2A812755F54D666EBAEC199 * L_0 = ___code0;
		NullCheck(L_0);
		ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074 * L_1 = QRCode_get_Matrix_m1D78FE3793DCF8D820A92DB86653DA5C8068DB17_inline(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0013;
		}
	}
	{
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_3 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1F94EA1226068BD1B7EAA1B836A59C99979F579E(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, QRCodeWriter_renderResult_mD16F8C4BA46AEFAF7593E56CFC80CADEF7EC3371_RuntimeMethod_var);
	}

IL_0013:
	{
		ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = ByteMatrix_get_Width_m930710557D0607CFFF125AC86B8E536684CA7B75_inline(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074 * L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = ByteMatrix_get_Height_m6FEEA80C7E4835A939F495C28FB903D37E7A1F27_inline(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		int32_t L_8 = V_1;
		int32_t L_9 = ___quietZone3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)((int32_t)((int32_t)L_9<<(int32_t)1))));
		int32_t L_10 = V_2;
		int32_t L_11 = ___quietZone3;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)((int32_t)((int32_t)L_11<<(int32_t)1))));
		int32_t L_12 = ___width1;
		int32_t L_13 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tFB388E53C7FDC6FCCF9A19ABF5A4E521FBD52E19_il2cpp_TypeInfo_var);
		int32_t L_14 = Math_Max_mA99E48BB021F2E4B62D4EA9F52EA6928EED618A2(L_12, L_13, /*hidden argument*/NULL);
		V_5 = L_14;
		int32_t L_15 = ___height2;
		int32_t L_16 = V_4;
		int32_t L_17 = Math_Max_mA99E48BB021F2E4B62D4EA9F52EA6928EED618A2(L_15, L_16, /*hidden argument*/NULL);
		V_6 = L_17;
		int32_t L_18 = V_5;
		int32_t L_19 = V_3;
		int32_t L_20 = V_6;
		int32_t L_21 = V_4;
		int32_t L_22 = Math_Min_mC950438198519FB2B0260FCB91220847EE4BB525(((int32_t)((int32_t)L_18/(int32_t)L_19)), ((int32_t)((int32_t)L_20/(int32_t)L_21)), /*hidden argument*/NULL);
		V_7 = L_22;
		int32_t L_23 = V_5;
		int32_t L_24 = V_1;
		int32_t L_25 = V_7;
		V_8 = ((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_23, (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_24, (int32_t)L_25))))/(int32_t)2));
		int32_t L_26 = V_6;
		int32_t L_27 = V_2;
		int32_t L_28 = V_7;
		V_9 = ((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_26, (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_27, (int32_t)L_28))))/(int32_t)2));
		int32_t L_29 = V_5;
		int32_t L_30 = V_6;
		BitMatrix_tB23698CCD2261D1983E1603CCAF19684444A1E32 * L_31 = (BitMatrix_tB23698CCD2261D1983E1603CCAF19684444A1E32 *)il2cpp_codegen_object_new(BitMatrix_tB23698CCD2261D1983E1603CCAF19684444A1E32_il2cpp_TypeInfo_var);
		BitMatrix__ctor_m37A95608226171AF5ABB00C563ACB5C163680D7C(L_31, L_29, L_30, /*hidden argument*/NULL);
		V_10 = L_31;
		V_11 = 0;
		int32_t L_32 = V_9;
		V_12 = L_32;
		goto IL_00cb;
	}

IL_007e:
	{
		V_13 = 0;
		int32_t L_33 = V_8;
		V_14 = L_33;
		goto IL_00b6;
	}

IL_008a:
	{
		ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074 * L_34 = V_0;
		int32_t L_35 = V_13;
		int32_t L_36 = V_11;
		NullCheck(L_34);
		int32_t L_37 = ByteMatrix_get_Item_m4F36BABC12EBEF956138502F8BD031279CA27D88(L_34, L_35, L_36, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_37) == ((uint32_t)1))))
		{
			goto IL_00a9;
		}
	}
	{
		BitMatrix_tB23698CCD2261D1983E1603CCAF19684444A1E32 * L_38 = V_10;
		int32_t L_39 = V_14;
		int32_t L_40 = V_12;
		int32_t L_41 = V_7;
		int32_t L_42 = V_7;
		NullCheck(L_38);
		BitMatrix_setRegion_m81FFADFC79822D450F24FAF1583E89FA90C6A162(L_38, L_39, L_40, L_41, L_42, /*hidden argument*/NULL);
	}

IL_00a9:
	{
		int32_t L_43 = V_13;
		V_13 = ((int32_t)il2cpp_codegen_add((int32_t)L_43, (int32_t)1));
		int32_t L_44 = V_14;
		int32_t L_45 = V_7;
		V_14 = ((int32_t)il2cpp_codegen_add((int32_t)L_44, (int32_t)L_45));
	}

IL_00b6:
	{
		int32_t L_46 = V_13;
		int32_t L_47 = V_1;
		if ((((int32_t)L_46) < ((int32_t)L_47)))
		{
			goto IL_008a;
		}
	}
	{
		int32_t L_48 = V_11;
		V_11 = ((int32_t)il2cpp_codegen_add((int32_t)L_48, (int32_t)1));
		int32_t L_49 = V_12;
		int32_t L_50 = V_7;
		V_12 = ((int32_t)il2cpp_codegen_add((int32_t)L_49, (int32_t)L_50));
	}

IL_00cb:
	{
		int32_t L_51 = V_11;
		int32_t L_52 = V_2;
		if ((((int32_t)L_51) < ((int32_t)L_52)))
		{
			goto IL_007e;
		}
	}
	{
		BitMatrix_tB23698CCD2261D1983E1603CCAF19684444A1E32 * L_53 = V_10;
		return L_53;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZXing.QrCode.QrCodeEncodingOptions::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void QrCodeEncodingOptions__ctor_mE72E2B3C7D465893A5B4A173C2187F81EF95FECA (QrCodeEncodingOptions_t8B2BF4DDE5125CDE1E4C768A41EEF650E68AE300 * __this, const RuntimeMethod* method)
{
	{
		EncodingOptions__ctor_m363D9FD3CD59DC41EE7C0C38D1B8012041DCC3F1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String ZXing.SupportClass::ToBinaryString(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SupportClass_ToBinaryString_mF93C2C2E4649147F9B8DBD4CDBBFBA9A2E697171 (int32_t ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SupportClass_ToBinaryString_mF93C2C2E4649147F9B8DBD4CDBBFBA9A2E697171_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* G_B3_1 = NULL;
	int32_t G_B2_0 = 0;
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* G_B2_1 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B4_1 = 0;
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* G_B4_2 = NULL;
	{
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_0 = (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)SZArrayNew(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var, (uint32_t)((int32_t)32));
		V_0 = L_0;
		V_1 = 0;
		goto IL_002d;
	}

IL_000f:
	{
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_1 = V_0;
		int32_t L_2 = V_1;
		int32_t L_3 = L_2;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
		int32_t L_4 = ___x0;
		G_B2_0 = L_3;
		G_B2_1 = L_1;
		if ((!(((uint32_t)((int32_t)((int32_t)L_4&(int32_t)1))) == ((uint32_t)1))))
		{
			G_B3_0 = L_3;
			G_B3_1 = L_1;
			goto IL_0025;
		}
	}
	{
		G_B4_0 = ((int32_t)49);
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_0027;
	}

IL_0025:
	{
		G_B4_0 = ((int32_t)48);
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_0027:
	{
		NullCheck(G_B4_2);
		(G_B4_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B4_1), (Il2CppChar)G_B4_0);
		int32_t L_5 = ___x0;
		___x0 = ((int32_t)((int32_t)L_5>>(int32_t)1));
	}

IL_002d:
	{
		int32_t L_6 = ___x0;
		if (L_6)
		{
			goto IL_000f;
		}
	}
	{
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_7 = V_0;
		int32_t L_8 = V_1;
		Array_Reverse_m2257A7D1D672441107CA66836FF6C136EA5E3C7B((RuntimeArray *)(RuntimeArray *)L_7, 0, L_8, /*hidden argument*/NULL);
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_9 = V_0;
		String_t* L_10 = String_CreateString_m394C06654854ADD4C51FF957BE0CC72EF52BAA96(NULL, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZXing.WriterException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WriterException__ctor_m984B83060D67D633A9496118D6CA5E07CCF055E3 (WriterException_t75BB88103D8CEF3A0F0DCCA4C44B05BCC724137D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriterException__ctor_m984B83060D67D633A9496118D6CA5E07CCF055E3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m5FEC89FBFACEEDCEE29CCFD44A85D72FC28EB0D1(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZXing.WriterException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WriterException__ctor_mA8B31A4537F9BBE1047525E91F2E9691C033E243 (WriterException_t75BB88103D8CEF3A0F0DCCA4C44B05BCC724137D * __this, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriterException__ctor_mA8B31A4537F9BBE1047525E91F2E9691C033E243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m89BADFF36C3B170013878726E07729D51AA9FBE0(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZXing.WriterException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WriterException__ctor_m4AD7D8048A426E901C4322D3A55233C831B5C6F2 (WriterException_t75BB88103D8CEF3A0F0DCCA4C44B05BCC724137D * __this, String_t* ___message0, Exception_t * ___innerExc1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WriterException__ctor_m4AD7D8048A426E901C4322D3A55233C831B5C6F2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		Exception_t * L_1 = ___innerExc1;
		IL2CPP_RUNTIME_CLASS_INIT(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m62590BC1925B7B354EBFD852E162CD170FEB861D(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t ECB_get_Count_m4FBB8FD4E11F2B8CE59D5EE756DF988D43C37D47_inline (ECB_t007E646D4E54F927A82C543F7EB9C57EF311F63E * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_count_0();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074 * QRCode_get_Matrix_m1D78FE3793DCF8D820A92DB86653DA5C8068DB17_inline (QRCode_t3B3E8ADDB511765BD2A812755F54D666EBAEC199 * __this, const RuntimeMethod* method)
{
	{
		ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074 * L_0 = __this->get_U3CMatrixU3Ek__BackingField_5();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t ByteMatrix_get_Width_m930710557D0607CFFF125AC86B8E536684CA7B75_inline (ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_width_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t ByteMatrix_get_Height_m6FEEA80C7E4835A939F495C28FB903D37E7A1F27_inline (ByteMatrix_tB615EAD975DEDADD16C18A10EF2DFB1867989074 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_height_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_mB664E2C41CADA8413EF8842E6601B8C696A7CE15_gshared_inline (Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
