﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void ZXing.Aztec.AztecWriter::.cctor()
extern void AztecWriter__cctor_mF914CB9DB393C2FA5D1FC88E098A26F4BCFFC5DE ();
// 0x00000002 System.Void ZXing.Aztec.AztecWriter::.ctor()
extern void AztecWriter__ctor_mF9FDBBE7E3966A4EC3CC6765E0D4A065965B3740 ();
// 0x00000003 ZXing.Common.BitMatrix ZXing.Aztec.AztecWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void AztecWriter_encode_m7C66AF93FA3E95CDF572E4F8757780717A4BF5D0 ();
// 0x00000004 ZXing.Common.BitMatrix ZXing.Aztec.AztecWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Text.Encoding,System.Int32,System.Int32)
extern void AztecWriter_encode_m0CEDB4DE70F4BC3D10FAC763133BBB008C38E5CF ();
// 0x00000005 ZXing.Common.BitMatrix ZXing.Aztec.AztecWriter::renderResult(ZXing.Aztec.Internal.AztecCode,System.Int32,System.Int32)
extern void AztecWriter_renderResult_m9C9791074A5B1D60960ECEB7C0EF7BF2B4E59154 ();
// 0x00000006 System.Void ZXing.Aztec.Internal.AztecCode::.ctor()
extern void AztecCode__ctor_mF636662C532ACBAD7134194BF92FB7D057F03D3A ();
// 0x00000007 System.Void ZXing.Aztec.Internal.AztecCode::set_isCompact(System.Boolean)
extern void AztecCode_set_isCompact_m710918DC850E7099DE68C013DF97197A309F2B66 ();
// 0x00000008 System.Void ZXing.Aztec.Internal.AztecCode::set_Size(System.Int32)
extern void AztecCode_set_Size_m35C1726D043706642500A6A6EDAB6D3A08020377 ();
// 0x00000009 System.Void ZXing.Aztec.Internal.AztecCode::set_Layers(System.Int32)
extern void AztecCode_set_Layers_m5FB29256969798E1E5A64680B796B91841576B7D ();
// 0x0000000A System.Void ZXing.Aztec.Internal.AztecCode::set_CodeWords(System.Int32)
extern void AztecCode_set_CodeWords_mD32CC8A7A886E1EDC04F7825405AEA9CA918EB12 ();
// 0x0000000B ZXing.Common.BitMatrix ZXing.Aztec.Internal.AztecCode::get_Matrix()
extern void AztecCode_get_Matrix_m7522E62499B97454D10F0C58CCEB2203BC70797D ();
// 0x0000000C System.Void ZXing.Aztec.Internal.AztecCode::set_Matrix(ZXing.Common.BitMatrix)
extern void AztecCode_set_Matrix_m290313F9EF40D642DA239ABD8669A34FE75F7353 ();
// 0x0000000D System.Void ZXing.Aztec.Internal.BinaryShiftToken::.ctor(ZXing.Aztec.Internal.Token,System.Int32,System.Int32)
extern void BinaryShiftToken__ctor_m29F17C3CE33045036754AC46485E075B85EFF793 ();
// 0x0000000E System.Void ZXing.Aztec.Internal.BinaryShiftToken::appendTo(ZXing.Common.BitArray,System.Byte[])
extern void BinaryShiftToken_appendTo_m0BDB93A3C838AA40C0A9333F07F212A667DA8B34 ();
// 0x0000000F System.String ZXing.Aztec.Internal.BinaryShiftToken::ToString()
extern void BinaryShiftToken_ToString_mDB69215F24E82204B99581AE5741D7B6B7B7EF6C ();
// 0x00000010 ZXing.Aztec.Internal.AztecCode ZXing.Aztec.Internal.Encoder::encode(System.Byte[],System.Int32,System.Int32)
extern void Encoder_encode_mBED400BE062A21825DA4B2D0E931BB0BF6D605CF ();
// 0x00000011 System.Void ZXing.Aztec.Internal.Encoder::drawBullsEye(ZXing.Common.BitMatrix,System.Int32,System.Int32)
extern void Encoder_drawBullsEye_m7DA324EE7286839BE83BEAA642109B7F6778310D ();
// 0x00000012 ZXing.Common.BitArray ZXing.Aztec.Internal.Encoder::generateModeMessage(System.Boolean,System.Int32,System.Int32)
extern void Encoder_generateModeMessage_m78514571851BCFCABEFD19A514426C229B32CFE0 ();
// 0x00000013 System.Void ZXing.Aztec.Internal.Encoder::drawModeMessage(ZXing.Common.BitMatrix,System.Boolean,System.Int32,ZXing.Common.BitArray)
extern void Encoder_drawModeMessage_mFAC54756787EA1133F6CA921B0D09EF765CFC3D1 ();
// 0x00000014 ZXing.Common.BitArray ZXing.Aztec.Internal.Encoder::generateCheckWords(ZXing.Common.BitArray,System.Int32,System.Int32)
extern void Encoder_generateCheckWords_mC78EF651D24DDFA74410A2A407AADB5A92F0B9BD ();
// 0x00000015 System.Int32[] ZXing.Aztec.Internal.Encoder::bitsToWords(ZXing.Common.BitArray,System.Int32,System.Int32)
extern void Encoder_bitsToWords_m1091CAAB9A0F56C51DE358DE5994A9E69A577879 ();
// 0x00000016 ZXing.Common.ReedSolomon.GenericGF ZXing.Aztec.Internal.Encoder::getGF(System.Int32)
extern void Encoder_getGF_m9525A32AD2D478549FF20197BA2854B337F41DEF ();
// 0x00000017 ZXing.Common.BitArray ZXing.Aztec.Internal.Encoder::stuffBits(ZXing.Common.BitArray,System.Int32)
extern void Encoder_stuffBits_mE99A0E5DE9CAAA1B4E01242DA0EB5A42FC03036F ();
// 0x00000018 System.Int32 ZXing.Aztec.Internal.Encoder::TotalBitsInLayer(System.Int32,System.Boolean)
extern void Encoder_TotalBitsInLayer_mCCDC72547500EE4DC42697165C9FD7A2B4612D8F ();
// 0x00000019 System.Void ZXing.Aztec.Internal.Encoder::.cctor()
extern void Encoder__cctor_mF57D6F26D1DFF5DB371886FD7494D955FBC235E0 ();
// 0x0000001A System.Void ZXing.Aztec.Internal.HighLevelEncoder::.cctor()
extern void HighLevelEncoder__cctor_mEDFEDE35542D90FC7D837A763B4269980C6EE141 ();
// 0x0000001B System.Void ZXing.Aztec.Internal.HighLevelEncoder::.ctor(System.Byte[])
extern void HighLevelEncoder__ctor_m5CEA27B8A8CA4B498D86E6BFC775E88667CECD7A ();
// 0x0000001C ZXing.Common.BitArray ZXing.Aztec.Internal.HighLevelEncoder::encode()
extern void HighLevelEncoder_encode_m0B68A1015975F3333A741ED0576FB71FD84A3DEE ();
// 0x0000001D System.Collections.Generic.ICollection`1<ZXing.Aztec.Internal.State> ZXing.Aztec.Internal.HighLevelEncoder::updateStateListForChar(System.Collections.Generic.IEnumerable`1<ZXing.Aztec.Internal.State>,System.Int32)
extern void HighLevelEncoder_updateStateListForChar_mA56C4AD15C601D108D3059B8843A0BBFE68BE6B5 ();
// 0x0000001E System.Void ZXing.Aztec.Internal.HighLevelEncoder::updateStateForChar(ZXing.Aztec.Internal.State,System.Int32,System.Collections.Generic.ICollection`1<ZXing.Aztec.Internal.State>)
extern void HighLevelEncoder_updateStateForChar_mC5BBCAC1712ABEE43F117718F764E2B15E872484 ();
// 0x0000001F System.Collections.Generic.ICollection`1<ZXing.Aztec.Internal.State> ZXing.Aztec.Internal.HighLevelEncoder::updateStateListForPair(System.Collections.Generic.IEnumerable`1<ZXing.Aztec.Internal.State>,System.Int32,System.Int32)
extern void HighLevelEncoder_updateStateListForPair_m8EA52D91E978058EA1959ED96A459CAEF413F16A ();
// 0x00000020 System.Void ZXing.Aztec.Internal.HighLevelEncoder::updateStateForPair(ZXing.Aztec.Internal.State,System.Int32,System.Int32,System.Collections.Generic.ICollection`1<ZXing.Aztec.Internal.State>)
extern void HighLevelEncoder_updateStateForPair_mC75922B79F4ABEC4486532CDC0495F93962EF067 ();
// 0x00000021 System.Collections.Generic.ICollection`1<ZXing.Aztec.Internal.State> ZXing.Aztec.Internal.HighLevelEncoder::simplifyStates(System.Collections.Generic.IEnumerable`1<ZXing.Aztec.Internal.State>)
extern void HighLevelEncoder_simplifyStates_mE6C18FAD7CF1057F46FC9E53042638B0B2B826CA ();
// 0x00000022 System.Void ZXing.Aztec.Internal.SimpleToken::.ctor(ZXing.Aztec.Internal.Token,System.Int32,System.Int32)
extern void SimpleToken__ctor_mD7D692213BF489D7D1FB5BD466E0605B7B559E20 ();
// 0x00000023 System.Void ZXing.Aztec.Internal.SimpleToken::appendTo(ZXing.Common.BitArray,System.Byte[])
extern void SimpleToken_appendTo_m3CFD844FA2574265BC74F8D1F817BC7FC0E27633 ();
// 0x00000024 System.String ZXing.Aztec.Internal.SimpleToken::ToString()
extern void SimpleToken_ToString_m5F57CCDB981468C35C539ACDB44B11709311D2C5 ();
// 0x00000025 System.Void ZXing.Aztec.Internal.State::.ctor(ZXing.Aztec.Internal.Token,System.Int32,System.Int32,System.Int32)
extern void State__ctor_mAC6145C205F642D82813613546183A59F17E4075 ();
// 0x00000026 System.Int32 ZXing.Aztec.Internal.State::get_Mode()
extern void State_get_Mode_m68510C11FC045C8E11D8AD457904F8F8AECEB586 ();
// 0x00000027 System.Int32 ZXing.Aztec.Internal.State::get_BinaryShiftByteCount()
extern void State_get_BinaryShiftByteCount_m7BA9567AA822E85D066B6A1A2CC6F7EC2EED204E ();
// 0x00000028 System.Int32 ZXing.Aztec.Internal.State::get_BitCount()
extern void State_get_BitCount_m96F2120C1F5543D25777F7D0F0074AA3B14F55A6 ();
// 0x00000029 ZXing.Aztec.Internal.State ZXing.Aztec.Internal.State::latchAndAppend(System.Int32,System.Int32)
extern void State_latchAndAppend_m5AFBF3B41913BCF41BBC300FF194E5D8CDC05D8E ();
// 0x0000002A ZXing.Aztec.Internal.State ZXing.Aztec.Internal.State::shiftAndAppend(System.Int32,System.Int32)
extern void State_shiftAndAppend_m5D5CE9892634A7506D99FF48F8861ABEF23ED83C ();
// 0x0000002B ZXing.Aztec.Internal.State ZXing.Aztec.Internal.State::addBinaryShiftChar(System.Int32)
extern void State_addBinaryShiftChar_m4333B7301F3C18481FFBBCB8772A745F9DDB4846 ();
// 0x0000002C ZXing.Aztec.Internal.State ZXing.Aztec.Internal.State::endBinaryShift(System.Int32)
extern void State_endBinaryShift_mA63F70B3F707589C73771B8F668CF4232548F3F1 ();
// 0x0000002D System.Boolean ZXing.Aztec.Internal.State::isBetterThanOrEqualTo(ZXing.Aztec.Internal.State)
extern void State_isBetterThanOrEqualTo_mBC51BEEE139936736F17317E4219BC72453F47CD ();
// 0x0000002E ZXing.Common.BitArray ZXing.Aztec.Internal.State::toBitArray(System.Byte[])
extern void State_toBitArray_mDD389077080992DEE4740D5D33A03F65211A3860 ();
// 0x0000002F System.String ZXing.Aztec.Internal.State::ToString()
extern void State_ToString_mD72A4DCB3D869F5FC417E545A586709981EDF964 ();
// 0x00000030 System.Void ZXing.Aztec.Internal.State::.cctor()
extern void State__cctor_m7393279D4D594C0BE15CA2078C601EE57831EC88 ();
// 0x00000031 System.Void ZXing.Aztec.Internal.Token::.ctor(ZXing.Aztec.Internal.Token)
extern void Token__ctor_mD23F2BE1F701847FF1B5F0BB08E460C006CFE4B8 ();
// 0x00000032 ZXing.Aztec.Internal.Token ZXing.Aztec.Internal.Token::get_Previous()
extern void Token_get_Previous_m28898878535A86A5B9C80E347EAC2AF7105025AD ();
// 0x00000033 ZXing.Aztec.Internal.Token ZXing.Aztec.Internal.Token::add(System.Int32,System.Int32)
extern void Token_add_mB53984B13180297251374AFA1153DD02B362EA3E ();
// 0x00000034 ZXing.Aztec.Internal.Token ZXing.Aztec.Internal.Token::addBinaryShift(System.Int32,System.Int32)
extern void Token_addBinaryShift_mA3A2E710E2E2E1E5D0D890A12722FFFE5D097B1E ();
// 0x00000035 System.Void ZXing.Aztec.Internal.Token::appendTo(ZXing.Common.BitArray,System.Byte[])
// 0x00000036 System.Void ZXing.Aztec.Internal.Token::.cctor()
extern void Token__cctor_m0170DC5D22E28131515B87426A018A41FE584839 ();
// 0x00000037 System.Void ZXing.BarcodeWriter::.ctor()
extern void BarcodeWriter__ctor_m35737422E781C6D87D44E816AF5C70CE62767BB3 ();
// 0x00000038 System.Void ZXing.BarcodeWriterGeneric`1::.ctor()
// 0x00000039 ZXing.BarcodeFormat ZXing.BarcodeWriterGeneric`1::get_Format()
// 0x0000003A System.Void ZXing.BarcodeWriterGeneric`1::set_Format(ZXing.BarcodeFormat)
// 0x0000003B ZXing.Common.EncodingOptions ZXing.BarcodeWriterGeneric`1::get_Options()
// 0x0000003C System.Void ZXing.BarcodeWriterGeneric`1::set_Options(ZXing.Common.EncodingOptions)
// 0x0000003D ZXing.Writer ZXing.BarcodeWriterGeneric`1::get_Encoder()
// 0x0000003E ZXing.Rendering.IBarcodeRenderer`1<TOutput> ZXing.BarcodeWriterGeneric`1::get_Renderer()
// 0x0000003F System.Void ZXing.BarcodeWriterGeneric`1::set_Renderer(ZXing.Rendering.IBarcodeRenderer`1<TOutput>)
// 0x00000040 ZXing.Common.BitMatrix ZXing.BarcodeWriterGeneric`1::Encode(System.String)
// 0x00000041 TOutput ZXing.BarcodeWriterGeneric`1::Write(System.String)
// 0x00000042 System.Void BigIntegerLibrary.Base10BigInteger::.ctor()
extern void Base10BigInteger__ctor_m6C860B744FE3F762B03FC88AB276C36E1EA6736E ();
// 0x00000043 System.Void BigIntegerLibrary.Base10BigInteger::.ctor(System.Int64)
extern void Base10BigInteger__ctor_m4842E436D2AEC111644D2F1F057AB6654C6139C2 ();
// 0x00000044 System.Void BigIntegerLibrary.Base10BigInteger::.ctor(BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger__ctor_m7B47E0A2AD545173E730E6ECA5B37BBA8F6CDE0F ();
// 0x00000045 System.Void BigIntegerLibrary.Base10BigInteger::set_NumberSign(BigIntegerLibrary.Sign)
extern void Base10BigInteger_set_NumberSign_mA34ABAF91342CEDF42666568BB5A85250D5042F2 ();
// 0x00000046 System.Boolean BigIntegerLibrary.Base10BigInteger::Equals(BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Equals_m037078BE651380C7D86FDB3ECB765BBA85696BA5 ();
// 0x00000047 System.Boolean BigIntegerLibrary.Base10BigInteger::Equals(System.Object)
extern void Base10BigInteger_Equals_mE62E2553E258FA4EEA7DC4731B48A2F2EC82CAA7 ();
// 0x00000048 System.Int32 BigIntegerLibrary.Base10BigInteger::GetHashCode()
extern void Base10BigInteger_GetHashCode_m9C6B858101EEE01E0E7A792EA3784BC41B4F0B47 ();
// 0x00000049 System.String BigIntegerLibrary.Base10BigInteger::ToString()
extern void Base10BigInteger_ToString_m67D58A86A3E9314F3903B6AA2E7CF350B38F75DF ();
// 0x0000004A BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Opposite(BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Opposite_m686350CA05971336799D2B94B2B8EEA6F1CD5A8A ();
// 0x0000004B System.Boolean BigIntegerLibrary.Base10BigInteger::Greater(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Greater_m60E7AE0022D7A9B167C9422BF725B5CE6B7F9EE8 ();
// 0x0000004C System.Boolean BigIntegerLibrary.Base10BigInteger::GreaterOrEqual(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_GreaterOrEqual_m6566D08D4E1FA7361E4201B6906F560A80A73FB6 ();
// 0x0000004D System.Boolean BigIntegerLibrary.Base10BigInteger::SmallerOrEqual(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_SmallerOrEqual_m638F37E3EC944921AF6AA6CF8F775445B281C43A ();
// 0x0000004E BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Abs(BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Abs_mFED8D9ACB754EEB150906C82B1C27771F582E2A8 ();
// 0x0000004F BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Addition(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Addition_mD60E8B54E68D7916A1B33EC18B72B06EBB661F5C ();
// 0x00000050 BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Multiplication(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Multiplication_m9A35FDBE28806F90BC857C76CD097FCF6DD3C69A ();
// 0x00000051 BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::op_Implicit(System.Int64)
extern void Base10BigInteger_op_Implicit_m2C6F5F174E9B8D973154E363A1289E4032D6D109 ();
// 0x00000052 System.Boolean BigIntegerLibrary.Base10BigInteger::op_Equality(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_Equality_m4EDBBF3C2AD667295ED6C26D04DE5E6C835FFEF0 ();
// 0x00000053 System.Boolean BigIntegerLibrary.Base10BigInteger::op_Inequality(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_Inequality_mD4FBCA391A1DB5949BC62068251CE8EC4C39CADD ();
// 0x00000054 System.Boolean BigIntegerLibrary.Base10BigInteger::op_GreaterThanOrEqual(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_GreaterThanOrEqual_m03B339289F68E6B8A7818A02E60282F54B4E884C ();
// 0x00000055 System.Boolean BigIntegerLibrary.Base10BigInteger::op_LessThanOrEqual(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_LessThanOrEqual_m96C5864BB2EA060D05E2BBCA088008870B9485E9 ();
// 0x00000056 BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::op_UnaryNegation(BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_UnaryNegation_mB45824A99755CBFAE60E57730040A80184D7C30C ();
// 0x00000057 BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::op_Addition(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_Addition_m450FCB40AE2F028AF793830E932D020BEB88EBAC ();
// 0x00000058 BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::op_Multiply(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_op_Multiply_m3AF47B9536AA8DA3796FB876A9DB09791EEE8437 ();
// 0x00000059 BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Add(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Add_mCC1E4047A312F40A78C6F7FD994CDB484D955925 ();
// 0x0000005A BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Subtract(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Subtract_m2D69C111AE6469DA2E99A3290F71FA8613395D58 ();
// 0x0000005B BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Multiply(BigIntegerLibrary.Base10BigInteger,BigIntegerLibrary.Base10BigInteger)
extern void Base10BigInteger_Multiply_m42B00519FD20568C488041861351AC4173CCF56C ();
// 0x0000005C System.Void BigIntegerLibrary.Base10BigInteger::.cctor()
extern void Base10BigInteger__cctor_mFEE513D02C85FFD1A76F202610C0E64532F2E9A1 ();
// 0x0000005D System.Void BigIntegerLibrary.Base10BigInteger_DigitContainer::.ctor()
extern void DigitContainer__ctor_m861AD7516AB9B3A51A73A1A9C40B2832E1534F5B ();
// 0x0000005E System.Int64 BigIntegerLibrary.Base10BigInteger_DigitContainer::get_Item(System.Int32)
extern void DigitContainer_get_Item_mC8EB6BCCCDC50C9B843DE7D704D9EF119F6DB232 ();
// 0x0000005F System.Void BigIntegerLibrary.Base10BigInteger_DigitContainer::set_Item(System.Int32,System.Int64)
extern void DigitContainer_set_Item_m499ED1EE74216FB85BC69823ABB42B84EE3104CA ();
// 0x00000060 System.Void BigIntegerLibrary.BigInteger::.ctor()
extern void BigInteger__ctor_mCF2C1A45FF522D92669D3CDE8249D49F95B424EA ();
// 0x00000061 System.Void BigIntegerLibrary.BigInteger::.ctor(System.Int64)
extern void BigInteger__ctor_m0E16B54C2243242463EF7C4D3A44272B50E88DB8 ();
// 0x00000062 System.Void BigIntegerLibrary.BigInteger::.ctor(BigIntegerLibrary.BigInteger)
extern void BigInteger__ctor_m9FA291997079054C20FC01E3C74BA51277DC8F71 ();
// 0x00000063 System.Void BigIntegerLibrary.BigInteger::.ctor(System.String)
extern void BigInteger__ctor_m3FF51CEDDE50EE0FE69B9371339569FB178F8CC9 ();
// 0x00000064 System.Void BigIntegerLibrary.BigInteger::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void BigInteger__ctor_m2B68A54A41E3D20DEF189E36B9CA8F0DED1CF269 ();
// 0x00000065 System.Void BigIntegerLibrary.BigInteger::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void BigInteger_GetObjectData_m0135E73F32A69E455AA85156734D13089968FC31 ();
// 0x00000066 System.Boolean BigIntegerLibrary.BigInteger::Equals(BigIntegerLibrary.BigInteger)
extern void BigInteger_Equals_m480221D5B04CCC0545E452D3C7FEBFF9E80D4E60 ();
// 0x00000067 System.Boolean BigIntegerLibrary.BigInteger::Equals(System.Object)
extern void BigInteger_Equals_m9D81B35ADFAFC95212D13260DEA165FEB345E20C ();
// 0x00000068 System.Int32 BigIntegerLibrary.BigInteger::GetHashCode()
extern void BigInteger_GetHashCode_m216F2C916483A4C15E6E63B9BBF82BD36376B726 ();
// 0x00000069 System.String BigIntegerLibrary.BigInteger::ToString()
extern void BigInteger_ToString_m6CD82EF9A23204683EC8D161D1950DC17BD44FA4 ();
// 0x0000006A BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Parse(System.String)
extern void BigInteger_Parse_m26369384240E51FAA3011A9383A2251A054D5B1B ();
// 0x0000006B System.Int32 BigIntegerLibrary.BigInteger::CompareTo(BigIntegerLibrary.BigInteger)
extern void BigInteger_CompareTo_m7C702C7EF4362E21CC1B48F0FA0081327908C089 ();
// 0x0000006C System.Int32 BigIntegerLibrary.BigInteger::CompareTo(System.Object)
extern void BigInteger_CompareTo_mD046F0342C90C14E9079B45756891F597F9EC950 ();
// 0x0000006D BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Opposite(BigIntegerLibrary.BigInteger)
extern void BigInteger_Opposite_m226222453D57320DF7EDA77BE54E26B51AB6EC99 ();
// 0x0000006E System.Boolean BigIntegerLibrary.BigInteger::Greater(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Greater_m173015695BA1E8F122C5BBCD18E29344A2A83EEA ();
// 0x0000006F System.Boolean BigIntegerLibrary.BigInteger::GreaterOrEqual(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_GreaterOrEqual_mEFFF6A3839421532475A7B39A3AEFD1EB71E7554 ();
// 0x00000070 System.Boolean BigIntegerLibrary.BigInteger::Smaller(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Smaller_m7016E935CA821FD7CBD511438E49F37B010D7DFA ();
// 0x00000071 System.Boolean BigIntegerLibrary.BigInteger::SmallerOrEqual(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_SmallerOrEqual_m0A7D0939BB9E54809981C96003177D1FFB33EC3C ();
// 0x00000072 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Abs(BigIntegerLibrary.BigInteger)
extern void BigInteger_Abs_m874A34DD42172540D0149FE8CD3182D7DBC4A61A ();
// 0x00000073 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Addition(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Addition_mA14D9813A5180E109F48363FBB62A8BD8E22A58B ();
// 0x00000074 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Subtraction(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Subtraction_m217518DB54A9BA3B00B0E9FB3BDED90D81968606 ();
// 0x00000075 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Multiplication(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Multiplication_m6B8A0B101868EF880962BB5DF53B36617F69D61C ();
// 0x00000076 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Division(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Division_m8F18818EA9A6536042E61C2E4E248E8906839553 ();
// 0x00000077 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Modulo(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Modulo_mAF8624E85A3EE794802EDA8FAA315B72EB1B1FCE ();
// 0x00000078 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Implicit(System.Int64)
extern void BigInteger_op_Implicit_mE6D986884BD80B1E696720104B8EBE3AEEA44980 ();
// 0x00000079 System.Boolean BigIntegerLibrary.BigInteger::op_Equality(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Equality_m9C45C1EF3BB845AAEFF4C98B9E4685FAED3A4991 ();
// 0x0000007A System.Boolean BigIntegerLibrary.BigInteger::op_Inequality(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Inequality_mC8CF7E512E1721ADFCF4FE0418E7AE230689B025 ();
// 0x0000007B System.Boolean BigIntegerLibrary.BigInteger::op_LessThan(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_LessThan_m7AD77825BC73C75A6CE950115874E0A5230F8358 ();
// 0x0000007C System.Boolean BigIntegerLibrary.BigInteger::op_GreaterThanOrEqual(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_GreaterThanOrEqual_m74E6AAE86671A31D0D4B025C62EC1076BE65DB4E ();
// 0x0000007D System.Boolean BigIntegerLibrary.BigInteger::op_LessThanOrEqual(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_LessThanOrEqual_m1EEBF7EF98EC693B6EE9CE84FA08FDD5D3C51CDA ();
// 0x0000007E BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_UnaryNegation(BigIntegerLibrary.BigInteger)
extern void BigInteger_op_UnaryNegation_m0A665EB313980CD31BD6F6E465CA1C34B7023C1C ();
// 0x0000007F BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Addition(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Addition_mB9879BDDA43B2F540E3C96A8AD2FA673F24702C7 ();
// 0x00000080 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Subtraction(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Subtraction_m7ACC641C06DDE0AFC0187ADDBE05CE08D99D343B ();
// 0x00000081 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Multiply(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Multiply_m3D39D9B8CEC35FA413984E1EA7763DB7CEE39910 ();
// 0x00000082 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::op_Division(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_op_Division_m1FBB22ABB15D0E4289F7D0F32F4C79D5758EE365 ();
// 0x00000083 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Add(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Add_mB0FB4E38EA21E16FA8059858D913F7575086389A ();
// 0x00000084 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Subtract(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Subtract_m873FB3C134FFB768959FA1BF4255397D8E65DAE6 ();
// 0x00000085 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Multiply(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_Multiply_m61FE301F61485FF64FE2F5120A4A608ECE5B4714 ();
// 0x00000086 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::DivideByOneDigitNumber(BigIntegerLibrary.BigInteger,System.Int64)
extern void BigInteger_DivideByOneDigitNumber_mE713956C0268D8814097FC4117D78D814BFE8344 ();
// 0x00000087 BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::DivideByBigNumber(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger)
extern void BigInteger_DivideByBigNumber_m22226F7D49644446C885E1443121EDD40CBFCC11 ();
// 0x00000088 System.Boolean BigIntegerLibrary.BigInteger::DivideByBigNumberSmaller(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger,System.Int32,System.Int32)
extern void BigInteger_DivideByBigNumberSmaller_m69B726AB842BDF3875AB880B8051A18E9EC4684A ();
// 0x00000089 System.Void BigIntegerLibrary.BigInteger::Difference(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger,System.Int32,System.Int32)
extern void BigInteger_Difference_m86F180B783B01353273EF5B756CA6D344308781F ();
// 0x0000008A System.Int64 BigIntegerLibrary.BigInteger::Trial(BigIntegerLibrary.BigInteger,BigIntegerLibrary.BigInteger,System.Int32,System.Int32)
extern void BigInteger_Trial_m65E96A00D96DE298D81BAA826E529DFB94CEB8A4 ();
// 0x0000008B System.Void BigIntegerLibrary.BigInteger::.cctor()
extern void BigInteger__cctor_m2B0CBD84D881AD79047FAE6684D025263A4F41D6 ();
// 0x0000008C System.Void BigIntegerLibrary.BigInteger_DigitContainer::.ctor()
extern void DigitContainer__ctor_m5F64CF9D6BD73E2720A85D10CE575E14F8D83D21 ();
// 0x0000008D System.Int64 BigIntegerLibrary.BigInteger_DigitContainer::get_Item(System.Int32)
extern void DigitContainer_get_Item_m1E4626878E0A8C44D7E9F68CFC495E2923A3911B ();
// 0x0000008E System.Void BigIntegerLibrary.BigInteger_DigitContainer::set_Item(System.Int32,System.Int64)
extern void DigitContainer_set_Item_mB359EEEF796A35EC9BC1CA363BB0A4EBDA2DDA59 ();
// 0x0000008F System.Void BigIntegerLibrary.BigIntegerException::.ctor(System.String,System.Exception)
extern void BigIntegerException__ctor_m7B4F155DEF04583BD7F927D4260CF3EA10697DCA ();
// 0x00000090 System.Void ZXing.Common.BitArray::.ctor()
extern void BitArray__ctor_m447EC39754D52295A74117EA49A803B99649EFE9 ();
// 0x00000091 System.Void ZXing.Common.BitArray::.ctor(System.Int32)
extern void BitArray__ctor_m62A2D29F7D3417E86A4B9B3CBFE38AB2FA1081E5 ();
// 0x00000092 System.Int32 ZXing.Common.BitArray::get_Size()
extern void BitArray_get_Size_m734BC4F5E57FD38D95E483C6E9427595CADFEFA0 ();
// 0x00000093 System.Int32 ZXing.Common.BitArray::get_SizeInBytes()
extern void BitArray_get_SizeInBytes_m228A29F53BBFDE448F0AA39F1C5A367135B0710C ();
// 0x00000094 System.Boolean ZXing.Common.BitArray::get_Item(System.Int32)
extern void BitArray_get_Item_m1E0112C7EB188828A246839BEFE04D04D0B3DD6F ();
// 0x00000095 System.Void ZXing.Common.BitArray::ensureCapacity(System.Int32)
extern void BitArray_ensureCapacity_mF36226C608FE756765C67B0E003A6FD6D0407B9D ();
// 0x00000096 System.Void ZXing.Common.BitArray::setBulk(System.Int32,System.Int32)
extern void BitArray_setBulk_m2E500414B7579D0AB91B86D06F06B43B0DC6D2FA ();
// 0x00000097 System.Void ZXing.Common.BitArray::clear()
extern void BitArray_clear_m99902C6E7AEF3CAB31A3BBDA119797E78AA49686 ();
// 0x00000098 System.Void ZXing.Common.BitArray::appendBit(System.Boolean)
extern void BitArray_appendBit_m51BA3537DBECE11102AC993DF7AB411600CE95AF ();
// 0x00000099 System.Int32[] ZXing.Common.BitArray::get_Array()
extern void BitArray_get_Array_m43621FAAC485557AF6FBA8C38C3C83798841F944 ();
// 0x0000009A System.Void ZXing.Common.BitArray::appendBits(System.Int32,System.Int32)
extern void BitArray_appendBits_m3FF17F258145BF037BE614A5AD5AC17EE2BE496C ();
// 0x0000009B System.Void ZXing.Common.BitArray::appendBitArray(ZXing.Common.BitArray)
extern void BitArray_appendBitArray_m38E9DF9AAB925DFF0987A05A6B772832E790DA81 ();
// 0x0000009C System.Void ZXing.Common.BitArray::xor(ZXing.Common.BitArray)
extern void BitArray_xor_mC318BD1A29B7B2E6EAACD4A8085B51B431EED6E8 ();
// 0x0000009D System.Void ZXing.Common.BitArray::toBytes(System.Int32,System.Byte[],System.Int32,System.Int32)
extern void BitArray_toBytes_m8BB01B224FAC67447D67C3C8DFCA7F436522FF48 ();
// 0x0000009E System.Int32[] ZXing.Common.BitArray::makeArray(System.Int32)
extern void BitArray_makeArray_m010C5493596C42A13D5EFB1A97D07C76EB9005D9 ();
// 0x0000009F System.Boolean ZXing.Common.BitArray::Equals(System.Object)
extern void BitArray_Equals_mF0F0307A759610B642EC29C5D819C5B4DF2CD50F ();
// 0x000000A0 System.Int32 ZXing.Common.BitArray::GetHashCode()
extern void BitArray_GetHashCode_m3A36F70A9D5FD427FF575F75A4AE717632F26257 ();
// 0x000000A1 System.String ZXing.Common.BitArray::ToString()
extern void BitArray_ToString_mECA006F58F4F1FF42B845F6DFCC5743B7FAE9A4C ();
// 0x000000A2 System.Void ZXing.Common.BitArray::.cctor()
extern void BitArray__cctor_m3F78027BD2DC8DE75D5F708781403FC3F695534C ();
// 0x000000A3 System.Void ZXing.Common.BitMatrix::.ctor(System.Int32)
extern void BitMatrix__ctor_m59A0D6C0309D72242D814F872A7081A207F6564C ();
// 0x000000A4 System.Void ZXing.Common.BitMatrix::.ctor(System.Int32,System.Int32)
extern void BitMatrix__ctor_m37A95608226171AF5ABB00C563ACB5C163680D7C ();
// 0x000000A5 System.Int32 ZXing.Common.BitMatrix::get_Width()
extern void BitMatrix_get_Width_m401A654CF5F80320401B04182BCDB5B51DACB03F ();
// 0x000000A6 System.Int32 ZXing.Common.BitMatrix::get_Height()
extern void BitMatrix_get_Height_mB09884394EDDD3ACEE910846E7A7433CAFA0569A ();
// 0x000000A7 System.Boolean ZXing.Common.BitMatrix::get_Item(System.Int32,System.Int32)
extern void BitMatrix_get_Item_m7C16B005C94F9DBB687F2BC3C8E4DB996D4C0E87 ();
// 0x000000A8 System.Void ZXing.Common.BitMatrix::set_Item(System.Int32,System.Int32,System.Boolean)
extern void BitMatrix_set_Item_mE71340CDB04E7EF55E66BED1AD4D7D540614FB94 ();
// 0x000000A9 System.Void ZXing.Common.BitMatrix::clear()
extern void BitMatrix_clear_m27DD9424D87D42360059B7C261D1765A79D56323 ();
// 0x000000AA System.Void ZXing.Common.BitMatrix::setRegion(System.Int32,System.Int32,System.Int32,System.Int32)
extern void BitMatrix_setRegion_m81FFADFC79822D450F24FAF1583E89FA90C6A162 ();
// 0x000000AB ZXing.Common.BitArray ZXing.Common.BitMatrix::getRow(System.Int32,ZXing.Common.BitArray)
extern void BitMatrix_getRow_m0FD1D8F749065384E3E55C83EE7D08C0DEE2BFB3 ();
// 0x000000AC System.Boolean ZXing.Common.BitMatrix::Equals(System.Object)
extern void BitMatrix_Equals_mF239925437985AE58560701D61454A5DB01AC9F5 ();
// 0x000000AD System.Int32 ZXing.Common.BitMatrix::GetHashCode()
extern void BitMatrix_GetHashCode_m23A7B81AC393789DAE6799C89B540D6273CF0D83 ();
// 0x000000AE System.String ZXing.Common.BitMatrix::ToString()
extern void BitMatrix_ToString_mD4CD1278121673E02D928C1282C284564184F84D ();
// 0x000000AF System.Void ZXing.Common.CharacterSetECI::.cctor()
extern void CharacterSetECI__cctor_mF120D5E60A5EAF5ED7352C794FCCB4197244EE9E ();
// 0x000000B0 System.Void ZXing.Common.CharacterSetECI::.ctor(System.Int32,System.String)
extern void CharacterSetECI__ctor_m288E0A497930AB078EC653AEF53C41B3B71CA745 ();
// 0x000000B1 System.Void ZXing.Common.CharacterSetECI::addCharacterSet(System.Int32,System.String)
extern void CharacterSetECI_addCharacterSet_m1ADBFE6459CA859B2DAC4FFE5A8D8F9B874F359E ();
// 0x000000B2 System.Void ZXing.Common.CharacterSetECI::addCharacterSet(System.Int32,System.String[])
extern void CharacterSetECI_addCharacterSet_m39E93174C44C27BABB4766EC95CB89E17435F12D ();
// 0x000000B3 ZXing.Common.CharacterSetECI ZXing.Common.CharacterSetECI::getCharacterSetECIByName(System.String)
extern void CharacterSetECI_getCharacterSetECIByName_m8A5BEF5ACA7B8DCA4E39080E76082774BBC9CA44 ();
// 0x000000B4 System.Void ZXing.Common.ECI::.ctor(System.Int32)
extern void ECI__ctor_mFA5C0E33E28DD95EBF553E413CF872D847D3A494 ();
// 0x000000B5 System.Int32 ZXing.Common.ECI::get_Value()
extern void ECI_get_Value_m36D1C634BFE6B2C1B320CB4C7FD60BC9B7BFFBA9 ();
// 0x000000B6 System.Void ZXing.Common.EncodingOptions::.ctor()
extern void EncodingOptions__ctor_m363D9FD3CD59DC41EE7C0C38D1B8012041DCC3F1 ();
// 0x000000B7 System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object> ZXing.Common.EncodingOptions::get_Hints()
extern void EncodingOptions_get_Hints_m8164F3129D6105DFFBA7CFC68BD5F318ACDBFF8B ();
// 0x000000B8 System.Void ZXing.Common.EncodingOptions::set_Hints(System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void EncodingOptions_set_Hints_mAD6CEED9E70DACD9F07023B7578BEE5B8A9F4B32 ();
// 0x000000B9 System.Int32 ZXing.Common.EncodingOptions::get_Height()
extern void EncodingOptions_get_Height_m420663F469C0105FC54F2FA4A294A92A700A5E39 ();
// 0x000000BA System.Void ZXing.Common.EncodingOptions::set_Height(System.Int32)
extern void EncodingOptions_set_Height_m38ECF044CE137957E38DE5A8D06282554F5DC5A3 ();
// 0x000000BB System.Int32 ZXing.Common.EncodingOptions::get_Width()
extern void EncodingOptions_get_Width_m61EDEF62CD743BBBA5164C5DBA7529916534784B ();
// 0x000000BC System.Void ZXing.Common.EncodingOptions::set_Width(System.Int32)
extern void EncodingOptions_set_Width_m9B3048830472BDCE08093649559AA695161C5793 ();
// 0x000000BD System.Void ZXing.Common.ReedSolomon.GenericGF::.ctor(System.Int32,System.Int32,System.Int32)
extern void GenericGF__ctor_mFAA9366A047AA42BEE8DD72D71C9EDB52433CD52 ();
// 0x000000BE System.Void ZXing.Common.ReedSolomon.GenericGF::initialize()
extern void GenericGF_initialize_m0465BF1D33A82B72D5FB62FD6AD9EA61A625B9DA ();
// 0x000000BF System.Void ZXing.Common.ReedSolomon.GenericGF::checkInit()
extern void GenericGF_checkInit_mBD21E6748ACB5F0D6B2318B0091EFDD161A548CB ();
// 0x000000C0 ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGF::get_Zero()
extern void GenericGF_get_Zero_m62CE6C876D9E420E9BBB2D219910032448C75ADC ();
// 0x000000C1 ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGF::buildMonomial(System.Int32,System.Int32)
extern void GenericGF_buildMonomial_mF224AD012599E2A041261483616AD73C592AAE95 ();
// 0x000000C2 System.Int32 ZXing.Common.ReedSolomon.GenericGF::addOrSubtract(System.Int32,System.Int32)
extern void GenericGF_addOrSubtract_mB78A2634FB827539489FAB0C8E2FAC352BD7EEF0 ();
// 0x000000C3 System.Int32 ZXing.Common.ReedSolomon.GenericGF::exp(System.Int32)
extern void GenericGF_exp_m9210FD9893001B36082411E95E612A6F9A6BC25B ();
// 0x000000C4 System.Int32 ZXing.Common.ReedSolomon.GenericGF::log(System.Int32)
extern void GenericGF_log_mD96C86BA141FC104A891DEBCC72FC96BC5C02186 ();
// 0x000000C5 System.Int32 ZXing.Common.ReedSolomon.GenericGF::inverse(System.Int32)
extern void GenericGF_inverse_m4AB1A6A0A8E10D684FFDBCA5EF0A77831AE88B2A ();
// 0x000000C6 System.Int32 ZXing.Common.ReedSolomon.GenericGF::multiply(System.Int32,System.Int32)
extern void GenericGF_multiply_mE1F0248324228772DE4475392CC446B6970FE413 ();
// 0x000000C7 System.Int32 ZXing.Common.ReedSolomon.GenericGF::get_GeneratorBase()
extern void GenericGF_get_GeneratorBase_m2750B801EE7BAE7C59A1BC0FBBC5F2BC4A0E3D6C ();
// 0x000000C8 System.String ZXing.Common.ReedSolomon.GenericGF::ToString()
extern void GenericGF_ToString_m6511ADC6FE9842F29B0358A834E177F92DF90040 ();
// 0x000000C9 System.Void ZXing.Common.ReedSolomon.GenericGF::.cctor()
extern void GenericGF__cctor_mEB9818FE714E5C4E10EA921B11FE0D900976415E ();
// 0x000000CA System.Void ZXing.Common.ReedSolomon.GenericGFPoly::.ctor(ZXing.Common.ReedSolomon.GenericGF,System.Int32[])
extern void GenericGFPoly__ctor_m960314B3510BF3F12143B6A38BB1790E77F24B5F ();
// 0x000000CB System.Int32[] ZXing.Common.ReedSolomon.GenericGFPoly::get_Coefficients()
extern void GenericGFPoly_get_Coefficients_m296CD9F7B5ACC16F448A338DA3977E4AED5695D1 ();
// 0x000000CC System.Int32 ZXing.Common.ReedSolomon.GenericGFPoly::get_Degree()
extern void GenericGFPoly_get_Degree_m1A1015BCA165E2BCE99E6B6B84575D8B8A922064 ();
// 0x000000CD System.Boolean ZXing.Common.ReedSolomon.GenericGFPoly::get_isZero()
extern void GenericGFPoly_get_isZero_m1E63B6A391165A4238A0C98CBD006AE281E2EAAE ();
// 0x000000CE System.Int32 ZXing.Common.ReedSolomon.GenericGFPoly::getCoefficient(System.Int32)
extern void GenericGFPoly_getCoefficient_m6C9CE85BF893795DB3A14E70EBC3DDB5B3A88FD5 ();
// 0x000000CF ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGFPoly::addOrSubtract(ZXing.Common.ReedSolomon.GenericGFPoly)
extern void GenericGFPoly_addOrSubtract_mE785B913612740A94C32AB7B268A6C8B8F60FD53 ();
// 0x000000D0 ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGFPoly::multiply(ZXing.Common.ReedSolomon.GenericGFPoly)
extern void GenericGFPoly_multiply_mEB3732155390CC69C01EACA6459315B9D5E7F381 ();
// 0x000000D1 ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.GenericGFPoly::multiplyByMonomial(System.Int32,System.Int32)
extern void GenericGFPoly_multiplyByMonomial_m161CF186334E6F4B74216B82DE6C85E0746A4AAA ();
// 0x000000D2 ZXing.Common.ReedSolomon.GenericGFPoly[] ZXing.Common.ReedSolomon.GenericGFPoly::divide(ZXing.Common.ReedSolomon.GenericGFPoly)
extern void GenericGFPoly_divide_mA193CA32955473D81048378E38BB44AE0CF3C6E6 ();
// 0x000000D3 System.String ZXing.Common.ReedSolomon.GenericGFPoly::ToString()
extern void GenericGFPoly_ToString_mA14C1515AE0DBC32396555AE6A4147E64A2C6775 ();
// 0x000000D4 System.Void ZXing.Common.ReedSolomon.ReedSolomonEncoder::.ctor(ZXing.Common.ReedSolomon.GenericGF)
extern void ReedSolomonEncoder__ctor_mE1DF0D1C4F4C719F9A02D12750F94AD1E036BB11 ();
// 0x000000D5 ZXing.Common.ReedSolomon.GenericGFPoly ZXing.Common.ReedSolomon.ReedSolomonEncoder::buildGenerator(System.Int32)
extern void ReedSolomonEncoder_buildGenerator_mA71A2FAC804D37DF7A04D42525767C12AC320F4E ();
// 0x000000D6 System.Void ZXing.Common.ReedSolomon.ReedSolomonEncoder::encode(System.Int32[],System.Int32)
extern void ReedSolomonEncoder_encode_mDBEA6B965F313DC0A95F8F7954D7660D8B873BBA ();
// 0x000000D7 System.Void ZXing.Datamatrix.DataMatrixWriter::.ctor()
extern void DataMatrixWriter__ctor_m258A636534E206DEE397EBEF90D0A2F593337E15 ();
// 0x000000D8 ZXing.Common.BitMatrix ZXing.Datamatrix.DataMatrixWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void DataMatrixWriter_encode_mB79F935E06D6DEC373237DD78E3A0401C94A2B3F ();
// 0x000000D9 ZXing.Common.BitMatrix ZXing.Datamatrix.DataMatrixWriter::encodeLowLevel(ZXing.Datamatrix.Encoder.DefaultPlacement,ZXing.Datamatrix.Encoder.SymbolInfo)
extern void DataMatrixWriter_encodeLowLevel_m8D4033B0F7C3C042F82E4A23130EA17F305E1AA8 ();
// 0x000000DA ZXing.Common.BitMatrix ZXing.Datamatrix.DataMatrixWriter::convertByteMatrixToBitMatrix(ZXing.QrCode.Internal.ByteMatrix)
extern void DataMatrixWriter_convertByteMatrixToBitMatrix_mE24CF10ECECD14F181BC48B9BA87D718D9573916 ();
// 0x000000DB System.Void ZXing.Datamatrix.Encoder.ASCIIEncoder::.ctor()
extern void ASCIIEncoder__ctor_m626A86365C898B3FFD9E120CAC7B241067FB36BA ();
// 0x000000DC System.Int32 ZXing.Datamatrix.Encoder.ASCIIEncoder::get_EncodingMode()
extern void ASCIIEncoder_get_EncodingMode_m15E395FDDDE8FD41419AAB2F00EF678C4DCA9D6B ();
// 0x000000DD System.Void ZXing.Datamatrix.Encoder.ASCIIEncoder::encode(ZXing.Datamatrix.Encoder.EncoderContext)
extern void ASCIIEncoder_encode_m4180056CBEBD87F65AF3D7215D3AF7DA34647DEC ();
// 0x000000DE System.Char ZXing.Datamatrix.Encoder.ASCIIEncoder::encodeASCIIDigits(System.Char,System.Char)
extern void ASCIIEncoder_encodeASCIIDigits_m1E3A40D88D173A3F38D48B60B08ED71295532F38 ();
// 0x000000DF System.Void ZXing.Datamatrix.Encoder.Base256Encoder::.ctor()
extern void Base256Encoder__ctor_m8CBED0DC1545803033EA31888C5A6E5FFEA85E4C ();
// 0x000000E0 System.Int32 ZXing.Datamatrix.Encoder.Base256Encoder::get_EncodingMode()
extern void Base256Encoder_get_EncodingMode_m801B0F1A69EDBE04D59698E6768F24F59EE95988 ();
// 0x000000E1 System.Void ZXing.Datamatrix.Encoder.Base256Encoder::encode(ZXing.Datamatrix.Encoder.EncoderContext)
extern void Base256Encoder_encode_m7BAC58C34BE993CE0EA5A8A3866343243E77944D ();
// 0x000000E2 System.Char ZXing.Datamatrix.Encoder.Base256Encoder::randomize255State(System.Char,System.Int32)
extern void Base256Encoder_randomize255State_m308254D546E206CB40DC7367D8C5D9A5291FC5E1 ();
// 0x000000E3 System.Void ZXing.Datamatrix.Encoder.C40Encoder::.ctor()
extern void C40Encoder__ctor_m1217F5ACE08F77B1FC446BE210ADF74A7B83E58F ();
// 0x000000E4 System.Int32 ZXing.Datamatrix.Encoder.C40Encoder::get_EncodingMode()
extern void C40Encoder_get_EncodingMode_mD0BF0DFCCBC1317A91B9E001585F3532E1A7B96C ();
// 0x000000E5 System.Void ZXing.Datamatrix.Encoder.C40Encoder::encode(ZXing.Datamatrix.Encoder.EncoderContext)
extern void C40Encoder_encode_m1B86D71DC289705D3DC662C4542A786615A8EFA2 ();
// 0x000000E6 System.Int32 ZXing.Datamatrix.Encoder.C40Encoder::backtrackOneCharacter(ZXing.Datamatrix.Encoder.EncoderContext,System.Text.StringBuilder,System.Text.StringBuilder,System.Int32)
extern void C40Encoder_backtrackOneCharacter_m4E81CE2A0772C5E8E01845F305E668095D773179 ();
// 0x000000E7 System.Void ZXing.Datamatrix.Encoder.C40Encoder::writeNextTriplet(ZXing.Datamatrix.Encoder.EncoderContext,System.Text.StringBuilder)
extern void C40Encoder_writeNextTriplet_m07F556069342607E4BA7B88E139D24DA075DF497 ();
// 0x000000E8 System.Void ZXing.Datamatrix.Encoder.C40Encoder::handleEOD(ZXing.Datamatrix.Encoder.EncoderContext,System.Text.StringBuilder)
extern void C40Encoder_handleEOD_mF417694C8A1C5812B324BEBD9760D78A977E3859 ();
// 0x000000E9 System.Int32 ZXing.Datamatrix.Encoder.C40Encoder::encodeChar(System.Char,System.Text.StringBuilder)
extern void C40Encoder_encodeChar_m457E009DEC8A2E0110264154D4A13493FF2CB84B ();
// 0x000000EA System.String ZXing.Datamatrix.Encoder.C40Encoder::encodeToCodewords(System.Text.StringBuilder,System.Int32)
extern void C40Encoder_encodeToCodewords_mB7689ACD903AB5EB0999DB736CB42C6C7C0CC4DC ();
// 0x000000EB System.Void ZXing.Datamatrix.Encoder.DataMatrixSymbolInfo144::.ctor()
extern void DataMatrixSymbolInfo144__ctor_m39A6680AAB68F9AA14AF502C3550BE87123AC42C ();
// 0x000000EC System.Int32 ZXing.Datamatrix.Encoder.DataMatrixSymbolInfo144::getInterleavedBlockCount()
extern void DataMatrixSymbolInfo144_getInterleavedBlockCount_m8E397842ED17EE2D12176EBC52FE361CF8266696 ();
// 0x000000ED System.Int32 ZXing.Datamatrix.Encoder.DataMatrixSymbolInfo144::getDataLengthForInterleavedBlock(System.Int32)
extern void DataMatrixSymbolInfo144_getDataLengthForInterleavedBlock_mA9BD281C782CE97EBC190F5EA6EE81980B68F2CB ();
// 0x000000EE System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::.ctor(System.String,System.Int32,System.Int32)
extern void DefaultPlacement__ctor_m67215D65D8625FB03157A77C2D8AE7F7DCF3DE48 ();
// 0x000000EF System.Boolean ZXing.Datamatrix.Encoder.DefaultPlacement::getBit(System.Int32,System.Int32)
extern void DefaultPlacement_getBit_m6C62D33EEA6AF14F9166AE2E5B67C6E3625B0661 ();
// 0x000000F0 System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::setBit(System.Int32,System.Int32,System.Boolean)
extern void DefaultPlacement_setBit_mDB203166E967E002D73BEEA19EC4430AD9D6510C ();
// 0x000000F1 System.Boolean ZXing.Datamatrix.Encoder.DefaultPlacement::hasBit(System.Int32,System.Int32)
extern void DefaultPlacement_hasBit_mAD7BA75EFDF34D3394ADC09C015897C6D1FE0F4F ();
// 0x000000F2 System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::place()
extern void DefaultPlacement_place_m673E91B4358421000993D4D8317B3E047F82E38D ();
// 0x000000F3 System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::module(System.Int32,System.Int32,System.Int32,System.Int32)
extern void DefaultPlacement_module_m081F643C371E107396283C051D83FB8F37F26D31 ();
// 0x000000F4 System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::utah(System.Int32,System.Int32,System.Int32)
extern void DefaultPlacement_utah_m0DF78D30D81B6EDCD6058AC00D3C30DA90135D27 ();
// 0x000000F5 System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::corner1(System.Int32)
extern void DefaultPlacement_corner1_mFE5FB486BF96FD7345BF5026D827129ECCCF5AE7 ();
// 0x000000F6 System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::corner2(System.Int32)
extern void DefaultPlacement_corner2_mC20D2B353AB2C45027F5298E1439A7DD59DEBC0E ();
// 0x000000F7 System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::corner3(System.Int32)
extern void DefaultPlacement_corner3_m7532D2BD89164EEEFA465C07808FC316568E1239 ();
// 0x000000F8 System.Void ZXing.Datamatrix.Encoder.DefaultPlacement::corner4(System.Int32)
extern void DefaultPlacement_corner4_m689E1FC7D2E8BD666E43FF2A12EE4E54BEF2B984 ();
// 0x000000F9 System.Void ZXing.Datamatrix.Encoder.EdifactEncoder::.ctor()
extern void EdifactEncoder__ctor_mE7EDDB5212526DC17BDD61AF1DE3776157B6110B ();
// 0x000000FA System.Int32 ZXing.Datamatrix.Encoder.EdifactEncoder::get_EncodingMode()
extern void EdifactEncoder_get_EncodingMode_m0B7B5FECA9991B1FA2E48989C7416335C3802EFD ();
// 0x000000FB System.Void ZXing.Datamatrix.Encoder.EdifactEncoder::encode(ZXing.Datamatrix.Encoder.EncoderContext)
extern void EdifactEncoder_encode_mC332FD4608158BD6EB2785D0626A9C3E0B6F979A ();
// 0x000000FC System.Void ZXing.Datamatrix.Encoder.EdifactEncoder::handleEOD(ZXing.Datamatrix.Encoder.EncoderContext,System.Text.StringBuilder)
extern void EdifactEncoder_handleEOD_m5F93B61D0EE7D38F03FDD0EBC209145F08A4B752 ();
// 0x000000FD System.Void ZXing.Datamatrix.Encoder.EdifactEncoder::encodeChar(System.Char,System.Text.StringBuilder)
extern void EdifactEncoder_encodeChar_mA01BEC4D03C59FC0A2344BCC9FB76CC2540ECAF6 ();
// 0x000000FE System.String ZXing.Datamatrix.Encoder.EdifactEncoder::encodeToCodewords(System.Text.StringBuilder,System.Int32)
extern void EdifactEncoder_encodeToCodewords_mC27058C11DDC5C08C6AF190C4F502414D3106E53 ();
// 0x000000FF System.Void ZXing.Datamatrix.Encoder.Encoder::encode(ZXing.Datamatrix.Encoder.EncoderContext)
// 0x00000100 System.Void ZXing.Datamatrix.Encoder.EncoderContext::.cctor()
extern void EncoderContext__cctor_mC0AB1D6282AFAB68FAE6951861718F8B03F4F283 ();
// 0x00000101 System.Void ZXing.Datamatrix.Encoder.EncoderContext::.ctor(System.String)
extern void EncoderContext__ctor_m75E1A4D50C7D5F3C966FA1C38BE421F6E4F0AD26 ();
// 0x00000102 System.Void ZXing.Datamatrix.Encoder.EncoderContext::setSymbolShape(ZXing.Datamatrix.Encoder.SymbolShapeHint)
extern void EncoderContext_setSymbolShape_m461AFA931620A91FEA8DD126087D47759391E096 ();
// 0x00000103 System.Void ZXing.Datamatrix.Encoder.EncoderContext::setSizeConstraints(ZXing.Dimension,ZXing.Dimension)
extern void EncoderContext_setSizeConstraints_m6871B5655B16118FD8D38FC888CFFA42793C4779 ();
// 0x00000104 System.Void ZXing.Datamatrix.Encoder.EncoderContext::setSkipAtEnd(System.Int32)
extern void EncoderContext_setSkipAtEnd_m70958BC35C88E935FA0F5BED982663C8EA1F3F78 ();
// 0x00000105 System.Char ZXing.Datamatrix.Encoder.EncoderContext::get_CurrentChar()
extern void EncoderContext_get_CurrentChar_m05D8526B6D803EFF8F78859E387BE3139FBC80A9 ();
// 0x00000106 System.Void ZXing.Datamatrix.Encoder.EncoderContext::writeCodewords(System.String)
extern void EncoderContext_writeCodewords_m1B943B100E23DCED73CC2D41FBD4DA5C8019EC37 ();
// 0x00000107 System.Void ZXing.Datamatrix.Encoder.EncoderContext::writeCodeword(System.Char)
extern void EncoderContext_writeCodeword_m9BA8A0B8B9CED2E0AA1112A1952F4CADA2B71B34 ();
// 0x00000108 System.Int32 ZXing.Datamatrix.Encoder.EncoderContext::get_CodewordCount()
extern void EncoderContext_get_CodewordCount_mF2C51F5BF3A3B6B7113FE9710DB2A76805F22604 ();
// 0x00000109 System.Void ZXing.Datamatrix.Encoder.EncoderContext::signalEncoderChange(System.Int32)
extern void EncoderContext_signalEncoderChange_m22CC9162BE624D8787A199DA9381B57527DC2784 ();
// 0x0000010A System.Void ZXing.Datamatrix.Encoder.EncoderContext::resetEncoderSignal()
extern void EncoderContext_resetEncoderSignal_m4EB0A4F1173F3254C73FC7112727C69EA34A501F ();
// 0x0000010B System.Boolean ZXing.Datamatrix.Encoder.EncoderContext::get_HasMoreCharacters()
extern void EncoderContext_get_HasMoreCharacters_m165BA5507CEE8CBB020E6DEE4E1B53F4F00D4C55 ();
// 0x0000010C System.Int32 ZXing.Datamatrix.Encoder.EncoderContext::get_TotalMessageCharCount()
extern void EncoderContext_get_TotalMessageCharCount_m8DFC582940DB2F8FF553E31A1B4A705EF3E0C6C1 ();
// 0x0000010D System.Int32 ZXing.Datamatrix.Encoder.EncoderContext::get_RemainingCharacters()
extern void EncoderContext_get_RemainingCharacters_mCEA489131F8AC9AE25BAB3C8A754ADC610BF7E7C ();
// 0x0000010E System.Void ZXing.Datamatrix.Encoder.EncoderContext::updateSymbolInfo()
extern void EncoderContext_updateSymbolInfo_mB181CA2192B9C1E50C82133999A1A6EDF0926E97 ();
// 0x0000010F System.Void ZXing.Datamatrix.Encoder.EncoderContext::updateSymbolInfo(System.Int32)
extern void EncoderContext_updateSymbolInfo_m2EAE6A1CFD629EB2E5B95457BA0C06B76E2A83FF ();
// 0x00000110 System.Void ZXing.Datamatrix.Encoder.EncoderContext::resetSymbolInfo()
extern void EncoderContext_resetSymbolInfo_m7EC92BFAF264D33E1BEA6CB9E593F14CF7CFA491 ();
// 0x00000111 System.Int32 ZXing.Datamatrix.Encoder.EncoderContext::get_Pos()
extern void EncoderContext_get_Pos_m913E7BCAEF09FB214D1D0865142F988FCF91E5B6 ();
// 0x00000112 System.Void ZXing.Datamatrix.Encoder.EncoderContext::set_Pos(System.Int32)
extern void EncoderContext_set_Pos_m447737B41665EEFC88A7F56BAC8D9784B8EA8FC9 ();
// 0x00000113 System.Text.StringBuilder ZXing.Datamatrix.Encoder.EncoderContext::get_Codewords()
extern void EncoderContext_get_Codewords_m2489D33CD728B1E1CF1AE1FBF07CDF367AF6F6A2 ();
// 0x00000114 ZXing.Datamatrix.Encoder.SymbolInfo ZXing.Datamatrix.Encoder.EncoderContext::get_SymbolInfo()
extern void EncoderContext_get_SymbolInfo_mFFB5185F4A6B35205A31BC2B0687C54639F5BADA ();
// 0x00000115 System.Int32 ZXing.Datamatrix.Encoder.EncoderContext::get_NewEncoding()
extern void EncoderContext_get_NewEncoding_m7ADEF28046A6020CCF69F19DDCC9DE22A2DE8528 ();
// 0x00000116 System.String ZXing.Datamatrix.Encoder.EncoderContext::get_Message()
extern void EncoderContext_get_Message_mC008DFA3436D7B18AE70D17696B04FA69231FE05 ();
// 0x00000117 System.Void ZXing.Datamatrix.Encoder.ErrorCorrection::.cctor()
extern void ErrorCorrection__cctor_m93166D1E927C86C42AEA0B97235A77A177C20816 ();
// 0x00000118 System.String ZXing.Datamatrix.Encoder.ErrorCorrection::encodeECC200(System.String,ZXing.Datamatrix.Encoder.SymbolInfo)
extern void ErrorCorrection_encodeECC200_m2E6EF339483574D20A3BCF0276DB171FFAB95CEB ();
// 0x00000119 System.String ZXing.Datamatrix.Encoder.ErrorCorrection::createECCBlock(System.String,System.Int32)
extern void ErrorCorrection_createECCBlock_mD9A9F6068D49D1DCB093D34F1B00A639DFF7513C ();
// 0x0000011A System.String ZXing.Datamatrix.Encoder.ErrorCorrection::createECCBlock(System.String,System.Int32,System.Int32,System.Int32)
extern void ErrorCorrection_createECCBlock_mBAFEB6C47566313E7CDAA9026F94CB51C09694D3 ();
// 0x0000011B System.Char ZXing.Datamatrix.Encoder.HighLevelEncoder::randomize253State(System.Char,System.Int32)
extern void HighLevelEncoder_randomize253State_m4B99E67922BC723DAA9F0BD08E2C13B602E97428 ();
// 0x0000011C System.String ZXing.Datamatrix.Encoder.HighLevelEncoder::encodeHighLevel(System.String,ZXing.Datamatrix.Encoder.SymbolShapeHint,ZXing.Dimension,ZXing.Dimension,System.Int32)
extern void HighLevelEncoder_encodeHighLevel_m46E0CBBDAB78E9207A15E7247CD232F6F3FCD1FB ();
// 0x0000011D System.Int32 ZXing.Datamatrix.Encoder.HighLevelEncoder::lookAheadTest(System.String,System.Int32,System.Int32)
extern void HighLevelEncoder_lookAheadTest_m92BB6C937FBFB8E239F373D4A733E55F091E66DC ();
// 0x0000011E System.Int32 ZXing.Datamatrix.Encoder.HighLevelEncoder::findMinimums(System.Single[],System.Int32[],System.Int32,System.Byte[])
extern void HighLevelEncoder_findMinimums_mC38ECAA2B8E783931BA2629A78505372A8A4220D ();
// 0x0000011F System.Int32 ZXing.Datamatrix.Encoder.HighLevelEncoder::getMinimumCount(System.Byte[])
extern void HighLevelEncoder_getMinimumCount_m21C07EF2B483FB6787E826B4449559F32C1D0DAB ();
// 0x00000120 System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isDigit(System.Char)
extern void HighLevelEncoder_isDigit_m3B3093F8F7540A49961C9219F70A801F7CBDE409 ();
// 0x00000121 System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isExtendedASCII(System.Char)
extern void HighLevelEncoder_isExtendedASCII_mD09A4A76901CE82416E5F526FEC9FC4E9EBB06EF ();
// 0x00000122 System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isNativeC40(System.Char)
extern void HighLevelEncoder_isNativeC40_m6CB164785EAD090E2AB675CCA371AB0229D38C17 ();
// 0x00000123 System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isNativeText(System.Char)
extern void HighLevelEncoder_isNativeText_m53ADB2C4F94CE1C91C533DEBA936A9B22FA48751 ();
// 0x00000124 System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isNativeX12(System.Char)
extern void HighLevelEncoder_isNativeX12_mAE5D7EC1E0BBADE55299524AD3C026E459FF9C74 ();
// 0x00000125 System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isX12TermSep(System.Char)
extern void HighLevelEncoder_isX12TermSep_m8A3357CD9D0F8CDE8345B50BC51B8D8C2862A6D6 ();
// 0x00000126 System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isNativeEDIFACT(System.Char)
extern void HighLevelEncoder_isNativeEDIFACT_m282041C2BD8247F59AD42C7161A9697FEC3DFFD4 ();
// 0x00000127 System.Boolean ZXing.Datamatrix.Encoder.HighLevelEncoder::isSpecialB256(System.Char)
extern void HighLevelEncoder_isSpecialB256_m3854D76A4AC01B90EA88B08AE7ECFED51EBD965B ();
// 0x00000128 System.Int32 ZXing.Datamatrix.Encoder.HighLevelEncoder::determineConsecutiveDigitCount(System.String,System.Int32)
extern void HighLevelEncoder_determineConsecutiveDigitCount_mB78CE09FEF1BEBB6AC5093FCEABC4453B3941E66 ();
// 0x00000129 System.Void ZXing.Datamatrix.Encoder.HighLevelEncoder::illegalCharacter(System.Char)
extern void HighLevelEncoder_illegalCharacter_mE125A4B1ED9E275AD43ACE27FB29649171728884 ();
// 0x0000012A System.Void ZXing.Datamatrix.Encoder.SymbolInfo::.ctor(System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void SymbolInfo__ctor_m46FF19F77A65FE4A6410280F60DE36EB6AAFD199 ();
// 0x0000012B System.Void ZXing.Datamatrix.Encoder.SymbolInfo::.ctor(System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void SymbolInfo__ctor_m9ADF770A020D45B12EC133FBF1EEDB33639250B5 ();
// 0x0000012C ZXing.Datamatrix.Encoder.SymbolInfo ZXing.Datamatrix.Encoder.SymbolInfo::lookup(System.Int32,ZXing.Datamatrix.Encoder.SymbolShapeHint,ZXing.Dimension,ZXing.Dimension,System.Boolean)
extern void SymbolInfo_lookup_m4D2DAB622BC59696BA46BFBAFFAE2A38F44D7A43 ();
// 0x0000012D System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getHorizontalDataRegions()
extern void SymbolInfo_getHorizontalDataRegions_m5BCC960BFE3B6E41451A1DF1CCFF9669FBEDA191 ();
// 0x0000012E System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getVerticalDataRegions()
extern void SymbolInfo_getVerticalDataRegions_m60BC96D1C1A2AAB2742FCA518D02AC12FB962568 ();
// 0x0000012F System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getSymbolDataWidth()
extern void SymbolInfo_getSymbolDataWidth_mFAE4F3F1978CD5AAA419E26AF4C2BCEA2ED3FA4C ();
// 0x00000130 System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getSymbolDataHeight()
extern void SymbolInfo_getSymbolDataHeight_mADA19D3D43477BF256DEE15C135C9715022DADFE ();
// 0x00000131 System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getSymbolWidth()
extern void SymbolInfo_getSymbolWidth_mA68950421BCDBDD480DC3CAB7A39B25AC07C9F19 ();
// 0x00000132 System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getSymbolHeight()
extern void SymbolInfo_getSymbolHeight_m71BE8D572FEEF517C3EE0F1563BE5EE901CEEAF5 ();
// 0x00000133 System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getInterleavedBlockCount()
extern void SymbolInfo_getInterleavedBlockCount_m1D3E413742D06B5D2ABB96CC0A35E35325E330E4 ();
// 0x00000134 System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getDataLengthForInterleavedBlock(System.Int32)
extern void SymbolInfo_getDataLengthForInterleavedBlock_mB2518CB22C004D3B7D332B051D772B70F4C4D5E2 ();
// 0x00000135 System.Int32 ZXing.Datamatrix.Encoder.SymbolInfo::getErrorLengthForInterleavedBlock(System.Int32)
extern void SymbolInfo_getErrorLengthForInterleavedBlock_mA5056E9B8302A9E9309AAEDCBD0EF74A94239059 ();
// 0x00000136 System.String ZXing.Datamatrix.Encoder.SymbolInfo::ToString()
extern void SymbolInfo_ToString_mFDA5D593AFD813C2CF1B097DB7947ED47DE44839 ();
// 0x00000137 System.Void ZXing.Datamatrix.Encoder.SymbolInfo::.cctor()
extern void SymbolInfo__cctor_m87B8716E6C160D1DE582263F770375D3FF1254EE ();
// 0x00000138 System.Void ZXing.Datamatrix.Encoder.TextEncoder::.ctor()
extern void TextEncoder__ctor_m56A82553B2C1F50A00CBB0A316E5DCAFB440D9A3 ();
// 0x00000139 System.Int32 ZXing.Datamatrix.Encoder.TextEncoder::get_EncodingMode()
extern void TextEncoder_get_EncodingMode_mD5210696A21436892D1397D403BF0CEE98FCEA87 ();
// 0x0000013A System.Int32 ZXing.Datamatrix.Encoder.TextEncoder::encodeChar(System.Char,System.Text.StringBuilder)
extern void TextEncoder_encodeChar_m928FFEC61C28655B325EC3F2E0BA156B43C106A9 ();
// 0x0000013B System.Void ZXing.Datamatrix.Encoder.X12Encoder::.ctor()
extern void X12Encoder__ctor_mADEC4C319CA6BF1C3AA93BB29F4F4B68FE6898D1 ();
// 0x0000013C System.Int32 ZXing.Datamatrix.Encoder.X12Encoder::get_EncodingMode()
extern void X12Encoder_get_EncodingMode_mBF3AA5B0509FB561447FFC2D8C5965D5FEA9D0B9 ();
// 0x0000013D System.Void ZXing.Datamatrix.Encoder.X12Encoder::encode(ZXing.Datamatrix.Encoder.EncoderContext)
extern void X12Encoder_encode_m7B099DD557F0568826A0AEBC0DBB7CADD8B59C64 ();
// 0x0000013E System.Int32 ZXing.Datamatrix.Encoder.X12Encoder::encodeChar(System.Char,System.Text.StringBuilder)
extern void X12Encoder_encodeChar_m8CA8B2A46FF37EDDB0F3410284FE18137138D2A3 ();
// 0x0000013F System.Void ZXing.Datamatrix.Encoder.X12Encoder::handleEOD(ZXing.Datamatrix.Encoder.EncoderContext,System.Text.StringBuilder)
extern void X12Encoder_handleEOD_m50D078D264E9D383FE30B0FA53BF0AD8E0D25597 ();
// 0x00000140 System.Int32 ZXing.Dimension::get_Width()
extern void Dimension_get_Width_mACCFA71D5D3A26E3F8E42DA90024D564655ED991 ();
// 0x00000141 System.Int32 ZXing.Dimension::get_Height()
extern void Dimension_get_Height_m5DEDBE3C4E34F3BA06BC14B89F41644DE0FBF020 ();
// 0x00000142 System.Void ZXing.MultiFormatWriter::.cctor()
extern void MultiFormatWriter__cctor_m1F83AB95E76186084300FDC74A829070C7C57BFA ();
// 0x00000143 System.Void ZXing.MultiFormatWriter::.ctor()
extern void MultiFormatWriter__ctor_m3F561F47E59DB70BAB7D6C70849F33A51D3F9BEB ();
// 0x00000144 ZXing.Common.BitMatrix ZXing.MultiFormatWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void MultiFormatWriter_encode_mA69844390DECC6B0715DDA3B36B6421053FD5646 ();
// 0x00000145 ZXing.Writer ZXing.MultiFormatWriter::<MultiFormatWriter>m__0()
extern void MultiFormatWriter_U3CMultiFormatWriterU3Em__0_m058BD0D8C0552103375C4A2468910BD713974B10 ();
// 0x00000146 ZXing.Writer ZXing.MultiFormatWriter::<MultiFormatWriter>m__1()
extern void MultiFormatWriter_U3CMultiFormatWriterU3Em__1_mC9CFCD52E56578199F731ADE7360BC7BE7964AC7 ();
// 0x00000147 ZXing.Writer ZXing.MultiFormatWriter::<MultiFormatWriter>m__2()
extern void MultiFormatWriter_U3CMultiFormatWriterU3Em__2_mDD92C482067636D5A962C024EB8121A71D96ECCE ();
// 0x00000148 ZXing.Writer ZXing.MultiFormatWriter::<MultiFormatWriter>m__3()
extern void MultiFormatWriter_U3CMultiFormatWriterU3Em__3_m66097729FEFA189191A847AF61997861DC8605D2 ();
// 0x00000149 ZXing.Writer ZXing.MultiFormatWriter::<MultiFormatWriter>m__4()
extern void MultiFormatWriter_U3CMultiFormatWriterU3Em__4_m5A96B249CEB330116C97662DABCE0669F30B59E3 ();
// 0x0000014A ZXing.Writer ZXing.MultiFormatWriter::<MultiFormatWriter>m__5()
extern void MultiFormatWriter_U3CMultiFormatWriterU3Em__5_mE4021ACA5C7B605988E62378D0EBFCB6DB9DADD4 ();
// 0x0000014B ZXing.Writer ZXing.MultiFormatWriter::<MultiFormatWriter>m__6()
extern void MultiFormatWriter_U3CMultiFormatWriterU3Em__6_m4DFF6B4AAAD8B16BF473D1D08051D0B908CEAFF6 ();
// 0x0000014C ZXing.Writer ZXing.MultiFormatWriter::<MultiFormatWriter>m__7()
extern void MultiFormatWriter_U3CMultiFormatWriterU3Em__7_mA5622E9819A052EC83E87D635A349240965D79B1 ();
// 0x0000014D ZXing.Writer ZXing.MultiFormatWriter::<MultiFormatWriter>m__8()
extern void MultiFormatWriter_U3CMultiFormatWriterU3Em__8_m024F89411F59C0958805B44A952835D1C2D3CFA4 ();
// 0x0000014E ZXing.Writer ZXing.MultiFormatWriter::<MultiFormatWriter>m__9()
extern void MultiFormatWriter_U3CMultiFormatWriterU3Em__9_m946B82ED0DA726757A99266DE24D3A13D8DB3693 ();
// 0x0000014F ZXing.Writer ZXing.MultiFormatWriter::<MultiFormatWriter>m__A()
extern void MultiFormatWriter_U3CMultiFormatWriterU3Em__A_m4B9DCE84DE764337D57DD23E844264911C910CC9 ();
// 0x00000150 ZXing.Writer ZXing.MultiFormatWriter::<MultiFormatWriter>m__B()
extern void MultiFormatWriter_U3CMultiFormatWriterU3Em__B_mBDC56BE23CE88881CC412685036A8A2D060E25B6 ();
// 0x00000151 ZXing.Writer ZXing.MultiFormatWriter::<MultiFormatWriter>m__C()
extern void MultiFormatWriter_U3CMultiFormatWriterU3Em__C_m93F50433EF820F62FF846B51B05199FC8E3BB2ED ();
// 0x00000152 System.Boolean ZXing.OneD.CodaBarReader::arrayContains(System.Char[],System.Char)
extern void CodaBarReader_arrayContains_mC9806AA3BEAD6FC8E6C32AEBE8F00E3DDACEE0A7 ();
// 0x00000153 System.Void ZXing.OneD.CodaBarReader::.cctor()
extern void CodaBarReader__cctor_mC5B5A2FFA2C57B104BC414FF3290FC6FB0417E6E ();
// 0x00000154 System.Void ZXing.OneD.CodaBarWriter::.ctor()
extern void CodaBarWriter__ctor_m5202D2CAAE23AD4D4DB74F23E7F9A2041578229B ();
// 0x00000155 System.Boolean[] ZXing.OneD.CodaBarWriter::encode(System.String)
extern void CodaBarWriter_encode_m8EB2486F77135E4D3CEB1D947F87815F564BDF10 ();
// 0x00000156 System.Void ZXing.OneD.CodaBarWriter::.cctor()
extern void CodaBarWriter__cctor_m504C94576A62648CFF6CBF8A0E0801E164F9F06A ();
// 0x00000157 System.Void ZXing.OneD.Code128Reader::.cctor()
extern void Code128Reader__cctor_m3DB7624E6B8C7869994BA5EC6D9CF00C06BE3E24 ();
// 0x00000158 System.Void ZXing.OneD.Code128Writer::.ctor()
extern void Code128Writer__ctor_m22B078CB74B77F0CC2CACAC963013D528716C54C ();
// 0x00000159 ZXing.Common.BitMatrix ZXing.OneD.Code128Writer::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void Code128Writer_encode_mC51F5EA6E2B7833DF96F9F4DBE88D7F6DAB88084 ();
// 0x0000015A System.Boolean[] ZXing.OneD.Code128Writer::encode(System.String)
extern void Code128Writer_encode_m9286F0B39A6B06C9EC09D0E4416C035AC73B4547 ();
// 0x0000015B System.Boolean ZXing.OneD.Code128Writer::isDigits(System.String,System.Int32,System.Int32)
extern void Code128Writer_isDigits_m96A99B8359090F5202E34B122692C7994BC38352 ();
// 0x0000015C System.Void ZXing.OneD.Code39Reader::.cctor()
extern void Code39Reader__cctor_m3B3BF9D619A9F539591757700C085FDBEF42D72F ();
// 0x0000015D System.Void ZXing.OneD.Code39Writer::.ctor()
extern void Code39Writer__ctor_m43BEA76902BE650E57A2E22DEAE0597C35F13E07 ();
// 0x0000015E ZXing.Common.BitMatrix ZXing.OneD.Code39Writer::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void Code39Writer_encode_m0D6EF9532618DC4C2D222D17CC2F4395B60D446D ();
// 0x0000015F System.Boolean[] ZXing.OneD.Code39Writer::encode(System.String)
extern void Code39Writer_encode_m155A582A2D0BAC2FD48345D29209930C62596EB9 ();
// 0x00000160 System.Void ZXing.OneD.Code39Writer::toIntArray(System.Int32,System.Int32[])
extern void Code39Writer_toIntArray_mDFFB1F39383CA12E3DADC3D7B428170E8190D888 ();
// 0x00000161 System.Void ZXing.OneD.EAN13Reader::.cctor()
extern void EAN13Reader__cctor_m00FC8F6485905889C10BF501536852BBC126C331 ();
// 0x00000162 System.Void ZXing.OneD.EAN13Writer::.ctor()
extern void EAN13Writer__ctor_m4BA9CDF077F7939F59B97C7E26632107905337CF ();
// 0x00000163 ZXing.Common.BitMatrix ZXing.OneD.EAN13Writer::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void EAN13Writer_encode_m14AB9E8542D567B70B6ED98672B57106F1EA3599 ();
// 0x00000164 System.Boolean[] ZXing.OneD.EAN13Writer::encode(System.String)
extern void EAN13Writer_encode_m21F4EA32965C436527FA0FDDAEBBF50968A83794 ();
// 0x00000165 System.Void ZXing.OneD.EAN8Writer::.ctor()
extern void EAN8Writer__ctor_mA595B48F927E64FC41516287D992E5354BB0BDA4 ();
// 0x00000166 ZXing.Common.BitMatrix ZXing.OneD.EAN8Writer::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void EAN8Writer_encode_m5D8A5724E05DAB33992FB819A8996BA817FC83BB ();
// 0x00000167 System.Boolean[] ZXing.OneD.EAN8Writer::encode(System.String)
extern void EAN8Writer_encode_mA77CE27B74C1FE8153251CF4A959CCAF3C86D10C ();
// 0x00000168 System.Void ZXing.OneD.ITFReader::.cctor()
extern void ITFReader__cctor_m27DB4754670C761A31CB80DA5926A3879260FA96 ();
// 0x00000169 System.Void ZXing.OneD.ITFWriter::.ctor()
extern void ITFWriter__ctor_mD7BAF73A3C0A6C2ADEF14CCD75C6C65D713C7DB5 ();
// 0x0000016A ZXing.Common.BitMatrix ZXing.OneD.ITFWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void ITFWriter_encode_m995A20BCE0DC27C55190734E0FF0D9713564ADA9 ();
// 0x0000016B System.Boolean[] ZXing.OneD.ITFWriter::encode(System.String)
extern void ITFWriter_encode_m0066503270D2FDCC8C2675265FEEB76CFB7516D1 ();
// 0x0000016C System.Void ZXing.OneD.ITFWriter::.cctor()
extern void ITFWriter__cctor_m0E557D2B5F84BD7EA240030FEB700CEB7DFB9363 ();
// 0x0000016D System.Void ZXing.OneD.MSIReader::.cctor()
extern void MSIReader__cctor_m792B9C2163A64B6D69D0A062C4786DEBBB7DFD46 ();
// 0x0000016E System.Void ZXing.OneD.MSIWriter::.ctor()
extern void MSIWriter__ctor_m6875D19008A44DB22BA2D09FBE8267720FBAACB3 ();
// 0x0000016F ZXing.Common.BitMatrix ZXing.OneD.MSIWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void MSIWriter_encode_m9ED75C0CBA96D7E7EB6E449DD6A599B3EB532BAC ();
// 0x00000170 System.Boolean[] ZXing.OneD.MSIWriter::encode(System.String)
extern void MSIWriter_encode_mB3369A4A82F1CEE1CFBE75A80B8BCD30D802B578 ();
// 0x00000171 System.Void ZXing.OneD.MSIWriter::.cctor()
extern void MSIWriter__cctor_m21D3CC2DF0E4A0194F4CCA7B94F844791119DDD3 ();
// 0x00000172 System.Void ZXing.OneD.OneDimensionalCodeWriter::.ctor()
extern void OneDimensionalCodeWriter__ctor_mBB8E043242D5DFDC8AD9234F859A043D0ACF5652 ();
// 0x00000173 ZXing.Common.BitMatrix ZXing.OneD.OneDimensionalCodeWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void OneDimensionalCodeWriter_encode_m7861865038725E794F40AC1CF7CA17285C86D099 ();
// 0x00000174 ZXing.Common.BitMatrix ZXing.OneD.OneDimensionalCodeWriter::renderResult(System.Boolean[],System.Int32,System.Int32,System.Int32)
extern void OneDimensionalCodeWriter_renderResult_m4B7FFDBEA64BF3F77DF2B1E4D366BB211AB432C4 ();
// 0x00000175 System.Int32 ZXing.OneD.OneDimensionalCodeWriter::appendPattern(System.Boolean[],System.Int32,System.Int32[],System.Boolean)
extern void OneDimensionalCodeWriter_appendPattern_m3D16DA3CB2C4E63F4274FA0EBD3CE2A725648931 ();
// 0x00000176 System.Int32 ZXing.OneD.OneDimensionalCodeWriter::get_DefaultMargin()
extern void OneDimensionalCodeWriter_get_DefaultMargin_m17487CC2733C1BE8694139CAD8E666C0D55ECE22 ();
// 0x00000177 System.Boolean[] ZXing.OneD.OneDimensionalCodeWriter::encode(System.String)
// 0x00000178 System.String ZXing.OneD.OneDimensionalCodeWriter::CalculateChecksumDigitModulo10(System.String)
extern void OneDimensionalCodeWriter_CalculateChecksumDigitModulo10_m3950D5CBC5AE98971ABC4D762A191A0E04C65F1C ();
// 0x00000179 System.Void ZXing.OneD.OneDReader::.cctor()
extern void OneDReader__cctor_mBB4730F04809247FD4C535080AE6FE407876D823 ();
// 0x0000017A System.Void ZXing.OneD.PlesseyWriter::.ctor()
extern void PlesseyWriter__ctor_mBD7EDAF3B209D7459C754586961C39A243F2E9D1 ();
// 0x0000017B ZXing.Common.BitMatrix ZXing.OneD.PlesseyWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void PlesseyWriter_encode_m7A22084B0EB8BCA13BEACD671E6FB517EF0257FD ();
// 0x0000017C System.Boolean[] ZXing.OneD.PlesseyWriter::encode(System.String)
extern void PlesseyWriter_encode_m74D4E1424D03A4B9E0F540D0CAFC880E0B8ECA12 ();
// 0x0000017D System.Void ZXing.OneD.PlesseyWriter::.cctor()
extern void PlesseyWriter__cctor_m2F567000902DFF6E1B7E09C1E168F790472CE731 ();
// 0x0000017E System.Void ZXing.OneD.UPCAWriter::.ctor()
extern void UPCAWriter__ctor_m3B824549A47812DAE9CB98654192251F73445A9C ();
// 0x0000017F ZXing.Common.BitMatrix ZXing.OneD.UPCAWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void UPCAWriter_encode_mE9A01BDD9B3DF4A0FB29EC50CC7DA6A223B6E375 ();
// 0x00000180 System.String ZXing.OneD.UPCAWriter::preencode(System.String)
extern void UPCAWriter_preencode_mACE2F02AD3073AAEDD2BBFC222CF7B5A17D7A0A5 ();
// 0x00000181 System.Void ZXing.OneD.UPCEANReader::.cctor()
extern void UPCEANReader__cctor_mD0CF54D611A3617883A374A1AD9844219280D15C ();
// 0x00000182 System.Boolean ZXing.OneD.UPCEANReader::checkStandardUPCEANChecksum(System.String)
extern void UPCEANReader_checkStandardUPCEANChecksum_mE33A361746A41CE9D38B29EBD3FA0CDEA397D90B ();
// 0x00000183 System.Void ZXing.OneD.UPCEANWriter::.ctor()
extern void UPCEANWriter__ctor_m0A28E8C9249F38E860C97422ED3CA70D0A8C46D6 ();
// 0x00000184 System.Int32 ZXing.OneD.UPCEANWriter::get_DefaultMargin()
extern void UPCEANWriter_get_DefaultMargin_mC6F57ADEE8B468029D63739375FE0D2A731D783E ();
// 0x00000185 System.Void ZXing.PDF417.Internal.BarcodeMatrix::.ctor(System.Int32,System.Int32)
extern void BarcodeMatrix__ctor_m5DBD05102C2AD6761E199CFFD6AD8633E6D4FC4F ();
// 0x00000186 System.Void ZXing.PDF417.Internal.BarcodeMatrix::startRow()
extern void BarcodeMatrix_startRow_mE36338175E41CDAA666D3BE9F2C6ADEABEA3BE29 ();
// 0x00000187 ZXing.PDF417.Internal.BarcodeRow ZXing.PDF417.Internal.BarcodeMatrix::getCurrentRow()
extern void BarcodeMatrix_getCurrentRow_mBAD8926BAC5196A0500A2A01390090EA41AEA674 ();
// 0x00000188 System.SByte[][] ZXing.PDF417.Internal.BarcodeMatrix::getScaledMatrix(System.Int32,System.Int32)
extern void BarcodeMatrix_getScaledMatrix_mB02AA858E62712D382782AA2F269D12675E3B305 ();
// 0x00000189 System.Void ZXing.PDF417.Internal.BarcodeRow::.ctor(System.Int32)
extern void BarcodeRow__ctor_m091E0F4546C595095DEA7A7DE2A0E9BA29C47A13 ();
// 0x0000018A System.Void ZXing.PDF417.Internal.BarcodeRow::set(System.Int32,System.Boolean)
extern void BarcodeRow_set_mE3BAA21801958C5608E86E761D5FC7D971D4DD5F ();
// 0x0000018B System.Void ZXing.PDF417.Internal.BarcodeRow::addBar(System.Boolean,System.Int32)
extern void BarcodeRow_addBar_m60654DA0FBF8DE37BE4B94BDE4E1BF23726F7C2C ();
// 0x0000018C System.SByte[] ZXing.PDF417.Internal.BarcodeRow::getScaledRow(System.Int32)
extern void BarcodeRow_getScaledRow_m4D5C0A6303045F395C6458EEB8E6CCBFDBC45C45 ();
// 0x0000018D System.Int32 ZXing.PDF417.Internal.Dimensions::get_MinCols()
extern void Dimensions_get_MinCols_mB8D2C7363D5C831C8D74D9FCA4CB1CFE85D7A641 ();
// 0x0000018E System.Int32 ZXing.PDF417.Internal.Dimensions::get_MaxCols()
extern void Dimensions_get_MaxCols_m8DF1BDAFAF1EFFFB6C76C043A61F3AD63B869121 ();
// 0x0000018F System.Int32 ZXing.PDF417.Internal.Dimensions::get_MinRows()
extern void Dimensions_get_MinRows_m525FE37BE5828E36C65D0D3ADE65DFCEEF8CA4EF ();
// 0x00000190 System.Int32 ZXing.PDF417.Internal.Dimensions::get_MaxRows()
extern void Dimensions_get_MaxRows_mCEEEE9EA9B5CAB6FDF18E7338973DD633F68D17A ();
// 0x00000191 System.Void ZXing.PDF417.Internal.PDF417::.ctor()
extern void PDF417__ctor_mF9ADE9689EF192C2C55CDBC28B086E4AC639514D ();
// 0x00000192 System.Void ZXing.PDF417.Internal.PDF417::.ctor(System.Boolean)
extern void PDF417__ctor_m45054C54F872DCF546398EBB6866F58F5CE8F3AC ();
// 0x00000193 ZXing.PDF417.Internal.BarcodeMatrix ZXing.PDF417.Internal.PDF417::get_BarcodeMatrix()
extern void PDF417_get_BarcodeMatrix_m24D58A573A242F616F3A63EFE896D1E4A927210A ();
// 0x00000194 System.Int32 ZXing.PDF417.Internal.PDF417::calculateNumberOfRows(System.Int32,System.Int32,System.Int32)
extern void PDF417_calculateNumberOfRows_mF3A0A3440F71845DA1B3B2E6F4814D0B4E2FA2A4 ();
// 0x00000195 System.Int32 ZXing.PDF417.Internal.PDF417::getNumberOfPadCodewords(System.Int32,System.Int32,System.Int32,System.Int32)
extern void PDF417_getNumberOfPadCodewords_m36E9A4C55DCA18D63DDF4164A117D3D6C92ED62A ();
// 0x00000196 System.Void ZXing.PDF417.Internal.PDF417::encodeChar(System.Int32,System.Int32,ZXing.PDF417.Internal.BarcodeRow)
extern void PDF417_encodeChar_mF802F2ABF981B0DC82E5D78257553774FF154611 ();
// 0x00000197 System.Void ZXing.PDF417.Internal.PDF417::encodeLowLevel(System.String,System.Int32,System.Int32,System.Int32,ZXing.PDF417.Internal.BarcodeMatrix)
extern void PDF417_encodeLowLevel_mC7E8814F16537C6A1592AED7355ED9C887FD11B9 ();
// 0x00000198 System.Void ZXing.PDF417.Internal.PDF417::generateBarcodeLogic(System.String,System.Int32)
extern void PDF417_generateBarcodeLogic_mCB020F6A8EC35D0D88F93700D78D471A9D3589F5 ();
// 0x00000199 System.Int32[] ZXing.PDF417.Internal.PDF417::determineDimensions(System.Int32,System.Int32)
extern void PDF417_determineDimensions_m27B2CB33E7770D8697EE235EEC75567647BC389C ();
// 0x0000019A System.Void ZXing.PDF417.Internal.PDF417::setDimensions(System.Int32,System.Int32,System.Int32,System.Int32)
extern void PDF417_setDimensions_mC4CDD7A9B545E17E90D0272713AFCE189C801E8A ();
// 0x0000019B System.Void ZXing.PDF417.Internal.PDF417::setCompaction(ZXing.PDF417.Internal.Compaction)
extern void PDF417_setCompaction_m836C7FE76054B28C08A1C17B60E6BAD4E0BEA3D4 ();
// 0x0000019C System.Void ZXing.PDF417.Internal.PDF417::setCompact(System.Boolean)
extern void PDF417_setCompact_mF1942E10F3EE643425EA9E1DA4B052888BD53994 ();
// 0x0000019D System.Void ZXing.PDF417.Internal.PDF417::setEncoding(System.String)
extern void PDF417_setEncoding_m534DAC5EC1FAD6DD24237BA3DE09A969A15B6F72 ();
// 0x0000019E System.Void ZXing.PDF417.Internal.PDF417::setDisableEci(System.Boolean)
extern void PDF417_setDisableEci_mE0693A66EA880E4AFB65E1B45E202755F37849A2 ();
// 0x0000019F System.Void ZXing.PDF417.Internal.PDF417::.cctor()
extern void PDF417__cctor_m38AF1F2805D350041C9FF9AC3782BFEB6B031308 ();
// 0x000001A0 System.Int32 ZXing.PDF417.Internal.PDF417ErrorCorrection::getErrorCorrectionCodewordCount(System.Int32)
extern void PDF417ErrorCorrection_getErrorCorrectionCodewordCount_m96D0CE86CAC53266702D10CD0AE2CC30750CB203 ();
// 0x000001A1 System.String ZXing.PDF417.Internal.PDF417ErrorCorrection::generateErrorCorrection(System.String,System.Int32)
extern void PDF417ErrorCorrection_generateErrorCorrection_m0807A2A8DA2A6F74F334C526F81E27DC06E7C376 ();
// 0x000001A2 System.Void ZXing.PDF417.Internal.PDF417ErrorCorrection::.cctor()
extern void PDF417ErrorCorrection__cctor_mA0ACFA7AC1C8D495F64CC0954D9866E5B59CCEEE ();
// 0x000001A3 System.Void ZXing.PDF417.Internal.PDF417HighLevelEncoder::.cctor()
extern void PDF417HighLevelEncoder__cctor_mA8C4376FA06A0315699DFC078D1AEB7D594E8C27 ();
// 0x000001A4 System.String ZXing.PDF417.Internal.PDF417HighLevelEncoder::encodeHighLevel(System.String,ZXing.PDF417.Internal.Compaction,System.Text.Encoding,System.Boolean)
extern void PDF417HighLevelEncoder_encodeHighLevel_m7083A8CDECD7430AB6B00D9F241EAC1268CB8C87 ();
// 0x000001A5 System.Int32 ZXing.PDF417.Internal.PDF417HighLevelEncoder::encodeText(System.String,System.Int32,System.Int32,System.Text.StringBuilder,System.Int32)
extern void PDF417HighLevelEncoder_encodeText_m1DDF567682712F498DEA0F90FC7C9CEA3577BE38 ();
// 0x000001A6 System.Void ZXing.PDF417.Internal.PDF417HighLevelEncoder::encodeBinary(System.Byte[],System.Int32,System.Int32,System.Int32,System.Text.StringBuilder)
extern void PDF417HighLevelEncoder_encodeBinary_m4DFA40CBF13BBB42B59F74774E0409D326E01447 ();
// 0x000001A7 System.Void ZXing.PDF417.Internal.PDF417HighLevelEncoder::encodeNumeric(System.String,System.Int32,System.Int32,System.Text.StringBuilder)
extern void PDF417HighLevelEncoder_encodeNumeric_m03C76815D3EA23BEAC9BBD7336435CAA6C6964BC ();
// 0x000001A8 System.Boolean ZXing.PDF417.Internal.PDF417HighLevelEncoder::isDigit(System.Char)
extern void PDF417HighLevelEncoder_isDigit_m3562DD53157C7C3C7EA2B5A0E0CC14F94188BD26 ();
// 0x000001A9 System.Boolean ZXing.PDF417.Internal.PDF417HighLevelEncoder::isAlphaUpper(System.Char)
extern void PDF417HighLevelEncoder_isAlphaUpper_m1549A2A58B18709B61457BCDB49588A188919758 ();
// 0x000001AA System.Boolean ZXing.PDF417.Internal.PDF417HighLevelEncoder::isAlphaLower(System.Char)
extern void PDF417HighLevelEncoder_isAlphaLower_m6E70B55DAE6E77B519222265599D806BB62C690C ();
// 0x000001AB System.Boolean ZXing.PDF417.Internal.PDF417HighLevelEncoder::isMixed(System.Char)
extern void PDF417HighLevelEncoder_isMixed_mD79AB0FF5773D30F1106EEE38F9758A0ABED9404 ();
// 0x000001AC System.Boolean ZXing.PDF417.Internal.PDF417HighLevelEncoder::isPunctuation(System.Char)
extern void PDF417HighLevelEncoder_isPunctuation_m6D3FE0F92C091195A70FFF6F3D16C34872ABEC85 ();
// 0x000001AD System.Boolean ZXing.PDF417.Internal.PDF417HighLevelEncoder::isText(System.Char)
extern void PDF417HighLevelEncoder_isText_m15068E3B6C85C685A09404F03FCEED35479F5870 ();
// 0x000001AE System.Int32 ZXing.PDF417.Internal.PDF417HighLevelEncoder::determineConsecutiveDigitCount(System.String,System.Int32)
extern void PDF417HighLevelEncoder_determineConsecutiveDigitCount_mFA58B9FE87C4F2F5A7A7977DF7BCB9CFA6F8B6E3 ();
// 0x000001AF System.Int32 ZXing.PDF417.Internal.PDF417HighLevelEncoder::determineConsecutiveTextCount(System.String,System.Int32)
extern void PDF417HighLevelEncoder_determineConsecutiveTextCount_m56FE630D7BEBEF8569194CB6F9B5BB2FC8461909 ();
// 0x000001B0 System.Int32 ZXing.PDF417.Internal.PDF417HighLevelEncoder::determineConsecutiveBinaryCount(System.String,System.Byte[],System.Int32)
extern void PDF417HighLevelEncoder_determineConsecutiveBinaryCount_mD47FFBA34B82E1F9BE5CA0474CD043B96F1D8320 ();
// 0x000001B1 System.Void ZXing.PDF417.Internal.PDF417HighLevelEncoder::encodingECI(System.Int32,System.Text.StringBuilder)
extern void PDF417HighLevelEncoder_encodingECI_m25D3B5569531383232B1C00297DFA9ABD36D7C0B ();
// 0x000001B2 System.Void ZXing.PDF417.PDF417Writer::.ctor()
extern void PDF417Writer__ctor_mAC725952C2CAE0E1D362947812C71EE6A0B00AB2 ();
// 0x000001B3 ZXing.Common.BitMatrix ZXing.PDF417.PDF417Writer::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void PDF417Writer_encode_m0F2A3280FAAF82F5CAD66591111F42070D30F4BF ();
// 0x000001B4 ZXing.Common.BitMatrix ZXing.PDF417.PDF417Writer::bitMatrixFromEncoder(ZXing.PDF417.Internal.PDF417,System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern void PDF417Writer_bitMatrixFromEncoder_mE15022FF1984EDF932770BFE9E475B6EF6540F7A ();
// 0x000001B5 ZXing.Common.BitMatrix ZXing.PDF417.PDF417Writer::bitMatrixFrombitArray(System.SByte[][],System.Int32)
extern void PDF417Writer_bitMatrixFrombitArray_mE8C4368A936A2ACBD161CA234907D687AE41C0FC ();
// 0x000001B6 System.SByte[][] ZXing.PDF417.PDF417Writer::rotateArray(System.SByte[][])
extern void PDF417Writer_rotateArray_m6E2AD6244374C3985D44E10AAA8975669C2CAD46 ();
// 0x000001B7 System.Void ZXing.QrCode.Internal.ErrorCorrectionLevel::.ctor(System.Int32,System.Int32,System.String)
extern void ErrorCorrectionLevel__ctor_m27A32FDDFE0B62FE1586AEC0141C015D836BE553 ();
// 0x000001B8 System.Int32 ZXing.QrCode.Internal.ErrorCorrectionLevel::get_Bits()
extern void ErrorCorrectionLevel_get_Bits_m2152ACD3809B1FE1073B0393B573240EC1C3505B ();
// 0x000001B9 System.Int32 ZXing.QrCode.Internal.ErrorCorrectionLevel::ordinal()
extern void ErrorCorrectionLevel_ordinal_mEEB9A2AB26BB498E953CE969D388FC916E522469 ();
// 0x000001BA System.String ZXing.QrCode.Internal.ErrorCorrectionLevel::ToString()
extern void ErrorCorrectionLevel_ToString_m6811EE04E4FDD101CE0791CC36BBF6C0D45F835C ();
// 0x000001BB System.Void ZXing.QrCode.Internal.ErrorCorrectionLevel::.cctor()
extern void ErrorCorrectionLevel__cctor_m0012DCB41E119F1920E24EABC47C6517336A4A46 ();
// 0x000001BC System.Void ZXing.QrCode.Internal.Mode::.ctor(System.Int32[],System.Int32,System.String)
extern void Mode__ctor_mB43F347BDA84F6293144BB8DEF73E1EA7D51ACDE ();
// 0x000001BD System.Int32 ZXing.QrCode.Internal.Mode::getCharacterCountBits(ZXing.QrCode.Internal.Version)
extern void Mode_getCharacterCountBits_m0443B77EDFE0DB92DFBC4B66D07FDADDC433356B ();
// 0x000001BE System.Int32 ZXing.QrCode.Internal.Mode::get_Bits()
extern void Mode_get_Bits_mF24E1F86934F81902116855C8DD5C693CF02FF90 ();
// 0x000001BF System.String ZXing.QrCode.Internal.Mode::ToString()
extern void Mode_ToString_m9F7D99459C5B76354A0925137B4356C0847B4A8E ();
// 0x000001C0 System.Void ZXing.QrCode.Internal.Mode::.cctor()
extern void Mode__cctor_mA7C80875E2817334E24F176CFB22A89964B6755F ();
// 0x000001C1 System.Void ZXing.QrCode.Internal.Version::.ctor(System.Int32,System.Int32[],ZXing.QrCode.Internal.Version_ECBlocks[])
extern void Version__ctor_m035D6B8BCF041DBF1123AE7001DC9874BB27AC84 ();
// 0x000001C2 System.Int32 ZXing.QrCode.Internal.Version::get_VersionNumber()
extern void Version_get_VersionNumber_m3762234275AB877967256435C89AD25A89770BF9 ();
// 0x000001C3 System.Int32 ZXing.QrCode.Internal.Version::get_TotalCodewords()
extern void Version_get_TotalCodewords_m639C102A1E8F571547502DE477E4CB364FB079A3 ();
// 0x000001C4 System.Int32 ZXing.QrCode.Internal.Version::get_DimensionForVersion()
extern void Version_get_DimensionForVersion_m76017F0F21F3D4EC59FDD9776DA1F04C0BC91B20 ();
// 0x000001C5 ZXing.QrCode.Internal.Version_ECBlocks ZXing.QrCode.Internal.Version::getECBlocksForLevel(ZXing.QrCode.Internal.ErrorCorrectionLevel)
extern void Version_getECBlocksForLevel_m4615B061C19EFA90ABBD3299E4E5125D4DBE3CDE ();
// 0x000001C6 ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.Version::getVersionForNumber(System.Int32)
extern void Version_getVersionForNumber_m07F3C488AE71EF6E0187BF28A54DF1F125D337BE ();
// 0x000001C7 System.String ZXing.QrCode.Internal.Version::ToString()
extern void Version_ToString_m5EDC04A486C3D2B8FEC715DEA91F4678B2DD8115 ();
// 0x000001C8 ZXing.QrCode.Internal.Version[] ZXing.QrCode.Internal.Version::buildVersions()
extern void Version_buildVersions_mAF4E45F358E2E8A62FE8626A5168695921533D49 ();
// 0x000001C9 System.Void ZXing.QrCode.Internal.Version::.cctor()
extern void Version__cctor_mC044E1E70D7FA2A267353A28D6D6D5ACFE75DB7A ();
// 0x000001CA System.Void ZXing.QrCode.Internal.Version_ECBlocks::.ctor(System.Int32,ZXing.QrCode.Internal.Version_ECB[])
extern void ECBlocks__ctor_m273BD850A3379D1335D32BC7EE1421CA6F60F6DA ();
// 0x000001CB System.Int32 ZXing.QrCode.Internal.Version_ECBlocks::get_ECCodewordsPerBlock()
extern void ECBlocks_get_ECCodewordsPerBlock_m038DC27C12D5D308E469DB99571FD02B307C0B1B ();
// 0x000001CC System.Int32 ZXing.QrCode.Internal.Version_ECBlocks::get_NumBlocks()
extern void ECBlocks_get_NumBlocks_mB6635BF406786F37EE84719EF4F4EB5545EDF2E1 ();
// 0x000001CD System.Int32 ZXing.QrCode.Internal.Version_ECBlocks::get_TotalECCodewords()
extern void ECBlocks_get_TotalECCodewords_m7822705F7441B8531FFD48D55455AA9B80048F8C ();
// 0x000001CE ZXing.QrCode.Internal.Version_ECB[] ZXing.QrCode.Internal.Version_ECBlocks::getECBlocks()
extern void ECBlocks_getECBlocks_mE3BCB1A131F4B969CA507F40B4BC943CB96A7C7C ();
// 0x000001CF System.Void ZXing.QrCode.Internal.Version_ECB::.ctor(System.Int32,System.Int32)
extern void ECB__ctor_mEEE7BB0A54D58BFAE0ED2EC3DBA48430602DD669 ();
// 0x000001D0 System.Int32 ZXing.QrCode.Internal.Version_ECB::get_Count()
extern void ECB_get_Count_m4FBB8FD4E11F2B8CE59D5EE756DF988D43C37D47 ();
// 0x000001D1 System.Int32 ZXing.QrCode.Internal.Version_ECB::get_DataCodewords()
extern void ECB_get_DataCodewords_m694477F0AD79E182212DECF38DCD53C259019B21 ();
// 0x000001D2 System.Void ZXing.QrCode.Internal.BlockPair::.ctor(System.Byte[],System.Byte[])
extern void BlockPair__ctor_m644FB7916C45868707C61F60FE859D8F28E9CAC0 ();
// 0x000001D3 System.Byte[] ZXing.QrCode.Internal.BlockPair::get_DataBytes()
extern void BlockPair_get_DataBytes_mACAFC0FC28CC4589485DDC6AB016FF224748542C ();
// 0x000001D4 System.Byte[] ZXing.QrCode.Internal.BlockPair::get_ErrorCorrectionBytes()
extern void BlockPair_get_ErrorCorrectionBytes_m45E295FFB335B83D3BE687CA78FCE9304A496E41 ();
// 0x000001D5 System.Void ZXing.QrCode.Internal.ByteMatrix::.ctor(System.Int32,System.Int32)
extern void ByteMatrix__ctor_mE319431A4A48D92ABE1C0B039C27665C60D26CD5 ();
// 0x000001D6 System.Int32 ZXing.QrCode.Internal.ByteMatrix::get_Height()
extern void ByteMatrix_get_Height_m6FEEA80C7E4835A939F495C28FB903D37E7A1F27 ();
// 0x000001D7 System.Int32 ZXing.QrCode.Internal.ByteMatrix::get_Width()
extern void ByteMatrix_get_Width_m930710557D0607CFFF125AC86B8E536684CA7B75 ();
// 0x000001D8 System.Int32 ZXing.QrCode.Internal.ByteMatrix::get_Item(System.Int32,System.Int32)
extern void ByteMatrix_get_Item_m4F36BABC12EBEF956138502F8BD031279CA27D88 ();
// 0x000001D9 System.Void ZXing.QrCode.Internal.ByteMatrix::set_Item(System.Int32,System.Int32,System.Int32)
extern void ByteMatrix_set_Item_m8F90ECD9E92D37625AD27AB96B64D92E4D1F6DED ();
// 0x000001DA System.Byte[][] ZXing.QrCode.Internal.ByteMatrix::get_Array()
extern void ByteMatrix_get_Array_m970682D3B1F77B7510E6D0FD1EB9FC1D3846C351 ();
// 0x000001DB System.Void ZXing.QrCode.Internal.ByteMatrix::set(System.Int32,System.Int32,System.Boolean)
extern void ByteMatrix_set_mDB606FE13323A95D7C272A433B6F7C4153B37644 ();
// 0x000001DC System.Void ZXing.QrCode.Internal.ByteMatrix::clear(System.Byte)
extern void ByteMatrix_clear_m3355B67D71EF69B946B2671FF1C24BF3C751D58E ();
// 0x000001DD System.String ZXing.QrCode.Internal.ByteMatrix::ToString()
extern void ByteMatrix_ToString_m656C037C3358AEE9BB909650D5D6EB670EFD2F7B ();
// 0x000001DE System.Int32 ZXing.QrCode.Internal.Encoder::calculateMaskPenalty(ZXing.QrCode.Internal.ByteMatrix)
extern void Encoder_calculateMaskPenalty_m215A23067D072A9698AB5E4CB35873AB512CD8C2 ();
// 0x000001DF ZXing.QrCode.Internal.QRCode ZXing.QrCode.Internal.Encoder::encode(System.String,ZXing.QrCode.Internal.ErrorCorrectionLevel,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void Encoder_encode_m7A8C877527E7D3F4365C54DE1B51438A67A616AF ();
// 0x000001E0 System.Int32 ZXing.QrCode.Internal.Encoder::getAlphanumericCode(System.Int32)
extern void Encoder_getAlphanumericCode_m687FFAE869348CD76C86957673D1C41101E5FA2E ();
// 0x000001E1 ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Encoder::chooseMode(System.String,System.String)
extern void Encoder_chooseMode_m23D932730C0E9E2E26DC7CA56B8B80C14178B68E ();
// 0x000001E2 System.Boolean ZXing.QrCode.Internal.Encoder::isOnlyDoubleByteKanji(System.String)
extern void Encoder_isOnlyDoubleByteKanji_mF261330E02EE372D9AA64DC407B69AF1E6DB988E ();
// 0x000001E3 System.Int32 ZXing.QrCode.Internal.Encoder::chooseMaskPattern(ZXing.Common.BitArray,ZXing.QrCode.Internal.ErrorCorrectionLevel,ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.ByteMatrix)
extern void Encoder_chooseMaskPattern_m0748FB6AA8EEE8BF9263A75BE90A04DCC216F4BB ();
// 0x000001E4 ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.Encoder::chooseVersion(System.Int32,ZXing.QrCode.Internal.ErrorCorrectionLevel)
extern void Encoder_chooseVersion_mC9E221FA4FE07FE4E3049E221228984A7D23A4D1 ();
// 0x000001E5 System.Void ZXing.QrCode.Internal.Encoder::terminateBits(System.Int32,ZXing.Common.BitArray)
extern void Encoder_terminateBits_m772AF3829AAB3E651DA314681E7251359BC55DE2 ();
// 0x000001E6 System.Void ZXing.QrCode.Internal.Encoder::getNumDataBytesAndNumECBytesForBlockID(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32[],System.Int32[])
extern void Encoder_getNumDataBytesAndNumECBytesForBlockID_m306AB6ED034E0DA8C703FC76753890E3E32AF373 ();
// 0x000001E7 ZXing.Common.BitArray ZXing.QrCode.Internal.Encoder::interleaveWithECBytes(ZXing.Common.BitArray,System.Int32,System.Int32,System.Int32)
extern void Encoder_interleaveWithECBytes_m9CDC66A9E752F7599EEC9556027FAA247B8D1280 ();
// 0x000001E8 System.Byte[] ZXing.QrCode.Internal.Encoder::generateECBytes(System.Byte[],System.Int32)
extern void Encoder_generateECBytes_mE7A1D8214278E1D21911B3B171CC0DEC5B24B553 ();
// 0x000001E9 System.Void ZXing.QrCode.Internal.Encoder::appendModeInfo(ZXing.QrCode.Internal.Mode,ZXing.Common.BitArray)
extern void Encoder_appendModeInfo_m5A5B954761F8753C897701E127B4D441383ECD9D ();
// 0x000001EA System.Void ZXing.QrCode.Internal.Encoder::appendLengthInfo(System.Int32,ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.Mode,ZXing.Common.BitArray)
extern void Encoder_appendLengthInfo_mC4AD32C750CD9E32DEBDE5C4CC4C3981AB5CDFC6 ();
// 0x000001EB System.Void ZXing.QrCode.Internal.Encoder::appendBytes(System.String,ZXing.QrCode.Internal.Mode,ZXing.Common.BitArray,System.String)
extern void Encoder_appendBytes_mD072A75FB468DDC8DC570CEB375731E2AB12C461 ();
// 0x000001EC System.Void ZXing.QrCode.Internal.Encoder::appendNumericBytes(System.String,ZXing.Common.BitArray)
extern void Encoder_appendNumericBytes_m61A265C537F6E1ACBA9A4000C7E91947E23EBB2A ();
// 0x000001ED System.Void ZXing.QrCode.Internal.Encoder::appendAlphanumericBytes(System.String,ZXing.Common.BitArray)
extern void Encoder_appendAlphanumericBytes_m1DF5FEF2BB2A348AABDA85F7F708B01539C55FEC ();
// 0x000001EE System.Void ZXing.QrCode.Internal.Encoder::append8BitBytes(System.String,ZXing.Common.BitArray,System.String)
extern void Encoder_append8BitBytes_mD56813DE06D3606FAC8B840452082C6D575D3450 ();
// 0x000001EF System.Void ZXing.QrCode.Internal.Encoder::appendKanjiBytes(System.String,ZXing.Common.BitArray)
extern void Encoder_appendKanjiBytes_m31816240EBCD08D0D75FCF92B554923B146C6148 ();
// 0x000001F0 System.Void ZXing.QrCode.Internal.Encoder::appendECI(ZXing.Common.CharacterSetECI,ZXing.Common.BitArray)
extern void Encoder_appendECI_m93FC04566D742677A28527204078BE4201683970 ();
// 0x000001F1 System.Void ZXing.QrCode.Internal.Encoder::.cctor()
extern void Encoder__cctor_m3CA40BBD197581777CCD0815830AA9954946A78E ();
// 0x000001F2 System.Int32 ZXing.QrCode.Internal.MaskUtil::applyMaskPenaltyRule1(ZXing.QrCode.Internal.ByteMatrix)
extern void MaskUtil_applyMaskPenaltyRule1_mEA63906366897B80B1699B56392FD106ED53B434 ();
// 0x000001F3 System.Int32 ZXing.QrCode.Internal.MaskUtil::applyMaskPenaltyRule2(ZXing.QrCode.Internal.ByteMatrix)
extern void MaskUtil_applyMaskPenaltyRule2_m8158E370D36E1747358B6D191AF9FD997F40941E ();
// 0x000001F4 System.Int32 ZXing.QrCode.Internal.MaskUtil::applyMaskPenaltyRule3(ZXing.QrCode.Internal.ByteMatrix)
extern void MaskUtil_applyMaskPenaltyRule3_m9A9DCE1046E83939C3E655103730F52967474813 ();
// 0x000001F5 System.Boolean ZXing.QrCode.Internal.MaskUtil::isWhiteHorizontal(System.Byte[],System.Int32,System.Int32)
extern void MaskUtil_isWhiteHorizontal_mD951D798698E7884B80E52E3BD1DE5A631FC1C02 ();
// 0x000001F6 System.Boolean ZXing.QrCode.Internal.MaskUtil::isWhiteVertical(System.Byte[][],System.Int32,System.Int32,System.Int32)
extern void MaskUtil_isWhiteVertical_mD0618326F1FE97B3717D4AC6A11DBB4EE8EC849D ();
// 0x000001F7 System.Int32 ZXing.QrCode.Internal.MaskUtil::applyMaskPenaltyRule4(ZXing.QrCode.Internal.ByteMatrix)
extern void MaskUtil_applyMaskPenaltyRule4_mE6C58669E7007119F716CEFBDB9F19F2C18F083C ();
// 0x000001F8 System.Boolean ZXing.QrCode.Internal.MaskUtil::getDataMaskBit(System.Int32,System.Int32,System.Int32)
extern void MaskUtil_getDataMaskBit_m5A4B1F376C54704BAC501E78033364928971F3EF ();
// 0x000001F9 System.Int32 ZXing.QrCode.Internal.MaskUtil::applyMaskPenaltyRule1Internal(ZXing.QrCode.Internal.ByteMatrix,System.Boolean)
extern void MaskUtil_applyMaskPenaltyRule1Internal_m48550444F3D202B9278C853F4D1ADD732EC8DA24 ();
// 0x000001FA System.Void ZXing.QrCode.Internal.MatrixUtil::clearMatrix(ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_clearMatrix_mF83179FD4D1FB0F5D89F744D050CD06534CF4FBF ();
// 0x000001FB System.Void ZXing.QrCode.Internal.MatrixUtil::buildMatrix(ZXing.Common.BitArray,ZXing.QrCode.Internal.ErrorCorrectionLevel,ZXing.QrCode.Internal.Version,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_buildMatrix_m9C894A4849ED2F812CE1FE9F489F20119DE16561 ();
// 0x000001FC System.Void ZXing.QrCode.Internal.MatrixUtil::embedBasicPatterns(ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_embedBasicPatterns_m65FDAFD119B3448D0AB5513CE4193483AB809C5E ();
// 0x000001FD System.Void ZXing.QrCode.Internal.MatrixUtil::embedTypeInfo(ZXing.QrCode.Internal.ErrorCorrectionLevel,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_embedTypeInfo_m7E7B2ADB02910195198EEB6B688AA1DB4E183056 ();
// 0x000001FE System.Void ZXing.QrCode.Internal.MatrixUtil::maybeEmbedVersionInfo(ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_maybeEmbedVersionInfo_m2FCC46EF4E66A798A412AC0CE3F1434D9C4A76B0 ();
// 0x000001FF System.Void ZXing.QrCode.Internal.MatrixUtil::embedDataBits(ZXing.Common.BitArray,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_embedDataBits_mEBE77110DE06B78ECC970F282B841EA3C7B43801 ();
// 0x00000200 System.Int32 ZXing.QrCode.Internal.MatrixUtil::findMSBSet(System.Int32)
extern void MatrixUtil_findMSBSet_m3C20BCCEF8CAAFCF2A59C8C146D1267408F55250 ();
// 0x00000201 System.Int32 ZXing.QrCode.Internal.MatrixUtil::calculateBCHCode(System.Int32,System.Int32)
extern void MatrixUtil_calculateBCHCode_m1EB414F29A575EEE2DF2159DB84B3CD3FA330D74 ();
// 0x00000202 System.Void ZXing.QrCode.Internal.MatrixUtil::makeTypeInfoBits(ZXing.QrCode.Internal.ErrorCorrectionLevel,System.Int32,ZXing.Common.BitArray)
extern void MatrixUtil_makeTypeInfoBits_m024083AC3E075161BFBAB0CA8F444FC68040431F ();
// 0x00000203 System.Void ZXing.QrCode.Internal.MatrixUtil::makeVersionInfoBits(ZXing.QrCode.Internal.Version,ZXing.Common.BitArray)
extern void MatrixUtil_makeVersionInfoBits_mA18CDE4C212E8226DCCCEFF83C443661144C7B89 ();
// 0x00000204 System.Boolean ZXing.QrCode.Internal.MatrixUtil::isEmpty(System.Int32)
extern void MatrixUtil_isEmpty_m5CD556AB8396F6E43E2D713BC1E14E214C2C8341 ();
// 0x00000205 System.Void ZXing.QrCode.Internal.MatrixUtil::embedTimingPatterns(ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_embedTimingPatterns_mA8488BBC0A2E3F66753E12F6E5BB9A8505E0377E ();
// 0x00000206 System.Void ZXing.QrCode.Internal.MatrixUtil::embedDarkDotAtLeftBottomCorner(ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_embedDarkDotAtLeftBottomCorner_mB9FEB816B1F9F6B19F076CB36AF7EDDD72887261 ();
// 0x00000207 System.Void ZXing.QrCode.Internal.MatrixUtil::embedHorizontalSeparationPattern(System.Int32,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_embedHorizontalSeparationPattern_m74C6F55E9E99A039A7DB5F3EB02B933139D49287 ();
// 0x00000208 System.Void ZXing.QrCode.Internal.MatrixUtil::embedVerticalSeparationPattern(System.Int32,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_embedVerticalSeparationPattern_m14B39EC0AF586DA8B46934978BDAFDF0CC737518 ();
// 0x00000209 System.Void ZXing.QrCode.Internal.MatrixUtil::embedPositionAdjustmentPattern(System.Int32,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_embedPositionAdjustmentPattern_mDEBC1795EFAA43156098FFA116EB0D83EEB700B6 ();
// 0x0000020A System.Void ZXing.QrCode.Internal.MatrixUtil::embedPositionDetectionPattern(System.Int32,System.Int32,ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_embedPositionDetectionPattern_m683E83BE75FAD3113CD39DE2B7E966E4F4B26F62 ();
// 0x0000020B System.Void ZXing.QrCode.Internal.MatrixUtil::embedPositionDetectionPatternsAndSeparators(ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_embedPositionDetectionPatternsAndSeparators_m57ACC8675796F629640F7760B032FBFF32799C51 ();
// 0x0000020C System.Void ZXing.QrCode.Internal.MatrixUtil::maybeEmbedPositionAdjustmentPatterns(ZXing.QrCode.Internal.Version,ZXing.QrCode.Internal.ByteMatrix)
extern void MatrixUtil_maybeEmbedPositionAdjustmentPatterns_mA17AD1BCC4921046BA0DF666BA04E107A6D6EE64 ();
// 0x0000020D System.Void ZXing.QrCode.Internal.MatrixUtil::.cctor()
extern void MatrixUtil__cctor_m49D328B7E0A02F70766EBDAC55D62E0BFEB93285 ();
// 0x0000020E System.Void ZXing.QrCode.Internal.QRCode::.ctor()
extern void QRCode__ctor_m920631BE88BC14762FD14BB110E84809F4F1A2B9 ();
// 0x0000020F ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.QRCode::get_Mode()
extern void QRCode_get_Mode_mE3911E5CD54E185F37448BA3FAC0AFBA7EE0D39F ();
// 0x00000210 System.Void ZXing.QrCode.Internal.QRCode::set_Mode(ZXing.QrCode.Internal.Mode)
extern void QRCode_set_Mode_m6E8D799E18D75A0F9488D9A4D207C00C4B603A60 ();
// 0x00000211 ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.QRCode::get_ECLevel()
extern void QRCode_get_ECLevel_m34D2255E9A0A349BBB961611A1CFE58B0EAC33D9 ();
// 0x00000212 System.Void ZXing.QrCode.Internal.QRCode::set_ECLevel(ZXing.QrCode.Internal.ErrorCorrectionLevel)
extern void QRCode_set_ECLevel_mBAD215F7143693D8F8C7CD098601F1BA645C55F0 ();
// 0x00000213 ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.QRCode::get_Version()
extern void QRCode_get_Version_m1FB98D178F34C2AAE4BD26FE34BDDD1078D9CA59 ();
// 0x00000214 System.Void ZXing.QrCode.Internal.QRCode::set_Version(ZXing.QrCode.Internal.Version)
extern void QRCode_set_Version_m65521E9FCB5B4DFD8E279C40702C5B8593CA56BB ();
// 0x00000215 System.Int32 ZXing.QrCode.Internal.QRCode::get_MaskPattern()
extern void QRCode_get_MaskPattern_mD72431EB03A36284CC8E9164C2A174A8FE72CE58 ();
// 0x00000216 System.Void ZXing.QrCode.Internal.QRCode::set_MaskPattern(System.Int32)
extern void QRCode_set_MaskPattern_mD6CD63CD0BE5BAA3AFBA1466AA3FD584785DC529 ();
// 0x00000217 ZXing.QrCode.Internal.ByteMatrix ZXing.QrCode.Internal.QRCode::get_Matrix()
extern void QRCode_get_Matrix_m1D78FE3793DCF8D820A92DB86653DA5C8068DB17 ();
// 0x00000218 System.Void ZXing.QrCode.Internal.QRCode::set_Matrix(ZXing.QrCode.Internal.ByteMatrix)
extern void QRCode_set_Matrix_mDA276361828EEA7DC6E0271A8A9519CFAB56650F ();
// 0x00000219 System.String ZXing.QrCode.Internal.QRCode::ToString()
extern void QRCode_ToString_mCE2DD343CF18C8394D404C86BD9D7A3DC009C25A ();
// 0x0000021A System.Boolean ZXing.QrCode.Internal.QRCode::isValidMaskPattern(System.Int32)
extern void QRCode_isValidMaskPattern_m59ED6D55C2CA143BF2399A0AE207B84E2281A6E7 ();
// 0x0000021B System.Void ZXing.QrCode.Internal.QRCode::.cctor()
extern void QRCode__cctor_m136724C96428B3226B5E8BB266DF4648C690DAE9 ();
// 0x0000021C System.Void ZXing.QrCode.QrCodeEncodingOptions::.ctor()
extern void QrCodeEncodingOptions__ctor_mE72E2B3C7D465893A5B4A173C2187F81EF95FECA ();
// 0x0000021D System.Void ZXing.QrCode.QRCodeWriter::.ctor()
extern void QRCodeWriter__ctor_mE79E7DD767D5CC5936047CEBD617FAB88B5F3DEA ();
// 0x0000021E ZXing.Common.BitMatrix ZXing.QrCode.QRCodeWriter::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
extern void QRCodeWriter_encode_m2BF5BEE9968D266156FA1F71BB5167D1CA3F76FA ();
// 0x0000021F ZXing.Common.BitMatrix ZXing.QrCode.QRCodeWriter::renderResult(ZXing.QrCode.Internal.QRCode,System.Int32,System.Int32,System.Int32)
extern void QRCodeWriter_renderResult_mD16F8C4BA46AEFAF7593E56CFC80CADEF7EC3371 ();
// 0x00000220 TOutput ZXing.Rendering.IBarcodeRenderer`1::Render(ZXing.Common.BitMatrix,ZXing.BarcodeFormat,System.String,ZXing.Common.EncodingOptions)
// 0x00000221 System.String ZXing.SupportClass::Join(System.String,System.Collections.Generic.IEnumerable`1<T>)
// 0x00000222 System.Void ZXing.SupportClass::Fill(T[],T)
// 0x00000223 System.String ZXing.SupportClass::ToBinaryString(System.Int32)
extern void SupportClass_ToBinaryString_mF93C2C2E4649147F9B8DBD4CDBBFBA9A2E697171 ();
// 0x00000224 System.Void ZXing.Color32Renderer::.ctor()
extern void Color32Renderer__ctor_m83FA3733A18A24F6DB483E01FD636E78DBF95198 ();
// 0x00000225 UnityEngine.Color32[] ZXing.Color32Renderer::Render(ZXing.Common.BitMatrix,ZXing.BarcodeFormat,System.String,ZXing.Common.EncodingOptions)
extern void Color32Renderer_Render_mDC9622F58D193D3241888E7309F18F9342F17A4F ();
// 0x00000226 ZXing.Common.BitMatrix ZXing.Writer::encode(System.String,ZXing.BarcodeFormat,System.Int32,System.Int32,System.Collections.Generic.IDictionary`2<ZXing.EncodeHintType,System.Object>)
// 0x00000227 System.Void ZXing.WriterException::.ctor()
extern void WriterException__ctor_m984B83060D67D633A9496118D6CA5E07CCF055E3 ();
// 0x00000228 System.Void ZXing.WriterException::.ctor(System.String)
extern void WriterException__ctor_mA8B31A4537F9BBE1047525E91F2E9691C033E243 ();
// 0x00000229 System.Void ZXing.WriterException::.ctor(System.String,System.Exception)
extern void WriterException__ctor_m4AD7D8048A426E901C4322D3A55233C831B5C6F2 ();
static Il2CppMethodPointer s_methodPointers[553] = 
{
	AztecWriter__cctor_mF914CB9DB393C2FA5D1FC88E098A26F4BCFFC5DE,
	AztecWriter__ctor_mF9FDBBE7E3966A4EC3CC6765E0D4A065965B3740,
	AztecWriter_encode_m7C66AF93FA3E95CDF572E4F8757780717A4BF5D0,
	AztecWriter_encode_m0CEDB4DE70F4BC3D10FAC763133BBB008C38E5CF,
	AztecWriter_renderResult_m9C9791074A5B1D60960ECEB7C0EF7BF2B4E59154,
	AztecCode__ctor_mF636662C532ACBAD7134194BF92FB7D057F03D3A,
	AztecCode_set_isCompact_m710918DC850E7099DE68C013DF97197A309F2B66,
	AztecCode_set_Size_m35C1726D043706642500A6A6EDAB6D3A08020377,
	AztecCode_set_Layers_m5FB29256969798E1E5A64680B796B91841576B7D,
	AztecCode_set_CodeWords_mD32CC8A7A886E1EDC04F7825405AEA9CA918EB12,
	AztecCode_get_Matrix_m7522E62499B97454D10F0C58CCEB2203BC70797D,
	AztecCode_set_Matrix_m290313F9EF40D642DA239ABD8669A34FE75F7353,
	BinaryShiftToken__ctor_m29F17C3CE33045036754AC46485E075B85EFF793,
	BinaryShiftToken_appendTo_m0BDB93A3C838AA40C0A9333F07F212A667DA8B34,
	BinaryShiftToken_ToString_mDB69215F24E82204B99581AE5741D7B6B7B7EF6C,
	Encoder_encode_mBED400BE062A21825DA4B2D0E931BB0BF6D605CF,
	Encoder_drawBullsEye_m7DA324EE7286839BE83BEAA642109B7F6778310D,
	Encoder_generateModeMessage_m78514571851BCFCABEFD19A514426C229B32CFE0,
	Encoder_drawModeMessage_mFAC54756787EA1133F6CA921B0D09EF765CFC3D1,
	Encoder_generateCheckWords_mC78EF651D24DDFA74410A2A407AADB5A92F0B9BD,
	Encoder_bitsToWords_m1091CAAB9A0F56C51DE358DE5994A9E69A577879,
	Encoder_getGF_m9525A32AD2D478549FF20197BA2854B337F41DEF,
	Encoder_stuffBits_mE99A0E5DE9CAAA1B4E01242DA0EB5A42FC03036F,
	Encoder_TotalBitsInLayer_mCCDC72547500EE4DC42697165C9FD7A2B4612D8F,
	Encoder__cctor_mF57D6F26D1DFF5DB371886FD7494D955FBC235E0,
	HighLevelEncoder__cctor_mEDFEDE35542D90FC7D837A763B4269980C6EE141,
	HighLevelEncoder__ctor_m5CEA27B8A8CA4B498D86E6BFC775E88667CECD7A,
	HighLevelEncoder_encode_m0B68A1015975F3333A741ED0576FB71FD84A3DEE,
	HighLevelEncoder_updateStateListForChar_mA56C4AD15C601D108D3059B8843A0BBFE68BE6B5,
	HighLevelEncoder_updateStateForChar_mC5BBCAC1712ABEE43F117718F764E2B15E872484,
	HighLevelEncoder_updateStateListForPair_m8EA52D91E978058EA1959ED96A459CAEF413F16A,
	HighLevelEncoder_updateStateForPair_mC75922B79F4ABEC4486532CDC0495F93962EF067,
	HighLevelEncoder_simplifyStates_mE6C18FAD7CF1057F46FC9E53042638B0B2B826CA,
	SimpleToken__ctor_mD7D692213BF489D7D1FB5BD466E0605B7B559E20,
	SimpleToken_appendTo_m3CFD844FA2574265BC74F8D1F817BC7FC0E27633,
	SimpleToken_ToString_m5F57CCDB981468C35C539ACDB44B11709311D2C5,
	State__ctor_mAC6145C205F642D82813613546183A59F17E4075,
	State_get_Mode_m68510C11FC045C8E11D8AD457904F8F8AECEB586,
	State_get_BinaryShiftByteCount_m7BA9567AA822E85D066B6A1A2CC6F7EC2EED204E,
	State_get_BitCount_m96F2120C1F5543D25777F7D0F0074AA3B14F55A6,
	State_latchAndAppend_m5AFBF3B41913BCF41BBC300FF194E5D8CDC05D8E,
	State_shiftAndAppend_m5D5CE9892634A7506D99FF48F8861ABEF23ED83C,
	State_addBinaryShiftChar_m4333B7301F3C18481FFBBCB8772A745F9DDB4846,
	State_endBinaryShift_mA63F70B3F707589C73771B8F668CF4232548F3F1,
	State_isBetterThanOrEqualTo_mBC51BEEE139936736F17317E4219BC72453F47CD,
	State_toBitArray_mDD389077080992DEE4740D5D33A03F65211A3860,
	State_ToString_mD72A4DCB3D869F5FC417E545A586709981EDF964,
	State__cctor_m7393279D4D594C0BE15CA2078C601EE57831EC88,
	Token__ctor_mD23F2BE1F701847FF1B5F0BB08E460C006CFE4B8,
	Token_get_Previous_m28898878535A86A5B9C80E347EAC2AF7105025AD,
	Token_add_mB53984B13180297251374AFA1153DD02B362EA3E,
	Token_addBinaryShift_mA3A2E710E2E2E1E5D0D890A12722FFFE5D097B1E,
	NULL,
	Token__cctor_m0170DC5D22E28131515B87426A018A41FE584839,
	BarcodeWriter__ctor_m35737422E781C6D87D44E816AF5C70CE62767BB3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Base10BigInteger__ctor_m6C860B744FE3F762B03FC88AB276C36E1EA6736E,
	Base10BigInteger__ctor_m4842E436D2AEC111644D2F1F057AB6654C6139C2,
	Base10BigInteger__ctor_m7B47E0A2AD545173E730E6ECA5B37BBA8F6CDE0F,
	Base10BigInteger_set_NumberSign_mA34ABAF91342CEDF42666568BB5A85250D5042F2,
	Base10BigInteger_Equals_m037078BE651380C7D86FDB3ECB765BBA85696BA5,
	Base10BigInteger_Equals_mE62E2553E258FA4EEA7DC4731B48A2F2EC82CAA7,
	Base10BigInteger_GetHashCode_m9C6B858101EEE01E0E7A792EA3784BC41B4F0B47,
	Base10BigInteger_ToString_m67D58A86A3E9314F3903B6AA2E7CF350B38F75DF,
	Base10BigInteger_Opposite_m686350CA05971336799D2B94B2B8EEA6F1CD5A8A,
	Base10BigInteger_Greater_m60E7AE0022D7A9B167C9422BF725B5CE6B7F9EE8,
	Base10BigInteger_GreaterOrEqual_m6566D08D4E1FA7361E4201B6906F560A80A73FB6,
	Base10BigInteger_SmallerOrEqual_m638F37E3EC944921AF6AA6CF8F775445B281C43A,
	Base10BigInteger_Abs_mFED8D9ACB754EEB150906C82B1C27771F582E2A8,
	Base10BigInteger_Addition_mD60E8B54E68D7916A1B33EC18B72B06EBB661F5C,
	Base10BigInteger_Multiplication_m9A35FDBE28806F90BC857C76CD097FCF6DD3C69A,
	Base10BigInteger_op_Implicit_m2C6F5F174E9B8D973154E363A1289E4032D6D109,
	Base10BigInteger_op_Equality_m4EDBBF3C2AD667295ED6C26D04DE5E6C835FFEF0,
	Base10BigInteger_op_Inequality_mD4FBCA391A1DB5949BC62068251CE8EC4C39CADD,
	Base10BigInteger_op_GreaterThanOrEqual_m03B339289F68E6B8A7818A02E60282F54B4E884C,
	Base10BigInteger_op_LessThanOrEqual_m96C5864BB2EA060D05E2BBCA088008870B9485E9,
	Base10BigInteger_op_UnaryNegation_mB45824A99755CBFAE60E57730040A80184D7C30C,
	Base10BigInteger_op_Addition_m450FCB40AE2F028AF793830E932D020BEB88EBAC,
	Base10BigInteger_op_Multiply_m3AF47B9536AA8DA3796FB876A9DB09791EEE8437,
	Base10BigInteger_Add_mCC1E4047A312F40A78C6F7FD994CDB484D955925,
	Base10BigInteger_Subtract_m2D69C111AE6469DA2E99A3290F71FA8613395D58,
	Base10BigInteger_Multiply_m42B00519FD20568C488041861351AC4173CCF56C,
	Base10BigInteger__cctor_mFEE513D02C85FFD1A76F202610C0E64532F2E9A1,
	DigitContainer__ctor_m861AD7516AB9B3A51A73A1A9C40B2832E1534F5B,
	DigitContainer_get_Item_mC8EB6BCCCDC50C9B843DE7D704D9EF119F6DB232,
	DigitContainer_set_Item_m499ED1EE74216FB85BC69823ABB42B84EE3104CA,
	BigInteger__ctor_mCF2C1A45FF522D92669D3CDE8249D49F95B424EA,
	BigInteger__ctor_m0E16B54C2243242463EF7C4D3A44272B50E88DB8,
	BigInteger__ctor_m9FA291997079054C20FC01E3C74BA51277DC8F71,
	BigInteger__ctor_m3FF51CEDDE50EE0FE69B9371339569FB178F8CC9,
	BigInteger__ctor_m2B68A54A41E3D20DEF189E36B9CA8F0DED1CF269,
	BigInteger_GetObjectData_m0135E73F32A69E455AA85156734D13089968FC31,
	BigInteger_Equals_m480221D5B04CCC0545E452D3C7FEBFF9E80D4E60,
	BigInteger_Equals_m9D81B35ADFAFC95212D13260DEA165FEB345E20C,
	BigInteger_GetHashCode_m216F2C916483A4C15E6E63B9BBF82BD36376B726,
	BigInteger_ToString_m6CD82EF9A23204683EC8D161D1950DC17BD44FA4,
	BigInteger_Parse_m26369384240E51FAA3011A9383A2251A054D5B1B,
	BigInteger_CompareTo_m7C702C7EF4362E21CC1B48F0FA0081327908C089,
	BigInteger_CompareTo_mD046F0342C90C14E9079B45756891F597F9EC950,
	BigInteger_Opposite_m226222453D57320DF7EDA77BE54E26B51AB6EC99,
	BigInteger_Greater_m173015695BA1E8F122C5BBCD18E29344A2A83EEA,
	BigInteger_GreaterOrEqual_mEFFF6A3839421532475A7B39A3AEFD1EB71E7554,
	BigInteger_Smaller_m7016E935CA821FD7CBD511438E49F37B010D7DFA,
	BigInteger_SmallerOrEqual_m0A7D0939BB9E54809981C96003177D1FFB33EC3C,
	BigInteger_Abs_m874A34DD42172540D0149FE8CD3182D7DBC4A61A,
	BigInteger_Addition_mA14D9813A5180E109F48363FBB62A8BD8E22A58B,
	BigInteger_Subtraction_m217518DB54A9BA3B00B0E9FB3BDED90D81968606,
	BigInteger_Multiplication_m6B8A0B101868EF880962BB5DF53B36617F69D61C,
	BigInteger_Division_m8F18818EA9A6536042E61C2E4E248E8906839553,
	BigInteger_Modulo_mAF8624E85A3EE794802EDA8FAA315B72EB1B1FCE,
	BigInteger_op_Implicit_mE6D986884BD80B1E696720104B8EBE3AEEA44980,
	BigInteger_op_Equality_m9C45C1EF3BB845AAEFF4C98B9E4685FAED3A4991,
	BigInteger_op_Inequality_mC8CF7E512E1721ADFCF4FE0418E7AE230689B025,
	BigInteger_op_LessThan_m7AD77825BC73C75A6CE950115874E0A5230F8358,
	BigInteger_op_GreaterThanOrEqual_m74E6AAE86671A31D0D4B025C62EC1076BE65DB4E,
	BigInteger_op_LessThanOrEqual_m1EEBF7EF98EC693B6EE9CE84FA08FDD5D3C51CDA,
	BigInteger_op_UnaryNegation_m0A665EB313980CD31BD6F6E465CA1C34B7023C1C,
	BigInteger_op_Addition_mB9879BDDA43B2F540E3C96A8AD2FA673F24702C7,
	BigInteger_op_Subtraction_m7ACC641C06DDE0AFC0187ADDBE05CE08D99D343B,
	BigInteger_op_Multiply_m3D39D9B8CEC35FA413984E1EA7763DB7CEE39910,
	BigInteger_op_Division_m1FBB22ABB15D0E4289F7D0F32F4C79D5758EE365,
	BigInteger_Add_mB0FB4E38EA21E16FA8059858D913F7575086389A,
	BigInteger_Subtract_m873FB3C134FFB768959FA1BF4255397D8E65DAE6,
	BigInteger_Multiply_m61FE301F61485FF64FE2F5120A4A608ECE5B4714,
	BigInteger_DivideByOneDigitNumber_mE713956C0268D8814097FC4117D78D814BFE8344,
	BigInteger_DivideByBigNumber_m22226F7D49644446C885E1443121EDD40CBFCC11,
	BigInteger_DivideByBigNumberSmaller_m69B726AB842BDF3875AB880B8051A18E9EC4684A,
	BigInteger_Difference_m86F180B783B01353273EF5B756CA6D344308781F,
	BigInteger_Trial_m65E96A00D96DE298D81BAA826E529DFB94CEB8A4,
	BigInteger__cctor_m2B0CBD84D881AD79047FAE6684D025263A4F41D6,
	DigitContainer__ctor_m5F64CF9D6BD73E2720A85D10CE575E14F8D83D21,
	DigitContainer_get_Item_m1E4626878E0A8C44D7E9F68CFC495E2923A3911B,
	DigitContainer_set_Item_mB359EEEF796A35EC9BC1CA363BB0A4EBDA2DDA59,
	BigIntegerException__ctor_m7B4F155DEF04583BD7F927D4260CF3EA10697DCA,
	BitArray__ctor_m447EC39754D52295A74117EA49A803B99649EFE9,
	BitArray__ctor_m62A2D29F7D3417E86A4B9B3CBFE38AB2FA1081E5,
	BitArray_get_Size_m734BC4F5E57FD38D95E483C6E9427595CADFEFA0,
	BitArray_get_SizeInBytes_m228A29F53BBFDE448F0AA39F1C5A367135B0710C,
	BitArray_get_Item_m1E0112C7EB188828A246839BEFE04D04D0B3DD6F,
	BitArray_ensureCapacity_mF36226C608FE756765C67B0E003A6FD6D0407B9D,
	BitArray_setBulk_m2E500414B7579D0AB91B86D06F06B43B0DC6D2FA,
	BitArray_clear_m99902C6E7AEF3CAB31A3BBDA119797E78AA49686,
	BitArray_appendBit_m51BA3537DBECE11102AC993DF7AB411600CE95AF,
	BitArray_get_Array_m43621FAAC485557AF6FBA8C38C3C83798841F944,
	BitArray_appendBits_m3FF17F258145BF037BE614A5AD5AC17EE2BE496C,
	BitArray_appendBitArray_m38E9DF9AAB925DFF0987A05A6B772832E790DA81,
	BitArray_xor_mC318BD1A29B7B2E6EAACD4A8085B51B431EED6E8,
	BitArray_toBytes_m8BB01B224FAC67447D67C3C8DFCA7F436522FF48,
	BitArray_makeArray_m010C5493596C42A13D5EFB1A97D07C76EB9005D9,
	BitArray_Equals_mF0F0307A759610B642EC29C5D819C5B4DF2CD50F,
	BitArray_GetHashCode_m3A36F70A9D5FD427FF575F75A4AE717632F26257,
	BitArray_ToString_mECA006F58F4F1FF42B845F6DFCC5743B7FAE9A4C,
	BitArray__cctor_m3F78027BD2DC8DE75D5F708781403FC3F695534C,
	BitMatrix__ctor_m59A0D6C0309D72242D814F872A7081A207F6564C,
	BitMatrix__ctor_m37A95608226171AF5ABB00C563ACB5C163680D7C,
	BitMatrix_get_Width_m401A654CF5F80320401B04182BCDB5B51DACB03F,
	BitMatrix_get_Height_mB09884394EDDD3ACEE910846E7A7433CAFA0569A,
	BitMatrix_get_Item_m7C16B005C94F9DBB687F2BC3C8E4DB996D4C0E87,
	BitMatrix_set_Item_mE71340CDB04E7EF55E66BED1AD4D7D540614FB94,
	BitMatrix_clear_m27DD9424D87D42360059B7C261D1765A79D56323,
	BitMatrix_setRegion_m81FFADFC79822D450F24FAF1583E89FA90C6A162,
	BitMatrix_getRow_m0FD1D8F749065384E3E55C83EE7D08C0DEE2BFB3,
	BitMatrix_Equals_mF239925437985AE58560701D61454A5DB01AC9F5,
	BitMatrix_GetHashCode_m23A7B81AC393789DAE6799C89B540D6273CF0D83,
	BitMatrix_ToString_mD4CD1278121673E02D928C1282C284564184F84D,
	CharacterSetECI__cctor_mF120D5E60A5EAF5ED7352C794FCCB4197244EE9E,
	CharacterSetECI__ctor_m288E0A497930AB078EC653AEF53C41B3B71CA745,
	CharacterSetECI_addCharacterSet_m1ADBFE6459CA859B2DAC4FFE5A8D8F9B874F359E,
	CharacterSetECI_addCharacterSet_m39E93174C44C27BABB4766EC95CB89E17435F12D,
	CharacterSetECI_getCharacterSetECIByName_m8A5BEF5ACA7B8DCA4E39080E76082774BBC9CA44,
	ECI__ctor_mFA5C0E33E28DD95EBF553E413CF872D847D3A494,
	ECI_get_Value_m36D1C634BFE6B2C1B320CB4C7FD60BC9B7BFFBA9,
	EncodingOptions__ctor_m363D9FD3CD59DC41EE7C0C38D1B8012041DCC3F1,
	EncodingOptions_get_Hints_m8164F3129D6105DFFBA7CFC68BD5F318ACDBFF8B,
	EncodingOptions_set_Hints_mAD6CEED9E70DACD9F07023B7578BEE5B8A9F4B32,
	EncodingOptions_get_Height_m420663F469C0105FC54F2FA4A294A92A700A5E39,
	EncodingOptions_set_Height_m38ECF044CE137957E38DE5A8D06282554F5DC5A3,
	EncodingOptions_get_Width_m61EDEF62CD743BBBA5164C5DBA7529916534784B,
	EncodingOptions_set_Width_m9B3048830472BDCE08093649559AA695161C5793,
	GenericGF__ctor_mFAA9366A047AA42BEE8DD72D71C9EDB52433CD52,
	GenericGF_initialize_m0465BF1D33A82B72D5FB62FD6AD9EA61A625B9DA,
	GenericGF_checkInit_mBD21E6748ACB5F0D6B2318B0091EFDD161A548CB,
	GenericGF_get_Zero_m62CE6C876D9E420E9BBB2D219910032448C75ADC,
	GenericGF_buildMonomial_mF224AD012599E2A041261483616AD73C592AAE95,
	GenericGF_addOrSubtract_mB78A2634FB827539489FAB0C8E2FAC352BD7EEF0,
	GenericGF_exp_m9210FD9893001B36082411E95E612A6F9A6BC25B,
	GenericGF_log_mD96C86BA141FC104A891DEBCC72FC96BC5C02186,
	GenericGF_inverse_m4AB1A6A0A8E10D684FFDBCA5EF0A77831AE88B2A,
	GenericGF_multiply_mE1F0248324228772DE4475392CC446B6970FE413,
	GenericGF_get_GeneratorBase_m2750B801EE7BAE7C59A1BC0FBBC5F2BC4A0E3D6C,
	GenericGF_ToString_m6511ADC6FE9842F29B0358A834E177F92DF90040,
	GenericGF__cctor_mEB9818FE714E5C4E10EA921B11FE0D900976415E,
	GenericGFPoly__ctor_m960314B3510BF3F12143B6A38BB1790E77F24B5F,
	GenericGFPoly_get_Coefficients_m296CD9F7B5ACC16F448A338DA3977E4AED5695D1,
	GenericGFPoly_get_Degree_m1A1015BCA165E2BCE99E6B6B84575D8B8A922064,
	GenericGFPoly_get_isZero_m1E63B6A391165A4238A0C98CBD006AE281E2EAAE,
	GenericGFPoly_getCoefficient_m6C9CE85BF893795DB3A14E70EBC3DDB5B3A88FD5,
	GenericGFPoly_addOrSubtract_mE785B913612740A94C32AB7B268A6C8B8F60FD53,
	GenericGFPoly_multiply_mEB3732155390CC69C01EACA6459315B9D5E7F381,
	GenericGFPoly_multiplyByMonomial_m161CF186334E6F4B74216B82DE6C85E0746A4AAA,
	GenericGFPoly_divide_mA193CA32955473D81048378E38BB44AE0CF3C6E6,
	GenericGFPoly_ToString_mA14C1515AE0DBC32396555AE6A4147E64A2C6775,
	ReedSolomonEncoder__ctor_mE1DF0D1C4F4C719F9A02D12750F94AD1E036BB11,
	ReedSolomonEncoder_buildGenerator_mA71A2FAC804D37DF7A04D42525767C12AC320F4E,
	ReedSolomonEncoder_encode_mDBEA6B965F313DC0A95F8F7954D7660D8B873BBA,
	DataMatrixWriter__ctor_m258A636534E206DEE397EBEF90D0A2F593337E15,
	DataMatrixWriter_encode_mB79F935E06D6DEC373237DD78E3A0401C94A2B3F,
	DataMatrixWriter_encodeLowLevel_m8D4033B0F7C3C042F82E4A23130EA17F305E1AA8,
	DataMatrixWriter_convertByteMatrixToBitMatrix_mE24CF10ECECD14F181BC48B9BA87D718D9573916,
	ASCIIEncoder__ctor_m626A86365C898B3FFD9E120CAC7B241067FB36BA,
	ASCIIEncoder_get_EncodingMode_m15E395FDDDE8FD41419AAB2F00EF678C4DCA9D6B,
	ASCIIEncoder_encode_m4180056CBEBD87F65AF3D7215D3AF7DA34647DEC,
	ASCIIEncoder_encodeASCIIDigits_m1E3A40D88D173A3F38D48B60B08ED71295532F38,
	Base256Encoder__ctor_m8CBED0DC1545803033EA31888C5A6E5FFEA85E4C,
	Base256Encoder_get_EncodingMode_m801B0F1A69EDBE04D59698E6768F24F59EE95988,
	Base256Encoder_encode_m7BAC58C34BE993CE0EA5A8A3866343243E77944D,
	Base256Encoder_randomize255State_m308254D546E206CB40DC7367D8C5D9A5291FC5E1,
	C40Encoder__ctor_m1217F5ACE08F77B1FC446BE210ADF74A7B83E58F,
	C40Encoder_get_EncodingMode_mD0BF0DFCCBC1317A91B9E001585F3532E1A7B96C,
	C40Encoder_encode_m1B86D71DC289705D3DC662C4542A786615A8EFA2,
	C40Encoder_backtrackOneCharacter_m4E81CE2A0772C5E8E01845F305E668095D773179,
	C40Encoder_writeNextTriplet_m07F556069342607E4BA7B88E139D24DA075DF497,
	C40Encoder_handleEOD_mF417694C8A1C5812B324BEBD9760D78A977E3859,
	C40Encoder_encodeChar_m457E009DEC8A2E0110264154D4A13493FF2CB84B,
	C40Encoder_encodeToCodewords_mB7689ACD903AB5EB0999DB736CB42C6C7C0CC4DC,
	DataMatrixSymbolInfo144__ctor_m39A6680AAB68F9AA14AF502C3550BE87123AC42C,
	DataMatrixSymbolInfo144_getInterleavedBlockCount_m8E397842ED17EE2D12176EBC52FE361CF8266696,
	DataMatrixSymbolInfo144_getDataLengthForInterleavedBlock_mA9BD281C782CE97EBC190F5EA6EE81980B68F2CB,
	DefaultPlacement__ctor_m67215D65D8625FB03157A77C2D8AE7F7DCF3DE48,
	DefaultPlacement_getBit_m6C62D33EEA6AF14F9166AE2E5B67C6E3625B0661,
	DefaultPlacement_setBit_mDB203166E967E002D73BEEA19EC4430AD9D6510C,
	DefaultPlacement_hasBit_mAD7BA75EFDF34D3394ADC09C015897C6D1FE0F4F,
	DefaultPlacement_place_m673E91B4358421000993D4D8317B3E047F82E38D,
	DefaultPlacement_module_m081F643C371E107396283C051D83FB8F37F26D31,
	DefaultPlacement_utah_m0DF78D30D81B6EDCD6058AC00D3C30DA90135D27,
	DefaultPlacement_corner1_mFE5FB486BF96FD7345BF5026D827129ECCCF5AE7,
	DefaultPlacement_corner2_mC20D2B353AB2C45027F5298E1439A7DD59DEBC0E,
	DefaultPlacement_corner3_m7532D2BD89164EEEFA465C07808FC316568E1239,
	DefaultPlacement_corner4_m689E1FC7D2E8BD666E43FF2A12EE4E54BEF2B984,
	EdifactEncoder__ctor_mE7EDDB5212526DC17BDD61AF1DE3776157B6110B,
	EdifactEncoder_get_EncodingMode_m0B7B5FECA9991B1FA2E48989C7416335C3802EFD,
	EdifactEncoder_encode_mC332FD4608158BD6EB2785D0626A9C3E0B6F979A,
	EdifactEncoder_handleEOD_m5F93B61D0EE7D38F03FDD0EBC209145F08A4B752,
	EdifactEncoder_encodeChar_mA01BEC4D03C59FC0A2344BCC9FB76CC2540ECAF6,
	EdifactEncoder_encodeToCodewords_mC27058C11DDC5C08C6AF190C4F502414D3106E53,
	NULL,
	EncoderContext__cctor_mC0AB1D6282AFAB68FAE6951861718F8B03F4F283,
	EncoderContext__ctor_m75E1A4D50C7D5F3C966FA1C38BE421F6E4F0AD26,
	EncoderContext_setSymbolShape_m461AFA931620A91FEA8DD126087D47759391E096,
	EncoderContext_setSizeConstraints_m6871B5655B16118FD8D38FC888CFFA42793C4779,
	EncoderContext_setSkipAtEnd_m70958BC35C88E935FA0F5BED982663C8EA1F3F78,
	EncoderContext_get_CurrentChar_m05D8526B6D803EFF8F78859E387BE3139FBC80A9,
	EncoderContext_writeCodewords_m1B943B100E23DCED73CC2D41FBD4DA5C8019EC37,
	EncoderContext_writeCodeword_m9BA8A0B8B9CED2E0AA1112A1952F4CADA2B71B34,
	EncoderContext_get_CodewordCount_mF2C51F5BF3A3B6B7113FE9710DB2A76805F22604,
	EncoderContext_signalEncoderChange_m22CC9162BE624D8787A199DA9381B57527DC2784,
	EncoderContext_resetEncoderSignal_m4EB0A4F1173F3254C73FC7112727C69EA34A501F,
	EncoderContext_get_HasMoreCharacters_m165BA5507CEE8CBB020E6DEE4E1B53F4F00D4C55,
	EncoderContext_get_TotalMessageCharCount_m8DFC582940DB2F8FF553E31A1B4A705EF3E0C6C1,
	EncoderContext_get_RemainingCharacters_mCEA489131F8AC9AE25BAB3C8A754ADC610BF7E7C,
	EncoderContext_updateSymbolInfo_mB181CA2192B9C1E50C82133999A1A6EDF0926E97,
	EncoderContext_updateSymbolInfo_m2EAE6A1CFD629EB2E5B95457BA0C06B76E2A83FF,
	EncoderContext_resetSymbolInfo_m7EC92BFAF264D33E1BEA6CB9E593F14CF7CFA491,
	EncoderContext_get_Pos_m913E7BCAEF09FB214D1D0865142F988FCF91E5B6,
	EncoderContext_set_Pos_m447737B41665EEFC88A7F56BAC8D9784B8EA8FC9,
	EncoderContext_get_Codewords_m2489D33CD728B1E1CF1AE1FBF07CDF367AF6F6A2,
	EncoderContext_get_SymbolInfo_mFFB5185F4A6B35205A31BC2B0687C54639F5BADA,
	EncoderContext_get_NewEncoding_m7ADEF28046A6020CCF69F19DDCC9DE22A2DE8528,
	EncoderContext_get_Message_mC008DFA3436D7B18AE70D17696B04FA69231FE05,
	ErrorCorrection__cctor_m93166D1E927C86C42AEA0B97235A77A177C20816,
	ErrorCorrection_encodeECC200_m2E6EF339483574D20A3BCF0276DB171FFAB95CEB,
	ErrorCorrection_createECCBlock_mD9A9F6068D49D1DCB093D34F1B00A639DFF7513C,
	ErrorCorrection_createECCBlock_mBAFEB6C47566313E7CDAA9026F94CB51C09694D3,
	HighLevelEncoder_randomize253State_m4B99E67922BC723DAA9F0BD08E2C13B602E97428,
	HighLevelEncoder_encodeHighLevel_m46E0CBBDAB78E9207A15E7247CD232F6F3FCD1FB,
	HighLevelEncoder_lookAheadTest_m92BB6C937FBFB8E239F373D4A733E55F091E66DC,
	HighLevelEncoder_findMinimums_mC38ECAA2B8E783931BA2629A78505372A8A4220D,
	HighLevelEncoder_getMinimumCount_m21C07EF2B483FB6787E826B4449559F32C1D0DAB,
	HighLevelEncoder_isDigit_m3B3093F8F7540A49961C9219F70A801F7CBDE409,
	HighLevelEncoder_isExtendedASCII_mD09A4A76901CE82416E5F526FEC9FC4E9EBB06EF,
	HighLevelEncoder_isNativeC40_m6CB164785EAD090E2AB675CCA371AB0229D38C17,
	HighLevelEncoder_isNativeText_m53ADB2C4F94CE1C91C533DEBA936A9B22FA48751,
	HighLevelEncoder_isNativeX12_mAE5D7EC1E0BBADE55299524AD3C026E459FF9C74,
	HighLevelEncoder_isX12TermSep_m8A3357CD9D0F8CDE8345B50BC51B8D8C2862A6D6,
	HighLevelEncoder_isNativeEDIFACT_m282041C2BD8247F59AD42C7161A9697FEC3DFFD4,
	HighLevelEncoder_isSpecialB256_m3854D76A4AC01B90EA88B08AE7ECFED51EBD965B,
	HighLevelEncoder_determineConsecutiveDigitCount_mB78CE09FEF1BEBB6AC5093FCEABC4453B3941E66,
	HighLevelEncoder_illegalCharacter_mE125A4B1ED9E275AD43ACE27FB29649171728884,
	SymbolInfo__ctor_m46FF19F77A65FE4A6410280F60DE36EB6AAFD199,
	SymbolInfo__ctor_m9ADF770A020D45B12EC133FBF1EEDB33639250B5,
	SymbolInfo_lookup_m4D2DAB622BC59696BA46BFBAFFAE2A38F44D7A43,
	SymbolInfo_getHorizontalDataRegions_m5BCC960BFE3B6E41451A1DF1CCFF9669FBEDA191,
	SymbolInfo_getVerticalDataRegions_m60BC96D1C1A2AAB2742FCA518D02AC12FB962568,
	SymbolInfo_getSymbolDataWidth_mFAE4F3F1978CD5AAA419E26AF4C2BCEA2ED3FA4C,
	SymbolInfo_getSymbolDataHeight_mADA19D3D43477BF256DEE15C135C9715022DADFE,
	SymbolInfo_getSymbolWidth_mA68950421BCDBDD480DC3CAB7A39B25AC07C9F19,
	SymbolInfo_getSymbolHeight_m71BE8D572FEEF517C3EE0F1563BE5EE901CEEAF5,
	SymbolInfo_getInterleavedBlockCount_m1D3E413742D06B5D2ABB96CC0A35E35325E330E4,
	SymbolInfo_getDataLengthForInterleavedBlock_mB2518CB22C004D3B7D332B051D772B70F4C4D5E2,
	SymbolInfo_getErrorLengthForInterleavedBlock_mA5056E9B8302A9E9309AAEDCBD0EF74A94239059,
	SymbolInfo_ToString_mFDA5D593AFD813C2CF1B097DB7947ED47DE44839,
	SymbolInfo__cctor_m87B8716E6C160D1DE582263F770375D3FF1254EE,
	TextEncoder__ctor_m56A82553B2C1F50A00CBB0A316E5DCAFB440D9A3,
	TextEncoder_get_EncodingMode_mD5210696A21436892D1397D403BF0CEE98FCEA87,
	TextEncoder_encodeChar_m928FFEC61C28655B325EC3F2E0BA156B43C106A9,
	X12Encoder__ctor_mADEC4C319CA6BF1C3AA93BB29F4F4B68FE6898D1,
	X12Encoder_get_EncodingMode_mBF3AA5B0509FB561447FFC2D8C5965D5FEA9D0B9,
	X12Encoder_encode_m7B099DD557F0568826A0AEBC0DBB7CADD8B59C64,
	X12Encoder_encodeChar_m8CA8B2A46FF37EDDB0F3410284FE18137138D2A3,
	X12Encoder_handleEOD_m50D078D264E9D383FE30B0FA53BF0AD8E0D25597,
	Dimension_get_Width_mACCFA71D5D3A26E3F8E42DA90024D564655ED991,
	Dimension_get_Height_m5DEDBE3C4E34F3BA06BC14B89F41644DE0FBF020,
	MultiFormatWriter__cctor_m1F83AB95E76186084300FDC74A829070C7C57BFA,
	MultiFormatWriter__ctor_m3F561F47E59DB70BAB7D6C70849F33A51D3F9BEB,
	MultiFormatWriter_encode_mA69844390DECC6B0715DDA3B36B6421053FD5646,
	MultiFormatWriter_U3CMultiFormatWriterU3Em__0_m058BD0D8C0552103375C4A2468910BD713974B10,
	MultiFormatWriter_U3CMultiFormatWriterU3Em__1_mC9CFCD52E56578199F731ADE7360BC7BE7964AC7,
	MultiFormatWriter_U3CMultiFormatWriterU3Em__2_mDD92C482067636D5A962C024EB8121A71D96ECCE,
	MultiFormatWriter_U3CMultiFormatWriterU3Em__3_m66097729FEFA189191A847AF61997861DC8605D2,
	MultiFormatWriter_U3CMultiFormatWriterU3Em__4_m5A96B249CEB330116C97662DABCE0669F30B59E3,
	MultiFormatWriter_U3CMultiFormatWriterU3Em__5_mE4021ACA5C7B605988E62378D0EBFCB6DB9DADD4,
	MultiFormatWriter_U3CMultiFormatWriterU3Em__6_m4DFF6B4AAAD8B16BF473D1D08051D0B908CEAFF6,
	MultiFormatWriter_U3CMultiFormatWriterU3Em__7_mA5622E9819A052EC83E87D635A349240965D79B1,
	MultiFormatWriter_U3CMultiFormatWriterU3Em__8_m024F89411F59C0958805B44A952835D1C2D3CFA4,
	MultiFormatWriter_U3CMultiFormatWriterU3Em__9_m946B82ED0DA726757A99266DE24D3A13D8DB3693,
	MultiFormatWriter_U3CMultiFormatWriterU3Em__A_m4B9DCE84DE764337D57DD23E844264911C910CC9,
	MultiFormatWriter_U3CMultiFormatWriterU3Em__B_mBDC56BE23CE88881CC412685036A8A2D060E25B6,
	MultiFormatWriter_U3CMultiFormatWriterU3Em__C_m93F50433EF820F62FF846B51B05199FC8E3BB2ED,
	CodaBarReader_arrayContains_mC9806AA3BEAD6FC8E6C32AEBE8F00E3DDACEE0A7,
	CodaBarReader__cctor_mC5B5A2FFA2C57B104BC414FF3290FC6FB0417E6E,
	CodaBarWriter__ctor_m5202D2CAAE23AD4D4DB74F23E7F9A2041578229B,
	CodaBarWriter_encode_m8EB2486F77135E4D3CEB1D947F87815F564BDF10,
	CodaBarWriter__cctor_m504C94576A62648CFF6CBF8A0E0801E164F9F06A,
	Code128Reader__cctor_m3DB7624E6B8C7869994BA5EC6D9CF00C06BE3E24,
	Code128Writer__ctor_m22B078CB74B77F0CC2CACAC963013D528716C54C,
	Code128Writer_encode_mC51F5EA6E2B7833DF96F9F4DBE88D7F6DAB88084,
	Code128Writer_encode_m9286F0B39A6B06C9EC09D0E4416C035AC73B4547,
	Code128Writer_isDigits_m96A99B8359090F5202E34B122692C7994BC38352,
	Code39Reader__cctor_m3B3BF9D619A9F539591757700C085FDBEF42D72F,
	Code39Writer__ctor_m43BEA76902BE650E57A2E22DEAE0597C35F13E07,
	Code39Writer_encode_m0D6EF9532618DC4C2D222D17CC2F4395B60D446D,
	Code39Writer_encode_m155A582A2D0BAC2FD48345D29209930C62596EB9,
	Code39Writer_toIntArray_mDFFB1F39383CA12E3DADC3D7B428170E8190D888,
	EAN13Reader__cctor_m00FC8F6485905889C10BF501536852BBC126C331,
	EAN13Writer__ctor_m4BA9CDF077F7939F59B97C7E26632107905337CF,
	EAN13Writer_encode_m14AB9E8542D567B70B6ED98672B57106F1EA3599,
	EAN13Writer_encode_m21F4EA32965C436527FA0FDDAEBBF50968A83794,
	EAN8Writer__ctor_mA595B48F927E64FC41516287D992E5354BB0BDA4,
	EAN8Writer_encode_m5D8A5724E05DAB33992FB819A8996BA817FC83BB,
	EAN8Writer_encode_mA77CE27B74C1FE8153251CF4A959CCAF3C86D10C,
	ITFReader__cctor_m27DB4754670C761A31CB80DA5926A3879260FA96,
	ITFWriter__ctor_mD7BAF73A3C0A6C2ADEF14CCD75C6C65D713C7DB5,
	ITFWriter_encode_m995A20BCE0DC27C55190734E0FF0D9713564ADA9,
	ITFWriter_encode_m0066503270D2FDCC8C2675265FEEB76CFB7516D1,
	ITFWriter__cctor_m0E557D2B5F84BD7EA240030FEB700CEB7DFB9363,
	MSIReader__cctor_m792B9C2163A64B6D69D0A062C4786DEBBB7DFD46,
	MSIWriter__ctor_m6875D19008A44DB22BA2D09FBE8267720FBAACB3,
	MSIWriter_encode_m9ED75C0CBA96D7E7EB6E449DD6A599B3EB532BAC,
	MSIWriter_encode_mB3369A4A82F1CEE1CFBE75A80B8BCD30D802B578,
	MSIWriter__cctor_m21D3CC2DF0E4A0194F4CCA7B94F844791119DDD3,
	OneDimensionalCodeWriter__ctor_mBB8E043242D5DFDC8AD9234F859A043D0ACF5652,
	OneDimensionalCodeWriter_encode_m7861865038725E794F40AC1CF7CA17285C86D099,
	OneDimensionalCodeWriter_renderResult_m4B7FFDBEA64BF3F77DF2B1E4D366BB211AB432C4,
	OneDimensionalCodeWriter_appendPattern_m3D16DA3CB2C4E63F4274FA0EBD3CE2A725648931,
	OneDimensionalCodeWriter_get_DefaultMargin_m17487CC2733C1BE8694139CAD8E666C0D55ECE22,
	NULL,
	OneDimensionalCodeWriter_CalculateChecksumDigitModulo10_m3950D5CBC5AE98971ABC4D762A191A0E04C65F1C,
	OneDReader__cctor_mBB4730F04809247FD4C535080AE6FE407876D823,
	PlesseyWriter__ctor_mBD7EDAF3B209D7459C754586961C39A243F2E9D1,
	PlesseyWriter_encode_m7A22084B0EB8BCA13BEACD671E6FB517EF0257FD,
	PlesseyWriter_encode_m74D4E1424D03A4B9E0F540D0CAFC880E0B8ECA12,
	PlesseyWriter__cctor_m2F567000902DFF6E1B7E09C1E168F790472CE731,
	UPCAWriter__ctor_m3B824549A47812DAE9CB98654192251F73445A9C,
	UPCAWriter_encode_mE9A01BDD9B3DF4A0FB29EC50CC7DA6A223B6E375,
	UPCAWriter_preencode_mACE2F02AD3073AAEDD2BBFC222CF7B5A17D7A0A5,
	UPCEANReader__cctor_mD0CF54D611A3617883A374A1AD9844219280D15C,
	UPCEANReader_checkStandardUPCEANChecksum_mE33A361746A41CE9D38B29EBD3FA0CDEA397D90B,
	UPCEANWriter__ctor_m0A28E8C9249F38E860C97422ED3CA70D0A8C46D6,
	UPCEANWriter_get_DefaultMargin_mC6F57ADEE8B468029D63739375FE0D2A731D783E,
	BarcodeMatrix__ctor_m5DBD05102C2AD6761E199CFFD6AD8633E6D4FC4F,
	BarcodeMatrix_startRow_mE36338175E41CDAA666D3BE9F2C6ADEABEA3BE29,
	BarcodeMatrix_getCurrentRow_mBAD8926BAC5196A0500A2A01390090EA41AEA674,
	BarcodeMatrix_getScaledMatrix_mB02AA858E62712D382782AA2F269D12675E3B305,
	BarcodeRow__ctor_m091E0F4546C595095DEA7A7DE2A0E9BA29C47A13,
	BarcodeRow_set_mE3BAA21801958C5608E86E761D5FC7D971D4DD5F,
	BarcodeRow_addBar_m60654DA0FBF8DE37BE4B94BDE4E1BF23726F7C2C,
	BarcodeRow_getScaledRow_m4D5C0A6303045F395C6458EEB8E6CCBFDBC45C45,
	Dimensions_get_MinCols_mB8D2C7363D5C831C8D74D9FCA4CB1CFE85D7A641,
	Dimensions_get_MaxCols_m8DF1BDAFAF1EFFFB6C76C043A61F3AD63B869121,
	Dimensions_get_MinRows_m525FE37BE5828E36C65D0D3ADE65DFCEEF8CA4EF,
	Dimensions_get_MaxRows_mCEEEE9EA9B5CAB6FDF18E7338973DD633F68D17A,
	PDF417__ctor_mF9ADE9689EF192C2C55CDBC28B086E4AC639514D,
	PDF417__ctor_m45054C54F872DCF546398EBB6866F58F5CE8F3AC,
	PDF417_get_BarcodeMatrix_m24D58A573A242F616F3A63EFE896D1E4A927210A,
	PDF417_calculateNumberOfRows_mF3A0A3440F71845DA1B3B2E6F4814D0B4E2FA2A4,
	PDF417_getNumberOfPadCodewords_m36E9A4C55DCA18D63DDF4164A117D3D6C92ED62A,
	PDF417_encodeChar_mF802F2ABF981B0DC82E5D78257553774FF154611,
	PDF417_encodeLowLevel_mC7E8814F16537C6A1592AED7355ED9C887FD11B9,
	PDF417_generateBarcodeLogic_mCB020F6A8EC35D0D88F93700D78D471A9D3589F5,
	PDF417_determineDimensions_m27B2CB33E7770D8697EE235EEC75567647BC389C,
	PDF417_setDimensions_mC4CDD7A9B545E17E90D0272713AFCE189C801E8A,
	PDF417_setCompaction_m836C7FE76054B28C08A1C17B60E6BAD4E0BEA3D4,
	PDF417_setCompact_mF1942E10F3EE643425EA9E1DA4B052888BD53994,
	PDF417_setEncoding_m534DAC5EC1FAD6DD24237BA3DE09A969A15B6F72,
	PDF417_setDisableEci_mE0693A66EA880E4AFB65E1B45E202755F37849A2,
	PDF417__cctor_m38AF1F2805D350041C9FF9AC3782BFEB6B031308,
	PDF417ErrorCorrection_getErrorCorrectionCodewordCount_m96D0CE86CAC53266702D10CD0AE2CC30750CB203,
	PDF417ErrorCorrection_generateErrorCorrection_m0807A2A8DA2A6F74F334C526F81E27DC06E7C376,
	PDF417ErrorCorrection__cctor_mA0ACFA7AC1C8D495F64CC0954D9866E5B59CCEEE,
	PDF417HighLevelEncoder__cctor_mA8C4376FA06A0315699DFC078D1AEB7D594E8C27,
	PDF417HighLevelEncoder_encodeHighLevel_m7083A8CDECD7430AB6B00D9F241EAC1268CB8C87,
	PDF417HighLevelEncoder_encodeText_m1DDF567682712F498DEA0F90FC7C9CEA3577BE38,
	PDF417HighLevelEncoder_encodeBinary_m4DFA40CBF13BBB42B59F74774E0409D326E01447,
	PDF417HighLevelEncoder_encodeNumeric_m03C76815D3EA23BEAC9BBD7336435CAA6C6964BC,
	PDF417HighLevelEncoder_isDigit_m3562DD53157C7C3C7EA2B5A0E0CC14F94188BD26,
	PDF417HighLevelEncoder_isAlphaUpper_m1549A2A58B18709B61457BCDB49588A188919758,
	PDF417HighLevelEncoder_isAlphaLower_m6E70B55DAE6E77B519222265599D806BB62C690C,
	PDF417HighLevelEncoder_isMixed_mD79AB0FF5773D30F1106EEE38F9758A0ABED9404,
	PDF417HighLevelEncoder_isPunctuation_m6D3FE0F92C091195A70FFF6F3D16C34872ABEC85,
	PDF417HighLevelEncoder_isText_m15068E3B6C85C685A09404F03FCEED35479F5870,
	PDF417HighLevelEncoder_determineConsecutiveDigitCount_mFA58B9FE87C4F2F5A7A7977DF7BCB9CFA6F8B6E3,
	PDF417HighLevelEncoder_determineConsecutiveTextCount_m56FE630D7BEBEF8569194CB6F9B5BB2FC8461909,
	PDF417HighLevelEncoder_determineConsecutiveBinaryCount_mD47FFBA34B82E1F9BE5CA0474CD043B96F1D8320,
	PDF417HighLevelEncoder_encodingECI_m25D3B5569531383232B1C00297DFA9ABD36D7C0B,
	PDF417Writer__ctor_mAC725952C2CAE0E1D362947812C71EE6A0B00AB2,
	PDF417Writer_encode_m0F2A3280FAAF82F5CAD66591111F42070D30F4BF,
	PDF417Writer_bitMatrixFromEncoder_mE15022FF1984EDF932770BFE9E475B6EF6540F7A,
	PDF417Writer_bitMatrixFrombitArray_mE8C4368A936A2ACBD161CA234907D687AE41C0FC,
	PDF417Writer_rotateArray_m6E2AD6244374C3985D44E10AAA8975669C2CAD46,
	ErrorCorrectionLevel__ctor_m27A32FDDFE0B62FE1586AEC0141C015D836BE553,
	ErrorCorrectionLevel_get_Bits_m2152ACD3809B1FE1073B0393B573240EC1C3505B,
	ErrorCorrectionLevel_ordinal_mEEB9A2AB26BB498E953CE969D388FC916E522469,
	ErrorCorrectionLevel_ToString_m6811EE04E4FDD101CE0791CC36BBF6C0D45F835C,
	ErrorCorrectionLevel__cctor_m0012DCB41E119F1920E24EABC47C6517336A4A46,
	Mode__ctor_mB43F347BDA84F6293144BB8DEF73E1EA7D51ACDE,
	Mode_getCharacterCountBits_m0443B77EDFE0DB92DFBC4B66D07FDADDC433356B,
	Mode_get_Bits_mF24E1F86934F81902116855C8DD5C693CF02FF90,
	Mode_ToString_m9F7D99459C5B76354A0925137B4356C0847B4A8E,
	Mode__cctor_mA7C80875E2817334E24F176CFB22A89964B6755F,
	Version__ctor_m035D6B8BCF041DBF1123AE7001DC9874BB27AC84,
	Version_get_VersionNumber_m3762234275AB877967256435C89AD25A89770BF9,
	Version_get_TotalCodewords_m639C102A1E8F571547502DE477E4CB364FB079A3,
	Version_get_DimensionForVersion_m76017F0F21F3D4EC59FDD9776DA1F04C0BC91B20,
	Version_getECBlocksForLevel_m4615B061C19EFA90ABBD3299E4E5125D4DBE3CDE,
	Version_getVersionForNumber_m07F3C488AE71EF6E0187BF28A54DF1F125D337BE,
	Version_ToString_m5EDC04A486C3D2B8FEC715DEA91F4678B2DD8115,
	Version_buildVersions_mAF4E45F358E2E8A62FE8626A5168695921533D49,
	Version__cctor_mC044E1E70D7FA2A267353A28D6D6D5ACFE75DB7A,
	ECBlocks__ctor_m273BD850A3379D1335D32BC7EE1421CA6F60F6DA,
	ECBlocks_get_ECCodewordsPerBlock_m038DC27C12D5D308E469DB99571FD02B307C0B1B,
	ECBlocks_get_NumBlocks_mB6635BF406786F37EE84719EF4F4EB5545EDF2E1,
	ECBlocks_get_TotalECCodewords_m7822705F7441B8531FFD48D55455AA9B80048F8C,
	ECBlocks_getECBlocks_mE3BCB1A131F4B969CA507F40B4BC943CB96A7C7C,
	ECB__ctor_mEEE7BB0A54D58BFAE0ED2EC3DBA48430602DD669,
	ECB_get_Count_m4FBB8FD4E11F2B8CE59D5EE756DF988D43C37D47,
	ECB_get_DataCodewords_m694477F0AD79E182212DECF38DCD53C259019B21,
	BlockPair__ctor_m644FB7916C45868707C61F60FE859D8F28E9CAC0,
	BlockPair_get_DataBytes_mACAFC0FC28CC4589485DDC6AB016FF224748542C,
	BlockPair_get_ErrorCorrectionBytes_m45E295FFB335B83D3BE687CA78FCE9304A496E41,
	ByteMatrix__ctor_mE319431A4A48D92ABE1C0B039C27665C60D26CD5,
	ByteMatrix_get_Height_m6FEEA80C7E4835A939F495C28FB903D37E7A1F27,
	ByteMatrix_get_Width_m930710557D0607CFFF125AC86B8E536684CA7B75,
	ByteMatrix_get_Item_m4F36BABC12EBEF956138502F8BD031279CA27D88,
	ByteMatrix_set_Item_m8F90ECD9E92D37625AD27AB96B64D92E4D1F6DED,
	ByteMatrix_get_Array_m970682D3B1F77B7510E6D0FD1EB9FC1D3846C351,
	ByteMatrix_set_mDB606FE13323A95D7C272A433B6F7C4153B37644,
	ByteMatrix_clear_m3355B67D71EF69B946B2671FF1C24BF3C751D58E,
	ByteMatrix_ToString_m656C037C3358AEE9BB909650D5D6EB670EFD2F7B,
	Encoder_calculateMaskPenalty_m215A23067D072A9698AB5E4CB35873AB512CD8C2,
	Encoder_encode_m7A8C877527E7D3F4365C54DE1B51438A67A616AF,
	Encoder_getAlphanumericCode_m687FFAE869348CD76C86957673D1C41101E5FA2E,
	Encoder_chooseMode_m23D932730C0E9E2E26DC7CA56B8B80C14178B68E,
	Encoder_isOnlyDoubleByteKanji_mF261330E02EE372D9AA64DC407B69AF1E6DB988E,
	Encoder_chooseMaskPattern_m0748FB6AA8EEE8BF9263A75BE90A04DCC216F4BB,
	Encoder_chooseVersion_mC9E221FA4FE07FE4E3049E221228984A7D23A4D1,
	Encoder_terminateBits_m772AF3829AAB3E651DA314681E7251359BC55DE2,
	Encoder_getNumDataBytesAndNumECBytesForBlockID_m306AB6ED034E0DA8C703FC76753890E3E32AF373,
	Encoder_interleaveWithECBytes_m9CDC66A9E752F7599EEC9556027FAA247B8D1280,
	Encoder_generateECBytes_mE7A1D8214278E1D21911B3B171CC0DEC5B24B553,
	Encoder_appendModeInfo_m5A5B954761F8753C897701E127B4D441383ECD9D,
	Encoder_appendLengthInfo_mC4AD32C750CD9E32DEBDE5C4CC4C3981AB5CDFC6,
	Encoder_appendBytes_mD072A75FB468DDC8DC570CEB375731E2AB12C461,
	Encoder_appendNumericBytes_m61A265C537F6E1ACBA9A4000C7E91947E23EBB2A,
	Encoder_appendAlphanumericBytes_m1DF5FEF2BB2A348AABDA85F7F708B01539C55FEC,
	Encoder_append8BitBytes_mD56813DE06D3606FAC8B840452082C6D575D3450,
	Encoder_appendKanjiBytes_m31816240EBCD08D0D75FCF92B554923B146C6148,
	Encoder_appendECI_m93FC04566D742677A28527204078BE4201683970,
	Encoder__cctor_m3CA40BBD197581777CCD0815830AA9954946A78E,
	MaskUtil_applyMaskPenaltyRule1_mEA63906366897B80B1699B56392FD106ED53B434,
	MaskUtil_applyMaskPenaltyRule2_m8158E370D36E1747358B6D191AF9FD997F40941E,
	MaskUtil_applyMaskPenaltyRule3_m9A9DCE1046E83939C3E655103730F52967474813,
	MaskUtil_isWhiteHorizontal_mD951D798698E7884B80E52E3BD1DE5A631FC1C02,
	MaskUtil_isWhiteVertical_mD0618326F1FE97B3717D4AC6A11DBB4EE8EC849D,
	MaskUtil_applyMaskPenaltyRule4_mE6C58669E7007119F716CEFBDB9F19F2C18F083C,
	MaskUtil_getDataMaskBit_m5A4B1F376C54704BAC501E78033364928971F3EF,
	MaskUtil_applyMaskPenaltyRule1Internal_m48550444F3D202B9278C853F4D1ADD732EC8DA24,
	MatrixUtil_clearMatrix_mF83179FD4D1FB0F5D89F744D050CD06534CF4FBF,
	MatrixUtil_buildMatrix_m9C894A4849ED2F812CE1FE9F489F20119DE16561,
	MatrixUtil_embedBasicPatterns_m65FDAFD119B3448D0AB5513CE4193483AB809C5E,
	MatrixUtil_embedTypeInfo_m7E7B2ADB02910195198EEB6B688AA1DB4E183056,
	MatrixUtil_maybeEmbedVersionInfo_m2FCC46EF4E66A798A412AC0CE3F1434D9C4A76B0,
	MatrixUtil_embedDataBits_mEBE77110DE06B78ECC970F282B841EA3C7B43801,
	MatrixUtil_findMSBSet_m3C20BCCEF8CAAFCF2A59C8C146D1267408F55250,
	MatrixUtil_calculateBCHCode_m1EB414F29A575EEE2DF2159DB84B3CD3FA330D74,
	MatrixUtil_makeTypeInfoBits_m024083AC3E075161BFBAB0CA8F444FC68040431F,
	MatrixUtil_makeVersionInfoBits_mA18CDE4C212E8226DCCCEFF83C443661144C7B89,
	MatrixUtil_isEmpty_m5CD556AB8396F6E43E2D713BC1E14E214C2C8341,
	MatrixUtil_embedTimingPatterns_mA8488BBC0A2E3F66753E12F6E5BB9A8505E0377E,
	MatrixUtil_embedDarkDotAtLeftBottomCorner_mB9FEB816B1F9F6B19F076CB36AF7EDDD72887261,
	MatrixUtil_embedHorizontalSeparationPattern_m74C6F55E9E99A039A7DB5F3EB02B933139D49287,
	MatrixUtil_embedVerticalSeparationPattern_m14B39EC0AF586DA8B46934978BDAFDF0CC737518,
	MatrixUtil_embedPositionAdjustmentPattern_mDEBC1795EFAA43156098FFA116EB0D83EEB700B6,
	MatrixUtil_embedPositionDetectionPattern_m683E83BE75FAD3113CD39DE2B7E966E4F4B26F62,
	MatrixUtil_embedPositionDetectionPatternsAndSeparators_m57ACC8675796F629640F7760B032FBFF32799C51,
	MatrixUtil_maybeEmbedPositionAdjustmentPatterns_mA17AD1BCC4921046BA0DF666BA04E107A6D6EE64,
	MatrixUtil__cctor_m49D328B7E0A02F70766EBDAC55D62E0BFEB93285,
	QRCode__ctor_m920631BE88BC14762FD14BB110E84809F4F1A2B9,
	QRCode_get_Mode_mE3911E5CD54E185F37448BA3FAC0AFBA7EE0D39F,
	QRCode_set_Mode_m6E8D799E18D75A0F9488D9A4D207C00C4B603A60,
	QRCode_get_ECLevel_m34D2255E9A0A349BBB961611A1CFE58B0EAC33D9,
	QRCode_set_ECLevel_mBAD215F7143693D8F8C7CD098601F1BA645C55F0,
	QRCode_get_Version_m1FB98D178F34C2AAE4BD26FE34BDDD1078D9CA59,
	QRCode_set_Version_m65521E9FCB5B4DFD8E279C40702C5B8593CA56BB,
	QRCode_get_MaskPattern_mD72431EB03A36284CC8E9164C2A174A8FE72CE58,
	QRCode_set_MaskPattern_mD6CD63CD0BE5BAA3AFBA1466AA3FD584785DC529,
	QRCode_get_Matrix_m1D78FE3793DCF8D820A92DB86653DA5C8068DB17,
	QRCode_set_Matrix_mDA276361828EEA7DC6E0271A8A9519CFAB56650F,
	QRCode_ToString_mCE2DD343CF18C8394D404C86BD9D7A3DC009C25A,
	QRCode_isValidMaskPattern_m59ED6D55C2CA143BF2399A0AE207B84E2281A6E7,
	QRCode__cctor_m136724C96428B3226B5E8BB266DF4648C690DAE9,
	QrCodeEncodingOptions__ctor_mE72E2B3C7D465893A5B4A173C2187F81EF95FECA,
	QRCodeWriter__ctor_mE79E7DD767D5CC5936047CEBD617FAB88B5F3DEA,
	QRCodeWriter_encode_m2BF5BEE9968D266156FA1F71BB5167D1CA3F76FA,
	QRCodeWriter_renderResult_mD16F8C4BA46AEFAF7593E56CFC80CADEF7EC3371,
	NULL,
	NULL,
	NULL,
	SupportClass_ToBinaryString_mF93C2C2E4649147F9B8DBD4CDBBFBA9A2E697171,
	Color32Renderer__ctor_m83FA3733A18A24F6DB483E01FD636E78DBF95198,
	Color32Renderer_Render_mDC9622F58D193D3241888E7309F18F9342F17A4F,
	NULL,
	WriterException__ctor_m984B83060D67D633A9496118D6CA5E07CCF055E3,
	WriterException__ctor_mA8B31A4537F9BBE1047525E91F2E9691C033E243,
	WriterException__ctor_m4AD7D8048A426E901C4322D3A55233C831B5C6F2,
};
static const int32_t s_InvokerIndices[553] = 
{
	3,
	23,
	2086,
	2087,
	202,
	23,
	31,
	32,
	32,
	32,
	14,
	26,
	35,
	27,
	14,
	202,
	191,
	2088,
	2089,
	202,
	202,
	43,
	119,
	297,
	3,
	3,
	26,
	14,
	58,
	107,
	202,
	194,
	0,
	35,
	27,
	14,
	201,
	10,
	10,
	10,
	199,
	199,
	34,
	34,
	9,
	28,
	14,
	3,
	26,
	14,
	199,
	199,
	27,
	3,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	208,
	26,
	32,
	9,
	9,
	10,
	14,
	0,
	135,
	135,
	135,
	0,
	1,
	1,
	217,
	135,
	135,
	135,
	135,
	0,
	1,
	1,
	1,
	1,
	1,
	3,
	23,
	182,
	209,
	23,
	208,
	26,
	26,
	111,
	111,
	9,
	9,
	10,
	14,
	0,
	112,
	112,
	0,
	135,
	135,
	135,
	135,
	0,
	1,
	1,
	1,
	1,
	1,
	217,
	135,
	135,
	135,
	135,
	135,
	0,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	357,
	1,
	1581,
	196,
	2090,
	3,
	23,
	182,
	209,
	27,
	23,
	32,
	10,
	10,
	30,
	32,
	129,
	23,
	31,
	14,
	129,
	26,
	26,
	483,
	43,
	9,
	10,
	14,
	3,
	32,
	129,
	10,
	10,
	52,
	1257,
	23,
	336,
	136,
	9,
	10,
	14,
	3,
	62,
	571,
	571,
	0,
	32,
	10,
	23,
	14,
	26,
	10,
	32,
	10,
	32,
	38,
	23,
	23,
	14,
	199,
	177,
	37,
	37,
	37,
	56,
	10,
	14,
	3,
	27,
	14,
	10,
	89,
	37,
	28,
	28,
	199,
	28,
	14,
	26,
	34,
	130,
	23,
	2086,
	1,
	0,
	23,
	10,
	26,
	434,
	23,
	10,
	26,
	2091,
	23,
	10,
	26,
	2092,
	137,
	27,
	2093,
	119,
	23,
	10,
	37,
	35,
	52,
	1257,
	52,
	23,
	336,
	38,
	32,
	32,
	32,
	32,
	23,
	10,
	26,
	137,
	2094,
	119,
	26,
	3,
	26,
	32,
	27,
	32,
	247,
	26,
	597,
	10,
	32,
	23,
	89,
	10,
	10,
	23,
	32,
	23,
	10,
	32,
	14,
	14,
	10,
	14,
	3,
	1,
	119,
	203,
	2091,
	2095,
	575,
	2096,
	94,
	48,
	48,
	48,
	48,
	48,
	48,
	48,
	48,
	131,
	2097,
	2098,
	2099,
	2100,
	10,
	10,
	10,
	10,
	10,
	10,
	10,
	37,
	37,
	14,
	3,
	23,
	10,
	2093,
	23,
	10,
	26,
	2093,
	27,
	10,
	10,
	3,
	23,
	2086,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	2101,
	3,
	23,
	28,
	3,
	3,
	23,
	2086,
	28,
	2102,
	3,
	23,
	2086,
	28,
	571,
	3,
	23,
	2086,
	28,
	23,
	2086,
	28,
	3,
	23,
	2086,
	28,
	3,
	3,
	23,
	2086,
	28,
	3,
	23,
	2086,
	203,
	2103,
	10,
	28,
	0,
	3,
	23,
	2086,
	28,
	3,
	23,
	2086,
	0,
	3,
	114,
	23,
	10,
	129,
	23,
	14,
	199,
	32,
	133,
	813,
	34,
	10,
	10,
	10,
	10,
	23,
	31,
	14,
	207,
	2104,
	2105,
	1067,
	130,
	199,
	336,
	32,
	31,
	26,
	31,
	3,
	21,
	119,
	3,
	3,
	2106,
	2107,
	2108,
	194,
	48,
	48,
	48,
	48,
	48,
	48,
	131,
	131,
	189,
	571,
	23,
	2086,
	2109,
	119,
	0,
	606,
	10,
	10,
	14,
	3,
	107,
	112,
	10,
	14,
	3,
	360,
	10,
	10,
	10,
	28,
	43,
	14,
	4,
	3,
	62,
	10,
	10,
	10,
	14,
	129,
	10,
	10,
	27,
	14,
	14,
	129,
	10,
	10,
	56,
	38,
	14,
	1257,
	31,
	14,
	94,
	2,
	21,
	1,
	114,
	777,
	132,
	571,
	2110,
	203,
	119,
	137,
	2111,
	2112,
	137,
	137,
	195,
	137,
	137,
	3,
	94,
	94,
	94,
	2102,
	910,
	94,
	667,
	2113,
	163,
	2114,
	137,
	488,
	137,
	488,
	21,
	177,
	488,
	137,
	46,
	163,
	163,
	2105,
	2105,
	2105,
	2105,
	163,
	137,
	3,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	10,
	32,
	14,
	26,
	14,
	46,
	3,
	23,
	23,
	2086,
	203,
	-1,
	-1,
	-1,
	43,
	23,
	142,
	2086,
	23,
	26,
	27,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x0200000C, { 0, 6 } },
	{ 0x06000221, { 6, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[9] = 
{
	{ (Il2CppRGCTXDataType)3, 18357 },
	{ (Il2CppRGCTXDataType)3, 18358 },
	{ (Il2CppRGCTXDataType)3, 18359 },
	{ (Il2CppRGCTXDataType)3, 18360 },
	{ (Il2CppRGCTXDataType)3, 18361 },
	{ (Il2CppRGCTXDataType)2, 23225 },
	{ (Il2CppRGCTXDataType)2, 23418 },
	{ (Il2CppRGCTXDataType)2, 25575 },
	{ (Il2CppRGCTXDataType)2, 23419 },
};
extern const Il2CppCodeGenModule g_zxing_unityCodeGenModule;
const Il2CppCodeGenModule g_zxing_unityCodeGenModule = 
{
	"zxing.unity.dll",
	553,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	9,
	s_rgctxValues,
	NULL,
};
